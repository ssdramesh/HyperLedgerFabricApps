#!/bin/bash

#Colors
DARK_GRAY='\033[1;30m'
RED='\033[0;31m'
GREEN='\033[0;32m'
ORANGE='\033[0;33m'
BLUE='\033[0;34m'
PURPLE='\033[0;35m'
CYAN='\033[0;36m'
LIGHT_GRAY='\033[0;36m'
LIGHT_RED='\033[1;31m'
LIGHT_GREEN='\033[1;32m'
YELLOW='\033[1;33m'
LIGHT_BLUE='\033[1;34m'
LIGHT_PURPLE='\033[1;35m'
LIGHT_CYAN='\033[1;36m'
WHITE='\033[1;37m'
NC='\033[0m'

#Source folders for templates
TEMPLATE_UI_DIR=${1}
BIZNET_UI_DIR=${2}
BIZNET_FOLDER_NAME=${3}

#Insert Entity Routes
cd ${BIZNET_UI_DIR}/${BIZNET_FOLDER_NAME}Setup/webapp
perl -i -p0e 's/<BEGIN_ENTITY_ROUTES>.*?<END_ENTITY_ROUTES>/`cat entityRoutes.ubak`/seg' ${BIZNET_UI_DIR}/${BIZNET_FOLDER_NAME}Setup/webapp/manifest.json
perl -i -p0e 's/<BEGIN_ENTITY_TARGETS>.*?<END_ENTITY_TARGETS>/`cat entityTargets.ubak`/seg' ${BIZNET_UI_DIR}/${BIZNET_FOLDER_NAME}Setup/webapp/manifest.json

#Update Selection view
cd "${BIZNET_UI_DIR}/${BIZNET_FOLDER_NAME}Setup"
perl -i -p0e 's/<!--BEGIN_REPLACE_RADIOBUTTON-->.*?<!--END_REPLACE_RADIOBUTTON-->/`cat entitySelectionViewAddEntity.ubak`/seg' ${BIZNET_UI_DIR}/${BIZNET_FOLDER_NAME}Setup/webapp/view/Selection.view.xml

#Update Selection Controller
cd "${BIZNET_UI_DIR}/${BIZNET_FOLDER_NAME}Setup"
perl -i -p0e 's/<BEGIN_REPLACE_DEF>.*?<END_REPLACE_DEF>/`cat entitySelectionControllerDefine.ubak`/seg' ${BIZNET_UI_DIR}/${BIZNET_FOLDER_NAME}Setup/webapp/controller/Selection.controller.js
perl -i -p0e 's/<BEGIN_REPLACE_INC>.*?<END_REPLACE_INC>/`cat entitySelectionControllerInclude.ubak`/seg' ${BIZNET_UI_DIR}/${BIZNET_FOLDER_NAME}Setup/webapp/controller/Selection.controller.js
perl -i -p0e 's/<BEGIN_REPLACE_SEL>.*?<END_REPLACE_SEL>/`cat entitySelectionControllerSelection.ubak`/seg' ${BIZNET_UI_DIR}/${BIZNET_FOLDER_NAME}Setup/webapp/controller/Selection.controller.js
perl -i -p0e 's/<BEGIN_REPLACE_DEL>.*?<END_REPLACE_DEL>/`cat entitySelectionControllerDeletion.ubak`/seg' ${BIZNET_UI_DIR}/${BIZNET_FOLDER_NAME}Setup/webapp/controller/Selection.controller.js

#Update i18n
cd ${BIZNET_UI_DIR}/${BIZNET_FOLDER_NAME}Setup/webapp/i18n
perl -i -p0e 's/<BEGIN_ENTITY_I18N>.*?<END_ENTITY_I18N>/`cat entityi18nProperties.ubak`/seg' ${BIZNET_UI_DIR}/${BIZNET_FOLDER_NAME}Setup/webapp/i18n/i18n.properties

#To avoid Chrome errors
cd ${BIZNET_UI_DIR}/${BIZNET_FOLDER_NAME}Setup/webapp/i18n
cp i18n.properties i18n_en.properties
cp i18n.properties i18n_en_US.properties

#Delete Backup files
cd "${BIZNET_UI_DIR}/${BIZNET_FOLDER_NAME}Setup"
find . -name "*.ubak" -type f -delete

cd "${BIZNET_UI_DIR}/${BIZNET_FOLDER_NAME}Setup/webapp/i18n"
find . -name "*.ubak" -type f -delete

exit 0
