package main

/*
Template Version: 1.2-20180921
Template Owner: Ramesh Suraparaju (ramesh.suraparaju@sap.com)
This template is used to generate the chaincode for an asset that implements the hyperledger fabric shim
*/
import (
	"bytes"
	"encoding/json"
	"errors"
	"fmt"
	"net/http"
	"strings"
	"time"

	"github.com/hyperledger/fabric/core/chaincode/shim"
	"github.com/hyperledger/fabric/protos/peer"
)

var logger = shim.NewLogger("CLDChaincode")

//SampleAsset - Chaincode for asset Sample
type SampleAsset struct {
}

//Sample - Details of the asset type Sample
type Sample struct {
	ID           string `json:"ID"`
	ObjectType   string `json:"docType"`
	Status       string `json:"status"`
	CreationDate string `json:"creationDate"`
}

//SampleIDIndex - Index on IDs for retrieval all Samples
type SampleIDIndex struct {
	IDs []string `json:"IDs"`
}

//Success - Success Message
func Success(rc int32, doc string, payload []byte) peer.Response {
	logger.Infof("Success %d = %s", rc, doc, payload)
	return peer.Response{
		Status:  rc,
		Message: doc,
		Payload: payload,
	}
}

//Error - Error Message
func Error(rc int32, doc string) peer.Response {
	logger.Errorf("Error %d = %s", rc, doc)
	return peer.Response{
		Status:  rc,
		Message: doc,
	}
}

func main() {
	err := shim.Start(new(SampleAsset))
	if err != nil {
		fmt.Printf("Error starting SampleAsset chaincode function main(): %s", err)
	} else {
		fmt.Printf("Starting SampleAsset chaincode function main() executed successfully")
	}
}

//Init - The chaincode Init function: No  arguments, only initializes a ID array as Index for retrieval of all Samples
func (smpl *SampleAsset) Init(stub shim.ChaincodeStubInterface) peer.Response {
	var sampleIDIndex SampleIDIndex
	record, _ := stub.GetState("sampleIDIndex")
	//chaincode upgrade: Index might already exist, create new only if it does not
	if record == nil {
		bytes, _ := json.Marshal(sampleIDIndex)
		stub.PutState("sampleIDIndex", bytes)
	}
	return Success(http.StatusOK, "sampleIDIndex initiated successfully", nil)
}

//Invoke - The chaincode Invoke function:
func (smpl *SampleAsset) Invoke(stub shim.ChaincodeStubInterface) peer.Response {
	function, args := stub.GetFunctionAndParameters()
	logger.Debug("function: ", function)
	switch function {
	case "addNewSample":
		return smpl.addNewSample(stub, args)
	case "updateSample":
		return smpl.updateSample(stub, args)
	case "removeSample":
		return smpl.removeSample(stub, args[0])
	case "removeAllSamples":
		return smpl.removeAllSamples(stub)
	case "readSample":
		return smpl.readSample(stub, args[0])
	case "readAllSamples":
		return smpl.readAllSamples(stub)
	case "searchSamplesByID":
		return smpl.searchSamplesByID(stub, args)
	case "getSampleHistory":
		return smpl.getSampleHistory(stub, args)
	default:
		return Error(http.StatusBadRequest, "Received unknown function invocation")
	}
}

//Invoke Route: addNewSample
func (smpl *SampleAsset) addNewSample(stub shim.ChaincodeStubInterface, args []string) peer.Response {
	sample, err := getSampleFromArgs(args)
	if err != nil {
		return Error(http.StatusBadRequest, "Sample Data is Corrupted")
	}
	sample.ObjectType = "Asset.SampleAsset"
	record, err := stub.GetState(sample.ID)
	if record != nil {
		return Error(http.StatusBadRequest, "This Sample already exists: "+sample.ID)
	}
	_, err = smpl.saveSample(stub, sample)
	if err != nil {
		return Error(http.StatusInternalServerError, err.Error())
	}
	_, err = smpl.updateSampleIDIndex(stub, sample)
	if err != nil {
		return Error(http.StatusInternalServerError, err.Error())
	}
	return Success(http.StatusOK, "addNewSample: successfully added: "+sample.ID, nil)
}

//Invoke Route: updateSample
func (smpl *SampleAsset) updateSample(stub shim.ChaincodeStubInterface, args []string) peer.Response {
	input, err := getSampleFromArgs(args)
	if err != nil {
		return Error(http.StatusBadRequest, "Sample Data is Corrupted")
	}
	record, err := stub.GetState(input.ID)
	if record == nil {
		return Error(http.StatusBadRequest, "This Sample does not exist: "+input.ID)
	}
	_, err = smpl.saveSample(stub, input)
	if err != nil {
		return Error(http.StatusInternalServerError, err.Error())
	}
	return Success(http.StatusOK, "updateSample: successfully updated: "+input.ID, nil)
}

//Invoke Route: removeSample
func (smpl *SampleAsset) removeSample(stub shim.ChaincodeStubInterface, sampleID string) peer.Response {
	_, err := smpl.deleteSample(stub, sampleID)
	if err != nil {
		return Error(http.StatusInternalServerError, err.Error())
	}
	_, err = smpl.deleteSampleIDIndex(stub, sampleID)
	if err != nil {
		return Error(http.StatusInternalServerError, err.Error())
	}
	return Success(http.StatusOK, "removeSample: successfully removed: "+sampleID, nil)
}

//Invoke Route: removeAllSamples
func (smpl *SampleAsset) removeAllSamples(stub shim.ChaincodeStubInterface) peer.Response {
	var sampleIDIndex SampleIDIndex
	bytes, err := stub.GetState("sampleIDIndex")
	if err != nil {
		return Error(http.StatusInternalServerError, "removeAllSamples: Error getting sampleIDIndex array")
	}
	err = json.Unmarshal(bytes, &sampleIDIndex)
	if err != nil {
		return Error(http.StatusInternalServerError, "removeAllSamples: Error unmarshalling sampleIDIndex array JSON")
	}
	if len(sampleIDIndex.IDs) == 0 {
		return Error(http.StatusInternalServerError, "removeAllSamples: No samples to remove")
	}
	for _, sampleStructID := range sampleIDIndex.IDs {
		_, err = smpl.deleteSample(stub, sampleStructID)
		if err != nil {
			return Error(http.StatusInternalServerError, "Failed to remove Sample with ID: "+sampleStructID)
		}
		_, err = smpl.deleteSampleIDIndex(stub, sampleStructID)
		if err != nil {
			return Error(http.StatusInternalServerError, err.Error())
		}
	}
	smpl.initHolder(stub)
	return Success(http.StatusOK, "removeAllSamples: Successfully removed all Samples", nil)
}

//Query Route: readSample
func (smpl *SampleAsset) readSample(stub shim.ChaincodeStubInterface, sampleID string) peer.Response {
	sampleAsByteArray, err := smpl.retrieveSample(stub, sampleID)
	if err != nil {
		return Error(http.StatusInternalServerError, err.Error())
	}
	return Success(http.StatusOK, "readSample: Successfully read Sample with ID: "+sampleID, sampleAsByteArray)
}

//Query Route: readAllSamples
func (smpl *SampleAsset) readAllSamples(stub shim.ChaincodeStubInterface) peer.Response {
	var sampleIDs SampleIDIndex
	bytes, err := stub.GetState("sampleIDIndex")
	if err != nil {
		return Error(http.StatusInternalServerError, "readAllSamples: Error getting sampleIDIndex array")
	}
	err = json.Unmarshal(bytes, &sampleIDs)
	if err != nil {
		return Error(http.StatusInternalServerError, "readAllSamples: Error unmarshalling sampleIDIndex array JSON")
	}
	result := "["

	var sampleAsByteArray []byte

	for _, sampleID := range sampleIDs.IDs {
		sampleAsByteArray, err = smpl.retrieveSample(stub, sampleID)
		if err != nil {
			return Error(http.StatusNotFound, "Failed to retrieve sample with ID: "+sampleID)
		}
		result += string(sampleAsByteArray) + ","
	}
	if len(result) == 1 {
		result = "[]"
	} else {
		result = result[:len(result)-1] + "]"
	}
	return Success(http.StatusOK, "OK", []byte(result))
}

//GetSampleHistory - reads history of a sample with specified ID
func (smpl *SampleAsset) getSampleHistory(stub shim.ChaincodeStubInterface, args []string) peer.Response {

	resultsIterator, err := stub.GetHistoryForKey(args[0])
	if err != nil {
		return Error(http.StatusNotFound, "Not Found")
	}
	defer resultsIterator.Close()
	// Write return buffer
	var buffer bytes.Buffer
	buffer.WriteString("{ \"values\": [")
	for resultsIterator.HasNext() {
		it, _ := resultsIterator.Next()
		if buffer.Len() > 15 {
			buffer.WriteString(",")
		}
		buffer.WriteString("{\"timestamp\":\"")
		buffer.WriteString(time.Unix(it.Timestamp.Seconds, int64(it.Timestamp.Nanos)).Format(time.Stamp))
		buffer.WriteString("\",\"sample\": ")
		buffer.WriteString(string(it.Value))
		buffer.WriteString("}")
	}
	buffer.WriteString("]}")
	return Success(http.StatusOK, "OK", buffer.Bytes())
}

// Search for all matching IDs, given a (regex) value expression and return both the IDs and text.
// For example: '^H.llo' will match any string starting with 'Hello' or 'Hallo'.
func (smpl *SampleAsset) searchSamplesByID(stub shim.ChaincodeStubInterface, args []string) peer.Response {

	searchString := strings.Replace(args[0], "\"", ".", -1) // protect against SQL injection

	// stub.GetQueryResult takes a verbatim CouchDB (assuming this is used DB). See CouchDB documentation:
	//     http://docs.couchdb.org/en/2.0.0/api/database/find.html
	// For example:
	//	{
	//		"selector": {
	//			"value": {"$regex": %s"}
	//		},
	//		"fields": ["ID","value"],
	//		"limit":  99
	//	}
	queryString := fmt.Sprintf("{\"selector\": {\"ID\": {\"$regex\": \"%s\"}}, \"fields\": [\"ID\", \"docType\", \"status\", \"creationDate\"], \"limit\":99, \"execution_stats\": true}", strings.Replace(searchString, "\"", ".", -1)) //<END_OF_QRY_STRING>
	resultsIterator, err := stub.GetQueryResult(queryString)
	if err != nil {
		return Error(http.StatusInternalServerError, err.Error())
	}
	defer resultsIterator.Close()

	// Write return buffer
	var buffer bytes.Buffer
	buffer.WriteString("{ \"values\": [")
	for resultsIterator.HasNext() {
		it, _ := resultsIterator.Next()
		if buffer.Len() > 15 {
			buffer.WriteString(",")
		}
		buffer.WriteString("{\"ID\":\"")
		buffer.WriteString(it.Key)
		buffer.WriteString("\",\"sample\": ")
		buffer.WriteString(string(it.Value))
		buffer.WriteString("}")
	}
	buffer.WriteString("]}")
	return Success(http.StatusOK, "OK", buffer.Bytes())
}

//Helper: Save SampleAsset
func (smpl *SampleAsset) saveSample(stub shim.ChaincodeStubInterface, sample Sample) (bool, error) {
	bytes, err := json.Marshal(sample)
	if err != nil {
		return false, errors.New("Error converting sample record JSON")
	}
	err = stub.PutState(sample.ID, bytes)
	if err != nil {
		return false, errors.New("Error storing Sample record")
	}
	return true, nil
}

//Helper: delete SampleAsset
func (smpl *SampleAsset) deleteSample(stub shim.ChaincodeStubInterface, sampleID string) (bool, error) {
	_, err := smpl.retrieveSample(stub, sampleID)
	if err != nil {
		return false, errors.New("Sample with ID: " + sampleID + " not found")
	}
	err = stub.DelState(sampleID)
	if err != nil {
		return false, errors.New("Error deleting Sample record")
	}
	return true, nil
}

//Helper: Update sample Holder - updates Index
func (smpl *SampleAsset) updateSampleIDIndex(stub shim.ChaincodeStubInterface, sample Sample) (bool, error) {
	var sampleIDs SampleIDIndex
	bytes, err := stub.GetState("sampleIDIndex")
	if err != nil {
		return false, errors.New("updateSampleIDIndex: Error getting sampleIDIndex array Index from state")
	}
	err = json.Unmarshal(bytes, &sampleIDs)
	if err != nil {
		return false, errors.New("updateSampleIDIndex: Error unmarshalling sampleIDIndex array JSON")
	}
	sampleIDs.IDs = append(sampleIDs.IDs, sample.ID)
	bytes, err = json.Marshal(sampleIDs)
	if err != nil {
		return false, errors.New("updateSampleIDIndex: Error marshalling new sample ID")
	}
	err = stub.PutState("sampleIDIndex", bytes)
	if err != nil {
		return false, errors.New("updateSampleIDIndex: Error storing new sample ID in sampleIDIndex (Index)")
	}
	return true, nil
}

//Helper: delete ID from sampleStruct Holder
func (smpl *SampleAsset) deleteSampleIDIndex(stub shim.ChaincodeStubInterface, sampleID string) (bool, error) {
	var sampleIDIndex SampleIDIndex
	bytes, err := stub.GetState("sampleIDIndex")
	if err != nil {
		return false, errors.New("deleteSampleIDIndex: Error getting sampleIDIndex array Index from state")
	}
	err = json.Unmarshal(bytes, &sampleIDIndex)
	if err != nil {
		return false, errors.New("deleteSampleIDIndex: Error unmarshalling sampleIDIndex array JSON")
	}
	sampleIDIndex.IDs, err = deleteKeyFromStringArray(sampleIDIndex.IDs, sampleID)
	if err != nil {
		return false, errors.New(err.Error())
	}
	bytes, err = json.Marshal(sampleIDIndex)
	if err != nil {
		return false, errors.New("deleteSampleIDIndex: Error marshalling new sampleStruct ID")
	}
	err = stub.PutState("sampleIDIndex", bytes)
	if err != nil {
		return false, errors.New("deleteSampleIDIndex: Error storing new sampleStruct ID in sampleIDIndex (Index)")
	}
	return true, nil
}

//Helper: Initialize truck ID Holder //change template
func (smpl *SampleAsset) initHolder(stub shim.ChaincodeStubInterface) bool {
	var sampleIDIndex SampleIDIndex
	bytes, _ := json.Marshal(sampleIDIndex)
	stub.DelState("sampleIDIndex")
	stub.PutState("sampleIDIndex", bytes)
	return true
}

//deleteKeyFromArray
func deleteKeyFromStringArray(array []string, key string) (newArray []string, err error) {
	for _, entry := range array {
		if entry != key {
			newArray = append(newArray, entry)
		}
	}
	if len(newArray) == len(array) {
		return newArray, errors.New("Specified Key: " + key + " not found in Array")
	}
	return newArray, nil
}

//Helper: Retrieve
func (smpl *SampleAsset) retrieveSample(stub shim.ChaincodeStubInterface, sampleID string) ([]byte, error) {
	var sample Sample
	var sampleAsByteArray []byte
	bytes, err := stub.GetState(sampleID)
	if err != nil {
		return sampleAsByteArray, errors.New("retrieveSample: Error retrieving sample with ID: " + sampleID)
	}
	err = json.Unmarshal(bytes, &sample)
	if err != nil {
		return sampleAsByteArray, errors.New("retrieveSample: Corrupt sample record " + string(bytes))
	}
	sampleAsByteArray, err = json.Marshal(sample)
	if err != nil {
		return sampleAsByteArray, errors.New("readSample: Invalid sample Object - Not a  valid JSON")
	}
	return sampleAsByteArray, nil
}

//getSampleFromArgs - construct a sample structure from string array of arguments
func getSampleFromArgs(args []string) (sample Sample, err error) {
	if !Valid(args[0]) {
		return sample, errors.New("Invalid json")
	}

	err = Unmarshal([]byte(args[0]), &sample)
	if err != nil {
		return sample, err
	}
	return sample, nil
}
