#!/bin/bash

#Colors
DARK_GRAY='\033[1;30m'
RED='\033[0;31m'
GREEN='\033[0;32m'
ORANGE='\033[0;33m'
BLUE='\033[0;34m'
PURPLE='\033[0;35m'
CYAN='\033[0;36m'
LIGHT_GRAY='\033[0;36m'
LIGHT_RED='\033[1;31m'
LIGHT_GREEN='\033[1;32m'
YELLOW='\033[1;33m'
LIGHT_BLUE='\033[1;34m'
LIGHT_PURPLE='\033[1;35m'
LIGHT_CYAN='\033[1;36m'
WHITE='\033[1;37m'
NC='\033[0m'

#Capture file and folder paths
TARGET_DIR=${1}
TARGET_CHAINCODE=${2}
TARGET_CHAINCODE_TEST=${3}
TARGET_API_DEFINITION=${4}
BIZNET_TEST_DIR=${5}
ASSET_NAME=${6}
ASSET_NAME_LOWER=${7}
ENTITY_TYPE=${8}
TEMPLATE_TEST_DIR=${9}
TEMPLATE_UI_DIR=${10}
BIZNET_UI_DIR=${11}
BIZNET_FOLDER_NAME=${12}
TEMPLATE_ROOT_DIR=${13}
ENTITY_ATTRIBUTES=${14}

TEMPLATE_GO_TYPE="type Sample struct {\n ID          string \`json:\"ID\"\`\n ObjectType   string \`json:\"docType\"\`\n  Status       string \`json:\"status\"\`\n  CreationDate string \`json:\"creationDate\"\`"
TEMPLATE_GO_TEST_1="func getFirst${ASSET_NAME}${ENTITY_TYPE}ForTesting() [][]byte { \n return [][]byte{[]byte(\"addNewSample\"), \n []byte(\"{\\\"ID\\\":\\\"100001\\\",\\\"docType\\\":\\\"Asset.${ASSET_NAME}${ENTITY_TYPE}\\\",\\\"status\\\":\\\"0\\\",\\\"creationDate\\\":\\\"12/01/2018\\\""
TEMPLATE_GO_TEST_1U="func getFirst${ASSET_NAME}${ENTITY_TYPE}ForUpdateTestingOK() [][]byte { \n return [][]byte{[]byte(\"updateSample\"), \n []byte(\"{\\\"ID\\\":\\\"100001\\\",\\\"docType\\\":\\\"Asset.${ASSET_NAME}${ENTITY_TYPE}\\\",\\\"status\\\":\\\"1\\\",\\\"creationDate\\\":\\\"12/01/2018\\\""
TEMPLATE_GO_TEST_2="func getSecond${ASSET_NAME}${ENTITY_TYPE}ForTesting() [][]byte { \n return [][]byte{[]byte(\"addNewSample\"), \n []byte(\"{\\\"ID\\\":\\\"100002\\\",\\\"docType\\\":\\\"Asset.${ASSET_NAME}${ENTITY_TYPE}\\\",\\\"status\\\":\\\"0\\\",\\\"creationDate\\\":\\\"12/01/2018\\\""
TEMPLATE_GO_TEST_STRUCT_1="	sample.ID = \"100001\"\n	sample.ObjectType = \"Asset.${ASSET_NAME}${ENTITY_TYPE}\"\n  sample.Status = \"0\"\n	sample.CreationDate = \"12/01/2018\""
TEMPLATE_GO_TEST_STRUCT_1U="func getUpdated${ASSET_NAME}Expected() []byte {\n\tvar sample Sample\n\tsample.ID = \"100001\"\n	sample.ObjectType = \"Asset.${ASSET_NAME}${ENTITY_TYPE}\"\n  sample.Status = \"1\"\n	sample.CreationDate = \"12/01/2018\""
TEMPLATE_GO_TEST_STRUCT_2="	sample.ID = \"100002\"\n	sample.ObjectType = \"Asset.${ASSET_NAME}${ENTITY_TYPE}\"\n  sample.Status = \"0\"\n	sample.CreationDate = \"12/01/2018\""
TEMPLATE_GO_QRY_STRING_START="queryString := fmt.Sprintf(\"{\\\\\"selector\\\\\": {\\\\\"ID\\\\\": {\\\\\"\$regex\\\\\": \\\\\"%s\\\\\"}}, \\\\\"fields\\\\\": [\\\\\"ID\\\\\", \\\\\"docType\\\\\", \\\\\"status\\\\\", \\\\\"creationDate\\\\\""
TEMPLATE_GO_QRY_STRING_END="], \\\\\"limit\\\\\":99, \\\\\"execution_stats\\\\\": true}\", strings.Replace(searchString, \"\\\\\"\", \".\", -1))"
TEMPLATE_JSON_SAMPLE="[\n\t{\n\t\t\"ID\":\"${ASSET_NAME}${ENTITY_TYPE}1001\",\n\t\t\"docType\":\"Asset.${ASSET_NAME}${ENTITY_TYPE}\",\n\t\t\"status\":\"Created\",\n\t\t\"creationDate\":\"12/28/2107\""
TEMPLATE_API_DEFINITION="definitions:\n  ${ASSET_NAME_LOWER}${ENTITY_TYPE}:\n    type: object\n    properties:\n      ID:\n        type: string\n      docType:\n        type: string\n      status:\n        type: string\n      creationDate:\n        type: string"
TEMPLATE_ASSET_ROUTES=", {
\t\t\t\t\"pattern\": \"${ASSET_NAME}${ENTITY_TYPE}s\",
\t\t\t\t\"name\": \"${ASSET_NAME_LOWER}${ENTITY_TYPE}s\",
\t\t\t\t\"target\": [\"t${ASSET_NAME}${ENTITY_TYPE}sRoot\", \"t${ASSET_NAME}${ENTITY_TYPE}Details\", \"t${ASSET_NAME}${ENTITY_TYPE}sOverview\"]
\t\t\t}, {
\t\t\t\t\"pattern\": \"${ASSET_NAME}${ENTITY_TYPE}s/{${ASSET_NAME_LOWER}${ENTITY_TYPE}Id}\",
\t\t\t\t\"name\": \"${ASSET_NAME_LOWER}${ENTITY_TYPE}\",\n
\t\t\t\t\"target\": [\"t${ASSET_NAME}${ENTITY_TYPE}sOverview\", \"t${ASSET_NAME}${ENTITY_TYPE}Details\"]
\t\t\t}, {
\t\t\t\t\"pattern\": \"${ASSET_NAME}${ENTITY_TYPE}All\",
\t\t\t\t\"name\": \"${ASSET_NAME_LOWER}${ENTITY_TYPE}s\",
\t\t\t\t\"target\": [\"t${ASSET_NAME}${ENTITY_TYPE}Details\", \"t${ASSET_NAME}${ENTITY_TYPE}sOverview\"]
\t\t\t}"
TEMPLATE_ASSET_TARGETS=",
\t\t\t\t\"t${ASSET_NAME}${ENTITY_TYPE}sRoot\": {
\t\t\t\t\t\"viewName\": \"${ASSET_NAME}${ENTITY_TYPE}sRoot\",
\t\t\t\t\t\"viewLevel\": 1,
\t\t\t\t\t\"controlId\": \"idAppControl\",
\t\t\t\t\t\"controlAggregation\": \"pages\",
\t\t\t\t\t\"title\": \"${ASSET_NAME}${ENTITY_TYPE}s\"
\t\t\t\t},
\t\t\t\t\"t${ASSET_NAME}${ENTITY_TYPE}sOverview\": {
\t\t\t\t\t\"parent\": \"t${ASSET_NAME}${ENTITY_TYPE}sRoot\",
\t\t\t\t\t\"viewName\": \"${ASSET_NAME}${ENTITY_TYPE}sOverview\",
\t\t\t\t\t\"viewLevel\": 2,
\t\t\t\t\t\"controlId\": \"idAppControl\",
\t\t\t\t\t\"controlAggregation\": \"masterPages\",
\t\t\t\t\t\"title\": \"${ASSET_NAME}${ENTITY_TYPE}sOverview\"
\t\t\t\t},
\t\t\t\t\"t${ASSET_NAME}${ENTITY_TYPE}Details\": {
\t\t\t\t\t\"parent\": \"t${ASSET_NAME}${ENTITY_TYPE}sRoot\",
\t\t\t\t\t\"viewName\": \"${ASSET_NAME}${ENTITY_TYPE}Details\",
\t\t\t\t\t\"viewLevel\": 3,
\t\t\t\t\t\"controlId\": \"idAppControl\",
\t\t\t\t\t\"controlAggregation\": \"detailPages\",
\t\t\t\t\t\"title\": \"${ASSET_NAME}${ENTITY_TYPE}Details\"
\t\t\t\t}"
TEMPLATE_ASSET_DETAILS_VIEW_SEL="
                                      <sap.ui.layout.form:FormElement id=\"__elementSel${ASSET_NAME}${ENTITY_TYPE}ID\" label=\"{i18n>_label${ASSET_NAME}${ENTITY_TYPE}ID}\">
                                        <sap.ui.layout.form:fields>
                                          <Input width=\"100%%\" id=\"__inputSel${ASSET_NAME}${ENTITY_TYPE}ID\" value=\"{${ASSET_NAME}${ENTITY_TYPE}s>/selected${ASSET_NAME}${ENTITY_TYPE}/ID}\" editable=\"false\"/>
                                        </sap.ui.layout.form:fields>
                                      </sap.ui.layout.form:FormElement>
                                      <sap.ui.layout.form:FormElement id=\"__elementSel${ASSET_NAME}${ENTITY_TYPE}ObjectType\" label=\"{i18n>_label${ASSET_NAME}${ENTITY_TYPE}ObjectType}\">
                                        <sap.ui.layout.form:fields>
                                          <Input width=\"100%%\" id=\"__inputSel${ASSET_NAME}${ENTITY_TYPE}ObjectType\" value=\"{${ASSET_NAME}${ENTITY_TYPE}s>/selected${ASSET_NAME}${ENTITY_TYPE}/ObjectType}\" editable=\"false\"/>
                                        </sap.ui.layout.form:fields>
                                      </sap.ui.layout.form:FormElement>
                                      <sap.ui.layout.form:FormElement id=\"__elementSel${ASSET_NAME}${ENTITY_TYPE}Status\" label=\"{i18n>_label${ASSET_NAME}${ENTITY_TYPE}Status}\">
                                        <sap.ui.layout.form:fields>
                                          <Input width=\"100%%\" id=\"__inputSel${ASSET_NAME}${ENTITY_TYPE}Status\" value=\"{${ASSET_NAME}${ENTITY_TYPE}s>/selected${ASSET_NAME}${ENTITY_TYPE}/Status}\" editable=\"false\"/>
                                        </sap.ui.layout.form:fields>
                                      </sap.ui.layout.form:FormElement>
                                      <sap.ui.layout.form:FormElement id=\"__elementSel${ASSET_NAME}${ENTITY_TYPE}CreationDate\" label=\"{i18n>_label${ASSET_NAME}${ENTITY_TYPE}CreationDate}\">
                                        <sap.ui.layout.form:fields>
                                          <Input width=\"100%%\" id=\"__inputSel${ASSET_NAME}${ENTITY_TYPE}CreationDate\" value=\"{${ASSET_NAME}${ENTITY_TYPE}s>/selected${ASSET_NAME}${ENTITY_TYPE}/CreationDate}\" editable=\"false\"/>
                                        </sap.ui.layout.form:fields>
                                      </sap.ui.layout.form:FormElement>
"

TEMPLATE_ASSET_DETAILS_VIEW_NEW="
                                      <sap.ui.layout.form:FormElement id=\"__elementNew${ASSET_NAME}${ENTITY_TYPE}ID\" label=\"{i18n>_label${ASSET_NAME}${ENTITY_TYPE}ID}\">
                                        <sap.ui.layout.form:fields>
                                          <Input width=\"100%%\" id=\"__inputNew${ASSET_NAME}${ENTITY_TYPE}ID\" value=\"{${ASSET_NAME}${ENTITY_TYPE}s>/new${ASSET_NAME}${ENTITY_TYPE}/ID}\" editable=\"true\" placeholder=\"{i18n>_labelIDPlaceholder}\"/>
                                        </sap.ui.layout.form:fields>
                                      </sap.ui.layout.form:FormElement>
                                      <sap.ui.layout.form:FormElement id=\"__elementNew${ASSET_NAME}${ENTITY_TYPE}Status\" label=\"{i18n>_label${ASSET_NAME}${ENTITY_TYPE}Status}\">
                                        <sap.ui.layout.form:fields>
                                          <Input width=\"100%%\" id=\"__inputNew${ASSET_NAME}${ENTITY_TYPE}Status\" value=\"{${ASSET_NAME}${ENTITY_TYPE}s>/new${ASSET_NAME}${ENTITY_TYPE}/Status}\" editable=\"true\"/>
                                        </sap.ui.layout.form:fields>
                                      </sap.ui.layout.form:FormElement>
                                      <sap.ui.layout.form:FormElement id=\"__elementNew${ASSET_NAME}${ENTITY_TYPE}CreationDate\" label=\"{i18n>_label${ASSET_NAME}${ENTITY_TYPE}CreationDate}\">
                                        <sap.ui.layout.form:fields>
                                          <Input width=\"100%%\" id=\"__inputNew${ASSET_NAME}${ENTITY_TYPE}CreationDate\" value=\"{${ASSET_NAME}${ENTITY_TYPE}s>/new${ASSET_NAME}${ENTITY_TYPE}/CreationDate}\" editable=\"true\"/>
                                        </sap.ui.layout.form:fields>
                                      </sap.ui.layout.form:FormElement>
"

TEMPLATE_ENTITY_TO_MODEL="
                Status:responseData.status,
                CreationDate:responseData.creationDate
"
TEMPLATE_ENTITY_TO_CHAINCODE_NEW="
                        \"status\":oModel.getProperty(\"/new${ASSET_NAME}${ENTITY_TYPE}/Status\"),
                        \"creationDate\":oModel.getProperty(\"/new${ASSET_NAME}${ENTITY_TYPE}/CreationDate\")
"
TEMPLATE_ENTITY_TO_CHAINCODE_SEL="
                        \"status\":oModel.getProperty(\"/selected${ASSET_NAME}${ENTITY_TYPE}/Status\"),
                        \"creationDate\":oModel.getProperty(\"/selected${ASSET_NAME}${ENTITY_TYPE}/CreationDate\")
"

TEMPLATE_ASSET_I18N="
#XTIT:
_title${ASSET_NAME}${ENTITY_TYPE}sList=${ASSET_NAME}s List

#XTIT:
_title${ASSET_NAME}${ENTITY_TYPE}Details=Details of ${ASSET_NAME}

#XTIT:
_objectHeaderTitle${ASSET_NAME}${ENTITY_TYPE}=${ASSET_NAME}

#XFLD:
_radioButton${ASSET_NAME}${ENTITY_TYPE}s=${ASSET_NAME}${ENTITY_TYPE}s

#XFLD:
_label${ASSET_NAME}${ENTITY_TYPE}Details=${ASSET_NAME} Details

#XFLD:
_labelNo${ASSET_NAME}${ENTITY_TYPE}sText=No ${ASSET_NAME}${ENTITY_TYPE}s

#XFLD:
_label${ASSET_NAME}${ENTITY_TYPE}Add=New

#XBUT:
_buttonAdd${ASSET_NAME}${ENTITY_TYPE}=Add

#XFLD:
_labelNo${ASSET_NAME}${ENTITY_TYPE}s=No ${ASSET_NAME}s

#XFLD:
_label${ASSET_NAME}${ENTITY_TYPE}ID=ID

#XFLD:
_label${ASSET_NAME}${ENTITY_TYPE}ObjectType=ObjectType

#XFLD:
_label${ASSET_NAME}${ENTITY_TYPE}Status=Status

#XFLD:
_label${ASSET_NAME}${ENTITY_TYPE}CreationDate=Creation Date
"

TEMPLATE_ENTITY_POSTMAN="{\\\\n\\\\t\\\\\"ID\\\\\":\\\\\"{{ID}}\\\\\",\\\\n\\\\t\\\\\"docType\\\\\":\\\\\"{{docType}}\\\\\",\\\\n\\\\t\\\\\"status\\\\\":\\\\\"{{status}}\\\\\",\\\\n\\\\t\\\\\"creationDate\\\\\":\\\\\"{{creationDate}}\\\\\""

#Prepare .go and _test.go files
#Type declaration
cd ${TARGET_DIR}
printf "${TEMPLATE_GO_TYPE}" >> assetStruct.sbak

#First sample for unit test
shopt -s xpg_echo
echo "${TEMPLATE_GO_TEST_1}\c" >> assetFirstTest.sbak

#Update sample for unit test
shopt -s xpg_echo
echo "${TEMPLATE_GO_TEST_1U}\c" >> assetFirstTestUpdateOK.sbak

#Second sample for unit test
shopt -s xpg_echo
echo "${TEMPLATE_GO_TEST_2}\c" >> assetSecondTest.sbak

#First sample unit test - expected result
printf "${TEMPLATE_GO_TEST_STRUCT_1}" >> assetFirstTestStruct.sbak

#Update sample unit test - expected result
printf "${TEMPLATE_GO_TEST_STRUCT_1U}" >> assetFirstTestUpdateOKStruct.sbak

#Second sample unit test - expected result
printf "${TEMPLATE_GO_TEST_STRUCT_2}" >> assetSecondTestStruct.sbak

#Wildcard search queryString
shopt -s xpg_echo
echo "${TEMPLATE_GO_QRY_STRING_START}\c" >> assetQueryString.sbak

#API Definition
shopt -s xpg_echo
echo "${TEMPLATE_API_DEFINITION}\c" >> entityAPIDefinition.sbak

#JSON sample initialization
printf "${TEMPLATE_JSON_SAMPLE}" >> ${BIZNET_TEST_DIR}/newman/CleanAllAndSetup/data/${ASSET_NAME}${ENTITY_TYPE}s.json

#Prepare postman collection
printf "${TEMPLATE_ENTITY_POSTMAN}" >> ${BIZNET_TEST_DIR}/newman/CleanAllAndSetup/entityCreateAllPostmanCollection.pbak

#Prepare routes/targets for added entity
printf "${TEMPLATE_ASSET_ROUTES}" >> ${BIZNET_UI_DIR}/${BIZNET_FOLDER_NAME}Setup/webapp/entityRoutes.ubak
printf "${TEMPLATE_ASSET_TARGETS}" >> ${BIZNET_UI_DIR}/${BIZNET_FOLDER_NAME}Setup/webapp/entityTargets.ubak

#Prepare details view for the added entity
printf "${TEMPLATE_ASSET_DETAILS_VIEW_SEL}" >> ${BIZNET_UI_DIR}/${BIZNET_FOLDER_NAME}Setup/webapp/view/entityDetailsViewSelected.ubak
printf "${TEMPLATE_ASSET_DETAILS_VIEW_NEW}" >> ${BIZNET_UI_DIR}/${BIZNET_FOLDER_NAME}Setup/webapp/view/entityDetailsViewNew.ubak

#Prepare replacements for formatter
printf "${TEMPLATE_ENTITY_TO_MODEL}" >> ${BIZNET_UI_DIR}/${BIZNET_FOLDER_NAME}Setup/webapp/util/replaceEntityToModel.ubak
printf "${TEMPLATE_ENTITY_TO_CHAINCODE_NEW}" >> ${BIZNET_UI_DIR}/${BIZNET_FOLDER_NAME}Setup/webapp/util/replaceEntityToChaincodeNew.ubak
printf "${TEMPLATE_ENTITY_TO_CHAINCODE_SEL}" >> ${BIZNET_UI_DIR}/${BIZNET_FOLDER_NAME}Setup/webapp/util/replaceEntityToChaincodeSelected.ubak

#Prepare i18n for added entity
printf "${TEMPLATE_ASSET_I18N}" >> ${BIZNET_UI_DIR}/${BIZNET_FOLDER_NAME}Setup/webapp/i18n/entityi18nProperties.ubak

printf "" >> ${BIZNET_UI_DIR}/${BIZNET_FOLDER_NAME}Setup/webapp/model/replaceColNames.ubak
printf "" >> ${BIZNET_UI_DIR}/${BIZNET_FOLDER_NAME}Setup/webapp/model/replaceModelAttributes.ubak

#Enhance Setup UI for the added entity
sh ${TEMPLATE_ROOT_DIR}/enhanceEntitySetupUI.sh \
  "${TEMPLATE_UI_DIR}"\
  "${BIZNET_UI_DIR}"\
  "${BIZNET_FOLDER_NAME}"\
  "${ASSET_NAME}"\
  "${ASSET_NAME_LOWER}"\
  "${ENTITY_TYPE}"

for i in $(jq '. | keys | .[]' <<< "${ENTITY_ATTRIBUTES}"); do
  ATTRIBUTE_NAME=$(jq -r ".[$i].name" <<< "${ENTITY_ATTRIBUTES}")
  if [ "$ATTRIBUTE_NAME" = "ID" ] || [ "$ATTRIBUTE_NAME" = "ObjectType" ] || [ "$ATTRIBUTE_NAME" = "Status" ] || [ "$ATTRIBUTE_NAME" = "CreationDate" ]; then
    continue
  fi
  ATTRIBUTE_JSON_NAME=$(echo ${ATTRIBUTE_NAME:0:1} | tr '[A-Z]' '[a-z]')${ATTRIBUTE_NAME:1}
# Add structure attribute in the chaincode
  printf "\n${ATTRIBUTE_NAME} string \`json:\"${ATTRIBUTE_JSON_NAME}\"\`" >> assetStruct.sbak
# Add test data for new attribute for tests - (1/2)
  shopt -s xpg_echo
  echo ", \\\"${ATTRIBUTE_JSON_NAME}\\\":\\\"${ATTRIBUTE_JSON_NAME}001\\\"\c" >> assetFirstTest.sbak
  shopt -s xpg_echo
  echo ", \\\"${ATTRIBUTE_JSON_NAME}\\\":\\\"${ATTRIBUTE_JSON_NAME}001\\\"\c" >> assetFirstTestUpdateOK.sbak
  printf "\nsample.${ATTRIBUTE_NAME}=\"${ATTRIBUTE_JSON_NAME}001\"" >> assetFirstTestStruct.sbak
  printf "\nsample.${ATTRIBUTE_NAME}=\"${ATTRIBUTE_JSON_NAME}001\"" >> assetFirstTestUpdateOKStruct.sbak
# Add test data for new attribute for tests - (2/2)
  shopt -s xpg_echo
  echo ", \\\"${ATTRIBUTE_JSON_NAME}\\\":\\\"${ATTRIBUTE_JSON_NAME}002\\\"\c" >> assetSecondTest.sbak
  printf "\nsample.${ATTRIBUTE_NAME}=\"${ATTRIBUTE_JSON_NAME}002\"" >> assetSecondTestStruct.sbak
# Add attribute to the search function resultsIterator
  shopt -s xpg_echo
  echo ", \\\\\"${ATTRIBUTE_JSON_NAME}\\\\\":\\\\\"${ATTRIBUTE_JSON_NAME}001\\\\\"\c" >> assetQueryString.sbak
# Add attribute to openAPI definition
  printf "\n      ${ATTRIBUTE_JSON_NAME}:\n        type: string" >> entityAPIDefinition.sbak
# Add data sample in JSON data file
  printf ",\n\t\t\"${ATTRIBUTE_JSON_NAME}\":\"${ATTRIBUTE_JSON_NAME}001\"" >> ${BIZNET_TEST_DIR}/newman/CleanAllAndSetup/data/${ASSET_NAME}${ENTITY_TYPE}s.json
# Add attribute to postman collection
  printf ",\\\\n\\\\t\\\\\"${ATTRIBUTE_JSON_NAME}\\\\\":\\\\\"{{${ATTRIBUTE_JSON_NAME}}}\\\\\"" >> ${BIZNET_TEST_DIR}/newman/CleanAllAndSetup/entityCreateAllPostmanCollection.pbak
# Add attribute in UI formatter
  printf ",\n                ${ATTRIBUTE_NAME}:responseData.${ATTRIBUTE_JSON_NAME}" >> ${BIZNET_UI_DIR}/${BIZNET_FOLDER_NAME}Setup/webapp/util/replaceEntityToModel.ubak
  printf ",\n                  \"${ATTRIBUTE_JSON_NAME}\":oModel.getProperty(\"/new${ASSET_NAME}${ENTITY_TYPE}/${ATTRIBUTE_NAME}\")" >> ${BIZNET_UI_DIR}/${BIZNET_FOLDER_NAME}Setup/webapp/util/replaceEntityToChaincodeNew.ubak
  printf ",\n                  \"${ATTRIBUTE_JSON_NAME}\":oModel.getProperty(\"/selected${ASSET_NAME}${ENTITY_TYPE}/${ATTRIBUTE_NAME}\")" >> ${BIZNET_UI_DIR}/${BIZNET_FOLDER_NAME}Setup/webapp/util/replaceEntityToChaincodeSelected.ubak
# Add attribute in UI model
  printf ",\n            {name:\"${ATTRIBUTE_NAME}\"}" >> ${BIZNET_UI_DIR}/${BIZNET_FOLDER_NAME}Setup/webapp/model/replaceColNames.ubak
  printf ",\n          ${ATTRIBUTE_NAME}:\"\"" >> ${BIZNET_UI_DIR}/${BIZNET_FOLDER_NAME}Setup/webapp/model/replaceModelAttributes.ubak
# Add attribute in Views
  printf "
                                        <sap.ui.layout.form:FormElement id=\"__elementSel${ASSET_NAME}${ENTITY_TYPE}${ATTRIBUTE_NAME}\" label=\"{i18n>_label${ASSET_NAME}${ENTITY_TYPE}${ATTRIBUTE_NAME}}\">
  	                                      <sap.ui.layout.form:fields>
  	                                        <Input width=\"100%%\" id=\"__inputSel${ASSET_NAME}${ENTITY_TYPE}${ATTRIBUTE_NAME}\" value=\"{${ASSET_NAME}${ENTITY_TYPE}s>/selected${ASSET_NAME}${ENTITY_TYPE}/${ATTRIBUTE_NAME}}\" editable=\"false\"/>
  	                                      </sap.ui.layout.form:fields>
  	                                    </sap.ui.layout.form:FormElement>" >> ${BIZNET_UI_DIR}/${BIZNET_FOLDER_NAME}Setup/webapp/view/entityDetailsViewSelected.ubak
  printf "
                                        <sap.ui.layout.form:FormElement id=\"__elementNew${ASSET_NAME}${ENTITY_TYPE}${ATTRIBUTE_NAME}\" label=\"{i18n>_label${ASSET_NAME}${ENTITY_TYPE}${ATTRIBUTE_NAME}}\">
                                          <sap.ui.layout.form:fields>
                                            <Input width=\"100%%\" id=\"__inputNew${ASSET_NAME}${ENTITY_TYPE}${ATTRIBUTE_NAME}\" value=\"{${ASSET_NAME}${ENTITY_TYPE}s>/new${ASSET_NAME}${ENTITY_TYPE}/${ATTRIBUTE_NAME}}\" editable=\"true\"/>
                                          </sap.ui.layout.form:fields>
                                        </sap.ui.layout.form:FormElement>" >> ${BIZNET_UI_DIR}/${BIZNET_FOLDER_NAME}Setup/webapp/view/entityDetailsViewNew.ubak
# Add i18n
  printf "
#XFLD:
_label${ASSET_NAME}${ENTITY_TYPE}${ATTRIBUTE_NAME}=${ATTRIBUTE_NAME}" >> ${BIZNET_UI_DIR}/${BIZNET_FOLDER_NAME}Setup/webapp/i18n/entityi18nProperties.ubak
done

printf "\n\n  sampleAssetHistory:" >> entityAPIDefinition.sbak
printf "\n\t}\n]" >> ${BIZNET_TEST_DIR}/newman/CleanAllAndSetup/data/${ASSET_NAME}${ENTITY_TYPE}s.json
printf "\\\\n}" >> ${BIZNET_TEST_DIR}/newman/CleanAllAndSetup/entityCreateAllPostmanCollection.pbak
printf "\n}\n" >> assetStruct.sbak
printf "}\")}\n}" >> assetFirstTest.sbak
printf "}\")}\n}" >> assetFirstTestUpdateOK.sbak
printf "}\")}\n}" >> assetSecondTest.sbak
printf "${TEMPLATE_GO_QRY_STRING_END}" >> assetQueryString.sbak

#Replace new structure to chaincode file
perl -i -p0e 's/type Sample struct.*?}\n/`cat assetStruct.sbak`/se' ${TARGET_CHAINCODE}
#Replace new structure in the queryResults structure
perl -i -p0e 's/queryString :=.*?<END_OF_QRY_STRING>/`cat assetQueryString.sbak`/se' ${TARGET_CHAINCODE}
#Replace the sample JSON test data
perl -i -p0e 's/func getFirstSampleAssetForTesting.*?}\n}/`cat assetFirstTest.sbak`/se' ${TARGET_CHAINCODE_TEST}
perl -i -p0e 's/func getFirstSampleAssetForUpdateTestingOK.*?}\n}/`cat assetFirstTestUpdateOK.sbak`/se' ${TARGET_CHAINCODE_TEST}
#Replace the sample GO structures for expected test result values
perl -i -p0e 's/func getSecondSampleAssetForTesting.*?}\n}/`cat assetSecondTest.sbak`/se' ${TARGET_CHAINCODE_TEST}
#Replace updated JSON test data to chaincode test file
perl -i -p0e 's/sample.ID = \"100001\".*?2018\"/`cat assetFirstTestStruct.sbak`/seg' ${TARGET_CHAINCODE_TEST}
#Replace updated sample GO structures for expected test results in the chaincode test file
perl -i -p0e 's/sample.ID = \"100002\".*?2018\"/`cat assetSecondTestStruct.sbak`/seg' ${TARGET_CHAINCODE_TEST}
#Replace updated sample GO structure for expected test results after update
perl -i -p0e 's/func getUpdatedSampleExpected.*?2018\"/`cat assetFirstTestUpdateOKStruct.sbak`/se' ${TARGET_CHAINCODE_TEST}
#Replace updated entity definition in the openAPI.yaml file
perl -i -p0e 's/definitions:.*?sampleAssetHistory:/`cat entityAPIDefinition.sbak`/se' ${TARGET_API_DEFINITION}

#Copy the test JSON to UI localStore
cd ${BIZNET_UI_DIR}/${BIZNET_FOLDER_NAME}Setup/webapp/util
cat "${BIZNET_TEST_DIR}/newman/CleanAllAndSetup/data/${ASSET_NAME}${ENTITY_TYPE}s.json" >> localStoreSamples.ubak
perl -i -p0e 's/<BEGIN_REPLACE_JSON>.*?<END_REPLACE_JSON>/`cat localStoreSamples.ubak`/seg' ${BIZNET_UI_DIR}/${BIZNET_FOLDER_NAME}Setup/webapp/util/localStore${ASSET_NAME}${ENTITY_TYPE}s.js

#Update the Entity formatter
cd ${BIZNET_UI_DIR}/${BIZNET_FOLDER_NAME}Setup/webapp/util
perl -i -p0e 's/<BEGIN_REPLACE_ENTITY_TO_MODEL>.*?<END_REPLACE_ENTITY_TO_MODEL>/`cat replaceEntityToModel.ubak`/seg' ${BIZNET_UI_DIR}/${BIZNET_FOLDER_NAME}Setup/webapp/util/formatter${ASSET_NAME}${ENTITY_TYPE}s.js
perl -i -p0e 's/<BEGIN_REPLACE_ENTITY_TO_CHAINCODE_NEW>.*?<END_REPLACE_ENTITY_TO_CHAINCODE_NEW>/`cat replaceEntityToChaincodeNew.ubak`/seg' ${BIZNET_UI_DIR}/${BIZNET_FOLDER_NAME}Setup/webapp/util/formatter${ASSET_NAME}${ENTITY_TYPE}s.js
perl -i -p0e 's/<BEGIN_REPLACE_ENTITY_TO_CHAINCODE_SEL>.*?<END_REPLACE_ENTITY_TO_CHAINCODE_SEL>/`cat replaceEntityToChaincodeSelected.ubak`/seg' ${BIZNET_UI_DIR}/${BIZNET_FOLDER_NAME}Setup/webapp/util/formatter${ASSET_NAME}${ENTITY_TYPE}s.js

#Update the Entity model
cd ${BIZNET_UI_DIR}/${BIZNET_FOLDER_NAME}Setup/webapp/model
perl -i -p0e 's/<BEGIN_REPLACE_COL_NAMES>.*?<END_REPLACE_COL_NAMES>/`cat replaceColNames.ubak`/seg' ${BIZNET_UI_DIR}/${BIZNET_FOLDER_NAME}Setup/webapp/model/models${ASSET_NAME}${ENTITY_TYPE}s.js
perl -i -p0e 's/<BEGIN_REPLACE_MODEL_ATTR>.*?<END_REPLACE_MODEL_ATTR>/`cat replaceModelAttributes.ubak`/seg' ${BIZNET_UI_DIR}/${BIZNET_FOLDER_NAME}Setup/webapp/model/models${ASSET_NAME}${ENTITY_TYPE}s.js

#Update View contents
cd ${BIZNET_UI_DIR}/${BIZNET_FOLDER_NAME}Setup/webapp/view
perl -i -p0e 's/<!--BEGIN_OF_SEL_ENTITY-->.*?<!--END_OF_SEL_ENTITY-->/`cat entityDetailsViewSelected.ubak`/seg' ${BIZNET_UI_DIR}/${BIZNET_FOLDER_NAME}Setup/webapp/view/${ASSET_NAME}${ENTITY_TYPE}Details.view.xml
perl -i -p0e 's/<!--BEGIN_OF_NEW_ENTITY-->.*?<!--END_OF_NEW_ENTITY-->/`cat entityDetailsViewNew.ubak`/seg' ${BIZNET_UI_DIR}/${BIZNET_FOLDER_NAME}Setup/webapp/view/${ASSET_NAME}${ENTITY_TYPE}Details.view.xml

#Delete all backup files
cd ${BIZNET_UI_DIR}/${BIZNET_FOLDER_NAME}Setup/webapp/model
find . -name "*.ubak" -type f -delete

cd ${BIZNET_UI_DIR}/${BIZNET_FOLDER_NAME}Setup/webapp/util
find . -name "*.ubak" -type f -delete

cd ${BIZNET_UI_DIR}/${BIZNET_FOLDER_NAME}Setup/webapp/view
find . -name "*.ubak" -type f -delete

cd ${TARGET_DIR}
find . -name "*.sbak" -type f -delete

cd ..
find . -name "*.sbak" -type f -delete

exit 0
