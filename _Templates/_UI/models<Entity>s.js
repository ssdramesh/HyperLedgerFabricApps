sap.ui.define([
	"sap/ui/model/json/JSONModel"
], function(JSONModel) {
	"use strict";

	return {

		createEntitysModel: function(){

			var oModel = new JSONModel();
			oModel.setDefaultBindingMode("TwoWay");
			oModel.setData({
				chaincode:{
					serviceUrl:"",
					serviceKey:{},
					entity:{}
				},
				entityCollection	: {
					cols:[
						{name:"ID"},
						{name:"ObjectType"},
						{name:"Status"},
						{name:"CreationDate"}<BEGIN_REPLACE_COL_NAMES>
						<END_REPLACE_COL_NAMES>
					],
					items:[]
				},
				selectedEntity:{},
				newEntity:{
					ID:"",
					ObjectType:"",
					Status:"",
					CreationDate:""<BEGIN_REPLACE_MODEL_ATTR>
					<END_REPLACE_MODEL_ATTR>
				},
				selectedEntityID	: "",
				searchEntityID : ""
			});
			return oModel;
		}
	};
});
