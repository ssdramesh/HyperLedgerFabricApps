sap.ui.define([
	"sample-networkSetup/util/restBuilder",
	"sample-networkSetup/util/formatterEntitys",
	"sample-networkSetup/util/localStoreEntitys"
], function(
		restBuilder,
		formatterEntitys,
		localStoreEntitys
	) {
	"use strict";

	return {

		loadAllEntitys:function(oComponent, oModel) {

			if ( !( oComponent.getModel("TestSwitch").getProperty("/testMode") ) ){
				restBuilder.execute(
					oModel.getProperty("/chaincode"),
					"readAll",
					[],
					function(responseData){
						oModel.setProperty(
							"/entityCollection/items",
							formatterEntitys.mapEntitysToModel(responseData)
						);
					});
			} else {
				oModel.setProperty(
					"/entityCollection/items",
					formatterEntitys.mapEntitysToModel(localStoreEntitys.getEntityData())
				);
			}
		},

		loadEntity:function(oModel, selectedEntityID){

			oModel.setProperty(
				"/selectedEntity",
				_.findWhere(oModel.getProperty("/entityCollection/items"),
					{
						ID: selectedEntityID
					},
				this));
		},

		addNewEntity:function(oComponent, oModel){

			if ( !( oComponent.getModel("TestSwitch").getProperty("/testMode") ) ){
				restBuilder.execute(
					oModel.getProperty("/chaincode"),
					"addNew",
					formatterEntitys.mapEntityToChaincode(oModel, true)
				);
			}  else {
				localStoreEntitys.put(formatterEntitys.mapEntityToLocalStorage(oModel, true));
			}
			this.loadAllEntitys(oComponent, oModel);
			return oModel.getProperty("/newEntity/ID");
		},

		updateEntity: function (oComponent, oModel) {

			if (!(oComponent.getModel("TestSwitch").getProperty("/testMode"))) {
				restBuilder.execute(
					oModel.getProperty("/chaincode"),
					"update",
					formatterEntitys.mapEntityToChaincode(oModel, false)
				);
			} else {
				localStoreEntitys.patch(formatterEntitys.mapEntityToLocalStorage(oModel, false));
			}
			this.loadAllEntitys(oComponent, oModel);
		},

		removeEntity : function(oComponent, oModel, entityID){

			if ( !( oComponent.getModel("TestSwitch").getProperty("/testMode") ) ) {
				restBuilder.execute(
					oModel.getProperty("/chaincode"),
					"remove",
					{ID:entityID}
				);
			} else {
				localStoreEntitys.remove(entityID);
			}
			this.loadAllEntitys(oComponent, oModel);
			return true;
		},

		removeAllEntitys : function(oComponent, oModel){

			if (!(oComponent.getModel("TestSwitch").getProperty("/testMode"))) {
				restBuilder.execute(
					oModel.getProperty("/chaincode"),
					"removeAll",
					[]
				);
			} else {
				localStoreEntitys.removeAll();
			}
			this.loadAllEntitys(oComponent, oModel);
			oModel.setProperty("/selectedEntity",{});
			return true;
		}
	};
});
