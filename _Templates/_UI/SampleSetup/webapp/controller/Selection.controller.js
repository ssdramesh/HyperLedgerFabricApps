sap.ui.define([
	"sample-networkSetup/controller/BaseController",
	"sample-networkSetup/util/messageProvider",
<BEGIN_REPLACE_DEF>
	"sample-networkSetup/util/bizNetAccessEntitys",
	"sample-networkSetup/model/modelsEntitys",
<END_REPLACE_DEF>
	"sample-networkSetup/model/modelsBase"
], function(
		BaseController,
		messageProvider,
<BEGIN_REPLACE_INC>
		bizNetAccessEntitys,
		modelsEntitys,
<END_REPLACE_INC>
		modelsBase
	) {
	"use strict";

	return BaseController.extend("sample-networkSetup.controller.Selection", {

		onInit: function(){
			if ( typeof this.getOwnerComponent().getModel("Messages") === "undefined" ){
				this.getOwnerComponent().setModel(modelsBase.createMessagesModel(), "Messages");
			}
			messageProvider.init(this.getOwnerComponent().getModel("Messages"));
		},

		onSelect: function(oEvent) {
			switch (oEvent.getSource().getText()) {
<BEGIN_REPLACE_SEL>
				case "Entitys":
					this.getView().getModel("Selection").setProperty("/entityName", "entity");
					this.getOwnerComponent().setModel(modelsEntitys.createEntitysModel(), "Entitys");
					this.loadMetaData("entity", this.getModel("Entitys"));
					bizNetAccessEntitys.loadAllEntitys(this.getOwnerComponent(), this.getView().getModel("Entitys"));
					this.getOwnerComponent().getRouter().navTo("entitys", {});
					break;
<END_REPLACE_SEL>
			}
		},

		removeAllEntities: function() {

			var msgStripID = this.getView().byId("__stripMessage");
			var location = this.getRunMode().location;

<BEGIN_REPLACE_DEL>
			if (typeof this.getOwnerComponent().getModel("Entitys") === "undefined") {
				this.getOwnerComponent().setModel(modelsEntitys.createEntitysModel(), "Entitys");
			}
			bizNetAccessEntitys.removeAllEntitys(this.getOwnerComponent(), this.getView().getModel("Entitys"));
			messageProvider.addMessage("Success", "All Entitys deleted from sample-network", "No Description", location, 1, "", "http://www.sap.com");
<END_REPLACE_DEL>

			this.showMessageStrip(msgStripID,"All Existing sample-network data deleted from SAP BaaS", "S");
		},

		onToggleBaaS: function() {

			this.onToggleRunMode(this.getView().byId("__stripMessage"));
		},

		onMessagePress: function() {

			this.onShowMessageDialog("sample-network Maintenance Log");
		}
	});
});
