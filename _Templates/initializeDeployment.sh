#!/bin/bash

#Colors
DARK_GRAY='\033[1;30m'
RED='\033[0;31m'
GREEN='\033[0;32m'
ORANGE='\033[0;33m'
BLUE='\033[0;34m'
PURPLE='\033[0;35m'
CYAN='\033[0;36m'
LIGHT_GRAY='\033[0;36m'
LIGHT_RED='\033[1;31m'
LIGHT_GREEN='\033[1;32m'
YELLOW='\033[1;33m'
LIGHT_BLUE='\033[1;34m'
LIGHT_PURPLE='\033[1;35m'
LIGHT_CYAN='\033[1;36m'
WHITE='\033[1;37m'
NC='\033[0m'

TEMPLATE_DEPLOYMENT_DIR=${1}
BIZNET_DEPLOY_DIR=${2}
BIZNET_SCRIPTS_DIR=${3}
BIZNET_UI_DIR=${4}
BIZNET_FOLDER_NAME={5}

#Check and delete previous states of configuration files
find . -name "*.dbak" -type f -delete

#Copy deployment descriptor skeleton
cp -fi "${TEMPLATE_DEPLOYMENT_DIR}/_initTemplateChaincodeDetails.json" "${BIZNET_DEPLOY_DIR}/chaincodeDetails.json"

#Copy channel service details
cp -rfi "${TEMPLATE_DEPLOYMENT_DIR}/ChannelDetails" "${BIZNET_DEPLOY_DIR}"

exit 0
