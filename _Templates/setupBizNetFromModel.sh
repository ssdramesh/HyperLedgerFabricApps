#!/bin/bash

#Colors
DARK_GRAY='\033[1;30m'
RED='\033[0;31m'
GREEN='\033[0;32m'
ORANGE='\033[0;33m'
BLUE='\033[0;34m'
PURPLE='\033[0;35m'
CYAN='\033[0;36m'
LIGHT_GRAY='\033[0;36m'
LIGHT_RED='\033[1;31m'
LIGHT_GREEN='\033[1;32m'
YELLOW='\033[1;33m'
LIGHT_BLUE='\033[1;34m'
LIGHT_PURPLE='\033[1;35m'
LIGHT_CYAN='\033[1;36m'
WHITE='\033[1;37m'
NC='\033[0m'

BIZNET_MDJ_FILE=$1
MODE="d"
if [[ $BIZNET_MDJ_FILE == "" ]]; then
  MODE="i"
fi

PARENT_DIR="$(dirname "$(pwd)")"

if [[ $MODE == "i" ]]; then
  echo -e "${GREEN}WELCOME TO BUSINESS NETWORK SETUP FROM MODEL${NC}"
  echo
fi
#Source folders for templates
TEMPLATE_ROOT_DIR="${PARENT_DIR}/_Templates"
TEMPLATE_DEPLOYMENT_DIR="${TEMPLATE_ROOT_DIR}/_Deployment"
TEMPLATE_TEST_DIR="${TEMPLATE_ROOT_DIR}/_Test"
TEMPLATE_MODEL_DIR="${PARENT_DIR}/_NetworkModels"
TEMPLATE_SCRIPTS_DIR="${TEMPLATE_ROOT_DIR}/_Scripts"
TEMPLATE_UI_DIR="${TEMPLATE_ROOT_DIR}/_UI"
TEMPLATE_ASSET_DIR="${TEMPLATE_ROOT_DIR}/Assets"
TEMPLATE_PARTICIPANT_DIR="${TEMPLATE_ROOT_DIR}/Participants"

# Load & Extract the network name from the model

if [[ $BIZNET_MDJ_FILE == "" ]]; then
  echo -e "${GREEN}Enter the full file path of your business network model file [${RED}.mdj${NC}]: ${NC}"
  read BIZNET_MDJ_FILE
fi
cd "$(dirname "${BIZNET_MDJ_FILE}")"

BIZNET_FOLDER_NAME=`cat ${BIZNET_MDJ_FILE} | jq 'if ._type == "Project" then .name else "Project not found" end'`
if [[ ${BIZNET_FOLDER_NAME} == "Project not found" ]]; then
  echo "The specified .mdj file does not contain a model project" && exit
fi
BIZNET_FOLDER_NAME=$(echo "$BIZNET_FOLDER_NAME" | tr -d '"')
cd ${PARENT_DIR}

BIZNET_DIR=${PARENT_DIR}/${BIZNET_FOLDER_NAME}
BIZNET_SRC_DIR=${BIZNET_DIR}/chaincode
BIZNET_DEPLOY_DIR=${BIZNET_DIR}/deployment
BIZNET_MODEL_DIR=${BIZNET_DIR}/model
BIZNET_SCRIPTS_DIR=${BIZNET_DIR}/scripts
BIZNET_TEST_DIR=${BIZNET_DIR}/test
BIZNET_UI_DIR=${BIZNET_DIR}/ui

echo -e "${GREEN}Setting up your business network from the specified model (.mdj file)...${NC}"
#Business Network Folder
if [ -d "${BIZNET_DIR}" ]; then
  echo -e "${YELLOW}A folder for this business network exists, so will NOT be created${NC}"
else
  cd "${PARENT_DIR}" && mkdir "${BIZNET_FOLDER_NAME}"
fi
#Chaincode folder
if [ -d "${BIZNET_SRC_DIR}" ]; then
  echo -e "${YELLOW}A chaincode folder for this business network exists, so will NOT be created${NC}"
else
  cd "${BIZNET_DIR}" && mkdir chaincode
fi
#Deployment folder
if [ -d "${BIZNET_DEPLOY_DIR}" ]; then
  echo -e "${YELLOW}A deployment folder for this business network exists and will NOT be created${NC}"
else
  cd "${BIZNET_DIR}" && mkdir deployment && cd deployment && mkdir ChannelDetails
  bash ${TEMPLATE_ROOT_DIR}/initializeDeployment.sh \
    "${TEMPLATE_DEPLOYMENT_DIR}"\
    "${BIZNET_DEPLOY_DIR}" \
    "${BIZNET_SCRIPTS_DIR}" \
    "${BIZNET_UI_DIR}" \
    "${BIZNET_FOLDER_NAME}"
fi
#Test folder
if [ -d "${BIZNET_TEST_DIR}" ]; then
  echo -e "${YELLOW}A test folder for this business network exists, so will NOT be created${NC}"
else
  cd "${BIZNET_DIR}" && mkdir test && cd test && mkdir newman && cd newman && mkdir CleanAllAndSetup && cd CleanAllAndSetup && mkdir data && mkdir log
  cd "${BIZNET_TEST_DIR}"
  bash ${TEMPLATE_ROOT_DIR}/initializeTest.sh \
    "${TEMPLATE_TEST_DIR}" \
    "${BIZNET_TEST_DIR}"\
    "${BIZNET_FOLDER_NAME}" \
    "${BIZNET_DIR}"
fi
#Model folder
if [ -d "${BIZNET_MODEL_DIR}" ]; then
  echo -e "${YELLOW}A model folder for this business network exists, so will NOT be created${NC}"
else
  cd "${BIZNET_DIR}" && mkdir model
  cd "${TEMPLATE_MODEL_DIR}"
fi
cp -fi "${BIZNET_MDJ_FILE}" "${BIZNET_MODEL_DIR}/${BIZNET_FOLDER_NAME}.mdj"

# Parse .mdj file to a simpler json
bash ${TEMPLATE_ROOT_DIR}/mdjParser.sh \
  "${BIZNET_MDJ_FILE}"\
  "${BIZNET_MODEL_DIR}"\
  "${BIZNET_FOLDER_NAME}"

#ui folder
if [ -d "$BIZNET_UI_DIR" ]; then
  echo -e "${YELLOW}A ui folder for this business network exists, so will NOT be created${NC}"
else
  cd "${BIZNET_DIR}" && mkdir ui
  bash ${TEMPLATE_ROOT_DIR}/initializeSetupUI.sh \
    "${TEMPLATE_UI_DIR}"\
    "${BIZNET_UI_DIR}"\
    "${BIZNET_FOLDER_NAME}"
fi
#scripts folder
if [ -d "${BIZNET_SCRIPTS_DIR}" ]; then
  echo -e "${YELLOW}A scripts folder for this business network exists, so will NOT be created${NC}"
else
  cd "${BIZNET_DIR}" && mkdir scripts && cd scripts
  bash ${TEMPLATE_ROOT_DIR}/initializeScripts.sh \
    "${TEMPLATE_SCRIPTS_DIR}" \
    "${BIZNET_SCRIPTS_DIR}" \
    "${BIZNET_FOLDER_NAME}" \
    "${BIZNET_SRC_DIR}" \
    "${BIZNET_DEPLOY_DIR}" \
    "${BIZNET_TEST_DIR}" \
    "${BIZNET_UI_DIR}"
fi
#TODO: README.md
if [ -f "$BIZNET_DIR/README.md" ]; then
  echo -e "${YELLOW}A README.md for this network exists, so will NOT be created${NC}"
else
  cd "${BIZNET_DIR}" && touch README.md
fi
cp -f "${TEMPLATE_ROOT_DIR}/_Markdown/_README_TEMPLATE.md" README.md

bash ${TEMPLATE_ROOT_DIR}/initializeREADME.sh \
  "${BIZNET_DIR}" \
  "${BIZNET_MODEL_DIR}" \
  "${BIZNET_FOLDER_NAME}"

# Loop at entities and create them
IS_FIRST=y
for i in $(jq '.entities | keys | .[]' ${BIZNET_MODEL_DIR}/${BIZNET_FOLDER_NAME}.json); do
# Extract entity properties
  ENTITY=$(jq -r ".entities[$i]" ${BIZNET_MODEL_DIR}/${BIZNET_FOLDER_NAME}.json);
  ENTITY_TYPE=$(jq -r '.stereotype' <<< "${ENTITY}");
  ENTITY_NAME=$(jq -r '.name' <<< "${ENTITY}");
  ENTITY_ATTRIBUTES=$(jq -r '.attributes' <<< "${ENTITY}")
# Add attributes of the entity
  case ${ENTITY_TYPE} in

    Asset )
    ASSET_NAME=$(echo ${ENTITY_NAME%Asset})
    ASSET_NAME_LOWER=$(echo ${ASSET_NAME:0:1} | tr '[A-Z]' '[a-z]')${ASSET_NAME:1}
    ASSET_ABBR=$(echo ${ASSET_NAME_LOWER} | tr -d aeiou)
#   Create source directories
    cd "${BIZNET_SRC_DIR}" && \
      mkdir "${ASSET_NAME}${ENTITY_TYPE}" && \
      cd "${ASSET_NAME}${ENTITY_TYPE}" && \
      mkdir src
    cp -fi "${TEMPLATE_ASSET_DIR}/SampleAsset.go" "${BIZNET_SRC_DIR}/${ASSET_NAME}${ENTITY_TYPE}/src/${ASSET_NAME}${ENTITY_TYPE}.go"
    cp -fi "${TEMPLATE_ASSET_DIR}/SampleAsset_test.go" "${BIZNET_SRC_DIR}/${ASSET_NAME}${ENTITY_TYPE}/src/${ASSET_NAME}${ENTITY_TYPE}_test.go"
    cp -fi "${TEMPLATE_ASSET_DIR}/gjson.go" "${BIZNET_SRC_DIR}/${ASSET_NAME}${ENTITY_TYPE}/src/gjson.go"
    cp -fi "${TEMPLATE_ASSET_DIR}/gjson_test.go" "${BIZNET_SRC_DIR}/${ASSET_NAME}${ENTITY_TYPE}/src/gjson_test.go"
    cp -fi "${TEMPLATE_ASSET_DIR}/match.go" "${BIZNET_SRC_DIR}/${ASSET_NAME}${ENTITY_TYPE}/src/match.go"
    cp -fi "${TEMPLATE_ASSET_DIR}/match_test.go" "${BIZNET_SRC_DIR}/${ASSET_NAME}${ENTITY_TYPE}/src/match_test.go"
    cp -fi "${TEMPLATE_ASSET_DIR}/SampleAsset.yaml" "${BIZNET_SRC_DIR}/${ASSET_NAME}${ENTITY_TYPE}/src/${ASSET_NAME}${ENTITY_TYPE}.yaml"
    cp -fi "${TEMPLATE_ASSET_DIR}/chaincode.yaml" "${BIZNET_SRC_DIR}/${ASSET_NAME}${ENTITY_TYPE}/chaincode.yaml"
#   Enhance the code artifacts (.go, _test.go and .yaml, test, deployment and ui etc.) for this entity
    bash ${TEMPLATE_ROOT_DIR}/enhanceAssetStructureFromModel.sh \
      "${BIZNET_SRC_DIR}/${ASSET_NAME}${ENTITY_TYPE}/src" \
      "${BIZNET_SRC_DIR}/${ASSET_NAME}${ENTITY_TYPE}/src/${ASSET_NAME}${ENTITY_TYPE}.go" \
      "${BIZNET_SRC_DIR}/${ASSET_NAME}${ENTITY_TYPE}/src/${ASSET_NAME}${ENTITY_TYPE}_test.go" \
      "${BIZNET_SRC_DIR}/${ASSET_NAME}${ENTITY_TYPE}/src/${ASSET_NAME}${ENTITY_TYPE}.yaml" \
      "${BIZNET_TEST_DIR}" \
      "${ASSET_NAME}" \
      "${ASSET_NAME_LOWER}" \
      "${ENTITY_TYPE}" \
      "${TEMPLATE_TEST_DIR}" \
      "${TEMPLATE_UI_DIR}" \
      "${BIZNET_UI_DIR}" \
      "${BIZNET_FOLDER_NAME}" \
      "${TEMPLATE_ROOT_DIR}" \
      "${ENTITY_ATTRIBUTES}"
#   Enhance the deployment descriptor for this entity
    bash ${TEMPLATE_ROOT_DIR}/enhanceEntityDeployment.sh \
      "${BIZNET_DEPLOY_DIR}" \
      "${ASSET_NAME}" \
      "${ASSET_NAME_LOWER}" \
      "${ENTITY_TYPE}" \
      "${IS_FIRST}"
#   Enhance the test collections / scripts for this entity
    bash ${TEMPLATE_ROOT_DIR}/enhanceEntityTest.sh \
      "${TEMPLATE_TEST_DIR}" \
      "${BIZNET_FOLDER_NAME}" \
      "${BIZNET_TEST_DIR}" \
      "${ASSET_NAME}" \
      "${ENTITY_TYPE}" \
      "${TEMPLATE_UI_DIR}" \
      "${BIZNET_UI_DIR}" \
      "${BIZNET_FOLDER_NAME}" \
      "${TEMPLATE_ROOT_DIR}"
#   Replace variables - .go file
    sed -i.bak 's/SampleAsset/'${ASSET_NAME}${ENTITY_TYPE}'/g' "${BIZNET_SRC_DIR}/${ASSET_NAME}${ENTITY_TYPE}/src/${ASSET_NAME}${ENTITY_TYPE}.go"
    sed -i.bak 's/Sample/'${ASSET_NAME}'/g' "${BIZNET_SRC_DIR}/${ASSET_NAME}${ENTITY_TYPE}/src/${ASSET_NAME}${ENTITY_TYPE}.go"
    sed -i.bak 's/sample/'${ASSET_NAME_LOWER}'/g' "${BIZNET_SRC_DIR}/${ASSET_NAME}${ENTITY_TYPE}/src/${ASSET_NAME}${ENTITY_TYPE}.go"
    sed -i.bak 's/smpl/'${ASSET_ABBR}'/g' "${BIZNET_SRC_DIR}/${ASSET_NAME}${ENTITY_TYPE}/src/${ASSET_NAME}${ENTITY_TYPE}.go"
#   Replace variables - _test.go file
    sed -i.bak 's/SampleAsset/'${ASSET_NAME}${ENTITY_TYPE}'/g' "${BIZNET_SRC_DIR}/${ASSET_NAME}${ENTITY_TYPE}/src/${ASSET_NAME}${ENTITY_TYPE}_test.go"
    sed -i.bak 's/Sample/'${ASSET_NAME}'/g' "${BIZNET_SRC_DIR}/${ASSET_NAME}${ENTITY_TYPE}/src/${ASSET_NAME}${ENTITY_TYPE}_test.go"
    sed -i.bak 's/sample/'${ASSET_NAME_LOWER}'/g' "${BIZNET_SRC_DIR}/${ASSET_NAME}${ENTITY_TYPE}/src/${ASSET_NAME}${ENTITY_TYPE}_test.go"
#   Replace variables - openAPI.yaml file
    sed -i.bak 's/SampleAsset/'${ASSET_NAME}${ENTITY_TYPE}'/g' "${BIZNET_SRC_DIR}/${ASSET_NAME}${ENTITY_TYPE}/src/${ASSET_NAME}${ENTITY_TYPE}.yaml"
    sed -i.bak 's/Sample/'${ASSET_NAME}'/g' "${BIZNET_SRC_DIR}/${ASSET_NAME}${ENTITY_TYPE}/src/${ASSET_NAME}${ENTITY_TYPE}.yaml"
    sed -i.bak 's/sampleAsset/'${ASSET_NAME_LOWER}${ENTITY_TYPE}'/g' "${BIZNET_SRC_DIR}/${ASSET_NAME}${ENTITY_TYPE}/src/${ASSET_NAME}${ENTITY_TYPE}.yaml"
#   Replace variables - chaincode.yaml file
    sed -i.bak 's/sampleAsset/'${ASSET_NAME_LOWER}${ENTITY_TYPE}'/g' "${BIZNET_SRC_DIR}/${ASSET_NAME}${ENTITY_TYPE}/chaincode.yaml"
    sed -i.bak 's/sample-net/'${BIZNET_FOLDER_NAME}'/g' "${BIZNET_SRC_DIR}/${ASSET_NAME}${ENTITY_TYPE}/chaincode.yaml"
#   Delete backup files
    cd "${BIZNET_SRC_DIR}/${ASSET_NAME}${ENTITY_TYPE}"
    find . -name "*.bak" -type f -delete
#   Zip file for chaincode deployment
    zip -rq "${BIZNET_SRC_DIR}/${ASSET_NAME}${ENTITY_TYPE}/${ASSET_NAME}${ENTITY_TYPE}.zip" ./*
    echo -e "${CYAN}Added ${ENTITY_TYPE}: ${ASSET_NAME}${ENTITY_TYPE} to your business network: ${BIZNET_FOLDER_NAME}${NC}"
    IS_FIRST=n
    continue;;

    Participant )
    PARTICIPANT_NAME=$(echo ${ENTITY_NAME%Participant})
    PARTICIPANT_NAME_LOWER=$(echo ${PARTICIPANT_NAME:0:1} | tr '[A-Z]' '[a-z]')${PARTICIPANT_NAME:1}
    PARTICIPANT_ABBR=$(echo ${PARTICIPANT_NAME_LOWER} | tr -d aeiou)
#   Create source directories
    cd "${BIZNET_SRC_DIR}" && mkdir "${PARTICIPANT_NAME}${ENTITY_TYPE}" && cd "${PARTICIPANT_NAME}${ENTITY_TYPE}" && mkdir src
    cp -fi "${TEMPLATE_PARTICIPANT_DIR}/SampleParticipant.go" "${BIZNET_SRC_DIR}/${PARTICIPANT_NAME}${ENTITY_TYPE}/src/${PARTICIPANT_NAME}${ENTITY_TYPE}.go"
    cp -fi "${TEMPLATE_PARTICIPANT_DIR}/SampleParticipant_test.go" "${BIZNET_SRC_DIR}/${PARTICIPANT_NAME}${ENTITY_TYPE}/src/${PARTICIPANT_NAME}${ENTITY_TYPE}_test.go"
    cp -fi "${TEMPLATE_PARTICIPANT_DIR}/gjson.go" "${BIZNET_SRC_DIR}/${PARTICIPANT_NAME}${ENTITY_TYPE}/src/gjson.go"
    cp -fi "${TEMPLATE_PARTICIPANT_DIR}/gjson_test.go" "${BIZNET_SRC_DIR}/${PARTICIPANT_NAME}${ENTITY_TYPE}/src/gjson_test.go"
    cp -fi "${TEMPLATE_PARTICIPANT_DIR}/match.go" "${BIZNET_SRC_DIR}/${PARTICIPANT_NAME}${ENTITY_TYPE}/src/match.go"
    cp -fi "${TEMPLATE_PARTICIPANT_DIR}/match_test.go" "${BIZNET_SRC_DIR}/${PARTICIPANT_NAME}${ENTITY_TYPE}/src/match_test.go"
    cp -fi "${TEMPLATE_PARTICIPANT_DIR}/SampleParticipant.yaml" "${BIZNET_SRC_DIR}/${PARTICIPANT_NAME}${ENTITY_TYPE}/src/${PARTICIPANT_NAME}${ENTITY_TYPE}.yaml"
    cp -fi "${TEMPLATE_PARTICIPANT_DIR}/chaincode.yaml" "${BIZNET_SRC_DIR}/${PARTICIPANT_NAME}${ENTITY_TYPE}/chaincode.yaml"
#   Enhance the code artifacts (.go, _test.go and .yaml, test, deployment and ui etc.) for this entity
    bash ${TEMPLATE_ROOT_DIR}/enhanceParticipantStructureFromModel.sh \
      "${BIZNET_SRC_DIR}/${PARTICIPANT_NAME}${ENTITY_TYPE}/src" \
      "${BIZNET_SRC_DIR}/${PARTICIPANT_NAME}${ENTITY_TYPE}/src/${PARTICIPANT_NAME}${ENTITY_TYPE}.go" \
      "${BIZNET_SRC_DIR}/${PARTICIPANT_NAME}${ENTITY_TYPE}/src/${PARTICIPANT_NAME}${ENTITY_TYPE}_test.go" \
      "${BIZNET_SRC_DIR}/${PARTICIPANT_NAME}${ENTITY_TYPE}/src/${PARTICIPANT_NAME}${ENTITY_TYPE}.yaml"  \
      "${BIZNET_TEST_DIR}" \
      "${PARTICIPANT_NAME}" \
      "${PARTICIPANT_NAME_LOWER}" \
      "${ENTITY_TYPE}" \
      "${TEMPLATE_TEST_DIR}" \
      "${TEMPLATE_UI_DIR}" \
      "${BIZNET_UI_DIR}" \
      "${BIZNET_FOLDER_NAME}" \
      "${TEMPLATE_ROOT_DIR}" \
      "${ENTITY_ATTRIBUTES}"
#   Enhance the deployment descriptor for this entity
    bash ${TEMPLATE_ROOT_DIR}/enhanceEntityDeployment.sh \
      "${BIZNET_DEPLOY_DIR}" \
      "${PARTICIPANT_NAME}" \
      "${PARTICIPANT_NAME_LOWER}" \
      "${ENTITY_TYPE}" \
      "${IS_FIRST}"
#   Enhance the test collections / scripts for this entity
    bash ${TEMPLATE_ROOT_DIR}/enhanceEntityTest.sh \
      "${TEMPLATE_TEST_DIR}" \
      "${BIZNET_FOLDER_NAME}" \
      "${BIZNET_TEST_DIR}" \
      "${PARTICIPANT_NAME}" \
      "${ENTITY_TYPE}"
#   Replace variables - .go file
    sed -i.bak 's/SampleParticipant/'${PARTICIPANT_NAME}${ENTITY_TYPE}'/g' "${BIZNET_SRC_DIR}/${PARTICIPANT_NAME}${ENTITY_TYPE}/src/${PARTICIPANT_NAME}${ENTITY_TYPE}.go"
    sed -i.bak 's/Sample/'${PARTICIPANT_NAME}'/g' "${BIZNET_SRC_DIR}/${PARTICIPANT_NAME}${ENTITY_TYPE}/src/${PARTICIPANT_NAME}${ENTITY_TYPE}.go"
    sed -i.bak 's/sample/'${PARTICIPANT_NAME_LOWER}'/g' "${BIZNET_SRC_DIR}/${PARTICIPANT_NAME}${ENTITY_TYPE}/src/${PARTICIPANT_NAME}${ENTITY_TYPE}.go"
    sed -i.bak 's/smpl/'${PARTICIPANT_ABBR}'/g' "${BIZNET_SRC_DIR}/${PARTICIPANT_NAME}${ENTITY_TYPE}/src/${PARTICIPANT_NAME}${ENTITY_TYPE}.go"
#   Replace variables - _test.go file
    sed -i.bak 's/SampleParticipant/'${PARTICIPANT_NAME}${ENTITY_TYPE}'/g' "${BIZNET_SRC_DIR}/${PARTICIPANT_NAME}${ENTITY_TYPE}/src/${PARTICIPANT_NAME}${ENTITY_TYPE}_test.go"
    sed -i.bak 's/Sample/'${PARTICIPANT_NAME}'/g' "${BIZNET_SRC_DIR}/${PARTICIPANT_NAME}${ENTITY_TYPE}/src/${PARTICIPANT_NAME}${ENTITY_TYPE}_test.go"
    sed -i.bak 's/sample/'${PARTICIPANT_NAME_LOWER}'/g' "${BIZNET_SRC_DIR}/${PARTICIPANT_NAME}${ENTITY_TYPE}/src/${PARTICIPANT_NAME}${ENTITY_TYPE}_test.go"
#   Replace variables - openAPI.yaml file
    sed -i.bak 's/SampleParticipant/'${PARTICIPANT_NAME}${ENTITY_TYPE}'/g' "${BIZNET_SRC_DIR}/${PARTICIPANT_NAME}${ENTITY_TYPE}/src/${PARTICIPANT_NAME}${ENTITY_TYPE}.yaml"
    sed -i.bak 's/Sample/'${PARTICIPANT_NAME}'/g' "${BIZNET_SRC_DIR}/${PARTICIPANT_NAME}${ENTITY_TYPE}/src/${PARTICIPANT_NAME}${ENTITY_TYPE}.yaml"
    sed -i.bak 's/sampleParticipant/'${PARTICIPANT_NAME_LOWER}${ENTITY_TYPE}'/g' "${BIZNET_SRC_DIR}/${PARTICIPANT_NAME}${ENTITY_TYPE}/src/${PARTICIPANT_NAME}${ENTITY_TYPE}.yaml"
#   Replace variables - chaincode.yaml file
    sed -i.bak 's/sampleParticipant/'${PARTICIPANT_NAME_LOWER}${ENTITY_TYPE}'/g' "${BIZNET_SRC_DIR}/${PARTICIPANT_NAME}${ENTITY_TYPE}/chaincode.yaml"
    sed -i.bak 's/sample-net/'$BIZNET_FOLDER_NAME'/g' "${BIZNET_SRC_DIR}/${PARTICIPANT_NAME}${ENTITY_TYPE}/chaincode.yaml"
#   Delete backup files
    cd "${BIZNET_SRC_DIR}/${PARTICIPANT_NAME}${ENTITY_TYPE}"
    find . -name "*.bak" -type f -delete
#   Zip file for chaincode deployment
    zip -rq "${BIZNET_SRC_DIR}/${PARTICIPANT_NAME}${ENTITY_TYPE}/${PARTICIPANT_NAME}${ENTITY_TYPE}.zip" ./*
    echo -e "${CYAN}Added ${ENTITY_TYPE}: ${PARTICIPANT_NAME}${ENTITY_TYPE} to your business network: ${BIZNET_FOLDER_NAME}${NC}"
    IS_FIRST=n
    continue;;

    * )
    echo "Invalid Option"
    break;;

  esac
  continue
done

# Finalize biznet project setup
bash ${TEMPLATE_ROOT_DIR}/finalizeDeployment.sh "${BIZNET_DEPLOY_DIR}"
bash ${TEMPLATE_ROOT_DIR}/finalizeTest.sh "${BIZNET_FOLDER_NAME}" "${BIZNET_TEST_DIR}" "${BIZNET_SCRIPTS_DIR}"
bash ${TEMPLATE_ROOT_DIR}/finalizeSetupUI.sh "${TEMPLATE_UI_DIR}" "${BIZNET_UI_DIR}" "${BIZNET_FOLDER_NAME}"

# Delete the simplified model .json file
cd "${BIZNET_MODEL_DIR}" && find . -name "*.json" -type f -delete && cd "${BIZNET_DIR}"

# biznet setup completion message
echo -e "${GREEN}Setup of your business network ${BIZNET_FOLDER_NAME} completed${NC}"

#Proceed to deploy on blockchain environment?
DEPLOY_CHAINCODE="y"
while [[ $DEPLOY_CHAINCODE == "y" ]]; do
  echo
  echo -e "${ORANGE}Proceeding with deployment...${NC}"
  echo -e "${ORANGE}Deploy ALL entities of your business network on the specified SAP BaaS environment? [${RED}y/n${ORANGE}]: ${NC}"
  read DEPLOY_CHAINCODE
  echo
  if [[ "${DEPLOY_CHAINCODE}" == "n" ]]; then
    break
  fi
  if [[ "${DEPLOY_CHAINCODE}" == "y" ]]; then
    cd ${BIZNET_SCRIPTS_DIR}
    bash ${BIZNET_SCRIPTS_DIR}/deployToHLFaaS.sh
    break
  fi
done

#Run newman?
RUN_NEWMAN="y"
while [[ $RUN_NEWMAN == "y" ]]; do
  echo
  echo -e "${BLUE}Proceeding with sample data creation...${NC}"
  echo -e "${BLUE}Do you want to run newman to populate sample data in your network? [${RED}y/n${LIGHT_GREEN}]: ${NC}"
  read RUN_NEWMAN
  echo
  if [[ "${RUN_NEWMAN}" == "n" ]]; then
    break
  fi
  if [[ "${RUN_NEWMAN}" == "y" ]]; then
    cd ${BIZNET_SCRIPTS_DIR}
    bash ${BIZNET_SCRIPTS_DIR}/runCleanAllAndSetup.sh
    break
  fi
done

#Push cfapp for setting up your entities?
DEPLOY_CF="y"
while [[ $DEPLOY_CF == "y" ]]; do
  echo
  echo -e "${PURPLE}Proceeding with UI deployment [Please check if your ${RED}OS user${NC} ${PURPLE}has${NC} ${RED}administration rights${NC} ${PURPLE}and the same are${NC} ${RED}enabled${NC} ${PURPLE}before you proceed]${NC}"
  echo -e "${PURPLE}[MacOS / Linux]Also ensure that you have ${RED}superuser (su) access${NC} ${PURPLE}before you proceed]${NC}"
  echo -e "${PURPLE}Do you want to deploy the genrated UI5 application on the Cloud Foundry Space and bind to channel service? [${RED}y/n${PURPLE}]: ${NC}"
  read DEPLOY_CF
  echo
  if [[ "${DEPLOY_CF}" == "n" ]]; then
    break
  fi
  if [[ "${DEPLOY_CF}" == "y" ]]; then
    cd ${BIZNET_SCRPITS_DIR}
    bash ${BIZNET_SCRIPTS_DIR}/deployCFUI.sh
    break
  fi
done

#Zip the project contents - to enable import to Web index
cd "${BIZNET_UI_DIR}/${BIZNET_FOLDER_NAME}Setup"
zip -rq "${BIZNET_UI_DIR}/${BIZNET_FOLDER_NAME}Setup.zip" ./*
echo
echo -e "${GREEN}ALL Done! Ready to ROCK?...Goodbye! ;-)"
exit 0
