#!/bin/bash

#Colors
DARK_GRAY='\033[1;30m'
RED='\033[0;31m'
GREEN='\033[0;32m'
ORANGE='\033[0;33m'
BLUE='\033[0;34m'
PURPLE='\033[0;35m'
CYAN='\033[0;36m'
LIGHT_GRAY='\033[0;36m'
LIGHT_RED='\033[1;31m'
LIGHT_GREEN='\033[1;32m'
YELLOW='\033[1;33m'
LIGHT_BLUE='\033[1;34m'
LIGHT_PURPLE='\033[1;35m'
LIGHT_CYAN='\033[1;36m'
WHITE='\033[1;37m'
NC='\033[0m'

TEMPLATE_TEST_DIR=$1
BIZNET_FOLDER_NAME=$2
BIZNET_TEST_DIR=$3
ENTITY_NAME=$4
ENTITY_TYPE=$5

#newman run script template for the new entity
ENTITY_NEWMAN_RUN_TEMPLATE="

echo \"Clean All ${ENTITY_NAME}${ENTITY_TYPE}s before Setup...\"
newman run cleanAll${ENTITY_NAME}${ENTITY_TYPE}s.postman_collection.json -e ${BIZNET_FOLDER_NAME}.postman_environment.json --bail newman
echo \"Setting up ${ENTITY_NAME}${ENTITY_TYPE}s...\"
newman run createAll${ENTITY_NAME}${ENTITY_TYPE}s.postman_collection.json -e ${BIZNET_FOLDER_NAME}.postman_environment.json -d ./data/${ENTITY_NAME}${ENTITY_TYPE}s.json --bail newman"

#newman environment template for the new entity
ENTITY_NEWMAN_ENV_TEMPLATE="
,
{
  \"key\": \"chaincodeId${ENTITY_NAME}${ENTITY_TYPE}\",
  \"value\": \"<CHAINCODE_ID_${ENTITY_NAME}${ENTITY_TYPE}>\",
  \"enabled\": true,
  \"type\": \"text\"
}"

#Append to backup files
echo "$ENTITY_NEWMAN_RUN_TEMPLATE" >> ${BIZNET_TEST_DIR}/newman/CleanAllAndSetup/entityRunScript.tbak
echo "$ENTITY_NEWMAN_ENV_TEMPLATE" >> ${BIZNET_TEST_DIR}/newman/CleanAllAndSetup/entityEnv.tbak

#Copy and replace entity name in the cleanAll POSTMAN Collection for the entity
cp -fi "${TEMPLATE_TEST_DIR}/newman/CleanAllAndSetup/cleanAllEntitys.postman_collection.json" "${BIZNET_TEST_DIR}/newman/CleanAllAndSetup/cleanAll${ENTITY_NAME}${ENTITY_TYPE}s.postman_collection.json"
sed -i.tbak 's/Entity/'${ENTITY_NAME}${ENTITY_TYPE}'/g' "${BIZNET_TEST_DIR}/newman/CleanAllAndSetup/cleanAll${ENTITY_NAME}${ENTITY_TYPE}s.postman_collection.json"
sed -i.tbak 's/sample-net/'${BIZNET_FOLDER_NAME}'/g' "${BIZNET_TEST_DIR}/newman/CleanAllAndSetup/cleanAll${ENTITY_NAME}${ENTITY_TYPE}s.postman_collection.json"

#Copy and replace entity name in the createAll POSTMAN Collection for the entity
cp -fi "${TEMPLATE_TEST_DIR}/newman/CleanAllAndSetup/createAllEntitys.postman_collection.json" "${BIZNET_TEST_DIR}/newman/CleanAllAndSetup/createAll${ENTITY_NAME}${ENTITY_TYPE}s.postman_collection.json"
sed -i.tbak 's/Entity/'${ENTITY_NAME}${ENTITY_TYPE}'/g' "${BIZNET_TEST_DIR}/newman/CleanAllAndSetup/createAll${ENTITY_NAME}${ENTITY_TYPE}s.postman_collection.json"
sed -i.tbak 's/sample-net/'${BIZNET_FOLDER_NAME}'/g' "${BIZNET_TEST_DIR}/newman/CleanAllAndSetup/createAll${ENTITY_NAME}${ENTITY_TYPE}s.postman_collection.json"

#Copy the test data into Postman collection
cd ${BIZNET_TEST_DIR}/newman/CleanAllAndSetup
perl -i -p0e 's/<BEGIN_ENTITY_SAMPLE>.*?<END_ENTITY_SAMPLE>/`cat entityCreateAllPostmanCollection.pbak`/seg' ${BIZNET_TEST_DIR}/newman/CleanAllAndSetup/createAll${ENTITY_NAME}${ENTITY_TYPE}s.postman_collection.json

#Delete postman backup
cd ${BIZNET_TEST_DIR}/newman/CleanAllAndSetup
find . -name "*.pbak" -type f -delete

exit 0
