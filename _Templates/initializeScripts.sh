#!/bin/bash

TEMPLATE_SCRIPTS_DIR=${1}
BIZNET_SCRIPTS_DIR=${2}
BIZNET_FOLDER_NAME=${3}
BIZNET_SRC_DIR=${4}
BIZNET_DEPLOY_DIR=${5}
BIZNET_TEST_DIR=${6}
BIZNET_UI_DIR=${7}

cp -fi "${TEMPLATE_SCRIPTS_DIR}/_initDeployToHLFaaS.sh" "${BIZNET_SCRIPTS_DIR}/deployToHLFaaS.sh"
cp -fi "${TEMPLATE_SCRIPTS_DIR}/_initRunCleanAllAndSetup.sh" "${BIZNET_SCRIPTS_DIR}/runCleanAllAndSetup.sh"
cp -fi "${TEMPLATE_SCRIPTS_DIR}/_initDeployCFUI.sh" "${BIZNET_SCRIPTS_DIR}/deployCFUI.sh"

sed -i.cbak 's|<BIZNET_FOLDER_NAME>|'${BIZNET_FOLDER_NAME}'|g' "${BIZNET_SCRIPTS_DIR}/deployToHLFaaS.sh"
sed -i.cbak 's|<BIZNET_SRC_DIR>|'${BIZNET_SRC_DIR}'|g' "${BIZNET_SCRIPTS_DIR}/deployToHLFaaS.sh"
sed -i.cbak 's|<BIZNET_DEPLOY_DIR>|'${BIZNET_DEPLOY_DIR}'|g' "${BIZNET_SCRIPTS_DIR}/deployToHLFaaS.sh"
sed -i.cbak 's|<BIZNET_TEST_DIR>|'${BIZNET_TEST_DIR}'|g' "${BIZNET_SCRIPTS_DIR}/deployToHLFaaS.sh"
sed -i.cbak 's|<BIZNET_UI_DIR>|'${BIZNET_UI_DIR}'|g' "${BIZNET_SCRIPTS_DIR}/deployToHLFaaS.sh"
sed -i.cbak 's|<BIZNET_SCRIPTS_DIR>|'${BIZNET_SCRIPTS_DIR}'|g' "${BIZNET_SCRIPTS_DIR}/deployToHLFaaS.sh"
sed -i.cbak 's|<BIZNET_SETUP_UI_DIR>|'${BIZNET_UI_DIR}/${BIZNET_FOLDER_NAME}Setup'|g' "${BIZNET_SCRIPTS_DIR}/deployCFUI.sh"
sed -i.cbak 's|<CLEAN_ALL_AND_SETUP_DIR>|'${BIZNET_TEST_DIR}/newman/CleanAllAndSetup'|g' "${BIZNET_SCRIPTS_DIR}/runCleanAllAndSetup.sh"

cd "${BIZNET_SCRIPTS_DIR}"
find . -name "*.cbak" -type f -delete
