#!/bin/bash

#Colors
DARK_GRAY='\033[1;30m'
RED='\033[0;31m'
GREEN='\033[0;32m'
ORANGE='\033[0;33m'
BLUE='\033[0;34m'
PURPLE='\033[0;35m'
CYAN='\033[0;36m'
LIGHT_GRAY='\033[0;36m'
LIGHT_RED='\033[1;31m'
LIGHT_GREEN='\033[1;32m'
YELLOW='\033[1;33m'
LIGHT_BLUE='\033[1;34m'
LIGHT_PURPLE='\033[1;35m'
LIGHT_CYAN='\033[1;36m'
WHITE='\033[1;37m'
NC='\033[0m'

PARENT_DIR="$(dirname "$(pwd)")"

echo -e "${GREEN}Setup your Business Network...${NC}"
#Source folders for templates
TEMPLATE_ROOT_DIR="${PARENT_DIR}/_Templates"
TEMPLATE_DEPLOYMENT_DIR="${TEMPLATE_ROOT_DIR}/_Deployment"
TEMPLATE_TEST_DIR="${TEMPLATE_ROOT_DIR}/_Test"
TEMPLATE_SCRIPTS_DIR="${TEMPLATE_ROOT_DIR}/_Scripts"
TEMPLATE_UI_DIR="${TEMPLATE_ROOT_DIR}/_UI"
TEMPLATE_ASSET_DIR="${TEMPLATE_ROOT_DIR}/Assets"
TEMPLATE_PARTICIPANT_DIR="${TEMPLATE_ROOT_DIR}/Participants"

#Business Network Folder Setup
echo -e "${GREEN}Enter the name of your business network folder [spaces are ${RED}NOT${NC} allowed]: ${NC}"
read BIZNET_FOLDER_NAME
#Target folders for Business Network
BIZNET_DIR=${PARENT_DIR}/${BIZNET_FOLDER_NAME}
BIZNET_SRC_DIR=${BIZNET_DIR}/chaincode
BIZNET_DEPLOY_DIR=${BIZNET_DIR}/deployment
BIZNET_MODEL_DIR=${BIZNET_DIR}/model
BIZNET_SCRIPTS_DIR=${BIZNET_DIR}/scripts
BIZNET_TEST_DIR=${BIZNET_DIR}/test
BIZNET_UI_DIR=${BIZNET_DIR}/ui

echo -e "${GREEN}Creating project folder structure for your business network...${NC}"
#Business Network Folder
if [ -d "${BIZNET_DIR}" ]; then
  echo -e "${YELLOW}A folder for this business network exists, so will NOT be created${NC}"
else
  cd "${PARENT_DIR}" && mkdir "${BIZNET_FOLDER_NAME}"
fi
#Chaincode folder
if [ -d "${BIZNET_SRC_DIR}" ]; then
  echo -e "${YELLOW}A chaincode folder for this business network exists, so will NOT be created${NC}"
else
  cd "${BIZNET_DIR}" && mkdir chaincode
fi
#Deployment folder
if [ -d "${BIZNET_DEPLOY_DIR}" ]; then
  echo -e "${YELLOW}A deployment folder for this business network exists and will NOT be created${NC}"
else
  cd "${BIZNET_DIR}" && mkdir deployment && cd deployment && mkdir ChannelDetails
  bash ${TEMPLATE_ROOT_DIR}/initializeDeployment.sh \
    "${TEMPLATE_DEPLOYMENT_DIR}"\
    "${BIZNET_DEPLOY_DIR}" \
    "${BIZNET_SCRIPTS_DIR}" \
    "${BIZNET_UI_DIR}" \
    "${BIZNET_FOLDER_NAME}"
fi
#Test folder
if [ -d "${BIZNET_TEST_DIR}" ]; then
  echo -e "${YELLOW}A test folder for this business network exists, so will NOT be created${NC}"
else
  cd "${BIZNET_DIR}" && mkdir test && cd test && mkdir newman && cd newman && mkdir CleanAllAndSetup && cd CleanAllAndSetup && mkdir data && mkdir log
  cd "${BIZNET_TEST_DIR}"
  bash ${TEMPLATE_ROOT_DIR}/initializeTest.sh \
    "${TEMPLATE_TEST_DIR}" \
    "${BIZNET_TEST_DIR}"\
    "${BIZNET_FOLDER_NAME}" \
    "${BIZNET_DIR}"
fi
#Model folder
if [ -d "${BIZNET_MODEL_DIR}" ]; then
  echo -e "${YELLOW}A model folder for this business network exists, so will NOT be created${NC}"
else
  cd "${BIZNET_DIR}" && mkdir model
fi
#ui folder
if [ -d "$BIZNET_UI_DIR" ]; then
  echo -e "${YELLOW}A ui folder for this business network exists, so will NOT be created${NC}"
else
  cd "${BIZNET_DIR}" && mkdir ui
  bash ${TEMPLATE_ROOT_DIR}/initializeSetupUI.sh \
    "${TEMPLATE_UI_DIR}"\
    "${BIZNET_UI_DIR}"\
    "${BIZNET_FOLDER_NAME}"
fi
#scripts folder
if [ -d "${BIZNET_SCRIPTS_DIR}" ]; then
  echo -e "${YELLOW}A scripts folder for this business network exists, so will NOT be created${NC}"
else
  cd "${BIZNET_DIR}" && mkdir scripts && cd scripts
  bash ${TEMPLATE_ROOT_DIR}/initializeScripts.sh \
    "${TEMPLATE_SCRIPTS_DIR}" \
    "${BIZNET_SCRIPTS_DIR}" \
    "${BIZNET_FOLDER_NAME}" \
    "${BIZNET_SRC_DIR}" \
    "${BIZNET_DEPLOY_DIR}" \
    "${BIZNET_TEST_DIR}" \
    "${BIZNET_UI_DIR}"
fi
#README.md
if [ -f "$BIZNET_DIR/README.md" ]; then
  echo -e "${YELLOW}A README.md for this network exists, so will NOT be created${NC}"
else
  cd "${BIZNET_DIR}" && touch README.md
fi
echo -e "${GREEN}...Done!${NC}"
MORE_ENTITIES=y
IS_FIRST=y

while [[ $MORE_ENTITIES == y ]]; do
  echo -e "${CYAN}Select the type of entity you want to create:[Select ${RED}number${CYAN} and ENTER]: "
  select ENTITY_TYPE in Asset Participant; do
    case $ENTITY_TYPE in
        Asset )
#       Collect replacement variables
        echo -e "${CYAN}Entity Type:${NC} "$ENTITY_TYPE;
        echo -e "${CYAN}Enter Name of the Asset in UpperCamelCase:${NC}"
        read ASSET_NAME
        ASSET_NAME_LOWER=$(echo ${ASSET_NAME:0:1} | tr '[A-Z]' '[a-z]')${ASSET_NAME:1}
        ASSET_ABBR=$(echo ${ASSET_NAME_LOWER} | tr -d aeiou)
#       Create source directories
        cd "${BIZNET_SRC_DIR}" && \
          mkdir "${ASSET_NAME}${ENTITY_TYPE}" && \
          cd "${ASSET_NAME}${ENTITY_TYPE}" && \
          mkdir src
        cp -fi "${TEMPLATE_ASSET_DIR}/SampleAsset.go" "${BIZNET_SRC_DIR}/${ASSET_NAME}${ENTITY_TYPE}/src/${ASSET_NAME}${ENTITY_TYPE}.go"
        cp -fi "${TEMPLATE_ASSET_DIR}/SampleAsset_test.go" "${BIZNET_SRC_DIR}/${ASSET_NAME}${ENTITY_TYPE}/src/${ASSET_NAME}${ENTITY_TYPE}_test.go"
        cp -fi "${TEMPLATE_ASSET_DIR}/gjson.go" "${BIZNET_SRC_DIR}/${ASSET_NAME}${ENTITY_TYPE}/src/gjson.go"
        cp -fi "${TEMPLATE_ASSET_DIR}/gjson_test.go" "${BIZNET_SRC_DIR}/${ASSET_NAME}${ENTITY_TYPE}/src/gjson_test.go"
        cp -fi "${TEMPLATE_ASSET_DIR}/match.go" "${BIZNET_SRC_DIR}/${ASSET_NAME}${ENTITY_TYPE}/src/match.go"
        cp -fi "${TEMPLATE_ASSET_DIR}/match_test.go" "${BIZNET_SRC_DIR}/${ASSET_NAME}${ENTITY_TYPE}/src/match_test.go"
        cp -fi "${TEMPLATE_ASSET_DIR}/SampleAsset.yaml" "${BIZNET_SRC_DIR}/${ASSET_NAME}${ENTITY_TYPE}/src/${ASSET_NAME}${ENTITY_TYPE}.yaml"
        cp -fi "${TEMPLATE_ASSET_DIR}/chaincode.yaml" "${BIZNET_SRC_DIR}/${ASSET_NAME}${ENTITY_TYPE}/chaincode.yaml"
#       Enhance the code artifacts (.go, _test.go and .yaml, test, deployment and ui etc.) for this entity
        bash ${TEMPLATE_ROOT_DIR}/enhanceAssetStructure.sh \
          "${BIZNET_SRC_DIR}/${ASSET_NAME}${ENTITY_TYPE}/src" \
          "${BIZNET_SRC_DIR}/${ASSET_NAME}${ENTITY_TYPE}/src/${ASSET_NAME}${ENTITY_TYPE}.go" \
          "${BIZNET_SRC_DIR}/${ASSET_NAME}${ENTITY_TYPE}/src/${ASSET_NAME}${ENTITY_TYPE}_test.go" \
          "${BIZNET_SRC_DIR}/${ASSET_NAME}${ENTITY_TYPE}/src/${ASSET_NAME}${ENTITY_TYPE}.yaml" \
          "${BIZNET_TEST_DIR}" \
          "${ASSET_NAME}" \
          "${ASSET_NAME_LOWER}" \
          "${ENTITY_TYPE}" \
          "${TEMPLATE_TEST_DIR}" \
          "${TEMPLATE_UI_DIR}" \
          "${BIZNET_UI_DIR}" \
          "${BIZNET_FOLDER_NAME}" \
          "${TEMPLATE_ROOT_DIR}" \
          && echo -e "${GREEN}Modeling ${ENTITY_TYPE}: ${ASSET_NAME}${ENTITY_TYPE} Done!${NC}"
#       Enhance the deployment descriptor for this entity
        bash ${TEMPLATE_ROOT_DIR}/enhanceEntityDeployment.sh \
          "${BIZNET_DEPLOY_DIR}" \
          "${ASSET_NAME}" \
          "${ASSET_NAME_LOWER}" \
          "${ENTITY_TYPE}" \
          "${IS_FIRST}" \
          && echo -e "${GREEN}Deployment descriptor ${ENTITY_TYPE}: ${ASSET_NAME}${ENTITY_TYPE} Done!${NC}"
#       Enhance the test collections / scripts for this entity
        bash ${TEMPLATE_ROOT_DIR}/enhanceEntityTest.sh \
          "${TEMPLATE_TEST_DIR}" \
          "${BIZNET_FOLDER_NAME}" \
          "${BIZNET_TEST_DIR}" \
          "${ASSET_NAME}" \
          "${ENTITY_TYPE}" \
          "${TEMPLATE_UI_DIR}" \
          "${BIZNET_UI_DIR}" \
          "${BIZNET_FOLDER_NAME}" \
          "${TEMPLATE_ROOT_DIR}" \
          && echo -e "${GREEN}Postman environment & collections ${ENTITY_TYPE}: ${ASSET_NAME}${ENTITY_TYPE} Done!${NC}"
#       Replace variables - .go file
        sed -i.bak 's/SampleAsset/'${ASSET_NAME}${ENTITY_TYPE}'/g' "${BIZNET_SRC_DIR}/${ASSET_NAME}${ENTITY_TYPE}/src/${ASSET_NAME}${ENTITY_TYPE}.go"
        sed -i.bak 's/Sample/'${ASSET_NAME}'/g' "${BIZNET_SRC_DIR}/${ASSET_NAME}${ENTITY_TYPE}/src/${ASSET_NAME}${ENTITY_TYPE}.go"
        sed -i.bak 's/sample/'${ASSET_NAME_LOWER}'/g' "${BIZNET_SRC_DIR}/${ASSET_NAME}${ENTITY_TYPE}/src/${ASSET_NAME}${ENTITY_TYPE}.go"
        sed -i.bak 's/smpl/'${ASSET_ABBR}'/g' "${BIZNET_SRC_DIR}/${ASSET_NAME}${ENTITY_TYPE}/src/${ASSET_NAME}${ENTITY_TYPE}.go"
#       Replace variables - _test.go file
        sed -i.bak 's/SampleAsset/'${ASSET_NAME}${ENTITY_TYPE}'/g' "${BIZNET_SRC_DIR}/${ASSET_NAME}${ENTITY_TYPE}/src/${ASSET_NAME}${ENTITY_TYPE}_test.go"
        sed -i.bak 's/Sample/'${ASSET_NAME}'/g' "${BIZNET_SRC_DIR}/${ASSET_NAME}${ENTITY_TYPE}/src/${ASSET_NAME}${ENTITY_TYPE}_test.go"
        sed -i.bak 's/sample/'${ASSET_NAME_LOWER}'/g' "${BIZNET_SRC_DIR}/${ASSET_NAME}${ENTITY_TYPE}/src/${ASSET_NAME}${ENTITY_TYPE}_test.go"
#       Replace variables - openAPI.yaml file
        sed -i.bak 's/SampleAsset/'${ASSET_NAME}${ENTITY_TYPE}'/g' "${BIZNET_SRC_DIR}/${ASSET_NAME}${ENTITY_TYPE}/src/${ASSET_NAME}${ENTITY_TYPE}.yaml"
        sed -i.bak 's/Sample/'${ASSET_NAME}'/g' "${BIZNET_SRC_DIR}/${ASSET_NAME}${ENTITY_TYPE}/src/${ASSET_NAME}${ENTITY_TYPE}.yaml"
        sed -i.bak 's/sampleAsset/'${ASSET_NAME_LOWER}${ENTITY_TYPE}'/g' "${BIZNET_SRC_DIR}/${ASSET_NAME}${ENTITY_TYPE}/src/${ASSET_NAME}${ENTITY_TYPE}.yaml"
#       Replace variables - chaincode.yaml file
        sed -i.bak 's/sampleAsset/'${ASSET_NAME_LOWER}${ENTITY_TYPE}'/g' "${BIZNET_SRC_DIR}/${ASSET_NAME}${ENTITY_TYPE}/chaincode.yaml"
        sed -i.bak 's/sample-net/'${BIZNET_FOLDER_NAME}'/g' "${BIZNET_SRC_DIR}/${ASSET_NAME}${ENTITY_TYPE}/chaincode.yaml"
#       Delete backup files
        cd "${BIZNET_SRC_DIR}/${ASSET_NAME}${ENTITY_TYPE}"
        find . -name "*.bak" -type f -delete
#       Zip file for chaincode deployment
        zip -rq "${BIZNET_SRC_DIR}/${ASSET_NAME}${ENTITY_TYPE}/${ASSET_NAME}${ENTITY_TYPE}.zip" ./*
        IS_FIRST=n
        break;;

        Participant )
#       Collect replacement variables
        echo -e "${CYAN}Entity Type:${NC} "$ENTITY_TYPE;
        echo -e "${CYAN}Enter Name of the Participant in UpperCamelCase:${NC}"
        read PARTICIPANT_NAME
        PARTICIPANT_NAME_LOWER=$(echo ${PARTICIPANT_NAME:0:1} | tr '[A-Z]' '[a-z]')${PARTICIPANT_NAME:1}
        PARTICIPANT_ABBR=$(echo ${PARTICIPANT_NAME_LOWER} | tr -d aeiou)
#       Create source directories
        cd "${BIZNET_SRC_DIR}" && mkdir "${PARTICIPANT_NAME}${ENTITY_TYPE}" && cd "${PARTICIPANT_NAME}${ENTITY_TYPE}" && mkdir src
        cp -fi "${TEMPLATE_PARTICIPANT_DIR}/SampleParticipant.go" "${BIZNET_SRC_DIR}/${PARTICIPANT_NAME}${ENTITY_TYPE}/src/${PARTICIPANT_NAME}${ENTITY_TYPE}.go"
        cp -fi "${TEMPLATE_PARTICIPANT_DIR}/SampleParticipant_test.go" "${BIZNET_SRC_DIR}/${PARTICIPANT_NAME}${ENTITY_TYPE}/src/${PARTICIPANT_NAME}${ENTITY_TYPE}_test.go"
        cp -fi "${TEMPLATE_PARTICIPANT_DIR}/gjson.go" "${BIZNET_SRC_DIR}/${PARTICIPANT_NAME}${ENTITY_TYPE}/src/gjson.go"
        cp -fi "${TEMPLATE_PARTICIPANT_DIR}/gjson_test.go" "${BIZNET_SRC_DIR}/${PARTICIPANT_NAME}${ENTITY_TYPE}/src/gjson_test.go"
        cp -fi "${TEMPLATE_PARTICIPANT_DIR}/match.go" "${BIZNET_SRC_DIR}/${PARTICIPANT_NAME}${ENTITY_TYPE}/src/match.go"
        cp -fi "${TEMPLATE_PARTICIPANT_DIR}/match_test.go" "${BIZNET_SRC_DIR}/${PARTICIPANT_NAME}${ENTITY_TYPE}/src/match_test.go"
        cp -fi "${TEMPLATE_PARTICIPANT_DIR}/SampleParticipant.yaml" "${BIZNET_SRC_DIR}/${PARTICIPANT_NAME}${ENTITY_TYPE}/src/${PARTICIPANT_NAME}${ENTITY_TYPE}.yaml"
        cp -fi "${TEMPLATE_PARTICIPANT_DIR}/chaincode.yaml" "${BIZNET_SRC_DIR}/${PARTICIPANT_NAME}${ENTITY_TYPE}/chaincode.yaml"
#       Enhance the code artifacts (.go, _test.go and .yaml, test, deployment and ui etc.) for this entity
        bash ${TEMPLATE_ROOT_DIR}/enhanceParticipantStructure.sh \
          "${BIZNET_SRC_DIR}/${PARTICIPANT_NAME}${ENTITY_TYPE}/src" \
          "${BIZNET_SRC_DIR}/${PARTICIPANT_NAME}${ENTITY_TYPE}/src/${PARTICIPANT_NAME}${ENTITY_TYPE}.go" \
          "${BIZNET_SRC_DIR}/${PARTICIPANT_NAME}${ENTITY_TYPE}/src/${PARTICIPANT_NAME}${ENTITY_TYPE}_test.go" \
          "${BIZNET_SRC_DIR}/${PARTICIPANT_NAME}${ENTITY_TYPE}/src/${PARTICIPANT_NAME}${ENTITY_TYPE}.yaml"  \
          "${BIZNET_TEST_DIR}" \
          "${PARTICIPANT_NAME}" \
          "${PARTICIPANT_NAME_LOWER}" \
          "${ENTITY_TYPE}" \
          "${TEMPLATE_TEST_DIR}" \
          "${TEMPLATE_UI_DIR}" \
          "${BIZNET_UI_DIR}" \
          "${BIZNET_FOLDER_NAME}" \
          "${TEMPLATE_ROOT_DIR}" \
          && echo -e "${GREEN}Modeling ${ENTITY_TYPE}: ${PARTICIPANT_NAME}${ENTITY_TYPE} Done!${NC}"
#       Enhance the deployment descriptor for this entity
        bash ${TEMPLATE_ROOT_DIR}/enhanceEntityDeployment.sh \
          "${BIZNET_DEPLOY_DIR}" \
          "${PARTICIPANT_NAME}" \
          "${PARTICIPANT_NAME_LOWER}" \
          "${ENTITY_TYPE}" \
          "${IS_FIRST}" \
          && echo -e "${GREEN}Deployment descriptor ${ENTITY_TYPE}: ${PARTICIPANT_NAME}${ENTITY_TYPE} Done!${NC}"
#       Enhance the test collections / scripts for this entity
        bash ${TEMPLATE_ROOT_DIR}/enhanceEntityTest.sh \
          "${TEMPLATE_TEST_DIR}" \
          "${BIZNET_FOLDER_NAME}" \
          "${BIZNET_TEST_DIR}" \
          "${PARTICIPANT_NAME}" \
          "${ENTITY_TYPE}" \
          && echo -e "${GREEN}Postman environment & collections ${ENTITY_TYPE}: ${PARTICIPANT_NAME}${ENTITY_TYPE}${NC}"
#       Replace variables - .go file
        sed -i.bak 's/SampleParticipant/'${PARTICIPANT_NAME}${ENTITY_TYPE}'/g' "${BIZNET_SRC_DIR}/${PARTICIPANT_NAME}${ENTITY_TYPE}/src/${PARTICIPANT_NAME}${ENTITY_TYPE}.go"
        sed -i.bak 's/Sample/'${PARTICIPANT_NAME}'/g' "${BIZNET_SRC_DIR}/${PARTICIPANT_NAME}${ENTITY_TYPE}/src/${PARTICIPANT_NAME}${ENTITY_TYPE}.go"
        sed -i.bak 's/sample/'${PARTICIPANT_NAME_LOWER}'/g' "${BIZNET_SRC_DIR}/${PARTICIPANT_NAME}${ENTITY_TYPE}/src/${PARTICIPANT_NAME}${ENTITY_TYPE}.go"
        sed -i.bak 's/smpl/'${PARTICIPANT_ABBR}'/g' "${BIZNET_SRC_DIR}/${PARTICIPANT_NAME}${ENTITY_TYPE}/src/${PARTICIPANT_NAME}${ENTITY_TYPE}.go"
#       Replace variables - _test.go file
        sed -i.bak 's/SampleParticipant/'${PARTICIPANT_NAME}${ENTITY_TYPE}'/g' "${BIZNET_SRC_DIR}/${PARTICIPANT_NAME}${ENTITY_TYPE}/src/${PARTICIPANT_NAME}${ENTITY_TYPE}_test.go"
        sed -i.bak 's/Sample/'${PARTICIPANT_NAME}'/g' "${BIZNET_SRC_DIR}/${PARTICIPANT_NAME}${ENTITY_TYPE}/src/${PARTICIPANT_NAME}${ENTITY_TYPE}_test.go"
        sed -i.bak 's/sample/'${PARTICIPANT_NAME_LOWER}'/g' "${BIZNET_SRC_DIR}/${PARTICIPANT_NAME}${ENTITY_TYPE}/src/${PARTICIPANT_NAME}${ENTITY_TYPE}_test.go"
#       Replace variables - openAPI.yaml file
        sed -i.bak 's/SampleParticipant/'${PARTICIPANT_NAME}${ENTITY_TYPE}'/g' "${BIZNET_SRC_DIR}/${PARTICIPANT_NAME}${ENTITY_TYPE}/src/${PARTICIPANT_NAME}${ENTITY_TYPE}.yaml"
        sed -i.bak 's/Sample/'${PARTICIPANT_NAME}'/g' "${BIZNET_SRC_DIR}/${PARTICIPANT_NAME}${ENTITY_TYPE}/src/${PARTICIPANT_NAME}${ENTITY_TYPE}.yaml"
        sed -i.bak 's/sampleParticipant/'${PARTICIPANT_NAME_LOWER}${ENTITY_TYPE}'/g' "${BIZNET_SRC_DIR}/${PARTICIPANT_NAME}${ENTITY_TYPE}/src/${PARTICIPANT_NAME}${ENTITY_TYPE}.yaml"
#       Replace variables - chaincode.yaml file
        sed -i.bak 's/sampleParticipant/'${PARTICIPANT_NAME_LOWER}${ENTITY_TYPE}'/g' "${BIZNET_SRC_DIR}/${PARTICIPANT_NAME}${ENTITY_TYPE}/chaincode.yaml"
        sed -i.bak 's/sample-net/'$BIZNET_FOLDER_NAME'/g' "${BIZNET_SRC_DIR}/${PARTICIPANT_NAME}${ENTITY_TYPE}/chaincode.yaml"
#       Delete backup files
        cd "${BIZNET_SRC_DIR}/${PARTICIPANT_NAME}${ENTITY_TYPE}"
        find . -name "*.bak" -type f -delete
#       Zip file for chaincode deployment
        zip -rq "${BIZNET_SRC_DIR}/${PARTICIPANT_NAME}${ENTITY_TYPE}/${PARTICIPANT_NAME}${ENTITY_TYPE}.zip" ./*
        IS_FIRST=n
        break;;

        * )
        echo "Invalid Option"
        break;;
    esac
  done
  echo
  echo -e "${CYAN}Do you want to create more entities in your network? [${RED}y/n${CYAN}]: ${NC}"
  read MORE_ENTITIES
  echo
  # MORE_ENTITIES=$REPEAT
  if [[ "${MORE_ENTITIES}" == "n" ]]; then
    break
  fi
done

bash ${TEMPLATE_ROOT_DIR}/finalizeDeployment.sh "${BIZNET_DEPLOY_DIR}"
echo -e "${CYAN}Deployment descriptor finalization - Done!${NC}"
bash ${TEMPLATE_ROOT_DIR}/finalizeTest.sh "${BIZNET_FOLDER_NAME}" "${BIZNET_TEST_DIR}" "${BIZNET_SCRIPTS_DIR}"
echo -e "${CYAN}Postman environment finalization - Done!${NC}"
bash ${TEMPLATE_ROOT_DIR}/finalizeSetupUI.sh "${TEMPLATE_UI_DIR}" "${BIZNET_UI_DIR}" "${BIZNET_FOLDER_NAME}"
echo -e "${CYAN}SetupUI finalization - Done!${NC}"

#Proceed to deploy on blockchain environment?
DEPLOY_CHAINCODE="y"
while [[ $DEPLOY_CHAINCODE == "y" ]]; do
  echo
  echo -e "${ORANGE}Deploy ALL entities of your business network on the specified SAP BaaS environment? [${RED}y/n${ORANGE}]: ${NC}"
  read DEPLOY_CHAINCODE
  echo
  if [[ "${DEPLOY_CHAINCODE}" == "n" ]]; then
    break
  fi
  if [[ "${DEPLOY_CHAINCODE}" == "y" ]]; then
    cd ${BIZNET_SCRIPTS_DIR}
    bash ${BIZNET_SCRIPTS_DIR}/deployToHLFaaS.sh
    break
  fi
done

#Run newman?
RUN_NEWMAN="y"
while [[ $RUN_NEWMAN == "y" ]]; do
  echo
  echo -e "${LIGHT_GREEN}Do you want to run newman to populate sample data in your network? [${RED}y/n${LIGHT_GREEN}]: ${NC}"
  read RUN_NEWMAN
  echo
  if [[ "${RUN_NEWMAN}" == "n" ]]; then
    break
  fi
  if [[ "${RUN_NEWMAN}" == "y" ]]; then
    cd ${BIZNET_SCRIPTS_DIR}
    bash ${BIZNET_SCRIPTS_DIR}/runCleanAllAndSetup.sh
    break
  fi
done

#TODO: Push cfapp for setting up your entities?
DEPLOY_CF="y"
while [[ $DEPLOY_CF == "y" ]]; do
  echo
  echo -e "${LIGHT_CYAN}Do you want to deploy the genrated UI5 application on the Cloud Foundry Space and bind to channel service? [${RED}y/n${LIGHT_CYAN}]: ${NC}"
  read DEPLOY_CF
  echo
  if [[ "${DEPLOY_CF}" == "n" ]]; then
    break
  fi
  if [[ "${DEPLOY_CF}" == "y" ]]; then
    cd ${BIZNET_SCRPITS_DIR}
    bash ${BIZNET_SCRIPTS_DIR}/deployCFUI.sh
    break
  fi
done

#Zip the project contents - to enable import to Web index
cd "${BIZNET_UI_DIR}/${BIZNET_FOLDER_NAME}Setup"
zip -rq "${BIZNET_UI_DIR}/${BIZNET_FOLDER_NAME}Setup.zip" ./*
echo
echo -e "${GREEN}ALL Done! Ready to ROCK?...Goodbye! ;-)"
exit 0
