#!/bin/bash

cd <BIZNET_SETUP_UI_DIR>
echo
echo -e "${LIGHT_PURPLE}Installing dependencies for node.js buildpack...Please enter your ${RED}super user (sudo)${LIGHT_PURPLE} password...${NC}"
sudo npm install --save
sudo npm audit fix
echo
cf login -a <API_ENDPOINT>
cf push
