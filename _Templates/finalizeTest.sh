#!/bin/bash

#Colors
DARK_GRAY='\033[1;30m'
RED='\033[0;31m'
GREEN='\033[0;32m'
ORANGE='\033[0;33m'
BLUE='\033[0;34m'
PURPLE='\033[0;35m'
CYAN='\033[0;36m'
LIGHT_GRAY='\033[0;36m'
LIGHT_RED='\033[1;31m'
LIGHT_GREEN='\033[1;32m'
YELLOW='\033[1;33m'
LIGHT_BLUE='\033[1;34m'
LIGHT_PURPLE='\033[1;35m'
LIGHT_CYAN='\033[1;36m'
WHITE='\033[1;37m'
NC='\033[0m'

BIZNET_FOLDER_NAME=${1}
BIZNET_TEST_DIR=${2}
BIZNET_SCRIPTS_DIR=${3}

#Close environment.json file
echo "  ],
  \"_postman_variable_scope\": \"environment\",
  \"_postman_exported_at\": \"2018-07-13T08:25:14.314Z\",
  \"_postman_exported_using\": \"Postman/6.1.4\"
}" >> ${BIZNET_TEST_DIR}/newman/CleanAllAndSetup/entityEnv.tbak

#Close run script
echo "
echo \"Done. Ready to run!\"" >> ${BIZNET_TEST_DIR}/newman/CleanAllAndSetup/entityRunScript.tbak

#Append the contents of test environment and script backup files target test environment and script files
cat ${BIZNET_TEST_DIR}/newman/CleanAllAndSetup/entityEnv.tbak >> ${BIZNET_TEST_DIR}/newman/CleanAllAndSetup/${BIZNET_FOLDER_NAME}.postman_environment.json
cat ${BIZNET_TEST_DIR}/newman/CleanAllAndSetup/entityRunScript.tbak >> ${BIZNET_SCRIPTS_DIR}/runCleanAllAndSetup.sh

#Check and delete previous states of configuration files
cd "${BIZNET_TEST_DIR}/newman/CleanAllAndSetup"
find . -name "*.tbak" -type f -delete

exit 0
