#!/bin/bash

#Colors
DARK_GRAY='\033[1;30m'
RED='\033[0;31m'
GREEN='\033[0;32m'
ORANGE='\033[0;33m'
BLUE='\033[0;34m'
PURPLE='\033[0;35m'
CYAN='\033[0;36m'
LIGHT_GRAY='\033[0;36m'
LIGHT_RED='\033[1;31m'
LIGHT_GREEN='\033[1;32m'
YELLOW='\033[1;33m'
LIGHT_BLUE='\033[1;34m'
LIGHT_PURPLE='\033[1;35m'
LIGHT_CYAN='\033[1;36m'
WHITE='\033[1;37m'
NC='\033[0m'

TEMPLATE_TEST_DIR=${1}
BIZNET_TEST_DIR=${2}
BIZNET_FOLDER_NAME=${3}
BIZNET_DIR=${4}

#Check and delete previous states of configuration files
cd "${BIZNET_TEST_DIR}/newman/CleanAllAndSetup"
find . -name "*.tbak" -type f -delete

#Copy environment, collection and script files to business network
cp -fi "${TEMPLATE_TEST_DIR}/newman/CleanAllAndSetup/sample-net-env.postman_environment.json" "${BIZNET_TEST_DIR}/newman/CleanAllAndSetup/${BIZNET_FOLDER_NAME}.postman_environment.json"

#Replace variables
sed -i.tbak 's/sample-net-env/'${BIZNET_FOLDER_NAME}'/g' "${BIZNET_TEST_DIR}/newman/CleanAllAndSetup/${BIZNET_FOLDER_NAME}.postman_environment.json"

exit 0
