#!/bin/bash

BIZNET_MDJ_FILE=${1}
BIZNET_MODEL_DIR=${2}
BIZNET_FOLDER_NAME=${3}

# Templates Section
TMP_MODEL_FILE_START="{\n"
TMP_ENTITIES_START="\n\t\\\"entities\\\":"
TMP_MODEL_FILE_END="\n}"

cd ${BIZNET_MODEL_DIR}

# Remove existing simplified JSON if any
if [ -f "${BIZNET_MODEL_DIR}/${BIZNET_FOLDER_NAME}.json" ]; then
  rm -fi "${BIZNET_MODEL_DIR}/${BIZNET_FOLDER_NAME}.json"
fi

# Somehow, jq does not work without this
export PATH=$PATH:/usr/local/bin

# Extract network name to the target file
printf ${TMP_MODEL_FILE_START} >> ${BIZNET_MODEL_DIR}/${BIZNET_FOLDER_NAME}.json
MDL_NETWORK_NAME=`cat ${BIZNET_MDJ_FILE} | jq 'if .ownedElements[0]._type == "UMLModel" then .ownedElements[0].ownedElements[0].name else "UMLModel not found" end'`
printf "\t\\\"networkName\\\":${MDL_NETWORK_NAME},">> ${BIZNET_MODEL_DIR}/${BIZNET_FOLDER_NAME}.json

# Extract the entities
printf ${TMP_ENTITIES_START} >> ${BIZNET_MODEL_DIR}/${BIZNET_FOLDER_NAME}.json
cat ${BIZNET_MDJ_FILE} | jq '[.ownedElements[] | select(._type == "UMLModel") | .ownedElements[] | select(._type == "UMLClass") | del(._type, ._id, ._parent, .ownedElements, .operations, .attributes[]._type, .attributes[]._id, .attributes[]._parent, .attributes[].type, .attributes[].isReadOnly, .attributes[].isUnique, .attributes[].isID, .attributes[].multiplicity,  .attributes[].defaultValue)]' >> ${BIZNET_FOLDER_NAME}.json

# End model file write
printf ${TMP_MODEL_FILE_END} >> ${BIZNET_MODEL_DIR}/${BIZNET_FOLDER_NAME}.json
