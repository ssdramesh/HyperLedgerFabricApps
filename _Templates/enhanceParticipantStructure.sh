#!/bin/bash

#Colors
DARK_GRAY='\033[1;30m'
RED='\033[0;31m'
GREEN='\033[0;32m'
ORANGE='\033[0;33m'
BLUE='\033[0;34m'
PURPLE='\033[0;35m'
CYAN='\033[0;36m'
LIGHT_GRAY='\033[0;36m'
LIGHT_RED='\033[1;31m'
LIGHT_GREEN='\033[1;32m'
YELLOW='\033[1;33m'
LIGHT_BLUE='\033[1;34m'
LIGHT_PURPLE='\033[1;35m'
LIGHT_CYAN='\033[1;36m'
WHITE='\033[1;37m'
NC='\033[0m'

#Capture file and folder paths
TARGET_DIR=${1}
TARGET_CHAINCODE=${2}
TARGET_CHAINCODE_TEST=${3}
TARGET_API_DEFINITION=${4}
BIZNET_TEST_DIR=${5}
PARTICIPANT_NAME=${6}
PARTICIPANT_NAME_LOWER=${7}
ENTITY_TYPE=${8}
TEMPLATE_TEST_DIR=${9}
TEMPLATE_UI_DIR=${10}
BIZNET_UI_DIR=${11}
BIZNET_FOLDER_NAME=${12}
TEMPLATE_ROOT_DIR=${13}

TEMPLATE_GO_TYPE="type Sample struct {\n ID          string \`json:\"ID\"\`\n ObjectType   string \`json:\"docType\"\`\n  Alias       string \`json:\"alias\"\`\n  Description string \`json:\"description\"\`"
TEMPLATE_GO_TEST_1="func getFirst${PARTICIPANT_NAME}${ENTITY_TYPE}ForTesting() [][]byte { \n return [][]byte{[]byte(\"addNewSample\"), \n []byte(\"{\\\"ID\\\":\\\"100001\\\",\\\"docType\\\":\\\"Participant.${PARTICIPANT_NAME}${ENTITY_TYPE}\\\",\\\"alias\\\":\\\"BRITECH\\\",\\\"description\\\":\\\"British Technology Pvt. Ltd.\\\""
TEMPLATE_GO_TEST_1U="func getFirst${PARTICIPANT_NAME}${ENTITY_TYPE}ForUpdateTestingOK() [][]byte { \n return [][]byte{[]byte(\"updateSample\"), \n []byte(\"{\\\"ID\\\":\\\"100001\\\",\\\"docType\\\":\\\"Participant.${PARTICIPANT_NAME}${ENTITY_TYPE}\\\",\\\"alias\\\":\\\"TECHBRI\\\",\\\"description\\\":\\\"British Technology\\\""
TEMPLATE_GO_TEST_2="func getSecond${PARTICIPANT_NAME}${ENTITY_TYPE}ForTesting() [][]byte { \n return [][]byte{[]byte(\"addNewSample\"), \n []byte(\"{\\\"ID\\\":\\\"100002\\\",\\\"docType\\\":\\\"Participant.${PARTICIPANT_NAME}${ENTITY_TYPE}\\\",\\\"alias\\\":\\\"DEMAGDELAG\\\",\\\"description\\\":\\\"Demag Delewal AG\\\""
TEMPLATE_GO_TEST_STRUCT_1="\tsample.ID = \"100001\"\n\tsample.ObjectType = \"Participant.${PARTICIPANT_NAME}${ENTITY_TYPE}\"\n\tsample.Alias = \"BRITECH\"\n\tsample.Description = \"British Technology Pvt. Ltd.\""
TEMPLATE_GO_TEST_STRUCT_1U="func getUpdated${PARTICIPANT_NAME}Expected() []byte {\n\tvar sample Sample\n\tsample.ID = \"100001\"\n	sample.ObjectType = \"Participant.${PARTICIPANT_NAME}${ENTITY_TYPE}\"\n  sample.Alias = \"TECHBRI\"\n	sample.Description = \"British Technology\""
TEMPLATE_GO_TEST_STRUCT_2="\tsample.ID = \"100002\"\n\tsample.ObjectType = \"Participant.${PARTICIPANT_NAME}${ENTITY_TYPE}\"\n\tsample.Alias = \"DEMAGDELAG\"\n\tsample.Description = \"Demag Delewal AG\""
TEMPLATE_GO_QRY_STRING_START="queryString := fmt.Sprintf(\"{\\\\\"selector\\\\\": {\\\\\"ID\\\\\": {\\\\\"\$regex\\\\\": \\\\\"%s\\\\\"}}, \\\\\"fields\\\\\": [\\\\\"ID\\\\\", \\\\\"docType\\\\\", \\\\\"alias\\\\\", \\\\\"description\\\\\""
TEMPLATE_GO_QRY_STRING_END="], \\\\\"limit\\\\\":99, \\\\\"execution_stats\\\\\": true}\", strings.Replace(searchString, \"\\\\\"\", \".\", -1))"
TEMPLATE_JSON_SAMPLE="[\n\t{\n\t\t\"ID\":\"${PARTICIPANT_NAME}${ENTITY_TYPE}1001\",\n\t\t\"docType\":\"Participant.${PARTICIPANT_NAME}${ENTITY_TYPE}\",\n\t\t\"alias\":\"BRITECH\",\n\t\t\"description\":\"British Technology Pvt. Ltd.\""
TEMPLATE_API_DEFINITION="definitions:\n  ${PARTICIPANT_NAME_LOWER}${ENTITY_TYPE}:\n    type: object\n    properties:\n      ID:\n        type: string\n      docType:\n        type: string\n      alias:\n        type: string\n      description:\n        type: string"
TEMPLATE_PARTICIPANT_ROUTES=", {
\t\t\t\t\"pattern\": \"${PARTICIPANT_NAME}${ENTITY_TYPE}s\",
\t\t\t\t\"name\": \"${PARTICIPANT_NAME_LOWER}${ENTITY_TYPE}s\",
\t\t\t\t\"target\": [\"t${PARTICIPANT_NAME}${ENTITY_TYPE}sRoot\", \"t${PARTICIPANT_NAME}${ENTITY_TYPE}Details\", \"t${PARTICIPANT_NAME}${ENTITY_TYPE}sOverview\"]
\t\t\t}, {
\t\t\t\t\"pattern\": \"${PARTICIPANT_NAME}${ENTITY_TYPE}s/{${PARTICIPANT_NAME_LOWER}${ENTITY_TYPE}Id}\",
\t\t\t\t\"name\": \"${PARTICIPANT_NAME_LOWER}${ENTITY_TYPE}\",
\t\t\t\t\"target\": [\"t${PARTICIPANT_NAME}${ENTITY_TYPE}sOverview\", \"t${PARTICIPANT_NAME}${ENTITY_TYPE}Details\"]
\t\t\t}, {
\t\t\t\t\"pattern\": \"${PARTICIPANT_NAME}${ENTITY_TYPE}All\",
\t\t\t\t\"name\": \"${PARTICIPANT_NAME_LOWER}${ENTITY_TYPE}s\",
\t\t\t\t\"target\": [\"t${PARTICIPANT_NAME}${ENTITY_TYPE}Details\", \"t${PARTICIPANT_NAME}${ENTITY_TYPE}sOverview\"]
\t\t\t}"
TEMPLATE_PARTICIPANT_TARGETS=",
\t\t\t\t\"t${PARTICIPANT_NAME}${ENTITY_TYPE}sRoot\": {
\t\t\t\t\t\"viewName\": \"${PARTICIPANT_NAME}${ENTITY_TYPE}sRoot\",
\t\t\t\t\t\"viewLevel\": 1,
\t\t\t\t\t\"controlId\": \"idAppControl\",
\t\t\t\t\t\"controlAggregation\": \"pages\",
\t\t\t\t\t\"title\": \"${PARTICIPANT_NAME}${ENTITY_TYPE}s\"
\t\t\t\t},
\t\t\t\t\"t${PARTICIPANT_NAME}${ENTITY_TYPE}sOverview\": {
\t\t\t\t\t\"parent\": \"t${PARTICIPANT_NAME}${ENTITY_TYPE}sRoot\",
\t\t\t\t\t\"viewName\": \"${PARTICIPANT_NAME}${ENTITY_TYPE}sOverview\",
\t\t\t\t\t\"viewLevel\": 2,
\t\t\t\t\t\"controlId\": \"idAppControl\",
\t\t\t\t\t\"controlAggregation\": \"masterPages\",
\t\t\t\t\t\"title\": \"${PARTICIPANT_NAME}${ENTITY_TYPE}sOverview\"
\t\t\t\t},
\t\t\t\t\t\"t${PARTICIPANT_NAME}${ENTITY_TYPE}Details\": {
\t\t\t\t\t\"parent\": \"t${PARTICIPANT_NAME}${ENTITY_TYPE}sRoot\",
\t\t\t\t\t\"viewName\": \"${PARTICIPANT_NAME}${ENTITY_TYPE}Details\",
\t\t\t\t\t\"viewLevel\": 3,
\t\t\t\t\t\"controlId\": \"idAppControl\",
\t\t\t\t\t\"controlAggregation\": \"detailPages\",
\t\t\t\t\t\"title\": \"${PARTICIPANT_NAME}${ENTITY_TYPE}Details\"
\t\t\t\t}"
TEMPLATE_PARTICIPANT_DETAILS_VIEW_SEL="
                                      <sap.ui.layout.form:FormElement id=\"__elementSel${PARTICIPANT_NAME}${ENTITY_TYPE}ID\" label=\"{i18n>_label${PARTICIPANT_NAME}${ENTITY_TYPE}ID}\">
                                        <sap.ui.layout.form:fields>
                                          <Input width=\"100%%\" id=\"__inputSel${PARTICIPANT_NAME}${ENTITY_TYPE}ID\" value=\"{${PARTICIPANT_NAME}${ENTITY_TYPE}s>/selected${PARTICIPANT_NAME}${ENTITY_TYPE}/ID}\" editable=\"false\"/>
                                        </sap.ui.layout.form:fields>
                                      </sap.ui.layout.form:FormElement>
                                      <sap.ui.layout.form:FormElement id=\"__elementSel${PARTICIPANT_NAME}${ENTITY_TYPE}ObjectType\" label=\"{i18n>_label${PARTICIPANT_NAME}${ENTITY_TYPE}ObjectType}\">
                                        <sap.ui.layout.form:fields>
                                          <Input width=\"100%%\" id=\"__inputSel${PARTICIPANT_NAME}${ENTITY_TYPE}ObjectType\" value=\"{${PARTICIPANT_NAME}${ENTITY_TYPE}s>/selected${PARTICIPANT_NAME}${ENTITY_TYPE}/ObjectType}\" editable=\"false\"/>
                                        </sap.ui.layout.form:fields>
                                      </sap.ui.layout.form:FormElement>
                                      <sap.ui.layout.form:FormElement id=\"__elementSel${PARTICIPANT_NAME}${ENTITY_TYPE}Alias\" label=\"{i18n>_label${PARTICIPANT_NAME}${ENTITY_TYPE}Alias}\">
                                        <sap.ui.layout.form:fields>
                                          <Input width=\"100%%\" id=\"__inputSel${PARTICIPANT_NAME}${ENTITY_TYPE}Alias\" value=\"{${PARTICIPANT_NAME}${ENTITY_TYPE}s>/selected${PARTICIPANT_NAME}${ENTITY_TYPE}/Alias}\" editable=\"false\"/>
                                        </sap.ui.layout.form:fields>
                                      </sap.ui.layout.form:FormElement>
                                      <sap.ui.layout.form:FormElement id=\"__elementSel${PARTICIPANT_NAME}${ENTITY_TYPE}Description\" label=\"{i18n>_label${PARTICIPANT_NAME}${ENTITY_TYPE}Description}\">
                                        <sap.ui.layout.form:fields>
                                          <Input width=\"100%%\" id=\"__inputSel${PARTICIPANT_NAME}${ENTITY_TYPE}Description\" value=\"{${PARTICIPANT_NAME}${ENTITY_TYPE}s>/selected${PARTICIPANT_NAME}${ENTITY_TYPE}/Description}\" editable=\"false\"/>
                                        </sap.ui.layout.form:fields>
                                      </sap.ui.layout.form:FormElement>
"

TEMPLATE_PARTICIPANT_DETAILS_VIEW_NEW="
                                      <sap.ui.layout.form:FormElement id=\"__elementNew${PARTICIPANT_NAME}${ENTITY_TYPE}ID\" label=\"{i18n>_label${PARTICIPANT_NAME}${ENTITY_TYPE}ID}\">
                                        <sap.ui.layout.form:fields>
                                          <Input width=\"100%%\" id=\"__inputNew${PARTICIPANT_NAME}${ENTITY_TYPE}ID\" value=\"{${PARTICIPANT_NAME}${ENTITY_TYPE}s>/new${PARTICIPANT_NAME}${ENTITY_TYPE}/ID}\" editable=\"true\" placeholder=\"{i18n>_labelIDPlaceholder}\"/>
                                        </sap.ui.layout.form:fields>
                                      </sap.ui.layout.form:FormElement>
                                      <sap.ui.layout.form:FormElement id=\"__elementNew${PARTICIPANT_NAME}${ENTITY_TYPE}Alias\" label=\"{i18n>_label${PARTICIPANT_NAME}${ENTITY_TYPE}Alias}\">
                                        <sap.ui.layout.form:fields>
                                          <Input width=\"100%%\" id=\"__inputNew${PARTICIPANT_NAME}${ENTITY_TYPE}Alias\" value=\"{${PARTICIPANT_NAME}${ENTITY_TYPE}s>/new${PARTICIPANT_NAME}${ENTITY_TYPE}/Alias}\" editable=\"true\"/>
                                        </sap.ui.layout.form:fields>
                                      </sap.ui.layout.form:FormElement>
                                      <sap.ui.layout.form:FormElement id=\"__elementNew${PARTICIPANT_NAME}${ENTITY_TYPE}Description\" label=\"{i18n>_label${PARTICIPANT_NAME}${ENTITY_TYPE}Description}\">
                                        <sap.ui.layout.form:fields>
                                          <Input width=\"100%%\" id=\"__inputNew${PARTICIPANT_NAME}${ENTITY_TYPE}Description\" value=\"{${PARTICIPANT_NAME}${ENTITY_TYPE}s>/new${PARTICIPANT_NAME}${ENTITY_TYPE}/Description}\" editable=\"true\"/>
                                        </sap.ui.layout.form:fields>
                                      </sap.ui.layout.form:FormElement>
"

TEMPLATE_ENTITY_TO_MODEL="
                Alias:responseData.alias,
                Description:responseData.description
"
TEMPLATE_ENTITY_TO_CHAINCODE_NEW="
                        \"alias\":oModel.getProperty(\"/new${PARTICIPANT_NAME}${ENTITY_TYPE}/Alias\"),
                        \"description\":oModel.getProperty(\"/new${PARTICIPANT_NAME}${ENTITY_TYPE}/Description\")
"
TEMPLATE_ENTITY_TO_CHAINCODE_SEL="
                        \"alias\":oModel.getProperty(\"/selected${PARTICIPANT_NAME}${ENTITY_TYPE}/Alias\"),
                        \"description\":oModel.getProperty(\"/selected${PARTICIPANT_NAME}${ENTITY_TYPE}/Description\")
"

TEMPLATE_PARTICIPANT_I18N="
#XTIT:
_title${PARTICIPANT_NAME}${ENTITY_TYPE}sList=${PARTICIPANT_NAME}s List

#XTIT:
_title${PARTICIPANT_NAME}${ENTITY_TYPE}Details=Details of ${PARTICIPANT_NAME}

#XTIT:
_objectHeaderTitle${PARTICIPANT_NAME}${ENTITY_TYPE}=${PARTICIPANT_NAME}

#XFLD:
_radioButton${PARTICIPANT_NAME}${ENTITY_TYPE}s=${PARTICIPANT_NAME}${ENTITY_TYPE}s

#XFLD:
_label${PARTICIPANT_NAME}${ENTITY_TYPE}Details=${PARTICIPANT_NAME} Details

#XFLD:
_labelNo${PARTICIPANT_NAME}${ENTITY_TYPE}sText=No ${PARTICIPANT_NAME}${ENTITY_TYPE}s

#XFLD:
_label${PARTICIPANT_NAME}${ENTITY_TYPE}Add=New

#XBUT:
_buttonAdd${PARTICIPANT_NAME}${ENTITY_TYPE}=Add

#XFLD:
_labelNo${PARTICIPANT_NAME}${ENTITY_TYPE}s=No ${PARTICIPANT_NAME}s

#XFLD:
_label${PARTICIPANT_NAME}${ENTITY_TYPE}ID=ID

#XFLD:
_label${PARTICIPANT_NAME}${ENTITY_TYPE}ObjectType=ObjectType

#XFLD:
_label${PARTICIPANT_NAME}${ENTITY_TYPE}Alias=Alias

#XFLD:
_label${PARTICIPANT_NAME}${ENTITY_TYPE}Description=Description
"

TEMPLATE_ENTITY_POSTMAN="{\\\\n\\\\t\\\\\"ID\\\\\":\\\\\"{{ID}}\\\\\",\\\\n\\\\t\\\\\"docType\\\\\":\\\\\"{{docType}}\\\\\",\\\\n\\\\t\\\\\"alias\\\\\":\\\\\"{{alias}}\\\\\",\\\\n\\\\t\\\\\"description\\\\\":\\\\\"{{description}}\\\\\""

#Prepare .go and _test.go files
#Type declaration
cd ${TARGET_DIR}
printf "${TEMPLATE_GO_TYPE}" >> participantStruct.sbak

#First sample for unit test
shopt -s xpg_echo
echo "${TEMPLATE_GO_TEST_1}\c" >> participantFirstTest.sbak

#Update sample for unit test
shopt -s xpg_echo
echo "${TEMPLATE_GO_TEST_1U}\c" >> participantFirstTestUpdateOK.sbak

#Second sample for unit test
shopt -s xpg_echo
echo "${TEMPLATE_GO_TEST_2}\c" >> participantSecondTest.sbak

#First sample unit test - expected result
printf "${TEMPLATE_GO_TEST_STRUCT_1}" >> participantFirstTestStruct.sbak

#Update sample unit test - expected result
printf "${TEMPLATE_GO_TEST_STRUCT_1U}" >> participantFirstTestUpdateOKStruct.sbak

#Second sample unit test - expected result
printf "${TEMPLATE_GO_TEST_STRUCT_2}" >> participantSecondTestStruct.sbak

#Wildcard search queryString
shopt -s xpg_echo
echo "${TEMPLATE_GO_QRY_STRING_START}\c" >> participantQueryString.sbak

#API Definition
shopt -s xpg_echo
echo "${TEMPLATE_API_DEFINITION}\c" >> entityAPIDefinition.sbak

#API Definition
printf "${TEMPLATE_JSON_SAMPLE}" >> ${BIZNET_TEST_DIR}/newman/CleanAllAndSetup/data/${PARTICIPANT_NAME}${ENTITY_TYPE}s.json

#Prepare postman collection
printf "${TEMPLATE_ENTITY_POSTMAN}" >> ${BIZNET_TEST_DIR}/newman/CleanAllAndSetup/entityCreateAllPostmanCollection.pbak

#Prepare routes for added entity
printf "${TEMPLATE_PARTICIPANT_ROUTES}" >> ${BIZNET_UI_DIR}/${BIZNET_FOLDER_NAME}Setup/webapp/entityRoutes.ubak
printf "${TEMPLATE_PARTICIPANT_TARGETS}" >> ${BIZNET_UI_DIR}/${BIZNET_FOLDER_NAME}Setup/webapp/entityTargets.ubak

#Prepare details view for the added entity
printf "${TEMPLATE_PARTICIPANT_DETAILS_VIEW_SEL}" >> ${BIZNET_UI_DIR}/${BIZNET_FOLDER_NAME}Setup/webapp/view/entityDetailsViewSelected.ubak
printf "${TEMPLATE_PARTICIPANT_DETAILS_VIEW_NEW}" >> ${BIZNET_UI_DIR}/${BIZNET_FOLDER_NAME}Setup/webapp/view/entityDetailsViewNew.ubak

#Prepare i18n for added entity
printf "${TEMPLATE_PARTICIPANT_I18N}" >> ${BIZNET_UI_DIR}/${BIZNET_FOLDER_NAME}Setup/webapp/i18n/entityi18nProperties.ubak

#Prepare replacements for formatter
printf "${TEMPLATE_ENTITY_TO_MODEL}" >> ${BIZNET_UI_DIR}/${BIZNET_FOLDER_NAME}Setup/webapp/util/replaceEntityToModel.ubak
printf "${TEMPLATE_ENTITY_TO_CHAINCODE_NEW}" >> ${BIZNET_UI_DIR}/${BIZNET_FOLDER_NAME}Setup/webapp/util/replaceEntityToChaincodeNew.ubak
printf "${TEMPLATE_ENTITY_TO_CHAINCODE_SEL}" >> ${BIZNET_UI_DIR}/${BIZNET_FOLDER_NAME}Setup/webapp/util/replaceEntityToChaincodeSelected.ubak

#Prepare i18n for added entity
printf "${TEMPLATE_PARTICIPANT_I18N}" >> ${BIZNET_UI_DIR}/${BIZNET_FOLDER_NAME}Setup/webapp/i18n/entityi18nProperties.ubak

#Attribute type selection
MORE_ATTRIBUTES=y
echo -e "${CYAN}Defining attributes of your entity...${NC}"
echo -e "${CYAN}NOTE: The following attributes are pre-defined for a Participant with type string:${NC}"
echo -e "${CYAN}ID           \`json:\"ID\"\`         ${NC}"
echo -e "${CYAN}ObjectType   \`json:\"docType\"\`    ${NC}"
echo -e "${CYAN}Alias        \`json:\"alias\"\`      ${NC}"
echo -e "${CYAN}Description  \`json:\"description\"\`${NC}"
echo -e "${CYAN}Do you want to add additional attributes to your entity? [${RED}y/n${CYAN}]: ${NC}${NC}"
read MORE_ATTRIBUTES

#Enhance Setup UI for the added entity
sh ${TEMPLATE_ROOT_DIR}/enhanceEntitySetupUI.sh \
  "${TEMPLATE_UI_DIR}" \
  "${BIZNET_UI_DIR}" \
  "${BIZNET_FOLDER_NAME}" \
  "${PARTICIPANT_NAME}" \
  "${PARTICIPANT_NAME_LOWER}" \
  "${ENTITY_TYPE}"

while [[ $MORE_ATTRIBUTES == y ]]; do
  echo -e "${CYAN}Enter attribute name [Use UpperCamelCase]: ${NC}"
  read ATTRIBUTE_NAME
  ATTRIBUTE_JSON_NAME=$(echo ${ATTRIBUTE_NAME:0:1} | tr '[A-Z]' '[a-z]')${ATTRIBUTE_NAME:1}
# Add structure attribute in the chaincode
  printf "\n${ATTRIBUTE_NAME} string \`json:\"${ATTRIBUTE_JSON_NAME}\"\`" >> participantStruct.sbak
# Add test data for new attribute for tests - (1/2)
  shopt -s xpg_echo
  echo ", \\\"${ATTRIBUTE_JSON_NAME}\\\":\\\"${ATTRIBUTE_JSON_NAME}001\\\"\c" >> participantFirstTest.sbak
  shopt -s xpg_echo
  echo ", \\\"${ATTRIBUTE_JSON_NAME}\\\":\\\"${ATTRIBUTE_JSON_NAME}001\\\"\c" >> participantFirstTestUpdateOK.sbak
  printf "\nsample.${ATTRIBUTE_NAME}=\"${ATTRIBUTE_JSON_NAME}001\"" >> participantFirstTestStruct.sbak
  printf "\nsample.${ATTRIBUTE_NAME}=\"${ATTRIBUTE_JSON_NAME}001\"" >> participantFirstTestUpdateOKStruct.sbak
# Add test data for new attribute for tests - (2/2)
  shopt -s xpg_echo
  echo ", \\\"${ATTRIBUTE_JSON_NAME}\\\":\\\"${ATTRIBUTE_JSON_NAME}002\\\"\c" >> participantSecondTest.sbak
  printf "\nsample.${ATTRIBUTE_NAME}=\"${ATTRIBUTE_JSON_NAME}002\"" >> participantSecondTestStruct.sbak
# Add attribute to the search function resultsIterator
  shopt -s xpg_echo
  echo ", \\\\\"${ATTRIBUTE_JSON_NAME}\\\\\":\\\\\"${ATTRIBUTE_JSON_NAME}001\\\\\"\c" >> participantQueryString.sbak
# Add attribute to openAPI definition
  printf "\n      ${ATTRIBUTE_JSON_NAME}:\n        type: string" >> entityAPIDefinition.sbak
# Add data sample in JSON data file
  printf ",\n\t\t\"${ATTRIBUTE_JSON_NAME}\":\"${ATTRIBUTE_JSON_NAME}001\"" >> ${BIZNET_TEST_DIR}/newman/CleanAllAndSetup/data/${PARTICIPANT_NAME}${ENTITY_TYPE}s.json
# Add attribute to postman collection
  printf ",\\\\n\\\\t\\\\\"${ATTRIBUTE_JSON_NAME}\\\\\":\\\\\"{{${ATTRIBUTE_JSON_NAME}}}\\\\\"" >> ${BIZNET_TEST_DIR}/newman/CleanAllAndSetup/entityCreateAllPostmanCollection.pbak
# Add attribute in UI formatter
  printf ",\n                ${ATTRIBUTE_NAME}:responseData.${ATTRIBUTE_JSON_NAME}" >> ${BIZNET_UI_DIR}/${BIZNET_FOLDER_NAME}Setup/webapp/util/replaceEntityToModel.ubak
  printf ",\n                  \"${ATTRIBUTE_JSON_NAME}\":oModel.getProperty(\"/new${PARTICIPANT_NAME}${ENTITY_TYPE}/${ATTRIBUTE_NAME}\")" >> ${BIZNET_UI_DIR}/${BIZNET_FOLDER_NAME}Setup/webapp/util/replaceEntityToChaincodeNew.ubak
  printf ",\n                  \"${ATTRIBUTE_JSON_NAME}\":oModel.getProperty(\"/selected${PARTICIPANT_NAME}${ENTITY_TYPE}/${ATTRIBUTE_NAME}\")" >> ${BIZNET_UI_DIR}/${BIZNET_FOLDER_NAME}Setup/webapp/util/replaceEntityToChaincodeSelected.ubak
# Add attribute in UI model
  printf ",\n            {name:\"${ATTRIBUTE_NAME}\"}" >> ${BIZNET_UI_DIR}/${BIZNET_FOLDER_NAME}Setup/webapp/model/replaceColNames.ubak
  printf ",\n          ${ATTRIBUTE_NAME}:\"\"" >> ${BIZNET_UI_DIR}/${BIZNET_FOLDER_NAME}Setup/webapp/model/replaceModelAttributes.ubak
# Add attribute in Views
  printf "
                                        <sap.ui.layout.form:FormElement id=\"__elementSel${PARTICIPANT_NAME}${ENTITY_TYPE}${ATTRIBUTE_NAME}\" label=\"{i18n>_label${PARTICIPANT_NAME}${ENTITY_TYPE}${ATTRIBUTE_NAME}}\">
  	                                      <sap.ui.layout.form:fields>
  	                                        <Input width=\"100%%\" id=\"__inputSel${PARTICIPANT_NAME}${ENTITY_TYPE}${ATTRIBUTE_NAME}\" value=\"{${PARTICIPANT_NAME}${ENTITY_TYPE}s>/selected${PARTICIPANT_NAME}${ENTITY_TYPE}/${ATTRIBUTE_NAME}}\" editable=\"false\"/>
  	                                      </sap.ui.layout.form:fields>
  	                                    </sap.ui.layout.form:FormElement>" >> ${BIZNET_UI_DIR}/${BIZNET_FOLDER_NAME}Setup/webapp/view/entityDetailsViewSelected.ubak
  printf "
                                        <sap.ui.layout.form:FormElement id=\"__elementNew${PARTICIPANT_NAME}${ENTITY_TYPE}${ATTRIBUTE_NAME}\" label=\"{i18n>_label${PARTICIPANT_NAME}${ENTITY_TYPE}${ATTRIBUTE_NAME}}\">
                                          <sap.ui.layout.form:fields>
                                            <Input width=\"100%%\" id=\"__inputNew${PARTICIPANT_NAME}${ENTITY_TYPE}${ATTRIBUTE_NAME}\" value=\"{${PARTICIPANT_NAME}${ENTITY_TYPE}s>/new${PARTICIPANT_NAME}${ENTITY_TYPE}/${ATTRIBUTE_NAME}}\" editable=\"true\"/>
                                          </sap.ui.layout.form:fields>
                                        </sap.ui.layout.form:FormElement>" >> ${BIZNET_UI_DIR}/${BIZNET_FOLDER_NAME}Setup/webapp/view/entityDetailsViewNew.ubak
#Add i18n
  printf "
#XFLD:
_label${PARTICIPANT_NAME}${ENTITY_TYPE}${ATTRIBUTE_NAME}=${ATTRIBUTE_NAME}" >> ${BIZNET_UI_DIR}/${BIZNET_FOLDER_NAME}Setup/webapp/i18n/entityi18nProperties.ubak
# Check for more attributes
  echo -e "${CYAN}Do you want to create more attributes in your entity? [${NC}y${CYAN}/${NC}n${CYAN}]: ${NC}"
  read MORE_ATTRIBUTES
  if [[ "${MORE_ATTRIBUTES}" == "n" ]]; then
    break
  fi
done

printf "\n\n  sampleParticipantHistory:" >> entityAPIDefinition.sbak
printf "\n\t}\n]" >> ${BIZNET_TEST_DIR}/newman/CleanAllAndSetup/data/${PARTICIPANT_NAME}${ENTITY_TYPE}s.json
printf "\\\\n}" >> ${BIZNET_TEST_DIR}/newman/CleanAllAndSetup/entityCreateAllPostmanCollection.pbak
printf "\n}\n" >> participantStruct.sbak
printf "}\")}\n}" >> participantFirstTest.sbak
printf "}\")}\n}" >> participantFirstTestUpdateOK.sbak
printf "}\")}\n}" >> participantSecondTest.sbak
printf "${TEMPLATE_GO_QRY_STRING_END}" >> participantQueryString.sbak

#Replace new structure to chaincode file
perl -i -p0e 's/type Sample struct.*?}\n/`cat participantStruct.sbak`/se' ${TARGET_CHAINCODE}
#Replace new structure in the queryResults structure
perl -i -p0e 's/queryString :=.*?<END_OF_QRY_STRING>/`cat participantQueryString.sbak`/se' ${TARGET_CHAINCODE}

perl -i -p0e 's/func getFirstSampleParticipantForTesting.*?}\n}/`cat participantFirstTest.sbak`/se' ${TARGET_CHAINCODE_TEST}
perl -i -p0e 's/func getFirstSampleParticipantForUpdateTestingOK.*?}\n}/`cat participantFirstTestUpdateOK.sbak`/se' ${TARGET_CHAINCODE_TEST}
#Replace the sample GO structures for expected test result values
perl -i -p0e 's/func getSecondSampleParticipantForTesting.*?}\n}/`cat participantSecondTest.sbak`/se' ${TARGET_CHAINCODE_TEST}
#Replace updated JSON test data to chaincode test file
perl -i -p0e 's/sample.ID = \"100001\".*?Ltd.\"/`cat participantFirstTestStruct.sbak`/seg' ${TARGET_CHAINCODE_TEST}
#Replace updated sample GO structures for expected test results in the chaincode test file
perl -i -p0e 's/sample.ID = \"100002\".*?AG\"/`cat participantSecondTestStruct.sbak`/seg' ${TARGET_CHAINCODE_TEST}
#Replace updated sample GO structure for expected test results after update
perl -i -p0e 's/func getUpdatedSampleExpected.*?Ltd.\"/`cat participantFirstTestUpdateOKStruct.sbak`/se' ${TARGET_CHAINCODE_TEST}
#Replace updated entity definition in the openAPI.yaml file
perl -i -p0e 's/definitions:.*?sampleParticipantHistory:/`cat entityAPIDefinition.sbak`/se' ${TARGET_API_DEFINITION}

#Copy the test JSON to UI localStore
cd ${BIZNET_UI_DIR}/${BIZNET_FOLDER_NAME}Setup/webapp/util
cat "${BIZNET_TEST_DIR}/newman/CleanAllAndSetup/data/${PARTICIPANT_NAME}${ENTITY_TYPE}s.json" >> localStoreSamples.ubak
perl -i -p0e 's/<BEGIN_REPLACE_JSON>.*?<END_REPLACE_JSON>/`cat localStoreSamples.ubak`/seg' ${BIZNET_UI_DIR}/${BIZNET_FOLDER_NAME}Setup/webapp/util/localStore${PARTICIPANT_NAME}${ENTITY_TYPE}s.js

#Update the Entity formatter
cd ${BIZNET_UI_DIR}/${BIZNET_FOLDER_NAME}Setup/webapp/util
perl -i -p0e 's/<BEGIN_REPLACE_ENTITY_TO_MODEL>.*?<END_REPLACE_ENTITY_TO_MODEL>/`cat replaceEntityToModel.ubak`/se' ${BIZNET_UI_DIR}/${BIZNET_FOLDER_NAME}Setup/webapp/util/formatter${PARTICIPANT_NAME}${ENTITY_TYPE}s.js
perl -i -p0e 's/<BEGIN_REPLACE_ENTITY_TO_CHAINCODE_NEW>.*?<END_REPLACE_ENTITY_TO_CHAINCODE_NEW>/`cat replaceEntityToChaincodeNew.ubak`/se' ${BIZNET_UI_DIR}/${BIZNET_FOLDER_NAME}Setup/webapp/util/formatter${PARTICIPANT_NAME}${ENTITY_TYPE}s.js
perl -i -p0e 's/<BEGIN_REPLACE_ENTITY_TO_CHAINCODE_SEL>.*?<END_REPLACE_ENTITY_TO_CHAINCODE_SEL>/`cat replaceEntityToChaincodeSelected.ubak`/se' ${BIZNET_UI_DIR}/${BIZNET_FOLDER_NAME}Setup/webapp/util/formatter${PARTICIPANT_NAME}${ENTITY_TYPE}s.js

#Update the Entity model
cd ${BIZNET_UI_DIR}/${BIZNET_FOLDER_NAME}Setup/webapp/model
perl -i -p0e 's/<BEGIN_REPLACE_COL_NAMES>.*?<END_REPLACE_COL_NAMES>/`cat replaceColNames.ubak`/seg' ${BIZNET_UI_DIR}/${BIZNET_FOLDER_NAME}Setup/webapp/model/models${PARTICIPANT_NAME}${ENTITY_TYPE}s.js
perl -i -p0e 's/<BEGIN_REPLACE_MODEL_ATTR>.*?<END_REPLACE_MODEL_ATTR>/`cat replaceModelAttributes.ubak`/seg' ${BIZNET_UI_DIR}/${BIZNET_FOLDER_NAME}Setup/webapp/model/models${PARTICIPANT_NAME}${ENTITY_TYPE}s.js

#Update View contents
cd ${BIZNET_UI_DIR}/${BIZNET_FOLDER_NAME}Setup/webapp/view
perl -i -p0e 's/<!--BEGIN_OF_SEL_ENTITY-->.*?<!--END_OF_SEL_ENTITY-->/`cat entityDetailsViewSelected.ubak`/seg' ${BIZNET_UI_DIR}/${BIZNET_FOLDER_NAME}Setup/webapp/view/${PARTICIPANT_NAME}${ENTITY_TYPE}Details.view.xml
perl -i -p0e 's/<!--BEGIN_OF_NEW_ENTITY-->.*?<!--END_OF_NEW_ENTITY-->/`cat entityDetailsViewNew.ubak`/seg' ${BIZNET_UI_DIR}/${BIZNET_FOLDER_NAME}Setup/webapp/view/${PARTICIPANT_NAME}${ENTITY_TYPE}Details.view.xml

#Delete all backup files
cd ${BIZNET_UI_DIR}/${BIZNET_FOLDER_NAME}Setup/webapp/model
find . -name "*.ubak" -type f -delete

cd ${BIZNET_UI_DIR}/${BIZNET_FOLDER_NAME}Setup/webapp/util
find . -name "*.ubak" -type f -delete

cd ${BIZNET_UI_DIR}/${BIZNET_FOLDER_NAME}Setup/webapp/view
find . -name "*.ubak" -type f -delete

cd ${TARGET_DIR}
find . -name "*.sbak" -type f -delete

cd ..
find . -name "*.sbak" -type f -delete

exit 0
