#!/bin/bash

#Colors
DARK_GRAY='\033[1;30m'
RED='\033[0;31m'
GREEN='\033[0;32m'
ORANGE='\033[0;33m'
BLUE='\033[0;34m'
PURPLE='\033[0;35m'
CYAN='\033[0;36m'
LIGHT_GRAY='\033[0;36m'
LIGHT_RED='\033[1;31m'
LIGHT_GREEN='\033[1;32m'
YELLOW='\033[1;33m'
LIGHT_BLUE='\033[1;34m'
LIGHT_PURPLE='\033[1;35m'
LIGHT_CYAN='\033[1;36m'
WHITE='\033[1;37m'
NC='\033[0m'

#Source folders for templates
TEMPLATE_UI_DIR=${1}
BIZNET_UI_DIR=${2}
BIZNET_FOLDER_NAME=${3}
ENTITY_NAME=${4}
ENTITY_NAME_LOWER=${5}
ENTITY_TYPE=${6}

#Navigate to
cd "${BIZNET_UI_DIR}/${BIZNET_FOLDER_NAME}Setup"

#Enhance Selection Controller
TEMPLATE_SEL_CTRL_DEFINE="
        \"${BIZNET_FOLDER_NAME}Setup/util/bizNetAccess${ENTITY_NAME}${ENTITY_TYPE}s\",
        \"${BIZNET_FOLDER_NAME}Setup/model/models${ENTITY_NAME}${ENTITY_TYPE}s\",
"

TEMPLATE_SEL_CTRL_INCLUDE="
        bizNetAccess${ENTITY_NAME}${ENTITY_TYPE}s,
        models${ENTITY_NAME}${ENTITY_TYPE}s,
"

TEMPLATE_SEL_CTRL_SELECTION="
        case \"${ENTITY_NAME}${ENTITY_TYPE}s\":
          this.getView().getModel(\"Selection\").setProperty(\"/entityName\", \"${ENTITY_NAME_LOWER}${ENTITY_TYPE}\");
          this.getOwnerComponent().setModel(models${ENTITY_NAME}${ENTITY_TYPE}s.create${ENTITY_NAME}${ENTITY_TYPE}sModel(), \"${ENTITY_NAME}${ENTITY_TYPE}s\");
          this.loadMetaData(\"${ENTITY_NAME_LOWER}${ENTITY_TYPE}\", this.getModel(\"${ENTITY_NAME}${ENTITY_TYPE}s\"));
          bizNetAccess${ENTITY_NAME}${ENTITY_TYPE}s.loadAll${ENTITY_NAME}${ENTITY_TYPE}s(this.getOwnerComponent(), this.getView().getModel(\"${ENTITY_NAME}${ENTITY_TYPE}s\"));
          this.getOwnerComponent().getRouter().navTo(\"${ENTITY_NAME_LOWER}${ENTITY_TYPE}s\", {});
          break;
"

TEMPLATE_SEL_CTRL_DELETION="
      if (typeof this.getOwnerComponent().getModel(\"${ENTITY_NAME}${ENTITY_TYPE}s\") === \"undefined\") {
        this.getOwnerComponent().setModel(models${ENTITY_NAME}${ENTITY_TYPE}s.create${ENTITY_NAME}${ENTITY_TYPE}sModel(), \"${ENTITY_NAME}${ENTITY_TYPE}s\");
      }
      bizNetAccess${ENTITY_NAME}${ENTITY_TYPE}s.removeAll${ENTITY_NAME}${ENTITY_TYPE}s(this.getOwnerComponent(), this.getView().getModel(\"${ENTITY_NAME}${ENTITY_TYPE}s\"));
      messageProvider.addMessage(\"Success\", \"All ${ENTITY_NAME}${ENTITY_TYPE}s deleted from sample-network\", \"No Description\", location, 1, \"\", \"http://www.sap.com\");
"
TEMPLATE_SEL_VIEW_ADDENTITY="
                  <RadioButton xmlns:core=\"sap.ui.core\" xmlns:mvc=\"sap.ui.core.mvc\" xmlns:action=\"http://schemas.sap.com/sapui5/extension/sap.ui.core.CustomData/1\" xmlns=\"sap.m\" groupName=\"__groupEntitySelection\" id=\"__button${ENTITY_NAME}${ENTITY_TYPE}s\" text=\"{i18n>_radioButton${ENTITY_NAME}${ENTITY_TYPE}s}\" select=\"onSelect\"/>"
#Update selection view with added entity
printf "${TEMPLATE_SEL_VIEW_ADDENTITY}" >> ${BIZNET_UI_DIR}/${BIZNET_FOLDER_NAME}Setup/entitySelectionViewAddEntity.ubak

#Update selection controller with added entity
printf "${TEMPLATE_SEL_CTRL_DEFINE}" >> ${BIZNET_UI_DIR}/${BIZNET_FOLDER_NAME}Setup/entitySelectionControllerDefine.ubak
printf "${TEMPLATE_SEL_CTRL_INCLUDE}" >> ${BIZNET_UI_DIR}/${BIZNET_FOLDER_NAME}Setup/entitySelectionControllerInclude.ubak
printf "${TEMPLATE_SEL_CTRL_SELECTION}" >> ${BIZNET_UI_DIR}/${BIZNET_FOLDER_NAME}Setup/entitySelectionControllerSelection.ubak
printf "${TEMPLATE_SEL_CTRL_DELETION}" >> ${BIZNET_UI_DIR}/${BIZNET_FOLDER_NAME}Setup/entitySelectionControllerDeletion.ubak

#Copy the relevant views for the added entity
cp -fi "${TEMPLATE_UI_DIR}/<Entity>sRoot.view.xml" "${BIZNET_UI_DIR}/${BIZNET_FOLDER_NAME}Setup/webapp/view/${ENTITY_NAME}${ENTITY_TYPE}sRoot.view.xml"
cp -fi "${TEMPLATE_UI_DIR}/<Entity>Details.view.xml" "${BIZNET_UI_DIR}/${BIZNET_FOLDER_NAME}Setup/webapp/view/${ENTITY_NAME}${ENTITY_TYPE}Details.view.xml"
cp -fi "${TEMPLATE_UI_DIR}/<Entity>Empty.view.xml" "${BIZNET_UI_DIR}/${BIZNET_FOLDER_NAME}Setup/webapp/view/${ENTITY_NAME}${ENTITY_TYPE}Empty.view.xml"
cp -fi "${TEMPLATE_UI_DIR}/<Entity>sOverview.view.xml" "${BIZNET_UI_DIR}/${BIZNET_FOLDER_NAME}Setup/webapp/view/${ENTITY_NAME}${ENTITY_TYPE}sOverview.view.xml"

#Replace entity properties in the target views
sed -i.ubak 's/<sample-network>/'${BIZNET_FOLDER_NAME}'/g' "${BIZNET_UI_DIR}/${BIZNET_FOLDER_NAME}Setup/webapp/view/${ENTITY_NAME}${ENTITY_TYPE}sRoot.view.xml"
sed -i.ubak 's/<Entity>/'${ENTITY_NAME}${ENTITY_TYPE}'/g' "${BIZNET_UI_DIR}/${BIZNET_FOLDER_NAME}Setup/webapp/view/${ENTITY_NAME}${ENTITY_TYPE}sRoot.view.xml"
sed -i.ubak 's/<sample-network>/'${BIZNET_FOLDER_NAME}'/g' "${BIZNET_UI_DIR}/${BIZNET_FOLDER_NAME}Setup/webapp/view/${ENTITY_NAME}${ENTITY_TYPE}Details.view.xml"
sed -i.ubak 's/<Entity>/'${ENTITY_NAME}${ENTITY_TYPE}'/g' "${BIZNET_UI_DIR}/${BIZNET_FOLDER_NAME}Setup/webapp/view/${ENTITY_NAME}${ENTITY_TYPE}Details.view.xml"
sed -i.ubak 's/<sample-network>/'${BIZNET_FOLDER_NAME}'/g' "${BIZNET_UI_DIR}/${BIZNET_FOLDER_NAME}Setup/webapp/view/${ENTITY_NAME}${ENTITY_TYPE}Empty.view.xml"
sed -i.ubak 's/<Entity>/'${ENTITY_NAME}${ENTITY_TYPE}'/g' "${BIZNET_UI_DIR}/${BIZNET_FOLDER_NAME}Setup/webapp/view/${ENTITY_NAME}${ENTITY_TYPE}Empty.view.xml"
sed -i.ubak 's/sample-network/'${BIZNET_FOLDER_NAME}'/g' "${BIZNET_UI_DIR}/${BIZNET_FOLDER_NAME}Setup/webapp/view/${ENTITY_NAME}${ENTITY_TYPE}sOverview.view.xml"
sed -i.ubak 's/<Entity>/'${ENTITY_NAME}${ENTITY_TYPE}'/g' "${BIZNET_UI_DIR}/${BIZNET_FOLDER_NAME}Setup/webapp/view/${ENTITY_NAME}${ENTITY_TYPE}sOverview.view.xml"
sed -i.ubak 's/<entity>/'${ENTITY_NAME_LOWER}${ENTITY_TYPE}'/g' "${BIZNET_UI_DIR}/${BIZNET_FOLDER_NAME}Setup/webapp/view/${ENTITY_NAME}${ENTITY_TYPE}sOverview.view.xml"

#Copy the corresponding controllers for the added views
cp -fi "${TEMPLATE_UI_DIR}/<Entity>sRoot.controller.js" "${BIZNET_UI_DIR}/${BIZNET_FOLDER_NAME}Setup/webapp/controller/${ENTITY_NAME}${ENTITY_TYPE}sRoot.controller.js"
cp -fi "${TEMPLATE_UI_DIR}/<Entity>Details.controller.js" "${BIZNET_UI_DIR}/${BIZNET_FOLDER_NAME}Setup/webapp/controller/${ENTITY_NAME}${ENTITY_TYPE}Details.controller.js"
cp -fi "${TEMPLATE_UI_DIR}/<Entity>Empty.controller.js" "${BIZNET_UI_DIR}/${BIZNET_FOLDER_NAME}Setup/webapp/controller/${ENTITY_NAME}${ENTITY_TYPE}Empty.controller.js"
cp -fi "${TEMPLATE_UI_DIR}/<Entity>sOverview.controller.js" "${BIZNET_UI_DIR}/${BIZNET_FOLDER_NAME}Setup/webapp/controller/${ENTITY_NAME}${ENTITY_TYPE}sOverview.controller.js"

#Replace entity properties in the target controllers
sed -i.ubak 's/sample-network/'${BIZNET_FOLDER_NAME}'/g' "${BIZNET_UI_DIR}/${BIZNET_FOLDER_NAME}Setup/webapp/controller/${ENTITY_NAME}${ENTITY_TYPE}sRoot.controller.js"
sed -i.ubak 's/Entity/'${ENTITY_NAME}${ENTITY_TYPE}'/g' "${BIZNET_UI_DIR}/${BIZNET_FOLDER_NAME}Setup/webapp/controller/${ENTITY_NAME}${ENTITY_TYPE}sRoot.controller.js"
sed -i.ubak 's/entity/'${ENTITY_NAME_LOWER}${ENTITY_TYPE}'/g' "${BIZNET_UI_DIR}/${BIZNET_FOLDER_NAME}Setup/webapp/controller/${ENTITY_NAME}${ENTITY_TYPE}sRoot.controller.js"
sed -i.ubak 's/sample-network/'${BIZNET_FOLDER_NAME}'/g' "${BIZNET_UI_DIR}/${BIZNET_FOLDER_NAME}Setup/webapp/controller/${ENTITY_NAME}${ENTITY_TYPE}Details.controller.js"
sed -i.ubak 's/Entity/'${ENTITY_NAME}${ENTITY_TYPE}'/g' "${BIZNET_UI_DIR}/${BIZNET_FOLDER_NAME}Setup/webapp/controller/${ENTITY_NAME}${ENTITY_TYPE}Details.controller.js"
sed -i.ubak 's/entity/'${ENTITY_NAME_LOWER}${ENTITY_TYPE}'/g' "${BIZNET_UI_DIR}/${BIZNET_FOLDER_NAME}Setup/webapp/controller/${ENTITY_NAME}${ENTITY_TYPE}Details.controller.js"
sed -i.ubak 's/sample-network/'${BIZNET_FOLDER_NAME}'/g' "${BIZNET_UI_DIR}/${BIZNET_FOLDER_NAME}Setup/webapp/controller/${ENTITY_NAME}${ENTITY_TYPE}Empty.controller.js"
sed -i.ubak 's/Entity/'${ENTITY_NAME}${ENTITY_TYPE}'/g' "${BIZNET_UI_DIR}/${BIZNET_FOLDER_NAME}Setup/webapp/controller/${ENTITY_NAME}${ENTITY_TYPE}Empty.controller.js"
sed -i.ubak 's/entity/'${ENTITY_NAME_LOWER}${ENTITY_TYPE}'/g' "${BIZNET_UI_DIR}/${BIZNET_FOLDER_NAME}Setup/webapp/controller/${ENTITY_NAME}${ENTITY_TYPE}Empty.controller.js"
sed -i.ubak 's/sample-network/'${BIZNET_FOLDER_NAME}'/g' "${BIZNET_UI_DIR}/${BIZNET_FOLDER_NAME}Setup/webapp/controller/${ENTITY_NAME}${ENTITY_TYPE}sOverview.controller.js"
sed -i.ubak 's/Entity/'${ENTITY_NAME}${ENTITY_TYPE}'/g' "${BIZNET_UI_DIR}/${BIZNET_FOLDER_NAME}Setup/webapp/controller/${ENTITY_NAME}${ENTITY_TYPE}sOverview.controller.js"
sed -i.ubak 's/entity/'${ENTITY_NAME_LOWER}${ENTITY_TYPE}'/g' "${BIZNET_UI_DIR}/${BIZNET_FOLDER_NAME}Setup/webapp/controller/${ENTITY_NAME}${ENTITY_TYPE}sOverview.controller.js"

#Copy the relevant util files for the entity
cp -fi "${TEMPLATE_UI_DIR}/localStore<Entity>s.js" "${BIZNET_UI_DIR}/${BIZNET_FOLDER_NAME}Setup/webapp/util/localStore${ENTITY_NAME}${ENTITY_TYPE}s.js"
cp -fi "${TEMPLATE_UI_DIR}/formatter<Entity>s.js" "${BIZNET_UI_DIR}/${BIZNET_FOLDER_NAME}Setup/webapp/util/formatter${ENTITY_NAME}${ENTITY_TYPE}s.js"
cp -fi "${TEMPLATE_UI_DIR}/bizNetAccess<Entity>s.js" "${BIZNET_UI_DIR}/${BIZNET_FOLDER_NAME}Setup/webapp/util/bizNetAccess${ENTITY_NAME}${ENTITY_TYPE}s.js"

#Replace entity properties in util files
sed -i.ubak 's/EntityType/'${ENTITY_TYPE}'/g' "${BIZNET_UI_DIR}/${BIZNET_FOLDER_NAME}Setup/webapp/util/localStore${ENTITY_NAME}${ENTITY_TYPE}s.js"
sed -i.ubak 's/Entity/'${ENTITY_NAME}${ENTITY_TYPE}'/g' "${BIZNET_UI_DIR}/${BIZNET_FOLDER_NAME}Setup/webapp/util/localStore${ENTITY_NAME}${ENTITY_TYPE}s.js"
sed -i.ubak 's/entity/'${ENTITY_NAME_LOWER}${ENTITY_TYPE}'/g' "${BIZNET_UI_DIR}/${BIZNET_FOLDER_NAME}Setup/webapp/util/localStore${ENTITY_NAME}${ENTITY_TYPE}s.js"
sed -i.ubak 's/EntityType/'${ENTITY_TYPE}'/g' "${BIZNET_UI_DIR}/${BIZNET_FOLDER_NAME}Setup/webapp/util/formatter${ENTITY_NAME}${ENTITY_TYPE}s.js"
sed -i.ubak 's/Entity/'${ENTITY_NAME}${ENTITY_TYPE}'/g' "${BIZNET_UI_DIR}/${BIZNET_FOLDER_NAME}Setup/webapp/util/formatter${ENTITY_NAME}${ENTITY_TYPE}s.js"
sed -i.ubak 's/sample-network/'${BIZNET_FOLDER_NAME}'/g' "${BIZNET_UI_DIR}/${BIZNET_FOLDER_NAME}Setup/webapp/util/bizNetAccess${ENTITY_NAME}${ENTITY_TYPE}s.js"
sed -i.ubak 's/Entity/'${ENTITY_NAME}${ENTITY_TYPE}'/g' "${BIZNET_UI_DIR}/${BIZNET_FOLDER_NAME}Setup/webapp/util/bizNetAccess${ENTITY_NAME}${ENTITY_TYPE}s.js"
sed -i.ubak 's/entity/'${ENTITY_NAME_LOWER}${ENTITY_TYPE}'/g' "${BIZNET_UI_DIR}/${BIZNET_FOLDER_NAME}Setup/webapp/util/bizNetAccess${ENTITY_NAME}${ENTITY_TYPE}s.js"

#Copy the relevant model for the entity
cp -fi "${TEMPLATE_UI_DIR}/models<Entity>s.js" "${BIZNET_UI_DIR}/${BIZNET_FOLDER_NAME}Setup/webapp/model/models${ENTITY_NAME}${ENTITY_TYPE}s.js"

#Replace entity properties in model
sed -i.ubak 's/Entity/'${ENTITY_NAME}${ENTITY_TYPE}'/g' "${BIZNET_UI_DIR}/${BIZNET_FOLDER_NAME}Setup/webapp/model/models${ENTITY_NAME}${ENTITY_TYPE}s.js"
sed -i.ubak 's/entity/'${ENTITY_NAME_LOWER}${ENTITY_TYPE}'/g' "${BIZNET_UI_DIR}/${BIZNET_FOLDER_NAME}Setup/webapp/model/models${ENTITY_NAME}${ENTITY_TYPE}s.js"

exit 0
