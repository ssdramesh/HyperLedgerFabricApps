#!/bin/bash

#Colors
DARK_GRAY='\033[1;30m'
RED='\033[0;31m'
GREEN='\033[0;32m'
ORANGE='\033[0;33m'
BLUE='\033[0;34m'
PURPLE='\033[0;35m'
CYAN='\033[0;36m'
LIGHT_GRAY='\033[0;36m'
LIGHT_RED='\033[1;31m'
LIGHT_GREEN='\033[1;32m'
YELLOW='\033[1;33m'
LIGHT_BLUE='\033[1;34m'
LIGHT_PURPLE='\033[1;35m'
LIGHT_CYAN='\033[1;36m'
WHITE='\033[1;37m'
NC='\033[0m'

BIZNET_DEPLOY_DIR=$1
ENTITY_NAME=$2
ENTITY_NAME_LOWER=$3
ENTITY_TYPE=$4
IS_FIRST=$5

cd "${BIZNET_DEPLOY_DIR}"

#Deployment Descriptor for specified entity
ENTITY_CHAINCODE_TEMPLATE="{
  \"name\":\"${ENTITY_NAME_LOWER}${ENTITY_TYPE}\",
  \"chaincodeId\": \"<Enter the deployed chaincodeId for ${ENTITY_NAME_LOWER}${ENTITY_TYPE} here>\",
  \"chaincodeVersion\": \"/latest\",
  \"methods\": [{
    \"name\": \"addNew${ENTITY_NAME}${ENTITY_TYPE}\",
    \"type\":\"addNew\",
    \"path\": \"/\",
    \"httpMethod\":\"POST\",
    \"successMessage\": \"Creation of new ${ENTITY_NAME}${ENTITY_TYPE} successful\"
  }, {
    \"name\": \"update${ENTITY_NAME}${ENTITY_TYPE}\",
    \"type\":\"update\",
    \"path\": \"/update\",
    \"httpMethod\":\"POST\",
    \"successMessage\": \"Update of ${ENTITY_NAME}${ENTITY_TYPE} successful\"
  }, {
    \"name\": \"readAll${ENTITY_NAME}${ENTITY_TYPE}s\",
    \"type\":\"readAll\",
    \"path\": \"/\",
    \"httpMethod\":\"GET\",
    \"successMessage\": \"Retrieval of all ${ENTITY_NAME}${ENTITY_TYPE}s successful\"
  }, {
    \"name\": \"read${ENTITY_NAME}${ENTITY_TYPE}\",
    \"type\":\"read\",
    \"path\": \"/{id}\",
    \"httpMethod\":\"GET\",
    \"successMessage\": \"Read ${ENTITY_NAME}${ENTITY_TYPE} successful\"
  }, {
    \"name\": \"remove${ENTITY_NAME}${ENTITY_TYPE}\",
    \"type\":\"remove\",
    \"path\": \"/{id}\",
    \"httpMethod\":\"DELETE\",
    \"successMessage\": \"Removal of ${ENTITY_NAME}${ENTITY_TYPE} successful\"
  }, {
    \"name\": \"removeAll${ENTITY_NAME}${ENTITY_TYPE}s\",
    \"type\":\"removeAll\",
    \"path\": \"/clear\",
    \"httpMethod\":\"DELETE\",
    \"successMessage\": \"Removal of all ${ENTITY_NAME}${ENTITY_TYPE}s successful\"
  }]
}"

#Append to backup file
if [[ "${IS_FIRST}" == "y" ]]; then
  echo "$ENTITY_CHAINCODE_TEMPLATE" >> ${BIZNET_DEPLOY_DIR}/entitiesDeployment.dbak
else
  echo ",
   $ENTITY_CHAINCODE_TEMPLATE" >> ${BIZNET_DEPLOY_DIR}/entitiesDeployment.dbak
fi

exit 0
