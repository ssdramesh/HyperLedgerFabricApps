/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

/*
 * The sample smart contract for documentation topic:
 * Writing Your First Blockchain Application
 */

package main

/* Imports
 * 4 utility libraries for formatting, handling bytes, reading and writing JSON, and string manipulation
 * 2 specific Hyperledger Fabric specific libraries for Smart Contracts
 */
import (
	"bytes"
	"encoding/json"
	"fmt"
	"strconv"

	"github.com/hyperledger/fabric/core/chaincode/shim"
	sc "github.com/hyperledger/fabric/protos/peer"
	"strings"
	"net/http"
)

// Define the Smart Contract structure
type SmartContract struct {
}

// Define the car structure, with 4 properties.  Structure tags are used by encoding/json library
type Car struct {
	Make   string `json:"make"`
	Model  string `json:"model"`
	Colour string `json:"colour"`
	Owner  string `json:"owner"`
}

/*
 * The Init method is called when the Smart Contract "fabcar" is instantiated by the blockchain network
 * Best practice is to have any Ledger initialization in separate function -- see initLedger()
 */
func (s *SmartContract) Init(APIstub shim.ChaincodeStubInterface) sc.Response {
	return shim.Success(nil)
}

/*
 * The Invoke method is called as a result of an application request to run the Smart Contract "fabcar"
 * The calling application program has also specified the particular smart contract function to be called, with arguments
 */
func (s *SmartContract) Invoke(APIstub shim.ChaincodeStubInterface) sc.Response {

	// Retrieve the requested Smart Contract function and arguments
	function, args := APIstub.GetFunctionAndParameters()
	// Route to the appropriate handler function to interact with the ledger appropriately
	if function == "queryCar" {
		return s.queryCar(APIstub, args)
	} else if function == "initLedger" {
		return s.initLedger(APIstub)
	} else if function == "createCar" {
		return s.createCar(APIstub, args)
	} else if function == "queryAllCars" {
		return s.queryAllCars(APIstub)
	} else if function == "changeCarOwner" {
		return s.changeCarOwner(APIstub, args)
	} else if function == "$SQL" {
		return s.sql(APIstub, args)
	}

	return shim.Error("Invalid Smart Contract function name.")
}

func (s *SmartContract) queryCar(APIstub shim.ChaincodeStubInterface, args []string) sc.Response {

	if len(args) != 1 {
		return shim.Error("Incorrect number of arguments. Expecting 1")
	}

	carAsBytes, _ := APIstub.GetState(args[0])
	return shim.Success(carAsBytes)
}

func (s *SmartContract) initLedger(APIstub shim.ChaincodeStubInterface) sc.Response {
	cars := []Car{
		Car{Make: "Toyota", Model: "Prius", Colour: "blue", Owner: "Tomoko"},
		Car{Make: "Ford", Model: "Mustang", Colour: "red", Owner: "Brad"},
		Car{Make: "Hyundai", Model: "Tucson", Colour: "green", Owner: "Jin Soo"},
		Car{Make: "Volkswagen", Model: "Passat", Colour: "yellow", Owner: "Max"},
		Car{Make: "Tesla", Model: "S", Colour: "black", Owner: "Adriana"},
		Car{Make: "Peugeot", Model: "205", Colour: "purple", Owner: "Michel"},
		Car{Make: "Chery", Model: "S22L", Colour: "white", Owner: "Aarav"},
		Car{Make: "Fiat", Model: "Punto", Colour: "violet", Owner: "Pari"},
		Car{Make: "Tata", Model: "Nano", Colour: "indigo", Owner: "Valeria"},
		Car{Make: "Holden", Model: "Barina", Colour: "brown", Owner: "Shotaro"},
		Car{Make: "Volkswagen", Model: "Touran", Colour: "silver", Owner: "Marco"},
		Car{Make: "BMW", Model: "123d", Colour: "black", Owner: "Kai-Christoph"},
		Car{Make: "Audi", Model: "S3", Colour: "blue", Owner: "Christian"},
	}

	i := 0
	for i < len(cars) {
		fmt.Println("i is ", i)
		carAsBytes, _ := json.Marshal(cars[i])
		APIstub.PutState("CAR"+strconv.Itoa(i), carAsBytes)
		fmt.Println("Added", cars[i])
		i = i + 1
	}

	return shim.Success(nil)
}

func (s *SmartContract) createCar(APIstub shim.ChaincodeStubInterface, args []string) sc.Response {
	fmt.Println("create car args", args)
	if len(args) != 5 {
		return shim.Error("Incorrect number of arguments. Expecting 5")
	}

	var car = Car{Make: args[1], Model: args[2], Colour: args[3], Owner: args[4]}

	carAsBytes, _ := json.Marshal(car)
	APIstub.PutState(args[0], carAsBytes)

	return shim.Success(nil)
}

func (s *SmartContract) queryAllCars(APIstub shim.ChaincodeStubInterface) sc.Response {

	startKey := "CAR0"
	endKey := "CAR999"

	resultsIterator, err := APIstub.GetStateByRange(startKey, endKey)
	if err != nil {
		return shim.Error(err.Error())
	}
	defer resultsIterator.Close()

	// buffer is a JSON array containing QueryResults
	var buffer bytes.Buffer
	buffer.WriteString("[")

	bArrayMemberAlreadyWritten := false
	for resultsIterator.HasNext() {
		queryResponse, err := resultsIterator.Next()
		if err != nil {
			return shim.Error(err.Error())
		}
		// Add a comma before array members, suppress it for the first array member
		if bArrayMemberAlreadyWritten == true {
			buffer.WriteString(",")
		}
		buffer.WriteString("{\"Key\":")
		buffer.WriteString("\"")
		buffer.WriteString(queryResponse.Key)
		buffer.WriteString("\"")

		buffer.WriteString(", \"Record\":")
		// Record is a JSON object, so we write as-is
		buffer.WriteString(string(queryResponse.Value))
		buffer.WriteString("}")
		bArrayMemberAlreadyWritten = true
	}
	buffer.WriteString("]")

	fmt.Printf("- queryAllCars:\n%s\n", buffer.String())

	return shim.Success(buffer.Bytes())
}

func (s *SmartContract) changeCarOwner(APIstub shim.ChaincodeStubInterface, args []string) sc.Response {

	if len(args) != 2 {
		return shim.Error("Incorrect number of arguments. Expecting 2")
	}

	carAsBytes, _ := APIstub.GetState(args[0])
	car := Car{}

	json.Unmarshal(carAsBytes, &car)
	car.Owner = args[1]

	carAsBytes, _ = json.Marshal(car)
	APIstub.PutState(args[0], carAsBytes)

	return shim.Success(nil)
}

func (s *SmartContract) sql(APIstub shim.ChaincodeStubInterface, args []string) sc.Response {
	//func (cc *SmartContract) sql(stub shim.ChaincodeStubInterface, args []string) sc.Response {

	// JSON Schema of all messages we support for replication to HANA
	const schema string = `{
    "messages": [
        {
            "type": "fabcar",
            "description": "Handle cars on the ledger",
            "properties": [
                {
                    "@key": {
                        "type": "string",
                        "description": "The ID of a car",
                        "minLength": 1,
                        "maxLength": 16
                    }
                },
                {
                    "make": {
                        "description": "The make of a car",
                        "type": "string",
                        "minLength": 1,
                        "maxLength": 64
                    }
                },
                {
                    "model": {
                        "description": "The model of a car",
                        "type": "string",
                        "minLength": 1,
                        "maxLength": 64
                    }
                },
                {
                    "colour": {
                        "description": "The colour of a car",
                        "type": "string",
                        "minLength": 1,
                        "maxLength": 64
                    }
                },
                {
                    "owner": {
                        "description": "The owner of a car",
                        "type": "string",
                        "minLength": 1,
                        "maxLength": 64
                    }
                }
            ]
        }
    ]
}`

	// Extract command
	var cmd string = "UNKNOWN COMMAND"
	if len(args) > 0 {
		cmd = strings.ToUpper(args[0])
	}

	// Process command
	switch cmd {

	case "SCHEMA":
		{
			return Success(http.StatusOK, "Message Schema", []byte(schema))
		}

	case "SELECT":
		{

			//if len(args) != 2 {
			//	return shim.Error("Incorrect number of arguments. Expecting the key to query")
			//}

			fmt.Println("args is", args)
			id := args[1]
			return s.queryCar(APIstub, []string{id})

			// carAsBytes
			/*			value := s.queryCar(APIstub, []string{id})
						//var car Car
						//carJ := car.FromJson(value.Payload)

						return Success(http.StatusOK, "OK", value.Payload)
			*/

			//	if rc.Status != http.StatusOK {
			//		return rc
			//	}

			//	msg.Text = string(rc.Payload)
			//return Success(http.StatusOK, "OK", []byte(readCar.ToJson()))
			//}
		}

	case "INSERT":

		{
			fmt.Printf("insert args are %v", args)
			id := args[1]
			car := &Car{}
			err := json.Unmarshal([]byte(args[3]), car)
			if err != nil {
				fmt.Println("Error parsing JSON", err.Error())
				return shim.Error("Error parsing JSON: " + err.Error())
			}
			//car := Car{Make: args[2], Model: args[3], Colour: args[4], Owner: args[5]}
			carAsBytes, _ := json.Marshal(car)
			APIstub.PutState(id, carAsBytes)
			fmt.Println("Added", car)

			return shim.Success(nil)
		}

	case "UPDATE":

		{
			fmt.Printf("update args are %v", args)
			id := args[1]
			car := &Car{}
			err := json.Unmarshal([]byte(args[3]), car)
			if err != nil {
				fmt.Println("Error parsing JSON", err.Error())
				return shim.Error("Error parsing JSON: " + err.Error())
			}
			//car := Car{Make: args[2], Model: args[3], Colour: args[4], Owner: args[5]}
			carAsBytes, _ := json.Marshal(car)
			APIstub.PutState(id, carAsBytes)
			fmt.Println("Added", car)

			return shim.Success(nil)


			//fmt.Println("update args are", args)
			//id := args[1]
			//car := Car{Make: args[2], Model: args[3], Colour: args[4], Owner: args[5]}
			//carAsBytes, _ := json.Marshal(car)
			//APIstub.PutState(id, carAsBytes)
			//fmt.Println("Updated", car)
			//
			//return shim.Success(nil)
		}

		//	if rc := Validate("sql", args, /*args[0]=INSERT||UPDATE*/ "%s", 1, 16, /*args[1]=id*/ "%s", 0, 256, /*args[2]=msgType*/ "%s", 0, 16, /*args[3]=payload*/ "%s", 0, 1024); rc.Status > 0 {
		//		return rc
		//	}

		//'{"Args":["$sql", "insert", "kc2", "Ferrari", "F40", "red", "Kai-Christoph"]}'

		//	id := args[1]
		//	msgType := args[2]
		//	payload := args[3]
		//
		//	var msg Msg
		//	if err := json.Unmarshal([]byte(payload), &msg); err != nil || msgType != "msgTxt" {
		//		return Error(http.StatusBadRequest, "Message invalid!")
		//	}
		//
		//	switch cmd {
		//	case "INSERT":
		//		return cc.create(stub, []string{id, msg.Text})
		//	case "UPDATE":
		//		return cc.update(stub, []string{id, msg.Text})
		//	}
		//}

	case "DELETE":
		//{
		//	if rc := Validate("sql", args, /*args[0]=DELETE*/ "%s", 1, 16, /*args[1]=id*/ "%s", 0, 256); rc.Status > 0 {
		//		return rc
		//	}
		id := args[1]
		APIstub.DelState(id)
		return shim.Success(nil)
		//	return cc.delete(stub, []string{id})
		//}
	}

	logger.Warningf("$SQL('%s') invalid!", cmd)
	return Error(http.StatusNotImplemented, "Invalid $SQL! Valid methods are 'SCHEMA|SELECT|INSERT|UPDATE|DELETE'!")
}

// Return handling: for return, we either return "shim.Success (payload []byte) with HttpRetCode=200" or "shim.Error(msg string) with HttpRetCode=500".
// However, we want to set our own status codes to map into HTTP return codes. A few utility functions:
func Success(rc int32, msg string, payload []byte) sc.Response {
	return sc.Response{
		Status:  rc,
		Message: msg,
		Payload: payload,
	}
}

func Error(rc int32, msg string) sc.Response {
	logger.Errorf("Error %d = %s", rc, msg)
	return sc.Response{
		Status:  rc,
		Message: msg,
	}
}

func (car *Car) FromJson(input []byte) *Car {
	json.Unmarshal(input, car)
	return car
}

func (car *Car) ToJson() []byte {
	jsonMsg, _ := json.Marshal(car)
	return jsonMsg
}

// init logger
var logger *shim.ChaincodeLogger

// The main function is only relevant in unit test mode. Only included here for completeness.
func main() {

	// Create a new Smart Contract
	err := shim.Start(new(SmartContract))
	if err != nil {
		fmt.Printf("Error creating new Smart Contract: %s", err)
	}
}
