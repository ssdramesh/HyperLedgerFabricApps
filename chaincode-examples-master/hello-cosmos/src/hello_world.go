// DISCLAIMER:
// THIS SAMPLE CODE MAY BE USED SOLELY AS PART OF THE TEST AND EVALUATION OF THE SAP CLOUD PLATFORM
// BLOCKCHAIN SERVICE (THE “SERVICE”) AND IN ACCORDANCE WITH THE TERMS OF THE AGREEMENT FOR THE SERVICE.
// THIS SAMPLE CODE PROVIDED “AS IS”, WITHOUT ANY WARRANTY, ESCROW, TRAINING, MAINTENANCE, OR SERVICE
// OBLIGATIONS WHATSOEVER ON THE PART OF SAP.

package main

//=================================================================================================
//========================================================================================== IMPORT
import (
	"bytes"
	"encoding/json"
	"fmt"
	"net/http"
	"strconv"
	"strings"
	"time"

	"github.com/hyperledger/fabric/core/chaincode/shim"
	"github.com/hyperledger/fabric/protos/peer"
)

//=================================================================================================
//============================================================================= BLOCKCHAIN DOCUMENT
// Doc writes string to the blockchain (as JSON object) for a specific key
type Doc struct {
	Text string `json:"text"`
}

func (doc *Doc) FromJson(input []byte) *Doc {
	json.Unmarshal(input, doc)
	return doc
}

func (doc *Doc) ToJson() []byte {
	jsonDoc, _ := json.Marshal(doc)
	return jsonDoc
}

//=================================================================================================
//================================================================================= RETURN HANDLING
// Return handling: for return, we either return "shim.Success (payload []byte) with HttpRetCode=200"
// or "shim.Error(doc string) with HttpRetCode=500". However, we want to set our own status codes to
// map into HTTP return codes. A few utility functions:

func Success(rc int32, doc string, payload []byte) peer.Response {
	return peer.Response{
		Status:  rc,
		Message: doc,
		Payload: payload,
	}
}

func Error(rc int32, doc string) peer.Response {
	logger.Errorf("Error %d = %s", rc, doc)
	return peer.Response{
		Status:  rc,
		Message: doc,
	}
}

//=================================================================================================
//====================================================================================== VALIDATION
// Validation: all arguments for a function call is passed as a string array args[]. Validate that
// the number, type and length of the arguments are correct.
//
// The Validate function is called as follow:
// 		Validate("chaincode function name", args, T[0], Ta[0], Tb[0], T[1], Ta[1], Tb[1], ...)
// The parameter groups T[i], Ta[i], Tb[i] are used to validate each parameter in sequence in args.
// T[i]describes the type/format for the parameter i and Ta[i] and Tb[i] are type dependent.
//
//		T[i]	Ta[i]		Tb[i]				Comment
//		"%s"	minLength	maxLength			String with min/max length
//		"%json"	minLength	maxLength			JSON format with min/max length
//		"%b"	0			0					Boolean
//		"%i"	minValue	maxValue			Integer between min/max (inclusive)
//		"%f"	minValue	maxValue			Float64 between min/max (inclusive)
//		"%ts"	type		0					Timestamp in correct format, use for example: time.RFC3339
//		"%enum"	max			"enum1 enum2 ..."	Enum (one or upto max) from the space separated list supplied
//												Set max=1 if only one specific value is expected
//
func Validate(funcName string, args []string, desc ...interface{}) peer.Response {

	logger.Debugf("Function: %s(%s)", funcName, strings.TrimSpace(strings.Join(args, ",")))

	nrArgs := len(desc) / 3

	if len(args) != nrArgs {
		return Error(http.StatusBadRequest, "Parameter Mismatch")
	}

	for i := 0; i < nrArgs; i++ {
		switch desc[i*3] {

		case "%json":
			var jsonData map[string]interface{}
			if jsonErr := json.Unmarshal([]byte(args[i]), &jsonData); jsonErr != nil {
				return Error(http.StatusBadRequest, "JSON Payload Not Valid")
			}
			fallthrough

		case "%s":
			var minLen = desc[i*3+1].(int)
			var maxLen = desc[i*3+2].(int)
			if len(args[i]) < minLen || len(args[i]) > maxLen {
				return Error(http.StatusBadRequest, "Parameter Length Error")
			}

		case "%b":
			if _, err := strconv.ParseBool(args[i]); err != nil {
				return Error(http.StatusBadRequest, "Boolean Parameter Error")
			}

		case "%i":
			var min = desc[i*3+1].(int64)
			var max = desc[i*3+2].(int64)
			if i, err := strconv.ParseInt(args[i], 10, 0); err != nil || i < min || i > max {
				return Error(http.StatusBadRequest, "Integer Parameter Error")
			}

		case "%f":
			var min = desc[i*3+1].(float64)
			var max = desc[i*3+2].(float64)
			if f, err := strconv.ParseFloat(args[i], 64); err != nil || f < min || f > max {
				return Error(http.StatusBadRequest, "Float Parameter Error")
			}

		case "%ts":
			var format = desc[i*3+1].(string) // for example: time.RFC3339
			if _, err := time.Parse(args[i], format); err != nil {
				return Error(http.StatusBadRequest, "Timestamp Parameter Error")
			}

		case "%enum":
			var maxCount = desc[i*3+1].(int)
			var enums = strings.Fields(desc[i*3+2].(string))
			var values = strings.Fields(args[i])
			found := 0
			for _, v := range values {
				for _, e := range enums {
					if v == e {
						found++
						break
					}
				}
			}
			if found != len(values) || len(values) > maxCount {
				return Error(http.StatusBadRequest, "Enum Validation Error")
			}
		}
	}

	return Success(0, "OK", nil)
}

//=================================================================================================
//============================================================================================ MAIN
// Main function starts up the chaincode in the container during instantiate
//
var logger = shim.NewLogger("chaincode")

type HelloWorld struct {
	// use this structure for information that is held (in-memory) within chaincode
	// instance and available over all chaincode calls
}

func main() {
	if err := shim.Start(new(HelloWorld)); err != nil {
		fmt.Printf("Main: Error starting chaincode: %s", err)
	}
}

//=================================================================================================
//============================================================================================ INIT
// Init is called during Instantiate transaction after the chaincode container
// has been established for the first time, allowing the chaincode to
// initialize its internal data. Note that chaincode upgrade also calls this
// function to reset or to migrate data, so be careful to avoid a scenario
// where you inadvertently clobber your ledger's data!
//
func (cc *HelloWorld) Init(stub shim.ChaincodeStubInterface) peer.Response {
	// Validate supplied init parameters, in this case zero arguments!
	if _, args := stub.GetFunctionAndParameters(); len(args) > 0 {
		return Error(http.StatusBadRequest, "Init: Incorrect number of arguments; no arguments were expected.")
	}
	return Success(http.StatusOK, "OK", nil)
}

//=================================================================================================
//========================================================================================== INVOKE
// Invoke is called to update or query the ledger in a proposal transaction.
// Updated state variables are not committed to the ledger until the
// transaction is committed.
//
func (cc *HelloWorld) Invoke(stub shim.ChaincodeStubInterface) peer.Response {

	// Which function is been called?
	function, args := stub.GetFunctionAndParameters()

	// Route call to the correct function
	switch function {
	case "exist":
		return cc.exist(stub, args)
	case "read":
		return cc.read(stub, args)
	case "create":
		return cc.create(stub, args)
	case "update":
		return cc.update(stub, args)
	case "delete":
		return cc.delete(stub, args)
	case "history":
		return cc.history(stub, args)
	case "search":
		return cc.search(stub, args)
	case "$SQL":
		return cc.SQL(stub, args)
	case "$sql":
		return cc.SQL(stub, args)
	default:
		logger.Warningf("Invoke('%s') invalid!", function)
		return Error(http.StatusNotImplemented, "Invalid method! Valid methods are 'create|update|delete|exist|read|history|search|$SQL'!")
	}
}

//=================================================================================================
//=========================================================================================== EXIST
// Validate text's existance by ID
//
func (cc *HelloWorld) exist(stub shim.ChaincodeStubInterface, args []string) peer.Response {

	// Validate and extract parameters
	if rc := Validate("exist", args /*args[0]=id*/, "%s", 1, 64); rc.Status > 0 {
		return rc
	}
	id := strings.ToLower(args[0])

	// If we can read the ID, then it exists
	if value, err := stub.GetState(id); err != nil || value == nil {
		return Error(http.StatusNotFound, "Not Found")
	}

	return Success(http.StatusNoContent, "Text Exists", nil)
}

//=================================================================================================
//============================================================================================ READ
// Read text by ID
//
func (cc *HelloWorld) read(stub shim.ChaincodeStubInterface, args []string) peer.Response {

	// Validate and extract parameters
	if rc := Validate("read", args /*args[0]=id*/, "%s", 1, 64); rc.Status > 0 {
		return rc
	}
	id := strings.ToLower(args[0])

	// Read the value for the ID
	if value, err := stub.GetState(id); err != nil || value == nil {
		return Error(http.StatusNotFound, "Not Found")
	} else {
		return Success(http.StatusOK, "OK", value)
	}
}

//=================================================================================================
//========================================================================================== CREATE
// Creates a text by ID
//
func (cc *HelloWorld) create(stub shim.ChaincodeStubInterface, args []string) peer.Response {

	// Validate and extract parameters
	if rc := Validate("create", args /*args[0]=id*/, "%s", 1, 64 /*args[1]=text*/, "%s", 1, 255); rc.Status > 0 {
		return rc
	}
	id := strings.ToLower(args[0])
	doc := &Doc{Text: args[1]}

	// Validate that this ID does not yet exist. If the key does not exist (nil, nil) is returned.
	if value, err := stub.GetState(id); !(err == nil && value == nil) {
		return Error(http.StatusConflict, "Text Exists")
	}

	// Write the message
	if err := stub.PutState(id, doc.ToJson()); err != nil {
		return Error(http.StatusInternalServerError, err.Error())
	}

	return Success(http.StatusCreated, "Text Created", nil)
}

//=================================================================================================
//========================================================================================== UPDATE
// Updates a text by ID
//
func (cc *HelloWorld) update(stub shim.ChaincodeStubInterface, args []string) peer.Response {

	// Validate and extract parameters
	if rc := Validate("update", args /*args[0]=id*/, "%s", 1, 64 /*args[1]=text*/, "%s", 1, 255); rc.Status > 0 {
		return rc
	}
	id := strings.ToLower(args[0])
	doc := &Doc{Text: args[1]}

	// Validate that this ID exist
	if value, err := stub.GetState(id); err != nil || value == nil {
		return Error(http.StatusNotFound, "Not Found")
	}

	// Write the message
	if err := stub.PutState(id, doc.ToJson()); err != nil {
		return Error(http.StatusInternalServerError, err.Error())
	}

	return Success(http.StatusNoContent, "Text Updated", nil)
}

//=================================================================================================
//========================================================================================== DELETE
// Delete text by ID
//
func (cc *HelloWorld) delete(stub shim.ChaincodeStubInterface, args []string) peer.Response {

	// Validate and extract parameters
	if rc := Validate("delete", args /*args[0]=id*/, "%s", 1, 64); rc.Status > 0 {
		return rc
	}
	id := strings.ToLower(args[0])

	// Validate that this ID exist
	if value, err := stub.GetState(id); err != nil || value == nil {
		return Error(http.StatusNotFound, "Not Found")
	}

	// Delete the message
	if err := stub.DelState(id); err != nil {
		return Error(http.StatusInternalServerError, err.Error())
	}

	return Success(http.StatusNoContent, "Message Deleted", nil)
}

//=================================================================================================
//========================================================================================= HISTORY
// Return history by ID
//
func (cc *HelloWorld) history(stub shim.ChaincodeStubInterface, args []string) peer.Response {

	// Validate and extract parameters
	if rc := Validate("history", args /*args[0]=id*/, "%s", 1, 64); rc.Status > 0 {
		return rc
	}
	id := strings.ToLower(args[0])

	// Read history
	resultsIterator, err := stub.GetHistoryForKey(id)
	if err != nil {
		return Error(http.StatusNotFound, "Not Found")
	}
	defer resultsIterator.Close()

	// Write return buffer
	var buffer bytes.Buffer
	buffer.WriteString("{ \"values\": [")
	for resultsIterator.HasNext() {
		it, _ := resultsIterator.Next()
		if buffer.Len() > 15 {
			buffer.WriteString(",")
		}
		var doc Doc
		buffer.WriteString("{\"timestamp\":\"")
		buffer.WriteString(time.Unix(it.Timestamp.Seconds, int64(it.Timestamp.Nanos)).Format(time.Stamp))
		buffer.WriteString("\", \"text\":")
		buffer.WriteString(doc.FromJson(it.Value).Text)
		buffer.WriteString("\"}")
	}
	buffer.WriteString("]}")

	return Success(200, "OK", buffer.Bytes())
}

//=================================================================================================
//========================================================================================== SEARCH
// Search for all matching IDs, given a (regex) value expression and return both the IDs and text.
// For example: '^H.llo' will match any string starting with 'Hello' or 'Hallo'.
//
func (cc *HelloWorld) search(stub shim.ChaincodeStubInterface, args []string) peer.Response {

	// Validate and extract parameters
	if rc := Validate("search", args /*args[0]=searchString*/, "%s", 1, 64); rc.Status > 0 {
		return rc
	}
	searchString := strings.Replace(args[0], "\"", ".", -1) // protect against SQL injection

	// stub.GetQueryResult takes a verbatim CouchDB (assuming this is used DB). See CouchDB documentation:
	//     http://docs.couchdb.org/en/2.0.0/api/database/find.html
	// For example:
	//	{
	//		"selector": {
	//			"value": {"$regex": %s"}
	//		},
	//		"fields": ["ID","value"],
	//		"limit":  99
	//	}
	queryString := fmt.Sprintf("{\"selector\": {\"text\": {\"$regex\": \"%s\"}}, \"fields\": [\"text\"], \"limit\":99}", strings.Replace(searchString, "\"", ".", -1))
	resultsIterator, err := stub.GetQueryResult(queryString)
	if err != nil {
		return Error(http.StatusInternalServerError, err.Error())
	}
	defer resultsIterator.Close()

	// Write return buffer
	var buffer bytes.Buffer
	buffer.WriteString("{ \"values\": [")
	for resultsIterator.HasNext() {
		it, _ := resultsIterator.Next()
		if buffer.Len() > 15 {
			buffer.WriteString(",")
		}
		var doc Doc
		buffer.WriteString("{\"id\":\"")
		buffer.WriteString(it.Key)
		buffer.WriteString("\", \"text\":\"")
		buffer.WriteString(doc.FromJson(it.Value).Text)
		buffer.WriteString("\"}")
	}
	buffer.WriteString("]}")

	return Success(http.StatusOK, "OK", buffer.Bytes())
}

//=================================================================================================
//============================================================================================= SQL
// SQL Interface for HANA Integration
//
// Calling sequences:
//		QUERY:	$SQL("SCHEMA")
//		QUERY:	$SQL("SELECT", id, docType)
//		INVOKE:	$SQL("INSERT", id, docType, payload)
//		INVOKE:	$SQL("UPDATE", id, docType, payload)
//		INVOKE:	$SQL("DELETE", id, docType)
//
func (cc *HelloWorld) SQL(stub shim.ChaincodeStubInterface, args []string) peer.Response {

	// JSON Schema of all messages we support for replication to HANA
	const schema string = `{
								"messages": [
									{
										"type": "docTxt",
										"properties": [
											{
												"@key": {
													"description": "ID of the message",
													"type": "string",
													"maxLength": 16,
													"minLength": 1
												}
											},
											{
												"text": {
													"description": "The message text.",
													"type": "string",
													"maxLength": 255
												}
											}
										]
									}
								]
							}`

	// Extract command
	cmd := "UNKNOWN COMMAND"
	if len(args) > 0 {
		cmd = strings.ToUpper(args[0])
	}

	// Process command
	switch cmd {

	case "SCHEMA":
		return Success(http.StatusOK, "Message Schema", []byte(schema))

	case "SELECT":
		if rc := Validate("$SQL", args /*args[0]=SELECT*/, "%s", 1, 16 /*args[1]=id*/, "%s", 1, 64 /*args[2]=docType*/, "%enum", 1, "docTxt"); rc.Status > 0 {
			return rc
		}
		return cc.read(stub, []string{args[1]})

	case "INSERT":
		if rc := Validate("$SQL", args /*args[0]=INSERT*/, "%s", 1, 16 /*args[1]=id*/, "%s", 1, 64 /*args[2]=docType*/, "%enum", 1, "docTxt" /*args[3]=payload*/, "%json", 2, 1024); rc.Status > 0 {
			return rc
		}
		var doc Doc
		return cc.create(stub, []string{args[1], doc.FromJson([]byte(args[3])).Text})

	case "UPDATE":
		if rc := Validate("$SQL", args /*args[0]=UPDATE*/, "%s", 1, 16 /*args[1]=id*/, "%s", 1, 64 /*args[2]=docType*/, "%enum", 1, "docTxt" /*args[3]=payload*/, "%json", 2, 1024); rc.Status > 0 {
			return rc
		}
		var doc Doc
		return cc.update(stub, []string{args[1], doc.FromJson([]byte(args[3])).Text})

	case "DELETE":
		if rc := Validate("$SQL", args /*args[0]=DELETE*/, "%s", 1, 16 /*args[1]=id*/, "%s", 1, 64 /*args[2]=docType*/, "%enum", 1, "docTxt"); rc.Status > 0 {
			return rc
		}
		return cc.delete(stub, []string{args[1]})
	}

	logger.Warningf("$SQL('%s') invalid!", cmd)
	return Error(http.StatusNotImplemented, "Invalid $SQL! Valid methods are 'SCHEMA|SELECT|INSERT|UPDATE|DELETE'!")
}
