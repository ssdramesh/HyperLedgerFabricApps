# Hperledger Fabric Applications
This repository contains all the demos and prototypes built using SAP Hyperledger Fabric as a service across APJ.

# Content Structure
The repository is organized with a folder for each demo.  Within that folder all the collaterals related to that demo are organized as follows:

1. chaincode - Folder for the blockchain chaincode (in GO language) when applicable.  If this folder exists then, the corresponding BlockchainApp is deployed on Hyperledger Fabric v1.0 (HLF v1.0) mostly on SAP Blockchain as a Service (SAP BaaS) instance.  (previous versions of tuna demo are deployed using fabric composer).  SAP BaaS instance are currently in the APJPresalesDigitalLab canary account (2017/11)

2. deployment -

3. Model - Folder for the models of the deployed chaincode.  Model is a UML static structure (class diagrams) representing the data model (as classes with attributes) and chaincode transactions (methods).  Models are stored as .mdj files in JSON format.  On Mac, they can be opened by StarUML software.  But any software that is able to read .mdj file can be used.

3. Postman -

4. UI5 - Folder for all HTML5 / SAPUI5 Fiori frontend applications that consume the chaincode.  This folder has all the source code for SAPUI5 applications.  Each zip file can be downloaded into any SAP Cloud Platform tenant of your choice and deployed.  They are ready to run with the corresponding deployed chaincode IDs on the APJPresalesDigitalLab (canary account for SAP BaaS)

# Further Info
Each folder under this repository has its own README.md (and optionally a detailed wiki) documenting further information about the following:
- Business Context and / or Customer Background
- Technical Implementation and Deployment Details
- Sample Data and / or Demo Scripts
- Links to JAM / InnoJAM where applicable
