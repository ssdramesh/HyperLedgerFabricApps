package main

//Template Version: 1.1-20180704
import (
	"encoding/json"
	"errors"
	"fmt"

	"github.com/hyperledger/fabric/core/chaincode/shim"
	"github.com/hyperledger/fabric/protos/peer"
)

var logger = shim.NewLogger("CLDChaincode")

//AcceptanceStatus - enumeration for acceptance statuses (Same status enumeration for Acceptance)
type AcceptanceStatus int

const (
	Accepted AcceptanceStatus = 1 + iota
	Rejected
)

var acceptanceStatuses = [...]string{
	"Accepted",
	"Rejected",
}

func (acceptanceStatus AcceptanceStatus) acceptanceStatusToString() string {
	return acceptanceStatuses[acceptanceStatus-1]
}

//Price (dataType)
type Price struct {
	Amount   string `json:"amount"`
	Currency string `json:"currency"`
}

type Route struct {
	FromAirport  string `json:"fromAirport"`
	ToAirport    string `json:"toAirport"`
	AirlineID    string `json:"airlineID"`
	AircraftType string `json:"aircraftType"`
	FlightNo     string `json:"flightNo"`
	FlightDate   string `json:"flightDate"`
}

type ULD struct {
	ID                     string `json:"ID"`
	Code                   string `json:"code"`
	OperationalGrossWeight string `json:"operationalGrossWeight"`
	ExternalVolume         string `json:"externalVolume"`
	InternalVolume         string `json:"internalVolume"`
	Price                  Price  `json:"price"`
}

//AcceptanceAsset - Chaincode for asset Acceptance
type AcceptanceAsset struct {
}

//Acceptance - Details of the asset type Acceptance
type Acceptance struct {
	ID                      string  `json:"ID"`
	ObjectType              string  `json:"docType"`
	Status                  string  `json:"status"`
	CreationDate            string  `json:"creationDate"`
	CargoTerminalOperatorID string  `json:"cargoTerminalOperatorID"`
	BookingID               string  `json:"bookingID"`
	FreightForwarderID      string  `json:"freightForwarderID"`
	AirwayBillID            string  `json:"airwayBillID"`
	RouteCollection         []Route `json:"routeCollection"`
	Freehold                string  `json:"freehold"`
	ULDCollection           []ULD   `json:"uLDCollection"`
}

//AcceptanceIDIndex - Index on IDs for retrieval all Acceptances
type AcceptanceIDIndex struct {
	IDs []string `json:"IDs"`
}

func main() {
	err := shim.Start(new(AcceptanceAsset))
	if err != nil {
		fmt.Printf("Error starting AcceptanceAsset chaincode function main(): %s", err)
	} else {
		fmt.Printf("Starting AcceptanceAsset chaincode function main() executed successfully")
	}
}

//Init - The chaincode Init function: No  arguments, only initializes a ID array as Index for retrieval of all Acceptances
func (acpt *AcceptanceAsset) Init(stub shim.ChaincodeStubInterface) peer.Response {
	var acceptanceIDIndex AcceptanceIDIndex
	record, _ := stub.GetState("acceptanceIDIndex")
	//chaincode upgrade: Index might already exist, create new only if it does not
	if record == nil {
		bytes, _ := json.Marshal(acceptanceIDIndex)
		stub.PutState("acceptanceIDIndex", bytes)
	}
	return shim.Success(nil)
}

//Invoke - The chaincode Invoke function:
func (acpt *AcceptanceAsset) Invoke(stub shim.ChaincodeStubInterface) peer.Response {
	function, args := stub.GetFunctionAndParameters()
	logger.Debug("function: ", function)
	switch function {
	case "addNewAcceptance":
		return acpt.addNewAcceptance(stub, args)
	case "updateAcceptance":
		return acpt.updateAcceptance(stub, args)
	case "removeAcceptance":
		return acpt.removeAcceptance(stub, args[0])
	case "removeAllAcceptances":
		return acpt.removeAllAcceptances(stub)
	case "readAcceptance":
		return acpt.readAcceptance(stub, args[0])
	case "readAllAcceptances":
		return acpt.readAllAcceptances(stub)
	default:
		return shim.Error("Received unknown function invocation")
	}
}

//Invoke Route: addNewAcceptance
func (acpt *AcceptanceAsset) addNewAcceptance(stub shim.ChaincodeStubInterface, args []string) peer.Response {
	acceptance, err := getAcceptanceFromArgs(args)
	if err != nil {
		return shim.Error("Acceptance Data is Corrupted")
	}
	acceptance.ObjectType = "Asset.AcceptanceAsset"
	record, err := stub.GetState(acceptance.ID)
	if record != nil {
		return shim.Error("This Acceptance already exists: " + acceptance.ID)
	}
	_, err = acpt.saveAcceptance(stub, acceptance)
	if err != nil {
		return shim.Error(err.Error())
	}
	_, err = acpt.updateAcceptanceIDIndex(stub, acceptance)
	if err != nil {
		return shim.Error(err.Error())
	}
	return shim.Success(nil)
}

//Invoke Route: updateAcceptance
func (acpt *AcceptanceAsset) updateAcceptance(stub shim.ChaincodeStubInterface, args []string) peer.Response {
	var acceptance Acceptance

	input, err := getAcceptanceFromArgs(args)
	if err != nil {
		return shim.Error("Acceptance Data is corrupted")
	}
	_, err = stub.GetState(input.ID)
	if err != nil {
		return shim.Error("Acceptance with ID: " + input.ID + "not found")
	}
	AcceptanceAsByteArray, err := acpt.retrieveAcceptance(stub, input.ID)
	if err != nil {
		return shim.Error(err.Error())
	}
	err = json.Unmarshal(AcceptanceAsByteArray, &acceptance)
	if err != nil {
		return shim.Error(err.Error())
	}
	acceptance = input
	_, err = acpt.saveAcceptance(stub, acceptance)
	if err != nil {
		return shim.Error(err.Error())
	}
	return shim.Success(nil)
}

//Invoke Route: removeAcceptance
func (acpt *AcceptanceAsset) removeAcceptance(stub shim.ChaincodeStubInterface, acceptanceID string) peer.Response {
	_, err := acpt.deleteAcceptance(stub, acceptanceID)
	if err != nil {
		return shim.Error(err.Error())
	}
	_, err = acpt.deleteAcceptanceIDIndex(stub, acceptanceID)
	if err != nil {
		return shim.Error(err.Error())
	}
	return shim.Success(nil)
}

//Invoke Route: removeAllAcceptances
func (acpt *AcceptanceAsset) removeAllAcceptances(stub shim.ChaincodeStubInterface) peer.Response {
	var acceptanceIDIndex AcceptanceIDIndex
	bytes, err := stub.GetState("acceptanceIDIndex")
	if err != nil {
		return shim.Error("removeAllAcceptances: Error getting acceptanceIDIndex array")
	}
	err = json.Unmarshal(bytes, &acceptanceIDIndex)
	if err != nil {
		return shim.Error("removeAllAcceptances: Error unmarshalling acceptanceIDIndex array JSON")
	}
	if len(acceptanceIDIndex.IDs) == 0 {
		return shim.Error("removeAllAcceptances: No acceptances to remove")
	}
	for _, acceptanceStructID := range acceptanceIDIndex.IDs {
		_, err = acpt.deleteAcceptance(stub, acceptanceStructID)
		if err != nil {
			return shim.Error("Failed to remove Acceptance with ID: " + acceptanceStructID)
		}
		_, err = acpt.deleteAcceptanceIDIndex(stub, acceptanceStructID)
		if err != nil {
			return shim.Error(err.Error())
		}
	}
	acpt.initHolder(stub)
	return shim.Success(nil)
}

//Query Route: readAcceptance
func (acpt *AcceptanceAsset) readAcceptance(stub shim.ChaincodeStubInterface, acceptanceID string) peer.Response {
	acceptanceAsByteArray, err := acpt.retrieveAcceptance(stub, acceptanceID)
	if err != nil {
		return shim.Error(err.Error())
	}
	return shim.Success(acceptanceAsByteArray)
}

//Query Route: readAllAcceptances
func (acpt *AcceptanceAsset) readAllAcceptances(stub shim.ChaincodeStubInterface) peer.Response {
	var acceptanceIDs AcceptanceIDIndex
	bytes, err := stub.GetState("acceptanceIDIndex")
	if err != nil {
		return shim.Error("readAllAcceptances: Error getting acceptanceIDIndex array")
	}
	err = json.Unmarshal(bytes, &acceptanceIDs)
	if err != nil {
		return shim.Error("readAllAcceptances: Error unmarshalling acceptanceIDIndex array JSON")
	}
	result := "["

	var acceptanceAsByteArray []byte

	for _, acceptanceID := range acceptanceIDs.IDs {
		acceptanceAsByteArray, err = acpt.retrieveAcceptance(stub, acceptanceID)
		if err != nil {
			return shim.Error("Failed to retrieve acceptance with ID: " + acceptanceID)
		}
		result += string(acceptanceAsByteArray) + ","
	}
	if len(result) == 1 {
		result = "[]"
	} else {
		result = result[:len(result)-1] + "]"
	}
	return shim.Success([]byte(result))
}

//Helper: Save AcceptanceAsset
func (acpt *AcceptanceAsset) saveAcceptance(stub shim.ChaincodeStubInterface, acceptance Acceptance) (bool, error) {
	bytes, err := json.Marshal(acceptance)
	if err != nil {
		return false, errors.New("Error converting acceptance record JSON")
	}
	err = stub.PutState(acceptance.ID, bytes)
	if err != nil {
		return false, errors.New("Error storing Acceptance record")
	}
	return true, nil
}

//Helper: delete AcceptanceAsset
func (acpt *AcceptanceAsset) deleteAcceptance(stub shim.ChaincodeStubInterface, acceptanceID string) (bool, error) {
	_, err := acpt.retrieveAcceptance(stub, acceptanceID)
	if err != nil {
		return false, errors.New("Acceptance with ID: " + acceptanceID + " not found")
	}
	err = stub.DelState(acceptanceID)
	if err != nil {
		return false, errors.New("Error deleting Acceptance record")
	}
	return true, nil
}

//Helper: Update acceptance Holder - updates Index
func (acpt *AcceptanceAsset) updateAcceptanceIDIndex(stub shim.ChaincodeStubInterface, acceptance Acceptance) (bool, error) {
	var acceptanceIDs AcceptanceIDIndex
	bytes, err := stub.GetState("acceptanceIDIndex")
	if err != nil {
		return false, errors.New("updateAcceptanceIDIndex: Error getting acceptanceIDIndex array Index from state")
	}
	err = json.Unmarshal(bytes, &acceptanceIDs)
	if err != nil {
		return false, errors.New("updateAcceptanceIDIndex: Error unmarshalling acceptanceIDIndex array JSON")
	}
	acceptanceIDs.IDs = append(acceptanceIDs.IDs, acceptance.ID)
	bytes, err = json.Marshal(acceptanceIDs)
	if err != nil {
		return false, errors.New("updateAcceptanceIDIndex: Error marshalling new acceptance ID")
	}
	err = stub.PutState("acceptanceIDIndex", bytes)
	if err != nil {
		return false, errors.New("updateAcceptanceIDIndex: Error storing new acceptance ID in acceptanceIDIndex (Index)")
	}
	return true, nil
}

//Helper: delete ID from acceptanceStruct Holder
func (acpt *AcceptanceAsset) deleteAcceptanceIDIndex(stub shim.ChaincodeStubInterface, acceptanceID string) (bool, error) {
	var acceptanceIDIndex AcceptanceIDIndex
	bytes, err := stub.GetState("acceptanceIDIndex")
	if err != nil {
		return false, errors.New("deleteAcceptanceIDIndex: Error getting acceptanceIDIndex array Index from state")
	}
	err = json.Unmarshal(bytes, &acceptanceIDIndex)
	if err != nil {
		return false, errors.New("deleteAcceptanceIDIndex: Error unmarshalling acceptanceIDIndex array JSON")
	}
	acceptanceIDIndex.IDs, err = deleteKeyFromStringArray(acceptanceIDIndex.IDs, acceptanceID)
	if err != nil {
		return false, errors.New(err.Error())
	}
	bytes, err = json.Marshal(acceptanceIDIndex)
	if err != nil {
		return false, errors.New("deleteAcceptanceIDIndex: Error marshalling new acceptanceStruct ID")
	}
	err = stub.PutState("acceptanceIDIndex", bytes)
	if err != nil {
		return false, errors.New("deleteAcceptanceIDIndex: Error storing new acceptanceStruct ID in acceptanceIDIndex (Index)")
	}
	return true, nil
}

//Helper: Initialize truck ID Holder //change template
func (acpt *AcceptanceAsset) initHolder(stub shim.ChaincodeStubInterface) bool {
	var acceptanceIDIndex AcceptanceIDIndex
	bytes, _ := json.Marshal(acceptanceIDIndex)
	stub.DelState("acceptanceIDIndex")
	stub.PutState("acceptanceIDIndex", bytes)
	return true
}

//deleteKeyFromArray
func deleteKeyFromStringArray(array []string, key string) (newArray []string, err error) {
	for _, entry := range array {
		if entry != key {
			newArray = append(newArray, entry)
		}
	}
	if len(newArray) == len(array) {
		return newArray, errors.New("Specified Key: " + key + " not found in Array")
	}
	return newArray, nil
}

//Helper: Retrieve purchaser
func (acpt *AcceptanceAsset) retrieveAcceptance(stub shim.ChaincodeStubInterface, acceptanceID string) ([]byte, error) {
	var acceptance Acceptance
	var acceptanceAsByteArray []byte
	bytes, err := stub.GetState(acceptanceID)
	if err != nil {
		return acceptanceAsByteArray, errors.New("retrieveAcceptance: Error retrieving acceptance with ID: " + acceptanceID)
	}
	err = json.Unmarshal(bytes, &acceptance)
	if err != nil {
		return acceptanceAsByteArray, errors.New("retrieveAcceptance: Corrupt acceptance record " + string(bytes))
	}
	acceptanceAsByteArray, err = json.Marshal(acceptance)
	if err != nil {
		return acceptanceAsByteArray, errors.New("readAcceptance: Invalid acceptance Object - Not a  valid JSON")
	}
	return acceptanceAsByteArray, nil
}

//getAcceptanceFromArgs - construct a acceptance structure from string array of arguments
func getAcceptanceFromArgs(args []string) (acceptance Acceptance, err error) {

	if !Valid(args[0]) {
		return acceptance, errors.New("Invalid json")
	}

	err = Unmarshal([]byte(args[0]), &acceptance)
	if err != nil {
		return acceptance, err
	}
	return acceptance, nil
}
