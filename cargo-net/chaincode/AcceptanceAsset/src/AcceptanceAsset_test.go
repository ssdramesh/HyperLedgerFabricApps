package main

//Template Version: 1.1-20180704
import (
	"bytes"
	"encoding/json"
	"fmt"
	"strings"
	"testing"

	"github.com/hyperledger/fabric/core/chaincode/shim"
)

//TestAcceptanceAsset_Init
func TestAcceptanceAsset_Init(t *testing.T) {
	acceptance := new(AcceptanceAsset)
	stub := shim.NewMockStub("acceptance", acceptance)
	checkInit(t, stub, [][]byte{[]byte("init")})
	checkState(t, stub, "acceptanceIDIndex", getExpectedAcceptanceIDIndex("init"))
}

//TestAcceptanceAsset_InvokeUnknownFunction
func TestAcceptanceAsset_InvokeUnknownFunction(t *testing.T) {
	acceptance := new(AcceptanceAsset)
	stub := shim.NewMockStub("acceptance", acceptance)
	checkInit(t, stub, [][]byte{[]byte("init")})
	checkInvokeUnknownFunction(t, stub, [][]byte{[]byte("myFunction"), []byte("docType:Asset")})
}

//TestAcceptanceAsset_Invoke_addNewAcceptance
func TestAcceptanceAsset_Invoke_addNewAcceptanceOK(t *testing.T) {
	acceptance := new(AcceptanceAsset)
	stub := shim.NewMockStub("acceptance", acceptance)
	checkInit(t, stub, [][]byte{[]byte("init")})
	checkInvoke(t, stub, getFirstAcceptanceAssetForTesting())
	newAcceptanceID := "ACPT1001"
	checkState(t, stub, newAcceptanceID, getNewAcceptanceExpected())
	checkState(t, stub, "acceptanceIDIndex", getExpectedAcceptanceIDIndex("addNewAcceptance"))
}

//TestAcceptanceAsset_Invoke_addNewAcceptance
func TestAcceptanceAsset_Invoke_addNewAcceptanceDuplicate(t *testing.T) {
	acceptance := new(AcceptanceAsset)
	stub := shim.NewMockStub("acceptance", acceptance)
	checkInit(t, stub, [][]byte{[]byte("init")})
	checkInvoke(t, stub, getFirstAcceptanceAssetForTesting())
	newAcceptanceID := "ACPT1001"
	checkState(t, stub, newAcceptanceID, getNewAcceptanceExpected())
	checkState(t, stub, "acceptanceIDIndex", getExpectedAcceptanceIDIndex("addNewAcceptance"))
	res := stub.MockInvoke("1", getFirstAcceptanceAssetForTesting())
	if res.Status != shim.OK {
		checkError(t, "This Acceptance already exists: ACPT1001", res.Message)
	} else {
		fmt.Println("Error was expected, but not raised")
		t.FailNow()
	}
}

//TestAcceptanceAsset_Invoke_removeAcceptanceOK  //change template
func TestAcceptanceAsset_Invoke_removeAcceptanceOK(t *testing.T) {
	acceptance := new(AcceptanceAsset)
	stub := shim.NewMockStub("acceptance", acceptance)
	checkInit(t, stub, [][]byte{[]byte("init")})
	checkInvoke(t, stub, getFirstAcceptanceAssetForTesting())
	checkInvoke(t, stub, getSecondAcceptanceAssetForTesting())
	checkReadAllAcceptancesOK(t, stub)
	checkState(t, stub, "acceptanceIDIndex", getExpectedAcceptanceIDIndex("beforeRemoveAcceptance"))
	checkInvoke(t, stub, getRemoveSecondAcceptanceAssetForTesting())
	remainingAcceptanceID := "ACPT1001"
	checkReadAcceptanceOK(t, stub, remainingAcceptanceID)
	checkState(t, stub, "acceptanceIDIndex", getExpectedAcceptanceIDIndex("afterRemoveAcceptance"))
}

//TestAcceptanceAsset_Invoke_removeAcceptanceNOK  //change template
func TestAcceptanceAsset_Invoke_removeAcceptanceNOK(t *testing.T) {
	acceptance := new(AcceptanceAsset)
	stub := shim.NewMockStub("acceptance", acceptance)
	checkInit(t, stub, [][]byte{[]byte("init")})
	checkInvoke(t, stub, getFirstAcceptanceAssetForTesting())
	firstAcceptanceID := "ACPT1001"
	checkReadAcceptanceOK(t, stub, firstAcceptanceID)
	checkState(t, stub, "acceptanceIDIndex", getExpectedAcceptanceIDIndex("addNewAcceptance"))
	res := stub.MockInvoke("1", getRemoveSecondAcceptanceAssetForTesting())
	if res.Status != shim.OK {
		checkError(t, "Acceptance with ID: "+"ACPT1002"+" not found", res.Message)
	} else {
		fmt.Println("Error was expected, but not raised")
		t.FailNow()
	}
	checkState(t, stub, "acceptanceIDIndex", getExpectedAcceptanceIDIndex("addNewAcceptance"))
}

//TestAcceptanceAsset_Invoke_removeAllAcceptancesOK  //change template
func TestAcceptanceAsset_Invoke_removeAllAcceptancesOK(t *testing.T) {
	acceptance := new(AcceptanceAsset)
	stub := shim.NewMockStub("acceptance", acceptance)
	checkInit(t, stub, [][]byte{[]byte("init")})
	checkInvoke(t, stub, getFirstAcceptanceAssetForTesting())
	checkInvoke(t, stub, getSecondAcceptanceAssetForTesting())
	checkReadAllAcceptancesOK(t, stub)
	checkState(t, stub, "acceptanceIDIndex", getExpectedAcceptanceIDIndex("beforeRemoveAcceptance"))
	checkInvoke(t, stub, getRemoveAllAcceptanceAssetsForTesting())
	checkState(t, stub, "acceptanceIDIndex", getExpectedAcceptanceIDIndex(""))
}

//TestAcceptanceAsset_Invoke_removeAcceptanceNOK  //change template
func TestAcceptanceAsset_Invoke_removeAllAcceptancesNOK(t *testing.T) {
	acceptance := new(AcceptanceAsset)
	stub := shim.NewMockStub("acceptance", acceptance)
	checkInit(t, stub, [][]byte{[]byte("init")})
	res := stub.MockInvoke("1", getRemoveAllAcceptanceAssetsForTesting())
	if res.Status != shim.OK {
		checkError(t, "removeAllAcceptances: No acceptances to remove", res.Message)
	} else {
		fmt.Println("Error was expected, but not raised")
		t.FailNow()
	}
	checkState(t, stub, "acceptanceIDIndex", getExpectedAcceptanceIDIndex(""))
}

//TestAcceptanceAsset_Query_readAcceptance
func TestAcceptanceAsset_Query_readAcceptance(t *testing.T) {
	acceptance := new(AcceptanceAsset)
	stub := shim.NewMockStub("acceptance", acceptance)
	checkInit(t, stub, [][]byte{[]byte("init")})
	acceptanceID := "ACPT1001"
	checkInvoke(t, stub, getFirstAcceptanceAssetForTesting())
	checkReadAcceptanceOK(t, stub, acceptanceID)
	checkReadAcceptanceNOK(t, stub, "")
}

//TestAcceptanceAsset_Query_readAllAcceptances
func TestAcceptanceAsset_Query_readAllAcceptances(t *testing.T) {
	acceptance := new(AcceptanceAsset)
	stub := shim.NewMockStub("acceptance", acceptance)
	checkInit(t, stub, [][]byte{[]byte("init")})
	checkInvoke(t, stub, getFirstAcceptanceAssetForTesting())
	checkInvoke(t, stub, getSecondAcceptanceAssetForTesting())
	checkReadAllAcceptancesOK(t, stub)
}

/*
*
*	Helper Functions
*
 */
//Get first AcceptanceAsset for testing
func getFirstAcceptanceAssetForTesting() [][]byte {
	return [][]byte{[]byte("addNewAcceptance"),
		[]byte("{\"ID\":\"ACPT1001\",\"docType\":\"Asset.AcceptanceAsset\",\"status\":\"Accepted\",\"creationDate\":\"08/27/2018\",\"cargoTerminalOperatorID\":\"CTO1001\",\"bookingID\":\"BKG1001\",\"freightForwarderID\":\"FF1001\",\"airwayBillID\":\"AWB1001\",\"routeCollection\":[{\"fromAirport\":\"MEL\",\"toAirport\":\"KUL\",\"airlineID\":\"AX\",\"aircraftType\":\"A320\",\"flightNo\":\"8210\",\"flightDate\":\"08/26/2018\"},{\"fromAirport\":\"KUL\",\"toAirport\":\"SIN\",\"airlineID\":\"AA\",\"aircraftType\":\"A330\",\"flightNo\":\"6754\",\"flightDate\":\"08/26/2018\"}],\"freehold\":\"1345\",\"uLDCollection\":[{\"ID\":\"ULD1001\",\"code\":\"ACX\",\"operationalGrossWeight\":\"120\",\"externalVolume\":\"100\",\"internalVolume\":\"80\",\"price\":{\"amount\":\"200\",\"currency\":\"AUD\"}},{\"ID\":\"ULD1002\",\"code\":\"AEX\",\"operationalGrossWeight\":\"175\",\"externalVolume\":\"145\",\"internalVolume\":\"120\",\"price\":{\"amount\":\"225\",\"currency\":\"AUD\"}}]\r\n}")}
}

//Get second AcceptanceAsset for testing
func getSecondAcceptanceAssetForTesting() [][]byte {
	return [][]byte{[]byte("addNewAcceptance"),
		[]byte("{\"ID\":\"ACPT1002\",\"docType\":\"Asset.AcceptanceAsset\",\"status\":\"Accepted\",\"creationDate\":\"08/27/2018\",\"cargoTerminalOperatorID\":\"CTO1001\",\"bookingID\":\"BKG1001\",\"freightForwarderID\":\"FF1001\",\"airwayBillID\":\"AWB1001\",\"routeCollection\":[{\"fromAirport\":\"MEL\",\"toAirport\":\"KUL\",\"airlineID\":\"AX\",\"aircraftType\":\"A320\",\"flightNo\":\"8210\",\"flightDate\":\"08/26/2018\"},{\"fromAirport\":\"KUL\",\"toAirport\":\"SIN\",\"airlineID\":\"AA\",\"aircraftType\":\"A330\",\"flightNo\":\"6754\",\"flightDate\":\"08/26/2018\"}],\"freehold\":\"1345\",\"uLDCollection\":[{\"ID\":\"ULD1001\",\"code\":\"ACX\",\"operationalGrossWeight\":\"120\",\"externalVolume\":\"100\",\"internalVolume\":\"80\",\"price\":{\"amount\":\"200\",\"currency\":\"AUD\"}},{\"ID\":\"ULD1002\",\"code\":\"AEX\",\"operationalGrossWeight\":\"175\",\"externalVolume\":\"145\",\"internalVolume\":\"120\",\"price\":{\"amount\":\"225\",\"currency\":\"AUD\"}}]\r\n}")}
}

//Get updated AcceptanceAsset for testing OK
func getUpdatedAcceptanceAssetForTestingOK() [][]byte {
	return [][]byte{[]byte("updateAcceptance"),
		[]byte("{\"ID\":\"ACPT1001\",\"docType\":\"Asset.AcceptanceAsset\",\"status\":\"Accepted\",\"creationDate\":\"08/27/2018\",\"cargoTerminalOperatorID\":\"CTO1001\",\"bookingID\":\"BKG1001\",\"freightForwarderID\":\"FF1001\",\"airwayBillID\":\"AWB1001\",\"routeCollection\":[{\"fromAirport\":\"MEL\",\"toAirport\":\"KUL\",\"airlineID\":\"AX\",\"aircraftType\":\"A320\",\"flightNo\":\"8210\",\"flightDate\":\"08/26/2018\"},{\"fromAirport\":\"KUL\",\"toAirport\":\"SIN\",\"airlineID\":\"AA\",\"aircraftType\":\"A330\",\"flightNo\":\"6754\",\"flightDate\":\"08/26/2018\"}],\"freehold\":\"1345\",\"uLDCollection\":[{\"ID\":\"ULD1001\",\"code\":\"ACX\",\"operationalGrossWeight\":\"120\",\"externalVolume\":\"100\",\"internalVolume\":\"80\",\"price\":{\"amount\":\"200\",\"currency\":\"AUD\"}},{\"ID\":\"ULD1002\",\"code\":\"AEX\",\"operationalGrossWeight\":\"175\",\"externalVolume\":\"145\",\"internalVolume\":\"120\",\"price\":{\"amount\":\"225\",\"currency\":\"AUD\"}}]\r\n}")}
}

//Get updated AcceptanceAsset for testing NOK
func getUpdatedAcceptanceAssetForTestingNOK() [][]byte {
	return [][]byte{[]byte("updateAcceptance"),
		[]byte("{\"ID\":\"ACPT1002\",\"docType\":\"Asset.AcceptanceAsset\",\"status\":\"Accepted\",\"creationDate\":\"08/27/2018\",\"cargoTerminalOperatorID\":\"CTO1001\",\"bookingID\":\"BKG1001\",\"freightForwarderID\":\"FF1001\",\"airwayBillID\":\"AWB1001\",\"routeCollection\":[{\"fromAirport\":\"MEL\",\"toAirport\":\"KUL\",\"airlineID\":\"AX\",\"aircraftType\":\"A320\",\"flightNo\":\"8210\",\"flightDate\":\"08/26/2018\"},{\"fromAirport\":\"KUL\",\"toAirport\":\"SIN\",\"airlineID\":\"AA\",\"aircraftType\":\"A330\",\"flightNo\":\"6754\",\"flightDate\":\"08/26/2018\"}],\"freehold\":\"1345\",\"uLDCollection\":[{\"ID\":\"ULD1001\",\"code\":\"ACX\",\"operationalGrossWeight\":\"120\",\"externalVolume\":\"100\",\"internalVolume\":\"80\",\"price\":{\"amount\":\"200\",\"currency\":\"AUD\"}},{\"ID\":\"ULD1002\",\"code\":\"AEX\",\"operationalGrossWeight\":\"175\",\"externalVolume\":\"145\",\"internalVolume\":\"120\",\"price\":{\"amount\":\"225\",\"currency\":\"AUD\"}}]\r\n}")}
}

//Get remove second AcceptanceAsset for testing //change template
func getRemoveSecondAcceptanceAssetForTesting() [][]byte {
	return [][]byte{[]byte("removeAcceptance"),
		[]byte("ACPT1002")}
}

//Get remove all AcceptanceAssets for testing //change template
func getRemoveAllAcceptanceAssetsForTesting() [][]byte {
	return [][]byte{[]byte("removeAllAcceptances")}
}

//Get an expected value for testing
func getNewAcceptanceExpected() []byte {
	var route Route
	var routeCollection []Route
	var price Price
	var uLDCollection []ULD
	var uLD ULD
	var acceptance Acceptance

	route.FromAirport = "MEL"
	route.ToAirport = "KUL"
	route.AirlineID = "AX"
	route.AircraftType = "A320"
	route.FlightNo = "8210"
	route.FlightDate = "08/26/2018"
	routeCollection = append(routeCollection, route)

	route.FromAirport = "KUL"
	route.ToAirport = "SIN"
	route.AirlineID = "AA"
	route.AircraftType = "A330"
	route.FlightNo = "6754"
	route.FlightDate = "08/26/2018"
	routeCollection = append(routeCollection, route)

	price.Amount = "200"
	price.Currency = "AUD"

	uLD.ID = "ULD1001"
	uLD.Code = "ACX"
	uLD.OperationalGrossWeight = "120"
	uLD.ExternalVolume = "100"
	uLD.InternalVolume = "80"
	uLD.Price = price
	uLDCollection = append(uLDCollection, uLD)

	price.Amount = "225"
	price.Currency = "AUD"

	uLD.ID = "ULD1002"
	uLD.Code = "AEX"
	uLD.OperationalGrossWeight = "175"
	uLD.ExternalVolume = "145"
	uLD.InternalVolume = "120"
	uLD.Price = price
	uLDCollection = append(uLDCollection, uLD)

	acceptance.ID = "ACPT1001"
	acceptance.ObjectType = "Asset.AcceptanceAsset"
	acceptance.Status = "Accepted"
	acceptance.CreationDate = "08/27/2018"
	acceptance.CargoTerminalOperatorID = "CTO1001"
	acceptance.BookingID = "BKG1001"
	acceptance.FreightForwarderID = "FF1001"
	acceptance.AirwayBillID = "AWB1001"
	acceptance.RouteCollection = routeCollection
	acceptance.Freehold = "1345"
	acceptance.ULDCollection = uLDCollection

	acceptanceJSON, err := json.Marshal(acceptance)
	if err != nil {
		fmt.Println("Error converting a Acceptance record to JSON")
		return nil
	}
	return []byte(acceptanceJSON)
}

//Get expected values of Acceptances for testing
func getExpectedAcceptances() []byte {
	var route Route
	var routeCollection []Route
	var price Price
	var uLDCollection []ULD
	var uLD ULD
	var acceptances []Acceptance
	var acceptance Acceptance

	route.FromAirport = "MEL"
	route.ToAirport = "KUL"
	route.AirlineID = "AX"
	route.AircraftType = "A320"
	route.FlightNo = "8210"
	route.FlightDate = "08/26/2018"
	routeCollection = append(routeCollection, route)

	route.FromAirport = "KUL"
	route.ToAirport = "SIN"
	route.AirlineID = "AA"
	route.AircraftType = "A330"
	route.FlightNo = "6754"
	route.FlightDate = "08/26/2018"
	routeCollection = append(routeCollection, route)

	price.Amount = "200"
	price.Currency = "AUD"

	uLD.ID = "ULD1001"
	uLD.Code = "ACX"
	uLD.OperationalGrossWeight = "120"
	uLD.ExternalVolume = "100"
	uLD.InternalVolume = "80"
	uLD.Price = price
	uLDCollection = append(uLDCollection, uLD)

	price.Amount = "225"
	price.Currency = "AUD"

	uLD.ID = "ULD1002"
	uLD.Code = "AEX"
	uLD.OperationalGrossWeight = "175"
	uLD.ExternalVolume = "145"
	uLD.InternalVolume = "120"
	uLD.Price = price
	uLDCollection = append(uLDCollection, uLD)

	acceptance.ID = "ACPT1001"
	acceptance.ObjectType = "Asset.AcceptanceAsset"
	acceptance.Status = "Accepted"
	acceptance.CreationDate = "08/27/2018"
	acceptance.CargoTerminalOperatorID = "CTO1001"
	acceptance.BookingID = "BKG1001"
	acceptance.FreightForwarderID = "FF1001"
	acceptance.AirwayBillID = "AWB1001"
	acceptance.RouteCollection = routeCollection
	acceptance.Freehold = "1345"
	acceptance.ULDCollection = uLDCollection
	acceptances = append(acceptances, acceptance)

	routeCollection = nil
	uLDCollection = nil

	route.FromAirport = "MEL"
	route.ToAirport = "KUL"
	route.AirlineID = "AX"
	route.AircraftType = "A320"
	route.FlightNo = "8210"
	route.FlightDate = "08/26/2018"
	routeCollection = append(routeCollection, route)

	route.FromAirport = "KUL"
	route.ToAirport = "SIN"
	route.AirlineID = "AA"
	route.AircraftType = "A330"
	route.FlightNo = "6754"
	route.FlightDate = "08/26/2018"
	routeCollection = append(routeCollection, route)

	price.Amount = "200"
	price.Currency = "AUD"

	uLD.ID = "ULD1001"
	uLD.Code = "ACX"
	uLD.OperationalGrossWeight = "120"
	uLD.ExternalVolume = "100"
	uLD.InternalVolume = "80"
	uLD.Price = price
	uLDCollection = append(uLDCollection, uLD)

	price.Amount = "225"
	price.Currency = "AUD"

	uLD.ID = "ULD1002"
	uLD.Code = "AEX"
	uLD.OperationalGrossWeight = "175"
	uLD.ExternalVolume = "145"
	uLD.InternalVolume = "120"
	uLD.Price = price
	uLDCollection = append(uLDCollection, uLD)

	acceptance.ID = "ACPT1002"
	acceptance.ObjectType = "Asset.AcceptanceAsset"
	acceptance.Status = "Accepted"
	acceptance.CreationDate = "08/27/2018"
	acceptance.CargoTerminalOperatorID = "CTO1001"
	acceptance.BookingID = "BKG1001"
	acceptance.FreightForwarderID = "FF1001"
	acceptance.AirwayBillID = "AWB1001"
	acceptance.RouteCollection = routeCollection
	acceptance.Freehold = "1345"
	acceptance.ULDCollection = uLDCollection
	acceptances = append(acceptances, acceptance)

	acceptanceJSON, err := json.Marshal(acceptances)
	if err != nil {
		fmt.Println("Error converting acceptance records to JSON")
		return nil
	}
	return []byte(acceptanceJSON)
}

func getExpectedAcceptanceIDIndex(funcName string) []byte {
	var acceptanceIDIndex AcceptanceIDIndex
	switch funcName {
	case "addNewAcceptance":
		acceptanceIDIndex.IDs = append(acceptanceIDIndex.IDs, "ACPT1001")
		acceptanceIDIndexBytes, err := json.Marshal(acceptanceIDIndex)
		if err != nil {
			fmt.Println("Error converting AcceptanceIDIndex to JSON")
			return nil
		}
		return acceptanceIDIndexBytes
	case "beforeRemoveAcceptance":
		acceptanceIDIndex.IDs = append(acceptanceIDIndex.IDs, "ACPT1001")
		acceptanceIDIndex.IDs = append(acceptanceIDIndex.IDs, "ACPT1002")
		acceptanceIDIndexBytes, err := json.Marshal(acceptanceIDIndex)
		if err != nil {
			fmt.Println("Error converting AcceptanceIDIndex to JSON")
			return nil
		}
		return acceptanceIDIndexBytes
	case "afterRemoveAcceptance":
		acceptanceIDIndex.IDs = append(acceptanceIDIndex.IDs, "ACPT1001")
		acceptanceIDIndexBytes, err := json.Marshal(acceptanceIDIndex)
		if err != nil {
			fmt.Println("Error converting AcceptanceIDIndex to JSON")
			return nil
		}
		return acceptanceIDIndexBytes
	default:
		acceptanceIDIndexBytes, err := json.Marshal(acceptanceIDIndex)
		if err != nil {
			fmt.Println("Error converting AcceptanceIDIndex to JSON")
			return nil
		}
		return acceptanceIDIndexBytes
	}
}

//checkInit - helper to check the Initialization of chaincode: AcceptanceAsset
func checkInit(t *testing.T, stub *shim.MockStub, args [][]byte) {
	res := stub.MockInit("1", args)
	if res.Status != shim.OK {
		fmt.Println("Init failed", string(res.Message))
		t.FailNow()
	}
}

//checkState - helper for checking the chaincode state for a given stateKey afgainst an expected value
func checkState(t *testing.T, stub *shim.MockStub, stateKey string, expectedState []byte) {
	actualState := stub.State[stateKey]
	if actualState == nil {
		fmt.Println("State for ", stateKey, ": failed to get value")
		t.FailNow()
	}

	if bytes.Compare(actualState, expectedState) != 0 {
		fmt.Println("Incorrect State for " + stateKey + ": \nExpected: " + string(expectedState) + "\nActual  : " + string(actualState))
		t.FailNow()
	}
}

//checkInvoke - helper for checking Invoke of chaincode
func checkInvoke(t *testing.T, stub *shim.MockStub, args [][]byte) {
	res := stub.MockInvoke("1", args)
	if res.Status != shim.OK {
		fmt.Println("Invoke", args, "failed", string(res.Message))
		t.FailNow()
	}
}

func checkInvokeUnknownFunction(t *testing.T, stub *shim.MockStub, args [][]byte) {
	res := stub.MockInvoke("1", args)
	if res.Status != shim.OK {
		expectedErr := "Received unknown function invocation"
		actualErr := string(res.Message)
		if !(strings.Contains(actualErr, expectedErr)) {
			fmt.Println("Invoke with unknown function must throw error: ", expectedErr, "\n Actual Error :", actualErr)
			t.FailNow()
		}
	} else {
		t.FailNow()
	}
}

//checkReadAcceptanceOK - helper for positive test readAcceptance
func checkReadAcceptanceOK(t *testing.T, stub *shim.MockStub, acceptanceID string) {
	res := stub.MockInvoke("1", [][]byte{[]byte("readAcceptance"), []byte(acceptanceID)})
	if res.Status != shim.OK {
		fmt.Println("func readAcceptance with ID: ", acceptanceID, " failed"+string(res.Message))
		t.FailNow()
	}
	if res.Payload == nil {
		fmt.Println("func readAcceptance with ID: ", acceptanceID, "failed to get value")
		t.FailNow()
	}
	if bytes.Compare(getNewAcceptanceExpected(), []byte(res.Payload)) != 0 {
		fmt.Println("func readAcceptance with ID: ", acceptanceID, "Expected:", string(getNewAcceptanceExpected()), "Actual:", string(res.Payload))
		t.FailNow()
	}
}

//checkReadAcceptanceNOK - helper for negative testing of readAcceptance
func checkReadAcceptanceNOK(t *testing.T, stub *shim.MockStub, acceptanceID string) {
	//with no acceptanceID
	res := stub.MockInvoke("1", [][]byte{[]byte("readAcceptance"), []byte("")})
	if res.Status != shim.OK {
		expectedErr := "retrieveAcceptance: Corrupt acceptance record "
		actualErr := string(res.Message)
		if !(strings.Contains(actualErr, expectedErr)) {
			fmt.Println("func readAcceptance negative test: ", "Expected Error:", expectedErr, "Actual Error", actualErr)
			t.FailNow()
		}
	} else {
		t.FailNow()
	}
}

func checkReadAllAcceptancesOK(t *testing.T, stub *shim.MockStub) {
	res := stub.MockInvoke("1", [][]byte{[]byte("readAllAcceptances")})
	if res.Status != shim.OK {
		fmt.Println("func readAllAcceptances failed", string(res.Message))
		t.FailNow()
	}
	if res.Payload == nil {
		fmt.Println("func readAllAcceptances failed to get value")
		t.FailNow()
	}
	if bytes.Compare(getExpectedAcceptances(), []byte(res.Payload)) != 0 {
		fmt.Println("func readAllAcceptances Expected:\n", string(getExpectedAcceptances()), "\nActual:\n", string(res.Payload))
		t.FailNow()
	}
}

func checkError(t *testing.T, exp string, act string) {
	if strings.Compare(exp, act) != 0 {
		fmt.Println("Unexpected Error! Expecting ", exp, "\n Actual :", act)
		t.FailNow()
	}
}
