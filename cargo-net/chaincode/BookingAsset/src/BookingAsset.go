package main

//Template Version: 1.1-20180704
import (
	"encoding/json"
	"errors"
	"fmt"

	"github.com/hyperledger/fabric/core/chaincode/shim"
	"github.com/hyperledger/fabric/protos/peer"
)

var logger = shim.NewLogger("CLDChaincode")

//BookingStatus - enumeration for booking statuses (Same status enumeration for Booking)
type BookingStatus int

//Reserved - Status Reserved
const (
	Reserved BookingStatus = 1 + iota
	Confirmed
)

var bookingStatuses = [...]string{
	"Reserved",
	"Confirmed",
}

func (bookingStatus BookingStatus) bookingStatusToString() string {
	return bookingStatuses[bookingStatus-1]
}

//Price (dataType)
type Price struct {
	Amount   string `json:"amount"`
	Currency string `json:"currency"`
}

//Route (dataType)
type Route struct {
	FromAirport  string `json:"fromAirport"`
	ToAirport    string `json:"toAirport"`
	AirlineID    string `json:"airlineID"`
	AircraftType string `json:"aircraftType"`
	FlightNo     string `json:"flightNo"`
	FlightDate   string `json:"flightDate"`
}

//ULD (dataType)
type ULD struct {
	ID                     string `json:"ID"`
	Code                   string `json:"code"`
	OperationalGrossWeight string `json:"operationalGrossWeight"`
	ExternalVolume         string `json:"externalVolume"`
	InternalVolume         string `json:"internalVolume"`
	Price                  Price  `json:"price"`
}

//BookingAsset - Chaincode for asset Booking
type BookingAsset struct {
}

//Booking - Details of the asset type Booking
type Booking struct {
	ID                 string  `json:"ID"`
	ObjectType         string  `json:"docType"`
	Status             string  `json:"status"`
	CreationDate       string  `json:"creationDate"`
	FreightForwarderID string  `json:"freightForwarderID"`
	RouteCollection    []Route `json:"routeCollection"`
	Freehold           string  `json:"freehold"`
	UnitPrice          Price   `json:"unitPrice"`
	ULDCollection      []ULD   `json:"uLDCollection"`
}

//BookingIDIndex - Index on IDs for retrieval all Bookings
type BookingIDIndex struct {
	IDs []string `json:"IDs"`
}

func main() {
	err := shim.Start(new(BookingAsset))
	if err != nil {
		fmt.Printf("Error starting BookingAsset chaincode function main(): %s", err)
	} else {
		fmt.Printf("Starting BookingAsset chaincode function main() executed successfully")
	}
}

//Init - The chaincode Init function: No  arguments, only initializes a ID array as Index for retrieval of all Bookings
func (bkg *BookingAsset) Init(stub shim.ChaincodeStubInterface) peer.Response {
	var bookingIDIndex BookingIDIndex
	record, _ := stub.GetState("bookingIDIndex")
	//chaincode upgrade: Index might already exist, create new only if it does not
	if record == nil {
		bytes, _ := json.Marshal(bookingIDIndex)
		stub.PutState("bookingIDIndex", bytes)
	}
	return shim.Success(nil)
}

//Invoke - The chaincode Invoke function:
func (bkg *BookingAsset) Invoke(stub shim.ChaincodeStubInterface) peer.Response {
	function, args := stub.GetFunctionAndParameters()
	logger.Debug("function: ", function)
	switch function {
	case "addNewBooking":
		return bkg.addNewBooking(stub, args)
	case "updateBooking":
		return bkg.updateBooking(stub, args)
	case "removeBooking":
		return bkg.removeBooking(stub, args[0])
	case "removeAllBookings":
		return bkg.removeAllBookings(stub)
	case "readBooking":
		return bkg.readBooking(stub, args[0])
	case "readAllBookings":
		return bkg.readAllBookings(stub)
	default:
		return shim.Error("Received unknown function invocation")
	}
}

//Invoke Route: addNewBooking
func (bkg *BookingAsset) addNewBooking(stub shim.ChaincodeStubInterface, args []string) peer.Response {
	booking, err := getBookingFromArgs(args)
	if err != nil {
		return shim.Error("Booking Data is Corrupted")
	}
	booking.ObjectType = "Asset.BookingAsset"
	record, err := stub.GetState(booking.ID)
	if record != nil {
		return shim.Error("This Booking already exists: " + booking.ID)
	}
	_, err = bkg.saveBooking(stub, booking)
	if err != nil {
		return shim.Error(err.Error())
	}
	_, err = bkg.updateBookingIDIndex(stub, booking)
	if err != nil {
		return shim.Error(err.Error())
	}
	return shim.Success(nil)
}

//Invoke Route: updateBooking
func (bkg *BookingAsset) updateBooking(stub shim.ChaincodeStubInterface, args []string) peer.Response {
	var booking Booking

	input, err := getBookingFromArgs(args)
	if err != nil {
		return shim.Error("Booking Data is corrupted")
	}
	_, err = stub.GetState(input.ID)
	if err != nil {
		return shim.Error("Booking with ID: " + input.ID + "not found")
	}
	BookingAsByteArray, err := bkg.retrieveBooking(stub, input.ID)
	if err != nil {
		return shim.Error(err.Error())
	}
	err = json.Unmarshal(BookingAsByteArray, &booking)
	if err != nil {
		return shim.Error(err.Error())
	}
	booking = input
	_, err = bkg.saveBooking(stub, booking)
	if err != nil {
		return shim.Error(err.Error())
	}
	return shim.Success(nil)
}

//Invoke Route: removeBooking
func (bkg *BookingAsset) removeBooking(stub shim.ChaincodeStubInterface, bookingID string) peer.Response {
	_, err := bkg.deleteBooking(stub, bookingID)
	if err != nil {
		return shim.Error(err.Error())
	}
	_, err = bkg.deleteBookingIDIndex(stub, bookingID)
	if err != nil {
		return shim.Error(err.Error())
	}
	return shim.Success(nil)
}

//Invoke Route: removeAllBookings
func (bkg *BookingAsset) removeAllBookings(stub shim.ChaincodeStubInterface) peer.Response {
	var bookingIDIndex BookingIDIndex
	bytes, err := stub.GetState("bookingIDIndex")
	if err != nil {
		return shim.Error("removeAllBookings: Error getting bookingIDIndex array")
	}
	err = json.Unmarshal(bytes, &bookingIDIndex)
	if err != nil {
		return shim.Error("removeAllBookings: Error unmarshalling bookingIDIndex array JSON")
	}
	if len(bookingIDIndex.IDs) == 0 {
		return shim.Error("removeAllBookings: No bookings to remove")
	}
	for _, bookingStructID := range bookingIDIndex.IDs {
		_, err = bkg.deleteBooking(stub, bookingStructID)
		if err != nil {
			return shim.Error("Failed to remove Booking with ID: " + bookingStructID)
		}
		_, err = bkg.deleteBookingIDIndex(stub, bookingStructID)
		if err != nil {
			return shim.Error(err.Error())
		}
	}
	bkg.initHolder(stub)
	return shim.Success(nil)
}

//Query Route: readBooking
func (bkg *BookingAsset) readBooking(stub shim.ChaincodeStubInterface, bookingID string) peer.Response {
	bookingAsByteArray, err := bkg.retrieveBooking(stub, bookingID)
	if err != nil {
		return shim.Error(err.Error())
	}
	return shim.Success(bookingAsByteArray)
}

//Query Route: readAllBookings
func (bkg *BookingAsset) readAllBookings(stub shim.ChaincodeStubInterface) peer.Response {
	var bookingIDs BookingIDIndex
	bytes, err := stub.GetState("bookingIDIndex")
	if err != nil {
		return shim.Error("readAllBookings: Error getting bookingIDIndex array")
	}
	err = json.Unmarshal(bytes, &bookingIDs)
	if err != nil {
		return shim.Error("readAllBookings: Error unmarshalling bookingIDIndex array JSON")
	}
	result := "["

	var bookingAsByteArray []byte

	for _, bookingID := range bookingIDs.IDs {
		bookingAsByteArray, err = bkg.retrieveBooking(stub, bookingID)
		if err != nil {
			return shim.Error("Failed to retrieve booking with ID: " + bookingID)
		}
		result += string(bookingAsByteArray) + ","
	}
	if len(result) == 1 {
		result = "[]"
	} else {
		result = result[:len(result)-1] + "]"
	}
	return shim.Success([]byte(result))
}

//Helper: Save BookingAsset
func (bkg *BookingAsset) saveBooking(stub shim.ChaincodeStubInterface, booking Booking) (bool, error) {
	bytes, err := json.Marshal(booking)
	if err != nil {
		return false, errors.New("Error converting booking record JSON")
	}
	err = stub.PutState(booking.ID, bytes)
	if err != nil {
		return false, errors.New("Error storing Booking record")
	}
	return true, nil
}

//Helper: delete BookingAsset
func (bkg *BookingAsset) deleteBooking(stub shim.ChaincodeStubInterface, bookingID string) (bool, error) {
	_, err := bkg.retrieveBooking(stub, bookingID)
	if err != nil {
		return false, errors.New("Booking with ID: " + bookingID + " not found")
	}
	err = stub.DelState(bookingID)
	if err != nil {
		return false, errors.New("Error deleting Booking record")
	}
	return true, nil
}

//Helper: Update booking Holder - updates Index
func (bkg *BookingAsset) updateBookingIDIndex(stub shim.ChaincodeStubInterface, booking Booking) (bool, error) {
	var bookingIDs BookingIDIndex
	bytes, err := stub.GetState("bookingIDIndex")
	if err != nil {
		return false, errors.New("updateBookingIDIndex: Error getting bookingIDIndex array Index from state")
	}
	err = json.Unmarshal(bytes, &bookingIDs)
	if err != nil {
		return false, errors.New("updateBookingIDIndex: Error unmarshalling bookingIDIndex array JSON")
	}
	bookingIDs.IDs = append(bookingIDs.IDs, booking.ID)
	bytes, err = json.Marshal(bookingIDs)
	if err != nil {
		return false, errors.New("updateBookingIDIndex: Error marshalling new booking ID")
	}
	err = stub.PutState("bookingIDIndex", bytes)
	if err != nil {
		return false, errors.New("updateBookingIDIndex: Error storing new booking ID in bookingIDIndex (Index)")
	}
	return true, nil
}

//Helper: delete ID from bookingStruct Holder
func (bkg *BookingAsset) deleteBookingIDIndex(stub shim.ChaincodeStubInterface, bookingID string) (bool, error) {
	var bookingIDIndex BookingIDIndex
	bytes, err := stub.GetState("bookingIDIndex")
	if err != nil {
		return false, errors.New("deleteBookingIDIndex: Error getting bookingIDIndex array Index from state")
	}
	err = json.Unmarshal(bytes, &bookingIDIndex)
	if err != nil {
		return false, errors.New("deleteBookingIDIndex: Error unmarshalling bookingIDIndex array JSON")
	}
	bookingIDIndex.IDs, err = deleteKeyFromStringArray(bookingIDIndex.IDs, bookingID)
	if err != nil {
		return false, errors.New(err.Error())
	}
	bytes, err = json.Marshal(bookingIDIndex)
	if err != nil {
		return false, errors.New("deleteBookingIDIndex: Error marshalling new bookingStruct ID")
	}
	err = stub.PutState("bookingIDIndex", bytes)
	if err != nil {
		return false, errors.New("deleteBookingIDIndex: Error storing new bookingStruct ID in bookingIDIndex (Index)")
	}
	return true, nil
}

//Helper: Initialize truck ID Holder //change template
func (bkg *BookingAsset) initHolder(stub shim.ChaincodeStubInterface) bool {
	var bookingIDIndex BookingIDIndex
	bytes, _ := json.Marshal(bookingIDIndex)
	stub.DelState("bookingIDIndex")
	stub.PutState("bookingIDIndex", bytes)
	return true
}

//deleteKeyFromArray
func deleteKeyFromStringArray(array []string, key string) (newArray []string, err error) {
	for _, entry := range array {
		if entry != key {
			newArray = append(newArray, entry)
		}
	}
	if len(newArray) == len(array) {
		return newArray, errors.New("Specified Key: " + key + " not found in Array")
	}
	return newArray, nil
}

//Helper: Retrieve purchaser
func (bkg *BookingAsset) retrieveBooking(stub shim.ChaincodeStubInterface, bookingID string) ([]byte, error) {
	var booking Booking
	var bookingAsByteArray []byte
	bytes, err := stub.GetState(bookingID)
	if err != nil {
		return bookingAsByteArray, errors.New("retrieveBooking: Error retrieving booking with ID: " + bookingID)
	}
	err = json.Unmarshal(bytes, &booking)
	if err != nil {
		return bookingAsByteArray, errors.New("retrieveBooking: Corrupt booking record " + string(bytes))
	}
	bookingAsByteArray, err = json.Marshal(booking)
	if err != nil {
		return bookingAsByteArray, errors.New("readBooking: Invalid booking Object - Not a  valid JSON")
	}
	return bookingAsByteArray, nil
}

//getBookingFromArgs - construct a booking structure from string array of arguments
func getBookingFromArgs(args []string) (booking Booking, err error) {

	if !Valid(args[0]) {
		return booking, errors.New("Invalid json")
	}

	err = Unmarshal([]byte(args[0]), &booking)
	if err != nil {
		return booking, err
	}
	return booking, nil
}
