package main

//Template Version: 1.1-20180704
import (
	"bytes"
	"encoding/json"
	"fmt"
	"strings"
	"testing"

	"github.com/hyperledger/fabric/core/chaincode/shim"
)

//TestBookingAsset_Init
func TestBookingAsset_Init(t *testing.T) {
	booking := new(BookingAsset)
	stub := shim.NewMockStub("booking", booking)
	checkInit(t, stub, [][]byte{[]byte("init")})
	checkState(t, stub, "bookingIDIndex", getExpectedBookingIDIndex("init"))
}

//TestBookingAsset_InvokeUnknownFunction
func TestBookingAsset_InvokeUnknownFunction(t *testing.T) {
	booking := new(BookingAsset)
	stub := shim.NewMockStub("booking", booking)
	checkInit(t, stub, [][]byte{[]byte("init")})
	checkInvokeUnknownFunction(t, stub, [][]byte{[]byte("myFunction"), []byte("docType:Asset")})
}

//TestBookingAsset_Invoke_addNewBooking
func TestBookingAsset_Invoke_addNewBookingOK(t *testing.T) {
	booking := new(BookingAsset)
	stub := shim.NewMockStub("booking", booking)
	checkInit(t, stub, [][]byte{[]byte("init")})
	checkInvoke(t, stub, getFirstBookingAssetForTesting())
	newBookingID := "BKG1001"
	checkState(t, stub, newBookingID, getNewBookingExpected())
	checkState(t, stub, "bookingIDIndex", getExpectedBookingIDIndex("addNewBooking"))
}

//TestBookingAsset_Invoke_addNewBooking
func TestBookingAsset_Invoke_addNewBookingDuplicate(t *testing.T) {
	booking := new(BookingAsset)
	stub := shim.NewMockStub("booking", booking)
	checkInit(t, stub, [][]byte{[]byte("init")})
	checkInvoke(t, stub, getFirstBookingAssetForTesting())
	newBookingID := "BKG1001"
	checkState(t, stub, newBookingID, getNewBookingExpected())
	checkState(t, stub, "bookingIDIndex", getExpectedBookingIDIndex("addNewBooking"))
	res := stub.MockInvoke("1", getFirstBookingAssetForTesting())
	if res.Status != shim.OK {
		checkError(t, "This Booking already exists: BKG1001", res.Message)
	} else {
		fmt.Println("Error was expected, but not raised")
		t.FailNow()
	}
}

//TestBookingAsset_Invoke_upadteBookingOK
func TestBookingAsset_Invoke_updateBookingOK(t *testing.T) {
	availability := new(BookingAsset)
	stub := shim.NewMockStub("availability", availability)
	checkInit(t, stub, [][]byte{[]byte("init")})
	checkInvoke(t, stub, getFirstBookingAssetForTesting())
	newBookingID := "BKG1001"
	checkState(t, stub, newBookingID, getNewBookingExpected())
	checkState(t, stub, "bookingIDIndex", getExpectedBookingIDIndex("addNewBooking"))
	res := stub.MockInvoke("1", getUpdatedBookingAssetForTestingOK())
	if res.Status != shim.OK {
		fmt.Println("UpdateBooking not Successful")
		t.FailNow()
	}
}

//TestBookingAsset_Invoke_upadteBookingNOK
func TestBookingAsset_Invoke_updateBookingNOK(t *testing.T) {
	availability := new(BookingAsset)
	stub := shim.NewMockStub("availability", availability)
	checkInit(t, stub, [][]byte{[]byte("init")})
	checkInvoke(t, stub, getFirstBookingAssetForTesting())
	res := stub.MockInvoke("1", getUpdatedBookingAssetForTestingNOK())
	if res.Status != shim.OK {
		checkError(t, "retrieveBooking: Corrupt booking record ", res.Message)
	} else {
		fmt.Println("Error was expected, but not raised")
		t.FailNow()
	}
}

//TestBookingAsset_Invoke_removeBookingOK  //change template
func TestBookingAsset_Invoke_removeBookingOK(t *testing.T) {
	booking := new(BookingAsset)
	stub := shim.NewMockStub("booking", booking)
	checkInit(t, stub, [][]byte{[]byte("init")})
	checkInvoke(t, stub, getFirstBookingAssetForTesting())
	checkInvoke(t, stub, getSecondBookingAssetForTesting())
	checkReadAllBookingsOK(t, stub)
	checkState(t, stub, "bookingIDIndex", getExpectedBookingIDIndex("beforeRemoveBooking"))
	checkInvoke(t, stub, getRemoveSecondBookingAssetForTesting())
	remainingBookingID := "BKG1001"
	checkReadBookingOK(t, stub, remainingBookingID)
	checkState(t, stub, "bookingIDIndex", getExpectedBookingIDIndex("afterRemoveBooking"))
}

//TestBookingAsset_Invoke_removeBookingNOK  //change template
func TestBookingAsset_Invoke_removeBookingNOK(t *testing.T) {
	booking := new(BookingAsset)
	stub := shim.NewMockStub("booking", booking)
	checkInit(t, stub, [][]byte{[]byte("init")})
	checkInvoke(t, stub, getFirstBookingAssetForTesting())
	firstBookingID := "BKG1001"
	checkReadBookingOK(t, stub, firstBookingID)
	checkState(t, stub, "bookingIDIndex", getExpectedBookingIDIndex("addNewBooking"))
	res := stub.MockInvoke("1", getRemoveSecondBookingAssetForTesting())
	if res.Status != shim.OK {
		checkError(t, "Booking with ID: "+"BKG1002"+" not found", res.Message)
	} else {
		fmt.Println("Error was expected, but not raised")
		t.FailNow()
	}
	checkState(t, stub, "bookingIDIndex", getExpectedBookingIDIndex("addNewBooking"))
}

//TestBookingAsset_Invoke_removeAllBookingsOK  //change template
func TestBookingAsset_Invoke_removeAllBookingsOK(t *testing.T) {
	booking := new(BookingAsset)
	stub := shim.NewMockStub("booking", booking)
	checkInit(t, stub, [][]byte{[]byte("init")})
	checkInvoke(t, stub, getFirstBookingAssetForTesting())
	checkInvoke(t, stub, getSecondBookingAssetForTesting())
	checkReadAllBookingsOK(t, stub)
	checkState(t, stub, "bookingIDIndex", getExpectedBookingIDIndex("beforeRemoveBooking"))
	checkInvoke(t, stub, getRemoveAllBookingAssetsForTesting())
	checkState(t, stub, "bookingIDIndex", getExpectedBookingIDIndex(""))
}

//TestBookingAsset_Invoke_removeBookingNOK  //change template
func TestBookingAsset_Invoke_removeAllBookingsNOK(t *testing.T) {
	booking := new(BookingAsset)
	stub := shim.NewMockStub("booking", booking)
	checkInit(t, stub, [][]byte{[]byte("init")})
	res := stub.MockInvoke("1", getRemoveAllBookingAssetsForTesting())
	if res.Status != shim.OK {
		checkError(t, "removeAllBookings: No bookings to remove", res.Message)
	} else {
		fmt.Println("Error was expected, but not raised")
		t.FailNow()
	}
	checkState(t, stub, "bookingIDIndex", getExpectedBookingIDIndex(""))
}

//TestBookingAsset_Query_readBooking
func TestBookingAsset_Query_readBooking(t *testing.T) {
	booking := new(BookingAsset)
	stub := shim.NewMockStub("booking", booking)
	checkInit(t, stub, [][]byte{[]byte("init")})
	bookingID := "BKG1001"
	checkInvoke(t, stub, getFirstBookingAssetForTesting())
	checkReadBookingOK(t, stub, bookingID)
	checkReadBookingNOK(t, stub, "")
}

//TestBookingAsset_Query_readAllBookings
func TestBookingAsset_Query_readAllBookings(t *testing.T) {
	booking := new(BookingAsset)
	stub := shim.NewMockStub("booking", booking)
	checkInit(t, stub, [][]byte{[]byte("init")})
	checkInvoke(t, stub, getFirstBookingAssetForTesting())
	checkInvoke(t, stub, getSecondBookingAssetForTesting())
	checkReadAllBookingsOK(t, stub)
}

/*
*
*	Helper Functions
*
 */
//Get first BookingAsset for testing
func getFirstBookingAssetForTesting() [][]byte {
	return [][]byte{[]byte("addNewBooking"),
		[]byte("{\"ID\":\"BKG1001\",\"docType\":\"Asset.BookingAsset\",\"status\":\"Reserved\",\"creationDate\":\"08/27/2018\",\"freightForwarderID\":\"FF1001\",\"routeCollection\":[{\"fromAirport\":\"MEL\",\"toAirport\":\"KUL\",\"airlineID\":\"AX\",\"aircraftType\":\"A320\",\"flightNo\":\"8210\",\"flightDate\":\"08/26/2018\"},{\"fromAirport\":\"KUL\",\"toAirport\":\"SIN\",\"airlineID\":\"AA\",\"aircraftType\":\"A330\",\"flightNo\":\"6754\",\"flightDate\":\"08/26/2018\"}],\"freehold\":\"1345\",\"unitPrice\":{\"amount\":\"225\",\"currency\":\"AUD\"},\"uLDCollection\":[{\"ID\":\"ULD1001\",\"code\":\"ACX\",\"operationalGrossWeight\":\"120\",\"externalVolume\":\"100\",\"internalVolume\":\"80\",\"price\":{\"amount\":\"200\",\"currency\":\"AUD\"}},{\"ID\":\"ULD1002\",\"code\":\"AEX\",\"operationalGrossWeight\":\"175\",\"externalVolume\":\"145\",\"internalVolume\":\"120\",\"price\":{\"amount\":\"225\",\"currency\":\"AUD\"}}]\r\n}")}
}

//Get second BookingAsset for testing
func getSecondBookingAssetForTesting() [][]byte {
	return [][]byte{[]byte("addNewBooking"),
		[]byte("{\"ID\":\"BKG1002\",\"docType\":\"Asset.BookingAsset\",\"status\":\"Reserved\",\"creationDate\":\"08/27/2018\",\"freightForwarderID\":\"FF1001\",\"routeCollection\":[{\"fromAirport\":\"MEL\",\"toAirport\":\"KUL\",\"airlineID\":\"AX\",\"aircraftType\":\"A320\",\"flightNo\":\"8210\",\"flightDate\":\"08/26/2018\"},{\"fromAirport\":\"KUL\",\"toAirport\":\"SIN\",\"airlineID\":\"AA\",\"aircraftType\":\"A330\",\"flightNo\":\"6754\",\"flightDate\":\"08/26/2018\"}],\"freehold\":\"1345\",\"unitPrice\":{\"amount\":\"225\",\"currency\":\"AUD\"},\"uLDCollection\":[{\"ID\":\"ULD1001\",\"code\":\"ACX\",\"operationalGrossWeight\":\"120\",\"externalVolume\":\"100\",\"internalVolume\":\"80\",\"price\":{\"amount\":\"200\",\"currency\":\"AUD\"}},{\"ID\":\"ULD1002\",\"code\":\"AEX\",\"operationalGrossWeight\":\"175\",\"externalVolume\":\"145\",\"internalVolume\":\"120\",\"price\":{\"amount\":\"225\",\"currency\":\"AUD\"}}]\r\n}")}
}

//Get updated booking for testing OK
func getUpdatedBookingAssetForTestingOK() [][]byte {
	return [][]byte{[]byte("updateBooking"),
		[]byte("{\"ID\":\"BKG1001\",\"docType\":\"Asset.BookingAsset\",\"status\":\"Reserved\",\"creationDate\":\"08/27/2018\",\"freightForwarderID\":\"FF1001\",\"routeCollection\":[{\"fromAirport\":\"MEL\",\"toAirport\":\"KUL\",\"airlineID\":\"AX\",\"aircraftType\":\"A320\",\"flightNo\":\"8210\",\"flightDate\":\"08/26/2018\"},{\"fromAirport\":\"KUL\",\"toAirport\":\"SIN\",\"airlineID\":\"AA\",\"aircraftType\":\"A330\",\"flightNo\":\"6754\",\"flightDate\":\"08/26/2018\"}],\"freehold\":\"1345\",\"unitPrice\":{\"amount\":\"225\",\"currency\":\"AUD\"},\"uLDCollection\":[{\"ID\":\"ULD1001\",\"code\":\"ACX\",\"operationalGrossWeight\":\"120\",\"externalVolume\":\"100\",\"internalVolume\":\"80\",\"price\":{\"amount\":\"200\",\"currency\":\"AUD\"}},{\"ID\":\"ULD1002\",\"code\":\"AEX\",\"operationalGrossWeight\":\"175\",\"externalVolume\":\"145\",\"internalVolume\":\"120\",\"price\":{\"amount\":\"225\",\"currency\":\"AUD\"}}]\r\n}")}
}

//Get updated booking for testing NOK
func getUpdatedBookingAssetForTestingNOK() [][]byte {
	return [][]byte{[]byte("updateBooking"),
		[]byte("{\"ID\":\"BKG1002\",\"docType\":\"Asset.BookingAsset\",\"status\":\"Reserved\",\"creationDate\":\"08/27/2018\",\"freightForwarderID\":\"FF1001\",\"routeCollection\":[{\"fromAirport\":\"MEL\",\"toAirport\":\"KUL\",\"airlineID\":\"AX\",\"aircraftType\":\"A320\",\"flightNo\":\"8210\",\"flightDate\":\"08/26/2018\"},{\"fromAirport\":\"KUL\",\"toAirport\":\"SIN\",\"airlineID\":\"AA\",\"aircraftType\":\"A330\",\"flightNo\":\"6754\",\"flightDate\":\"08/26/2018\"}],\"freehold\":\"1345\",\"unitPrice\":{\"amount\":\"225\",\"currency\":\"AUD\"},\"uLDCollection\":[{\"ID\":\"ULD1001\",\"code\":\"ACX\",\"operationalGrossWeight\":\"120\",\"externalVolume\":\"100\",\"internalVolume\":\"80\",\"price\":{\"amount\":\"200\",\"currency\":\"AUD\"}},{\"ID\":\"ULD1002\",\"code\":\"AEX\",\"operationalGrossWeight\":\"175\",\"externalVolume\":\"145\",\"internalVolume\":\"120\",\"price\":{\"amount\":\"225\",\"currency\":\"AUD\"}}]\r\n}")}
}

//Get remove second BookingAsset for testing //change template
func getRemoveSecondBookingAssetForTesting() [][]byte {
	return [][]byte{[]byte("removeBooking"),
		[]byte("BKG1002")}
}

//Get remove all BookingAssets for testing //change template
func getRemoveAllBookingAssetsForTesting() [][]byte {
	return [][]byte{[]byte("removeAllBookings")}
}

//Get an expected value for testing
func getNewBookingExpected() []byte {
	var route Route
	var routeCollection []Route
	var price Price
	var uLDCollection []ULD
	var uLD ULD
	var booking Booking

	price.Amount = "200"
	price.Currency = "AUD"

	route.FromAirport = "MEL"
	route.ToAirport = "KUL"
	route.AirlineID = "AX"
	route.AircraftType = "A320"
	route.FlightNo = "8210"
	route.FlightDate = "08/26/2018"
	routeCollection = append(routeCollection, route)

	route.FromAirport = "KUL"
	route.ToAirport = "SIN"
	route.AirlineID = "AA"
	route.AircraftType = "A330"
	route.FlightNo = "6754"
	route.FlightDate = "08/26/2018"
	routeCollection = append(routeCollection, route)

	uLD.ID = "ULD1001"
	uLD.Code = "ACX"
	uLD.OperationalGrossWeight = "120"
	uLD.ExternalVolume = "100"
	uLD.InternalVolume = "80"
	uLD.Price = price
	uLDCollection = append(uLDCollection, uLD)

	price.Amount = "225"
	price.Currency = "AUD"

	uLD.ID = "ULD1002"
	uLD.Code = "AEX"
	uLD.OperationalGrossWeight = "175"
	uLD.ExternalVolume = "145"
	uLD.InternalVolume = "120"
	uLD.Price = price
	uLDCollection = append(uLDCollection, uLD)

	booking.ID = "BKG1001"
	booking.ObjectType = "Asset.BookingAsset"
	booking.Status = "Reserved"
	booking.CreationDate = "08/27/2018"
	booking.RouteCollection = routeCollection
	booking.FreightForwarderID = "FF1001"
	booking.Freehold = "1345"
	booking.UnitPrice = price
	booking.ULDCollection = uLDCollection
	bookingJSON, err := json.Marshal(booking)
	if err != nil {
		fmt.Println("Error converting a Booking record to JSON")
		return nil
	}
	return []byte(bookingJSON)
}

//Get expected values of Bookings for testing
func getExpectedBookings() []byte {
	var bookings []Booking
	var route Route
	var routeCollection []Route
	var price Price
	var uLDCollection []ULD
	var uLD ULD
	var booking Booking

	price.Amount = "200"
	price.Currency = "AUD"

	route.FromAirport = "MEL"
	route.ToAirport = "KUL"
	route.AirlineID = "AX"
	route.AircraftType = "A320"
	route.FlightNo = "8210"
	route.FlightDate = "08/26/2018"
	routeCollection = append(routeCollection, route)

	route.FromAirport = "KUL"
	route.ToAirport = "SIN"
	route.AirlineID = "AA"
	route.AircraftType = "A330"
	route.FlightNo = "6754"
	route.FlightDate = "08/26/2018"
	routeCollection = append(routeCollection, route)

	uLD.ID = "ULD1001"
	uLD.Code = "ACX"
	uLD.OperationalGrossWeight = "120"
	uLD.ExternalVolume = "100"
	uLD.InternalVolume = "80"
	uLD.Price = price
	uLDCollection = append(uLDCollection, uLD)

	price.Amount = "225"
	price.Currency = "AUD"

	uLD.ID = "ULD1002"
	uLD.Code = "AEX"
	uLD.OperationalGrossWeight = "175"
	uLD.ExternalVolume = "145"
	uLD.InternalVolume = "120"
	uLD.Price = price
	uLDCollection = append(uLDCollection, uLD)

	booking.ID = "BKG1001"
	booking.ObjectType = "Asset.BookingAsset"
	booking.Status = "Reserved"
	booking.CreationDate = "08/27/2018"
	booking.RouteCollection = routeCollection
	booking.FreightForwarderID = "FF1001"
	booking.Freehold = "1345"
	booking.UnitPrice = price
	booking.ULDCollection = uLDCollection
	bookings = append(bookings, booking)

	uLDCollection = nil
	routeCollection = nil

	price.Amount = "200"
	price.Currency = "AUD"

	route.FromAirport = "MEL"
	route.ToAirport = "KUL"
	route.AirlineID = "AX"
	route.AircraftType = "A320"
	route.FlightNo = "8210"
	route.FlightDate = "08/26/2018"
	routeCollection = append(routeCollection, route)

	route.FromAirport = "KUL"
	route.ToAirport = "SIN"
	route.AirlineID = "AA"
	route.AircraftType = "A330"
	route.FlightNo = "6754"
	route.FlightDate = "08/26/2018"
	routeCollection = append(routeCollection, route)

	uLD.ID = "ULD1001"
	uLD.Code = "ACX"
	uLD.OperationalGrossWeight = "120"
	uLD.ExternalVolume = "100"
	uLD.InternalVolume = "80"
	uLD.Price = price
	uLDCollection = append(uLDCollection, uLD)

	price.Amount = "225"
	price.Currency = "AUD"

	uLD.ID = "ULD1002"
	uLD.Code = "AEX"
	uLD.OperationalGrossWeight = "175"
	uLD.ExternalVolume = "145"
	uLD.InternalVolume = "120"
	uLD.Price = price
	uLDCollection = append(uLDCollection, uLD)

	booking.ID = "BKG1002"
	booking.ObjectType = "Asset.BookingAsset"
	booking.Status = "Reserved"
	booking.CreationDate = "08/27/2018"
	booking.RouteCollection = routeCollection
	booking.FreightForwarderID = "FF1001"
	booking.Freehold = "1345"
	booking.UnitPrice = price
	booking.ULDCollection = uLDCollection

	bookings = append(bookings, booking)

	bookingJSON, err := json.Marshal(bookings)
	if err != nil {
		fmt.Println("Error converting booking records to JSON")
		return nil
	}
	return []byte(bookingJSON)
}

func getExpectedBookingIDIndex(funcName string) []byte {
	var bookingIDIndex BookingIDIndex
	switch funcName {
	case "addNewBooking":
		bookingIDIndex.IDs = append(bookingIDIndex.IDs, "BKG1001")
		bookingIDIndexBytes, err := json.Marshal(bookingIDIndex)
		if err != nil {
			fmt.Println("Error converting BookingIDIndex to JSON")
			return nil
		}
		return bookingIDIndexBytes
	case "beforeRemoveBooking":
		bookingIDIndex.IDs = append(bookingIDIndex.IDs, "BKG1001")
		bookingIDIndex.IDs = append(bookingIDIndex.IDs, "BKG1002")
		bookingIDIndexBytes, err := json.Marshal(bookingIDIndex)
		if err != nil {
			fmt.Println("Error converting BookingIDIndex to JSON")
			return nil
		}
		return bookingIDIndexBytes
	case "afterRemoveBooking":
		bookingIDIndex.IDs = append(bookingIDIndex.IDs, "BKG1001")
		bookingIDIndexBytes, err := json.Marshal(bookingIDIndex)
		if err != nil {
			fmt.Println("Error converting BookingIDIndex to JSON")
			return nil
		}
		return bookingIDIndexBytes
	default:
		bookingIDIndexBytes, err := json.Marshal(bookingIDIndex)
		if err != nil {
			fmt.Println("Error converting BookingIDIndex to JSON")
			return nil
		}
		return bookingIDIndexBytes
	}
}

//checkInit - helper to check the Initialization of chaincode: BookingAsset
func checkInit(t *testing.T, stub *shim.MockStub, args [][]byte) {
	res := stub.MockInit("1", args)
	if res.Status != shim.OK {
		fmt.Println("Init failed", string(res.Message))
		t.FailNow()
	}
}

//checkState - helper for checking the chaincode state for a given stateKey afgainst an expected value
func checkState(t *testing.T, stub *shim.MockStub, stateKey string, expectedState []byte) {
	actualState := stub.State[stateKey]
	if actualState == nil {
		fmt.Println("State for ", stateKey, ": failed to get value")
		t.FailNow()
	}

	if bytes.Compare(actualState, expectedState) != 0 {
		fmt.Println("Incorrect State for " + stateKey + ": \nExpected: " + string(expectedState) + "\nActual  : " + string(actualState))
		t.FailNow()
	}
}

//checkInvoke - helper for checking Invoke of chaincode
func checkInvoke(t *testing.T, stub *shim.MockStub, args [][]byte) {
	res := stub.MockInvoke("1", args)
	if res.Status != shim.OK {
		fmt.Println("Invoke", args, "failed", string(res.Message))
		t.FailNow()
	}
}

func checkInvokeUnknownFunction(t *testing.T, stub *shim.MockStub, args [][]byte) {
	res := stub.MockInvoke("1", args)
	if res.Status != shim.OK {
		expectedErr := "Received unknown function invocation"
		actualErr := string(res.Message)
		if !(strings.Contains(actualErr, expectedErr)) {
			fmt.Println("Invoke with unknown function must throw error: ", expectedErr, "\n Actual Error :", actualErr)
			t.FailNow()
		}
	} else {
		t.FailNow()
	}
}

//checkReadBookingOK - helper for positive test readBooking
func checkReadBookingOK(t *testing.T, stub *shim.MockStub, bookingID string) {
	res := stub.MockInvoke("1", [][]byte{[]byte("readBooking"), []byte(bookingID)})
	if res.Status != shim.OK {
		fmt.Println("func readBooking with ID: ", bookingID, " failed"+string(res.Message))
		t.FailNow()
	}
	if res.Payload == nil {
		fmt.Println("func readBooking with ID: ", bookingID, "failed to get value")
		t.FailNow()
	}
	if bytes.Compare(getNewBookingExpected(), []byte(res.Payload)) != 0 {
		fmt.Println("func readBooking with ID: ", bookingID, "Expected:", string(getNewBookingExpected()), "Actual:", string(res.Payload))
		t.FailNow()
	}
}

//checkReadBookingNOK - helper for negative testing of readBooking
func checkReadBookingNOK(t *testing.T, stub *shim.MockStub, bookingID string) {
	//with no bookingID
	res := stub.MockInvoke("1", [][]byte{[]byte("readBooking"), []byte("")})
	if res.Status != shim.OK {
		expectedErr := "retrieveBooking: Corrupt booking record "
		actualErr := string(res.Message)
		if !(strings.Contains(actualErr, expectedErr)) {
			fmt.Println("func readBooking negative test: ", "Expected Error:", expectedErr, "Actual Error", actualErr)
			t.FailNow()
		}
	} else {
		t.FailNow()
	}
}

func checkReadAllBookingsOK(t *testing.T, stub *shim.MockStub) {
	res := stub.MockInvoke("1", [][]byte{[]byte("readAllBookings")})
	if res.Status != shim.OK {
		fmt.Println("func readAllBookings failed", string(res.Message))
		t.FailNow()
	}
	if res.Payload == nil {
		fmt.Println("func readAllBookings failed to get value")
		t.FailNow()
	}
	if bytes.Compare(getExpectedBookings(), []byte(res.Payload)) != 0 {
		fmt.Println("func readAllBookings Expected:\n", string(getExpectedBookings()), "\nActual:\n", string(res.Payload))
		t.FailNow()
	}
}

func checkError(t *testing.T, exp string, act string) {
	if strings.Compare(exp, act) != 0 {
		fmt.Println("Unexpected Error! Expecting ", exp, "\n Actual :", act)
		t.FailNow()
	}
}
