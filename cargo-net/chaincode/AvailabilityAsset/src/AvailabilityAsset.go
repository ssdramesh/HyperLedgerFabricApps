package main

//Template Version: 1.1-20180704
import (
	"encoding/json"
	"errors"
	"fmt"

	"github.com/hyperledger/fabric/core/chaincode/shim"
	"github.com/hyperledger/fabric/protos/peer"
)

var logger = shim.NewLogger("CLDChaincode")

//AvailabilityStatus - enumeration for availability statuses (Same status enumeration for Availability)
type AvailabilityStatus int

const (
	Available AvailabilityStatus = 1 + iota //Available: Cargo space is available
	Full                                    //Full: Cargo space id full
)

var availabilityStatuses = [...]string{
	"Available",
	"Full",
}

func (availabilityStatus AvailabilityStatus) availabilityStatusToString() string {
	return availabilityStatuses[availabilityStatus-1]
}

//Price (dataType)
type Price struct {
	Amount   string `json:"amount"`
	Currency string `json:"currency"`
}

type Route struct {
	FromAirport  string `json:"fromAirport"`
	ToAirport    string `json:"toAirport"`
	AirlineID    string `json:"airlineID"`
	AircraftType string `json:"aircraftType"`
	FlightNo     string `json:"flightNo"`
	FlightDate   string `json:"flightDate"`
}

type ULD struct {
	ID                     string `json:"ID"`
	Code                   string `json:"code"`
	OperationalGrossWeight string `json:"operationalGrossWeight"`
	ExternalVolume         string `json:"externalVolume"`
	InternalVolume         string `json:"internalVolume"`
	Price                  Price  `json:"price"`
}

//AvailabilityAsset - Chaincode for asset Availability
type AvailabilityAsset struct {
}

//Availability - Details of the asset type Availability
type Availability struct {
	ID            string `json:"ID"`
	ObjectType    string `json:"docType"`
	Status        string `json:"status"`
	CreationDate  string `json:"creationDate"`
	AirlineID     string `json:"airlineID"`
	Route         Route  `json:"route"`
	Freehold      string `json:"freehold"`
	UnitPrice     Price  `json:"unitPrice"`
	ULDCollection []ULD  `json:"uLDCollection"`
}

//AvailabilityIDIndex - Index on IDs for retrieval all Availabilitys
type AvailabilityIDIndex struct {
	IDs []string `json:"IDs"`
}

func main() {
	err := shim.Start(new(AvailabilityAsset))
	if err != nil {
		fmt.Printf("Error starting AvailabilityAsset chaincode function main(): %s", err)
	} else {
		fmt.Printf("Starting AvailabilityAsset chaincode function main() executed successfully")
	}
}

//Init - The chaincode Init function: No  arguments, only initializes a ID array as Index for retrieval of all Availabilitys
func (avbl *AvailabilityAsset) Init(stub shim.ChaincodeStubInterface) peer.Response {
	var availabilityIDIndex AvailabilityIDIndex
	record, _ := stub.GetState("availabilityIDIndex")
	//chaincode upgrade: Index might already exist, create new only if it does not
	if record == nil {
		bytes, _ := json.Marshal(availabilityIDIndex)
		stub.PutState("availabilityIDIndex", bytes)
	}
	return shim.Success(nil)
}

//Invoke - The chaincode Invoke function:
func (avbl *AvailabilityAsset) Invoke(stub shim.ChaincodeStubInterface) peer.Response {
	function, args := stub.GetFunctionAndParameters()
	logger.Debug("function: ", function)
	switch function {
	case "addNewAvailability":
		return avbl.addNewAvailability(stub, args)
	case "updateAvailability":
		return avbl.updateAvailability(stub, args)
	case "removeAvailability":
		return avbl.removeAvailability(stub, args[0])
	case "removeAllAvailabilitys":
		return avbl.removeAllAvailabilitys(stub)
	case "readAvailability":
		return avbl.readAvailability(stub, args[0])
	case "readAllAvailabilitys":
		return avbl.readAllAvailabilitys(stub)
	default:
		return shim.Error("Received unknown function invocation")
	}
}

//Invoke Route: addNewAvailability
func (avbl *AvailabilityAsset) addNewAvailability(stub shim.ChaincodeStubInterface, args []string) peer.Response {
	availability, err := getAvailabilityFromArgs(args)
	if err != nil {
		return shim.Error("Availability Data is corrupted")
	}
	availability.ObjectType = "Asset.AvailabilityAsset"
	record, err := stub.GetState(availability.ID)
	if record != nil {
		return shim.Error("This Availability already exists: " + availability.ID)
	}
	_, err = avbl.saveAvailability(stub, availability)
	if err != nil {
		return shim.Error(err.Error())
	}
	_, err = avbl.updateAvailabilityIDIndex(stub, availability)
	if err != nil {
		return shim.Error(err.Error())
	}
	return shim.Success(nil)
}

//Invoke Route: updateAvailability
func (avbl *AvailabilityAsset) updateAvailability(stub shim.ChaincodeStubInterface, args []string) peer.Response {
	var availability Availability

	input, err := getAvailabilityFromArgs(args)
	if err != nil {
		return shim.Error("Availability Data is corrupted")
	}
	_, err = stub.GetState(input.ID)
	if err != nil {
		return shim.Error("Availability with ID: " + input.ID + "not found")
	}
	AvailabilityAsByteArray, err := avbl.retrieveAvailability(stub, input.ID)
	if err != nil {
		return shim.Error(err.Error())
	}
	err = json.Unmarshal(AvailabilityAsByteArray, &availability)
	if err != nil {
		return shim.Error(err.Error())
	}
	availability = input
	_, err = avbl.saveAvailability(stub, availability)
	if err != nil {
		return shim.Error(err.Error())
	}
	return shim.Success(nil)
}

//Invoke Route: removeAvailability
func (avbl *AvailabilityAsset) removeAvailability(stub shim.ChaincodeStubInterface, availabilityID string) peer.Response {
	_, err := avbl.deleteAvailability(stub, availabilityID)
	if err != nil {
		return shim.Error(err.Error())
	}
	_, err = avbl.deleteAvailabilityIDIndex(stub, availabilityID)
	if err != nil {
		return shim.Error(err.Error())
	}
	return shim.Success(nil)
}

//Invoke Route: removeAllAvailabilitys
func (avbl *AvailabilityAsset) removeAllAvailabilitys(stub shim.ChaincodeStubInterface) peer.Response {
	var availabilityIDIndex AvailabilityIDIndex
	bytes, err := stub.GetState("availabilityIDIndex")
	if err != nil {
		return shim.Error("removeAllAvailabilitys: Error getting availabilityIDIndex array")
	}
	err = json.Unmarshal(bytes, &availabilityIDIndex)
	if err != nil {
		return shim.Error("removeAllAvailabilitys: Error unmarshalling availabilityIDIndex array JSON")
	}
	if len(availabilityIDIndex.IDs) == 0 {
		return shim.Error("removeAllAvailabilitys: No availabilitys to remove")
	}
	for _, availabilityStructID := range availabilityIDIndex.IDs {
		_, err = avbl.deleteAvailability(stub, availabilityStructID)
		if err != nil {
			return shim.Error("Failed to remove Availability with ID: " + availabilityStructID)
		}
		_, err = avbl.deleteAvailabilityIDIndex(stub, availabilityStructID)
		if err != nil {
			return shim.Error(err.Error())
		}
	}
	avbl.initHolder(stub)
	return shim.Success(nil)
}

//Query Route: readAvailability
func (avbl *AvailabilityAsset) readAvailability(stub shim.ChaincodeStubInterface, availabilityID string) peer.Response {
	availabilityAsByteArray, err := avbl.retrieveAvailability(stub, availabilityID)
	if err != nil {
		return shim.Error(err.Error())
	}
	return shim.Success(availabilityAsByteArray)
}

//Query Route: readAllAvailabilitys
func (avbl *AvailabilityAsset) readAllAvailabilitys(stub shim.ChaincodeStubInterface) peer.Response {
	var availabilityIDs AvailabilityIDIndex
	bytes, err := stub.GetState("availabilityIDIndex")
	if err != nil {
		return shim.Error("readAllAvailabilitys: Error getting availabilityIDIndex array")
	}
	err = json.Unmarshal(bytes, &availabilityIDs)
	if err != nil {
		return shim.Error("readAllAvailabilitys: Error unmarshalling availabilityIDIndex array JSON")
	}
	result := "["

	var availabilityAsByteArray []byte

	for _, availabilityID := range availabilityIDs.IDs {
		availabilityAsByteArray, err = avbl.retrieveAvailability(stub, availabilityID)
		if err != nil {
			return shim.Error("Failed to retrieve availability with ID: " + availabilityID)
		}
		result += string(availabilityAsByteArray) + ","
	}
	if len(result) == 1 {
		result = "[]"
	} else {
		result = result[:len(result)-1] + "]"
	}
	return shim.Success([]byte(result))
}

//Helper: Save AvailabilityAsset
func (avbl *AvailabilityAsset) saveAvailability(stub shim.ChaincodeStubInterface, availability Availability) (bool, error) {
	bytes, err := json.Marshal(availability)
	if err != nil {
		return false, errors.New("Error converting availability record JSON")
	}
	err = stub.PutState(availability.ID, bytes)
	if err != nil {
		return false, errors.New("Error storing Availability record")
	}
	return true, nil
}

//Helper: delete AvailabilityAsset
func (avbl *AvailabilityAsset) deleteAvailability(stub shim.ChaincodeStubInterface, availabilityID string) (bool, error) {
	_, err := avbl.retrieveAvailability(stub, availabilityID)
	if err != nil {
		return false, errors.New("Availability with ID: " + availabilityID + " not found")
	}
	err = stub.DelState(availabilityID)
	if err != nil {
		return false, errors.New("Error deleting Availability record")
	}
	return true, nil
}

//Helper: Update availability Holder - updates Index
func (avbl *AvailabilityAsset) updateAvailabilityIDIndex(stub shim.ChaincodeStubInterface, availability Availability) (bool, error) {
	var availabilityIDs AvailabilityIDIndex
	bytes, err := stub.GetState("availabilityIDIndex")
	if err != nil {
		return false, errors.New("updateAvailabilityIDIndex: Error getting availabilityIDIndex array Index from state")
	}
	err = json.Unmarshal(bytes, &availabilityIDs)
	if err != nil {
		return false, errors.New("updateAvailabilityIDIndex: Error unmarshalling availabilityIDIndex array JSON")
	}
	availabilityIDs.IDs = append(availabilityIDs.IDs, availability.ID)
	bytes, err = json.Marshal(availabilityIDs)
	if err != nil {
		return false, errors.New("updateAvailabilityIDIndex: Error marshalling new availability ID")
	}
	err = stub.PutState("availabilityIDIndex", bytes)
	if err != nil {
		return false, errors.New("updateAvailabilityIDIndex: Error storing new availability ID in availabilityIDIndex (Index)")
	}
	return true, nil
}

//Helper: delete ID from availabilityStruct Holder
func (avbl *AvailabilityAsset) deleteAvailabilityIDIndex(stub shim.ChaincodeStubInterface, availabilityID string) (bool, error) {
	var availabilityIDIndex AvailabilityIDIndex
	bytes, err := stub.GetState("availabilityIDIndex")
	if err != nil {
		return false, errors.New("deleteAvailabilityIDIndex: Error getting availabilityIDIndex array Index from state")
	}
	err = json.Unmarshal(bytes, &availabilityIDIndex)
	if err != nil {
		return false, errors.New("deleteAvailabilityIDIndex: Error unmarshalling availabilityIDIndex array JSON")
	}
	availabilityIDIndex.IDs, err = deleteKeyFromStringArray(availabilityIDIndex.IDs, availabilityID)
	if err != nil {
		return false, errors.New(err.Error())
	}
	bytes, err = json.Marshal(availabilityIDIndex)
	if err != nil {
		return false, errors.New("deleteAvailabilityIDIndex: Error marshalling new availabilityStruct ID")
	}
	err = stub.PutState("availabilityIDIndex", bytes)
	if err != nil {
		return false, errors.New("deleteAvailabilityIDIndex: Error storing new availabilityStruct ID in availabilityIDIndex (Index)")
	}
	return true, nil
}

//Helper: Initialize truck ID Holder //change template
func (avbl *AvailabilityAsset) initHolder(stub shim.ChaincodeStubInterface) bool {
	var availabilityIDIndex AvailabilityIDIndex
	bytes, _ := json.Marshal(availabilityIDIndex)
	stub.DelState("availabilityIDIndex")
	stub.PutState("availabilityIDIndex", bytes)
	return true
}

//deleteKeyFromArray
func deleteKeyFromStringArray(array []string, key string) (newArray []string, err error) {
	for _, entry := range array {
		if entry != key {
			newArray = append(newArray, entry)
		}
	}
	if len(newArray) == len(array) {
		return newArray, errors.New("Specified Key: " + key + " not found in Array")
	}
	return newArray, nil
}

//Helper: Retrieve purchaser
func (avbl *AvailabilityAsset) retrieveAvailability(stub shim.ChaincodeStubInterface, availabilityID string) ([]byte, error) {
	var availability Availability
	var availabilityAsByteArray []byte
	bytes, err := stub.GetState(availabilityID)
	if err != nil {
		return availabilityAsByteArray, errors.New("retrieveAvailability: Error retrieving availability with ID: " + availabilityID)
	}
	err = json.Unmarshal(bytes, &availability)
	if err != nil {
		return availabilityAsByteArray, errors.New("retrieveAvailability: Corrupt availability record " + string(bytes))
	}
	availabilityAsByteArray, err = json.Marshal(availability)
	if err != nil {
		return availabilityAsByteArray, errors.New("readAvailability: Invalid availability Object - Not a  valid JSON")
	}
	return availabilityAsByteArray, nil
}

//getAvailabilityFromArgs - construct a availability structure from string array of arguments
func getAvailabilityFromArgs(args []string) (availability Availability, err error) {

	if !Valid(args[0]) {
		return availability, errors.New("Invalid json")
	}

	err = Unmarshal([]byte(args[0]), &availability)
	if err != nil {
		return availability, err
	}
	return availability, nil
}
