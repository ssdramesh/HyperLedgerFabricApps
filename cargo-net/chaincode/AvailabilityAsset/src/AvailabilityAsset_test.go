package main

//Template Version: 1.1-20180704
import (
	"bytes"
	"encoding/json"
	"fmt"
	"strings"
	"testing"

	"github.com/hyperledger/fabric/core/chaincode/shim"
)

//TestAvailabilityAsset_Init
func TestAvailabilityAsset_Init(t *testing.T) {
	availability := new(AvailabilityAsset)
	stub := shim.NewMockStub("availability", availability)
	checkInit(t, stub, [][]byte{[]byte("init")})
	checkState(t, stub, "availabilityIDIndex", getExpectedAvailabilityIDIndex("init"))
}

//TestAvailabilityAsset_InvokeUnknownFunction
func TestAvailabilityAsset_InvokeUnknownFunction(t *testing.T) {
	availability := new(AvailabilityAsset)
	stub := shim.NewMockStub("availability", availability)
	checkInit(t, stub, [][]byte{[]byte("init")})
	checkInvokeUnknownFunction(t, stub, [][]byte{[]byte("myFunction"), []byte("docType:Asset")})
}

//TestAvailabilityAsset_Invoke_addNewAvailability
func TestAvailabilityAsset_Invoke_addNewAvailabilityOK(t *testing.T) {
	availability := new(AvailabilityAsset)
	stub := shim.NewMockStub("availability", availability)
	checkInit(t, stub, [][]byte{[]byte("init")})
	checkInvoke(t, stub, getFirstAvailabilityAssetForTesting())
	newAvailabilityID := "AVAIL1001"
	checkState(t, stub, newAvailabilityID, getNewAvailabilityExpected())
	checkState(t, stub, "availabilityIDIndex", getExpectedAvailabilityIDIndex("addNewAvailability"))
}

//TestAvailabilityAsset_Invoke_addNewAvailability
func TestAvailabilityAsset_Invoke_addNewAvailabilityDuplicate(t *testing.T) {
	availability := new(AvailabilityAsset)
	stub := shim.NewMockStub("availability", availability)
	checkInit(t, stub, [][]byte{[]byte("init")})
	checkInvoke(t, stub, getFirstAvailabilityAssetForTesting())
	newAvailabilityID := "AVAIL1001"
	checkState(t, stub, newAvailabilityID, getNewAvailabilityExpected())
	checkState(t, stub, "availabilityIDIndex", getExpectedAvailabilityIDIndex("addNewAvailability"))
	res := stub.MockInvoke("1", getFirstAvailabilityAssetForTesting())
	if res.Status != shim.OK {
		checkError(t, "This Availability already exists: AVAIL1001", res.Message)
	} else {
		fmt.Println("Error was expected, but not raised")
		t.FailNow()
	}
}

//TestAvailabilityAsset_Invoke_upadteAvailabilityOK
func TestAvailabilityAsset_Invoke_updateAvailabilityOK(t *testing.T) {
	availability := new(AvailabilityAsset)
	stub := shim.NewMockStub("availability", availability)
	checkInit(t, stub, [][]byte{[]byte("init")})
	checkInvoke(t, stub, getFirstAvailabilityAssetForTesting())
	newAvailabilityID := "AVAIL1001"
	checkState(t, stub, newAvailabilityID, getNewAvailabilityExpected())
	checkState(t, stub, "availabilityIDIndex", getExpectedAvailabilityIDIndex("addNewAvailability"))
	res := stub.MockInvoke("1", getUpdatedAvailabilityAssetForTestingOK())
	if res.Status != shim.OK {
		fmt.Println("UpdateAvailability not Successful")
		t.FailNow()
	}
}

//TestAvailabilityAsset_Invoke_upadteAvailabilityNOK
func TestAvailabilityAsset_Invoke_updateAvailabilityNOK(t *testing.T) {
	availability := new(AvailabilityAsset)
	stub := shim.NewMockStub("availability", availability)
	checkInit(t, stub, [][]byte{[]byte("init")})
	checkInvoke(t, stub, getFirstAvailabilityAssetForTesting())
	res := stub.MockInvoke("1", getUpdatedAvailabilityAssetForTestingNOK())
	if res.Status != shim.OK {
		checkError(t, "retrieveAvailability: Corrupt availability record ", res.Message)
	} else {
		fmt.Println("Error was expected, but not raised")
		t.FailNow()
	}
}

//TestAvailabilityAsset_Invoke_removeAvailabilityOK  //change template
func TestAvailabilityAsset_Invoke_removeAvailabilityOK(t *testing.T) {
	availability := new(AvailabilityAsset)
	stub := shim.NewMockStub("availability", availability)
	checkInit(t, stub, [][]byte{[]byte("init")})
	checkInvoke(t, stub, getFirstAvailabilityAssetForTesting())
	checkInvoke(t, stub, getSecondAvailabilityAssetForTesting())
	checkReadAllAvailabilitysOK(t, stub)
	checkState(t, stub, "availabilityIDIndex", getExpectedAvailabilityIDIndex("beforeRemoveAvailability"))
	checkInvoke(t, stub, getRemoveSecondAvailabilityAssetForTesting())
	remainingAvailabilityID := "AVAIL1001"
	checkReadAvailabilityOK(t, stub, remainingAvailabilityID)
	checkState(t, stub, "availabilityIDIndex", getExpectedAvailabilityIDIndex("afterRemoveAvailability"))
}

//TestAvailabilityAsset_Invoke_removeAvailabilityNOK  //change template
func TestAvailabilityAsset_Invoke_removeAvailabilityNOK(t *testing.T) {
	availability := new(AvailabilityAsset)
	stub := shim.NewMockStub("availability", availability)
	checkInit(t, stub, [][]byte{[]byte("init")})
	checkInvoke(t, stub, getFirstAvailabilityAssetForTesting())
	firstAvailabilityID := "AVAIL1001"
	checkReadAvailabilityOK(t, stub, firstAvailabilityID)
	checkState(t, stub, "availabilityIDIndex", getExpectedAvailabilityIDIndex("addNewAvailability"))
	res := stub.MockInvoke("1", getRemoveSecondAvailabilityAssetForTesting())
	if res.Status != shim.OK {
		checkError(t, "Availability with ID: "+"AVAIL1002"+" not found", res.Message)
	} else {
		fmt.Println("Error was expected, but not raised")
		t.FailNow()
	}
	checkState(t, stub, "availabilityIDIndex", getExpectedAvailabilityIDIndex("addNewAvailability"))
}

//TestAvailabilityAsset_Invoke_removeAllAvailabilitysOK  //change template
func TestAvailabilityAsset_Invoke_removeAllAvailabilitysOK(t *testing.T) {
	availability := new(AvailabilityAsset)
	stub := shim.NewMockStub("availability", availability)
	checkInit(t, stub, [][]byte{[]byte("init")})
	checkInvoke(t, stub, getFirstAvailabilityAssetForTesting())
	checkInvoke(t, stub, getSecondAvailabilityAssetForTesting())
	checkReadAllAvailabilitysOK(t, stub)
	checkState(t, stub, "availabilityIDIndex", getExpectedAvailabilityIDIndex("beforeRemoveAvailability"))
	checkInvoke(t, stub, getRemoveAllAvailabilityAssetsForTesting())
	checkState(t, stub, "availabilityIDIndex", getExpectedAvailabilityIDIndex(""))
}

//TestAvailabilityAsset_Invoke_removeAvailabilityNOK  //change template
func TestAvailabilityAsset_Invoke_removeAllAvailabilitysNOK(t *testing.T) {
	availability := new(AvailabilityAsset)
	stub := shim.NewMockStub("availability", availability)
	checkInit(t, stub, [][]byte{[]byte("init")})
	res := stub.MockInvoke("1", getRemoveAllAvailabilityAssetsForTesting())
	if res.Status != shim.OK {
		checkError(t, "removeAllAvailabilitys: No availabilitys to remove", res.Message)
	} else {
		fmt.Println("Error was expected, but not raised")
		t.FailNow()
	}
	checkState(t, stub, "availabilityIDIndex", getExpectedAvailabilityIDIndex(""))
}

//TestAvailabilityAsset_Query_readAvailability
func TestAvailabilityAsset_Query_readAvailability(t *testing.T) {
	availability := new(AvailabilityAsset)
	stub := shim.NewMockStub("availability", availability)
	checkInit(t, stub, [][]byte{[]byte("init")})
	availabilityID := "AVAIL1001"
	checkInvoke(t, stub, getFirstAvailabilityAssetForTesting())
	checkReadAvailabilityOK(t, stub, availabilityID)
	checkReadAvailabilityNOK(t, stub, "")
}

//TestAvailabilityAsset_Query_readAllAvailabilitys
func TestAvailabilityAsset_Query_readAllAvailabilitys(t *testing.T) {
	availability := new(AvailabilityAsset)
	stub := shim.NewMockStub("availability", availability)
	checkInit(t, stub, [][]byte{[]byte("init")})
	checkInvoke(t, stub, getFirstAvailabilityAssetForTesting())
	checkInvoke(t, stub, getSecondAvailabilityAssetForTesting())
	checkReadAllAvailabilitysOK(t, stub)
}

/*
*
*	Helper Functions
*
 */
//Get first AvailabilityAsset for testing
func getFirstAvailabilityAssetForTesting() [][]byte {
	return [][]byte{[]byte("addNewAvailability"),
		[]byte("{\"ID\":\"AVAIL1001\",\"docType\":\"Asset.AvailabilityAsset\",\"status\":\"Available\",\"creationDate\": \"08/25/2018\",\"airlineID\":\"AX\",\"route\":{\"fromAirport\":\"MEL\",\"toAirport\":\"KUL\",\"airlineID\":\"AX\",\"aircraftType\":\"A320\",\"flightNo\":\"8210\",\"flightDate\":\"08/26/2018\"},\"freehold\":\"1245\",\"unitPrice\":{\"amount\":\"225\",\"currency\":\"AUD\"},\"uLDCollection\":[{\"ID\":\"ULD1001\",\"code\": \"ACX\",\"operationalGrossWeight\":\"120\",\"externalVolume\":\"100\",\"internalVolume\":\"80\",\"price\":{\"amount\":\"200\",\"currency\":\"AUD\"}},{\"ID\":\"ULD1002\",\"code\":\"AEX\",\"operationalGrossWeight\":\"175\",\"externalVolume\":\"145\",\"internalVolume\":\"120\",\"price\":{\"amount\":\"225\",\"currency\":\"AUD\"}}]}")}
}

//Get second AvailabilityAsset for testing
func getSecondAvailabilityAssetForTesting() [][]byte {
	return [][]byte{[]byte("addNewAvailability"),
		[]byte("{\"ID\":\"AVAIL1002\",\"docType\":\"Asset.AvailabilityAsset\",\"status\":\"Available\",\"creationDate\": \"08/25/2018\",\"airlineID\":\"AX\",\"route\":{\"fromAirport\":\"MEL\",\"toAirport\":\"KUL\",\"airlineID\":\"AX\",\"aircraftType\":\"A320\",\"flightNo\":\"8210\",\"flightDate\":\"08/26/2018\"},\"freehold\":\"1245\",\"unitPrice\":{\"amount\":\"225\",\"currency\":\"AUD\"},\"uLDCollection\":[{\"ID\":\"ULD1001\",\"code\": \"ACX\",\"operationalGrossWeight\":\"120\",\"externalVolume\":\"100\",\"internalVolume\":\"80\",\"price\":{\"amount\":\"200\",\"currency\":\"AUD\"}},{\"ID\":\"ULD1002\",\"code\":\"AEX\",\"operationalGrossWeight\":\"175\",\"externalVolume\":\"145\",\"internalVolume\":\"120\",\"price\":{\"amount\":\"225\",\"currency\":\"AUD\"}}]}")}
}

//Get updated availability for testing OK
func getUpdatedAvailabilityAssetForTestingOK() [][]byte {
	return [][]byte{[]byte("updateAvailability"),
		[]byte("{\"ID\":\"AVAIL1001\",\"docType\":\"Asset.AvailabilityAsset\",\"status\":\"Available\",\"creationDate\": \"08/25/2018\",\"airlineID\":\"AX\",\"route\":{\"fromAirport\":\"MEL\",\"toAirport\":\"KUL\",\"airlineID\":\"AX\",\"aircraftType\":\"A320\",\"flightNo\":\"8210\",\"flightDate\":\"08/26/2018\"},\"freehold\":\"1245\",\"unitPrice\":{\"amount\":\"225\",\"currency\":\"AUD\"},\"uLDCollection\":[{\"ID\":\"ULD1001\",\"code\": \"ACX\",\"operationalGrossWeight\":\"120\",\"externalVolume\":\"100\",\"internalVolume\":\"80\",\"price\":{\"amount\":\"200\",\"currency\":\"AUD\"}},{\"ID\":\"ULD1002\",\"code\":\"AEX\",\"operationalGrossWeight\":\"175\",\"externalVolume\":\"145\",\"internalVolume\":\"120\",\"price\":{\"amount\":\"225\",\"currency\":\"AUD\"}}]}")}
}

//Get updated availability for testing NOK
func getUpdatedAvailabilityAssetForTestingNOK() [][]byte {
	return [][]byte{[]byte("updateAvailability"),
		[]byte("{\"ID\":\"AVAIL1002\",\"docType\":\"Asset.AvailabilityAsset\",\"status\":\"Available\",\"creationDate\": \"08/25/2018\",\"airlineID\":\"AX\",\"route\":{\"fromAirport\":\"MEL\",\"toAirport\":\"KUL\",\"airlineID\":\"AX\",\"aircraftType\":\"A320\",\"flightNo\":\"8210\",\"flightDate\":\"08/26/2018\"},\"freehold\":\"1245\",\"unitPrice\":{\"amount\":\"225\",\"currency\":\"AUD\"},\"uLDCollection\":[{\"ID\":\"ULD1001\",\"code\": \"ACX\",\"operationalGrossWeight\":\"120\",\"externalVolume\":\"100\",\"internalVolume\":\"80\",\"price\":{\"amount\":\"200\",\"currency\":\"AUD\"}},{\"ID\":\"ULD1002\",\"code\":\"AEX\",\"operationalGrossWeight\":\"175\",\"externalVolume\":\"145\",\"internalVolume\":\"120\",\"price\":{\"amount\":\"225\",\"currency\":\"AUD\"}}]}")}
}

//Get remove second AvailabilityAsset for testing //change template
func getRemoveSecondAvailabilityAssetForTesting() [][]byte {
	return [][]byte{[]byte("removeAvailability"),
		[]byte("AVAIL1002")}
}

//Get remove all AvailabilityAssets for testing //change template
func getRemoveAllAvailabilityAssetsForTesting() [][]byte {
	return [][]byte{[]byte("removeAllAvailabilitys")}
}

//Get an expected value for testing
func getNewAvailabilityExpected() []byte {
	var route Route
	var price Price
	var uLDCollection []ULD
	var uLD ULD
	var availability Availability

	price.Amount = "200"
	price.Currency = "AUD"

	route.FromAirport = "MEL"
	route.ToAirport = "KUL"
	route.AirlineID = "AX"
	route.AircraftType = "A320"
	route.FlightNo = "8210"
	route.FlightDate = "08/26/2018"

	uLD.ID = "ULD1001"
	uLD.Code = "ACX"
	uLD.OperationalGrossWeight = "120"
	uLD.ExternalVolume = "100"
	uLD.InternalVolume = "80"
	uLD.Price = price
	uLDCollection = append(uLDCollection, uLD)

	price.Amount = "225"
	price.Currency = "AUD"

	uLD.ID = "ULD1002"
	uLD.Code = "AEX"
	uLD.OperationalGrossWeight = "175"
	uLD.ExternalVolume = "145"
	uLD.InternalVolume = "120"
	uLD.Price = price
	uLDCollection = append(uLDCollection, uLD)

	availability.ID = "AVAIL1001"
	availability.ObjectType = "Asset.AvailabilityAsset"
	availability.Status = "Available"
	availability.CreationDate = "08/25/2018"
	availability.AirlineID = "AX"
	availability.Route = route
	availability.Freehold = "1245"
	availability.UnitPrice = price
	availability.ULDCollection = uLDCollection
	availabilityJSON, err := json.Marshal(availability)
	if err != nil {
		fmt.Println("Error converting a Availability record to JSON")
		return nil
	}
	return []byte(availabilityJSON)
}

//Get expected values of Availabilitys for testing
func getExpectedAvailabilitys() []byte {
	var route Route
	var price Price
	var uLDCollection []ULD
	var uLD ULD
	var availability Availability
	var availabilitys []Availability

	price.Amount = "200"
	price.Currency = "AUD"

	route.FromAirport = "MEL"
	route.ToAirport = "KUL"
	route.AirlineID = "AX"
	route.AircraftType = "A320"
	route.FlightNo = "8210"
	route.FlightDate = "08/26/2018"

	uLD.ID = "ULD1001"
	uLD.Code = "ACX"
	uLD.OperationalGrossWeight = "120"
	uLD.ExternalVolume = "100"
	uLD.InternalVolume = "80"
	uLD.Price = price
	uLDCollection = append(uLDCollection, uLD)

	price.Amount = "225"
	price.Currency = "AUD"

	uLD.ID = "ULD1002"
	uLD.Code = "AEX"
	uLD.OperationalGrossWeight = "175"
	uLD.ExternalVolume = "145"
	uLD.InternalVolume = "120"
	uLD.Price = price
	uLDCollection = append(uLDCollection, uLD)

	availability.ID = "AVAIL1001"
	availability.ObjectType = "Asset.AvailabilityAsset"
	availability.Status = "Available"
	availability.CreationDate = "08/25/2018"
	availability.AirlineID = "AX"
	availability.Route = route
	availability.Freehold = "1245"
	availability.UnitPrice = price
	availability.ULDCollection = uLDCollection

	availabilitys = append(availabilitys, availability)

	uLDCollection = nil

	price.Amount = "200"
	price.Currency = "AUD"

	route.FromAirport = "MEL"
	route.ToAirport = "KUL"
	route.AirlineID = "AX"
	route.AircraftType = "A320"
	route.FlightNo = "8210"
	route.FlightDate = "08/26/2018"

	uLD.ID = "ULD1001"
	uLD.Code = "ACX"
	uLD.OperationalGrossWeight = "120"
	uLD.ExternalVolume = "100"
	uLD.InternalVolume = "80"
	uLD.Price = price
	uLDCollection = append(uLDCollection, uLD)

	price.Amount = "225"
	price.Currency = "AUD"

	uLD.ID = "ULD1002"
	uLD.Code = "AEX"
	uLD.OperationalGrossWeight = "175"
	uLD.ExternalVolume = "145"
	uLD.InternalVolume = "120"
	uLD.Price = price
	uLDCollection = append(uLDCollection, uLD)

	availability.ID = "AVAIL1002"
	availability.ObjectType = "Asset.AvailabilityAsset"
	availability.Status = "Available"
	availability.CreationDate = "08/25/2018"
	availability.AirlineID = "AX"
	availability.Route = route
	availability.Freehold = "1245"
	availability.UnitPrice = price
	availability.ULDCollection = uLDCollection

	availabilitys = append(availabilitys, availability)

	availabilityJSON, err := json.Marshal(availabilitys)
	if err != nil {
		fmt.Println("Error converting availability records to JSON")
		return nil
	}
	return []byte(availabilityJSON)
}

func getExpectedAvailabilityIDIndex(funcName string) []byte {
	var availabilityIDIndex AvailabilityIDIndex
	switch funcName {
	case "addNewAvailability":
		availabilityIDIndex.IDs = append(availabilityIDIndex.IDs, "AVAIL1001")
		availabilityIDIndexBytes, err := json.Marshal(availabilityIDIndex)
		if err != nil {
			fmt.Println("Error converting AvailabilityIDIndex to JSON")
			return nil
		}
		return availabilityIDIndexBytes
	case "beforeRemoveAvailability":
		availabilityIDIndex.IDs = append(availabilityIDIndex.IDs, "AVAIL1001")
		availabilityIDIndex.IDs = append(availabilityIDIndex.IDs, "AVAIL1002")
		availabilityIDIndexBytes, err := json.Marshal(availabilityIDIndex)
		if err != nil {
			fmt.Println("Error converting AvailabilityIDIndex to JSON")
			return nil
		}
		return availabilityIDIndexBytes
	case "afterRemoveAvailability":
		availabilityIDIndex.IDs = append(availabilityIDIndex.IDs, "AVAIL1001")
		availabilityIDIndexBytes, err := json.Marshal(availabilityIDIndex)
		if err != nil {
			fmt.Println("Error converting AvailabilityIDIndex to JSON")
			return nil
		}
		return availabilityIDIndexBytes
	default:
		availabilityIDIndexBytes, err := json.Marshal(availabilityIDIndex)
		if err != nil {
			fmt.Println("Error converting AvailabilityIDIndex to JSON")
			return nil
		}
		return availabilityIDIndexBytes
	}
}

//checkInit - helper to check the Initialization of chaincode: AvailabilityAsset
func checkInit(t *testing.T, stub *shim.MockStub, args [][]byte) {
	res := stub.MockInit("1", args)
	if res.Status != shim.OK {
		fmt.Println("Init failed", string(res.Message))
		t.FailNow()
	}
}

//checkState - helper for checking the chaincode state for a given stateKey afgainst an expected value
func checkState(t *testing.T, stub *shim.MockStub, stateKey string, expectedState []byte) {
	actualState := stub.State[stateKey]
	if actualState == nil {
		fmt.Println("State for ", stateKey, ": failed to get value")
		t.FailNow()
	}

	if bytes.Compare(actualState, expectedState) != 0 {
		fmt.Println("Incorrect State for " + stateKey + ": \nExpected: \n" + string(expectedState) + "\nActual  : \n" + string(actualState))
		t.FailNow()
	}
}

//checkInvoke - helper for checking Invoke of chaincode
func checkInvoke(t *testing.T, stub *shim.MockStub, args [][]byte) {
	res := stub.MockInvoke("1", args)
	if res.Status != shim.OK {
		fmt.Println("Invoke", args, "failed", string(res.Message))
		t.FailNow()
	}
}

func checkInvokeUnknownFunction(t *testing.T, stub *shim.MockStub, args [][]byte) {
	res := stub.MockInvoke("1", args)
	if res.Status != shim.OK {
		expectedErr := "Received unknown function invocation"
		actualErr := string(res.Message)
		if !(strings.Contains(actualErr, expectedErr)) {
			fmt.Println("Invoke with unknown function must throw error: ", expectedErr, " Actual Error :", actualErr)
			t.FailNow()
		}
	} else {
		t.FailNow()
	}
}

//checkReadAvailabilityOK - helper for positive test readAvailability
func checkReadAvailabilityOK(t *testing.T, stub *shim.MockStub, availabilityID string) {
	res := stub.MockInvoke("1", [][]byte{[]byte("readAvailability"), []byte(availabilityID)})
	if res.Status != shim.OK {
		fmt.Println("func readAvailability with ID: ", availabilityID, " failed"+string(res.Message))
		t.FailNow()
	}
	if res.Payload == nil {
		fmt.Println("func readAvailability with ID: ", availabilityID, "failed to get value")
		t.FailNow()
	}
	if bytes.Compare(getNewAvailabilityExpected(), []byte(res.Payload)) != 0 {
		fmt.Println("func readAvailability with ID: ", availabilityID, "Expected:", string(getNewAvailabilityExpected()), "Actual:", string(res.Payload))
		t.FailNow()
	}
}

//checkReadAvailabilityNOK - helper for negative testing of readAvailability
func checkReadAvailabilityNOK(t *testing.T, stub *shim.MockStub, availabilityID string) {
	//with no availabilityID
	res := stub.MockInvoke("1", [][]byte{[]byte("readAvailability"), []byte("")})
	if res.Status != shim.OK {
		expectedErr := "retrieveAvailability: Corrupt availability record "
		actualErr := string(res.Message)
		if !(strings.Contains(actualErr, expectedErr)) {
			fmt.Println("func readAvailability negative test: ", "Expected Error:", expectedErr, "Actual Error", actualErr)
			t.FailNow()
		}
	} else {
		t.FailNow()
	}
}

func checkReadAllAvailabilitysOK(t *testing.T, stub *shim.MockStub) {
	res := stub.MockInvoke("1", [][]byte{[]byte("readAllAvailabilitys")})
	if res.Status != shim.OK {
		fmt.Println("func readAllAvailabilitys failed", string(res.Message))
		t.FailNow()
	}
	if res.Payload == nil {
		fmt.Println("func readAllAvailabilitys failed to get value")
		t.FailNow()
	}
	if bytes.Compare(getExpectedAvailabilitys(), []byte(res.Payload)) != 0 {
		fmt.Println("func readAllAvailabilitys Expected:\n", string(getExpectedAvailabilitys()), "\nActual:\n", string(res.Payload))
		t.FailNow()
	}
}

func checkError(t *testing.T, exp string, act string) {
	if strings.Compare(exp, act) != 0 {
		fmt.Println("Unexpected Error! Expecting ", exp, " Actual :", act)
		t.FailNow()
	}
}
