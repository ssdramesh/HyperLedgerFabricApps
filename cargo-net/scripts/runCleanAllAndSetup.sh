#!/bin/bash

cd /Users/i047582/Documents/Workspaces/git/github.wdf.sap.corp/I047582/HyperLedgerFabricApps/cargo-net/test/newman/CleanAllAndSetup


echo "Clean All AvailabilityAssets before Setup..."
newman run cleanAllAvailabilityAssets.postman_collection.json -e cargo-net.postman_environment.json --bail newman
echo "Setting up AvailabilityAssets..."
newman run createAllAvailabilityAssets.postman_collection.json -e cargo-net.postman_environment.json -d ./data/AvailabilityAssets.json --bail newman


echo "Clean All BookingAssets before Setup..."
newman run cleanAllBookingAssets.postman_collection.json -e cargo-net.postman_environment.json --bail newman
echo "Setting up BookingAssets..."
newman run createAllBookingAssets.postman_collection.json -e cargo-net.postman_environment.json -d ./data/BookingAssets.json --bail newman


echo "Clean All AcceptanceAssets before Setup..."
newman run cleanAllAcceptanceAssets.postman_collection.json -e cargo-net.postman_environment.json --bail newman
echo "Setting up AcceptanceAssets..."
newman run createAllAcceptanceAssets.postman_collection.json -e cargo-net.postman_environment.json -d ./data/AcceptanceAssets.json --bail newman

echo "Done. Ready to run!"
