sap.ui.define([
	"cargo-netSetup/controller/BaseController",
	"cargo-netSetup/model/modelsBase",
	"cargo-netSetup/util/messageProvider",
	"cargo-netSetup/util/localStoreAcceptanceAssets",
	"cargo-netSetup/util/bizNetAccessAcceptanceAssets",
	"sap/ui/core/routing/History",
	"sap/ui/model/Filter",
	"sap/ui/model/FilterOperator",
	"sap/ui/Device"
], function(
		BaseController,
		modelsBase,
		messageProvider,
		localStoreAcceptanceAssets,
		bizNetAccessAcceptanceAssets,
		History,
		Filter,
		FilterOperator,
		Device
	) {
	"use strict";

	return BaseController.extend("cargo-netSetup.controller.AcceptanceAssetsOverview", {

		onInit : function(){

			var oList = this.byId("acceptanceAssetsList");
			this._oList = oList;
			this._oListFilterState = {
				aFilter : [],
				aSearch : []
			};
			localStoreAcceptanceAssets.init();
			if ( typeof this.getOwnerComponent().getModel("Messages") === "undefined" ){
				this.getOwnerComponent().setModel(modelsBase.createMessagesModel(), "Messages");
			}
			messageProvider.init(this.getOwnerComponent().getModel("Messages"));
		},

		onSelectionChange : function (oEvent) {

			this._showDetail(oEvent.getParameter("listItem") || oEvent.getSource());
		},

		onAdd : function() {

			this.getRouter().navTo("acceptanceAsset", {acceptanceAssetId : "___new"});
		},

		onRemoveAll : function(){

			bizNetAccessAcceptanceAssets.removeAllAcceptanceAssets(this.getOwnerComponent(), this.getOwnerComponent().getModel("AcceptanceAssets"));
		},

		onSearch : function(oEvent) {

			var sQuery = oEvent.getParameter("query");
			if (sQuery) {
				this._oListFilterState.aSearch = [new Filter("ID", FilterOperator.Contains, sQuery)];
			} else {
				this._oListFilterState.aSearch = [];
			}
			this._applyFilterSearch();
			var bReplace = !Device.system.phone;
			var oModel = this.getModel("AcceptanceAssets");
			oModel.setProperty(
				"/selectedAcceptanceAsset",
				_.findWhere(oModel.getProperty("/acceptanceAssetCollection/items"),
					{
						ID: oModel.getProperty("/searchAcceptanceAssetID")
					},
				this));
			this.getRouter().navTo("acceptanceAsset", {
				acceptanceAssetId : oModel.getProperty("/selectedAcceptanceAsset").ID
			}, bReplace);
		},

		_onToggleBaaS : function(){

			this.getOwnerComponent().getModel("TestSwitch").setProperty("/testMode",!(this.getView().byId("__buttonToggleAcceptanceAssets").getPressed()));
			var location = this.getRunMode().location;
			messageProvider.addMessage("Success", "Switching Network for Blockchain Data to: ", "No Description", location, 1, "", "http://www.sap.com");
			if ( this.getRunMode().testMode && localStoreAcceptanceAssets.getAcceptanceAssetData() === null ){
				localStoreAcceptanceAssets.init();
			}
			bizNetAccessAcceptanceAssets.loadAllAcceptanceAssets(this.getOwnerComponent(), this.getModel("AcceptanceAssets"));
		},

		_showDetail : function(oItem) {

			var bReplace = !Device.system.phone;
			var oModel = this.getModel("AcceptanceAssets");
			this.getRouter().navTo("acceptanceAsset", {
				acceptanceAssetId : oModel.getObject(oItem.getBindingContextPath()).ID
			}, bReplace);
		},

		_applyFilterSearch : function () {
			var aFilters = this._oListFilterState.aSearch.concat(this._oListFilterState.aFilter),
				oViewModel = this.getModel("AcceptanceAssets");
			this._oList.getBinding("items").filter(aFilters, "Application");
			if (aFilters.length !== 0) {
				oViewModel.setProperty("/noDataText", this.getResourceBundle().getText("_labelNoAcceptanceAssetsText"));
			}
		},

		onMessagePress: function() {

			this.onShowMessageDialog("cargo-netSetup Log");
		},

		onNavBack : function() {
			this.getRouter().navTo("home");
		}
	});
});
