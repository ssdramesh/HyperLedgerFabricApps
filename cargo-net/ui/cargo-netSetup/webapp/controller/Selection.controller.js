sap.ui.define([
	"cargo-netSetup/controller/BaseController",
	"cargo-netSetup/util/messageProvider",

        "cargo-netSetup/util/bizNetAccessAvailabilityAssets",
        "cargo-netSetup/model/modelsAvailabilityAssets",

        "cargo-netSetup/util/bizNetAccessBookingAssets",
        "cargo-netSetup/model/modelsBookingAssets",

        "cargo-netSetup/util/bizNetAccessAcceptanceAssets",
        "cargo-netSetup/model/modelsAcceptanceAssets",

	"cargo-netSetup/model/modelsBase"
], function(
		BaseController,
		messageProvider,

        bizNetAccessAvailabilityAssets,
        modelsAvailabilityAssets,

        bizNetAccessBookingAssets,
        modelsBookingAssets,

        bizNetAccessAcceptanceAssets,
        modelsAcceptanceAssets,

		modelsBase
	) {
	"use strict";

	return BaseController.extend("cargo-netSetup.controller.Selection", {

		onInit: function(){
			if ( typeof this.getOwnerComponent().getModel("Messages") === "undefined" ){
				this.getOwnerComponent().setModel(modelsBase.createMessagesModel(), "Messages");
			}
			messageProvider.init(this.getOwnerComponent().getModel("Messages"));
		},

		onSelect: function(oEvent) {
			switch (oEvent.getSource().getText()) {

        case "AvailabilityAssets":
          this.getView().getModel("Selection").setProperty("/entityName", "availabilityAsset");
          this.getOwnerComponent().setModel(modelsAvailabilityAssets.createAvailabilityAssetsModel(), "AvailabilityAssets");
          this.loadMetaData("availabilityAsset", this.getModel("AvailabilityAssets"));
          bizNetAccessAvailabilityAssets.loadAllAvailabilityAssets(this.getOwnerComponent(), this.getView().getModel("AvailabilityAssets"));
          this.getOwnerComponent().getRouter().navTo("availabilityAssets", {});
          break;

        case "BookingAssets":
          this.getView().getModel("Selection").setProperty("/entityName", "bookingAsset");
          this.getOwnerComponent().setModel(modelsBookingAssets.createBookingAssetsModel(), "BookingAssets");
          this.loadMetaData("bookingAsset", this.getModel("BookingAssets"));
          bizNetAccessBookingAssets.loadAllBookingAssets(this.getOwnerComponent(), this.getView().getModel("BookingAssets"));
          this.getOwnerComponent().getRouter().navTo("bookingAssets", {});
          break;

        case "AcceptanceAssets":
          this.getView().getModel("Selection").setProperty("/entityName", "acceptanceAsset");
          this.getOwnerComponent().setModel(modelsAcceptanceAssets.createAcceptanceAssetsModel(), "AcceptanceAssets");
          this.loadMetaData("acceptanceAsset", this.getModel("AcceptanceAssets"));
          bizNetAccessAcceptanceAssets.loadAllAcceptanceAssets(this.getOwnerComponent(), this.getView().getModel("AcceptanceAssets"));
          this.getOwnerComponent().getRouter().navTo("acceptanceAssets", {});
          break;

			}
		},

		removeAllEntities: function() {

			var msgStripID = this.getView().byId("__stripMessage");
			var location = this.getRunMode().location;


      if (typeof this.getOwnerComponent().getModel("AvailabilityAssets") === "undefined") {
        this.getOwnerComponent().setModel(modelsAvailabilityAssets.createAvailabilityAssetsModel(), "AvailabilityAssets");
      }
      bizNetAccessAvailabilityAssets.removeAllAvailabilityAssets(this.getOwnerComponent(), this.getView().getModel("AvailabilityAssets"));
      messageProvider.addMessage("Success", "All AvailabilityAssets deleted from sample-network", "No Description", location, 1, "", "http://www.sap.com");

      if (typeof this.getOwnerComponent().getModel("BookingAssets") === "undefined") {
        this.getOwnerComponent().setModel(modelsBookingAssets.createBookingAssetsModel(), "BookingAssets");
      }
      bizNetAccessBookingAssets.removeAllBookingAssets(this.getOwnerComponent(), this.getView().getModel("BookingAssets"));
      messageProvider.addMessage("Success", "All BookingAssets deleted from sample-network", "No Description", location, 1, "", "http://www.sap.com");

      if (typeof this.getOwnerComponent().getModel("AcceptanceAssets") === "undefined") {
        this.getOwnerComponent().setModel(modelsAcceptanceAssets.createAcceptanceAssetsModel(), "AcceptanceAssets");
      }
      bizNetAccessAcceptanceAssets.removeAllAcceptanceAssets(this.getOwnerComponent(), this.getView().getModel("AcceptanceAssets"));
      messageProvider.addMessage("Success", "All AcceptanceAssets deleted from sample-network", "No Description", location, 1, "", "http://www.sap.com");


			this.showMessageStrip(msgStripID,"All Existing cargo-net data deleted from SAP BaaS", "S");
		},

		onToggleBaaS: function() {

			this.onToggleRunMode(this.getView().byId("__stripMessage"));
		},

		onMessagePress: function() {

			this.onShowMessageDialog("cargo-net Maintenance Log");
		}
	});
});
