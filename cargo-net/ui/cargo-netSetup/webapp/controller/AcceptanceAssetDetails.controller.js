sap.ui.define([
	"cargo-netSetup/controller/BaseController",
	"cargo-netSetup/util/bizNetAccessAcceptanceAssets"
], function(
		BaseController,
		bizNetAccessAcceptanceAssets
	) {
	"use strict";

	return BaseController.extend("cargo-netSetup.controller.AcceptanceAssetDetails", {

		onInit : function(){

			this.getOwnerComponent().getRouter().getRoute("acceptanceAsset").attachPatternMatched(this._onObjectMatched, this);
		},

		_onObjectMatched : function (oEvent) {

			var pId = oEvent.getParameter("arguments").acceptanceAssetId;
			if ( pId === "___new" ) {
				this.getView().byId("__barAcceptanceAsset").setSelectedKey("New");
			} else {
				bizNetAccessAcceptanceAssets.loadAcceptanceAsset(this.getView().getModel("AcceptanceAssets"), oEvent.getParameter("arguments").acceptanceAssetId);
			}
		},

		addNew : function(){

			var oModel = this.getView().getModel("AcceptanceAssets");
			if ( oModel.getProperty("/newAcceptanceAsset/Alias") !== "" ||
				   oModel.getProperty("/newAcceptanceAsset/Description") !== "" ) {
				var acceptanceAssetId = bizNetAccessAcceptanceAssets.addNewAcceptanceAsset(this.getOwnerComponent(), oModel);
				this.getView().byId("__barAcceptanceAsset").setSelectedKey("Details");
				bizNetAccessAcceptanceAssets.loadAcceptanceAsset(this.getView().getModel("AcceptanceAssets"), acceptanceAssetId);
			} else {
				sap.m.MessageToast.show("Please enter valid data in all fields", {});
			}
			this._clearNewAsset();
		},

		removeAcceptanceAsset : function(){

			var oModel = this.getView().getModel("AcceptanceAssets");
			bizNetAccessAcceptanceAssets.removeAcceptanceAsset(this.getOwnerComponent(), oModel, oModel.getProperty("/selectedAcceptanceAsset/ID"));
		},

		_clearNewAsset : function(){

			this.getView().getModel("AcceptanceAssets").setProperty("/newAcceptanceAsset",{});
		}
	});

});
