sap.ui.define([
	"cargo-netSetup/controller/BaseController",
	"cargo-netSetup/util/bizNetAccessAvailabilityAssets"
], function(
		BaseController,
		bizNetAccessAvailabilityAssets
	) {
	"use strict";

	return BaseController.extend("cargo-netSetup.controller.AvailabilityAssetDetails", {

		onInit : function(){

			this.getOwnerComponent().getRouter().getRoute("availabilityAsset").attachPatternMatched(this._onObjectMatched, this);
		},

		_onObjectMatched : function (oEvent) {

			var pId = oEvent.getParameter("arguments").availabilityAssetId;
			if ( pId === "___new" ) {
				this.getView().byId("__barAvailabilityAsset").setSelectedKey("New");
			} else {
				bizNetAccessAvailabilityAssets.loadAvailabilityAsset(this.getView().getModel("AvailabilityAssets"), oEvent.getParameter("arguments").availabilityAssetId);
			}
		},

		addNew : function(){

			var oModel = this.getView().getModel("AvailabilityAssets");
			if ( oModel.getProperty("/newAvailabilityAsset/Alias") !== "" ||
				   oModel.getProperty("/newAvailabilityAsset/Description") !== "" ) {
				var availabilityAssetId = bizNetAccessAvailabilityAssets.addNewAvailabilityAsset(this.getOwnerComponent(), oModel);
				this.getView().byId("__barAvailabilityAsset").setSelectedKey("Details");
				bizNetAccessAvailabilityAssets.loadAvailabilityAsset(this.getView().getModel("AvailabilityAssets"), availabilityAssetId);
			} else {
				sap.m.MessageToast.show("Please enter valid data in all fields", {});
			}
			this._clearNewAsset();
		},

		removeAvailabilityAsset : function(){

			var oModel = this.getView().getModel("AvailabilityAssets");
			bizNetAccessAvailabilityAssets.removeAvailabilityAsset(this.getOwnerComponent(), oModel, oModel.getProperty("/selectedAvailabilityAsset/ID"));
		},

		_clearNewAsset : function(){

			this.getView().getModel("AvailabilityAssets").setProperty("/newAvailabilityAsset",{});
		}
	});

});
