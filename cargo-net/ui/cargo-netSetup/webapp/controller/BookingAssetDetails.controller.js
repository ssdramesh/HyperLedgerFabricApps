sap.ui.define([
	"cargo-netSetup/controller/BaseController",
	"cargo-netSetup/util/bizNetAccessBookingAssets"
], function(
		BaseController,
		bizNetAccessBookingAssets
	) {
	"use strict";

	return BaseController.extend("cargo-netSetup.controller.BookingAssetDetails", {

		onInit : function(){

			this.getOwnerComponent().getRouter().getRoute("bookingAsset").attachPatternMatched(this._onObjectMatched, this);
		},

		_onObjectMatched : function (oEvent) {

			var pId = oEvent.getParameter("arguments").bookingAssetId;
			if ( pId === "___new" ) {
				this.getView().byId("__barBookingAsset").setSelectedKey("New");
			} else {
				bizNetAccessBookingAssets.loadBookingAsset(this.getView().getModel("BookingAssets"), oEvent.getParameter("arguments").bookingAssetId);
			}
		},

		addNew : function(){

			var oModel = this.getView().getModel("BookingAssets");
			if ( oModel.getProperty("/newBookingAsset/Alias") !== "" ||
				   oModel.getProperty("/newBookingAsset/Description") !== "" ) {
				var bookingAssetId = bizNetAccessBookingAssets.addNewBookingAsset(this.getOwnerComponent(), oModel);
				this.getView().byId("__barBookingAsset").setSelectedKey("Details");
				bizNetAccessBookingAssets.loadBookingAsset(this.getView().getModel("BookingAssets"), bookingAssetId);
			} else {
				sap.m.MessageToast.show("Please enter valid data in all fields", {});
			}
			this._clearNewAsset();
		},

		removeBookingAsset : function(){

			var oModel = this.getView().getModel("BookingAssets");
			bizNetAccessBookingAssets.removeBookingAsset(this.getOwnerComponent(), oModel, oModel.getProperty("/selectedBookingAsset/ID"));
		},

		_clearNewAsset : function(){

			this.getView().getModel("BookingAssets").setProperty("/newBookingAsset",{});
		}
	});

});
