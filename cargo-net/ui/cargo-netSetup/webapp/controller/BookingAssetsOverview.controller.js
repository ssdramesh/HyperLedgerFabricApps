sap.ui.define([
	"cargo-netSetup/controller/BaseController",
	"cargo-netSetup/model/modelsBase",
	"cargo-netSetup/util/messageProvider",
	"cargo-netSetup/util/localStoreBookingAssets",
	"cargo-netSetup/util/bizNetAccessBookingAssets",
	"sap/ui/core/routing/History",
	"sap/ui/model/Filter",
	"sap/ui/model/FilterOperator",
	"sap/ui/Device"
], function(
		BaseController,
		modelsBase,
		messageProvider,
		localStoreBookingAssets,
		bizNetAccessBookingAssets,
		History,
		Filter,
		FilterOperator,
		Device
	) {
	"use strict";

	return BaseController.extend("cargo-netSetup.controller.BookingAssetsOverview", {

		onInit : function(){

			var oList = this.byId("bookingAssetsList");
			this._oList = oList;
			this._oListFilterState = {
				aFilter : [],
				aSearch : []
			};
			localStoreBookingAssets.init();
			if ( typeof this.getOwnerComponent().getModel("Messages") === "undefined" ){
				this.getOwnerComponent().setModel(modelsBase.createMessagesModel(), "Messages");
			}
			messageProvider.init(this.getOwnerComponent().getModel("Messages"));
		},

		onSelectionChange : function (oEvent) {

			this._showDetail(oEvent.getParameter("listItem") || oEvent.getSource());
		},

		onAdd : function() {

			this.getRouter().navTo("bookingAsset", {bookingAssetId : "___new"});
		},

		onRemoveAll : function(){

			bizNetAccessBookingAssets.removeAllBookingAssets(this.getOwnerComponent(), this.getOwnerComponent().getModel("BookingAssets"));
		},

		onSearch : function(oEvent) {

			var sQuery = oEvent.getParameter("query");
			if (sQuery) {
				this._oListFilterState.aSearch = [new Filter("ID", FilterOperator.Contains, sQuery)];
			} else {
				this._oListFilterState.aSearch = [];
			}
			this._applyFilterSearch();
			var bReplace = !Device.system.phone;
			var oModel = this.getModel("BookingAssets");
			oModel.setProperty(
				"/selectedBookingAsset",
				_.findWhere(oModel.getProperty("/bookingAssetCollection/items"),
					{
						ID: oModel.getProperty("/searchBookingAssetID")
					},
				this));
			this.getRouter().navTo("bookingAsset", {
				bookingAssetId : oModel.getProperty("/selectedBookingAsset").ID
			}, bReplace);
		},

		_onToggleBaaS : function(){

			this.getOwnerComponent().getModel("TestSwitch").setProperty("/testMode",!(this.getView().byId("__buttonToggleBookingAssets").getPressed()));
			var location = this.getRunMode().location;
			messageProvider.addMessage("Success", "Switching Network for Blockchain Data to: ", "No Description", location, 1, "", "http://www.sap.com");
			if ( this.getRunMode().testMode && localStoreBookingAssets.getBookingAssetData() === null ){
				localStoreBookingAssets.init();
			}
			bizNetAccessBookingAssets.loadAllBookingAssets(this.getOwnerComponent(), this.getModel("BookingAssets"));
		},

		_showDetail : function(oItem) {

			var bReplace = !Device.system.phone;
			var oModel = this.getModel("BookingAssets");
			this.getRouter().navTo("bookingAsset", {
				bookingAssetId : oModel.getObject(oItem.getBindingContextPath()).ID
			}, bReplace);
		},

		_applyFilterSearch : function () {
			var aFilters = this._oListFilterState.aSearch.concat(this._oListFilterState.aFilter),
				oViewModel = this.getModel("BookingAssets");
			this._oList.getBinding("items").filter(aFilters, "Application");
			if (aFilters.length !== 0) {
				oViewModel.setProperty("/noDataText", this.getResourceBundle().getText("_labelNoBookingAssetsText"));
			}
		},

		onMessagePress: function() {

			this.onShowMessageDialog("cargo-netSetup Log");
		},

		onNavBack : function() {
			this.getRouter().navTo("home");
		}
	});
});
