sap.ui.define([
	"cargo-netSetup/controller/BaseController",
	"cargo-netSetup/model/modelsBase",
	"cargo-netSetup/util/messageProvider",
	"cargo-netSetup/util/localStoreAvailabilityAssets",
	"cargo-netSetup/util/bizNetAccessAvailabilityAssets",
	"sap/ui/core/routing/History",
	"sap/ui/model/Filter",
	"sap/ui/model/FilterOperator",
	"sap/ui/Device"
], function(
		BaseController,
		modelsBase,
		messageProvider,
		localStoreAvailabilityAssets,
		bizNetAccessAvailabilityAssets,
		History,
		Filter,
		FilterOperator,
		Device
	) {
	"use strict";

	return BaseController.extend("cargo-netSetup.controller.AvailabilityAssetsOverview", {

		onInit : function(){

			var oList = this.byId("availabilityAssetsList");
			this._oList = oList;
			this._oListFilterState = {
				aFilter : [],
				aSearch : []
			};
			localStoreAvailabilityAssets.init();
			if ( typeof this.getOwnerComponent().getModel("Messages") === "undefined" ){
				this.getOwnerComponent().setModel(modelsBase.createMessagesModel(), "Messages");
			}
			messageProvider.init(this.getOwnerComponent().getModel("Messages"));
		},

		onSelectionChange : function (oEvent) {

			this._showDetail(oEvent.getParameter("listItem") || oEvent.getSource());
		},

		onAdd : function() {

			this.getRouter().navTo("availabilityAsset", {availabilityAssetId : "___new"});
		},

		onRemoveAll : function(){

			bizNetAccessAvailabilityAssets.removeAllAvailabilityAssets(this.getOwnerComponent(), this.getOwnerComponent().getModel("AvailabilityAssets"));
		},

		onSearch : function(oEvent) {

			var sQuery = oEvent.getParameter("query");
			if (sQuery) {
				this._oListFilterState.aSearch = [new Filter("ID", FilterOperator.Contains, sQuery)];
			} else {
				this._oListFilterState.aSearch = [];
			}
			this._applyFilterSearch();
			var bReplace = !Device.system.phone;
			var oModel = this.getModel("AvailabilityAssets");
			oModel.setProperty(
				"/selectedAvailabilityAsset",
				_.findWhere(oModel.getProperty("/availabilityAssetCollection/items"),
					{
						ID: oModel.getProperty("/searchAvailabilityAssetID")
					},
				this));
			this.getRouter().navTo("availabilityAsset", {
				availabilityAssetId : oModel.getProperty("/selectedAvailabilityAsset").ID
			}, bReplace);
		},

		_onToggleBaaS : function(){

			this.getOwnerComponent().getModel("TestSwitch").setProperty("/testMode",!(this.getView().byId("__buttonToggleAvailabilityAssets").getPressed()));
			var location = this.getRunMode().location;
			messageProvider.addMessage("Success", "Switching Network for Blockchain Data to: ", "No Description", location, 1, "", "http://www.sap.com");
			if ( this.getRunMode().testMode && localStoreAvailabilityAssets.getAvailabilityAssetData() === null ){
				localStoreAvailabilityAssets.init();
			}
			bizNetAccessAvailabilityAssets.loadAllAvailabilityAssets(this.getOwnerComponent(), this.getModel("AvailabilityAssets"));
		},

		_showDetail : function(oItem) {

			var bReplace = !Device.system.phone;
			var oModel = this.getModel("AvailabilityAssets");
			this.getRouter().navTo("availabilityAsset", {
				availabilityAssetId : oModel.getObject(oItem.getBindingContextPath()).ID
			}, bReplace);
		},

		_applyFilterSearch : function () {
			var aFilters = this._oListFilterState.aSearch.concat(this._oListFilterState.aFilter),
				oViewModel = this.getModel("AvailabilityAssets");
			this._oList.getBinding("items").filter(aFilters, "Application");
			if (aFilters.length !== 0) {
				oViewModel.setProperty("/noDataText", this.getResourceBundle().getText("_labelNoAvailabilityAssetsText"));
			}
		},

		onMessagePress: function() {

			this.onShowMessageDialog("cargo-netSetup Log");
		},

		onNavBack : function() {
			this.getRouter().navTo("home");
		}
	});
});
