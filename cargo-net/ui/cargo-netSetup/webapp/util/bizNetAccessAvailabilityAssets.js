sap.ui.define([
	"cargo-netSetup/util/restBuilder",
	"cargo-netSetup/util/formatterAvailabilityAssets",
	"cargo-netSetup/util/localStoreAvailabilityAssets"
], function(
		restBuilder,
		formatterAvailabilityAssets,
		localStoreAvailabilityAssets
	) {
	"use strict";

	return {

		loadAllAvailabilityAssets:function(oComponent, oModel) {

			if ( !( oComponent.getModel("TestSwitch").getProperty("/testMode") ) ){
				restBuilder.execute(
					oModel.getProperty("/chaincode"),
					"readAll",
					[],
					function(responseData){
						oModel.setProperty(
							"/availabilityAssetCollection/items",
							formatterAvailabilityAssets.mapAvailabilityAssetsToModel(responseData)
						);
					});
			} else {
				oModel.setProperty(
					"/availabilityAssetCollection/items",
					formatterAvailabilityAssets.mapAvailabilityAssetsToModel(localStoreAvailabilityAssets.getAvailabilityAssetData())
				);
			}
		},

		loadAvailabilityAsset:function(oModel, selectedAvailabilityAssetID){

			oModel.setProperty(
				"/selectedAvailabilityAsset",
				_.findWhere(oModel.getProperty("/availabilityAssetCollection/items"),
					{
						ID: selectedAvailabilityAssetID
					},
				this));
		},

		addNewAvailabilityAsset:function(oComponent, oModel){

			if ( !( oComponent.getModel("TestSwitch").getProperty("/testMode") ) ){
				restBuilder.execute(
					oModel.getProperty("/chaincode"),
					"addNew",
					formatterAvailabilityAssets.mapAvailabilityAssetToChaincode(oModel, true)
				);
			}  else {
				localStoreAvailabilityAssets.put(formatterAvailabilityAssets.mapAvailabilityAssetToLocalStorage(oModel, true));
			}
			this.loadAllAvailabilityAssets(oComponent, oModel);
			return oModel.getProperty("/newAvailabilityAsset/ID");
		},

		removeAvailabilityAsset : function(oComponent, oModel, availabilityAssetID){

			if ( !( oComponent.getModel("TestSwitch").getProperty("/testMode") ) ) {
				restBuilder.execute(
					oModel.getProperty("/chaincode"),
					"remove",
					{ID:availabilityAssetID}
				);
			} else {
				localStoreAvailabilityAssets.remove(availabilityAssetID);
			}
			this.loadAllAvailabilityAssets(oComponent, oModel);
			return true;
		},

		removeAllAvailabilityAssets : function(oComponent, oModel){

			if (!(oComponent.getModel("TestSwitch").getProperty("/testMode"))) {
				restBuilder.execute(
					oModel.getProperty("/chaincode"),
					"removeAll",
					[]
				);
			} else {
				localStoreAvailabilityAssets.removeAll();
			}
			this.loadAllAvailabilityAssets(oComponent, oModel);
			oModel.setProperty("/selectedAvailabilityAsset",{});
			return true;
		}
	};
});
