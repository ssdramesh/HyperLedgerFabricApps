sap.ui.define(function() {
	"use strict";

	return {

		mapBookingAssetToModel:function(responseData){
			return {
				ID:responseData.ID,
				ObjectType:responseData.docType,
				
                Status:responseData.status,
                CreationDate:responseData.creationDate
,
                FreightForwarderID:responseData.freightForwarderID,
                RouteCollection:responseData.routeCollection,
                Freehold:responseData.freehold,
                UnitPrice:responseData.unitPrice,
                ULDCollection:responseData.uLDCollection
			};
		},

		mapBookingAssetsToModel:function(responseData){

			var items = [];
			if( responseData ){
				for ( var i = 0; i < responseData.length; i++ ){
					items.push(this.mapBookingAssetToModel(responseData[i]));
				}
			}
			return items;
		},

		mapBookingAssetToChaincode:function(oModel, newBookingAsset){

			if ( newBookingAsset === true ) {
				return {
						"ID":this.getNewBookingAssetID(oModel),
						"docType":"Asset.BookingAsset",
						
                        "status":oModel.getProperty("/newBookingAsset/Status"),
                        "creationDate":oModel.getProperty("/newBookingAsset/CreationDate")
,
                  "freightForwarderID":oModel.getProperty("/newBookingAsset/FreightForwarderID"),
                  "routeCollection":oModel.getProperty("/newBookingAsset/RouteCollection"),
                  "freehold":oModel.getProperty("/newBookingAsset/Freehold"),
                  "unitPrice":oModel.getProperty("/newBookingAsset/UnitPrice"),
                  "uLDCollection":oModel.getProperty("/newBookingAsset/ULDCollection")
				};
			} else {
				return {
						"ID":oModel.getProperty("/selectedBookingAsset/ID"),
						"docType":"Asset.BookingAsset",
						
                        "status":oModel.getProperty("/selectedBookingAsset/Status"),
                        "creationDate":oModel.getProperty("/selectedBookingAsset/CreationDate")
,
                  "freightForwarderID":oModel.getProperty("/selectedBookingAsset/FreightForwarderID"),
                  "routeCollection":oModel.getProperty("/selectedBookingAsset/RouteCollection"),
                  "freehold":oModel.getProperty("/selectedBookingAsset/Freehold"),
                  "unitPrice":oModel.getProperty("/selectedBookingAsset/UnitPrice"),
                  "uLDCollection":oModel.getProperty("/selectedBookingAsset/ULDCollection")
				};
			}
		},

		mapBookingAssetToLocalStorage : function(oModel, newBookingAsset){

			return this.mapBookingAssetToChaincode(oModel, newBookingAsset);
		},

		getNewBookingAssetID:function(oModel){

		    if ( typeof oModel.getProperty("/newBookingAsset/ID") === "undefined" ||
		    		oModel.getProperty("/newBookingAsset/ID") === ""
		    	){
			    var iD = "BookingAsset";
			    var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
			    for( var i = 0; i < 8; i++ ){
			        iD += possible.charAt(Math.floor(Math.random() * possible.length));
			    }
		    } else {
				iD = oModel.getProperty("/newBookingAsset/ID");
			}
			oModel.setProperty("/newBookingAsset/ID",iD);
		    return iD;
		}
	};
});
