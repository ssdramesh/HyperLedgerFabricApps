sap.ui.define(function() {
	"use strict";

	return {

		mapAcceptanceAssetToModel:function(responseData){
			return {
				ID:responseData.ID,
				ObjectType:responseData.docType,
				
                Status:responseData.status,
                CreationDate:responseData.creationDate
,
                CargoTerminalOperatorID:responseData.cargoTerminalOperatorID,
                BookingID:responseData.bookingID,
                FreightForwarderID:responseData.freightForwarderID,
                AirwayBillID:responseData.airwayBillID,
                RouteCollection:responseData.routeCollection,
                Freehold:responseData.freehold,
                ULDCollection:responseData.uLDCollection
			};
		},

		mapAcceptanceAssetsToModel:function(responseData){

			var items = [];
			if( responseData ){
				for ( var i = 0; i < responseData.length; i++ ){
					items.push(this.mapAcceptanceAssetToModel(responseData[i]));
				}
			}
			return items;
		},

		mapAcceptanceAssetToChaincode:function(oModel, newAcceptanceAsset){

			if ( newAcceptanceAsset === true ) {
				return {
						"ID":this.getNewAcceptanceAssetID(oModel),
						"docType":"Asset.AcceptanceAsset",
						
                        "status":oModel.getProperty("/newAcceptanceAsset/Status"),
                        "creationDate":oModel.getProperty("/newAcceptanceAsset/CreationDate")
,
                  "cargoTerminalOperatorID":oModel.getProperty("/newAcceptanceAsset/CargoTerminalOperatorID"),
                  "bookingID":oModel.getProperty("/newAcceptanceAsset/BookingID"),
                  "freightForwarderID":oModel.getProperty("/newAcceptanceAsset/FreightForwarderID"),
                  "airwayBillID":oModel.getProperty("/newAcceptanceAsset/AirwayBillID"),
                  "routeCollection":oModel.getProperty("/newAcceptanceAsset/RouteCollection"),
                  "freehold":oModel.getProperty("/newAcceptanceAsset/Freehold"),
                  "uLDCollection":oModel.getProperty("/newAcceptanceAsset/ULDCollection")
				};
			} else {
				return {
						"ID":oModel.getProperty("/selectedAcceptanceAsset/ID"),
						"docType":"Asset.AcceptanceAsset",
						
                        "status":oModel.getProperty("/selectedAcceptanceAsset/Status"),
                        "creationDate":oModel.getProperty("/selectedAcceptanceAsset/CreationDate")
,
                  "cargoTerminalOperatorID":oModel.getProperty("/selectedAcceptanceAsset/CargoTerminalOperatorID"),
                  "bookingID":oModel.getProperty("/selectedAcceptanceAsset/BookingID"),
                  "freightForwarderID":oModel.getProperty("/selectedAcceptanceAsset/FreightForwarderID"),
                  "airwayBillID":oModel.getProperty("/selectedAcceptanceAsset/AirwayBillID"),
                  "routeCollection":oModel.getProperty("/selectedAcceptanceAsset/RouteCollection"),
                  "freehold":oModel.getProperty("/selectedAcceptanceAsset/Freehold"),
                  "uLDCollection":oModel.getProperty("/selectedAcceptanceAsset/ULDCollection")
				};
			}
		},

		mapAcceptanceAssetToLocalStorage : function(oModel, newAcceptanceAsset){

			return this.mapAcceptanceAssetToChaincode(oModel, newAcceptanceAsset);
		},

		getNewAcceptanceAssetID:function(oModel){

		    if ( typeof oModel.getProperty("/newAcceptanceAsset/ID") === "undefined" ||
		    		oModel.getProperty("/newAcceptanceAsset/ID") === ""
		    	){
			    var iD = "AcceptanceAsset";
			    var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
			    for( var i = 0; i < 8; i++ ){
			        iD += possible.charAt(Math.floor(Math.random() * possible.length));
			    }
		    } else {
				iD = oModel.getProperty("/newAcceptanceAsset/ID");
			}
			oModel.setProperty("/newAcceptanceAsset/ID",iD);
		    return iD;
		}
	};
});
