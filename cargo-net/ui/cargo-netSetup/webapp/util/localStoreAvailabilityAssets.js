sap.ui.define(function() {
	"use strict";

	jQuery.sap.require("jquery.sap.storage");
	var oStorage = jQuery.sap.storage(jQuery.sap.storage.Type.local);
	var availabilityAssetsDataID = "availabilityAssets";

	return {

		init: function(){

			oStorage.put(availabilityAssetsDataID,[]);
			oStorage.put(
				availabilityAssetsDataID,
[
	{
		"ID":"AvailabilityAsset1001",
		"docType":"Asset.AvailabilityAsset",
		"status":"Created",
		"creationDate":"12/28/2107",
		"airlineID":"airlineID001",
		"route":"route001",
		"freehold":"freehold001",
		"unitPrice":"unitPrice001",
		"uLDCollection":"uLDCollection001"
	}
]				
			);
		},

		getAvailabilityAssetDataID : function(){

			return availabilityAssetsDataID;
		},

		getAvailabilityAssetData  : function(){

			return oStorage.get("availabilityAssets");
		},

		put: function(newAvailabilityAsset){

			var availabilityAssetData = this.getAvailabilityAssetData();
			availabilityAssetData.push(newAvailabilityAsset);
			oStorage.put(availabilityAssetsDataID, availabilityAssetData);
		},

		remove : function (id){

			var availabilityAssetData = this.getAvailabilityAssetData();
			availabilityAssetData = _.without(availabilityAssetData,_.findWhere(availabilityAssetData,{ID:id}));
			oStorage.put(availabilityAssetsDataID, availabilityAssetData);
		},

		removeAll : function(){

			oStorage.put(availabilityAssetsDataID,[]);
		},

		clearAvailabilityAssetData: function(){

			oStorage.put(availabilityAssetsDataID,[]);
		}
	};
});
