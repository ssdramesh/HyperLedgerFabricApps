sap.ui.define(function() {
	"use strict";

	jQuery.sap.require("jquery.sap.storage");
	var oStorage = jQuery.sap.storage(jQuery.sap.storage.Type.local);
	var acceptanceAssetsDataID = "acceptanceAssets";

	return {

		init: function(){

			oStorage.put(acceptanceAssetsDataID,[]);
			oStorage.put(
				acceptanceAssetsDataID,
[
	{
		"ID":"AcceptanceAsset1001",
		"docType":"Asset.AcceptanceAsset",
		"status":"Created",
		"creationDate":"12/28/2107",
		"cargoTerminalOperatorID":"cargoTerminalOperatorID001",
		"bookingID":"bookingID001",
		"freightForwarderID":"freightForwarderID001",
		"airwayBillID":"airwayBillID001",
		"routeCollection":"routeCollection001",
		"freehold":"freehold001",
		"uLDCollection":"uLDCollection001"
	}
]				
			);
		},

		getAcceptanceAssetDataID : function(){

			return acceptanceAssetsDataID;
		},

		getAcceptanceAssetData  : function(){

			return oStorage.get("acceptanceAssets");
		},

		put: function(newAcceptanceAsset){

			var acceptanceAssetData = this.getAcceptanceAssetData();
			acceptanceAssetData.push(newAcceptanceAsset);
			oStorage.put(acceptanceAssetsDataID, acceptanceAssetData);
		},

		remove : function (id){

			var acceptanceAssetData = this.getAcceptanceAssetData();
			acceptanceAssetData = _.without(acceptanceAssetData,_.findWhere(acceptanceAssetData,{ID:id}));
			oStorage.put(acceptanceAssetsDataID, acceptanceAssetData);
		},

		removeAll : function(){

			oStorage.put(acceptanceAssetsDataID,[]);
		},

		clearAcceptanceAssetData: function(){

			oStorage.put(acceptanceAssetsDataID,[]);
		}
	};
});
