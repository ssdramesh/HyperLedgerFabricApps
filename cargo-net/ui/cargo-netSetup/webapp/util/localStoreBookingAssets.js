sap.ui.define(function() {
	"use strict";

	jQuery.sap.require("jquery.sap.storage");
	var oStorage = jQuery.sap.storage(jQuery.sap.storage.Type.local);
	var bookingAssetsDataID = "bookingAssets";

	return {

		init: function(){

			oStorage.put(bookingAssetsDataID,[]);
			oStorage.put(
				bookingAssetsDataID,
[
	{
		"ID":"BookingAsset1001",
		"docType":"Asset.BookingAsset",
		"status":"Created",
		"creationDate":"12/28/2107",
		"freightForwarderID":"freightForwarderID001",
		"routeCollection":"routeCollection001",
		"freehold":"freehold001",
		"unitPrice":"unitPrice001",
		"uLDCollection":"uLDCollection001"
	}
]				
			);
		},

		getBookingAssetDataID : function(){

			return bookingAssetsDataID;
		},

		getBookingAssetData  : function(){

			return oStorage.get("bookingAssets");
		},

		put: function(newBookingAsset){

			var bookingAssetData = this.getBookingAssetData();
			bookingAssetData.push(newBookingAsset);
			oStorage.put(bookingAssetsDataID, bookingAssetData);
		},

		remove : function (id){

			var bookingAssetData = this.getBookingAssetData();
			bookingAssetData = _.without(bookingAssetData,_.findWhere(bookingAssetData,{ID:id}));
			oStorage.put(bookingAssetsDataID, bookingAssetData);
		},

		removeAll : function(){

			oStorage.put(bookingAssetsDataID,[]);
		},

		clearBookingAssetData: function(){

			oStorage.put(bookingAssetsDataID,[]);
		}
	};
});
