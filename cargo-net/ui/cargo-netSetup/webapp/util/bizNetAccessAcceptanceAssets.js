sap.ui.define([
	"cargo-netSetup/util/restBuilder",
	"cargo-netSetup/util/formatterAcceptanceAssets",
	"cargo-netSetup/util/localStoreAcceptanceAssets"
], function(
		restBuilder,
		formatterAcceptanceAssets,
		localStoreAcceptanceAssets
	) {
	"use strict";

	return {

		loadAllAcceptanceAssets:function(oComponent, oModel) {

			if ( !( oComponent.getModel("TestSwitch").getProperty("/testMode") ) ){
				restBuilder.execute(
					oModel.getProperty("/chaincode"),
					"readAll",
					[],
					function(responseData){
						oModel.setProperty(
							"/acceptanceAssetCollection/items",
							formatterAcceptanceAssets.mapAcceptanceAssetsToModel(responseData)
						);
					});
			} else {
				oModel.setProperty(
					"/acceptanceAssetCollection/items",
					formatterAcceptanceAssets.mapAcceptanceAssetsToModel(localStoreAcceptanceAssets.getAcceptanceAssetData())
				);
			}
		},

		loadAcceptanceAsset:function(oModel, selectedAcceptanceAssetID){

			oModel.setProperty(
				"/selectedAcceptanceAsset",
				_.findWhere(oModel.getProperty("/acceptanceAssetCollection/items"),
					{
						ID: selectedAcceptanceAssetID
					},
				this));
		},

		addNewAcceptanceAsset:function(oComponent, oModel){

			if ( !( oComponent.getModel("TestSwitch").getProperty("/testMode") ) ){
				restBuilder.execute(
					oModel.getProperty("/chaincode"),
					"addNew",
					formatterAcceptanceAssets.mapAcceptanceAssetToChaincode(oModel, true)
				);
			}  else {
				localStoreAcceptanceAssets.put(formatterAcceptanceAssets.mapAcceptanceAssetToLocalStorage(oModel, true));
			}
			this.loadAllAcceptanceAssets(oComponent, oModel);
			return oModel.getProperty("/newAcceptanceAsset/ID");
		},

		removeAcceptanceAsset : function(oComponent, oModel, acceptanceAssetID){

			if ( !( oComponent.getModel("TestSwitch").getProperty("/testMode") ) ) {
				restBuilder.execute(
					oModel.getProperty("/chaincode"),
					"remove",
					{ID:acceptanceAssetID}
				);
			} else {
				localStoreAcceptanceAssets.remove(acceptanceAssetID);
			}
			this.loadAllAcceptanceAssets(oComponent, oModel);
			return true;
		},

		removeAllAcceptanceAssets : function(oComponent, oModel){

			if (!(oComponent.getModel("TestSwitch").getProperty("/testMode"))) {
				restBuilder.execute(
					oModel.getProperty("/chaincode"),
					"removeAll",
					[]
				);
			} else {
				localStoreAcceptanceAssets.removeAll();
			}
			this.loadAllAcceptanceAssets(oComponent, oModel);
			oModel.setProperty("/selectedAcceptanceAsset",{});
			return true;
		}
	};
});
