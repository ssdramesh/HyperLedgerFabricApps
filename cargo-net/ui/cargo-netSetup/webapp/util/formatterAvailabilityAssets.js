sap.ui.define(function() {
	"use strict";

	return {

		mapAvailabilityAssetToModel:function(responseData){
			return {
				ID:responseData.ID,
				ObjectType:responseData.docType,
				
                Status:responseData.status,
                CreationDate:responseData.creationDate
,
                AirlineID:responseData.airlineID,
                Route:responseData.route,
                Freehold:responseData.freehold,
                UnitPrice:responseData.unitPrice,
                ULDCollection:responseData.uLDCollection
			};
		},

		mapAvailabilityAssetsToModel:function(responseData){

			var items = [];
			if( responseData ){
				for ( var i = 0; i < responseData.length; i++ ){
					items.push(this.mapAvailabilityAssetToModel(responseData[i]));
				}
			}
			return items;
		},

		mapAvailabilityAssetToChaincode:function(oModel, newAvailabilityAsset){

			if ( newAvailabilityAsset === true ) {
				return {
						"ID":this.getNewAvailabilityAssetID(oModel),
						"docType":"Asset.AvailabilityAsset",
						
                        "status":oModel.getProperty("/newAvailabilityAsset/Status"),
                        "creationDate":oModel.getProperty("/newAvailabilityAsset/CreationDate")
,
                  "airlineID":oModel.getProperty("/newAvailabilityAsset/AirlineID"),
                  "route":oModel.getProperty("/newAvailabilityAsset/Route"),
                  "freehold":oModel.getProperty("/newAvailabilityAsset/Freehold"),
                  "unitPrice":oModel.getProperty("/newAvailabilityAsset/UnitPrice"),
                  "uLDCollection":oModel.getProperty("/newAvailabilityAsset/ULDCollection")
				};
			} else {
				return {
						"ID":oModel.getProperty("/selectedAvailabilityAsset/ID"),
						"docType":"Asset.AvailabilityAsset",
						
                        "status":oModel.getProperty("/selectedAvailabilityAsset/Status"),
                        "creationDate":oModel.getProperty("/selectedAvailabilityAsset/CreationDate")
,
                  "airlineID":oModel.getProperty("/selectedAvailabilityAsset/AirlineID"),
                  "route":oModel.getProperty("/selectedAvailabilityAsset/Route"),
                  "freehold":oModel.getProperty("/selectedAvailabilityAsset/Freehold"),
                  "unitPrice":oModel.getProperty("/selectedAvailabilityAsset/UnitPrice"),
                  "uLDCollection":oModel.getProperty("/selectedAvailabilityAsset/ULDCollection")
				};
			}
		},

		mapAvailabilityAssetToLocalStorage : function(oModel, newAvailabilityAsset){

			return this.mapAvailabilityAssetToChaincode(oModel, newAvailabilityAsset);
		},

		getNewAvailabilityAssetID:function(oModel){

		    if ( typeof oModel.getProperty("/newAvailabilityAsset/ID") === "undefined" ||
		    		oModel.getProperty("/newAvailabilityAsset/ID") === ""
		    	){
			    var iD = "AvailabilityAsset";
			    var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
			    for( var i = 0; i < 8; i++ ){
			        iD += possible.charAt(Math.floor(Math.random() * possible.length));
			    }
		    } else {
				iD = oModel.getProperty("/newAvailabilityAsset/ID");
			}
			oModel.setProperty("/newAvailabilityAsset/ID",iD);
		    return iD;
		}
	};
});
