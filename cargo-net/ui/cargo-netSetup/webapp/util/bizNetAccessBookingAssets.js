sap.ui.define([
	"cargo-netSetup/util/restBuilder",
	"cargo-netSetup/util/formatterBookingAssets",
	"cargo-netSetup/util/localStoreBookingAssets"
], function(
		restBuilder,
		formatterBookingAssets,
		localStoreBookingAssets
	) {
	"use strict";

	return {

		loadAllBookingAssets:function(oComponent, oModel) {

			if ( !( oComponent.getModel("TestSwitch").getProperty("/testMode") ) ){
				restBuilder.execute(
					oModel.getProperty("/chaincode"),
					"readAll",
					[],
					function(responseData){
						oModel.setProperty(
							"/bookingAssetCollection/items",
							formatterBookingAssets.mapBookingAssetsToModel(responseData)
						);
					});
			} else {
				oModel.setProperty(
					"/bookingAssetCollection/items",
					formatterBookingAssets.mapBookingAssetsToModel(localStoreBookingAssets.getBookingAssetData())
				);
			}
		},

		loadBookingAsset:function(oModel, selectedBookingAssetID){

			oModel.setProperty(
				"/selectedBookingAsset",
				_.findWhere(oModel.getProperty("/bookingAssetCollection/items"),
					{
						ID: selectedBookingAssetID
					},
				this));
		},

		addNewBookingAsset:function(oComponent, oModel){

			if ( !( oComponent.getModel("TestSwitch").getProperty("/testMode") ) ){
				restBuilder.execute(
					oModel.getProperty("/chaincode"),
					"addNew",
					formatterBookingAssets.mapBookingAssetToChaincode(oModel, true)
				);
			}  else {
				localStoreBookingAssets.put(formatterBookingAssets.mapBookingAssetToLocalStorage(oModel, true));
			}
			this.loadAllBookingAssets(oComponent, oModel);
			return oModel.getProperty("/newBookingAsset/ID");
		},

		removeBookingAsset : function(oComponent, oModel, bookingAssetID){

			if ( !( oComponent.getModel("TestSwitch").getProperty("/testMode") ) ) {
				restBuilder.execute(
					oModel.getProperty("/chaincode"),
					"remove",
					{ID:bookingAssetID}
				);
			} else {
				localStoreBookingAssets.remove(bookingAssetID);
			}
			this.loadAllBookingAssets(oComponent, oModel);
			return true;
		},

		removeAllBookingAssets : function(oComponent, oModel){

			if (!(oComponent.getModel("TestSwitch").getProperty("/testMode"))) {
				restBuilder.execute(
					oModel.getProperty("/chaincode"),
					"removeAll",
					[]
				);
			} else {
				localStoreBookingAssets.removeAll();
			}
			this.loadAllBookingAssets(oComponent, oModel);
			oModel.setProperty("/selectedBookingAsset",{});
			return true;
		}
	};
});
