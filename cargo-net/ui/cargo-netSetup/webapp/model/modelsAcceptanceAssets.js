sap.ui.define([
	"sap/ui/model/json/JSONModel"
], function(JSONModel) {
	"use strict";

	return {

		createAcceptanceAssetsModel: function(){

			var oModel = new JSONModel();
			oModel.setDefaultBindingMode("TwoWay");
			oModel.setData({
				chaincode:{
					serviceUrl:"",
					serviceKey:{},
					acceptanceAsset:{}
				},
				acceptanceAssetCollection	: {
					cols:[
						{name:"ID"},
						{name:"ObjectType"},
						{name:"Status"},
						{name:"CreationDate"},
            {name:"CargoTerminalOperatorID"},
            {name:"BookingID"},
            {name:"FreightForwarderID"},
            {name:"AirwayBillID"},
            {name:"RouteCollection"},
            {name:"Freehold"},
            {name:"ULDCollection"}
					],
					items:[]
				},
				selectedAcceptanceAsset:{},
				newAcceptanceAsset:{
					ID:"",
					ObjectType:"",
					Status:"",
					CreationDate:"",
          CargoTerminalOperatorID:"",
          BookingID:"",
          FreightForwarderID:"",
          AirwayBillID:"",
          RouteCollection:"",
          Freehold:"",
          ULDCollection:""
				},
				selectedAcceptanceAssetID	: "",
				searchAcceptanceAssetID : ""
			});
			return oModel;
		}
	};
});
