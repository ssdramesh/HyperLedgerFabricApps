sap.ui.define([
	"sap/ui/model/json/JSONModel"
], function(JSONModel) {
	"use strict";

	return {

		createBookingAssetsModel: function(){

			var oModel = new JSONModel();
			oModel.setDefaultBindingMode("TwoWay");
			oModel.setData({
				chaincode:{
					serviceUrl:"",
					serviceKey:{},
					bookingAsset:{}
				},
				bookingAssetCollection	: {
					cols:[
						{name:"ID"},
						{name:"ObjectType"},
						{name:"Status"},
						{name:"CreationDate"},
            {name:"FreightForwarderID"},
            {name:"RouteCollection"},
            {name:"Freehold"},
            {name:"UnitPrice"},
            {name:"ULDCollection"}
					],
					items:[]
				},
				selectedBookingAsset:{},
				newBookingAsset:{
					ID:"",
					ObjectType:"",
					Status:"",
					CreationDate:"",
          FreightForwarderID:"",
          RouteCollection:"",
          Freehold:"",
          UnitPrice:"",
          ULDCollection:""
				},
				selectedBookingAssetID	: "",
				searchBookingAssetID : ""
			});
			return oModel;
		}
	};
});
