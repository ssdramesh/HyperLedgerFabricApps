sap.ui.define([
	"sap/ui/model/json/JSONModel"
], function(JSONModel) {
	"use strict";

	return {

		createAvailabilityAssetsModel: function(){

			var oModel = new JSONModel();
			oModel.setDefaultBindingMode("TwoWay");
			oModel.setData({
				chaincode:{
					serviceUrl:"",
					serviceKey:{},
					availabilityAsset:{}
				},
				availabilityAssetCollection	: {
					cols:[
						{name:"ID"},
						{name:"ObjectType"},
						{name:"Status"},
						{name:"CreationDate"},
            {name:"AirlineID"},
            {name:"Route"},
            {name:"Freehold"},
            {name:"UnitPrice"},
            {name:"ULDCollection"}
					],
					items:[]
				},
				selectedAvailabilityAsset:{},
				newAvailabilityAsset:{
					ID:"",
					ObjectType:"",
					Status:"",
					CreationDate:"",
          AirlineID:"",
          Route:"",
          Freehold:"",
          UnitPrice:"",
          ULDCollection:""
				},
				selectedAvailabilityAssetID	: "",
				searchAvailabilityAssetID : ""
			});
			return oModel;
		}
	};
});
