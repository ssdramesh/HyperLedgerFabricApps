sap.ui.define([
	"tunaNetSetup/controller/BaseController",
	"tunaNetSetup/util/bizNetAccessAdministratorParticipants"
], function(
		BaseController,
		bizNetAccessAdministratorParticipants
	) {
	"use strict";

	return BaseController.extend("tunaNetSetup.controller.AdministratorParticipantDetails", {

		onInit : function(){

			this.getOwnerComponent().getRouter().getRoute("administratorParticipant").attachPatternMatched(this._onObjectMatched, this);
		},

		_onObjectMatched : function (oEvent) {

			var pId = oEvent.getParameter("arguments").administratorParticipantId;
			if ( pId === "___new" ) {
				this.getView().byId("__barAdministratorParticipant").setSelectedKey("New");
			} else {
				bizNetAccessAdministratorParticipants.loadAdministratorParticipant(this.getView().getModel("AdministratorParticipants"), oEvent.getParameter("arguments").administratorParticipantId);
			}
		},

		addNew : function(){

			var oModel = this.getView().getModel("AdministratorParticipants");
			if ( oModel.getProperty("/newAdministratorParticipant/Alias") !== "" ||
				   oModel.getProperty("/newAdministratorParticipant/Description") !== "" ) {
				var administratorParticipantId = bizNetAccessAdministratorParticipants.addNewAdministratorParticipant(this.getOwnerComponent(), oModel);
				this.getView().byId("__barAdministratorParticipant").setSelectedKey("Details");
				bizNetAccessAdministratorParticipants.loadAdministratorParticipant(this.getView().getModel("AdministratorParticipants"), administratorParticipantId);
			} else {
				sap.m.MessageToast.show("Please enter valid data in all fields", {});
			}
			this._clearNewAsset();
		},

		removeAdministratorParticipant : function(){

			var oModel = this.getView().getModel("AdministratorParticipants");
			bizNetAccessAdministratorParticipants.removeAdministratorParticipant(this.getOwnerComponent(), oModel, oModel.getProperty("/selectedAdministratorParticipant/ID"));
		},

		_clearNewAsset : function(){

			this.getView().getModel("AdministratorParticipants").setProperty("/newAdministratorParticipant",{});
		}
	});

});
