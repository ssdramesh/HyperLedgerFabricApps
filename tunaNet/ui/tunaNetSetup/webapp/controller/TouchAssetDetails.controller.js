sap.ui.define([
	"tunaNetSetup/controller/BaseController",
	"tunaNetSetup/util/bizNetAccessTouchAssets"
], function(
		BaseController,
		bizNetAccessTouchAssets
	) {
	"use strict";

	return BaseController.extend("tunaNetSetup.controller.TouchAssetDetails", {

		onInit : function(){

			this.getOwnerComponent().getRouter().getRoute("touchAsset").attachPatternMatched(this._onObjectMatched, this);
		},

		_onObjectMatched : function (oEvent) {

			var pId = oEvent.getParameter("arguments").touchAssetId;
			if ( pId === "___new" ) {
				this.getView().byId("__barTouchAsset").setSelectedKey("New");
			} else {
				bizNetAccessTouchAssets.loadTouchAsset(this.getView().getModel("TouchAssets"), oEvent.getParameter("arguments").touchAssetId);
			}
		},

		addNew : function(){

			var oModel = this.getView().getModel("TouchAssets");
			if ( oModel.getProperty("/newTouchAsset/Alias") !== "" ||
				   oModel.getProperty("/newTouchAsset/Description") !== "" ) {
				var touchAssetId = bizNetAccessTouchAssets.addNewTouchAsset(this.getOwnerComponent(), oModel);
				this.getView().byId("__barTouchAsset").setSelectedKey("Details");
				bizNetAccessTouchAssets.loadTouchAsset(this.getView().getModel("TouchAssets"), touchAssetId);
			} else {
				sap.m.MessageToast.show("Please enter valid data in all fields", {});
			}
			this._clearNewAsset();
		},

		removeTouchAsset : function(){

			var oModel = this.getView().getModel("TouchAssets");
			bizNetAccessTouchAssets.removeTouchAsset(this.getOwnerComponent(), oModel, oModel.getProperty("/selectedTouchAsset/ID"));
		},

		_clearNewAsset : function(){

			this.getView().getModel("TouchAssets").setProperty("/newTouchAsset",{});
		}
	});

});
