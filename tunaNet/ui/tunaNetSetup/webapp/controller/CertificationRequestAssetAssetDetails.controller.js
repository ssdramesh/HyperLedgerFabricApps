sap.ui.define([
	"tunaNetSetup/controller/BaseController",
	"tunaNetSetup/util/bizNetAccessCertificationRequestAssetAssets"
], function(
		BaseController,
		bizNetAccessCertificationRequestAssetAssets
	) {
	"use strict";

	return BaseController.extend("tunaNetSetup.controller.CertificationRequestAssetAssetDetails", {

		onInit : function(){

			this.getOwnerComponent().getRouter().getRoute("certificationRequestAssetAsset").attachPatternMatched(this._onObjectMatched, this);
		},

		_onObjectMatched : function (oEvent) {

			var pId = oEvent.getParameter("arguments").certificationRequestAssetAssetId;
			if ( pId === "___new" ) {
				this.getView().byId("__barCertificationRequestAssetAsset").setSelectedKey("New");
			} else {
				bizNetAccessCertificationRequestAssetAssets.loadCertificationRequestAssetAsset(this.getView().getModel("CertificationRequestAssetAssets"), oEvent.getParameter("arguments").certificationRequestAssetAssetId);
			}
		},

		addNew : function(){

			var oModel = this.getView().getModel("CertificationRequestAssetAssets");
			if ( oModel.getProperty("/newCertificationRequestAssetAsset/Alias") !== "" ||
				   oModel.getProperty("/newCertificationRequestAssetAsset/Description") !== "" ) {
				var certificationRequestAssetAssetId = bizNetAccessCertificationRequestAssetAssets.addNewCertificationRequestAssetAsset(this.getOwnerComponent(), oModel);
				this.getView().byId("__barCertificationRequestAssetAsset").setSelectedKey("Details");
				bizNetAccessCertificationRequestAssetAssets.loadCertificationRequestAssetAsset(this.getView().getModel("CertificationRequestAssetAssets"), certificationRequestAssetAssetId);
			} else {
				sap.m.MessageToast.show("Please enter valid data in all fields", {});
			}
			this._clearNewAsset();
		},

		removeCertificationRequestAssetAsset : function(){

			var oModel = this.getView().getModel("CertificationRequestAssetAssets");
			bizNetAccessCertificationRequestAssetAssets.removeCertificationRequestAssetAsset(this.getOwnerComponent(), oModel, oModel.getProperty("/selectedCertificationRequestAssetAsset/ID"));
		},

		_clearNewAsset : function(){

			this.getView().getModel("CertificationRequestAssetAssets").setProperty("/newCertificationRequestAssetAsset",{});
		}
	});

});
