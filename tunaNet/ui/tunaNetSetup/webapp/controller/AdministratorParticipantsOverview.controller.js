sap.ui.define([
	"tunaNetSetup/controller/BaseController",
	"tunaNetSetup/model/modelsBase",
	"tunaNetSetup/util/messageProvider",
	"tunaNetSetup/util/localStoreAdministratorParticipants",
	"tunaNetSetup/util/bizNetAccessAdministratorParticipants",
	"sap/ui/core/routing/History",
	"sap/ui/model/Filter",
	"sap/ui/model/FilterOperator",
	"sap/ui/Device"
], function(
		BaseController,
		modelsBase,
		messageProvider,
		localStoreAdministratorParticipants,
		bizNetAccessAdministratorParticipants,
		History,
		Filter,
		FilterOperator,
		Device
	) {
	"use strict";

	return BaseController.extend("tunaNetSetup.controller.AdministratorParticipantsOverview", {

		onInit : function(){

			var oList = this.byId("administratorParticipantsList");
			this._oList = oList;
			this._oListFilterState = {
				aFilter : [],
				aSearch : []
			};
			localStoreAdministratorParticipants.init();
			if ( typeof this.getOwnerComponent().getModel("Messages") === "undefined" ){
				this.getOwnerComponent().setModel(modelsBase.createMessagesModel(), "Messages");
			}
			messageProvider.init(this.getOwnerComponent().getModel("Messages"));
		},

		onSelectionChange : function (oEvent) {

			this._showDetail(oEvent.getParameter("listItem") || oEvent.getSource());
		},

		onAdd : function() {

			this.getRouter().navTo("administratorParticipant", {administratorParticipantId : "___new"});
		},

		onRemoveAll : function(){

			bizNetAccessAdministratorParticipants.removeAllAdministratorParticipants(this.getOwnerComponent(), this.getOwnerComponent().getModel("AdministratorParticipants"));
		},

		onSearch : function(oEvent) {

			var sQuery = oEvent.getParameter("query");
			if (sQuery) {
				this._oListFilterState.aSearch = [new Filter("ID", FilterOperator.Contains, sQuery)];
			} else {
				this._oListFilterState.aSearch = [];
			}
			this._applyFilterSearch();
			var bReplace = !Device.system.phone;
			var oModel = this.getModel("AdministratorParticipants");
			oModel.setProperty(
				"/selectedAdministratorParticipant",
				_.findWhere(oModel.getProperty("/administratorParticipantCollection/items"),
					{
						ID: oModel.getProperty("/searchAdministratorParticipantID")
					},
				this));
			this.getRouter().navTo("administratorParticipant", {
				administratorParticipantId : oModel.getProperty("/selectedAdministratorParticipant").ID
			}, bReplace);
		},

		_onToggleBaaS : function(){

			this.getOwnerComponent().getModel("TestSwitch").setProperty("/testMode",!(this.getView().byId("__buttonToggleAdministratorParticipants").getPressed()));
			var location = this.getRunMode().location;
			messageProvider.addMessage("Success", "Switching Network for Blockchain Data to: ", "No Description", location, 1, "", "http://www.sap.com");
			if ( this.getRunMode().testMode && localStoreAdministratorParticipants.getAdministratorParticipantData() === null ){
				localStoreAdministratorParticipants.init();
			}
			bizNetAccessAdministratorParticipants.loadAllAdministratorParticipants(this.getOwnerComponent(), this.getModel("AdministratorParticipants"));
		},

		_showDetail : function(oItem) {

			var bReplace = !Device.system.phone;
			var oModel = this.getModel("AdministratorParticipants");
			this.getRouter().navTo("administratorParticipant", {
				administratorParticipantId : oModel.getObject(oItem.getBindingContextPath()).ID
			}, bReplace);
		},

		_applyFilterSearch : function () {
			var aFilters = this._oListFilterState.aSearch.concat(this._oListFilterState.aFilter),
				oViewModel = this.getModel("AdministratorParticipants");
			this._oList.getBinding("items").filter(aFilters, "Application");
			if (aFilters.length !== 0) {
				oViewModel.setProperty("/noDataText", this.getResourceBundle().getText("_labelNoAdministratorParticipantsText"));
			}
		},

		onMessagePress: function() {

			this.onShowMessageDialog("tunaNetSetup Log");
		},

		onNavBack : function() {
			this.getRouter().navTo("home");
		}
	});
});
