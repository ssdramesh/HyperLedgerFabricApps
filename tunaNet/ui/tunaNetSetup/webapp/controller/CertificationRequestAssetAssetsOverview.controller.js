sap.ui.define([
	"tunaNetSetup/controller/BaseController",
	"tunaNetSetup/model/modelsBase",
	"tunaNetSetup/util/messageProvider",
	"tunaNetSetup/util/localStoreCertificationRequestAssetAssets",
	"tunaNetSetup/util/bizNetAccessCertificationRequestAssetAssets",
	"sap/ui/core/routing/History",
	"sap/ui/model/Filter",
	"sap/ui/model/FilterOperator",
	"sap/ui/Device"
], function(
		BaseController,
		modelsBase,
		messageProvider,
		localStoreCertificationRequestAssetAssets,
		bizNetAccessCertificationRequestAssetAssets,
		History,
		Filter,
		FilterOperator,
		Device
	) {
	"use strict";

	return BaseController.extend("tunaNetSetup.controller.CertificationRequestAssetAssetsOverview", {

		onInit : function(){

			var oList = this.byId("certificationRequestAssetAssetsList");
			this._oList = oList;
			this._oListFilterState = {
				aFilter : [],
				aSearch : []
			};
			localStoreCertificationRequestAssetAssets.init();
			if ( typeof this.getOwnerComponent().getModel("Messages") === "undefined" ){
				this.getOwnerComponent().setModel(modelsBase.createMessagesModel(), "Messages");
			}
			messageProvider.init(this.getOwnerComponent().getModel("Messages"));
		},

		onSelectionChange : function (oEvent) {

			this._showDetail(oEvent.getParameter("listItem") || oEvent.getSource());
		},

		onAdd : function() {

			this.getRouter().navTo("certificationRequestAssetAsset", {certificationRequestAssetAssetId : "___new"});
		},

		onRemoveAll : function(){

			bizNetAccessCertificationRequestAssetAssets.removeAllCertificationRequestAssetAssets(this.getOwnerComponent(), this.getOwnerComponent().getModel("CertificationRequestAssetAssets"));
		},

		onSearch : function(oEvent) {

			var sQuery = oEvent.getParameter("query");
			if (sQuery) {
				this._oListFilterState.aSearch = [new Filter("ID", FilterOperator.Contains, sQuery)];
			} else {
				this._oListFilterState.aSearch = [];
			}
			this._applyFilterSearch();
			var bReplace = !Device.system.phone;
			var oModel = this.getModel("CertificationRequestAssetAssets");
			oModel.setProperty(
				"/selectedCertificationRequestAssetAsset",
				_.findWhere(oModel.getProperty("/certificationRequestAssetAssetCollection/items"),
					{
						ID: oModel.getProperty("/searchCertificationRequestAssetAssetID")
					},
				this));
			this.getRouter().navTo("certificationRequestAssetAsset", {
				certificationRequestAssetAssetId : oModel.getProperty("/selectedCertificationRequestAssetAsset").ID
			}, bReplace);
		},

		_onToggleBaaS : function(){

			this.getOwnerComponent().getModel("TestSwitch").setProperty("/testMode",!(this.getView().byId("__buttonToggleCertificationRequestAssetAssets").getPressed()));
			var location = this.getRunMode().location;
			messageProvider.addMessage("Success", "Switching Network for Blockchain Data to: ", "No Description", location, 1, "", "http://www.sap.com");
			if ( this.getRunMode().testMode && localStoreCertificationRequestAssetAssets.getCertificationRequestAssetAssetData() === null ){
				localStoreCertificationRequestAssetAssets.init();
			}
			bizNetAccessCertificationRequestAssetAssets.loadAllCertificationRequestAssetAssets(this.getOwnerComponent(), this.getModel("CertificationRequestAssetAssets"));
		},

		_showDetail : function(oItem) {

			var bReplace = !Device.system.phone;
			var oModel = this.getModel("CertificationRequestAssetAssets");
			this.getRouter().navTo("certificationRequestAssetAsset", {
				certificationRequestAssetAssetId : oModel.getObject(oItem.getBindingContextPath()).ID
			}, bReplace);
		},

		_applyFilterSearch : function () {
			var aFilters = this._oListFilterState.aSearch.concat(this._oListFilterState.aFilter),
				oViewModel = this.getModel("CertificationRequestAssetAssets");
			this._oList.getBinding("items").filter(aFilters, "Application");
			if (aFilters.length !== 0) {
				oViewModel.setProperty("/noDataText", this.getResourceBundle().getText("_labelNoCertificationRequestAssetAssetsText"));
			}
		},

		onMessagePress: function() {

			this.onShowMessageDialog("tunaNetSetup Log");
		},

		onNavBack : function() {
			this.getRouter().navTo("home");
		}
	});
});
