sap.ui.define([
	"tunaNetSetup/controller/BaseController",
	"tunaNetSetup/model/modelsBase",
	"tunaNetSetup/util/messageProvider",
	"tunaNetSetup/util/localStoreCertificationAuthorityParticipants",
	"tunaNetSetup/util/bizNetAccessCertificationAuthorityParticipants",
	"sap/ui/core/routing/History",
	"sap/ui/model/Filter",
	"sap/ui/model/FilterOperator",
	"sap/ui/Device"
], function(
		BaseController,
		modelsBase,
		messageProvider,
		localStoreCertificationAuthorityParticipants,
		bizNetAccessCertificationAuthorityParticipants,
		History,
		Filter,
		FilterOperator,
		Device
	) {
	"use strict";

	return BaseController.extend("tunaNetSetup.controller.CertificationAuthorityParticipantsOverview", {

		onInit : function(){

			var oList = this.byId("certificationAuthorityParticipantsList");
			this._oList = oList;
			this._oListFilterState = {
				aFilter : [],
				aSearch : []
			};
			localStoreCertificationAuthorityParticipants.init();
			if ( typeof this.getOwnerComponent().getModel("Messages") === "undefined" ){
				this.getOwnerComponent().setModel(modelsBase.createMessagesModel(), "Messages");
			}
			messageProvider.init(this.getOwnerComponent().getModel("Messages"));
		},

		onSelectionChange : function (oEvent) {

			this._showDetail(oEvent.getParameter("listItem") || oEvent.getSource());
		},

		onAdd : function() {

			this.getRouter().navTo("certificationAuthorityParticipant", {certificationAuthorityParticipantId : "___new"});
		},

		onRemoveAll : function(){

			bizNetAccessCertificationAuthorityParticipants.removeAllCertificationAuthorityParticipants(this.getOwnerComponent(), this.getOwnerComponent().getModel("CertificationAuthorityParticipants"));
		},

		onSearch : function(oEvent) {

			var sQuery = oEvent.getParameter("query");
			if (sQuery) {
				this._oListFilterState.aSearch = [new Filter("ID", FilterOperator.Contains, sQuery)];
			} else {
				this._oListFilterState.aSearch = [];
			}
			this._applyFilterSearch();
			var bReplace = !Device.system.phone;
			var oModel = this.getModel("CertificationAuthorityParticipants");
			oModel.setProperty(
				"/selectedCertificationAuthorityParticipant",
				_.findWhere(oModel.getProperty("/certificationAuthorityParticipantCollection/items"),
					{
						ID: oModel.getProperty("/searchCertificationAuthorityParticipantID")
					},
				this));
			this.getRouter().navTo("certificationAuthorityParticipant", {
				certificationAuthorityParticipantId : oModel.getProperty("/selectedCertificationAuthorityParticipant").ID
			}, bReplace);
		},

		_onToggleBaaS : function(){

			this.getOwnerComponent().getModel("TestSwitch").setProperty("/testMode",!(this.getView().byId("__buttonToggleCertificationAuthorityParticipants").getPressed()));
			var location = this.getRunMode().location;
			messageProvider.addMessage("Success", "Switching Network for Blockchain Data to: ", "No Description", location, 1, "", "http://www.sap.com");
			if ( this.getRunMode().testMode && localStoreCertificationAuthorityParticipants.getCertificationAuthorityParticipantData() === null ){
				localStoreCertificationAuthorityParticipants.init();
			}
			bizNetAccessCertificationAuthorityParticipants.loadAllCertificationAuthorityParticipants(this.getOwnerComponent(), this.getModel("CertificationAuthorityParticipants"));
		},

		_showDetail : function(oItem) {

			var bReplace = !Device.system.phone;
			var oModel = this.getModel("CertificationAuthorityParticipants");
			this.getRouter().navTo("certificationAuthorityParticipant", {
				certificationAuthorityParticipantId : oModel.getObject(oItem.getBindingContextPath()).ID
			}, bReplace);
		},

		_applyFilterSearch : function () {
			var aFilters = this._oListFilterState.aSearch.concat(this._oListFilterState.aFilter),
				oViewModel = this.getModel("CertificationAuthorityParticipants");
			this._oList.getBinding("items").filter(aFilters, "Application");
			if (aFilters.length !== 0) {
				oViewModel.setProperty("/noDataText", this.getResourceBundle().getText("_labelNoCertificationAuthorityParticipantsText"));
			}
		},

		onMessagePress: function() {

			this.onShowMessageDialog("tunaNetSetup Log");
		},

		onNavBack : function() {
			this.getRouter().navTo("home");
		}
	});
});
