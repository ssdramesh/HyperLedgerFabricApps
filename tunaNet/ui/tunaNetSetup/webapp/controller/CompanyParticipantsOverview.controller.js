sap.ui.define([
	"tunaNetSetup/controller/BaseController",
	"tunaNetSetup/model/modelsBase",
	"tunaNetSetup/util/messageProvider",
	"tunaNetSetup/util/localStoreCompanyParticipants",
	"tunaNetSetup/util/bizNetAccessCompanyParticipants",
	"sap/ui/core/routing/History",
	"sap/ui/model/Filter",
	"sap/ui/model/FilterOperator",
	"sap/ui/Device"
], function(
		BaseController,
		modelsBase,
		messageProvider,
		localStoreCompanyParticipants,
		bizNetAccessCompanyParticipants,
		History,
		Filter,
		FilterOperator,
		Device
	) {
	"use strict";

	return BaseController.extend("tunaNetSetup.controller.CompanyParticipantsOverview", {

		onInit : function(){

			var oList = this.byId("companyParticipantsList");
			this._oList = oList;
			this._oListFilterState = {
				aFilter : [],
				aSearch : []
			};
			localStoreCompanyParticipants.init();
			if ( typeof this.getOwnerComponent().getModel("Messages") === "undefined" ){
				this.getOwnerComponent().setModel(modelsBase.createMessagesModel(), "Messages");
			}
			messageProvider.init(this.getOwnerComponent().getModel("Messages"));
		},

		onSelectionChange : function (oEvent) {

			this._showDetail(oEvent.getParameter("listItem") || oEvent.getSource());
		},

		onAdd : function() {

			this.getRouter().navTo("companyParticipant", {companyParticipantId : "___new"});
		},

		onRemoveAll : function(){

			bizNetAccessCompanyParticipants.removeAllCompanyParticipants(this.getOwnerComponent(), this.getOwnerComponent().getModel("CompanyParticipants"));
		},

		onSearch : function(oEvent) {

			var sQuery = oEvent.getParameter("query");
			if (sQuery) {
				this._oListFilterState.aSearch = [new Filter("ID", FilterOperator.Contains, sQuery)];
			} else {
				this._oListFilterState.aSearch = [];
			}
			this._applyFilterSearch();
			var bReplace = !Device.system.phone;
			var oModel = this.getModel("CompanyParticipants");
			oModel.setProperty(
				"/selectedCompanyParticipant",
				_.findWhere(oModel.getProperty("/companyParticipantCollection/items"),
					{
						ID: oModel.getProperty("/searchCompanyParticipantID")
					},
				this));
			this.getRouter().navTo("companyParticipant", {
				companyParticipantId : oModel.getProperty("/selectedCompanyParticipant").ID
			}, bReplace);
		},

		_onToggleBaaS : function(){

			this.getOwnerComponent().getModel("TestSwitch").setProperty("/testMode",!(this.getView().byId("__buttonToggleCompanyParticipants").getPressed()));
			var location = this.getRunMode().location;
			messageProvider.addMessage("Success", "Switching Network for Blockchain Data to: ", "No Description", location, 1, "", "http://www.sap.com");
			if ( this.getRunMode().testMode && localStoreCompanyParticipants.getCompanyParticipantData() === null ){
				localStoreCompanyParticipants.init();
			}
			bizNetAccessCompanyParticipants.loadAllCompanyParticipants(this.getOwnerComponent(), this.getModel("CompanyParticipants"));
		},

		_showDetail : function(oItem) {

			var bReplace = !Device.system.phone;
			var oModel = this.getModel("CompanyParticipants");
			this.getRouter().navTo("companyParticipant", {
				companyParticipantId : oModel.getObject(oItem.getBindingContextPath()).ID
			}, bReplace);
		},

		_applyFilterSearch : function () {
			var aFilters = this._oListFilterState.aSearch.concat(this._oListFilterState.aFilter),
				oViewModel = this.getModel("CompanyParticipants");
			this._oList.getBinding("items").filter(aFilters, "Application");
			if (aFilters.length !== 0) {
				oViewModel.setProperty("/noDataText", this.getResourceBundle().getText("_labelNoCompanyParticipantsText"));
			}
		},

		onMessagePress: function() {

			this.onShowMessageDialog("tunaNetSetup Log");
		},

		onNavBack : function() {
			this.getRouter().navTo("home");
		}
	});
});
