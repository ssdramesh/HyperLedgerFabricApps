sap.ui.define([
	"tunaNetSetup/util/restBuilder",
	"tunaNetSetup/util/formatterCertificationRequestAssetAssets",
	"tunaNetSetup/util/localStoreCertificationRequestAssetAssets"
], function(
		restBuilder,
		formatterCertificationRequestAssetAssets,
		localStoreCertificationRequestAssetAssets
	) {
	"use strict";

	return {

		loadAllCertificationRequestAssetAssets:function(oComponent, oModel) {

			if ( !( oComponent.getModel("TestSwitch").getProperty("/testMode") ) ){
				restBuilder.execute(
					oModel.getProperty("/chaincode"),
					"readAll",
					[],
					function(responseData){
						oModel.setProperty(
							"/certificationRequestAssetAssetCollection/items",
							formatterCertificationRequestAssetAssets.mapCertificationRequestAssetAssetsToModel(responseData)
						);
					});
			} else {
				oModel.setProperty(
					"/certificationRequestAssetAssetCollection/items",
					formatterCertificationRequestAssetAssets.mapCertificationRequestAssetAssetsToModel(localStoreCertificationRequestAssetAssets.getCertificationRequestAssetAssetData())
				);
			}
		},

		loadCertificationRequestAssetAsset:function(oModel, selectedCertificationRequestAssetAssetID){

			oModel.setProperty(
				"/selectedCertificationRequestAssetAsset",
				_.findWhere(oModel.getProperty("/certificationRequestAssetAssetCollection/items"),
					{
						ID: selectedCertificationRequestAssetAssetID
					},
				this));
		},

		addNewCertificationRequestAssetAsset:function(oComponent, oModel){

			if ( !( oComponent.getModel("TestSwitch").getProperty("/testMode") ) ){
				restBuilder.execute(
					oModel.getProperty("/chaincode"),
					"addNew",
					formatterCertificationRequestAssetAssets.mapCertificationRequestAssetAssetToChaincode(oModel, true)
				);
			}  else {
				localStoreCertificationRequestAssetAssets.put(formatterCertificationRequestAssetAssets.mapCertificationRequestAssetAssetToLocalStorage(oModel, true));
			}
			this.loadAllCertificationRequestAssetAssets(oComponent, oModel);
			return oModel.getProperty("/newCertificationRequestAssetAsset/ID");
		},

		removeCertificationRequestAssetAsset : function(oComponent, oModel, certificationRequestAssetAssetID){

			if ( !( oComponent.getModel("TestSwitch").getProperty("/testMode") ) ) {
				restBuilder.execute(
					oModel.getProperty("/chaincode"),
					"remove",
					{ID:certificationRequestAssetAssetID}
				);
			} else {
				localStoreCertificationRequestAssetAssets.remove(certificationRequestAssetAssetID);
			}
			this.loadAllCertificationRequestAssetAssets(oComponent, oModel);
			return true;
		},

		removeAllCertificationRequestAssetAssets : function(oComponent, oModel){

			if (!(oComponent.getModel("TestSwitch").getProperty("/testMode"))) {
				restBuilder.execute(
					oModel.getProperty("/chaincode"),
					"removeAll",
					[]
				);
			} else {
				localStoreCertificationRequestAssetAssets.removeAll();
			}
			this.loadAllCertificationRequestAssetAssets(oComponent, oModel);
			oModel.setProperty("/selectedCertificationRequestAssetAsset",{});
			return true;
		}
	};
});
