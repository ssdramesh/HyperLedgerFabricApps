sap.ui.define(function() {
	"use strict";

	jQuery.sap.require("jquery.sap.storage");
	var oStorage = jQuery.sap.storage(jQuery.sap.storage.Type.local);
	var certificationRequestAssetAssetsDataID = "certificationRequestAssetAssets";

	return {

		init: function(){

			oStorage.put(certificationRequestAssetAssetsDataID,[]);
			oStorage.put(
				certificationRequestAssetAssetsDataID,
[
	{
		"ID":"CertificationRequestAssetAsset1001",
		"docType":"Asset.CertificationRequestAssetAsset",
		"status":"Created",
		"creationDate":"12/28/2107",
		"companyID":"companyID001",
		"certificationAuthorityID":"certificationAuthorityID001"
	}
]				
			);
		},

		getCertificationRequestAssetAssetDataID : function(){

			return certificationRequestAssetAssetsDataID;
		},

		getCertificationRequestAssetAssetData  : function(){

			return oStorage.get("certificationRequestAssetAssets");
		},

		put: function(newCertificationRequestAssetAsset){

			var certificationRequestAssetAssetData = this.getCertificationRequestAssetAssetData();
			certificationRequestAssetAssetData.push(newCertificationRequestAssetAsset);
			oStorage.put(certificationRequestAssetAssetsDataID, certificationRequestAssetAssetData);
		},

		remove : function (id){

			var certificationRequestAssetAssetData = this.getCertificationRequestAssetAssetData();
			certificationRequestAssetAssetData = _.without(certificationRequestAssetAssetData,_.findWhere(certificationRequestAssetAssetData,{ID:id}));
			oStorage.put(certificationRequestAssetAssetsDataID, certificationRequestAssetAssetData);
		},

		removeAll : function(){

			oStorage.put(certificationRequestAssetAssetsDataID,[]);
		},

		clearCertificationRequestAssetAssetData: function(){

			oStorage.put(certificationRequestAssetAssetsDataID,[]);
		}
	};
});
