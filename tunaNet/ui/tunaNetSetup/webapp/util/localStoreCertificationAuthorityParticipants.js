sap.ui.define(function() {
	"use strict";

	jQuery.sap.require("jquery.sap.storage");
	var oStorage = jQuery.sap.storage(jQuery.sap.storage.Type.local);
	var certificationAuthorityParticipantsDataID = "certificationAuthorityParticipants";

	return {

		init: function(){

			oStorage.put(certificationAuthorityParticipantsDataID,[]);
			oStorage.put(
				certificationAuthorityParticipantsDataID,
[
	{
		"ID":"CertificationAuthorityParticipant1001",
		"docType":"Participant.CertificationAuthorityParticipant",
		"alias":"BRITECH",
		"description":"British Technology Pvt. Ltd."
  }
]				
			);
		},

		getCertificationAuthorityParticipantDataID : function(){

			return certificationAuthorityParticipantsDataID;
		},

		getCertificationAuthorityParticipantData  : function(){

			return oStorage.get("certificationAuthorityParticipants");
		},

		put: function(newCertificationAuthorityParticipant){

			var certificationAuthorityParticipantData = this.getCertificationAuthorityParticipantData();
			certificationAuthorityParticipantData.push(newCertificationAuthorityParticipant);
			oStorage.put(certificationAuthorityParticipantsDataID, certificationAuthorityParticipantData);
		},

		remove : function (id){

			var certificationAuthorityParticipantData = this.getCertificationAuthorityParticipantData();
			certificationAuthorityParticipantData = _.without(certificationAuthorityParticipantData,_.findWhere(certificationAuthorityParticipantData,{ID:id}));
			oStorage.put(certificationAuthorityParticipantsDataID, certificationAuthorityParticipantData);
		},

		removeAll : function(){

			oStorage.put(certificationAuthorityParticipantsDataID,[]);
		},

		clearCertificationAuthorityParticipantData: function(){

			oStorage.put(certificationAuthorityParticipantsDataID,[]);
		}
	};
});
