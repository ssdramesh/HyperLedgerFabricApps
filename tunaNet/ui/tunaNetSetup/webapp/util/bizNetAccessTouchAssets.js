sap.ui.define([
	"tunaNetSetup/util/restBuilder",
	"tunaNetSetup/util/formatterTouchAssets",
	"tunaNetSetup/util/localStoreTouchAssets"
], function(
		restBuilder,
		formatterTouchAssets,
		localStoreTouchAssets
	) {
	"use strict";

	return {

		loadAllTouchAssets:function(oComponent, oModel) {

			if ( !( oComponent.getModel("TestSwitch").getProperty("/testMode") ) ){
				restBuilder.execute(
					oModel.getProperty("/chaincode"),
					"readAll",
					[],
					function(responseData){
						oModel.setProperty(
							"/touchAssetCollection/items",
							formatterTouchAssets.mapTouchAssetsToModel(responseData)
						);
					});
			} else {
				oModel.setProperty(
					"/touchAssetCollection/items",
					formatterTouchAssets.mapTouchAssetsToModel(localStoreTouchAssets.getTouchAssetData())
				);
			}
		},

		loadTouchAsset:function(oModel, selectedTouchAssetID){

			oModel.setProperty(
				"/selectedTouchAsset",
				_.findWhere(oModel.getProperty("/touchAssetCollection/items"),
					{
						ID: selectedTouchAssetID
					},
				this));
		},

		addNewTouchAsset:function(oComponent, oModel){

			if ( !( oComponent.getModel("TestSwitch").getProperty("/testMode") ) ){
				restBuilder.execute(
					oModel.getProperty("/chaincode"),
					"addNew",
					formatterTouchAssets.mapTouchAssetToChaincode(oModel, true)
				);
			}  else {
				localStoreTouchAssets.put(formatterTouchAssets.mapTouchAssetToLocalStorage(oModel, true));
			}
			this.loadAllTouchAssets(oComponent, oModel);
			return oModel.getProperty("/newTouchAsset/ID");
		},

		removeTouchAsset : function(oComponent, oModel, touchAssetID){

			if ( !( oComponent.getModel("TestSwitch").getProperty("/testMode") ) ) {
				restBuilder.execute(
					oModel.getProperty("/chaincode"),
					"remove",
					{ID:touchAssetID}
				);
			} else {
				localStoreTouchAssets.remove(touchAssetID);
			}
			this.loadAllTouchAssets(oComponent, oModel);
			return true;
		},

		removeAllTouchAssets : function(oComponent, oModel){

			if (!(oComponent.getModel("TestSwitch").getProperty("/testMode"))) {
				restBuilder.execute(
					oModel.getProperty("/chaincode"),
					"removeAll",
					[]
				);
			} else {
				localStoreTouchAssets.removeAll();
			}
			this.loadAllTouchAssets(oComponent, oModel);
			oModel.setProperty("/selectedTouchAsset",{});
			return true;
		}
	};
});
