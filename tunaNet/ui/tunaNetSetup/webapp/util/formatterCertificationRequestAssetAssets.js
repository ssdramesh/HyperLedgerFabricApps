sap.ui.define(function() {
	"use strict";

	return {

		mapCertificationRequestAssetAssetToModel:function(responseData){
			return {
				ID:responseData.ID,
				ObjectType:responseData.docType,
				
                Status:responseData.status,
                CreationDate:responseData.creationDate
,
                CompanyID:responseData.companyID,
                CertificationAuthorityID:responseData.certificationAuthorityID
			};
		},

		mapCertificationRequestAssetAssetsToModel:function(responseData){

			var items = [];
			if( responseData ){
				for ( var i = 0; i < responseData.length; i++ ){
					items.push(this.mapCertificationRequestAssetAssetToModel(responseData[i]));
				}
			}
			return items;
		},

		mapCertificationRequestAssetAssetToChaincode:function(oModel, newCertificationRequestAssetAsset){

			if ( newCertificationRequestAssetAsset === true ) {
				return {
						"ID":this.getNewCertificationRequestAssetAssetID(oModel),
						"docType":"Asset.CertificationRequestAssetAsset",
						
                        "status":oModel.getProperty("/newCertificationRequestAssetAsset/Status"),
                        "creationDate":oModel.getProperty("/newCertificationRequestAssetAsset/CreationDate")
,
                  "companyID":oModel.getProperty("/newCertificationRequestAssetAsset/CompanyID"),
                  "certificationAuthorityID":oModel.getProperty("/newCertificationRequestAssetAsset/CertificationAuthorityID")
				};
			} else {
				return {
						"ID":oModel.getProperty("/selectedCertificationRequestAssetAsset/ID"),
						"docType":"Asset.CertificationRequestAssetAsset",
						
                        "status":oModel.getProperty("/selectedCertificationRequestAssetAsset/Status"),
                        "creationDate":oModel.getProperty("/selectedCertificationRequestAssetAsset/CreationDate")
,
                  "companyID":oModel.getProperty("/selectedCertificationRequestAssetAsset/CompanyID"),
                  "certificationAuthorityID":oModel.getProperty("/selectedCertificationRequestAssetAsset/CertificationAuthorityID")
				};
			}
		},

		mapCertificationRequestAssetAssetToLocalStorage : function(oModel, newCertificationRequestAssetAsset){

			return this.mapCertificationRequestAssetAssetToChaincode(oModel, newCertificationRequestAssetAsset);
		},

		getNewCertificationRequestAssetAssetID:function(oModel){

		    if ( typeof oModel.getProperty("/newCertificationRequestAssetAsset/ID") === "undefined" ||
		    		oModel.getProperty("/newCertificationRequestAssetAsset/ID") === ""
		    	){
			    var iD = "CertificationRequestAssetAsset";
			    var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
			    for( var i = 0; i < 8; i++ ){
			        iD += possible.charAt(Math.floor(Math.random() * possible.length));
			    }
		    } else {
				iD = oModel.getProperty("/newCertificationRequestAssetAsset/ID");
			}
			oModel.setProperty("/newCertificationRequestAssetAsset/ID",iD);
		    return iD;
		}
	};
});
