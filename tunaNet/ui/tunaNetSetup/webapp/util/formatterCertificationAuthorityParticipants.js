sap.ui.define(function() {
	"use strict";

	return {

		mapCertificationAuthorityParticipantToModel:function(responseData){
			return {
				ID:responseData.ID,
				ObjectType:responseData.docType,
				
                Alias:responseData.alias,
                Description:responseData.description

			};
		},

		mapCertificationAuthorityParticipantsToModel:function(responseData){

			var items = [];
			if( responseData ){
				for ( var i = 0; i < responseData.length; i++ ){
					items.push(this.mapCertificationAuthorityParticipantToModel(responseData[i]));
				}
			}
			return items;
		},

		mapCertificationAuthorityParticipantToChaincode:function(oModel, newCertificationAuthorityParticipant){

			if ( newCertificationAuthorityParticipant === true ) {
				return {
						"ID":this.getNewCertificationAuthorityParticipantID(oModel),
						"docType":"Participant.CertificationAuthorityParticipant",
						
                        "alias":oModel.getProperty("/newCertificationAuthorityParticipant/Alias"),
                        "description":oModel.getProperty("/newCertificationAuthorityParticipant/Description")

				};
			} else {
				return {
						"ID":oModel.getProperty("/selectedCertificationAuthorityParticipant/ID"),
						"docType":"Participant.CertificationAuthorityParticipant",
						
                        "alias":oModel.getProperty("/selectedCertificationAuthorityParticipant/Alias"),
                        "description":oModel.getProperty("/selectedCertificationAuthorityParticipant/Description")

				};
			}
		},

		mapCertificationAuthorityParticipantToLocalStorage : function(oModel, newCertificationAuthorityParticipant){

			return this.mapCertificationAuthorityParticipantToChaincode(oModel, newCertificationAuthorityParticipant);
		},

		getNewCertificationAuthorityParticipantID:function(oModel){

		    if ( typeof oModel.getProperty("/newCertificationAuthorityParticipant/ID") === "undefined" ||
		    		oModel.getProperty("/newCertificationAuthorityParticipant/ID") === ""
		    	){
			    var iD = "CertificationAuthorityParticipant";
			    var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
			    for( var i = 0; i < 8; i++ ){
			        iD += possible.charAt(Math.floor(Math.random() * possible.length));
			    }
		    } else {
				iD = oModel.getProperty("/newCertificationAuthorityParticipant/ID");
			}
			oModel.setProperty("/newCertificationAuthorityParticipant/ID",iD);
		    return iD;
		}
	};
});
