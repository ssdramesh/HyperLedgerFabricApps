sap.ui.define([
	"tunaNetSetup/util/restBuilder",
	"tunaNetSetup/util/formatterCompanyParticipants",
	"tunaNetSetup/util/localStoreCompanyParticipants"
], function(
		restBuilder,
		formatterCompanyParticipants,
		localStoreCompanyParticipants
	) {
	"use strict";

	return {

		loadAllCompanyParticipants:function(oComponent, oModel) {

			if ( !( oComponent.getModel("TestSwitch").getProperty("/testMode") ) ){
				restBuilder.execute(
					oModel.getProperty("/chaincode"),
					"readAll",
					[],
					function(responseData){
						oModel.setProperty(
							"/companyParticipantCollection/items",
							formatterCompanyParticipants.mapCompanyParticipantsToModel(responseData)
						);
					});
			} else {
				oModel.setProperty(
					"/companyParticipantCollection/items",
					formatterCompanyParticipants.mapCompanyParticipantsToModel(localStoreCompanyParticipants.getCompanyParticipantData())
				);
			}
		},

		loadCompanyParticipant:function(oModel, selectedCompanyParticipantID){

			oModel.setProperty(
				"/selectedCompanyParticipant",
				_.findWhere(oModel.getProperty("/companyParticipantCollection/items"),
					{
						ID: selectedCompanyParticipantID
					},
				this));
		},

		addNewCompanyParticipant:function(oComponent, oModel){

			if ( !( oComponent.getModel("TestSwitch").getProperty("/testMode") ) ){
				restBuilder.execute(
					oModel.getProperty("/chaincode"),
					"addNew",
					formatterCompanyParticipants.mapCompanyParticipantToChaincode(oModel, true)
				);
			}  else {
				localStoreCompanyParticipants.put(formatterCompanyParticipants.mapCompanyParticipantToLocalStorage(oModel, true));
			}
			this.loadAllCompanyParticipants(oComponent, oModel);
			return oModel.getProperty("/newCompanyParticipant/ID");
		},

		removeCompanyParticipant : function(oComponent, oModel, companyParticipantID){

			if ( !( oComponent.getModel("TestSwitch").getProperty("/testMode") ) ) {
				restBuilder.execute(
					oModel.getProperty("/chaincode"),
					"remove",
					{ID:companyParticipantID}
				);
			} else {
				localStoreCompanyParticipants.remove(companyParticipantID);
			}
			this.loadAllCompanyParticipants(oComponent, oModel);
			return true;
		},

		removeAllCompanyParticipants : function(oComponent, oModel){

			if (!(oComponent.getModel("TestSwitch").getProperty("/testMode"))) {
				restBuilder.execute(
					oModel.getProperty("/chaincode"),
					"removeAll",
					[]
				);
			} else {
				localStoreCompanyParticipants.removeAll();
			}
			this.loadAllCompanyParticipants(oComponent, oModel);
			oModel.setProperty("/selectedCompanyParticipant",{});
			return true;
		}
	};
});
