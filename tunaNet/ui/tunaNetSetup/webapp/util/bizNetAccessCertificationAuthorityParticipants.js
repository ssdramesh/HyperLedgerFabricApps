sap.ui.define([
	"tunaNetSetup/util/restBuilder",
	"tunaNetSetup/util/formatterCertificationAuthorityParticipants",
	"tunaNetSetup/util/localStoreCertificationAuthorityParticipants"
], function(
		restBuilder,
		formatterCertificationAuthorityParticipants,
		localStoreCertificationAuthorityParticipants
	) {
	"use strict";

	return {

		loadAllCertificationAuthorityParticipants:function(oComponent, oModel) {

			if ( !( oComponent.getModel("TestSwitch").getProperty("/testMode") ) ){
				restBuilder.execute(
					oModel.getProperty("/chaincode"),
					"readAll",
					[],
					function(responseData){
						oModel.setProperty(
							"/certificationAuthorityParticipantCollection/items",
							formatterCertificationAuthorityParticipants.mapCertificationAuthorityParticipantsToModel(responseData)
						);
					});
			} else {
				oModel.setProperty(
					"/certificationAuthorityParticipantCollection/items",
					formatterCertificationAuthorityParticipants.mapCertificationAuthorityParticipantsToModel(localStoreCertificationAuthorityParticipants.getCertificationAuthorityParticipantData())
				);
			}
		},

		loadCertificationAuthorityParticipant:function(oModel, selectedCertificationAuthorityParticipantID){

			oModel.setProperty(
				"/selectedCertificationAuthorityParticipant",
				_.findWhere(oModel.getProperty("/certificationAuthorityParticipantCollection/items"),
					{
						ID: selectedCertificationAuthorityParticipantID
					},
				this));
		},

		addNewCertificationAuthorityParticipant:function(oComponent, oModel){

			if ( !( oComponent.getModel("TestSwitch").getProperty("/testMode") ) ){
				restBuilder.execute(
					oModel.getProperty("/chaincode"),
					"addNew",
					formatterCertificationAuthorityParticipants.mapCertificationAuthorityParticipantToChaincode(oModel, true)
				);
			}  else {
				localStoreCertificationAuthorityParticipants.put(formatterCertificationAuthorityParticipants.mapCertificationAuthorityParticipantToLocalStorage(oModel, true));
			}
			this.loadAllCertificationAuthorityParticipants(oComponent, oModel);
			return oModel.getProperty("/newCertificationAuthorityParticipant/ID");
		},

		removeCertificationAuthorityParticipant : function(oComponent, oModel, certificationAuthorityParticipantID){

			if ( !( oComponent.getModel("TestSwitch").getProperty("/testMode") ) ) {
				restBuilder.execute(
					oModel.getProperty("/chaincode"),
					"remove",
					{ID:certificationAuthorityParticipantID}
				);
			} else {
				localStoreCertificationAuthorityParticipants.remove(certificationAuthorityParticipantID);
			}
			this.loadAllCertificationAuthorityParticipants(oComponent, oModel);
			return true;
		},

		removeAllCertificationAuthorityParticipants : function(oComponent, oModel){

			if (!(oComponent.getModel("TestSwitch").getProperty("/testMode"))) {
				restBuilder.execute(
					oModel.getProperty("/chaincode"),
					"removeAll",
					[]
				);
			} else {
				localStoreCertificationAuthorityParticipants.removeAll();
			}
			this.loadAllCertificationAuthorityParticipants(oComponent, oModel);
			oModel.setProperty("/selectedCertificationAuthorityParticipant",{});
			return true;
		}
	};
});
