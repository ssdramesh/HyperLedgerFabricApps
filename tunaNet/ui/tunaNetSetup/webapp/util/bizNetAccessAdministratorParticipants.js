sap.ui.define([
	"tunaNetSetup/util/restBuilder",
	"tunaNetSetup/util/formatterAdministratorParticipants",
	"tunaNetSetup/util/localStoreAdministratorParticipants"
], function(
		restBuilder,
		formatterAdministratorParticipants,
		localStoreAdministratorParticipants
	) {
	"use strict";

	return {

		loadAllAdministratorParticipants:function(oComponent, oModel) {

			if ( !( oComponent.getModel("TestSwitch").getProperty("/testMode") ) ){
				restBuilder.execute(
					oModel.getProperty("/chaincode"),
					"readAll",
					[],
					function(responseData){
						oModel.setProperty(
							"/administratorParticipantCollection/items",
							formatterAdministratorParticipants.mapAdministratorParticipantsToModel(responseData)
						);
					});
			} else {
				oModel.setProperty(
					"/administratorParticipantCollection/items",
					formatterAdministratorParticipants.mapAdministratorParticipantsToModel(localStoreAdministratorParticipants.getAdministratorParticipantData())
				);
			}
		},

		loadAdministratorParticipant:function(oModel, selectedAdministratorParticipantID){

			oModel.setProperty(
				"/selectedAdministratorParticipant",
				_.findWhere(oModel.getProperty("/administratorParticipantCollection/items"),
					{
						ID: selectedAdministratorParticipantID
					},
				this));
		},

		addNewAdministratorParticipant:function(oComponent, oModel){

			if ( !( oComponent.getModel("TestSwitch").getProperty("/testMode") ) ){
				restBuilder.execute(
					oModel.getProperty("/chaincode"),
					"addNew",
					formatterAdministratorParticipants.mapAdministratorParticipantToChaincode(oModel, true)
				);
			}  else {
				localStoreAdministratorParticipants.put(formatterAdministratorParticipants.mapAdministratorParticipantToLocalStorage(oModel, true));
			}
			this.loadAllAdministratorParticipants(oComponent, oModel);
			return oModel.getProperty("/newAdministratorParticipant/ID");
		},

		removeAdministratorParticipant : function(oComponent, oModel, administratorParticipantID){

			if ( !( oComponent.getModel("TestSwitch").getProperty("/testMode") ) ) {
				restBuilder.execute(
					oModel.getProperty("/chaincode"),
					"remove",
					{ID:administratorParticipantID}
				);
			} else {
				localStoreAdministratorParticipants.remove(administratorParticipantID);
			}
			this.loadAllAdministratorParticipants(oComponent, oModel);
			return true;
		},

		removeAllAdministratorParticipants : function(oComponent, oModel){

			if (!(oComponent.getModel("TestSwitch").getProperty("/testMode"))) {
				restBuilder.execute(
					oModel.getProperty("/chaincode"),
					"removeAll",
					[]
				);
			} else {
				localStoreAdministratorParticipants.removeAll();
			}
			this.loadAllAdministratorParticipants(oComponent, oModel);
			oModel.setProperty("/selectedAdministratorParticipant",{});
			return true;
		}
	};
});
