sap.ui.define([
	"sap/ui/model/json/JSONModel"
], function(JSONModel) {
	"use strict";

	return {

		createTouchAssetsModel: function(){

			var oModel = new JSONModel();
			oModel.setDefaultBindingMode("TwoWay");
			oModel.setData({
				chaincode:{
					serviceUrl:"",
					serviceKey:{},
					touchAsset:{}
				},
				touchAssetCollection	: {
					cols:[
						{name:"ID"},
						{name:"ObjectType"},
						{name:"Status"},
						{name:"CreationDate"},
            {name:"CompanyID"},
            {name:"BatchID"},
            {name:"Latitude"},
            {name:"Longitude"},
            {name:"Description"},
            {name:"Temperature"}
					],
					items:[]
				},
				selectedTouchAsset:{},
				newTouchAsset:{
					ID:"",
					ObjectType:"",
					Status:"",
					CreationDate:"",
          CompanyID:"",
          BatchID:"",
          Latitude:"",
          Longitude:"",
          Description:"",
          Temperature:""
				},
				selectedTouchAssetID	: "",
				searchTouchAssetID : ""
			});
			return oModel;
		}
	};
});
