sap.ui.define([
	"sap/ui/model/json/JSONModel"
], function(JSONModel) {
	"use strict";

	return {

		createCompanyParticipantsModel: function(){

			var oModel = new JSONModel();
			oModel.setDefaultBindingMode("TwoWay");
			oModel.setData({
				chaincode:{
					serviceUrl:"",
					serviceKey:{},
					companyParticipant:{}
				},
				companyParticipantCollection	: {
					cols:[
						{name:"ID"},
						{name:"ObjectType"},
						{name:"Status"},
						{name:"CreationDate"},
            {name:"Type"},
            {name:"RegistrationID"},
            {name:"CertificationID"},
            {name:"Name"}
					],
					items:[]
				},
				selectedCompanyParticipant:{},
				newCompanyParticipant:{
					ID:"",
					ObjectType:"",
					Status:"",
					CreationDate:"",
          Type:"",
          RegistrationID:"",
          CertificationID:"",
          Name:""
				},
				selectedCompanyParticipantID	: "",
				searchCompanyParticipantID : ""
			});
			return oModel;
		}
	};
});
