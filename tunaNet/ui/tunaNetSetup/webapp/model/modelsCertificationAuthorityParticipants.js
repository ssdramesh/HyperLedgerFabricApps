sap.ui.define([
	"sap/ui/model/json/JSONModel"
], function(JSONModel) {
	"use strict";

	return {

		createCertificationAuthorityParticipantsModel: function(){

			var oModel = new JSONModel();
			oModel.setDefaultBindingMode("TwoWay");
			oModel.setData({
				chaincode:{
					serviceUrl:"",
					serviceKey:{},
					certificationAuthorityParticipant:{}
				},
				certificationAuthorityParticipantCollection	: {
					cols:[
						{name:"ID"},
						{name:"ObjectType"},
						{name:"Status"},
						{name:"CreationDate"}
					],
					items:[]
				},
				selectedCertificationAuthorityParticipant:{},
				newCertificationAuthorityParticipant:{
					ID:"",
					ObjectType:"",
					Status:"",
					CreationDate:""
				},
				selectedCertificationAuthorityParticipantID	: "",
				searchCertificationAuthorityParticipantID : ""
			});
			return oModel;
		}
	};
});
