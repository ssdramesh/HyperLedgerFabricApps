sap.ui.define([
	"sap/ui/model/json/JSONModel"
], function(JSONModel) {
	"use strict";

	return {

		createCertificationRequestAssetAssetsModel: function(){

			var oModel = new JSONModel();
			oModel.setDefaultBindingMode("TwoWay");
			oModel.setData({
				chaincode:{
					serviceUrl:"",
					serviceKey:{},
					certificationRequestAssetAsset:{}
				},
				certificationRequestAssetAssetCollection	: {
					cols:[
						{name:"ID"},
						{name:"ObjectType"},
						{name:"Status"},
						{name:"CreationDate"},
            {name:"CompanyID"},
            {name:"CertificationAuthorityID"}
					],
					items:[]
				},
				selectedCertificationRequestAssetAsset:{},
				newCertificationRequestAssetAsset:{
					ID:"",
					ObjectType:"",
					Status:"",
					CreationDate:"",
          CompanyID:"",
          CertificationAuthorityID:""
				},
				selectedCertificationRequestAssetAssetID	: "",
				searchCertificationRequestAssetAssetID : ""
			});
			return oModel;
		}
	};
});
