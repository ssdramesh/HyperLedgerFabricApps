sap.ui.define([
	"sap/ui/model/json/JSONModel"
], function(JSONModel) {
	"use strict";

	return {

		createCertificateAssetsModel: function(){

			var oModel = new JSONModel();
			oModel.setDefaultBindingMode("TwoWay");
			oModel.setData({
				chaincode:{
					serviceUrl:"",
					serviceKey:{},
					certificateAsset:{}
				},
				certificateAssetCollection	: {
					cols:[
						{name:"ID"},
						{name:"ObjectType"},
						{name:"Status"},
						{name:"CreationDate"},
            {name:"CertificationAuthorityID"},
            {name:"CompanyID"},
            {name:"IssueDate"},
            {name:"ValidityStartDate"},
            {name:"ValidityEndDate"}
					],
					items:[]
				},
				selectedCertificateAsset:{},
				newCertificateAsset:{
					ID:"",
					ObjectType:"",
					Status:"",
					CreationDate:"",
          CertificationAuthorityID:"",
          CompanyID:"",
          IssueDate:"",
          ValidityStartDate:"",
          ValidityEndDate:""
				},
				selectedCertificateAssetID	: "",
				searchCertificateAssetID : ""
			});
			return oModel;
		}
	};
});
