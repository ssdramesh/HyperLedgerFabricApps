sap.ui.define([
	"sap/ui/model/json/JSONModel"
], function(JSONModel) {
	"use strict";

	return {

		createBatchAssetsModel: function(){

			var oModel = new JSONModel();
			oModel.setDefaultBindingMode("TwoWay");
			oModel.setData({
				chaincode:{
					serviceUrl:"",
					serviceKey:{},
					batchAsset:{}
				},
				batchAssetCollection	: {
					cols:[
						{name:"ID"},
						{name:"ObjectType"},
						{name:"Status"},
						{name:"CreationDate"},
            {name:"Weight"},
            {name:"Texture"},
            {name:"Color"},
            {name:"Clarity"},
            {name:"FatContent"},
            {name:"ProductID"},
            {name:"ProductDescription"}
					],
					items:[]
				},
				selectedBatchAsset:{},
				newBatchAsset:{
					ID:"",
					ObjectType:"",
					Status:"",
					CreationDate:"",
          Weight:"",
          Texture:"",
          Color:"",
          Clarity:"",
          FatContent:"",
          ProductID:"",
          ProductDescription:""
				},
				selectedBatchAssetID	: "",
				searchBatchAssetID : ""
			});
			return oModel;
		}
	};
});
