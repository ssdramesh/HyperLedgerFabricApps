This Demo Scenario for the sustainable Tuna fishing track and trace sample network.  The sample data and details are for demo purposes only and no claim is made towards the validity and authenticity of the data used.  The sample data is only to showcase the concept of how blockchain technology can be applied for track and trace use case.

# Use case description
Blockchain technology has the potential to improve transparency and accountability across the supply chain. Applications are already being used to track and trace materials back to the source, prove authenticity and origin, get ahead of recalls, and accelerate the flow of goods.

This demo illustrates how blockchain can be applied to showcase the tuna fishing supply chain, to track and trace the journey of tuna from fishing location at high seas to finished product in the consumer’s hand.

# Data Model
![Data Model](./model/TunaNetWorkStructure.png)

# Demo Flow
Access:
[Base URL](https://onlinetunablockchain-g85556318.dispatcher.jp1.hana.ondemand.com/webapp/index.html?hc_reset)

Enter: BAT001 and search or access below [URL](https://onlinetunablockchain-g85556318.dispatcher.jp1.hana.ondemand.com/webapp/index.html?hc_reset#//milkBatch/BAT001)

# Maintain Sample Data
In case, you want to maintain your own sample data, for example country-specific locations, company names and certification agencies etc.  Please use this [URL](https://tunanetsetup.cfapps.us10.hana.ondemand.com/ui5)

# Contact
Ramesh Suraparaju (I047582)
