package main

//Template Version: 1.1-20180704
import (
	"encoding/json"
	"errors"
	"fmt"

	"github.com/hyperledger/fabric/core/chaincode/shim"
	"github.com/hyperledger/fabric/protos/peer"
)

var logger = shim.NewLogger("CLDChaincode")

//CertificationRequestAssetAsset - Chaincode for asset CertificationRequestAsset
type CertificationRequestAssetAsset struct {
}

//CertificationRequestAsset - Details of the asset type CertificationRequestAsset
type CertificationRequestAsset struct {
 ID          string `json:"ID"`
 ObjectType   string `json:"docType"`
  Status       string `json:"status"`
  CreationDate string `json:"creationDate"`
CompanyID string `json:"companyID"`
CertificationAuthorityID string `json:"certificationAuthorityID"`
}

//CertificationRequestAssetIDIndex - Index on IDs for retrieval all CertificationRequestAssets
type CertificationRequestAssetIDIndex struct {
	IDs []string `json:"IDs"`
}

func main() {
	err := shim.Start(new(CertificationRequestAssetAsset))
	if err != nil {
		fmt.Printf("Error starting CertificationRequestAssetAsset chaincode function main(): %s", err)
	} else {
		fmt.Printf("Starting CertificationRequestAssetAsset chaincode function main() executed successfully")
	}
}

//Init - The chaincode Init function: No  arguments, only initializes a ID array as Index for retrieval of all CertificationRequestAssets
func (certReq *CertificationRequestAssetAsset) Init(stub shim.ChaincodeStubInterface) peer.Response {
	var certificationRequestAssetIDIndex CertificationRequestAssetIDIndex
	record, _ := stub.GetState("certificationRequestAssetIDIndex")
	//chaincode upgrade: Index might already exist, create new only if it does not
	if record == nil {
		bytes, _ := json.Marshal(certificationRequestAssetIDIndex)
		stub.PutState("certificationRequestAssetIDIndex", bytes)
	}
	return shim.Success(nil)
}

//Invoke - The chaincode Invoke function:
func (certReq *CertificationRequestAssetAsset) Invoke(stub shim.ChaincodeStubInterface) peer.Response {
	function, args := stub.GetFunctionAndParameters()
	logger.Debug("function: ", function)
	switch function {
	case "addNewCertificationRequestAsset":
		return certReq.addNewCertificationRequestAsset(stub, args)
	case "removeCertificationRequestAsset":
		return certReq.removeCertificationRequestAsset(stub, args[0])
	case "removeAllCertificationRequestAssets":
		return certReq.removeAllCertificationRequestAssets(stub)
	case "readCertificationRequestAsset":
		return certReq.readCertificationRequestAsset(stub, args[0])
	case "readAllCertificationRequestAssets":
		return certReq.readAllCertificationRequestAssets(stub)
	default:
		return shim.Error("Received unknown function invocation")
	}
}

//Invoke Route: addNewCertificationRequestAsset
func (certReq *CertificationRequestAssetAsset) addNewCertificationRequestAsset(stub shim.ChaincodeStubInterface, args []string) peer.Response {
	certificationRequestAsset, err := getCertificationRequestAssetFromArgs(args)
	if err != nil {
		return shim.Error("CertificationRequestAsset Data is Corrupted")
	}
	certificationRequestAsset.ObjectType = "Asset.CertificationRequestAssetAsset"
	record, err := stub.GetState(certificationRequestAsset.ID)
	if record != nil {
		return shim.Error("This CertificationRequestAsset already exists: " + certificationRequestAsset.ID)
	}
	_, err = certReq.saveCertificationRequestAsset(stub, certificationRequestAsset)
	if err != nil {
		return shim.Error(err.Error())
	}
	_, err = certReq.updateCertificationRequestAssetIDIndex(stub, certificationRequestAsset)
	if err != nil {
		return shim.Error(err.Error())
	}
	return shim.Success(nil)
}

//Invoke Route: removeCertificationRequestAsset
func (certReq *CertificationRequestAssetAsset) removeCertificationRequestAsset(stub shim.ChaincodeStubInterface, certificationRequestAssetID string) peer.Response {
	_, err := certReq.deleteCertificationRequestAsset(stub, certificationRequestAssetID)
	if err != nil {
		return shim.Error(err.Error())
	}
	_, err = certReq.deleteCertificationRequestAssetIDIndex(stub, certificationRequestAssetID)
	if err != nil {
		return shim.Error(err.Error())
	}
	return shim.Success(nil)
}

//Invoke Route: removeAllCertificationRequestAssets
func (certReq *CertificationRequestAssetAsset) removeAllCertificationRequestAssets(stub shim.ChaincodeStubInterface) peer.Response {
	var certificationRequestAssetIDIndex CertificationRequestAssetIDIndex
	bytes, err := stub.GetState("certificationRequestAssetIDIndex")
	if err != nil {
		return shim.Error("removeAllCertificationRequestAssets: Error getting certificationRequestAssetIDIndex array")
	}
	err = json.Unmarshal(bytes, &certificationRequestAssetIDIndex)
	if err != nil {
		return shim.Error("removeAllCertificationRequestAssets: Error unmarshalling certificationRequestAssetIDIndex array JSON")
	}
	if len(certificationRequestAssetIDIndex.IDs) == 0 {
		return shim.Error("removeAllCertificationRequestAssets: No certificationRequestAssets to remove")
	}
	for _, certificationRequestAssetStructID := range certificationRequestAssetIDIndex.IDs {
		_, err = certReq.deleteCertificationRequestAsset(stub, certificationRequestAssetStructID)
		if err != nil {
			return shim.Error("Failed to remove CertificationRequestAsset with ID: " + certificationRequestAssetStructID)
		}
		_, err = certReq.deleteCertificationRequestAssetIDIndex(stub, certificationRequestAssetStructID)
		if err != nil {
			return shim.Error(err.Error())
		}
	}
	certReq.initHolder(stub)
	return shim.Success(nil)
}

//Query Route: readCertificationRequestAsset
func (certReq *CertificationRequestAssetAsset) readCertificationRequestAsset(stub shim.ChaincodeStubInterface, certificationRequestAssetID string) peer.Response {
	certificationRequestAssetAsByteArray, err := certReq.retrieveCertificationRequestAsset(stub, certificationRequestAssetID)
	if err != nil {
		return shim.Error(err.Error())
	}
	return shim.Success(certificationRequestAssetAsByteArray)
}

//Query Route: readAllCertificationRequestAssets
func (certReq *CertificationRequestAssetAsset) readAllCertificationRequestAssets(stub shim.ChaincodeStubInterface) peer.Response {
	var certificationRequestAssetIDs CertificationRequestAssetIDIndex
	bytes, err := stub.GetState("certificationRequestAssetIDIndex")
	if err != nil {
		return shim.Error("readAllCertificationRequestAssets: Error getting certificationRequestAssetIDIndex array")
	}
	err = json.Unmarshal(bytes, &certificationRequestAssetIDs)
	if err != nil {
		return shim.Error("readAllCertificationRequestAssets: Error unmarshalling certificationRequestAssetIDIndex array JSON")
	}
	result := "["

	var certificationRequestAssetAsByteArray []byte

	for _, certificationRequestAssetID := range certificationRequestAssetIDs.IDs {
		certificationRequestAssetAsByteArray, err = certReq.retrieveCertificationRequestAsset(stub, certificationRequestAssetID)
		if err != nil {
			return shim.Error("Failed to retrieve certificationRequestAsset with ID: " + certificationRequestAssetID)
		}
		result += string(certificationRequestAssetAsByteArray) + ","
	}
	if len(result) == 1 {
		result = "[]"
	} else {
		result = result[:len(result)-1] + "]"
	}
	return shim.Success([]byte(result))
}

//Helper: Save CertificationRequestAssetAsset
func (certReq *CertificationRequestAssetAsset) saveCertificationRequestAsset(stub shim.ChaincodeStubInterface, certificationRequestAsset CertificationRequestAsset) (bool, error) {
	bytes, err := json.Marshal(certificationRequestAsset)
	if err != nil {
		return false, errors.New("Error converting certificationRequestAsset record JSON")
	}
	err = stub.PutState(certificationRequestAsset.ID, bytes)
	if err != nil {
		return false, errors.New("Error storing CertificationRequestAsset record")
	}
	return true, nil
}

//Helper: delete CertificationRequestAssetAsset
func (certReq *CertificationRequestAssetAsset) deleteCertificationRequestAsset(stub shim.ChaincodeStubInterface, certificationRequestAssetID string) (bool, error) {
	_, err := certReq.retrieveCertificationRequestAsset(stub, certificationRequestAssetID)
	if err != nil {
		return false, errors.New("CertificationRequestAsset with ID: " + certificationRequestAssetID + " not found")
	}
	err = stub.DelState(certificationRequestAssetID)
	if err != nil {
		return false, errors.New("Error deleting CertificationRequestAsset record")
	}
	return true, nil
}

//Helper: Update certificationRequestAsset Holder - updates Index
func (certReq *CertificationRequestAssetAsset) updateCertificationRequestAssetIDIndex(stub shim.ChaincodeStubInterface, certificationRequestAsset CertificationRequestAsset) (bool, error) {
	var certificationRequestAssetIDs CertificationRequestAssetIDIndex
	bytes, err := stub.GetState("certificationRequestAssetIDIndex")
	if err != nil {
		return false, errors.New("updateCertificationRequestAssetIDIndex: Error getting certificationRequestAssetIDIndex array Index from state")
	}
	err = json.Unmarshal(bytes, &certificationRequestAssetIDs)
	if err != nil {
		return false, errors.New("updateCertificationRequestAssetIDIndex: Error unmarshalling certificationRequestAssetIDIndex array JSON")
	}
	certificationRequestAssetIDs.IDs = append(certificationRequestAssetIDs.IDs, certificationRequestAsset.ID)
	bytes, err = json.Marshal(certificationRequestAssetIDs)
	if err != nil {
		return false, errors.New("updateCertificationRequestAssetIDIndex: Error marshalling new certificationRequestAsset ID")
	}
	err = stub.PutState("certificationRequestAssetIDIndex", bytes)
	if err != nil {
		return false, errors.New("updateCertificationRequestAssetIDIndex: Error storing new certificationRequestAsset ID in certificationRequestAssetIDIndex (Index)")
	}
	return true, nil
}

//Helper: delete ID from certificationRequestAssetStruct Holder
func (certReq *CertificationRequestAssetAsset) deleteCertificationRequestAssetIDIndex(stub shim.ChaincodeStubInterface, certificationRequestAssetID string) (bool, error) {
	var certificationRequestAssetIDIndex CertificationRequestAssetIDIndex
	bytes, err := stub.GetState("certificationRequestAssetIDIndex")
	if err != nil {
		return false, errors.New("deleteCertificationRequestAssetIDIndex: Error getting certificationRequestAssetIDIndex array Index from state")
	}
	err = json.Unmarshal(bytes, &certificationRequestAssetIDIndex)
	if err != nil {
		return false, errors.New("deleteCertificationRequestAssetIDIndex: Error unmarshalling certificationRequestAssetIDIndex array JSON")
	}
	certificationRequestAssetIDIndex.IDs, err = deleteKeyFromStringArray(certificationRequestAssetIDIndex.IDs, certificationRequestAssetID)
	if err != nil {
		return false, errors.New(err.Error())
	}
	bytes, err = json.Marshal(certificationRequestAssetIDIndex)
	if err != nil {
		return false, errors.New("deleteCertificationRequestAssetIDIndex: Error marshalling new certificationRequestAssetStruct ID")
	}
	err = stub.PutState("certificationRequestAssetIDIndex", bytes)
	if err != nil {
		return false, errors.New("deleteCertificationRequestAssetIDIndex: Error storing new certificationRequestAssetStruct ID in certificationRequestAssetIDIndex (Index)")
	}
	return true, nil
}

//Helper: Initialize truck ID Holder //change template
func (certReq *CertificationRequestAssetAsset) initHolder(stub shim.ChaincodeStubInterface) bool {
	var certificationRequestAssetIDIndex CertificationRequestAssetIDIndex
	bytes, _ := json.Marshal(certificationRequestAssetIDIndex)
	stub.DelState("certificationRequestAssetIDIndex")
	stub.PutState("certificationRequestAssetIDIndex", bytes)
	return true
}

//deleteKeyFromArray
func deleteKeyFromStringArray(array []string, key string) (newArray []string, err error) {
	for _, entry := range array {
		if entry != key {
			newArray = append(newArray, entry)
		}
	}
	if len(newArray) == len(array) {
		return newArray, errors.New("Specified Key: " + key + " not found in Array")
	}
	return newArray, nil
}

//Helper: Retrieve purchaser
func (certReq *CertificationRequestAssetAsset) retrieveCertificationRequestAsset(stub shim.ChaincodeStubInterface, certificationRequestAssetID string) ([]byte, error) {
	var certificationRequestAsset CertificationRequestAsset
	var certificationRequestAssetAsByteArray []byte
	bytes, err := stub.GetState(certificationRequestAssetID)
	if err != nil {
		return certificationRequestAssetAsByteArray, errors.New("retrieveCertificationRequestAsset: Error retrieving certificationRequestAsset with ID: " + certificationRequestAssetID)
	}
	err = json.Unmarshal(bytes, &certificationRequestAsset)
	if err != nil {
		return certificationRequestAssetAsByteArray, errors.New("retrieveCertificationRequestAsset: Corrupt certificationRequestAsset record " + string(bytes))
	}
	certificationRequestAssetAsByteArray, err = json.Marshal(certificationRequestAsset)
	if err != nil {
		return certificationRequestAssetAsByteArray, errors.New("readCertificationRequestAsset: Invalid certificationRequestAsset Object - Not a  valid JSON")
	}
	return certificationRequestAssetAsByteArray, nil
}

//getCertificationRequestAssetFromArgs - construct a certificationRequestAsset structure from string array of arguments
func getCertificationRequestAssetFromArgs(args []string) (certificationRequestAsset CertificationRequestAsset, err error) {

	if !Valid(args[0]) {
		return certificationRequestAsset, errors.New("Invalid json")
	}

	err = Unmarshal([]byte(args[0]), &certificationRequestAsset)
	if err != nil {
		return certificationRequestAsset, err
	}
	return certificationRequestAsset, nil
}
