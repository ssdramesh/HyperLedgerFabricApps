package main

//Template Version: 1.1-20180704
import (
	"bytes"
	"encoding/json"
	"fmt"
	"strings"
	"testing"

	"github.com/hyperledger/fabric/core/chaincode/shim"
)

//TestCertificationRequestAssetAsset_Init
func TestCertificationRequestAssetAsset_Init(t *testing.T) {
	certificationRequestAsset := new(CertificationRequestAssetAsset)
	stub := shim.NewMockStub("certificationRequestAsset", certificationRequestAsset)
	checkInit(t, stub, [][]byte{[]byte("init")})
	checkState(t, stub, "certificationRequestAssetIDIndex", getExpectedCertificationRequestAssetIDIndex("init"))
}

//TestCertificationRequestAssetAsset_InvokeUnknownFunction
func TestCertificationRequestAssetAsset_InvokeUnknownFunction(t *testing.T) {
	certificationRequestAsset := new(CertificationRequestAssetAsset)
	stub := shim.NewMockStub("certificationRequestAsset", certificationRequestAsset)
	checkInit(t, stub, [][]byte{[]byte("init")})
	checkInvokeUnknownFunction(t, stub, [][]byte{[]byte("myFunction"), []byte("docType:Asset")})
}

//TestCertificationRequestAssetAsset_Invoke_addNewCertificationRequestAsset
func TestCertificationRequestAssetAsset_Invoke_addNewCertificationRequestAssetOK(t *testing.T) {
	certificationRequestAsset := new(CertificationRequestAssetAsset)
	stub := shim.NewMockStub("certificationRequestAsset", certificationRequestAsset)
	checkInit(t, stub, [][]byte{[]byte("init")})
	checkInvoke(t, stub, getFirstCertificationRequestAssetAssetForTesting())
	newCertificationRequestAssetID := "100001"
	checkState(t, stub, newCertificationRequestAssetID, getNewCertificationRequestAssetExpected())
	checkState(t, stub, "certificationRequestAssetIDIndex", getExpectedCertificationRequestAssetIDIndex("addNewCertificationRequestAsset"))
}

//TestCertificationRequestAssetAsset_Invoke_addNewCertificationRequestAsset
func TestCertificationRequestAssetAsset_Invoke_addNewCertificationRequestAssetDuplicate(t *testing.T) {
	certificationRequestAsset := new(CertificationRequestAssetAsset)
	stub := shim.NewMockStub("certificationRequestAsset", certificationRequestAsset)
	checkInit(t, stub, [][]byte{[]byte("init")})
	checkInvoke(t, stub, getFirstCertificationRequestAssetAssetForTesting())
	newCertificationRequestAssetID := "100001"
	checkState(t, stub, newCertificationRequestAssetID, getNewCertificationRequestAssetExpected())
	checkState(t, stub, "certificationRequestAssetIDIndex", getExpectedCertificationRequestAssetIDIndex("addNewCertificationRequestAsset"))
	res := stub.MockInvoke("1", getFirstCertificationRequestAssetAssetForTesting())
	if res.Status != shim.OK {
		checkError(t, "This CertificationRequestAsset already exists: 100001", res.Message)
	} else {
		fmt.Println("Error was expected, but not raised")
		t.FailNow()
	}
}

//TestCertificationRequestAssetAsset_Invoke_removeCertificationRequestAssetOK  //change template
func TestCertificationRequestAssetAsset_Invoke_removeCertificationRequestAssetOK(t *testing.T) {
	certificationRequestAsset := new(CertificationRequestAssetAsset)
	stub := shim.NewMockStub("certificationRequestAsset", certificationRequestAsset)
	checkInit(t, stub, [][]byte{[]byte("init")})
	checkInvoke(t, stub, getFirstCertificationRequestAssetAssetForTesting())
	checkInvoke(t, stub, getSecondCertificationRequestAssetAssetForTesting())
	checkReadAllCertificationRequestAssetsOK(t, stub)
	checkState(t, stub, "certificationRequestAssetIDIndex", getExpectedCertificationRequestAssetIDIndex("beforeRemoveCertificationRequestAsset"))
	checkInvoke(t, stub, getRemoveSecondCertificationRequestAssetAssetForTesting())
	remainingCertificationRequestAssetID := "100001"
	checkReadCertificationRequestAssetOK(t, stub, remainingCertificationRequestAssetID)
	checkState(t, stub, "certificationRequestAssetIDIndex", getExpectedCertificationRequestAssetIDIndex("afterRemoveCertificationRequestAsset"))
}

//TestCertificationRequestAssetAsset_Invoke_removeCertificationRequestAssetNOK  //change template
func TestCertificationRequestAssetAsset_Invoke_removeCertificationRequestAssetNOK(t *testing.T) {
	certificationRequestAsset := new(CertificationRequestAssetAsset)
	stub := shim.NewMockStub("certificationRequestAsset", certificationRequestAsset)
	checkInit(t, stub, [][]byte{[]byte("init")})
	checkInvoke(t, stub, getFirstCertificationRequestAssetAssetForTesting())
	firstCertificationRequestAssetID := "100001"
	checkReadCertificationRequestAssetOK(t, stub, firstCertificationRequestAssetID)
	checkState(t, stub, "certificationRequestAssetIDIndex", getExpectedCertificationRequestAssetIDIndex("addNewCertificationRequestAsset"))
	res := stub.MockInvoke("1", getRemoveSecondCertificationRequestAssetAssetForTesting())
	if res.Status != shim.OK {
		checkError(t, "CertificationRequestAsset with ID: "+"100002"+" not found", res.Message)
	} else {
		fmt.Println("Error was expected, but not raised")
		t.FailNow()
	}
	checkState(t, stub, "certificationRequestAssetIDIndex", getExpectedCertificationRequestAssetIDIndex("addNewCertificationRequestAsset"))
}

//TestCertificationRequestAssetAsset_Invoke_removeAllCertificationRequestAssetsOK  //change template
func TestCertificationRequestAssetAsset_Invoke_removeAllCertificationRequestAssetsOK(t *testing.T) {
	certificationRequestAsset := new(CertificationRequestAssetAsset)
	stub := shim.NewMockStub("certificationRequestAsset", certificationRequestAsset)
	checkInit(t, stub, [][]byte{[]byte("init")})
	checkInvoke(t, stub, getFirstCertificationRequestAssetAssetForTesting())
	checkInvoke(t, stub, getSecondCertificationRequestAssetAssetForTesting())
	checkReadAllCertificationRequestAssetsOK(t, stub)
	checkState(t, stub, "certificationRequestAssetIDIndex", getExpectedCertificationRequestAssetIDIndex("beforeRemoveCertificationRequestAsset"))
	checkInvoke(t, stub, getRemoveAllCertificationRequestAssetAssetsForTesting())
	checkState(t, stub, "certificationRequestAssetIDIndex", getExpectedCertificationRequestAssetIDIndex(""))
}

//TestCertificationRequestAssetAsset_Invoke_removeCertificationRequestAssetNOK  //change template
func TestCertificationRequestAssetAsset_Invoke_removeAllCertificationRequestAssetsNOK(t *testing.T) {
	certificationRequestAsset := new(CertificationRequestAssetAsset)
	stub := shim.NewMockStub("certificationRequestAsset", certificationRequestAsset)
	checkInit(t, stub, [][]byte{[]byte("init")})
	res := stub.MockInvoke("1", getRemoveAllCertificationRequestAssetAssetsForTesting())
	if res.Status != shim.OK {
		checkError(t, "removeAllCertificationRequestAssets: No certificationRequestAssets to remove", res.Message)
	} else {
		fmt.Println("Error was expected, but not raised")
		t.FailNow()
	}
	checkState(t, stub, "certificationRequestAssetIDIndex", getExpectedCertificationRequestAssetIDIndex(""))
}

//TestCertificationRequestAssetAsset_Query_readCertificationRequestAsset
func TestCertificationRequestAssetAsset_Query_readCertificationRequestAsset(t *testing.T) {
	certificationRequestAsset := new(CertificationRequestAssetAsset)
	stub := shim.NewMockStub("certificationRequestAsset", certificationRequestAsset)
	checkInit(t, stub, [][]byte{[]byte("init")})
	certificationRequestAssetID := "100001"
	checkInvoke(t, stub, getFirstCertificationRequestAssetAssetForTesting())
	checkReadCertificationRequestAssetOK(t, stub, certificationRequestAssetID)
	checkReadCertificationRequestAssetNOK(t, stub, "")
}

//TestCertificationRequestAssetAsset_Query_readAllCertificationRequestAssets
func TestCertificationRequestAssetAsset_Query_readAllCertificationRequestAssets(t *testing.T) {
	certificationRequestAsset := new(CertificationRequestAssetAsset)
	stub := shim.NewMockStub("certificationRequestAsset", certificationRequestAsset)
	checkInit(t, stub, [][]byte{[]byte("init")})
	checkInvoke(t, stub, getFirstCertificationRequestAssetAssetForTesting())
	checkInvoke(t, stub, getSecondCertificationRequestAssetAssetForTesting())
	checkReadAllCertificationRequestAssetsOK(t, stub)
}

/*
*
*	Helper Functions
*
 */
//Get first CertificationRequestAssetAsset for testing
func getFirstCertificationRequestAssetAssetForTesting() [][]byte { 
 return [][]byte{[]byte("addNewCertificationRequestAsset"), 
 []byte("{\"ID\":\"100001\",\"docType\":\"Asset.CertificationRequestAssetAsset\",\"status\":\"0\",\"creationDate\":\"12/01/2018\", \"companyID\":\"companyID001\", \"certificationAuthorityID\":\"certificationAuthorityID001\"}")}
}

//Get second CertificationRequestAssetAsset for testing
func getSecondCertificationRequestAssetAssetForTesting() [][]byte { 
 return [][]byte{[]byte("addNewCertificationRequestAsset"), 
 []byte("{\"ID\":\"100002\",\"docType\":\"Asset.CertificationRequestAssetAsset\",\"status\":\"0\",\"creationDate\":\"12/01/2018\", \"companyID\":\"companyID002\", \"certificationAuthorityID\":\"certificationAuthorityID002\"}")}
}

//Get remove second CertificationRequestAssetAsset for testing //change template
func getRemoveSecondCertificationRequestAssetAssetForTesting() [][]byte {
	return [][]byte{[]byte("removeCertificationRequestAsset"),
		[]byte("100002")}
}

//Get remove all CertificationRequestAssetAssets for testing //change template
func getRemoveAllCertificationRequestAssetAssetsForTesting() [][]byte {
	return [][]byte{[]byte("removeAllCertificationRequestAssets")}
}

//Get an expected value for testing
func getNewCertificationRequestAssetExpected() []byte {
	var certificationRequestAsset CertificationRequestAsset
		certificationRequestAsset.ID = "100001"
	certificationRequestAsset.ObjectType = "Asset.CertificationRequestAssetAsset"
  certificationRequestAsset.Status = "0"
	certificationRequestAsset.CreationDate = "12/01/2018"
certificationRequestAsset.CompanyID="companyID001"
certificationRequestAsset.CertificationAuthorityID="certificationAuthorityID001"
	certificationRequestAssetJSON, err := json.Marshal(certificationRequestAsset)
	if err != nil {
		fmt.Println("Error converting a CertificationRequestAsset record to JSON")
		return nil
	}
	return []byte(certificationRequestAssetJSON)
}

//Get expected values of CertificationRequestAssets for testing
func getExpectedCertificationRequestAssets() []byte {
	var certificationRequestAssets []CertificationRequestAsset
	var certificationRequestAsset CertificationRequestAsset
		certificationRequestAsset.ID = "100001"
	certificationRequestAsset.ObjectType = "Asset.CertificationRequestAssetAsset"
  certificationRequestAsset.Status = "0"
	certificationRequestAsset.CreationDate = "12/01/2018"
certificationRequestAsset.CompanyID="companyID001"
certificationRequestAsset.CertificationAuthorityID="certificationAuthorityID001"
	certificationRequestAssets = append(certificationRequestAssets, certificationRequestAsset)
		certificationRequestAsset.ID = "100002"
	certificationRequestAsset.ObjectType = "Asset.CertificationRequestAssetAsset"
  certificationRequestAsset.Status = "0"
	certificationRequestAsset.CreationDate = "12/01/2018"
certificationRequestAsset.CompanyID="companyID002"
certificationRequestAsset.CertificationAuthorityID="certificationAuthorityID002"
	certificationRequestAssets = append(certificationRequestAssets, certificationRequestAsset)
	certificationRequestAssetJSON, err := json.Marshal(certificationRequestAssets)
	if err != nil {
		fmt.Println("Error converting certificationRequestAsset records to JSON")
		return nil
	}
	return []byte(certificationRequestAssetJSON)
}

func getExpectedCertificationRequestAssetIDIndex(funcName string) []byte {
	var certificationRequestAssetIDIndex CertificationRequestAssetIDIndex
	switch funcName {
	case "addNewCertificationRequestAsset":
		certificationRequestAssetIDIndex.IDs = append(certificationRequestAssetIDIndex.IDs, "100001")
		certificationRequestAssetIDIndexBytes, err := json.Marshal(certificationRequestAssetIDIndex)
		if err != nil {
			fmt.Println("Error converting CertificationRequestAssetIDIndex to JSON")
			return nil
		}
		return certificationRequestAssetIDIndexBytes
	case "beforeRemoveCertificationRequestAsset":
		certificationRequestAssetIDIndex.IDs = append(certificationRequestAssetIDIndex.IDs, "100001")
		certificationRequestAssetIDIndex.IDs = append(certificationRequestAssetIDIndex.IDs, "100002")
		certificationRequestAssetIDIndexBytes, err := json.Marshal(certificationRequestAssetIDIndex)
		if err != nil {
			fmt.Println("Error converting CertificationRequestAssetIDIndex to JSON")
			return nil
		}
		return certificationRequestAssetIDIndexBytes
	case "afterRemoveCertificationRequestAsset":
		certificationRequestAssetIDIndex.IDs = append(certificationRequestAssetIDIndex.IDs, "100001")
		certificationRequestAssetIDIndexBytes, err := json.Marshal(certificationRequestAssetIDIndex)
		if err != nil {
			fmt.Println("Error converting CertificationRequestAssetIDIndex to JSON")
			return nil
		}
		return certificationRequestAssetIDIndexBytes
	default:
		certificationRequestAssetIDIndexBytes, err := json.Marshal(certificationRequestAssetIDIndex)
		if err != nil {
			fmt.Println("Error converting CertificationRequestAssetIDIndex to JSON")
			return nil
		}
		return certificationRequestAssetIDIndexBytes
	}
}

//checkInit - helper to check the Initialization of chaincode: CertificationRequestAssetAsset
func checkInit(t *testing.T, stub *shim.MockStub, args [][]byte) {
	res := stub.MockInit("1", args)
	if res.Status != shim.OK {
		fmt.Println("Init failed", string(res.Message))
		t.FailNow()
	}
}

//checkState - helper for checking the chaincode state for a given stateKey afgainst an expected value
func checkState(t *testing.T, stub *shim.MockStub, stateKey string, expectedState []byte) {
	actualState := stub.State[stateKey]
	if actualState == nil {
		fmt.Println("State for ", stateKey, ": failed to get value")
		t.FailNow()
	}

	if bytes.Compare(actualState, expectedState) != 0 {
		fmt.Println("Incorrect State for " + stateKey + ": \nExpected: " + string(expectedState) + "\nActual  : " + string(actualState))
		t.FailNow()
	}
}

//checkInvoke - helper for checking Invoke of chaincode
func checkInvoke(t *testing.T, stub *shim.MockStub, args [][]byte) {
	res := stub.MockInvoke("1", args)
	if res.Status != shim.OK {
		fmt.Println("Invoke", args, "failed", string(res.Message))
		t.FailNow()
	}
}

func checkInvokeUnknownFunction(t *testing.T, stub *shim.MockStub, args [][]byte) {
	res := stub.MockInvoke("1", args)
	if res.Status != shim.OK {
		expectedErr := "Received unknown function invocation"
		actualErr := string(res.Message)
		if !(strings.Contains(actualErr, expectedErr)) {
			fmt.Println("Invoke with unknown function must throw error: ", expectedErr, "\n Actual Error :", actualErr)
			t.FailNow()
		}
	} else {
		t.FailNow()
	}
}

//checkReadCertificationRequestAssetOK - helper for positive test readCertificationRequestAsset
func checkReadCertificationRequestAssetOK(t *testing.T, stub *shim.MockStub, certificationRequestAssetID string) {
	res := stub.MockInvoke("1", [][]byte{[]byte("readCertificationRequestAsset"), []byte(certificationRequestAssetID)})
	if res.Status != shim.OK {
		fmt.Println("func readCertificationRequestAsset with ID: ", certificationRequestAssetID, " failed"+string(res.Message))
		t.FailNow()
	}
	if res.Payload == nil {
		fmt.Println("func readCertificationRequestAsset with ID: ", certificationRequestAssetID, "failed to get value")
		t.FailNow()
	}
	if bytes.Compare(getNewCertificationRequestAssetExpected(), []byte(res.Payload)) != 0 {
		fmt.Println("func readCertificationRequestAsset with ID: ", certificationRequestAssetID, "Expected:", string(getNewCertificationRequestAssetExpected()), "Actual:", string(res.Payload))
		t.FailNow()
	}
}

//checkReadCertificationRequestAssetNOK - helper for negative testing of readCertificationRequestAsset
func checkReadCertificationRequestAssetNOK(t *testing.T, stub *shim.MockStub, certificationRequestAssetID string) {
	//with no certificationRequestAssetID
	res := stub.MockInvoke("1", [][]byte{[]byte("readCertificationRequestAsset"), []byte("")})
	if res.Status != shim.OK {
		expectedErr := "retrieveCertificationRequestAsset: Corrupt certificationRequestAsset record "
		actualErr := string(res.Message)
		if !(strings.Contains(actualErr, expectedErr)) {
			fmt.Println("func readCertificationRequestAsset negative test: ", "Expected Error:", expectedErr, "Actual Error", actualErr)
			t.FailNow()
		}
	} else {
		t.FailNow()
	}
}

func checkReadAllCertificationRequestAssetsOK(t *testing.T, stub *shim.MockStub) {
	res := stub.MockInvoke("1", [][]byte{[]byte("readAllCertificationRequestAssets")})
	if res.Status != shim.OK {
		fmt.Println("func readAllCertificationRequestAssets failed", string(res.Message))
		t.FailNow()
	}
	if res.Payload == nil {
		fmt.Println("func readAllCertificationRequestAssets failed to get value")
		t.FailNow()
	}
	if bytes.Compare(getExpectedCertificationRequestAssets(), []byte(res.Payload)) != 0 {
		fmt.Println("func readAllCertificationRequestAssets Expected:\n", string(getExpectedCertificationRequestAssets()), "\nActual:\n", string(res.Payload))
		t.FailNow()
	}
}

func checkError(t *testing.T, exp string, act string) {
	if strings.Compare(exp, act) != 0 {
		fmt.Println("Unexpected Error! Expecting ", exp, "\n Actual :", act)
		t.FailNow()
	}
}
