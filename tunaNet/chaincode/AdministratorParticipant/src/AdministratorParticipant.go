package main

//Template Version: 1.1-20180704
import (
	"encoding/json"
	"errors"
	"fmt"

	"github.com/hyperledger/fabric/core/chaincode/shim"
	"github.com/hyperledger/fabric/protos/peer"
)

var logger = shim.NewLogger("CLDChaincode")

//AdministratorParticipant - Chaincode for Administrator Participant
type AdministratorParticipant struct {
}

//Administrator - Details of the participant type Administrator
type Administrator struct {
	ID          string `json:"ID"`
	ObjectType  string `json:"docType"`
	Alias       string `json:"alias"`
	Description string `json:"description"`
}

//AdministratorIDIndex - Index on IDs for retrieval all Administrators
type AdministratorIDIndex struct {
	IDs []string `json:"IDs"`
}

func main() {
	err := shim.Start(new(AdministratorParticipant))
	if err != nil {
		fmt.Printf("Error starting AdministratorParticipant chaincode function main(): %s", err)
	} else {
		fmt.Printf("Starting AdministratorParticipant chaincode function main() executed successfully")
	}
}

//Init - The chaincode Init function: No  arguments, only initializes a ID array as Index for retrieval of all Administrators
func (admin *AdministratorParticipant) Init(stub shim.ChaincodeStubInterface) peer.Response {
	var administratorIDIndex AdministratorIDIndex
	record, _ := stub.GetState("administratorIDIndex")
	//chaincode upgrade: Index might already exist, create new only if it does not
	if record == nil {
		bytes, _ := json.Marshal(administratorIDIndex)
		stub.PutState("administratorIDIndex", bytes)
	}
	return shim.Success(nil)
}

//Invoke - The chaincode Invoke function:
func (admin *AdministratorParticipant) Invoke(stub shim.ChaincodeStubInterface) peer.Response {
	function, args := stub.GetFunctionAndParameters()
	logger.Debug("function: ", function)
	switch function {
	case "addNewAdministrator":
		return admin.addNewAdministrator(stub, args)
	case "removeAdministrator":
		return admin.removeAdministrator(stub, args[0])
	case "removeAllAdministrators":
		return admin.removeAllAdministrators(stub)
	case "readAdministrator":
		return admin.readAdministrator(stub, args[0])
	case "readAllAdministrators":
		return admin.readAllAdministrators(stub)
	default:
		return shim.Error("Received unknown function invocation")
	}
}

//Invoke Route: addNewAdministrator
func (admin *AdministratorParticipant) addNewAdministrator(stub shim.ChaincodeStubInterface, args []string) peer.Response {
	administrator, err := getAdministratorFromArgs(args)
	if err != nil {
		return shim.Error("Administrator Data is Corrupted")
	}
	administrator.ObjectType = "Participant.AdministratorParticipant"
	record, err := stub.GetState(administrator.ID)
	if record != nil {
		return shim.Error("This Administrator already exists: " + administrator.ID)
	}
	_, err = admin.saveAdministrator(stub, administrator)
	if err != nil {
		return shim.Error(err.Error())
	}
	_, err = admin.updateAdministratorIDIndex(stub, administrator)
	if err != nil {
		return shim.Error(err.Error())
	}
	return shim.Success(nil)
}

//Invoke Route: removeAdministrator
func (admin *AdministratorParticipant) removeAdministrator(stub shim.ChaincodeStubInterface, administratorStructParticipantID string) peer.Response {
	_, err := admin.deleteAdministrator(stub, administratorStructParticipantID)
	if err != nil {
		return shim.Error(err.Error())
	}
	_, err = admin.deleteAdministratorIDIndex(stub, administratorStructParticipantID)
	if err != nil {
		return shim.Error(err.Error())
	}
	return shim.Success(nil)
}

//Invoke Route: removeAllAdministrators
func (admin *AdministratorParticipant) removeAllAdministrators(stub shim.ChaincodeStubInterface) peer.Response {
	var administratorStructIDs AdministratorIDIndex
	bytes, err := stub.GetState("administratorIDIndex")
	if err != nil {
		return shim.Error("removeAllAdministrators: Error getting administratorIDIndex array")
	}
	err = json.Unmarshal(bytes, &administratorStructIDs)
	if err != nil {
		return shim.Error("removeAllAdministrators: Error unmarshalling administratorIDIndex array JSON")
	}
	if len(administratorStructIDs.IDs) == 0 {
		return shim.Error("removeAllAdministrators: No administrators to remove")
	}
	for _, administratorStructParticipantID := range administratorStructIDs.IDs {
		_, err = admin.deleteAdministrator(stub, administratorStructParticipantID)
		if err != nil {
			return shim.Error("Failed to remove Administrator with ID: " + administratorStructParticipantID)
		}
		_, err = admin.deleteAdministratorIDIndex(stub, administratorStructParticipantID)
		if err != nil {
			return shim.Error(err.Error())
		}
	}
	admin.initHolder(stub)
	return shim.Success(nil)
}

//Query Route: readAdministrator
func (admin *AdministratorParticipant) readAdministrator(stub shim.ChaincodeStubInterface, administratorParticipantID string) peer.Response {
	administratorAsByteArray, err := admin.retrieveAdministrator(stub, administratorParticipantID)
	if err != nil {
		return shim.Error(err.Error())
	}
	return shim.Success(administratorAsByteArray)
}

//Query Route: readAllAdministrators
func (admin *AdministratorParticipant) readAllAdministrators(stub shim.ChaincodeStubInterface) peer.Response {
	var administratorIDs AdministratorIDIndex
	bytes, err := stub.GetState("administratorIDIndex")
	if err != nil {
		return shim.Error("readAllAdministrators: Error getting administratorIDIndex array")
	}
	err = json.Unmarshal(bytes, &administratorIDs)
	if err != nil {
		return shim.Error("readAllAdministrators: Error unmarshalling administratorIDIndex array JSON")
	}
	result := "["

	var administratorAsByteArray []byte

	for _, administratorID := range administratorIDs.IDs {
		administratorAsByteArray, err = admin.retrieveAdministrator(stub, administratorID)
		if err != nil {
			return shim.Error("Failed to retrieve administrator with ID: " + administratorID)
		}
		result += string(administratorAsByteArray) + ","
	}
	if len(result) == 1 {
		result = "[]"
	} else {
		result = result[:len(result)-1] + "]"
	}
	return shim.Success([]byte(result))
}

//Helper: Save administratorParticipant
func (admin *AdministratorParticipant) saveAdministrator(stub shim.ChaincodeStubInterface, administrator Administrator) (bool, error) {
	bytes, err := json.Marshal(administrator)
	if err != nil {
		return false, errors.New("Error converting administrator record JSON")
	}
	err = stub.PutState(administrator.ID, bytes)
	if err != nil {
		return false, errors.New("Error storing Administrator record")
	}
	return true, nil
}

//Helper: Delete administratorStructParticipant
func (admin *AdministratorParticipant) deleteAdministrator(stub shim.ChaincodeStubInterface, administratorStructParticipantID string) (bool, error) {
	_, err := admin.retrieveAdministrator(stub, administratorStructParticipantID)
	if err != nil {
		return false, errors.New("Administrator with ID: " + administratorStructParticipantID + " not found")
	}
	err = stub.DelState(administratorStructParticipantID)
	if err != nil {
		return false, errors.New("Error deleting Administrator record")
	}
	return true, nil
}

//Helper: Update administrator Holder - updates Index
func (admin *AdministratorParticipant) updateAdministratorIDIndex(stub shim.ChaincodeStubInterface, administrator Administrator) (bool, error) {
	var administratorIDs AdministratorIDIndex
	bytes, err := stub.GetState("administratorIDIndex")
	if err != nil {
		return false, errors.New("upadeteAdministratorIDIndex: Error getting administratorIDIndex array Index from state")
	}
	err = json.Unmarshal(bytes, &administratorIDs)
	if err != nil {
		return false, errors.New("upadeteAdministratorIDIndex: Error unmarshalling administratorIDIndex array JSON")
	}
	administratorIDs.IDs = append(administratorIDs.IDs, administrator.ID)
	bytes, err = json.Marshal(administratorIDs)
	if err != nil {
		return false, errors.New("updateAdministratorIDIndex: Error marshalling new administrator ID")
	}
	err = stub.PutState("administratorIDIndex", bytes)
	if err != nil {
		return false, errors.New("updateAdministratorIDIndex: Error storing new administrator ID in administratorIDIndex (Index)")
	}
	return true, nil
}

//Helper: delete ID from administratorStruct Holder
func (admin *AdministratorParticipant) deleteAdministratorIDIndex(stub shim.ChaincodeStubInterface, administratorStructParticipantID string) (bool, error) {
	var administratorStructIDs AdministratorIDIndex
	bytes, err := stub.GetState("administratorIDIndex")
	if err != nil {
		return false, errors.New("deleteAdministratorIDIndex: Error getting administratorIDIndex array Index from state")
	}
	err = json.Unmarshal(bytes, &administratorStructIDs)
	if err != nil {
		return false, errors.New("deleteAdministratorIDIndex: Error unmarshalling administratorIDIndex array JSON")
	}
	administratorStructIDs.IDs, err = deleteKeyFromStringArray(administratorStructIDs.IDs, administratorStructParticipantID)
	if err != nil {
		return false, errors.New(err.Error())
	}
	bytes, err = json.Marshal(administratorStructIDs)
	if err != nil {
		return false, errors.New("deleteAdministratorIDIndex: Error marshalling new administratorStruct ID")
	}
	err = stub.PutState("administratorIDIndex", bytes)
	if err != nil {
		return false, errors.New("deleteAdministratorIDIndex: Error storing new administratorStruct ID in administratorIDIndex (Index)")
	}
	return true, nil
}

//Helper: Initialize administrator ID Holder
func (admin *AdministratorParticipant) initHolder(stub shim.ChaincodeStubInterface) bool {
	var administratorIDIndex AdministratorIDIndex
	bytes, _ := json.Marshal(administratorIDIndex)
	stub.DelState("administratorIDIndex")
	stub.PutState("administratorIDIndex", bytes)
	return true
}

//Helper: Retrieve administratorParticipant
func (admin *AdministratorParticipant) retrieveAdministrator(stub shim.ChaincodeStubInterface, administratorParticipantID string) ([]byte, error) {
	var administrator Administrator
	var administratorAsByteArray []byte
	bytes, err := stub.GetState(administratorParticipantID)
	if err != nil {
		return administratorAsByteArray, errors.New("retrieveAdministrator: Error retrieving administrator with ID: " + administratorParticipantID)
	}
	err = json.Unmarshal(bytes, &administrator)
	if err != nil {
		return administratorAsByteArray, errors.New("retrieveAdministrator: Corrupt administrator record " + string(bytes))
	}
	administratorAsByteArray, err = json.Marshal(administrator)
	if err != nil {
		return administratorAsByteArray, errors.New("readAdministrator: Invalid administrator Object - Not a  valid JSON")
	}
	return administratorAsByteArray, nil
}

//deleteKeyFromArray
func deleteKeyFromStringArray(array []string, key string) (newArray []string, err error) {
	for _, entry := range array {
		if entry != key {
			newArray = append(newArray, entry)
		}
	}
	if len(newArray) == len(array) {
		return newArray, errors.New("Specified Key: " + key + " not found in Array")
	}
	return newArray, nil
}

//getAdministratorFromArgs - construct a administrator structure from string array of arguments
func getAdministratorFromArgs(args []string) (administrator Administrator, err error) {

	if !Valid(args[0]) {
		return administrator, errors.New("Invalid json")
	}

	err = Unmarshal([]byte(args[0]), &administrator)
	if err != nil {
		return administrator, err
	}
	return administrator, nil
}
