package main

//Template Version: 1.1-20180704
import (
	"bytes"
	"encoding/json"
	"fmt"
	"strings"
	"testing"

	"github.com/hyperledger/fabric/core/chaincode/shim"
)

//TestCertificationAuthorityParticipant_Init
func TestCertificationAuthorityParticipant_Init(t *testing.T) {
	certificationAuthority := new(CertificationAuthorityParticipant)
	stub := shim.NewMockStub("certificationAuthority", certificationAuthority)
	checkInit(t, stub, [][]byte{[]byte("init")})
	checkState(t, stub, "certificationAuthorityIDIndex", getExpectedCertificationAuthorityIDIndex("init"))
}

//TestCertificationAuthorityParticipant_InvokeUnknownFunction
func TestCertificationAuthorityParticipant_InvokeUnknownFunction(t *testing.T) {
	certificationAuthority := new(CertificationAuthorityParticipant)
	stub := shim.NewMockStub("certificationAuthority", certificationAuthority)
	checkInit(t, stub, [][]byte{[]byte("init")})
	checkInvokeUnknownFunction(t, stub, [][]byte{[]byte("myFunction"), []byte("docType:Participant")})
}

//TestCertificationAuthorityParticipant_Invoke_addNewCertificationAuthority
func TestCertificationAuthorityParticipant_Invoke_addNewCertificationAuthorityOK(t *testing.T) {
	certificationAuthority := new(CertificationAuthorityParticipant)
	stub := shim.NewMockStub("certificationAuthority", certificationAuthority)
	checkInit(t, stub, [][]byte{[]byte("init")})
	checkInvoke(t, stub, getFirstCertificationAuthorityParticipantForTesting())
	newCertificationAuthorityID := "100001"
	checkState(t, stub, newCertificationAuthorityID, getNewCertificationAuthorityExpected())
	checkState(t, stub, "certificationAuthorityIDIndex", getExpectedCertificationAuthorityIDIndex("addNewCertificationAuthority"))
}

//TestCertificationAuthorityParticipant_Invoke_addNewCertificationAuthority
func TestCertificationAuthorityParticipant_Invoke_addNewCertificationAuthorityDuplicate(t *testing.T) {
	certificationAuthority := new(CertificationAuthorityParticipant)
	stub := shim.NewMockStub("certificationAuthority", certificationAuthority)
	checkInit(t, stub, [][]byte{[]byte("init")})
	checkInvoke(t, stub, getFirstCertificationAuthorityParticipantForTesting())
	newCertificationAuthorityID := "100001"
	checkState(t, stub, newCertificationAuthorityID, getNewCertificationAuthorityExpected())
	checkState(t, stub, "certificationAuthorityIDIndex", getExpectedCertificationAuthorityIDIndex("addNewCertificationAuthority"))
	res := stub.MockInvoke("1", getFirstCertificationAuthorityParticipantForTesting())
	if res.Status != shim.OK {
		checkError(t, "This CertificationAuthority already exists: 100001", res.Message)
	} else {
		fmt.Println("Error was expected, but not raised")
		t.FailNow()
	}
}

//TestCertificationAuthorityParticipant_Invoke_removeCertificationAuthorityOK  //change template
func TestCertificationAuthorityParticipant_Invoke_removeCertificationAuthorityOK(t *testing.T) {
	certificationAuthority := new(CertificationAuthorityParticipant)
	stub := shim.NewMockStub("certificationAuthority", certificationAuthority)
	checkInit(t, stub, [][]byte{[]byte("init")})
	checkInvoke(t, stub, getFirstCertificationAuthorityParticipantForTesting())
	checkInvoke(t, stub, getSecondCertificationAuthorityParticipantForTesting())
	checkReadAllCertificationAuthoritysOK(t, stub)
	checkState(t, stub, "certificationAuthorityIDIndex", getExpectedCertificationAuthorityIDIndex("beforeRemoveCertificationAuthority"))
	checkInvoke(t, stub, getRemoveSecondCertificationAuthorityParticipantForTesting())
	remainingCertificationAuthorityID := "100001"
	checkReadCertificationAuthorityOK(t, stub, remainingCertificationAuthorityID)
	checkState(t, stub, "certificationAuthorityIDIndex", getExpectedCertificationAuthorityIDIndex("afterRemoveCertificationAuthority"))
}

//TestCertificationAuthorityParticipant_Invoke_removeCertificationAuthorityNOK  //change template
func TestCertificationAuthorityParticipant_Invoke_removeCertificationAuthorityNOK(t *testing.T) {
	certificationAuthority := new(CertificationAuthorityParticipant)
	stub := shim.NewMockStub("certificationAuthority", certificationAuthority)
	checkInit(t, stub, [][]byte{[]byte("init")})
	checkInvoke(t, stub, getFirstCertificationAuthorityParticipantForTesting())
	firstCertificationAuthorityID := "100001"
	checkReadCertificationAuthorityOK(t, stub, firstCertificationAuthorityID)
	checkState(t, stub, "certificationAuthorityIDIndex", getExpectedCertificationAuthorityIDIndex("addNewCertificationAuthority"))
	res := stub.MockInvoke("1", getRemoveSecondCertificationAuthorityParticipantForTesting())
	if res.Status != shim.OK {
		checkError(t, "CertificationAuthority with ID: "+"100002"+" not found", res.Message)
	} else {
		fmt.Println("Error was expected, but not raised")
		t.FailNow()
	}
	checkState(t, stub, "certificationAuthorityIDIndex", getExpectedCertificationAuthorityIDIndex("addNewCertificationAuthority"))
}

//TestCertificationAuthorityParticipant_Invoke_removeAllCertificationAuthoritysOK  //change template
func TestCertificationAuthorityParticipant_Invoke_removeAllCertificationAuthoritysOK(t *testing.T) {
	certificationAuthority := new(CertificationAuthorityParticipant)
	stub := shim.NewMockStub("certificationAuthority", certificationAuthority)
	checkInit(t, stub, [][]byte{[]byte("init")})
	checkInvoke(t, stub, getFirstCertificationAuthorityParticipantForTesting())
	checkInvoke(t, stub, getSecondCertificationAuthorityParticipantForTesting())
	checkReadAllCertificationAuthoritysOK(t, stub)
	checkState(t, stub, "certificationAuthorityIDIndex", getExpectedCertificationAuthorityIDIndex("beforeRemoveCertificationAuthority"))
	checkInvoke(t, stub, getRemoveAllCertificationAuthorityParticipantsForTesting())
	checkState(t, stub, "certificationAuthorityIDIndex", getExpectedCertificationAuthorityIDIndex(""))
}

//TestCertificationAuthorityParticipant_Invoke_removeCertificationAuthorityNOK  //change template
func TestCertificationAuthorityParticipant_Invoke_removeAllCertificationAuthoritysNOK(t *testing.T) {
	certificationAuthority := new(CertificationAuthorityParticipant)
	stub := shim.NewMockStub("certificationAuthority", certificationAuthority)
	checkInit(t, stub, [][]byte{[]byte("init")})
	res := stub.MockInvoke("1", getRemoveAllCertificationAuthorityParticipantsForTesting())
	if res.Status != shim.OK {
		checkError(t, "removeAllCertificationAuthoritys: No certificationAuthoritys to remove", res.Message)
	} else {
		fmt.Println("Error was expected, but not raised")
		t.FailNow()
	}
	checkState(t, stub, "certificationAuthorityIDIndex", getExpectedCertificationAuthorityIDIndex(""))
}

//TestCertificationAuthorityParticipant_Query_readCertificationAuthority
func TestCertificationAuthorityParticipant_Query_readCertificationAuthority(t *testing.T) {
	certificationAuthority := new(CertificationAuthorityParticipant)
	stub := shim.NewMockStub("certificationAuthority", certificationAuthority)
	checkInit(t, stub, [][]byte{[]byte("init")})
	certificationAuthorityID := "100001"
	checkInvoke(t, stub, getFirstCertificationAuthorityParticipantForTesting())
	checkReadCertificationAuthorityOK(t, stub, certificationAuthorityID)
	checkReadCertificationAuthorityNOK(t, stub, "")
}

//TestCertificationAuthorityParticipant_Query_readAllCertificationAuthoritys
func TestCertificationAuthorityParticipant_Query_readAllCertificationAuthoritys(t *testing.T) {
	certificationAuthority := new(CertificationAuthorityParticipant)
	stub := shim.NewMockStub("certificationAuthority", certificationAuthority)
	checkInit(t, stub, [][]byte{[]byte("init")})
	checkInvoke(t, stub, getFirstCertificationAuthorityParticipantForTesting())
	checkInvoke(t, stub, getSecondCertificationAuthorityParticipantForTesting())
	checkReadAllCertificationAuthoritysOK(t, stub)
}

/*
*
*	Helper Functions
*
 */
//Get first CertificationAuthorityParticipant for testing
func getFirstCertificationAuthorityParticipantForTesting() [][]byte { 
 return [][]byte{[]byte("addNewCertificationAuthority"), 
 []byte("{\"ID\":\"100001\",\"docType\":\"Participant.CertificationAuthorityParticipant\",\"alias\":\"BRITECH\",\"description\":\"British Technology Pvt. Ltd.\"}")}
}

//Get second CertificationAuthorityParticipant for testing
func getSecondCertificationAuthorityParticipantForTesting() [][]byte { 
 return [][]byte{[]byte("addNewCertificationAuthority"), 
 []byte("{\"ID\":\"100002\",\"docType\":\"Participant.CertificationAuthorityParticipant\",\"alias\":\"DEMAGDELAG\",\"description\":\"Demag Delewal AG\"}")}
}

//Get remove second CertificationAuthorityParticipant for testing //change template
func getRemoveSecondCertificationAuthorityParticipantForTesting() [][]byte {
	return [][]byte{[]byte("removeCertificationAuthority"),
		[]byte("100002")}
}

//Get remove all CertificationAuthorityParticipants for testing //change template
func getRemoveAllCertificationAuthorityParticipantsForTesting() [][]byte {
	return [][]byte{[]byte("removeAllCertificationAuthoritys")}
}

//Get an expected value for testing
func getNewCertificationAuthorityExpected() []byte {
	var certificationAuthority CertificationAuthority
		certificationAuthority.ID = "100001"
	certificationAuthority.ObjectType = "Participant.CertificationAuthorityParticipant"
	certificationAuthority.Alias = "BRITECH"
	certificationAuthority.Description = "British Technology Pvt. Ltd."
	certificationAuthorityJSON, err := json.Marshal(certificationAuthority)
	if err != nil {
		fmt.Println("Error converting a CertificationAuthority record to JSON")
		return nil
	}
	return []byte(certificationAuthorityJSON)
}

//Get expected values of CertificationAuthoritys for testing
func getExpectedCertificationAuthoritys() []byte {
	var certificationAuthoritys []CertificationAuthority
	var certificationAuthority CertificationAuthority
		certificationAuthority.ID = "100001"
	certificationAuthority.ObjectType = "Participant.CertificationAuthorityParticipant"
	certificationAuthority.Alias = "BRITECH"
	certificationAuthority.Description = "British Technology Pvt. Ltd."
	certificationAuthoritys = append(certificationAuthoritys, certificationAuthority)
		certificationAuthority.ID = "100002"
	certificationAuthority.ObjectType = "Participant.CertificationAuthorityParticipant"
	certificationAuthority.Alias = "DEMAGDELAG"
	certificationAuthority.Description = "Demag Delewal AG"
	certificationAuthority.Description = "Demag Delewal AG"
	certificationAuthoritys = append(certificationAuthoritys, certificationAuthority)
	certificationAuthorityJSON, err := json.Marshal(certificationAuthoritys)
	if err != nil {
		fmt.Println("Error converting certificationAuthorityancer records to JSON")
		return nil
	}
	return []byte(certificationAuthorityJSON)
}

func getExpectedCertificationAuthorityIDIndex(funcName string) []byte {
	var certificationAuthorityIDIndex CertificationAuthorityIDIndex
	switch funcName {
	case "addNewCertificationAuthority":
		certificationAuthorityIDIndex.IDs = append(certificationAuthorityIDIndex.IDs, "100001")
		certificationAuthorityIDIndexBytes, err := json.Marshal(certificationAuthorityIDIndex)
		if err != nil {
			fmt.Println("Error converting CertificationAuthorityIDIndex to JSON")
			return nil
		}
		return certificationAuthorityIDIndexBytes
	case "beforeRemoveCertificationAuthority":
		certificationAuthorityIDIndex.IDs = append(certificationAuthorityIDIndex.IDs, "100001")
		certificationAuthorityIDIndex.IDs = append(certificationAuthorityIDIndex.IDs, "100002")
		certificationAuthorityIDIndexBytes, err := json.Marshal(certificationAuthorityIDIndex)
		if err != nil {
			fmt.Println("Error converting CertificationAuthorityIDIndex to JSON")
			return nil
		}
		return certificationAuthorityIDIndexBytes
	case "afterRemoveCertificationAuthority":
		certificationAuthorityIDIndex.IDs = append(certificationAuthorityIDIndex.IDs, "100001")
		certificationAuthorityIDIndexBytes, err := json.Marshal(certificationAuthorityIDIndex)
		if err != nil {
			fmt.Println("Error converting CertificationAuthorityIDIndex to JSON")
			return nil
		}
		return certificationAuthorityIDIndexBytes
	default:
		certificationAuthorityIDIndexBytes, err := json.Marshal(certificationAuthorityIDIndex)
		if err != nil {
			fmt.Println("Error converting CertificationAuthorityIDIndex to JSON")
			return nil
		}
		return certificationAuthorityIDIndexBytes
	}
}

//checkInit - helper to check the Initialization of chaincode: CertificationAuthorityParticipant
func checkInit(t *testing.T, stub *shim.MockStub, args [][]byte) {
	res := stub.MockInit("1", args)
	if res.Status != shim.OK {
		fmt.Println("Init failed", string(res.Message))
		t.FailNow()
	}
}

//checkState - helper for checking the chaincode state for a given stateKey afgainst an expected value
func checkState(t *testing.T, stub *shim.MockStub, stateKey string, expectedState []byte) {
	actualState := stub.State[stateKey]
	if actualState == nil {
		fmt.Println("State for ", stateKey, ": failed to get value")
		t.FailNow()
	}

	if bytes.Compare(actualState, expectedState) != 0 {
		fmt.Println("Incorrect State for " + stateKey + ": \nExpected: " + string(expectedState) + "\nActual  : " + string(actualState))
		t.FailNow()
	}
}

//checkInvoke - helper for checking Invoke of chaincode
func checkInvoke(t *testing.T, stub *shim.MockStub, args [][]byte) {
	res := stub.MockInvoke("1", args)
	if res.Status != shim.OK {
		fmt.Println("Invoke", args, "failed", string(res.Message))
		t.FailNow()
	}
}

func checkInvokeUnknownFunction(t *testing.T, stub *shim.MockStub, args [][]byte) {
	res := stub.MockInvoke("1", args)
	if res.Status != shim.OK {
		expectedErr := "Received unknown function invocation"
		actualErr := string(res.Message)
		if !(strings.Contains(actualErr, expectedErr)) {
			fmt.Println("Invoke with unknown function must throw error: ", expectedErr, "\n Actual Error :", actualErr)
			t.FailNow()
		}
	} else {
		t.FailNow()
	}
}

//checkReadCertificationAuthorityOK - helper for positive test readCertificationAuthority
func checkReadCertificationAuthorityOK(t *testing.T, stub *shim.MockStub, certificationAuthorityancerID string) {
	res := stub.MockInvoke("1", [][]byte{[]byte("readCertificationAuthority"), []byte(certificationAuthorityancerID)})
	if res.Status != shim.OK {
		fmt.Println("func readCertificationAuthority with ID: ", certificationAuthorityancerID, " failed"+string(res.Message))
		t.FailNow()
	}
	if res.Payload == nil {
		fmt.Println("func readCertificationAuthority with ID: ", certificationAuthorityancerID, "failed to get value")
		t.FailNow()
	}
	if bytes.Compare(getNewCertificationAuthorityExpected(), []byte(res.Payload)) != 0 {
		fmt.Println("func readCertificationAuthority with ID: ", certificationAuthorityancerID, "Expected:", string(getNewCertificationAuthorityExpected()), "Actual:", string(res.Payload))
		t.FailNow()
	}
}

//checkReadCertificationAuthorityNOK - helper for negative testing of readCertificationAuthority
func checkReadCertificationAuthorityNOK(t *testing.T, stub *shim.MockStub, certificationAuthorityancerID string) {
	//with no certificationAuthorityancerID
	res := stub.MockInvoke("1", [][]byte{[]byte("readCertificationAuthority"), []byte("")})
	if res.Status != shim.OK {
		expectedErr := "retrieveCertificationAuthority: Corrupt certificationAuthority record "
		actualErr := string(res.Message)
		if !(strings.Contains(actualErr, expectedErr)) {
			fmt.Println("func readCertificationAuthority neagtive test: ", "Expected Error:", expectedErr, "Actual Error", actualErr)
			t.FailNow()
		}
	} else {
		t.FailNow()
	}
}
func checkReadAllCertificationAuthoritysOK(t *testing.T, stub *shim.MockStub) {
	res := stub.MockInvoke("1", [][]byte{[]byte("readAllCertificationAuthoritys")})
	if res.Status != shim.OK {
		fmt.Println("func readAllCertificationAuthoritys failed", string(res.Message))
		t.FailNow()
	}
	if res.Payload == nil {
		fmt.Println("func readAllCertificationAuthoritys failed to get value")
		t.FailNow()
	}
	if bytes.Compare(getExpectedCertificationAuthoritys(), []byte(res.Payload)) != 0 {
		fmt.Println("func readAllCertificationAuthoritys Expected:\n", string(getExpectedCertificationAuthoritys()), "\nActual:\n", string(res.Payload))
		t.FailNow()
	}
}

func checkError(t *testing.T, exp string, act string) {
	if strings.Compare(exp, act) != 0 {
		fmt.Println("Unexpected Error! Expecting ", exp, "\n Actual :", act)
		t.FailNow()
	}
}
