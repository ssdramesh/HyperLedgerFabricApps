package main

//Template Version: 1.1-20180704
import (
	"encoding/json"
	"errors"
	"fmt"

	"github.com/hyperledger/fabric/core/chaincode/shim"
	"github.com/hyperledger/fabric/protos/peer"
)

var logger = shim.NewLogger("CLDChaincode")

//CertificationAuthorityParticipant - Chaincode for CertificationAuthority Participant
type CertificationAuthorityParticipant struct {
}

//CertificationAuthority - Details of the participant type CertificationAuthority
type CertificationAuthority struct {
 ID          string `json:"ID"`
 ObjectType   string `json:"docType"`
  Alias       string `json:"alias"`
  Description string `json:"description"`
}

//CertificationAuthorityIDIndex - Index on IDs for retrieval all CertificationAuthoritys
type CertificationAuthorityIDIndex struct {
	IDs []string `json:"IDs"`
}

func main() {
	err := shim.Start(new(CertificationAuthorityParticipant))
	if err != nil {
		fmt.Printf("Error starting CertificationAuthorityParticipant chaincode function main(): %s", err)
	} else {
		fmt.Printf("Starting CertificationAuthorityParticipant chaincode function main() executed successfully")
	}
}

//Init - The chaincode Init function: No  arguments, only initializes a ID array as Index for retrieval of all CertificationAuthoritys
func (cta *CertificationAuthorityParticipant) Init(stub shim.ChaincodeStubInterface) peer.Response {
	var certificationAuthorityIDIndex CertificationAuthorityIDIndex
	record, _ := stub.GetState("certificationAuthorityIDIndex")
	//chaincode upgrade: Index might already exist, create new only if it does not
	if record == nil {
		bytes, _ := json.Marshal(certificationAuthorityIDIndex)
		stub.PutState("certificationAuthorityIDIndex", bytes)
	}
	return shim.Success(nil)
}

//Invoke - The chaincode Invoke function:
func (cta *CertificationAuthorityParticipant) Invoke(stub shim.ChaincodeStubInterface) peer.Response {
	function, args := stub.GetFunctionAndParameters()
	logger.Debug("function: ", function)
	switch function {
	case "addNewCertificationAuthority":
		return cta.addNewCertificationAuthority(stub, args)
	case "removeCertificationAuthority":
		return cta.removeCertificationAuthority(stub, args[0])
	case "removeAllCertificationAuthoritys":
		return cta.removeAllCertificationAuthoritys(stub)
	case "readCertificationAuthority":
		return cta.readCertificationAuthority(stub, args[0])
	case "readAllCertificationAuthoritys":
		return cta.readAllCertificationAuthoritys(stub)
	default:
		return shim.Error("Received unknown function invocation")
	}
}

//Invoke Route: addNewCertificationAuthority
func (cta *CertificationAuthorityParticipant) addNewCertificationAuthority(stub shim.ChaincodeStubInterface, args []string) peer.Response {
	certificationAuthority, err := getCertificationAuthorityFromArgs(args)
	if err != nil {
		return shim.Error("CertificationAuthority Data is Corrupted")
	}
	certificationAuthority.ObjectType = "Participant.CertificationAuthorityParticipant"
	record, err := stub.GetState(certificationAuthority.ID)
	if record != nil {
		return shim.Error("This CertificationAuthority already exists: " + certificationAuthority.ID)
	}
	_, err = cta.saveCertificationAuthority(stub, certificationAuthority)
	if err != nil {
		return shim.Error(err.Error())
	}
	_, err = cta.updateCertificationAuthorityIDIndex(stub, certificationAuthority)
	if err != nil {
		return shim.Error(err.Error())
	}
	return shim.Success(nil)
}

//Invoke Route: removeCertificationAuthority
func (cta *CertificationAuthorityParticipant) removeCertificationAuthority(stub shim.ChaincodeStubInterface, certificationAuthorityStructParticipantID string) peer.Response {
	_, err := cta.deleteCertificationAuthority(stub, certificationAuthorityStructParticipantID)
	if err != nil {
		return shim.Error(err.Error())
	}
	_, err = cta.deleteCertificationAuthorityIDIndex(stub, certificationAuthorityStructParticipantID)
	if err != nil {
		return shim.Error(err.Error())
	}
	return shim.Success(nil)
}

//Invoke Route: removeAllCertificationAuthoritys
func (cta *CertificationAuthorityParticipant) removeAllCertificationAuthoritys(stub shim.ChaincodeStubInterface) peer.Response {
	var certificationAuthorityStructIDs CertificationAuthorityIDIndex
	bytes, err := stub.GetState("certificationAuthorityIDIndex")
	if err != nil {
		return shim.Error("removeAllCertificationAuthoritys: Error getting certificationAuthorityIDIndex array")
	}
	err = json.Unmarshal(bytes, &certificationAuthorityStructIDs)
	if err != nil {
		return shim.Error("removeAllCertificationAuthoritys: Error unmarshalling certificationAuthorityIDIndex array JSON")
	}
	if len(certificationAuthorityStructIDs.IDs) == 0 {
		return shim.Error("removeAllCertificationAuthoritys: No certificationAuthoritys to remove")
	}
	for _, certificationAuthorityStructParticipantID := range certificationAuthorityStructIDs.IDs {
		_, err = cta.deleteCertificationAuthority(stub, certificationAuthorityStructParticipantID)
		if err != nil {
			return shim.Error("Failed to remove CertificationAuthority with ID: " + certificationAuthorityStructParticipantID)
		}
		_, err = cta.deleteCertificationAuthorityIDIndex(stub, certificationAuthorityStructParticipantID)
		if err != nil {
			return shim.Error(err.Error())
		}
	}
	cta.initHolder(stub)
	return shim.Success(nil)
}

//Query Route: readCertificationAuthority
func (cta *CertificationAuthorityParticipant) readCertificationAuthority(stub shim.ChaincodeStubInterface, certificationAuthorityParticipantID string) peer.Response {
	certificationAuthorityAsByteArray, err := cta.retrieveCertificationAuthority(stub, certificationAuthorityParticipantID)
	if err != nil {
		return shim.Error(err.Error())
	}
	return shim.Success(certificationAuthorityAsByteArray)
}

//Query Route: readAllCertificationAuthoritys
func (cta *CertificationAuthorityParticipant) readAllCertificationAuthoritys(stub shim.ChaincodeStubInterface) peer.Response {
	var certificationAuthorityIDs CertificationAuthorityIDIndex
	bytes, err := stub.GetState("certificationAuthorityIDIndex")
	if err != nil {
		return shim.Error("readAllCertificationAuthoritys: Error getting certificationAuthorityIDIndex array")
	}
	err = json.Unmarshal(bytes, &certificationAuthorityIDs)
	if err != nil {
		return shim.Error("readAllCertificationAuthoritys: Error unmarshalling certificationAuthorityIDIndex array JSON")
	}
	result := "["

	var certificationAuthorityAsByteArray []byte

	for _, certificationAuthorityID := range certificationAuthorityIDs.IDs {
		certificationAuthorityAsByteArray, err = cta.retrieveCertificationAuthority(stub, certificationAuthorityID)
		if err != nil {
			return shim.Error("Failed to retrieve certificationAuthority with ID: " + certificationAuthorityID)
		}
		result += string(certificationAuthorityAsByteArray) + ","
	}
	if len(result) == 1 {
		result = "[]"
	} else {
		result = result[:len(result)-1] + "]"
	}
	return shim.Success([]byte(result))
}

//Helper: Save certificationAuthorityParticipant
func (cta *CertificationAuthorityParticipant) saveCertificationAuthority(stub shim.ChaincodeStubInterface, certificationAuthority CertificationAuthority) (bool, error) {
	bytes, err := json.Marshal(certificationAuthority)
	if err != nil {
		return false, errors.New("Error converting certificationAuthority record JSON")
	}
	err = stub.PutState(certificationAuthority.ID, bytes)
	if err != nil {
		return false, errors.New("Error storing CertificationAuthority record")
	}
	return true, nil
}

//Helper: Delete certificationAuthorityStructParticipant
func (cta *CertificationAuthorityParticipant) deleteCertificationAuthority(stub shim.ChaincodeStubInterface, certificationAuthorityStructParticipantID string) (bool, error) {
	_, err := cta.retrieveCertificationAuthority(stub, certificationAuthorityStructParticipantID)
	if err != nil {
		return false, errors.New("CertificationAuthority with ID: " + certificationAuthorityStructParticipantID + " not found")
	}
	err = stub.DelState(certificationAuthorityStructParticipantID)
	if err != nil {
		return false, errors.New("Error deleting CertificationAuthority record")
	}
	return true, nil
}

//Helper: Update certificationAuthority Holder - updates Index
func (cta *CertificationAuthorityParticipant) updateCertificationAuthorityIDIndex(stub shim.ChaincodeStubInterface, certificationAuthority CertificationAuthority) (bool, error) {
	var certificationAuthorityIDs CertificationAuthorityIDIndex
	bytes, err := stub.GetState("certificationAuthorityIDIndex")
	if err != nil {
		return false, errors.New("upadeteCertificationAuthorityIDIndex: Error getting certificationAuthorityIDIndex array Index from state")
	}
	err = json.Unmarshal(bytes, &certificationAuthorityIDs)
	if err != nil {
		return false, errors.New("upadeteCertificationAuthorityIDIndex: Error unmarshalling certificationAuthorityIDIndex array JSON")
	}
	certificationAuthorityIDs.IDs = append(certificationAuthorityIDs.IDs, certificationAuthority.ID)
	bytes, err = json.Marshal(certificationAuthorityIDs)
	if err != nil {
		return false, errors.New("updateCertificationAuthorityIDIndex: Error marshalling new certificationAuthority ID")
	}
	err = stub.PutState("certificationAuthorityIDIndex", bytes)
	if err != nil {
		return false, errors.New("updateCertificationAuthorityIDIndex: Error storing new certificationAuthority ID in certificationAuthorityIDIndex (Index)")
	}
	return true, nil
}

//Helper: delete ID from certificationAuthorityStruct Holder
func (cta *CertificationAuthorityParticipant) deleteCertificationAuthorityIDIndex(stub shim.ChaincodeStubInterface, certificationAuthorityStructParticipantID string) (bool, error) {
	var certificationAuthorityStructIDs CertificationAuthorityIDIndex
	bytes, err := stub.GetState("certificationAuthorityIDIndex")
	if err != nil {
		return false, errors.New("deleteCertificationAuthorityIDIndex: Error getting certificationAuthorityIDIndex array Index from state")
	}
	err = json.Unmarshal(bytes, &certificationAuthorityStructIDs)
	if err != nil {
		return false, errors.New("deleteCertificationAuthorityIDIndex: Error unmarshalling certificationAuthorityIDIndex array JSON")
	}
	certificationAuthorityStructIDs.IDs, err = deleteKeyFromStringArray(certificationAuthorityStructIDs.IDs, certificationAuthorityStructParticipantID)
	if err != nil {
		return false, errors.New(err.Error())
	}
	bytes, err = json.Marshal(certificationAuthorityStructIDs)
	if err != nil {
		return false, errors.New("deleteCertificationAuthorityIDIndex: Error marshalling new certificationAuthorityStruct ID")
	}
	err = stub.PutState("certificationAuthorityIDIndex", bytes)
	if err != nil {
		return false, errors.New("deleteCertificationAuthorityIDIndex: Error storing new certificationAuthorityStruct ID in certificationAuthorityIDIndex (Index)")
	}
	return true, nil
}

//Helper: Initialize certificationAuthority ID Holder
func (cta *CertificationAuthorityParticipant) initHolder(stub shim.ChaincodeStubInterface) bool {
	var certificationAuthorityIDIndex CertificationAuthorityIDIndex
	bytes, _ := json.Marshal(certificationAuthorityIDIndex)
	stub.DelState("certificationAuthorityIDIndex")
	stub.PutState("certificationAuthorityIDIndex", bytes)
	return true
}

//Helper: Retrieve certificationAuthorityParticipant
func (cta *CertificationAuthorityParticipant) retrieveCertificationAuthority(stub shim.ChaincodeStubInterface, certificationAuthorityParticipantID string) ([]byte, error) {
	var certificationAuthority CertificationAuthority
	var certificationAuthorityAsByteArray []byte
	bytes, err := stub.GetState(certificationAuthorityParticipantID)
	if err != nil {
		return certificationAuthorityAsByteArray, errors.New("retrieveCertificationAuthority: Error retrieving certificationAuthority with ID: " + certificationAuthorityParticipantID)
	}
	err = json.Unmarshal(bytes, &certificationAuthority)
	if err != nil {
		return certificationAuthorityAsByteArray, errors.New("retrieveCertificationAuthority: Corrupt certificationAuthority record " + string(bytes))
	}
	certificationAuthorityAsByteArray, err = json.Marshal(certificationAuthority)
	if err != nil {
		return certificationAuthorityAsByteArray, errors.New("readCertificationAuthority: Invalid certificationAuthority Object - Not a  valid JSON")
	}
	return certificationAuthorityAsByteArray, nil
}

//deleteKeyFromArray
func deleteKeyFromStringArray(array []string, key string) (newArray []string, err error) {
	for _, entry := range array {
		if entry != key {
			newArray = append(newArray, entry)
		}
	}
	if len(newArray) == len(array) {
		return newArray, errors.New("Specified Key: " + key + " not found in Array")
	}
	return newArray, nil
}

//getCertificationAuthorityFromArgs - construct a certificationAuthority structure from string array of arguments
func getCertificationAuthorityFromArgs(args []string) (certificationAuthority CertificationAuthority, err error) {

	if !Valid(args[0]) {
		return certificationAuthority, errors.New("Invalid json")
	}

	err = Unmarshal([]byte(args[0]), &certificationAuthority)
	if err != nil {
		return certificationAuthority, err
	}
	return certificationAuthority, nil
}
