package main

//Template Version: 1.1-20180704
import (
	"bytes"
	"encoding/json"
	"fmt"
	"strings"
	"testing"

	"github.com/hyperledger/fabric/core/chaincode/shim"
)

//TestCertificateAsset_Init
func TestCertificateAsset_Init(t *testing.T) {
	certificate := new(CertificateAsset)
	stub := shim.NewMockStub("certificate", certificate)
	checkInit(t, stub, [][]byte{[]byte("init")})
	checkState(t, stub, "certificateIDIndex", getExpectedCertificateIDIndex("init"))
}

//TestCertificateAsset_InvokeUnknownFunction
func TestCertificateAsset_InvokeUnknownFunction(t *testing.T) {
	certificate := new(CertificateAsset)
	stub := shim.NewMockStub("certificate", certificate)
	checkInit(t, stub, [][]byte{[]byte("init")})
	checkInvokeUnknownFunction(t, stub, [][]byte{[]byte("myFunction"), []byte("docType:Asset")})
}

//TestCertificateAsset_Invoke_addNewCertificate
func TestCertificateAsset_Invoke_addNewCertificateOK(t *testing.T) {
	certificate := new(CertificateAsset)
	stub := shim.NewMockStub("certificate", certificate)
	checkInit(t, stub, [][]byte{[]byte("init")})
	checkInvoke(t, stub, getFirstCertificateAssetForTesting())
	newCertificateID := "100001"
	checkState(t, stub, newCertificateID, getNewCertificateExpected())
	checkState(t, stub, "certificateIDIndex", getExpectedCertificateIDIndex("addNewCertificate"))
}

//TestCertificateAsset_Invoke_addNewCertificate
func TestCertificateAsset_Invoke_addNewCertificateDuplicate(t *testing.T) {
	certificate := new(CertificateAsset)
	stub := shim.NewMockStub("certificate", certificate)
	checkInit(t, stub, [][]byte{[]byte("init")})
	checkInvoke(t, stub, getFirstCertificateAssetForTesting())
	newCertificateID := "100001"
	checkState(t, stub, newCertificateID, getNewCertificateExpected())
	checkState(t, stub, "certificateIDIndex", getExpectedCertificateIDIndex("addNewCertificate"))
	res := stub.MockInvoke("1", getFirstCertificateAssetForTesting())
	if res.Status != shim.OK {
		checkError(t, "This Certificate already exists: 100001", res.Message)
	} else {
		fmt.Println("Error was expected, but not raised")
		t.FailNow()
	}
}

//TestCertificateAsset_Invoke_removeCertificateOK  //change template
func TestCertificateAsset_Invoke_removeCertificateOK(t *testing.T) {
	certificate := new(CertificateAsset)
	stub := shim.NewMockStub("certificate", certificate)
	checkInit(t, stub, [][]byte{[]byte("init")})
	checkInvoke(t, stub, getFirstCertificateAssetForTesting())
	checkInvoke(t, stub, getSecondCertificateAssetForTesting())
	checkReadAllCertificatesOK(t, stub)
	checkState(t, stub, "certificateIDIndex", getExpectedCertificateIDIndex("beforeRemoveCertificate"))
	checkInvoke(t, stub, getRemoveSecondCertificateAssetForTesting())
	remainingCertificateID := "100001"
	checkReadCertificateOK(t, stub, remainingCertificateID)
	checkState(t, stub, "certificateIDIndex", getExpectedCertificateIDIndex("afterRemoveCertificate"))
}

//TestCertificateAsset_Invoke_removeCertificateNOK  //change template
func TestCertificateAsset_Invoke_removeCertificateNOK(t *testing.T) {
	certificate := new(CertificateAsset)
	stub := shim.NewMockStub("certificate", certificate)
	checkInit(t, stub, [][]byte{[]byte("init")})
	checkInvoke(t, stub, getFirstCertificateAssetForTesting())
	firstCertificateID := "100001"
	checkReadCertificateOK(t, stub, firstCertificateID)
	checkState(t, stub, "certificateIDIndex", getExpectedCertificateIDIndex("addNewCertificate"))
	res := stub.MockInvoke("1", getRemoveSecondCertificateAssetForTesting())
	if res.Status != shim.OK {
		checkError(t, "Certificate with ID: "+"100002"+" not found", res.Message)
	} else {
		fmt.Println("Error was expected, but not raised")
		t.FailNow()
	}
	checkState(t, stub, "certificateIDIndex", getExpectedCertificateIDIndex("addNewCertificate"))
}

//TestCertificateAsset_Invoke_removeAllCertificatesOK  //change template
func TestCertificateAsset_Invoke_removeAllCertificatesOK(t *testing.T) {
	certificate := new(CertificateAsset)
	stub := shim.NewMockStub("certificate", certificate)
	checkInit(t, stub, [][]byte{[]byte("init")})
	checkInvoke(t, stub, getFirstCertificateAssetForTesting())
	checkInvoke(t, stub, getSecondCertificateAssetForTesting())
	checkReadAllCertificatesOK(t, stub)
	checkState(t, stub, "certificateIDIndex", getExpectedCertificateIDIndex("beforeRemoveCertificate"))
	checkInvoke(t, stub, getRemoveAllCertificateAssetsForTesting())
	checkState(t, stub, "certificateIDIndex", getExpectedCertificateIDIndex(""))
}

//TestCertificateAsset_Invoke_removeCertificateNOK  //change template
func TestCertificateAsset_Invoke_removeAllCertificatesNOK(t *testing.T) {
	certificate := new(CertificateAsset)
	stub := shim.NewMockStub("certificate", certificate)
	checkInit(t, stub, [][]byte{[]byte("init")})
	res := stub.MockInvoke("1", getRemoveAllCertificateAssetsForTesting())
	if res.Status != shim.OK {
		checkError(t, "removeAllCertificates: No certificates to remove", res.Message)
	} else {
		fmt.Println("Error was expected, but not raised")
		t.FailNow()
	}
	checkState(t, stub, "certificateIDIndex", getExpectedCertificateIDIndex(""))
}

//TestCertificateAsset_Query_readCertificate
func TestCertificateAsset_Query_readCertificate(t *testing.T) {
	certificate := new(CertificateAsset)
	stub := shim.NewMockStub("certificate", certificate)
	checkInit(t, stub, [][]byte{[]byte("init")})
	certificateID := "100001"
	checkInvoke(t, stub, getFirstCertificateAssetForTesting())
	checkReadCertificateOK(t, stub, certificateID)
	checkReadCertificateNOK(t, stub, "")
}

//TestCertificateAsset_Query_readAllCertificates
func TestCertificateAsset_Query_readAllCertificates(t *testing.T) {
	certificate := new(CertificateAsset)
	stub := shim.NewMockStub("certificate", certificate)
	checkInit(t, stub, [][]byte{[]byte("init")})
	checkInvoke(t, stub, getFirstCertificateAssetForTesting())
	checkInvoke(t, stub, getSecondCertificateAssetForTesting())
	checkReadAllCertificatesOK(t, stub)
}

/*
*
*	Helper Functions
*
 */
//Get first CertificateAsset for testing
func getFirstCertificateAssetForTesting() [][]byte { 
 return [][]byte{[]byte("addNewCertificate"), 
 []byte("{\"ID\":\"100001\",\"docType\":\"Asset.CertificateAsset\",\"status\":\"0\",\"creationDate\":\"12/01/2018\", \"certificationAuthorityID\":\"certificationAuthorityID001\", \"companyID\":\"companyID001\", \"issueDate\":\"issueDate001\", \"validityStartDate\":\"validityStartDate001\", \"validityEndDate\":\"validityEndDate001\"}")}
}

//Get second CertificateAsset for testing
func getSecondCertificateAssetForTesting() [][]byte { 
 return [][]byte{[]byte("addNewCertificate"), 
 []byte("{\"ID\":\"100002\",\"docType\":\"Asset.CertificateAsset\",\"status\":\"0\",\"creationDate\":\"12/01/2018\", \"certificationAuthorityID\":\"certificationAuthorityID002\", \"companyID\":\"companyID002\", \"issueDate\":\"issueDate002\", \"validityStartDate\":\"validityStartDate002\", \"validityEndDate\":\"validityEndDate002\"}")}
}

//Get remove second CertificateAsset for testing //change template
func getRemoveSecondCertificateAssetForTesting() [][]byte {
	return [][]byte{[]byte("removeCertificate"),
		[]byte("100002")}
}

//Get remove all CertificateAssets for testing //change template
func getRemoveAllCertificateAssetsForTesting() [][]byte {
	return [][]byte{[]byte("removeAllCertificates")}
}

//Get an expected value for testing
func getNewCertificateExpected() []byte {
	var certificate Certificate
		certificate.ID = "100001"
	certificate.ObjectType = "Asset.CertificateAsset"
  certificate.Status = "0"
	certificate.CreationDate = "12/01/2018"
certificate.CertificationAuthorityID="certificationAuthorityID001"
certificate.CompanyID="companyID001"
certificate.IssueDate="issueDate001"
certificate.ValidityStartDate="validityStartDate001"
certificate.ValidityEndDate="validityEndDate001"
	certificateJSON, err := json.Marshal(certificate)
	if err != nil {
		fmt.Println("Error converting a Certificate record to JSON")
		return nil
	}
	return []byte(certificateJSON)
}

//Get expected values of Certificates for testing
func getExpectedCertificates() []byte {
	var certificates []Certificate
	var certificate Certificate
		certificate.ID = "100001"
	certificate.ObjectType = "Asset.CertificateAsset"
  certificate.Status = "0"
	certificate.CreationDate = "12/01/2018"
certificate.CertificationAuthorityID="certificationAuthorityID001"
certificate.CompanyID="companyID001"
certificate.IssueDate="issueDate001"
certificate.ValidityStartDate="validityStartDate001"
certificate.ValidityEndDate="validityEndDate001"
	certificates = append(certificates, certificate)
		certificate.ID = "100002"
	certificate.ObjectType = "Asset.CertificateAsset"
  certificate.Status = "0"
	certificate.CreationDate = "12/01/2018"
certificate.CertificationAuthorityID="certificationAuthorityID002"
certificate.CompanyID="companyID002"
certificate.IssueDate="issueDate002"
certificate.ValidityStartDate="validityStartDate002"
certificate.ValidityEndDate="validityEndDate002"
	certificates = append(certificates, certificate)
	certificateJSON, err := json.Marshal(certificates)
	if err != nil {
		fmt.Println("Error converting certificate records to JSON")
		return nil
	}
	return []byte(certificateJSON)
}

func getExpectedCertificateIDIndex(funcName string) []byte {
	var certificateIDIndex CertificateIDIndex
	switch funcName {
	case "addNewCertificate":
		certificateIDIndex.IDs = append(certificateIDIndex.IDs, "100001")
		certificateIDIndexBytes, err := json.Marshal(certificateIDIndex)
		if err != nil {
			fmt.Println("Error converting CertificateIDIndex to JSON")
			return nil
		}
		return certificateIDIndexBytes
	case "beforeRemoveCertificate":
		certificateIDIndex.IDs = append(certificateIDIndex.IDs, "100001")
		certificateIDIndex.IDs = append(certificateIDIndex.IDs, "100002")
		certificateIDIndexBytes, err := json.Marshal(certificateIDIndex)
		if err != nil {
			fmt.Println("Error converting CertificateIDIndex to JSON")
			return nil
		}
		return certificateIDIndexBytes
	case "afterRemoveCertificate":
		certificateIDIndex.IDs = append(certificateIDIndex.IDs, "100001")
		certificateIDIndexBytes, err := json.Marshal(certificateIDIndex)
		if err != nil {
			fmt.Println("Error converting CertificateIDIndex to JSON")
			return nil
		}
		return certificateIDIndexBytes
	default:
		certificateIDIndexBytes, err := json.Marshal(certificateIDIndex)
		if err != nil {
			fmt.Println("Error converting CertificateIDIndex to JSON")
			return nil
		}
		return certificateIDIndexBytes
	}
}

//checkInit - helper to check the Initialization of chaincode: CertificateAsset
func checkInit(t *testing.T, stub *shim.MockStub, args [][]byte) {
	res := stub.MockInit("1", args)
	if res.Status != shim.OK {
		fmt.Println("Init failed", string(res.Message))
		t.FailNow()
	}
}

//checkState - helper for checking the chaincode state for a given stateKey afgainst an expected value
func checkState(t *testing.T, stub *shim.MockStub, stateKey string, expectedState []byte) {
	actualState := stub.State[stateKey]
	if actualState == nil {
		fmt.Println("State for ", stateKey, ": failed to get value")
		t.FailNow()
	}

	if bytes.Compare(actualState, expectedState) != 0 {
		fmt.Println("Incorrect State for " + stateKey + ": \nExpected: " + string(expectedState) + "\nActual  : " + string(actualState))
		t.FailNow()
	}
}

//checkInvoke - helper for checking Invoke of chaincode
func checkInvoke(t *testing.T, stub *shim.MockStub, args [][]byte) {
	res := stub.MockInvoke("1", args)
	if res.Status != shim.OK {
		fmt.Println("Invoke", args, "failed", string(res.Message))
		t.FailNow()
	}
}

func checkInvokeUnknownFunction(t *testing.T, stub *shim.MockStub, args [][]byte) {
	res := stub.MockInvoke("1", args)
	if res.Status != shim.OK {
		expectedErr := "Received unknown function invocation"
		actualErr := string(res.Message)
		if !(strings.Contains(actualErr, expectedErr)) {
			fmt.Println("Invoke with unknown function must throw error: ", expectedErr, "\n Actual Error :", actualErr)
			t.FailNow()
		}
	} else {
		t.FailNow()
	}
}

//checkReadCertificateOK - helper for positive test readCertificate
func checkReadCertificateOK(t *testing.T, stub *shim.MockStub, certificateID string) {
	res := stub.MockInvoke("1", [][]byte{[]byte("readCertificate"), []byte(certificateID)})
	if res.Status != shim.OK {
		fmt.Println("func readCertificate with ID: ", certificateID, " failed"+string(res.Message))
		t.FailNow()
	}
	if res.Payload == nil {
		fmt.Println("func readCertificate with ID: ", certificateID, "failed to get value")
		t.FailNow()
	}
	if bytes.Compare(getNewCertificateExpected(), []byte(res.Payload)) != 0 {
		fmt.Println("func readCertificate with ID: ", certificateID, "Expected:", string(getNewCertificateExpected()), "Actual:", string(res.Payload))
		t.FailNow()
	}
}

//checkReadCertificateNOK - helper for negative testing of readCertificate
func checkReadCertificateNOK(t *testing.T, stub *shim.MockStub, certificateID string) {
	//with no certificateID
	res := stub.MockInvoke("1", [][]byte{[]byte("readCertificate"), []byte("")})
	if res.Status != shim.OK {
		expectedErr := "retrieveCertificate: Corrupt certificate record "
		actualErr := string(res.Message)
		if !(strings.Contains(actualErr, expectedErr)) {
			fmt.Println("func readCertificate negative test: ", "Expected Error:", expectedErr, "Actual Error", actualErr)
			t.FailNow()
		}
	} else {
		t.FailNow()
	}
}

func checkReadAllCertificatesOK(t *testing.T, stub *shim.MockStub) {
	res := stub.MockInvoke("1", [][]byte{[]byte("readAllCertificates")})
	if res.Status != shim.OK {
		fmt.Println("func readAllCertificates failed", string(res.Message))
		t.FailNow()
	}
	if res.Payload == nil {
		fmt.Println("func readAllCertificates failed to get value")
		t.FailNow()
	}
	if bytes.Compare(getExpectedCertificates(), []byte(res.Payload)) != 0 {
		fmt.Println("func readAllCertificates Expected:\n", string(getExpectedCertificates()), "\nActual:\n", string(res.Payload))
		t.FailNow()
	}
}

func checkError(t *testing.T, exp string, act string) {
	if strings.Compare(exp, act) != 0 {
		fmt.Println("Unexpected Error! Expecting ", exp, "\n Actual :", act)
		t.FailNow()
	}
}
