#!/bin/bash

cd /Users/i047582/Documents/Workspaces/git/github.wdf.sap.corp/I047582/HyperLedgerFabricApps/odo-net/test/newman/CleanAllAndSetup


echo "Clean All ReadingAssets before Setup..."
newman run cleanAllReadingAssets.postman_collection.json -e odo-net.postman_environment.json --bail newman
echo "Setting up ReadingAssets..."
newman run createAllReadingAssets.postman_collection.json -e odo-net.postman_environment.json -d ./data/ReadingAssets.json --bail newman

echo "Done. Ready to run!"
