# What's in here?
This is the folder for basic "getting started" tutorial on hyperledger service from SAP Cloud Platform

# Context
All the related documentation can be found [here](https://sap-my.sharepoint.com/personal/ramesh_suraparaju_sap_com/_layouts/15/onedrive.aspx?id=%2Fpersonal%2Framesh_suraparaju_sap_com%2FDocuments%2FWork%2FDemos%2FHyperledgerFabricApps%2Fodo-net%2Fhowto)

# Business Network Specification
Used car sales is an area of business that has a lot of possibilities for fraud.  One example is the illegal tweaking of the odometers. (The dial in the car dash that shows the total number of kilometers that the car has run in its entire lifetime).  Although it is difficult, it is not technically impossible for fraudsters to rewind the kilometer readings on the odometer of a car.
![UsedCars Today](./Model/UsedCarsToday.png)

How could blockchain help to prevent this kind of fraud?  Now imagine that there is a network of used car dealers that have access to a blockchain, where every dealer can record basic information of a used car like registration number, perhaps other key IDs like engine, chassis numbers etc. and also the odometer reading with the date at which reading was taken.  When we go back to previous scenario where the Seller tweaks his reading and gets the car to Dealer 2, the dealer can easily verify the last known odometer reading and detect the fraud.

![UsedCars with Blockchain](./Model/UsedCarsBlockchain.png)

# Model
![odoNet Model](./Model/odoNetReading.png)

# Deployed Systems
Hyperledger Fabric service on SAP Cloud Foundry (Canary)

[Hyperledger Node](https://account.int.sap.hana.ondemand.com/cockpit#/globalaccount/5d2d03c6-4f00-47bf-b3b3-2e4350382836/subaccount/f64c010a-07e4-4242-a9f3-f0a88d7d6e1b/org/961a2212-9ffc-4290-b769-2a51844fccfd/space/d527ef3f-17d4-4d88-bdc2-0837c5cb6c24/service/46f22deb-5a2f-46d3-b116-67308b7bb801/instance/18ab7647-68f5-4a78-8fc9-5cc56e34fac3/referencingApps)

[Channel exposed by the HLF Node](https://hyperledger-fabric-dashboard.cfapps.sap.hana.ondemand.com/961a2212-9ffc-4290-b769-2a51844fccfd/d527ef3f-17d4-4d88-bdc2-0837c5cb6c24/18ab7647-68f5-4a78-8fc9-5cc56e34fac3/node/channels)

[Development Channel Dashboard](https://hyperledger-fabric-dashboard.cfapps.sap.hana.ondemand.com/961a2212-9ffc-4290-b769-2a51844fccfd/c87674fe-7b53-4b4b-aae8-b34b35386cd6/15015c32-e3fd-4875-8bd7-235540d447d7/channel)

# Links to Documents

## Prerequisites
[Link](https://sap-my.sharepoint.com/:w:/r/personal/ramesh_suraparaju_sap_com/_layouts/15/Doc.aspx?sourcedoc=%7B284BF196-913F-4448-B1EF-5028DE2C0D75%7D&file=Prerequisites%20for%20Hands-On-SAP%20Blockchain%20as%20a%20Service%20_V2.docx&action=default&mobileredirect=true)

## HOWTO Document
[Link](https://sap-my.sharepoint.com/:w:/r/personal/ramesh_suraparaju_sap_com/_layouts/15/Doc.aspx?sourcedoc=%7B15BCC4A0-9504-400F-BD76-4F5E71AD9B29%7D&file=Hands-On-SAP%20Blockchain%20as%20a%20Service%20_V2.docx&action=default&mobileredirect=true)

## Presentation
[Link](https://sap-my.sharepoint.com/personal/ramesh_suraparaju_sap_com/_layouts/15/onedrive.aspx?id=%2Fpersonal%2Framesh_suraparaju_sap_com%2FDocuments%2FWork%2FDemos%2FHyperledgerFabricApps%2Fodo-net%2Fhowto%2Fpresentation)

# Published Links
None

# Contact(s)
Ramesh Suraparaju (I047582)
