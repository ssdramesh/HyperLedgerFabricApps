package main

import (
	"encoding/json"
	"errors"
	"fmt"

	"github.com/hyperledger/fabric/core/chaincode/shim"
	"github.com/hyperledger/fabric/protos/peer"
)

var logger = shim.NewLogger("CLDChaincode")

//VehicleStatus - enumeration for vehicle statuses
type VehicleStatus int

//enumeration
const (
	ReadyForConsignment VehicleStatus = 1 + iota
	AssignedToConsignment
	SubmittedForInspection
	InspectionCompleted
)

var vehicleStatuses = [...]string{
	"Ready for Consignment",
	"Assigned to Consignment",
	"Submitted for Inspection",
	"Inspection Completed",
}

func (vehicleStatus VehicleStatus) vehicleStatusToString() string {
	return vehicleStatuses[vehicleStatus-1]
}

//ConsignmentStatus - enumeration for consignment statuses
type ConsignmentStatus int

const (
	//ReadyForFreightForwarder - Ready for Freight Forwarder
	ReadyForFreightForwarder ConsignmentStatus = 1 + iota
	//HandedOverToOriginFreightForwarder - Handed Over to Origin Freight Forwarder
	HandedOverToOriginFreightForwarder
	//LadingCompleted - Lading Completed
	LadingCompleted
	//ReceivedByDestinationFreightForwarder - Received By Destination Freight Forwarder
	ReceivedByDestinationFreightForwarder
	//EnRouteToProcessingCenter - EnRoute To Processing Center
	EnRouteToProcessingCenter
	//HandedOverToProcessingCenter - Handed Over To Processing Center
	HandedOverToProcessingCenter
)

var consignmentStatuses = [...]string{
	"Ready for Freight Forwarder",
	"Handed over to Origin Freight Forwarder",
	"Lading Completed",
	"Received by Destination Freight Forwarder",
	"EnRoute to Processing Center",
	"Handed over to Processing Center",
}

func (consignmentStatus ConsignmentStatus) consignmentStatusToString() string {
	return consignmentStatuses[consignmentStatus-1]
}

//TripStatus - enumeration for trip statuses (Same status enumeration for Trip and Voyage)
type TripStatus int

const (
	//Loaded - Loaded
	Loaded TripStatus = 1 + iota
	//EnRoute - EnRoute
	EnRoute
	//Delayed - Delayed
	Delayed
	//Arrived - Arrived
	Arrived
	//Unloaded - Unloaded
	Unloaded
)

var tripStatuses = [...]string{
	"Loaded",
	"EnRoute",
	"Delayed",
	"Arrived",
	"Unloaded",
}

func (tripStatus TripStatus) tripStatusToString() string {
	return tripStatuses[tripStatus-1]
}

//Location (dataType)
type Location struct {
	Latitude        string `json:"latitude"`
	Longitude       string `json:"longitude"`
	PhysicalAddress string `json:"physicalAddress"`
}

//Party (dataType)
type Party struct {
	ID    string `json:"ID"`
	Name  string `json:"name"`
	Email string `json:"email"`
	Phone string `json:"phone"`
}

/*
Status (dataType) - see corresponding enuerations for vehicle status
and the aggregated status of consignment
*/
type Status struct {
	Name           string   `json:"name"`
	ChangeDateTime string   `json:"changeDateTime"`
	Location       Location `json:"location"`
}

//Quote (dataType)
type Quote struct {
	ID       string `json:"ID"`
	Customer Party  `json:"customer"`
}

//Truck (dataType)
type Truck struct {
	ID     string `json:"ID"`
	Driver Party  `json:"driver"`
}

//Trip (dataType)
type Trip struct {
	ID                   string   `json:"ID"`
	Statuses             []Status `json:"statuses"`
	FreightForwarder     Party    `json:"freightForwarder"`
	Truck                Truck    `json:"truck"`
	PickUpLocation       Location `json:"pickupLocation"`
	DestinationLocation  Location `json:"destinationLocation"`
	PickUpDate           string   `json:"pickupDate"`
	ExpectedDeliveryDate string   `json:"expectedDeliveryDate"`
	DelayReason          string   `json:"delayReason"`
}

//Voyage (dataType)
type Voyage struct {
	ID                    string   `json:"ID"`
	Statuses              []Status `json:"statuses"`
	VesselID              string   `json:"vesselID"`
	ShippingLine          Party    `json:"shippingLine"`
	BillOfLadingID        string   `json:"billOfLadingID"`
	OriginPort            Location `json:"originPort"`
	DestinationPort       Location `json:"destinationPort"`
	ExpectedDepartureDate string   `json:"expectedDepartureDate"`
	ExpectedArrivalDate   string   `json:"expectedArrivalDate"`
	DelayReason           string   `json:"delayReason"`
}

//Vehicle (dataType)
type Vehicle struct {
	VIN      string   `json:"VIN"` //VIN = Vehicle Identification Number
	Statuses []Status `json:"statuses"`
	Model    string   `json:"model"`
	Quote    Quote    `json:"quote"`
}

//ConsignmentAsset - Chaincode for asset Consignment
type ConsignmentAsset struct {
}

//Consignment - Details of the asset type Consignment
type Consignment struct {
	ID           string    `json:"ID"`
	Statuses     []Status  `json:"statuses"` //Status of whole consignment
	Vehicles     []Vehicle `json:"vehicles"`
	OutboundTrip Trip      `json:"outboundTrip"`
	Voyage       Voyage    `json:"voyage"`
	InboundTrip  Trip      `json:"inboundTrip"`
}

//ConsignmentIDIndex - Index on IDs for retrieval all Consignments
type ConsignmentIDIndex struct {
	IDs []string `json:"IDs"`
}

func main() {
	err := shim.Start(new(ConsignmentAsset))
	if err != nil {
		fmt.Printf("Error starting ConsignmentAsset chaincode function main(): %s", err)
	} else {
		fmt.Printf("Starting ConsignmentAsset chaincode function main() executed successfully")
	}
}

//Init - The chaincode Init function: No  arguments, only initializes a ID array as Index for retrieval of all Consignments
func (csgmnt *ConsignmentAsset) Init(stub shim.ChaincodeStubInterface) peer.Response {
	var consignmentIDIndex ConsignmentIDIndex
	bytes, _ := json.Marshal(consignmentIDIndex)
	stub.PutState("consignmentIDIndex", bytes)
	return shim.Success(nil)
}

//Invoke - The chaincode Invoke function:
func (csgmnt *ConsignmentAsset) Invoke(stub shim.ChaincodeStubInterface) peer.Response {
	function, args := stub.GetFunctionAndParameters()
	logger.Debug("function: ", function)
	switch function {
	case "addNewConsignment":
		return csgmnt.addNewConsignment(stub, args)
	case "readConsignment":
		return csgmnt.readConsignment(stub, args[0])
	case "readAllConsignments":
		return csgmnt.readAllConsignments(stub)
	case "updateVehicles":
		return csgmnt.updateVehicles(stub, args)
	case "updateOutboundTrip":
		return csgmnt.updateOutboundTrip(stub, args)
	case "updateVoyage":
		return csgmnt.updateVoyage(stub, args)
	case "updateInboundTrip":
		return csgmnt.updateInboundTrip(stub, args)
	case "removeConsignment":
		return csgmnt.removeConsignment(stub, args[0])
	case "removeAllConsignments":
		return csgmnt.removeAllConsignments(stub)
	default:
		return shim.Error("Received unknown function invocation")
	}
}

//Invoke Route: addNewConsignment
func (csgmnt *ConsignmentAsset) addNewConsignment(stub shim.ChaincodeStubInterface, args []string) peer.Response {
	consignment, err := getConsignmentFromArgs(args)
	if err != nil {
		return shim.Error("Consignment Data is Corrupted")
	}
	record, err := stub.GetState(consignment.ID)
	if record != nil {
		return shim.Error("This Consignment already exists: " + consignment.ID)
	}
	_, err = csgmnt.saveConsignment(stub, consignment)
	if err != nil {
		return shim.Error(err.Error())
	}
	_, err = csgmnt.updateConsignmentIDIndex(stub, consignment)
	if err != nil {
		return shim.Error(err.Error())
	}
	return shim.Success(nil)
}

//Invoke Route: removeConsignment
func (csgmnt *ConsignmentAsset) removeConsignment(stub shim.ChaincodeStubInterface, consignmentID string) peer.Response {
	_, err := csgmnt.deleteConsignment(stub, consignmentID)
	if err != nil {
		return shim.Error(err.Error())
	}
	_, err = csgmnt.deleteConsignmentIDIndex(stub, consignmentID)
	if err != nil {
		return shim.Error(err.Error())
	}
	return shim.Success(nil)
}

//Invoke Route: removeAllConsignments
func (csgmnt *ConsignmentAsset) removeAllConsignments(stub shim.ChaincodeStubInterface) peer.Response {
	var consignmentStructIDs ConsignmentIDIndex
	bytes, err := stub.GetState("consignmentIDIndex")
	if err != nil {
		return shim.Error("removeAllConsignments: Error getting consignmentIDIndex array")
	}
	err = json.Unmarshal(bytes, &consignmentStructIDs)
	if err != nil {
		return shim.Error("removeAllConsignments: Error unmarshalling consignmentIDIndex array JSON")
	}
	if len(consignmentStructIDs.IDs) == 0 {
		return shim.Error("removeAllConsignments: No consignments to remove")
	}
	for _, consignmentStructID := range consignmentStructIDs.IDs {
		_, err = csgmnt.deleteConsignment(stub, consignmentStructID)
		if err != nil {
			return shim.Error("Failed to remove Consignment with ID: " + consignmentStructID)
		}
		_, err = csgmnt.deleteConsignmentIDIndex(stub, consignmentStructID)
		if err != nil {
			return shim.Error(err.Error())
		}
	}
	csgmnt.initHolder(stub)
	return shim.Success(nil)
}

//Update Vehicles
func (csgmnt *ConsignmentAsset) updateVehicles(stub shim.ChaincodeStubInterface, args []string) peer.Response {

	var consignment Consignment
	var vehicles []Vehicle

	consignmentAsByteArray, err := csgmnt.retrieveConsignment(stub, args[0])
	if err != nil {
		return shim.Error(err.Error())
	}
	err = Unmarshal(consignmentAsByteArray, &consignment)
	if err != nil {
		return shim.Error(err.Error())
	}
	if !Valid(args[1]) {
		return shim.Error("updateVehicles: Invalid input json for update")
	}
	err = Unmarshal([]byte(args[1]), &vehicles)
	if err != nil {
		return shim.Error(err.Error())
	}
	consignment.Vehicles = append(consignment.Vehicles, vehicles...)
	_, err = csgmnt.saveConsignment(stub, consignment)
	if err != nil {
		return shim.Error(err.Error())
	}
	return shim.Success(nil)
}

//UpdateOutBoundTrip
func (csgmnt *ConsignmentAsset) updateOutboundTrip(stub shim.ChaincodeStubInterface, args []string) peer.Response {

	var consignment Consignment
	var trip Trip

	consignmentAsByteArray, err := csgmnt.retrieveConsignment(stub, args[0])
	if err != nil {
		return shim.Error(err.Error())
	}
	err = Unmarshal(consignmentAsByteArray, &consignment)
	if err != nil {
		return shim.Error(err.Error())
	}
	if !Valid(args[1]) {
		return shim.Error("updateOutboundTrip: Invalid input json for update")
	}
	err = Unmarshal([]byte(args[1]), &trip)
	if err != nil {
		return shim.Error(err.Error())
	}
	consignment.OutboundTrip = trip
	_, err = csgmnt.saveConsignment(stub, consignment)
	if err != nil {
		return shim.Error(err.Error())
	}
	return shim.Success(nil)
}

// TODO: UpdateVoyage
func (csgmnt *ConsignmentAsset) updateVoyage(stub shim.ChaincodeStubInterface, args []string) peer.Response {

	var consignment Consignment
	var voyage Voyage

	consignmentAsByteArray, err := csgmnt.retrieveConsignment(stub, args[0])
	if err != nil {
		return shim.Error(err.Error())
	}
	err = Unmarshal(consignmentAsByteArray, &consignment)
	if err != nil {
		return shim.Error(err.Error())
	}
	if !Valid(args[1]) {
		return shim.Error("updateVoyage: Invalid input json for update")
	}
	err = Unmarshal([]byte(args[1]), &voyage)
	if err != nil {
		return shim.Error(err.Error())
	}
	consignment.Voyage = voyage
	_, err = csgmnt.saveConsignment(stub, consignment)
	if err != nil {
		return shim.Error(err.Error())
	}
	return shim.Success(nil)
}

//UpdateInboundTrip
func (csgmnt *ConsignmentAsset) updateInboundTrip(stub shim.ChaincodeStubInterface, args []string) peer.Response {

	var consignment Consignment
	var trip Trip

	consignmentAsByteArray, err := csgmnt.retrieveConsignment(stub, args[0])
	if err != nil {
		return shim.Error(err.Error())
	}
	err = Unmarshal(consignmentAsByteArray, &consignment)
	if err != nil {
		return shim.Error(err.Error())
	}
	if !Valid(args[1]) {
		return shim.Error("updateInboundTrip: Invalid input json for update")
	}
	err = Unmarshal([]byte(args[1]), &trip)
	if err != nil {
		return shim.Error(err.Error())
	}
	consignment.InboundTrip = trip
	_, err = csgmnt.saveConsignment(stub, consignment)
	if err != nil {
		return shim.Error(err.Error())
	}
	return shim.Success(nil)
}

//Query Route: readConsignment
func (csgmnt *ConsignmentAsset) readConsignment(stub shim.ChaincodeStubInterface, consignmentID string) peer.Response {
	consignmentAsByteArray, err := csgmnt.retrieveConsignment(stub, consignmentID)
	if err != nil {
		return shim.Error(err.Error())
	}
	return shim.Success(consignmentAsByteArray)
}

//Query Route: readAllConsignments
func (csgmnt *ConsignmentAsset) readAllConsignments(stub shim.ChaincodeStubInterface) peer.Response {
	var consignmentIDs ConsignmentIDIndex
	bytes, err := stub.GetState("consignmentIDIndex")
	if err != nil {
		return shim.Error("readAllConsignments: Error getting consignmentIDIndex array")
	}
	err = json.Unmarshal(bytes, &consignmentIDs)
	if err != nil {
		return shim.Error("readAllConsignments: Error unmarshalling consignmentIDIndex array JSON")
	}
	result := "["

	var consignmentAsByteArray []byte

	for _, consignmentID := range consignmentIDs.IDs {
		consignmentAsByteArray, err = csgmnt.retrieveConsignment(stub, consignmentID)
		if err != nil {
			return shim.Error("Failed to retrieve consignment with ID: " + consignmentID)
		}
		result += string(consignmentAsByteArray) + ","
	}
	if len(result) == 1 {
		result = "[]"
	} else {
		result = result[:len(result)-1] + "]"
	}
	return shim.Success([]byte(result))
}

//Helper: Save ConsignmentAsset
func (csgmnt *ConsignmentAsset) saveConsignment(stub shim.ChaincodeStubInterface, consignment Consignment) (bool, error) {
	bytes, err := json.Marshal(consignment)
	if err != nil {
		return false, errors.New("Error converting consignment record JSON")
	}
	err = stub.PutState(consignment.ID, bytes)
	if err != nil {
		return false, errors.New("Error storing Consignment record")
	}
	return true, nil
}

//Helper: delete ConsignmentAsset
func (csgmnt *ConsignmentAsset) deleteConsignment(stub shim.ChaincodeStubInterface, consignmentID string) (bool, error) {
	_, err := csgmnt.retrieveConsignment(stub, consignmentID)
	if err != nil {
		return false, errors.New("Consignment with ID: " + consignmentID + " not found")
	}
	err = stub.DelState(consignmentID)
	if err != nil {
		return false, errors.New("Error deleting Consignment record")
	}
	return true, nil
}

//Helper: Update consignment Holder - updates Index
func (csgmnt *ConsignmentAsset) updateConsignmentIDIndex(stub shim.ChaincodeStubInterface, consignment Consignment) (bool, error) {
	var consignmentIDs ConsignmentIDIndex
	bytes, err := stub.GetState("consignmentIDIndex")
	if err != nil {
		return false, errors.New("updateConsignmentIDIndex: Error getting consignmentIDIndex array Index from state")
	}
	err = json.Unmarshal(bytes, &consignmentIDs)
	if err != nil {
		return false, errors.New("updateConsignmentIDIndex: Error unmarshalling consignmentIDIndex array JSON")
	}
	consignmentIDs.IDs = append(consignmentIDs.IDs, consignment.ID)
	bytes, err = json.Marshal(consignmentIDs)
	if err != nil {
		return false, errors.New("updateConsignmentIDIndex: Error marshalling new consignment ID")
	}
	err = stub.PutState("consignmentIDIndex", bytes)
	if err != nil {
		return false, errors.New("updateConsignmentIDIndex: Error storing new consignment ID in consignmentIDIndex (Index)")
	}
	return true, nil
}

//Helper: delete ID from consignmentStruct Holder
func (csgmnt *ConsignmentAsset) deleteConsignmentIDIndex(stub shim.ChaincodeStubInterface, consignmentID string) (bool, error) {
	var consignmentStructIDs ConsignmentIDIndex
	bytes, err := stub.GetState("consignmentIDIndex")
	if err != nil {
		return false, errors.New("deleteConsignmentIDIndex: Error getting consignmentIDIndex array Index from state")
	}
	err = json.Unmarshal(bytes, &consignmentStructIDs)
	if err != nil {
		return false, errors.New("deleteConsignmentIDIndex: Error unmarshalling consignmentIDIndex array JSON")
	}
	consignmentStructIDs.IDs, err = deleteKeyFromStringArray(consignmentStructIDs.IDs, consignmentID)
	if err != nil {
		return false, errors.New(err.Error())
	}
	bytes, err = json.Marshal(consignmentStructIDs)
	if err != nil {
		return false, errors.New("deleteConsignmentIDIndex: Error marshalling new consignmentStruct ID")
	}
	err = stub.PutState("consignmentIDIndex", bytes)
	if err != nil {
		return false, errors.New("deleteConsignmentIDIndex: Error storing new consignmentStruct ID in consignmentIDIndex (Index)")
	}
	return true, nil
}

//Helper: Initialize truck ID Holder //change template
func (csgmnt *ConsignmentAsset) initHolder(stub shim.ChaincodeStubInterface) bool {
	var consignmentIDIndex ConsignmentIDIndex
	bytes, _ := json.Marshal(consignmentIDIndex)
	stub.DelState("consignmentIDIndex")
	stub.PutState("consignmentIDIndex", bytes)
	return true
}

//deleteKeyFromArray
func deleteKeyFromStringArray(array []string, key string) (newArray []string, err error) {
	for _, entry := range array {
		if entry != key {
			newArray = append(newArray, entry)
		}
	}
	if len(newArray) == len(array) {
		return newArray, errors.New("Specified Key: " + key + " not found in Array")
	}
	return newArray, nil
}

//Helper: Retrieve Consignment
func (csgmnt *ConsignmentAsset) retrieveConsignment(stub shim.ChaincodeStubInterface, consignmentID string) ([]byte, error) {
	var consignment Consignment
	var consignmentAsByteArray []byte
	bytes, err := stub.GetState(consignmentID)
	if err != nil {
		return consignmentAsByteArray, errors.New("retrieveConsignment: Error retrieving consignment with ID: " + consignmentID)
	}
	err = json.Unmarshal(bytes, &consignment)
	if err != nil {
		return consignmentAsByteArray, errors.New("retrieveConsignment: Corrupt consignment record " + string(bytes))
	}
	consignmentAsByteArray, err = json.Marshal(consignment)
	if err != nil {
		return consignmentAsByteArray, errors.New("readConsignment: Invalid consignment Object - Not a  valid JSON")
	}
	return consignmentAsByteArray, nil
}

//getConsignmentFromArgs - construct a consignment structure from string array of arguments
func getConsignmentFromArgs(args []string) (consignment Consignment, err error) {

	if !Valid(args[0]) {
		return consignment, errors.New("Invalid json")
	}

	err = Unmarshal([]byte(args[0]), &consignment)
	if err != nil {
		return consignment, err
	}
	return consignment, nil
}
