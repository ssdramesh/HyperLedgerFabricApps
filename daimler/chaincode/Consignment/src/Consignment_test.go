package main

import (
	"bytes"
	"encoding/json"
	"fmt"
	"strings"
	"testing"

	"github.com/hyperledger/fabric/core/chaincode/shim"
)

//TestConsignmentAsset_Init
func TestConsignmentAsset_Init(t *testing.T) {
	consignment := new(ConsignmentAsset)
	stub := shim.NewMockStub("consignment", consignment)
	checkInit(t, stub, [][]byte{[]byte("init")})
	checkState(t, stub, "consignmentIDIndex", getExpectedConsignmentIDIndex("init"))
}

//TestConsignmentAsset_InvokeUnknownFunction
func TestConsignmentAsset_InvokeUnknownFunction(t *testing.T) {
	consignment := new(ConsignmentAsset)
	stub := shim.NewMockStub("consignment", consignment)
	checkInit(t, stub, [][]byte{[]byte("init")})
	checkInvokeUnknownFunction(t, stub, [][]byte{[]byte("myFunction"), []byte("docType:Asset")})
}

//TestConsignmentAsset_Invoke_addNewConsignment
func TestConsignmentAsset_Invoke_addNewConsignmentOK(t *testing.T) {
	consignment := new(ConsignmentAsset)
	stub := shim.NewMockStub("consignment", consignment)
	checkInit(t, stub, [][]byte{[]byte("init")})
	checkInvoke(t, stub, getFirstConsignmentAssetForTesting())
	newConsignmentID := "100001"
	checkState(t, stub, newConsignmentID, getNewConsignmentExpected())
	checkState(t, stub, "consignmentIDIndex", getExpectedConsignmentIDIndex("addNewConsignment"))
}

//TestConsignmentAsset_Invoke_addNewConsignment
func TestConsignmentAsset_Invoke_addNewConsignmentDuplicate(t *testing.T) {
	consignment := new(ConsignmentAsset)
	stub := shim.NewMockStub("consignment", consignment)
	checkInit(t, stub, [][]byte{[]byte("init")})
	checkInvoke(t, stub, getFirstConsignmentAssetForTesting())
	newConsignmentID := "100001"
	checkState(t, stub, newConsignmentID, getNewConsignmentExpected())
	checkState(t, stub, "consignmentIDIndex", getExpectedConsignmentIDIndex("addNewConsignment"))
	res := stub.MockInvoke("1", getFirstConsignmentAssetForTesting())
	if res.Status != shim.OK {
		checkError(t, "This Consignment already exists: 100001", res.Message)
	} else {
		fmt.Println("Error was expected, but not raised")
		t.FailNow()
	}
}

//TestConsignmentAsset_Invoke_removeConsignmentOK  //change template
func TestConsignmentAsset_Invoke_removeConsignmentOK(t *testing.T) {
	consignment := new(ConsignmentAsset)
	stub := shim.NewMockStub("consignment", consignment)
	checkInit(t, stub, [][]byte{[]byte("init")})
	checkInvoke(t, stub, getFirstConsignmentAssetForTesting())
	checkInvoke(t, stub, getSecondConsignmentAssetForTesting())
	checkReadAllConsignmentsOK(t, stub)
	checkState(t, stub, "consignmentIDIndex", getExpectedConsignmentIDIndex("beforeRemoveConsignment"))
	checkInvoke(t, stub, getRemoveSecondConsignmentAssetForTesting())
	remainingConsignmentID := "100001"
	checkReadConsignmentOK(t, stub, remainingConsignmentID)
	checkState(t, stub, "consignmentIDIndex", getExpectedConsignmentIDIndex("afterRemoveConsignment"))
}

//TestConsignmentAsset_Invoke_removeConsignmentNOK  //change template
func TestConsignmentAsset_Invoke_removeConsignmentNOK(t *testing.T) {
	consignment := new(ConsignmentAsset)
	stub := shim.NewMockStub("consignment", consignment)
	checkInit(t, stub, [][]byte{[]byte("init")})
	checkInvoke(t, stub, getFirstConsignmentAssetForTesting())
	firstConsignmentID := "100001"
	checkReadConsignmentOK(t, stub, firstConsignmentID)
	checkState(t, stub, "consignmentIDIndex", getExpectedConsignmentIDIndex("addNewConsignment"))
	res := stub.MockInvoke("1", getRemoveSecondConsignmentAssetForTesting())
	if res.Status != shim.OK {
		checkError(t, "Consignment with ID: "+"100002"+" not found", res.Message)
	} else {
		fmt.Println("Error was expected, but not raised")
		t.FailNow()
	}
	checkState(t, stub, "consignmentIDIndex", getExpectedConsignmentIDIndex("addNewConsignment"))
}

//TestConsignmentAsset_Invoke_removeAllConsignmentsOK  //change template
func TestConsignmentAsset_Invoke_removeAllConsignmentsOK(t *testing.T) {
	consignment := new(ConsignmentAsset)
	stub := shim.NewMockStub("consignment", consignment)
	checkInit(t, stub, [][]byte{[]byte("init")})
	checkInvoke(t, stub, getFirstConsignmentAssetForTesting())
	checkInvoke(t, stub, getSecondConsignmentAssetForTesting())
	checkReadAllConsignmentsOK(t, stub)
	checkState(t, stub, "consignmentIDIndex", getExpectedConsignmentIDIndex("beforeRemoveConsignment"))
	checkInvoke(t, stub, getRemoveAllConsignmentAssetsForTesting())
	checkState(t, stub, "consignmentIDIndex", getExpectedConsignmentIDIndex(""))
}

//TestConsignmentAsset_Invoke_removeConsignmentNOK  //change template
func TestConsignmentAsset_Invoke_removeAllConsignmentsNOK(t *testing.T) {
	consignment := new(ConsignmentAsset)
	stub := shim.NewMockStub("consignment", consignment)
	checkInit(t, stub, [][]byte{[]byte("init")})
	res := stub.MockInvoke("1", getRemoveAllConsignmentAssetsForTesting())
	if res.Status != shim.OK {
		checkError(t, "removeAllConsignments: No consignments to remove", res.Message)
	} else {
		fmt.Println("Error was expected, but not raised")
		t.FailNow()
	}
	checkState(t, stub, "consignmentIDIndex", getExpectedConsignmentIDIndex(""))
}

//TestConsignmentAsset_Query_readConsignment
func TestConsignmentAsset_Query_readConsignment(t *testing.T) {
	consignment := new(ConsignmentAsset)
	stub := shim.NewMockStub("consignment", consignment)
	checkInit(t, stub, [][]byte{[]byte("init")})
	consignmentID := "100001"
	checkInvoke(t, stub, getFirstConsignmentAssetForTesting())
	checkReadConsignmentOK(t, stub, consignmentID)
	checkReadConsignmentNOK(t, stub, "")
}

//TestConsignmentAsset_Query_readAllConsignments
func TestConsignmentAsset_Query_readAllConsignments(t *testing.T) {
	consignment := new(ConsignmentAsset)
	stub := shim.NewMockStub("consignment", consignment)
	checkInit(t, stub, [][]byte{[]byte("init")})
	checkInvoke(t, stub, getFirstConsignmentAssetForTesting())
	checkInvoke(t, stub, getSecondConsignmentAssetForTesting())
	checkReadAllConsignmentsOK(t, stub)
}

//TestConsignmentAsset_UpdateVehicleOK
func TestConsignmentAsset_UpdateVehicleOK(t *testing.T) {
	consignment := new(ConsignmentAsset)
	stub := shim.NewMockStub("consignment", consignment)
	checkInit(t, stub, [][]byte{[]byte("init")})
	consignmentID := "100001"
	checkInvoke(t, stub, getFirstConsignmentAssetForTesting())
	checkReadConsignmentOK(t, stub, consignmentID)
	checkInvoke(t, stub, getFirstVehicleForUpdateVehiclesOKTesting())
	checkInvoke(t, stub, getSecondVehicleForUpdateVehiclesOKTesting())
	checkReadConsignmentAfterVehiclesUpdateOK(t, stub, consignmentID)
}

// TODO: updateVehiclesNOK

//TestConsignmentAsset_UpdateOutboundTripOK
func TestConsignmentAsset_UpdateOutboundTripOK(t *testing.T) {
	consignment := new(ConsignmentAsset)
	stub := shim.NewMockStub("consignment", consignment)
	checkInit(t, stub, [][]byte{[]byte("init")})
	consignmentID := "100001"
	checkInvoke(t, stub, getFirstConsignmentAssetForTesting())
	checkReadConsignmentOK(t, stub, consignmentID)
	checkInvoke(t, stub, getOutboundTripForTesting())
	checkReadConsignmentAfterOutboundTripUpdateOK(t, stub, consignmentID)
}

// TODO: updateOutboundTripNOK

//TestConsignmentAsset_UpdateVoyageOK
func TestConsignmentAsset_UpdateVoyageOK(t *testing.T) {
	consignment := new(ConsignmentAsset)
	stub := shim.NewMockStub("consignment", consignment)
	checkInit(t, stub, [][]byte{[]byte("init")})
	consignmentID := "100001"
	checkInvoke(t, stub, getFirstConsignmentAssetForTesting())
	checkReadConsignmentOK(t, stub, consignmentID)
	checkInvoke(t, stub, getVoyageForTesting())
	checkReadConsignmentAfterVoyageUpdateOK(t, stub, consignmentID)
}

// TODO: updateVoyageNOK

//TestConsignmentAsset_UpdateInpoundTripOK
func TestConsignmentAsset_UpdateInpoundTripOK(t *testing.T) {
	consignment := new(ConsignmentAsset)
	stub := shim.NewMockStub("consignment", consignment)
	checkInit(t, stub, [][]byte{[]byte("init")})
	consignmentID := "100001"
	checkInvoke(t, stub, getFirstConsignmentAssetForTesting())
	checkReadConsignmentOK(t, stub, consignmentID)
	checkInvoke(t, stub, getInboundTripForTesting())
	checkReadConsignmentAfterInboundTripUpdateOK(t, stub, consignmentID)
}

// TODO: updateInboundTripNOK

/*
*
*	Helper Functions
*
 */
//Get first ConsignmentAsset for testing - barebones consignment only
func getFirstConsignmentAssetForTesting() [][]byte {
	return [][]byte{[]byte("addNewConsignment"),
		[]byte("{\"ID\":\"100001\",\"statuses\":[{\"name\":\"Ready for Freight Forwarder\",\"changeDateTime\":\"2018-06-02\",\"location\":{\"latitude\":\"12.123456\",\"longitude\":\"120.123456\",\"physicalAddress\":\"Middle of nowhere\"}}],\"outboundTrip\":{},\"voyage\":{},\"inboundTrip\":{}}")}
}

//Get second ConsignmentAsset for testing
func getSecondConsignmentAssetForTesting() [][]byte {
	return [][]byte{[]byte("addNewConsignment"),
		[]byte("{\"ID\":\"100002\",\"statuses\":[{\"name\":\"Handed over to Freight Forwarder\",\"changeDateTime\":\"2018-06-01\",\"location\":{\"latitude\":\"11.123456\",\"longitude\":\"110.123456\",\"physicalAddress\":\"Middle of somewhere\"}}],\"outboundTrip\":{},\"voyage\":{},\"inboundTrip\":{}}")}
}

//Get remove second ConsignmentAsset for testing //change template
func getRemoveSecondConsignmentAssetForTesting() [][]byte {
	return [][]byte{[]byte("removeConsignment"),
		[]byte("100002")}
}

//Get remove all ConsignmentAssets for testing //change template
func getRemoveAllConsignmentAssetsForTesting() [][]byte {
	return [][]byte{[]byte("removeAllConsignments")}
}

//Get getFirstVehicleForUpdateVehiclesOKTesting()
func getFirstVehicleForUpdateVehiclesOKTesting() [][]byte {
	return [][]byte{[]byte("updateVehicles"),
		[]byte("100001"),
		[]byte("[{\"VIN\":\"4711\",\"statuses\":[{\"name\":\"Ready for Freight Forwarder\",\"changeDateTime\":\"2018-06-02\",\"location\":{\"latitude\":\"12.123456\",\"longitude\":\"120.123456\",\"physicalAddress\":\"Middle of nowhere\"}}],\"model\":\"C180\",\"quote\":{\"ID\":\"QUO1001\",\"customer\":{\"ID\":\"CUS1001\",\"name\":\"John Doe\",\"email\":\"john.doe@email.com\",\"phone\":\"001-002-4567-8976\"}}}]")}
}

//Get getSecondVehicleForUpdateVehiclesOKTesting()
func getSecondVehicleForUpdateVehiclesOKTesting() [][]byte {
	return [][]byte{[]byte("updateVehicles"),
		[]byte("100001"),
		[]byte("[{\"VIN\":\"4712\",\"statuses\":[{\"name\":\"Ready for Freight Forwarder\",\"changeDateTime\":\"2018-06-02\",\"location\":{\"latitude\":\"12.123456\",\"longitude\":\"120.123456\",\"physicalAddress\":\"Middle of nowhere\"}}],\"model\":\"E120\",\"quote\":{\"ID\":\"QUO1002\",\"customer\":{\"ID\":\"CUS1002\",\"name\":\"Jane Doe\",\"email\":\"jane.doe@email.com\",\"phone\":\"001-002-8976-4567\"}}}]")}
}

//getOutboundTripForTesting()
func getOutboundTripForTesting() [][]byte {
	return [][]byte{[]byte("updateOutboundTrip"),
		[]byte("100001"),
		[]byte("{\"ID\":\"OTRP1001\",\"statuses\":[{\"name\":\"Loaded\",\"changeDateTime\":\"2018-06-02\",\"location\":{\"latitude\":\"12.123456\",\"longitude\":\"120.123456\",\"physicalAddress\":\"Middle of nowhere\"}}],\"freightForwarder\":{\"ID\":\"FF1001\",\"name\":\"Jane Doe\",\"email\":\"jane.doe@email.com\",\"phone\":\"001-002-4567-8976\"},\"truck\":{\"ID\":\"TRU1001\",\"driver\":{\"ID\":\"DRV1001\",\"name\":\"John Doe\",\"email\":\"john.doe@email.com\",\"phone\":\"001-002-8976-4567\"}},\"pickupLocation\":{\"latitude\":\"10.123456\",\"longitude\":\"100.123456\",\"physicalAddress\":\"Middle of nowhere\"}, \"destinationLocation\":{\"latitude\":\"12.123456\",\"longitude\":\"120.123456\",\"physicalAddress\":\"Middle of somewhere\"},\"pickupDate\":\"2018-06-02\",\"expectedDeliveryDate\":\"2018-06-20\",\"delayReason\":\"none\"}")}
}

//getVoyageForTesting()
func getVoyageForTesting() [][]byte {
	return [][]byte{[]byte("updateVoyage"),
		[]byte("100001"),
		[]byte("{\"ID\":\"VOY1001\",\"statuses\":[{\"name\":\"Loaded\",\"changeDateTime\":\"2018-06-02\",\"location\":{\"latitude\":\"12.123456\",\"longitude\":\"120.123456\",\"physicalAddress\":\"Middle of nowhere\"}}],\"vesselID\":\"VSL1001\",\"shippingLine\":{\"ID\":\"SL1001\",\"name\":\"Jane Doe\",\"email\":\"jane.doe@email.com\",\"phone\":\"001-002-4567-8976\"},\"billOfLadingID\":\"BL1001\",\"originPort\":{\"latitude\":\"10.123456\",\"longitude\":\"100.123456\",\"physicalAddress\":\"Middle of nowhere\"}, \"destinationPort\":{\"latitude\":\"12.123456\",\"longitude\":\"120.123456\",\"physicalAddress\":\"Middle of somewhere\"},\"expectedDepartureDate\":\"2018-06-02\",\"expectedArrivalDate\":\"2018-06-20\",\"delayReason\":\"none\"}")}
}

//getInboundTripForTesting()
func getInboundTripForTesting() [][]byte {
	return [][]byte{[]byte("updateInboundTrip"),
		[]byte("100001"),
		[]byte("{\"ID\":\"ITRP1001\",\"statuses\":[{\"name\":\"Loaded\",\"changeDateTime\":\"2018-06-02\",\"location\":{\"latitude\":\"12.123456\",\"longitude\":\"120.123456\",\"physicalAddress\":\"Middle of nowhere\"}}],\"freightForwarder\":{\"ID\":\"FF1001\",\"name\":\"Jane Doe\",\"email\":\"jane.doe@email.com\",\"phone\":\"001-002-4567-8976\"},\"truck\":{\"ID\":\"TRU1001\",\"driver\":{\"ID\":\"DRV1001\",\"name\":\"John Doe\",\"email\":\"john.doe@email.com\",\"phone\":\"001-002-8976-4567\"}},\"pickupLocation\":{\"latitude\":\"10.123456\",\"longitude\":\"100.123456\",\"physicalAddress\":\"Middle of nowhere\"}, \"destinationLocation\":{\"latitude\":\"12.123456\",\"longitude\":\"120.123456\",\"physicalAddress\":\"Middle of somewhere\"},\"pickupDate\":\"2018-06-02\",\"expectedDeliveryDate\":\"2018-06-20\",\"delayReason\":\"none\"}")}
}

//Get an expected value for testing
func getNewConsignmentExpected() []byte {
	var consignment Consignment
	var conStatus Status
	var conStatuses []Status
	var loc Location

	consignment.ID = "100001"
	loc.Latitude = "12.123456"
	loc.Longitude = "120.123456"
	loc.PhysicalAddress = "Middle of nowhere"

	conStatus.Name = "Ready for Freight Forwarder"
	conStatus.ChangeDateTime = "2018-06-02"
	conStatus.Location = loc
	conStatuses = append(conStatuses, conStatus)

	consignment.Statuses = conStatuses

	consignmentJSON, err := json.Marshal(consignment)
	if err != nil {
		fmt.Println("Error converting a Consignment record to JSON")
		return nil
	}
	return []byte(consignmentJSON)
}

//Get expected values of Consignments for testing - before updates
func getExpectedConsignments() []byte {
	var consignments []Consignment
	var consignment Consignment

	var conStatus Status
	var conStatuses []Status
	var loc Location

	consignment.ID = "100001"
	loc.Latitude = "12.123456"
	loc.Longitude = "120.123456"
	loc.PhysicalAddress = "Middle of nowhere"

	conStatus.Name = "Ready for Freight Forwarder"
	conStatus.ChangeDateTime = "2018-06-02"
	conStatus.Location = loc
	conStatuses = append(conStatuses, conStatus)

	consignment.Statuses = conStatuses

	consignments = append(consignments, consignment)

	conStatuses = nil

	consignment.ID = "100002"
	loc.Latitude = "11.123456"
	loc.Longitude = "110.123456"
	loc.PhysicalAddress = "Middle of somewhere"

	conStatus.Name = "Handed over to Freight Forwarder"
	conStatus.ChangeDateTime = "2018-06-01"
	conStatus.Location = loc
	conStatuses = append(conStatuses, conStatus)

	consignment.Statuses = conStatuses

	consignments = append(consignments, consignment)

	consignmentJSON, err := json.Marshal(consignments)
	if err != nil {
		fmt.Println("Error converting consignment records to JSON")
		return nil
	}
	return []byte(consignmentJSON)
}

//Get expected values of Consignments for testing - after vehicle updates
func getExpectedConsignmentAfterVehiclesUpdate() []byte {
	var consignment Consignment

	var conStatus Status
	var conStatuses []Status
	var loc Location

	var vehicle Vehicle
	var vehicles []Vehicle
	var party Party
	var quote Quote

	consignment.ID = "100001"
	loc.Latitude = "12.123456"
	loc.Longitude = "120.123456"
	loc.PhysicalAddress = "Middle of nowhere"

	conStatus.Name = "Ready for Freight Forwarder"
	conStatus.ChangeDateTime = "2018-06-02"
	conStatus.Location = loc
	conStatuses = append(conStatuses, conStatus)

	consignment.Statuses = conStatuses

	vehicle.VIN = "4711"
	vehicle.Model = "C180"
	quote.ID = "QUO1001"
	party.ID = "CUS1001"
	party.Name = "John Doe"
	party.Email = "john.doe@email.com"
	party.Phone = "001-002-4567-8976"
	quote.Customer = party
	vehicle.Quote = quote
	vehicle.Statuses = conStatuses

	vehicles = append(vehicles, vehicle)

	vehicle.VIN = "4712"
	vehicle.Model = "E120"
	quote.ID = "QUO1002"
	party.ID = "CUS1002"
	party.Name = "Jane Doe"
	party.Email = "jane.doe@email.com"
	party.Phone = "001-002-8976-4567"
	quote.Customer = party
	vehicle.Quote = quote
	vehicle.Statuses = conStatuses

	vehicles = append(vehicles, vehicle)

	consignment.Vehicles = vehicles

	consignmentJSON, err := json.Marshal(consignment)
	if err != nil {
		fmt.Println("Error converting consignment records to JSON")
		return nil
	}
	return []byte(consignmentJSON)
}

//getExpectedConsignmentAfterOutboundTripUpdate()
func getExpectedConsignmentAfterOutboundTripUpdate() []byte {
	var consignment Consignment

	var conStatus Status
	var conStatuses []Status
	var loc Location
	var truck Truck
	var trip Trip
	var party Party

	consignment.ID = "100001"
	loc.Latitude = "12.123456"
	loc.Longitude = "120.123456"
	loc.PhysicalAddress = "Middle of nowhere"
	conStatus.Name = "Ready for Freight Forwarder"
	conStatus.ChangeDateTime = "2018-06-02"
	conStatus.Location = loc
	conStatuses = append(conStatuses, conStatus)
	consignment.Statuses = conStatuses

	conStatuses = nil

	trip.ID = "OTRP1001"
	loc.Latitude = "12.123456"
	loc.Longitude = "120.123456"
	loc.PhysicalAddress = "Middle of nowhere"
	conStatus.Name = "Loaded"
	conStatus.ChangeDateTime = "2018-06-02"
	conStatus.Location = loc
	conStatuses = append(conStatuses, conStatus)
	trip.Statuses = conStatuses
	party.ID = "FF1001"
	party.Name = "Jane Doe"
	party.Email = "jane.doe@email.com"
	party.Phone = "001-002-4567-8976"
	trip.FreightForwarder = party
	party.ID = "DRV1001"
	party.Name = "John Doe"
	party.Email = "john.doe@email.com"
	party.Phone = "001-002-8976-4567"
	truck.ID = "TRU1001"
	truck.Driver = party
	trip.Truck = truck
	loc.Latitude = "10.123456"
	loc.Longitude = "100.123456"
	loc.PhysicalAddress = "Middle of nowhere"
	trip.PickUpLocation = loc
	loc.Latitude = "12.123456"
	loc.Longitude = "120.123456"
	loc.PhysicalAddress = "Middle of somewhere"
	trip.DestinationLocation = loc
	trip.PickUpDate = "2018-06-02"
	trip.ExpectedDeliveryDate = "2018-06-20"
	trip.DelayReason = "none"

	consignment.OutboundTrip = trip

	consignmentJSON, err := json.Marshal(consignment)
	if err != nil {
		fmt.Println("Error converting consignment records to JSON")
		return nil
	}
	return []byte(consignmentJSON)
}

//getExpectedConsignmentAfterInboundTripUpdate()
func getExpectedConsignmentAfterInboundTripUpdate() []byte {
	var consignment Consignment

	var conStatus Status
	var conStatuses []Status
	var loc Location
	var truck Truck
	var trip Trip
	var party Party

	consignment.ID = "100001"
	loc.Latitude = "12.123456"
	loc.Longitude = "120.123456"
	loc.PhysicalAddress = "Middle of nowhere"
	conStatus.Name = "Ready for Freight Forwarder"
	conStatus.ChangeDateTime = "2018-06-02"
	conStatus.Location = loc
	conStatuses = append(conStatuses, conStatus)
	consignment.Statuses = conStatuses

	conStatuses = nil

	trip.ID = "ITRP1001"
	loc.Latitude = "12.123456"
	loc.Longitude = "120.123456"
	loc.PhysicalAddress = "Middle of nowhere"
	conStatus.Name = "Loaded"
	conStatus.ChangeDateTime = "2018-06-02"
	conStatus.Location = loc
	conStatuses = append(conStatuses, conStatus)
	trip.Statuses = conStatuses
	party.ID = "FF1001"
	party.Name = "Jane Doe"
	party.Email = "jane.doe@email.com"
	party.Phone = "001-002-4567-8976"
	trip.FreightForwarder = party
	party.ID = "DRV1001"
	party.Name = "John Doe"
	party.Email = "john.doe@email.com"
	party.Phone = "001-002-8976-4567"
	truck.ID = "TRU1001"
	truck.Driver = party
	trip.Truck = truck
	loc.Latitude = "10.123456"
	loc.Longitude = "100.123456"
	loc.PhysicalAddress = "Middle of nowhere"
	trip.PickUpLocation = loc
	loc.Latitude = "12.123456"
	loc.Longitude = "120.123456"
	loc.PhysicalAddress = "Middle of somewhere"
	trip.DestinationLocation = loc
	trip.PickUpDate = "2018-06-02"
	trip.ExpectedDeliveryDate = "2018-06-20"
	trip.DelayReason = "none"

	consignment.InboundTrip = trip

	consignmentJSON, err := json.Marshal(consignment)
	if err != nil {
		fmt.Println("Error converting consignment records to JSON")
		return nil
	}
	return []byte(consignmentJSON)
}

//getExpectedConsignmentAfterVoyageUpdate()
func getExpectedConsignmentAfterVoyageUpdate() []byte {
	var consignment Consignment

	var conStatus Status
	var conStatuses []Status
	var loc Location

	var voyage Voyage
	var party Party

	consignment.ID = "100001"
	loc.Latitude = "12.123456"
	loc.Longitude = "120.123456"
	loc.PhysicalAddress = "Middle of nowhere"
	conStatus.Name = "Ready for Freight Forwarder"
	conStatus.ChangeDateTime = "2018-06-02"
	conStatus.Location = loc
	conStatuses = append(conStatuses, conStatus)
	consignment.Statuses = conStatuses

	conStatuses = nil

	voyage.ID = "VOY1001"
	voyage.VesselID = "VSL1001"
	loc.Latitude = "12.123456"
	loc.Longitude = "120.123456"
	loc.PhysicalAddress = "Middle of nowhere"
	conStatus.Name = "Loaded"
	conStatus.ChangeDateTime = "2018-06-02"
	conStatus.Location = loc
	conStatuses = append(conStatuses, conStatus)
	voyage.Statuses = conStatuses
	party.ID = "SL1001"
	party.Name = "Jane Doe"
	party.Email = "jane.doe@email.com"
	party.Phone = "001-002-4567-8976"
	voyage.ShippingLine = party
	voyage.BillOfLadingID = "BL1001"
	loc.Latitude = "10.123456"
	loc.Longitude = "100.123456"
	loc.PhysicalAddress = "Middle of nowhere"
	voyage.OriginPort = loc
	loc.Latitude = "12.123456"
	loc.Longitude = "120.123456"
	loc.PhysicalAddress = "Middle of somewhere"
	voyage.DestinationPort = loc
	voyage.ExpectedDepartureDate = "2018-06-02"
	voyage.ExpectedArrivalDate = "2018-06-20"
	voyage.DelayReason = "none"

	consignment.Voyage = voyage

	consignmentJSON, err := json.Marshal(consignment)
	if err != nil {
		fmt.Println("Error converting consignment records to JSON")
		return nil
	}
	return []byte(consignmentJSON)
}

func getExpectedConsignmentIDIndex(funcName string) []byte {
	var consignmentIDIndex ConsignmentIDIndex
	switch funcName {
	case "addNewConsignment":
		consignmentIDIndex.IDs = append(consignmentIDIndex.IDs, "100001")
		consignmentIDIndexBytes, err := json.Marshal(consignmentIDIndex)
		if err != nil {
			fmt.Println("Error converting ConsignmentIDIndex to JSON")
			return nil
		}
		return consignmentIDIndexBytes
	case "beforeRemoveConsignment":
		consignmentIDIndex.IDs = append(consignmentIDIndex.IDs, "100001")
		consignmentIDIndex.IDs = append(consignmentIDIndex.IDs, "100002")
		consignmentIDIndexBytes, err := json.Marshal(consignmentIDIndex)
		if err != nil {
			fmt.Println("Error converting ConsignmentIDIndex to JSON")
			return nil
		}
		return consignmentIDIndexBytes
	case "afterRemoveConsignment":
		consignmentIDIndex.IDs = append(consignmentIDIndex.IDs, "100001")
		consignmentIDIndexBytes, err := json.Marshal(consignmentIDIndex)
		if err != nil {
			fmt.Println("Error converting ConsignmentIDIndex to JSON")
			return nil
		}
		return consignmentIDIndexBytes
	default:
		consignmentIDIndexBytes, err := json.Marshal(consignmentIDIndex)
		if err != nil {
			fmt.Println("Error converting ConsignmentIDIndex to JSON")
			return nil
		}
		return consignmentIDIndexBytes
	}
}

//checkInit - helper to check the Initialization of chaincode: ConsignmentAsset
func checkInit(t *testing.T, stub *shim.MockStub, args [][]byte) {
	res := stub.MockInit("1", args)
	if res.Status != shim.OK {
		fmt.Println("Init failed", string(res.Message))
		t.FailNow()
	}
}

//checkState - helper for checking the chaincode state for a given stateKey afgainst an expected value
func checkState(t *testing.T, stub *shim.MockStub, stateKey string, expectedState []byte) {
	actualState := stub.State[stateKey]
	if actualState == nil {
		fmt.Println("State for ", stateKey, ": failed to get value")
		t.FailNow()
	}

	if bytes.Compare(actualState, expectedState) != 0 {
		fmt.Println("Incorrect State for " + stateKey + ": \nExpected: " + string(expectedState) + "\nActual  : " + string(actualState))
		t.FailNow()
	}
}

//checkInvoke - helper for checking Invoke of chaincode
func checkInvoke(t *testing.T, stub *shim.MockStub, args [][]byte) {
	res := stub.MockInvoke("1", args)
	if res.Status != shim.OK {
		fmt.Println("Invoke", args, "failed", string(res.Message))
		t.FailNow()
	}
}

func checkInvokeUnknownFunction(t *testing.T, stub *shim.MockStub, args [][]byte) {
	res := stub.MockInvoke("1", args)
	if res.Status != shim.OK {
		expectedErr := "Received unknown function invocation"
		actualErr := string(res.Message)
		if !(strings.Contains(actualErr, expectedErr)) {
			fmt.Println("Invoke with unknown function must throw error: ", expectedErr, "\n Actual Error :", actualErr)
			t.FailNow()
		}
	} else {
		t.FailNow()
	}
}

//checkReadConsignmentOK - helper for positive test readConsignment
func checkReadConsignmentOK(t *testing.T, stub *shim.MockStub, consignmentID string) {
	res := stub.MockInvoke("1", [][]byte{[]byte("readConsignment"), []byte(consignmentID)})
	if res.Status != shim.OK {
		fmt.Println("func readConsignment with ID: ", consignmentID, " failed"+string(res.Message))
		t.FailNow()
	}
	if res.Payload == nil {
		fmt.Println("func readConsignment with ID: ", consignmentID, "failed to get value")
		t.FailNow()
	}
	if bytes.Compare(getNewConsignmentExpected(), []byte(res.Payload)) != 0 {
		fmt.Println("func readConsignment with ID: ", consignmentID, "Expected:", string(getNewConsignmentExpected()), "Actual:", string(res.Payload))
		t.FailNow()
	}
}

//checkReadConsignmentNOK - helper for negative testing of readConsignment
func checkReadConsignmentNOK(t *testing.T, stub *shim.MockStub, consignmentID string) {
	//with no consignmentID
	res := stub.MockInvoke("1", [][]byte{[]byte("readConsignment"), []byte("")})
	if res.Status != shim.OK {
		expectedErr := "retrieveConsignment: Corrupt consignment record "
		actualErr := string(res.Message)
		if !(strings.Contains(actualErr, expectedErr)) {
			fmt.Println("func readConsignment negative test: ", "Expected Error:", expectedErr, "Actual Error", actualErr)
			t.FailNow()
		}
	} else {
		t.FailNow()
	}
}

func checkReadConsignmentAfterVehiclesUpdateOK(t *testing.T, stub *shim.MockStub, consignmentID string) {
	res := stub.MockInvoke("1", [][]byte{[]byte("readConsignment"), []byte(consignmentID)})
	if res.Status != shim.OK {
		fmt.Println("updateVehicles: func readConsignment failed", string(res.Message))
		t.FailNow()
	}
	if res.Payload == nil {
		fmt.Println("updateVehicles: func readConsignment failed to get value")
		t.FailNow()
	}
	if bytes.Compare(getExpectedConsignmentAfterVehiclesUpdate(), []byte(res.Payload)) != 0 {
		fmt.Println("updateVehicles: func readConsignment Expected:\n", string(getExpectedConsignmentAfterVehiclesUpdate()), "\nActual:\n", string(res.Payload))
		t.FailNow()
	}
}

func checkReadConsignmentAfterOutboundTripUpdateOK(t *testing.T, stub *shim.MockStub, consignmentID string) {
	res := stub.MockInvoke("1", [][]byte{[]byte("readConsignment"), []byte(consignmentID)})
	if res.Status != shim.OK {
		fmt.Println("updateOutboundTrip: func readConsignment failed", string(res.Message))
		t.FailNow()
	}
	if res.Payload == nil {
		fmt.Println("updateOutboundTrip: func readConsignment failed to get value")
		t.FailNow()
	}
	if bytes.Compare(getExpectedConsignmentAfterOutboundTripUpdate(), []byte(res.Payload)) != 0 {
		fmt.Println("updateOutboundTrip: func readConsignment Expected:\n", string(getExpectedConsignmentAfterOutboundTripUpdate()), "\nActual:\n", string(res.Payload))
		t.FailNow()
	}
}

func checkReadConsignmentAfterInboundTripUpdateOK(t *testing.T, stub *shim.MockStub, consignmentID string) {
	res := stub.MockInvoke("1", [][]byte{[]byte("readConsignment"), []byte(consignmentID)})
	if res.Status != shim.OK {
		fmt.Println("updateInboundTrip: func readConsignment failed", string(res.Message))
		t.FailNow()
	}
	if res.Payload == nil {
		fmt.Println("updateInboundTrip: func readConsignment failed to get value")
		t.FailNow()
	}
	if bytes.Compare(getExpectedConsignmentAfterInboundTripUpdate(), []byte(res.Payload)) != 0 {
		fmt.Println("updateInboundTrip: func readConsignment Expected:\n", string(getExpectedConsignmentAfterInboundTripUpdate()), "\nActual:\n", string(res.Payload))
		t.FailNow()
	}
}

func checkReadConsignmentAfterVoyageUpdateOK(t *testing.T, stub *shim.MockStub, consignmentID string) {
	res := stub.MockInvoke("1", [][]byte{[]byte("readConsignment"), []byte(consignmentID)})
	if res.Status != shim.OK {
		fmt.Println("updateVoyage: func readConsignment failed", string(res.Message))
		t.FailNow()
	}
	if res.Payload == nil {
		fmt.Println("updateVoyage: func readConsignment failed to get value")
		t.FailNow()
	}
	if bytes.Compare(getExpectedConsignmentAfterVoyageUpdate(), []byte(res.Payload)) != 0 {
		fmt.Println("updateVoyage: func readConsignment Expected:\n", string(getExpectedConsignmentAfterVoyageUpdate()), "\nActual:\n", string(res.Payload))
		t.FailNow()
	}
}

func checkReadAllConsignmentsOK(t *testing.T, stub *shim.MockStub) {
	res := stub.MockInvoke("1", [][]byte{[]byte("readAllConsignments")})
	if res.Status != shim.OK {
		fmt.Println("func readAllConsignments failed", string(res.Message))
		t.FailNow()
	}
	if res.Payload == nil {
		fmt.Println("func readAllConsignments failed to get value")
		t.FailNow()
	}
	if bytes.Compare(getExpectedConsignments(), []byte(res.Payload)) != 0 {
		fmt.Println("func readAllConsignments Expected:\n", string(getExpectedConsignments()), "\nActual:\n", string(res.Payload))
		t.FailNow()
	}
}

func checkError(t *testing.T, exp string, act string) {
	if strings.Compare(exp, act) != 0 {
		fmt.Println("Unexpected Error! Expecting ", exp, "\n Actual :", act)
		t.FailNow()
	}
}
