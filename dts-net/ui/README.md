# Off-Farm Milk Sample Docket Collections at Pickup (Lion)

## Dockets Collection
[Dockets Repo](https://github.wdf.sap.corp/I047582/Dockets)

# Steps to deploy
1. Clone into SAP WebIDE for SAP Cloud Platform tenant of your choice.
3. Deploy to SAP Cloud Platform
4. Run

# Demo Script / Sample Input Data:
1. Select teh load ID 22 in the aster screen of the app.
2. Press New Intake Tab and input the following:
-- SupplierID: 4711
-- Volume: 815
-- Temperature: 2.3
-- Time: 9:45:55
-- Info: 8671418
-- Sample Bottle ID: 04711815B
3. Press Add
4. Check the details tab whether the new Intake has been added to teh collection. (Sometimes a re-load Load ID 22might be necessary - give some time for blockchain to update, navigate to another loadID 23 and come back to loadID 22 for example)
