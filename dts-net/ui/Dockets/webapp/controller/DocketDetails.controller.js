sap.ui.define([
	"Dockets/controller/BaseController",
	"Dockets/util/localStoreDockets",
	"Dockets/util/bizNetAccessDockets",
	"sap/ui/Device"
], function(
		BaseController, 
		localStoreDockets, 
		bizNetAccessDockets,
		Device
	) {
	"use strict";
	
	return BaseController.extend("Dockets.controller.DocketDetails", {
		
		onInit : function(){
			
			this.getOwnerComponent().getRouter().getRoute("docket").attachPatternMatched(this._onObjectMatched, this);
		},

		_onObjectMatched : function (oEvent) {
			
			this.getOwnerComponent().getModel("Dockets").setProperty("/selectedDocket",{});
			var pId = oEvent.getParameter("arguments").docketId;
			if ( pId === "___new" ) {
				this.getView().byId("__barDocket").setSelectedKey("New");
			} else {
				bizNetAccessDockets.loadDocket(this.getOwnerComponent(), this.getView().getModel("Dockets"), oEvent.getParameter("arguments").docketId);
				this.getView().getModel("Dockets").setProperty("/intakeCollection/items",this.getView().getModel("Dockets").getProperty("/selectedDocket/Intakes"));
			}
		},
		
		addNewIntake : function(){
			
			var oModel = this.getView().getModel("Dockets");
			if ( 
				 oModel.getProperty("/newIntake/SupplierID") 	!== "" ||
				 oModel.getProperty("/newIntake/Volume") 		!== "" ||
				 oModel.getProperty("/newIntake/Time") 			!== "" ||
				 oModel.getProperty("/newIntake/Info") 			!== "" ||
				 oModel.getProperty("/newIntake/SampleBottleID")!== "" 
				) {
				var docketId = bizNetAccessDockets.addNewIntake(this.getOwnerComponent(), oModel);
				bizNetAccessDockets.loadDocket(this.getOwnerComponent(), this.getView().getModel("Dockets"), docketId);
				this._showDetail();
				this.getView().byId("__barDocket").setSelectedKey("Details");
			} else {
				sap.m.MessageToast.show("Please enter valid data in all fields", {});
			}
			this._clearNewIntake();
		},
		
		removeDocket : function(){
			
			var oModel = this.getView().getModel("Dockets");
			bizNetAccessDockets.removeDocket(this.getOwnerComponent(), oModel, oModel.getProperty("/selectedDocket/ID"));
		},		
		
		_showDetail : function() {
			
			var bReplace = !Device.system.phone;
			this.getRouter().navTo("dockets", {}, bReplace);
		},
		
		_clearNewIntake : function(){
			
			var oModel = this.getView().getModel("Dockets");
			oModel.setProperty("/newIntake/SupplierID","");
			oModel.setProperty("/newIntake/Volume","");
			oModel.setProperty("/newIntake/Temperature","");
			oModel.setProperty("/newIntake/Time","");
			oModel.setProperty("/newIntake/Info","");
			oModel.setProperty("/newIntake/SampleBottleID","");
		}
	});

});