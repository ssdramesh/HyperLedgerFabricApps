sap.ui.define([
	"Dockets/controller/BaseController",
	"Dockets/model/modelsBase",
	"Dockets/util/messageProvider",
	"Dockets/util/localStoreDockets",
	"Dockets/util/bizNetAccessDockets",
	"sap/ui/core/routing/History",
	"sap/ui/model/Filter",
	"sap/ui/model/FilterOperator",
	"sap/ui/Device"
], function(
		BaseController, 
		modelsBase, 
		messageProvider,
		localStoreDockets,
		bizNetAccessDockets, 
		History, 
		Filter, 
		FilterOperator, 
		Device
	) {
	"use strict";

	return BaseController.extend("Dockets.controller.DocketsOverview", {

		onInit : function(){
			
			var oList = this.byId("docketsList");
			this._oList = oList;
			this._oListFilterState = {
				aFilter : [],
				aSearch : []
			};
			if ( this.getRunMode().testMode ){
				localStoreDockets.init();
			}		
			if ( typeof this.getOwnerComponent().getModel("Messages") === "undefined" ){
				this.getOwnerComponent().setModel(modelsBase.createMessagesModel(), "Messages");
			}
			messageProvider.init(this.getOwnerComponent().getModel("Messages"));
		},
		
		onAfterRendering: function() {
	
			this.loadEntityMetaData("dockets", this.getModel("Dockets"));
			bizNetAccessDockets.loadAllDockets(this, this.getModel("Dockets"));
		},		
		
		onSelectionChange : function (oEvent) {
			
			this._showDetail(oEvent.getParameter("listItem") || oEvent.getSource());
		},
		
		onAdd : function() {
			
			this.getRouter().navTo("Docket", {DocketId : "___new"});
		},
		
		onRemoveAll : function(){
			
			bizNetAccessDockets.removeAllDockets(this.getOwnerComponent(), this.getOwnerComponent().getModel("Dockets"));          
		},		
		
		onSearch : function(oEvent) {
			
			var sQuery = oEvent.getParameter("query");
			if (sQuery) {
				this._oListFilterState.aSearch = [new Filter("ID", FilterOperator.Contains, sQuery)];
			} else {
				this._oListFilterState.aSearch = [];
			}
			this._applyFilterSearch();			
			var bReplace = !Device.system.phone;
			var oModel = this.getModel("Dockets");
			oModel.setProperty(
				"/selectedDocket",
				_.findWhere(oModel.getProperty("/docketCollection/items"), 
					{
						ID: oModel.getProperty("/searchDocketID")
					},
				this));
			this.getRouter().navTo("docket", {
				docketId : oModel.getProperty("/selectedDocket").ID
			}, bReplace);			
		},
		
		_onToggle : function(){
			
			this.getOwnerComponent().getModel("TestSwitch").setProperty("/testMode",!(this.getView().byId("__buttonToggleDockets").getPressed()));
			var location = this.getRunMode().location;
			messageProvider.addMessage("Success", "Switching Network for Blockchain Data to: ", location, location, 1, "", "http://www.sap.com");
			bizNetAccessDockets.loadAllDockets(this.getOwnerComponent(), this.getOwnerComponent().getModel("Dockets"));
		},		
		
		_showDetail : function(oItem) {
			
			var bReplace = !Device.system.phone;
			var oModel = this.getModel("Dockets");
			this.getRouter().navTo("docket", {
				docketId : oModel.getObject(oItem.getBindingContextPath()).LoadID
			}, bReplace);
		},
		
		_applyFilterSearch : function () {
			var aFilters = this._oListFilterState.aSearch.concat(this._oListFilterState.aFilter),
				oViewModel = this.getModel("Dockets");
			this._oList.getBinding("items").filter(aFilters, "Application");
			if (aFilters.length !== 0) {
				oViewModel.setProperty("/noDataText", this.getResourceBundle().getText("_labelNoDocketsText"));
			}
		},
		
		onMessagePress: function() {
			
			this.onShowMessageDialog("Coffee Network Log");
		},		
		
		onNavBack : function() {
			this.getRouter().navTo("home");
		}		
	});
});