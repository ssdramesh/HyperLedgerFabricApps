sap.ui.define([
	"sap/ui/model/json/JSONModel"
], function(JSONModel) {
	"use strict";

	return {

		createDocketsModel: function(){
			
			var oModel = new JSONModel();
			oModel.setDefaultBindingMode("TwoWay");
			oModel.setData({
				chaincode:{
					serviceUrl:"",
					serviceKey:{},
					entity:{}						
				},
				docketCollection	: {
					cols:[
						{name:"LoadID"},
						{name:"FactoryID"},
						{name:"LoadTemperature"},
						{name:"LastCIPTemperature"},
						{name:"SampleCount"},
						{name:"IntakeCount"},
						{name:"TotalIntakeVolume"}
					],
					items:[]
				},
				intakeCollection:{
					cols:[
						{name:"SupplierID"},
						{name:"Volume"},
						{name:"Temperature"},
						{name:"Time"},
						{name:"Info"},
						{name:"SampleBottleID"}
					],
					items:[]
				},
				selectedDocket	: {},
				newDocket		: {
					LoadID				: "",
					FactoryID		: "",
					Truck: {
						ID: "",
					    DriverID: "",
					    Run: {
					      ID: "",
					      Date: "",
					      StartTime: "",
					      StartKilometerReading: "",
					      EndTime: "",
					      EndKilometerReading: ""
					    }
					},
					CoolBox: {
					    StartTemperature: "",
					    EndTemperature: "",
					    AverageTemperature: ""
					},					
					LoadTemperature			: "",
					LastCIPTemperature	: "",
					SampleCount: "",
					IntakeCount:"",
					TotalIntakeVolume:"",
					Intakes:[]
				},
				newIntake		: {
					SupplierID:"",
					Volume:"",
					Temperature:"",
					Time:"",
					Info:"",
					SampleBottleID: ""
				},
				selectedIntake:{},
				selectedDocketLoadID : "",
				searchDocketLoadID   : ""
			});
			return oModel;			
		}
	};
});