sap.ui.define(function() {
	"use strict";
	
	jQuery.sap.require("jquery.sap.storage");
	var oStorage = jQuery.sap.storage(jQuery.sap.storage.Type.local);
	var docketsDataID = "dockets";
	
	return {
		
		init: function(){

			oStorage.put(docketsDataID,[]);
			oStorage.put(
				docketsDataID, 	
				[
					{
					  "loadID": "22",
					  "factoryID": "616412",
					  "truck": {
					    "ID": "400003",
					    "driverID": "15",
					    "run": {
					      "ID": "616412",
					      "date": "05/26/2018",
					      "startTime": "06:14:41",
					      "startKilometerReading": "11231",
					      "endTime": "09:39:00",
					      "endKilometerReading": "11265"
					    }
					  },
					  "coolBox": {
					    "startTemperature": "0",
					    "endTemperature": "8",
					    "averageTemperature": "4"
					  },
					  "loadTemperature": "4.6",
					  "lastCIPTemperature": "76.7",
					  "sampleCount": "5",
					  "intakeCount": "5",
					  "totalIntakeVolume": "18780",
					  "intakes": [
					    {
					      "supplierID": "421",
					      "volume": "1838",
					      "temperature": "2.7",
					      "time": "06:49:59",
					      "info": "1695747",
					      "sampleBottleID": "04213924"
					    },{
					      "supplierID": "9184",
					      "volume": "2630",
					      "temperature": "3.5",
					      "time": "07:26:15",
					      "info": "131360",
					      "sampleBottleID": "91843926"    	
					    },{
					      "supplierID": "30",
					      "volume": "3587",
					      "temperature": "3.8",
					      "time": "08:26:21",
					      "info": "1695772",
					      "sampleBottleID": "0030392A"    	
					    },{
					      "supplierID": "30",
					      "volume": "4272",
					      "temperature": "3.8",
					      "time": "08:30:37",
					      "info": "131360",
					      "sampleBottleID": "0030392B"    	
					    },{
					      "supplierID": "9109",
					      "volume": "6453",
					      "temperature": "3.3",
					      "time": "08:57:18",
					      "info": "147754",
					      "sampleBottleID": "9109392C"    	
					    }
					  ]
					},
					{
					  "loadID": "24",
					  "factoryID": "516412",
					  "truck": {
					    "ID": "400004",
					    "driverID": "25",
					    "run": {
					      "ID": "516412",
					      "date": "06/26/2018",
					      "startTime": "06:14:41",
					      "startKilometerReading": "11231",
					      "endTime": "09:59:00",
					      "endKilometerReading": "11265"
					    }
					  },
					  "coolBox": {
					    "startTemperature": "0",
					    "endTemperature": "6",
					    "averageTemperature": "3"
					  },
					  "loadTemperature": "3.6",
					  "lastCIPTemperature": "6.7",
					  "sampleCount": "4",
					  "intakeCount": "4",
					  "totalIntakeVolume": "16942",
					  "intakes": [
						{
					      "supplierID": "9184",
					      "volume": "2630",
					      "temperature": "3.5",
					      "time": "07:26:15",
					      "info": "131360",
					      "sampleBottleID": "91843926"    	
					    },{
					      "supplierID": "30",
					      "volume": "3587",
					      "temperature": "3.8",
					      "time": "08:26:21",
					      "info": "1695772",
					      "sampleBottleID": "0030392A"    	
					    },{
					      "supplierID": "30",
					      "volume": "4272",
					      "temperature": "3.8",
					      "time": "08:30:37",
					      "info": "131360",
					      "sampleBottleID": "0030392B"    	
					    },{
					      "supplierID": "9109",
					      "volume": "6453",
					      "temperature": "3.3",
					      "time": "08:57:18",
					      "info": "147754",
					      "sampleBottleID": "9109392C"    	
					    }
					  ]
					}					
				]
			);
		},
		
		getDocketDataID : function(){
			
			return docketsDataID;
		},
		
		getDocketData  : function(){
			
			return oStorage.get("dockets");
		},
		
		put: function(newDocket){
			
			var docketsData = this.getDocketData();
			docketsData.push(newDocket);
			oStorage.put(docketsDataID, docketsData);
		},
		
		addNewIntake: function(id, newIntake){
			
			var docketsData = this.getDocketData();
			var docket = _.findWhere(docketsData,{loadID:id})
			var intakes = docket.intakes;
			intakes.push(newIntake);
			docket.intakes = intakes;
			docket.sampleCount = (parseInt(docket.sampleCount) + 1).toString();
			docket.intakeCount = (parseInt(docket.intakeCount) + 1).toString();
			docket.totalIntakeVolume = (parseInt(docket.totalIntakeVolume) + parseInt(newIntake.volume)).toString();
			this.remove(id);
			this.put(docket);
		},
		
		remove : function (id){
		
			var docketsData = this.getDocketData();
			docketsData = _.without(docketsData,_.findWhere(docketsData,{loadID:id}));
			oStorage.put(docketsDataID, docketsData);
		},
		
		removeAll : function(){
		
			oStorage.put(docketsDataID,[]);	
		},
		
		clearDocketData: function(){
			
			oStorage.put(docketsDataID,[]);
		}
	};
});