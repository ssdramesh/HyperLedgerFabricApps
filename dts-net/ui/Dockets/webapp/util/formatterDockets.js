sap.ui.define(function() {
	"use strict";

	return {

		mapDocketToModel:function(responseData){
			
			return {
				LoadID						: responseData.loadID,
				FactoryID 					: responseData.factoryID,
				TruckID						: responseData.truck.ID,
				DriverID					: responseData.truck.driverID,
				RunID						: responseData.truck.run.ID,
				RunDate						: responseData.truck.run.date,
				RunStartTime				: responseData.truck.run.startTime,
				RunStartKilometerReading	: responseData.truck.run.startKilometerReading,
				RunEndTime					: responseData.truck.run.endTime,
				RunEndKilometerReading		: responseData.truck.run.endKilometerReading,
				CoolBoxStartTemperature		: responseData.coolBox.startTemperature,
				CoolBoxEndTemperature		: responseData.coolBox.endTemperature,
				CoolBoxAverageTemperature	: responseData.coolBox.averageTemperature,
				LoadTemperature				: responseData.loadTemperature,
				LastCIPTemperature			: responseData.lastCIPTemperature,
				SampleCount					: responseData.sampleCount,
				IntakeCount					: responseData.intakeCount,
				TotalIntakeVolume			: responseData.totalIntakeVolume,
				Intakes						: this.mapIntakesToModel(responseData.intakes)
			};
		},
		
		mapIntakeToModel : function(intake) {
			
			return {
				SupplierID		:	intake.supplierID,
				Volume			:	intake.volume,
				Temperature		:	intake.temperature,
				Time			:	intake.time,
				Info			:	intake.info,
				SampleBottleID	:	intake.sampleBottleID
			};
		},
		
		mapIntakesToModel : function(intakes){
			
			var items = [];
			if ( intakes ){
				for (var i = 0; i < intakes.length; i++ ){
					items.push(this.mapIntakeToModel(intakes[i]));
				}
			}
			return items;
		},

		mapDocketsToModel:function(responseData){
			
			var items = [];
			if( responseData ){
				for ( var i = 0; i < responseData.length; i++ ){
					items.push(this.mapDocketToModel(responseData[i]));
				}
			}
			return items;
		},

		mapDocketToChaincode:function(oModel){
			
			return this.mapDocketToLocalStorage(oModel);
		},
		
		mapIntakeToChaincode:function(intake){
			
			return this.mapIntakeToLocalStorage(intake);
		},
		
		mapDocketToLocalStorage : function(oModel){
				
			return {
					"loadID"				:	this.getNewDocketLoadID(oModel),
					"factoryID"				:	oModel.getProperty("/newDocket/FactoryID"),
					"loadTemperature"		:	oModel.getProperty("/newDocket/LoadTemperature"),
					"lastCIPTemperature"	:	oModel.getProperty("/newDocket/LastCIPTemperature"),
					"sampleCount"			:	oModel.getProperty("/newDocket/SampleCount"),
					"intakeCount"			:	oModel.getProperty("/newDocket/IntakeCount"),
					"totalIntakeVolume"		:	oModel.getProperty("/newDocket/TotalIntakeVolume"),
					"intakes"				:	this.mapIntakesToLocalStorage(oModel.getProperty("/newDocket/Intakes"))
			};
		},
		
		mapIntakeToLocalStorage : function(intake){
			
			return {
				"supplierID"		:	intake.SupplierID,	
				"volume"			:	intake.Volume,
				"temperature"		:	intake.Temperature,
				"time"				:	intake.Time,
				"info"				:	intake.Info,
				"sampleBottleID"	:	intake.SampleBottleID	
			};
		},
		
		mapIntakesToLocalStorage : function(intakes) {
			
			var items = [];
			if( intakes ){
				for ( var i = 0; i < intakes.length; i++ ){
					items.push(this.mapIntakeToLocalStorage(intakes[i]));
				}
			}
			return items;			
		},
		
		getNewDocketLoadID:function(oModel){

		    if ( typeof oModel.getProperty("/newDocket/LoadID") === "undefined" ||
		    		oModel.getProperty("/newDocket/LoadID") === ""
		    	){
			    var iD = "LOD";
			    var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
			    for( var i = 0; i < 8; i++ ){
			        iD += possible.charAt(Math.floor(Math.random() * possible.length));
			    }
		    } else {
				iD = oModel.getProperty("/newDocket/LoadID");
			}
			oModel.setProperty("/newDocket/LoadID",iD);
		    return iD;
		}
	};
});