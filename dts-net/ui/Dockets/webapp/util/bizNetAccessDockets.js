sap.ui.define([
	"Dockets/util/restBuilder",
	"Dockets/util/formatterDockets",
	"Dockets/util/localStoreDockets"
], function(
				restBuilder,
				formatterDockets, 
				localStoreDockets
			) {
	"use strict";

	return {

		loadAllDockets:function(oComponent, oModel) {

			if ( !( oComponent.getModel("TestSwitch").getProperty("/testMode") ) ){
				restBuilder.execute(
					oModel.getProperty("/chaincode"), 
					"readAll",
					[],
					function(responseData){
						oModel.setProperty(
							"/docketCollection/items",
							formatterDockets.mapDocketsToModel(responseData)
						);
					});
			} else {
				oModel.setProperty(
					"/docketCollection/items",
					formatterDockets.mapDocketsToModel(localStoreDockets.getDocketData())
				);
			}
		},

		loadDocket:function(oComponent, oModel, selectedDocketID){

			// oModel.setProperty(
			// 	"/selectedDocket",
			// 	_.findWhere(oModel.getProperty("/docketCollection/items"), 
			// 		{
			// 			LoadID: selectedDocketID
			// 		},
			// 	this));
			if ( !( oComponent.getModel("TestSwitch").getProperty("/testMode") ) ){	
				restBuilder.execute(
					oModel.getProperty("/chaincode"), 
					"read",
					selectedDocketID,
					function(responseData){
						oModel.setProperty(
							"/selectedDocket",
							formatterDockets.mapDocketToModel(responseData)
						);
					},
					selectedDocketID
				);
			} else {
				oModel.setProperty(
					"/selectedDocket",
					formatterDockets.mapDocketToModel(_.findWhere(localStoreDockets.getDocketData(), 
					{
						loadID: selectedDocketID
					},
				this))
				);
			}			
		},

		addNewDocket:function(oComponent, oModel){
			
			if ( !( oComponent.getModel("TestSwitch").getProperty("/testMode") ) ){
				restBuilder.execute(
					oModel.getProperty("/chaincode"), 
					"addNew",
					formatterDockets.mapDocketToChaincode(oModel)
				);
			}  else {
				localStoreDockets.put(formatterDockets.mapDocketToLocalStorage(oModel));
			}
			this.loadAllDockets(oComponent, oModel);
			return oModel.getProperty("/newDocket/ID");
		},
		
		removeDocket : function(oComponent, oModel, DocketID){
			
			if ( !( oComponent.getModel("TestSwitch").getProperty("/testMode") ) ) {
				restBuilder.execute(
					oModel.getProperty("/chaincode"), 
					"remove",
					[DocketID]
				);
			} else {
				localStoreDockets.remove(DocketID);
			}
			this.loadAllDockets(oComponent, oModel);
			return true;
		},
		
		removeAllDockets : function(oComponent, oModel){
			
			if ( !( oComponent.getModel("TestSwitch").getProperty("/testMode") ) ) {
				restBuilder.execute(
					oModel.getProperty("/chaincode"), 
					"removeAll",
					[]
				);
			} else {
				localStoreDockets.removeAll();
			}
			this.loadAllDockets(oComponent, oModel);
			oModel.setProperty("/selectedDocket",{});
			return true;
		},
		
		addNewIntake : function(oComponent, oModel){
			
			if ( !( oComponent.getModel("TestSwitch").getProperty("/testMode") ) ) {
				restBuilder.execute(
					oModel.getProperty("/chaincode"), 
					"addNewIntake",
					formatterDockets.mapIntakeToChaincode(oModel.getProperty("/newIntake")),
					function(){},					
					oModel.getProperty("/selectedDocket/LoadID")
				);
			} else {
				localStoreDockets.addNewIntake(oModel.getProperty("/selectedDocket/LoadID"), formatterDockets.mapIntakeToLocalStorage(oModel.getProperty("/newIntake")));
			}
			this.loadAllDockets(oComponent, oModel);
			return oModel.getProperty("/selectedDocket/LoadID");
		}		
	};	
});