# What's in here?
This is a demo for illustrating the collection of sample docktes and milk batches by trucker at the offsite Farms to enable track and trace for quality and quantity attributes of milk produce in a cost effective manner using blockchain.

Use-case Cluster: Blockchain for provenance / track and trace in Agri / Food Supply Chains

# Context
Off-Farm Milk Samples (To-Be Process)
![Use Case](./Model/Usecase.png)

# Business Network Specification
The persona for teh demo is the trucker collecting intakes of milk at several farms along the way of a single run to complete the load and unload it at the milk processing unit.

# Model
![Dairy Docket Data Model](./Model/dairyDocketModel.png)

# Deployed Systems
Hyperledger Fabric service on SAP Cloud Foundry (Canary)
[Hyperledger Node](https://account.int.sap.hana.ondemand.com/cockpit#/globalaccount/d0fb539e-df71-473b-a191-a7d84af02279/subaccount/140a46a4-1afa-4d0b-842b-bc4a8c7b40bf/org/88a15bfa-408c-4e9f-982e-d0599113c004/space/ef2b3baf-f098-4726-96c0-37af668b3803/service/46f22deb-5a2f-46d3-b116-67308b7bb801/instance/6385655a-4646-49cd-bac2-d6e0535cafa5/referencingApps)
[Channel exposed by the HLF Node](https://account.int.sap.hana.ondemand.com/cockpit#/globalaccount/d0fb539e-df71-473b-a191-a7d84af02279/subaccount/140a46a4-1afa-4d0b-842b-bc4a8c7b40bf/org/88a15bfa-408c-4e9f-982e-d0599113c004/space/ef2b3baf-f098-4726-96c0-37af668b3803/service/46f22deb-5a2f-46d3-b116-67308b7bb801/instance/6385655a-4646-49cd-bac2-d6e0535cafa5/referencingApps)
[Development Channel Dashboard (for chaincode)](https://hyperledger-fabric-dashboard.cfapps.sap.hana.ondemand.com/88a15bfa-408c-4e9f-982e-d0599113c004/cdef3f85-8ffd-4b13-8d86-9669e17484cc/72fbbf52-0fc5-4039-8188-f990b59deaf3/channel)

# Demo Script(s)

Prerequisites: SAP-Corporate LAN / WiFi (at SAP site) or F5 BigEdge logged in (remote)

A short script for demo.  The demo URL is [here](https://dockets-g85556318.dispatcher.jp1.hana.ondemand.com/webapp/index.html?hc_reset).
1. Click on the Load ID: 22 on the Master list on the left-hand side.
2. Scroll through the details on the tab - Details
3. Scroll down to Intakes and show how many existing intakes are present
4. Switch to tab "New Intake"
5. Fill out the following details: SupplierID: 4711, Volume: 815, Temperature: 2.7, Time: 09:45:55, Info: 7567412, SampleBottleID: 04711815A
6. Click the button - "Add"
7. Switch back to the "Details" view and check if the new Intake is added.  Press Browser Refresh if teh new Intake is not shown (needs a while for blockchain update)

# Published Links
No links are published yet in Innobook.

# Contact(s):
Ramesh Suraparaju
