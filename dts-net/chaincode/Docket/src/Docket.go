package main

import (
	"encoding/json"
	"errors"
	"fmt"
	"strconv"

	"github.com/hyperledger/fabric/core/chaincode/shim"
	"github.com/hyperledger/fabric/protos/peer"
)

var logger = shim.NewLogger("CLDChaincode")

//Run (dataType)
type Run struct {
	ID                    string `json:"ID"`
	Date                  string `json:"date"`
	StartTime             string `json:"startTime"`
	StartKilometerReading string `json:"startKilometerReading"`
	EndTime               string `json:"endTime"`
	EndKilometerReading   string `json:"endKilometerReading"`
}

//CoolBox  (dataType)
type CoolBox struct {
	StartTemperature   string `json:"startTemperature"`
	EndTemperature     string `json:"endTemperature"`
	AverageTemperature string `json:"averageTemperature"`
}

//Truck (dataType)
type Truck struct {
	ID       string `json:"ID"`
	DriverID string `json:"driverID"`
	Run      Run    `json:"run"`
}

//Intake (dataType)
type Intake struct {
	SupplierID     string `json:"supplierID"`
	Volume         string `json:"volume"`
	Temperature    string `json:"temperature"`
	Time           string `json:"time"`
	Info           string `json:"info"`
	SampleBottleID string `json:"sampleBottleID"`
}

//DocketAsset - Chaincode for asset Docket
type DocketAsset struct {
}

//Docket - Details of the asset type Docket
type Docket struct {
	LoadID             string   `json:"loadID"`
	FactoryID          string   `json:"factoryID"`
	Truck              Truck    `json:"truck"`
	CoolBox            CoolBox  `json:"coolBox"`
	LoadTemperature    string   `json:"loadTemperature"`
	LastCIPTemperature string   `json:"lastCIPTemperature"`
	SampleCount        string   `json:"sampleCount"`
	IntakeCount        string   `json:"intakeCount"`
	TotalIntakeVolume  string   `json:"totalIntakeVolume"`
	Intakes            []Intake `json:"intakes"`
}

//DocketIDIndex - Index on IDs for retrieval all Dockets
type DocketIDIndex struct {
	IDs []string `json:"IDs"`
}

func main() {
	err := shim.Start(new(DocketAsset))
	if err != nil {
		fmt.Printf("Error starting DocketAsset chaincode function main(): %s", err)
	} else {
		fmt.Printf("Starting DocketAsset chaincode function main() executed successfully")
	}
}

//Init - The chaincode Init function: No  arguments, only initializes a ID array as Index for retrieval of all Dockets
func (dkt *DocketAsset) Init(stub shim.ChaincodeStubInterface) peer.Response {
	var docketIDIndex DocketIDIndex
	bytes, _ := json.Marshal(docketIDIndex)
	stub.PutState("docketIDIndex", bytes)
	return shim.Success(nil)
}

//Invoke - The chaincode Invoke function:
func (dkt *DocketAsset) Invoke(stub shim.ChaincodeStubInterface) peer.Response {
	function, args := stub.GetFunctionAndParameters()
	logger.Debug("function: ", function)
	switch function {
	case "addNewDocket":
		return dkt.addNewDocket(stub, args)
	case "removeDocket":
		return dkt.removeDocket(stub, args[0])
	case "removeAllDockets":
		return dkt.removeAllDockets(stub)
	case "readDocket":
		return dkt.readDocket(stub, args[0])
	case "readAllDockets":
		return dkt.readAllDockets(stub)
	case "addNewIntake":
		return dkt.addNewIntake(stub, args)
	default:
		return shim.Error("Received unknown function invocation")
	}
}

//Invoke Route: addNewDocket
func (dkt *DocketAsset) addNewDocket(stub shim.ChaincodeStubInterface, args []string) peer.Response {
	docket, err := getDocketFromArgs(args)
	if err != nil {
		return shim.Error("Docket Data is Corrupted")
	}
	record, err := stub.GetState(docket.LoadID)
	if record != nil {
		return shim.Error("This Docket already exists: " + docket.LoadID)
	}
	_, err = dkt.saveDocket(stub, docket)
	if err != nil {
		return shim.Error(err.Error())
	}
	_, err = dkt.updateDocketIDIndex(stub, docket)
	if err != nil {
		return shim.Error(err.Error())
	}
	return shim.Success(nil)
}

//Invoke Route: removeDocket
func (dkt *DocketAsset) removeDocket(stub shim.ChaincodeStubInterface, docketID string) peer.Response {
	_, err := dkt.deleteDocket(stub, docketID)
	if err != nil {
		return shim.Error(err.Error())
	}
	_, err = dkt.deleteDocketIDIndex(stub, docketID)
	if err != nil {
		return shim.Error(err.Error())
	}
	return shim.Success(nil)
}

//Invoke Route: removeAllDockets
func (dkt *DocketAsset) removeAllDockets(stub shim.ChaincodeStubInterface) peer.Response {
	var docketStructIDs DocketIDIndex
	bytes, err := stub.GetState("docketIDIndex")
	if err != nil {
		return shim.Error("removeAllDockets: Error getting docketIDIndex array")
	}
	err = json.Unmarshal(bytes, &docketStructIDs)
	if err != nil {
		return shim.Error("removeAllDockets: Error unmarshalling docketIDIndex array JSON")
	}
	if len(docketStructIDs.IDs) == 0 {
		return shim.Error("removeAllDockets: No dockets to remove")
	}
	for _, docketStructID := range docketStructIDs.IDs {
		_, err = dkt.deleteDocket(stub, docketStructID)
		if err != nil {
			return shim.Error("Failed to remove Docket with ID: " + docketStructID)
		}
		_, err = dkt.deleteDocketIDIndex(stub, docketStructID)
		if err != nil {
			return shim.Error(err.Error())
		}
	}
	dkt.initHolder(stub)
	return shim.Success(nil)
}

//Query Route: readDocket
func (dkt *DocketAsset) readDocket(stub shim.ChaincodeStubInterface, docketID string) peer.Response {
	docketAsByteArray, err := dkt.retrieveDocket(stub, docketID)
	if err != nil {
		return shim.Error(err.Error())
	}
	return shim.Success(docketAsByteArray)
}

//Query Route: readAllDockets
func (dkt *DocketAsset) readAllDockets(stub shim.ChaincodeStubInterface) peer.Response {
	var docketIDs DocketIDIndex
	bytes, err := stub.GetState("docketIDIndex")
	if err != nil {
		return shim.Error("readAllDockets: Error getting docketIDIndex array")
	}
	err = json.Unmarshal(bytes, &docketIDs)
	if err != nil {
		return shim.Error("readAllDockets: Error unmarshalling docketIDIndex array JSON")
	}
	result := "["

	var docketAsByteArray []byte

	for _, docketID := range docketIDs.IDs {
		docketAsByteArray, err = dkt.retrieveDocket(stub, docketID)
		if err != nil {
			return shim.Error("Failed to retrieve docket with ID: " + docketID)
		}
		result += string(docketAsByteArray) + ","
	}
	if len(result) == 1 {
		result = "[]"
	} else {
		result = result[:len(result)-1] + "]"
	}
	return shim.Success([]byte(result))
}

//Invoke Route: addNewIntake
func (dkt *DocketAsset) addNewIntake(stub shim.ChaincodeStubInterface, args []string) peer.Response {

	var docket Docket
	var intake Intake

	docketAsByteArray, err := dkt.retrieveDocket(stub, args[0])
	if err != nil {
		return shim.Error(err.Error())
	}

	err = Unmarshal(docketAsByteArray, &docket)
	if err != nil {
		return shim.Error(err.Error())
	}

	if !Valid(args[1]) {
		return shim.Error("addNewintake: Invalid input json for adding new intake")
	}

	err = Unmarshal([]byte(args[1]), &intake)
	if err != nil {
		return shim.Error(err.Error())
	}

	docket.Intakes = append(docket.Intakes, intake)
	sampleCount, _ := strconv.Atoi(docket.SampleCount)
	intakeCount, _ := strconv.Atoi(docket.IntakeCount)
	totalIntakeVolume, _ := strconv.Atoi(docket.TotalIntakeVolume)
	volume, _ := strconv.Atoi(intake.Volume)

	docket.SampleCount = fmt.Sprint(sampleCount + 1)
	docket.IntakeCount = fmt.Sprint(intakeCount + 1)
	docket.TotalIntakeVolume = fmt.Sprint(totalIntakeVolume + volume)

	_, err = dkt.saveDocket(stub, docket)
	if err != nil {
		return shim.Error(err.Error())
	}
	return shim.Success(nil)
}

//Helper: Save DocketAsset
func (dkt *DocketAsset) saveDocket(stub shim.ChaincodeStubInterface, docket Docket) (bool, error) {
	bytes, err := json.Marshal(docket)
	if err != nil {
		return false, errors.New("Error converting docket record JSON")
	}
	err = stub.PutState(docket.LoadID, bytes)
	if err != nil {
		return false, errors.New("Error storing Docket record")
	}
	return true, nil
}

//Helper: delete DocketAsset
func (dkt *DocketAsset) deleteDocket(stub shim.ChaincodeStubInterface, docketID string) (bool, error) {
	_, err := dkt.retrieveDocket(stub, docketID)
	if err != nil {
		return false, errors.New("Docket with ID: " + docketID + " not found")
	}
	err = stub.DelState(docketID)
	if err != nil {
		return false, errors.New("Error deleting Docket record")
	}
	return true, nil
}

//Helper: Update docket Holder - updates Index
func (dkt *DocketAsset) updateDocketIDIndex(stub shim.ChaincodeStubInterface, docket Docket) (bool, error) {
	var docketIDs DocketIDIndex
	bytes, err := stub.GetState("docketIDIndex")
	if err != nil {
		return false, errors.New("updateDocketIDIndex: Error getting docketIDIndex array Index from state")
	}
	err = json.Unmarshal(bytes, &docketIDs)
	if err != nil {
		return false, errors.New("updateDocketIDIndex: Error unmarshalling docketIDIndex array JSON")
	}
	docketIDs.IDs = append(docketIDs.IDs, docket.LoadID)
	bytes, err = json.Marshal(docketIDs)
	if err != nil {
		return false, errors.New("updateDocketIDIndex: Error marshalling new docket ID")
	}
	err = stub.PutState("docketIDIndex", bytes)
	if err != nil {
		return false, errors.New("updateDocketIDIndex: Error storing new docket ID in docketIDIndex (Index)")
	}
	return true, nil
}

//Helper: delete ID from docketStruct Holder
func (dkt *DocketAsset) deleteDocketIDIndex(stub shim.ChaincodeStubInterface, docketID string) (bool, error) {
	var docketStructIDs DocketIDIndex
	bytes, err := stub.GetState("docketIDIndex")
	if err != nil {
		return false, errors.New("deleteDocketIDIndex: Error getting docketIDIndex array Index from state")
	}
	err = json.Unmarshal(bytes, &docketStructIDs)
	if err != nil {
		return false, errors.New("deleteDocketIDIndex: Error unmarshalling docketIDIndex array JSON")
	}
	docketStructIDs.IDs, err = deleteKeyFromStringArray(docketStructIDs.IDs, docketID)
	if err != nil {
		return false, errors.New(err.Error())
	}
	bytes, err = json.Marshal(docketStructIDs)
	if err != nil {
		return false, errors.New("deleteDocketIDIndex: Error marshalling new docketStruct ID")
	}
	err = stub.PutState("docketIDIndex", bytes)
	if err != nil {
		return false, errors.New("deleteDocketIDIndex: Error storing new docketStruct ID in docketIDIndex (Index)")
	}
	return true, nil
}

//Helper: Initialize truck ID Holder //change template
func (dkt *DocketAsset) initHolder(stub shim.ChaincodeStubInterface) bool {
	var docketIDIndex DocketIDIndex
	bytes, _ := json.Marshal(docketIDIndex)
	stub.DelState("docketIDIndex")
	stub.PutState("docketIDIndex", bytes)
	return true
}

//deleteKeyFromArray
func deleteKeyFromStringArray(array []string, key string) (newArray []string, err error) {
	for _, entry := range array {
		if entry != key {
			newArray = append(newArray, entry)
		}
	}
	if len(newArray) == len(array) {
		return newArray, errors.New("Specified Key: " + key + " not found in Array")
	}
	return newArray, nil
}

//Helper: Retrieve purchaser
func (dkt *DocketAsset) retrieveDocket(stub shim.ChaincodeStubInterface, docketID string) ([]byte, error) {
	var docket Docket
	var docketAsByteArray []byte
	bytes, err := stub.GetState(docketID)
	if err != nil {
		return docketAsByteArray, errors.New("retrieveDocket: Error retrieving docket with ID: " + docketID)
	}
	err = json.Unmarshal(bytes, &docket)
	if err != nil {
		return docketAsByteArray, errors.New("retrieveDocket: Corrupt docket record " + string(bytes))
	}
	docketAsByteArray, err = json.Marshal(docket)
	if err != nil {
		return docketAsByteArray, errors.New("readDocket: Invalid docket Object - Not a  valid JSON")
	}
	return docketAsByteArray, nil
}

//getDocketFromArgs - construct a docket structure from string array of arguments
func getDocketFromArgs(args []string) (docket Docket, err error) {

	if !Valid(args[0]) {
		return docket, errors.New("Invalid json")
	}

	err = Unmarshal([]byte(args[0]), &docket)
	if err != nil {
		return docket, err
	}
	return docket, nil

}
