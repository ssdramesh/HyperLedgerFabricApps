package main

import (
	"bytes"
	"encoding/json"
	"fmt"
	"strings"
	"testing"

	"github.com/hyperledger/fabric/core/chaincode/shim"
)

//TestDocketAsset_Init
func TestDocketAsset_Init(t *testing.T) {
	docket := new(DocketAsset)
	stub := shim.NewMockStub("docket", docket)
	checkInit(t, stub, [][]byte{[]byte("init")})
	checkState(t, stub, "docketIDIndex", getExpectedDocketIDIndex("init"))
}

//TestDocketAsset_InvokeUnknownFunction
func TestDocketAsset_InvokeUnknownFunction(t *testing.T) {
	docket := new(DocketAsset)
	stub := shim.NewMockStub("docket", docket)
	checkInit(t, stub, [][]byte{[]byte("init")})
	checkInvokeUnknownFunction(t, stub, [][]byte{[]byte("myFunction"), []byte("docType:Asset")})
}

//TestDocketAsset_Invoke_addNewDocket
func TestDocketAsset_Invoke_addNewDocketOK(t *testing.T) {
	docket := new(DocketAsset)
	stub := shim.NewMockStub("docket", docket)
	checkInit(t, stub, [][]byte{[]byte("init")})
	checkInvoke(t, stub, getFirstDocketAssetForTesting())
	newDocketID := "LOD1001"
	checkState(t, stub, newDocketID, getNewDocketExpected())
	checkState(t, stub, "docketIDIndex", getExpectedDocketIDIndex("addNewDocket"))
}

//TestDocketAsset_Invoke_addNewDocket
func TestDocketAsset_Invoke_addNewDocketDuplicate(t *testing.T) {
	docket := new(DocketAsset)
	stub := shim.NewMockStub("docket", docket)
	checkInit(t, stub, [][]byte{[]byte("init")})
	checkInvoke(t, stub, getFirstDocketAssetForTesting())
	newDocketID := "LOD1001"
	checkState(t, stub, newDocketID, getNewDocketExpected())
	checkState(t, stub, "docketIDIndex", getExpectedDocketIDIndex("addNewDocket"))
	res := stub.MockInvoke("1", getFirstDocketAssetForTesting())
	if res.Status != shim.OK {
		checkError(t, "This Docket already exists: LOD1001", res.Message)
	} else {
		fmt.Println("Error was expected, but not raised")
		t.FailNow()
	}
}

//TestDocketAsset_Invoke_removeDocketOK  //change template
func TestDocketAsset_Invoke_removeDocketOK(t *testing.T) {
	docket := new(DocketAsset)
	stub := shim.NewMockStub("docket", docket)
	checkInit(t, stub, [][]byte{[]byte("init")})
	checkInvoke(t, stub, getFirstDocketAssetForTesting())
	checkInvoke(t, stub, getSecondDocketAssetForTesting())
	checkReadAllDocketsOK(t, stub)
	checkState(t, stub, "docketIDIndex", getExpectedDocketIDIndex("beforeRemoveDocket"))
	checkInvoke(t, stub, getRemoveSecondDocketAssetForTesting())
	remainingDocketID := "LOD1001"
	checkReadDocketOK(t, stub, remainingDocketID)
	checkState(t, stub, "docketIDIndex", getExpectedDocketIDIndex("afterRemoveDocket"))
}

//TestDocketAsset_Invoke_removeDocketNOK  //change template
func TestDocketAsset_Invoke_removeDocketNOK(t *testing.T) {
	docket := new(DocketAsset)
	stub := shim.NewMockStub("docket", docket)
	checkInit(t, stub, [][]byte{[]byte("init")})
	checkInvoke(t, stub, getFirstDocketAssetForTesting())
	firstDocketID := "LOD1001"
	checkReadDocketOK(t, stub, firstDocketID)
	checkState(t, stub, "docketIDIndex", getExpectedDocketIDIndex("addNewDocket"))
	res := stub.MockInvoke("1", getRemoveSecondDocketAssetForTesting())
	if res.Status != shim.OK {
		checkError(t, "Docket with ID: "+"LOD1002"+" not found", res.Message)
	} else {
		fmt.Println("Error was expected, but not raised")
		t.FailNow()
	}
	checkState(t, stub, "docketIDIndex", getExpectedDocketIDIndex("addNewDocket"))
}

//TestDocketAsset_Invoke_removeAllDocketsOK  //change template
func TestDocketAsset_Invoke_removeAllDocketsOK(t *testing.T) {
	docket := new(DocketAsset)
	stub := shim.NewMockStub("docket", docket)
	checkInit(t, stub, [][]byte{[]byte("init")})
	checkInvoke(t, stub, getFirstDocketAssetForTesting())
	checkInvoke(t, stub, getSecondDocketAssetForTesting())
	checkReadAllDocketsOK(t, stub)
	checkState(t, stub, "docketIDIndex", getExpectedDocketIDIndex("beforeRemoveDocket"))
	checkInvoke(t, stub, getRemoveAllDocketAssetsForTesting())
	checkState(t, stub, "docketIDIndex", getExpectedDocketIDIndex(""))
}

//TestDocketAsset_Invoke_removeDocketNOK  //change template
func TestDocketAsset_Invoke_removeAllDocketsNOK(t *testing.T) {
	docket := new(DocketAsset)
	stub := shim.NewMockStub("docket", docket)
	checkInit(t, stub, [][]byte{[]byte("init")})
	res := stub.MockInvoke("1", getRemoveAllDocketAssetsForTesting())
	if res.Status != shim.OK {
		checkError(t, "removeAllDockets: No dockets to remove", res.Message)
	} else {
		fmt.Println("Error was expected, but not raised")
		t.FailNow()
	}
	checkState(t, stub, "docketIDIndex", getExpectedDocketIDIndex(""))
}

//TestDocketAsset_Query_readDocket
func TestDocketAsset_Query_readDocket(t *testing.T) {
	docket := new(DocketAsset)
	stub := shim.NewMockStub("docket", docket)
	checkInit(t, stub, [][]byte{[]byte("init")})
	docketID := "LOD1001"
	checkInvoke(t, stub, getFirstDocketAssetForTesting())
	checkReadDocketOK(t, stub, docketID)
	checkReadDocketNOK(t, stub, "")
}

//TestDocketAsset_Query_readAllDockets
func TestDocketAsset_Query_readAllDockets(t *testing.T) {
	docket := new(DocketAsset)
	stub := shim.NewMockStub("docket", docket)
	checkInit(t, stub, [][]byte{[]byte("init")})
	checkInvoke(t, stub, getFirstDocketAssetForTesting())
	checkInvoke(t, stub, getSecondDocketAssetForTesting())
	checkReadAllDocketsOK(t, stub)
}

//TestDocketAsset_Invoke_addNewIntake
func TestDocketAsset_Invoke_addNewIntakeOK(t *testing.T) {
	docket := new(DocketAsset)
	stub := shim.NewMockStub("docket", docket)
	checkInit(t, stub, [][]byte{[]byte("init")})
	checkInvoke(t, stub, getFirstDocketAssetForTesting())
	newDocketID := "LOD1001"
	checkState(t, stub, newDocketID, getNewDocketExpected())
	checkState(t, stub, "docketIDIndex", getExpectedDocketIDIndex("addNewDocket"))
	checkInvoke(t, stub, getNewIntakeForTesting())
	checkState(t, stub, newDocketID, getDocketWithNewIntakeExpected())
}

/*
*
*	Helper Functions
*
 */
//Get first DocketAsset for testing
func getFirstDocketAssetForTesting() [][]byte {
	return [][]byte{[]byte("addNewDocket"),
		[]byte("{\"loadID\":\"LOD1001\",\"factoryID\":\"FAC1001\",\"truck\":{\"ID\":\"TRU1001\",\"driverID\":\"DRV1001\",\"run\":{\"ID\":\"RUN1001\",\"Date\":\"06/21/2018\",\"startTime\":\"06:00:00\",\"startKilometerReading\":\"100\",\"endTime\":\"09:35:00\",\"endKilometerReading\":\"200\"}},\"coolBox\":{\"startTemperature\":\"0\",\"endTemperature\":\"0\",\"averageTemperature\":\"0\"},\"loadTemperature\":\"0\",\"lastCIPTemperature\":\"0\",\"sampleCount\":\"2\",\"intakeCount\":\"2\",\"totalIntakeVolume\":\"50\",\"intakes\":[{\"supplierID\":\"SUP1001\",\"volume\":\"25\",\"temperature\":\"0\",\"time\":\"07:00:00\",\"info\":\"4711\",\"sampleBottleID\":\"0815\"},{\"supplierID\":\"SUP1002\",\"volume\":\"25\",\"temperature\":\"0\",\"time\":\"09:00:00\",\"info\":\"4712\",\"sampleBottleID\":\"0816\"}]}")}
}

//Get second DocketAsset for testing
func getSecondDocketAssetForTesting() [][]byte {
	return [][]byte{[]byte("addNewDocket"),
		[]byte("{\"loadID\":\"LOD1002\",\"factoryID\":\"FAC1002\",\"truck\":{\"ID\":\"TRU1002\",\"driverID\":\"DRV1002\",\"run\":{\"ID\":\"RUN1002\",\"Date\":\"06/20/2018\",\"startTime\":\"07:00:00\",\"startKilometerReading\":\"200\",\"endTime\":\"10:35:00\",\"endKilometerReading\":\"300\"}},\"coolBox\":{\"startTemperature\":\"0\",\"endTemperature\":\"0\",\"averageTemperature\":\"0\"},\"loadTemperature\":\"0\",\"lastCIPTemperature\":\"0\",\"sampleCount\":\"2\",\"intakeCount\":\"2\",\"totalIntakeVolume\":\"100\",\"intakes\":[{\"supplierID\":\"SUP1003\",\"volume\":\"50\",\"temperature\":\"0\",\"time\":\"08:00:00\",\"info\":\"4713\",\"sampleBottleID\":\"0817\"},{\"supplierID\":\"SUP1004\",\"volume\":\"50\",\"temperature\":\"0\",\"time\":\"10:00:00\",\"info\":\"4714\",\"sampleBottleID\":\"0818\"}]}")}
}

//Get remove second DocketAsset for testing //change template
func getRemoveSecondDocketAssetForTesting() [][]byte {
	return [][]byte{[]byte("removeDocket"),
		[]byte("LOD1002")}
}

//Get remove all DocketAssets for testing //change template
func getRemoveAllDocketAssetsForTesting() [][]byte {
	return [][]byte{[]byte("removeAllDockets")}
}

//Get new Intake for getVoyageForTesting
func getNewIntakeForTesting() [][]byte {
	return [][]byte{[]byte("addNewIntake"),
		[]byte("LOD1001"),
		[]byte("{\"supplierID\":\"SUP1005\",\"volume\":\"25\",\"temperature\":\"0\",\"time\":\"09:00:00\",\"info\":\"4715\",\"sampleBottleID\":\"0819\"}")}
}

//Get an expected value for testing
func getNewDocketExpected() []byte {

	var coolBox CoolBox
	var run Run
	var truck Truck
	var intake Intake
	var intakes []Intake
	var docket Docket

	docket.LoadID = "LOD1001"
	docket.FactoryID = "FAC1001"
	run.ID = "RUN1001"
	run.Date = "06/21/2018"
	run.StartTime = "06:00:00"
	run.StartKilometerReading = "100"
	run.EndTime = "09:35:00"
	run.EndKilometerReading = "200"
	truck.ID = "TRU1001"
	truck.DriverID = "DRV1001"
	truck.Run = run
	docket.Truck = truck
	coolBox.StartTemperature = "0"
	coolBox.EndTemperature = "0"
	coolBox.AverageTemperature = "0"
	docket.CoolBox = coolBox
	docket.LoadTemperature = "0"
	docket.LastCIPTemperature = "0"
	docket.SampleCount = "2"
	docket.IntakeCount = "2"
	docket.TotalIntakeVolume = "50"

	intakes = nil

	intake.SupplierID = "SUP1001"
	intake.Volume = "25"
	intake.Temperature = "0"
	intake.Time = "07:00:00"
	intake.Info = "4711"
	intake.SampleBottleID = "0815"
	intakes = append(intakes, intake)

	intake.SupplierID = "SUP1002"
	intake.Volume = "25"
	intake.Temperature = "0"
	intake.Time = "09:00:00"
	intake.Info = "4712"
	intake.SampleBottleID = "0816"
	intakes = append(intakes, intake)

	docket.Intakes = intakes

	docketJSON, err := json.Marshal(docket)
	if err != nil {
		fmt.Println("Error converting a Docket record to JSON")
		return nil
	}
	return []byte(docketJSON)
}

//Get expected values of Dockets for testing
func getExpectedDockets() []byte {

	var coolBox CoolBox
	var run Run
	var truck Truck
	var intake Intake
	var intakes []Intake

	var dockets []Docket
	var docket Docket

	docket.LoadID = "LOD1001"
	docket.FactoryID = "FAC1001"
	run.ID = "RUN1001"
	run.Date = "06/21/2018"
	run.StartTime = "06:00:00"
	run.StartKilometerReading = "100"
	run.EndTime = "09:35:00"
	run.EndKilometerReading = "200"
	truck.ID = "TRU1001"
	truck.DriverID = "DRV1001"
	truck.Run = run
	docket.Truck = truck
	coolBox.StartTemperature = "0"
	coolBox.EndTemperature = "0"
	coolBox.AverageTemperature = "0"
	docket.CoolBox = coolBox
	docket.LoadTemperature = "0"
	docket.LastCIPTemperature = "0"
	docket.SampleCount = "2"
	docket.IntakeCount = "2"
	docket.TotalIntakeVolume = "50"

	intakes = nil

	intake.SupplierID = "SUP1001"
	intake.Volume = "25"
	intake.Temperature = "0"
	intake.Time = "07:00:00"
	intake.Info = "4711"
	intake.SampleBottleID = "0815"
	intakes = append(intakes, intake)

	intake.SupplierID = "SUP1002"
	intake.Volume = "25"
	intake.Temperature = "0"
	intake.Time = "09:00:00"
	intake.Info = "4712"
	intake.SampleBottleID = "0816"
	intakes = append(intakes, intake)

	docket.Intakes = intakes
	dockets = append(dockets, docket)

	docket.LoadID = "LOD1002"
	docket.FactoryID = "FAC1002"
	run.ID = "RUN1002"
	run.Date = "06/20/2018"
	run.StartTime = "07:00:00"
	run.StartKilometerReading = "200"
	run.EndTime = "10:35:00"
	run.EndKilometerReading = "300"
	truck.ID = "TRU1002"
	truck.DriverID = "DRV1002"
	truck.Run = run
	docket.Truck = truck
	coolBox.StartTemperature = "0"
	coolBox.EndTemperature = "0"
	coolBox.AverageTemperature = "0"
	docket.CoolBox = coolBox
	docket.LoadTemperature = "0"
	docket.LastCIPTemperature = "0"
	docket.SampleCount = "2"
	docket.IntakeCount = "2"
	docket.TotalIntakeVolume = "100"

	intakes = nil

	intake.SupplierID = "SUP1003"
	intake.Volume = "50"
	intake.Temperature = "0"
	intake.Time = "08:00:00"
	intake.Info = "4713"
	intake.SampleBottleID = "0817"
	intakes = append(intakes, intake)

	intake.SupplierID = "SUP1004"
	intake.Volume = "50"
	intake.Temperature = "0"
	intake.Time = "10:00:00"
	intake.Info = "4714"
	intake.SampleBottleID = "0818"
	intakes = append(intakes, intake)

	docket.Intakes = intakes

	dockets = append(dockets, docket)
	docketJSON, err := json.Marshal(dockets)
	if err != nil {
		fmt.Println("Error converting docket records to JSON")
		return nil
	}
	return []byte(docketJSON)
}

//get Docket after update with new Intake
func getDocketWithNewIntakeExpected() []byte {

	var coolBox CoolBox
	var run Run
	var truck Truck
	var intake Intake
	var intakes []Intake
	var docket Docket

	docket.LoadID = "LOD1001"
	docket.FactoryID = "FAC1001"
	run.ID = "RUN1001"
	run.Date = "06/21/2018"
	run.StartTime = "06:00:00"
	run.StartKilometerReading = "100"
	run.EndTime = "09:35:00"
	run.EndKilometerReading = "200"
	truck.ID = "TRU1001"
	truck.DriverID = "DRV1001"
	truck.Run = run
	docket.Truck = truck
	coolBox.StartTemperature = "0"
	coolBox.EndTemperature = "0"
	coolBox.AverageTemperature = "0"
	docket.CoolBox = coolBox
	docket.LoadTemperature = "0"
	docket.LastCIPTemperature = "0"
	docket.SampleCount = "3"
	docket.IntakeCount = "3"
	docket.TotalIntakeVolume = "75"

	intakes = nil

	intake.SupplierID = "SUP1001"
	intake.Volume = "25"
	intake.Temperature = "0"
	intake.Time = "07:00:00"
	intake.Info = "4711"
	intake.SampleBottleID = "0815"
	intakes = append(intakes, intake)

	intake.SupplierID = "SUP1002"
	intake.Volume = "25"
	intake.Temperature = "0"
	intake.Time = "09:00:00"
	intake.Info = "4712"
	intake.SampleBottleID = "0816"
	intakes = append(intakes, intake)

	intake.SupplierID = "SUP1005"
	intake.Volume = "25"
	intake.Temperature = "0"
	intake.Time = "09:00:00"
	intake.Info = "4715"
	intake.SampleBottleID = "0819"
	intakes = append(intakes, intake)

	docket.Intakes = intakes

	docketJSON, err := json.Marshal(docket)
	if err != nil {
		fmt.Println("Error converting a Docket record to JSON")
		return nil
	}
	return []byte(docketJSON)
}

func getExpectedDocketIDIndex(funcName string) []byte {
	var docketIDIndex DocketIDIndex
	switch funcName {
	case "addNewDocket":
		docketIDIndex.IDs = append(docketIDIndex.IDs, "LOD1001")
		docketIDIndexBytes, err := json.Marshal(docketIDIndex)
		if err != nil {
			fmt.Println("Error converting DocketIDIndex to JSON")
			return nil
		}
		return docketIDIndexBytes
	case "beforeRemoveDocket":
		docketIDIndex.IDs = append(docketIDIndex.IDs, "LOD1001")
		docketIDIndex.IDs = append(docketIDIndex.IDs, "LOD1002")
		docketIDIndexBytes, err := json.Marshal(docketIDIndex)
		if err != nil {
			fmt.Println("Error converting DocketIDIndex to JSON")
			return nil
		}
		return docketIDIndexBytes
	case "afterRemoveDocket":
		docketIDIndex.IDs = append(docketIDIndex.IDs, "LOD1001")
		docketIDIndexBytes, err := json.Marshal(docketIDIndex)
		if err != nil {
			fmt.Println("Error converting DocketIDIndex to JSON")
			return nil
		}
		return docketIDIndexBytes
	default:
		docketIDIndexBytes, err := json.Marshal(docketIDIndex)
		if err != nil {
			fmt.Println("Error converting DocketIDIndex to JSON")
			return nil
		}
		return docketIDIndexBytes
	}
}

//checkInit - helper to check the Initialization of chaincode: DocketAsset
func checkInit(t *testing.T, stub *shim.MockStub, args [][]byte) {
	res := stub.MockInit("1", args)
	if res.Status != shim.OK {
		fmt.Println("Init failed", string(res.Message))
		t.FailNow()
	}
}

//checkState - helper for checking the chaincode state for a given stateKey afgainst an expected value
func checkState(t *testing.T, stub *shim.MockStub, stateKey string, expectedState []byte) {
	actualState := stub.State[stateKey]
	if actualState == nil {
		fmt.Println("State for ", stateKey, ": failed to get value")
		t.FailNow()
	}

	if bytes.Compare(actualState, expectedState) != 0 {
		fmt.Println("Incorrect State for " + stateKey + ": \nExpected: " + string(expectedState) + "\nActual  : " + string(actualState))
		t.FailNow()
	}
}

//checkInvoke - helper for checking Invoke of chaincode
func checkInvoke(t *testing.T, stub *shim.MockStub, args [][]byte) {
	res := stub.MockInvoke("1", args)
	if res.Status != shim.OK {
		fmt.Println("Invoke", args, "failed", string(res.Message))
		t.FailNow()
	}
}

func checkInvokeUnknownFunction(t *testing.T, stub *shim.MockStub, args [][]byte) {
	res := stub.MockInvoke("1", args)
	if res.Status != shim.OK {
		expectedErr := "Received unknown function invocation"
		actualErr := string(res.Message)
		if !(strings.Contains(actualErr, expectedErr)) {
			fmt.Println("Invoke with unknown function must throw error: ", expectedErr, "\n Actual Error :", actualErr)
			t.FailNow()
		}
	} else {
		t.FailNow()
	}
}

//checkReadDocketOK - helper for positive test readDocket
func checkReadDocketOK(t *testing.T, stub *shim.MockStub, docketID string) {
	res := stub.MockInvoke("1", [][]byte{[]byte("readDocket"), []byte(docketID)})
	if res.Status != shim.OK {
		fmt.Println("func readDocket with ID: ", docketID, " failed"+string(res.Message))
		t.FailNow()
	}
	if res.Payload == nil {
		fmt.Println("func readDocket with ID: ", docketID, "failed to get value")
		t.FailNow()
	}
	if bytes.Compare(getNewDocketExpected(), []byte(res.Payload)) != 0 {
		fmt.Println("func readDocket with ID: ", docketID, "Expected:", string(getNewDocketExpected()), "Actual:", string(res.Payload))
		t.FailNow()
	}
}

//checkReadDocketNOK - helper for negative testing of readDocket
func checkReadDocketNOK(t *testing.T, stub *shim.MockStub, docketID string) {
	//with no docketID
	res := stub.MockInvoke("1", [][]byte{[]byte("readDocket"), []byte("")})
	if res.Status != shim.OK {
		expectedErr := "retrieveDocket: Corrupt docket record "
		actualErr := string(res.Message)
		if !(strings.Contains(actualErr, expectedErr)) {
			fmt.Println("func readDocket negative test: ", "Expected Error:", expectedErr, "Actual Error", actualErr)
			t.FailNow()
		}
	} else {
		t.FailNow()
	}
}

func checkReadAllDocketsOK(t *testing.T, stub *shim.MockStub) {
	res := stub.MockInvoke("1", [][]byte{[]byte("readAllDockets")})
	if res.Status != shim.OK {
		fmt.Println("func readAllDockets failed", string(res.Message))
		t.FailNow()
	}
	if res.Payload == nil {
		fmt.Println("func readAllDockets failed to get value")
		t.FailNow()
	}
	if bytes.Compare(getExpectedDockets(), []byte(res.Payload)) != 0 {
		fmt.Println("func readAllDockets Expected:\n", string(getExpectedDockets()), "\nActual:\n", string(res.Payload))
		t.FailNow()
	}
}

func checkError(t *testing.T, exp string, act string) {
	if strings.Compare(exp, act) != 0 {
		fmt.Println("Unexpected Error! Expecting ", exp, "\n Actual :", act)
		t.FailNow()
	}
}
