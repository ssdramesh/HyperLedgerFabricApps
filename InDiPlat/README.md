# What's in here?
This is the repo for the demo of supply chain financing solution based on blockchain.
The demo showcases how multi-party collaboration in the area of supply chain financing can be streamlined with blockchain-based platform and achieve process optimization using smart contracts.

# Business Context
Operational cash management for small and medium scale suppliers poses significant challenges that operate against purchasing contracts / purchase orders with larger scale buyers that dictate longer payout periods as terms of payment.  That is, when a Buyer is having a high number of invoices that are to be paid out 45/60/90 days after shipment, the Buyer faces a lowered operating cash to run the business for that period.

The following figure depicts the high-level business process of Procure-to-Pay (from Buyer's perspective) / Invoice-to-Cash (from Seller's perspective) and the specific paint point that is addressed by the proposed solution.

![Business Process and Pain Point](./Model/OverallProcess.png)<br/>

Buyers have been trying to address this business challenge for a long time now by discounting invoices with banks and financial institutions with varying business models like factoring, spot factoring etc.  The following figure provides a brief list of definitions and terminology clarifications.

![Definitions](./Model/Definitions.png)<br/>

![What's New?](./Model/WhatsNew.png)<br/>

Most recently there has been a rise of FinTech / Startups that have been fairly successful in providing solutions in this space.  The following figure depicts a short summary of such approaches.

![New Players](./Model/Players.png)<br/>

# Business Network Specification
This demo showcases how blockchain can be used in this business context.  The following figures depict the targeted business process implemented in this demo and also a very high-level architecture that shows what other services are used in the SAP Cloud Platform.

![Business Flow](./Model/BusinessFlow.png)<br/>

![High Level Architecture](./Model/HighLevelArchitecture.png)<br/>

# Model
![Model](./Model/InDiPlatFabricComposer.png)<br/>

# Deployed Systems
Coming Soon!

# Demo Script(s)
Coming Soon!

# Published Links

# Contact(s)
Ramesh Suraparaju (I047582)
