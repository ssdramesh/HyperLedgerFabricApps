#!/bin/bash
#exec >> ./log/logfile.txt 2>&1

echo "Clean All InDiPlat Network Entities before UI Setup..."
# newman run BAT1001_CLEAN_ALL.postman_Collection.json -e BAT1001.postman_environment.json --bail newman
#
# echo "Setting up Entities..."
#
# echo "Create Bags..."
# newman run BAT1001_BAGS.postman_Collection.json -e BAT1001.postman_environment.json -d ./data/Bags.json --bail newman
#
# echo "Create Batches..."
# newman run BAT1001_BATCHES.postman_Collection.json -e BAT1001.postman_environment.json -d ./data/Batches.json --bail newman
#
# echo "Create Dealers..."
# newman run BAT1001_DEALERS.postman_Collection.json -e BAT1001.postman_environment.json -d ./data/Dealers.json --bail newman
#
# echo "Create Farmers..."
# newman run BAT1001_FARMERS.postman_Collection.json -e BAT1001.postman_environment.json -d ./data/Farmers.json --bail newman
#
# echo "Create Plants..."
# newman run BAT1001_PLANTS.postman_Collection.json -e BAT1001.postman_environment.json -d ./data/Plants.json --bail newman
#
# echo "Create Supermarkets..."
# newman run BAT1001_SUPERMARKETS.postman_Collection.json -e BAT1001.postman_environment.json -d ./data/Supermarkets.json --bail newman
#
# echo "Create Trucks..."
# newman run BAT1001_TRUCKS.postman_Collection.json -e BAT1001.postman_environment.json -d ./data/Trucks.json --bail newman
#
# echo "Create Trips..."
# newman run BAT1001_TRIPS.postman_Collection.json -e BAT1001.postman_environment.json -d ./data/Trips.json --bail newman
#
# echo "Create Touches..."
# newman run BAT1001_TOUCHES.postman_collection.json -e BAT1001.postman_environment.json -d ./data/Touches.json --bail newman
