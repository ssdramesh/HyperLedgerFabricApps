#!/bin/bash

cd /Users/i047582/Documents/Workspaces/git/github.wdf.sap.corp/I047582/HyperLedgerFabricApps/InDiPlat/test/newman/CleanAllAndSetup


echo "Clean All FinancierParticipants before Setup..."
newman run cleanAllFinancierParticipants.postman_collection.json -e InDiPlat.postman_environment.json --bail newman
echo "Setting up FinancierParticipants..."
newman run createAllFinancierParticipants.postman_collection.json -e InDiPlat.postman_environment.json -d ./data/FinancierParticipants.json --bail newman

echo "Clean All PurchaserParticipants before Setup..."
newman run cleanAllPurchaserParticipants.postman_collection.json -e InDiPlat.postman_environment.json --bail newman
echo "Setting up PurchaserParticipants..."
newman run createAllPurchaserParticipants.postman_collection.json -e InDiPlat.postman_environment.json -d ./data/PurchaserParticipants.json --bail newman

echo "Clean All SupplierParticipants before Setup..."
newman run cleanAllSupplierParticipants.postman_collection.json -e InDiPlat.postman_environment.json --bail newman
echo "Setting up SupplierParticipants..."
newman run createAllSupplierParticipants.postman_collection.json -e InDiPlat.postman_environment.json -d ./data/SupplierParticipants.json --bail newman

echo "Clean All PurchaseOrderAssets before Setup..."
newman run cleanAllPurchaseOrderAssets.postman_collection.json -e InDiPlat.postman_environment.json --bail newman
echo "Setting up PurchaseOrderAssets..."
newman run createAllPurchaseOrderAssets.postman_collection.json -e InDiPlat.postman_environment.json -d ./data/PurchaseOrderAssets.json --bail newman

echo "Clean All FinancingRequestAssets before Setup..."
newman run cleanAllFinancingRequestAssets.postman_collection.json -e InDiPlat.postman_environment.json --bail newman
echo "Setting up FinancingRequestAssets..."
newman run createAllFinancingRequestAssets.postman_collection.json -e InDiPlat.postman_environment.json -d ./data/FinancingRequestAssets.json --bail newman

echo "Clean All FinancingOfferAssets before Setup..."
newman run cleanAllFinancingOfferAssets.postman_collection.json -e InDiPlat.postman_environment.json --bail newman
echo "Setting up FinancingOfferAssets..."
newman run createAllFinancingOfferAssets.postman_collection.json -e InDiPlat.postman_environment.json -d ./data/FinancingOfferAssets.json --bail newman

echo "Clean All InvoiceAssets before Setup..."
newman run cleanAllInvoiceAssets.postman_collection.json -e InDiPlat.postman_environment.json --bail newman
echo "Setting up InvoiceAssets..."
newman run createAllInvoiceAssets.postman_collection.json -e InDiPlat.postman_environment.json -d ./data/InvoiceAssets.json --bail newman

echo "Clean All InterestRateAssets before Setup..."
newman run cleanAllInterestRateAssets.postman_collection.json -e InDiPlat.postman_environment.json --bail newman
echo "Setting up InterestRateAssets..."
newman run createAllInterestRateAssets.postman_collection.json -e InDiPlat.postman_environment.json -d ./data/InterestRateAssets.json --bail newman

echo "Done. Ready to run!"
