package main

import (
	"bytes"
	"encoding/json"
	"fmt"
	"strings"
	"testing"

	"github.com/hyperledger/fabric/core/chaincode/shim"
)

//TestPurchaserParticipant_Init
func TestPurchaserParticipant_Init(t *testing.T) {
	sample := new(PurchaserParticipant)
	stub := shim.NewMockStub("sample", sample)
	checkInit(t, stub, [][]byte{[]byte("init")})
	checkState(t, stub, "purchaserIDIndex", getExpectedPurchaserIDIndex("init"))
}

//TestPurchaserParticipant_InvokeUnknownFunction
func TestPurchaserParticipant_InvokeUnknownFunction(t *testing.T) {
	sample := new(PurchaserParticipant)
	stub := shim.NewMockStub("sample", sample)
	checkInit(t, stub, [][]byte{[]byte("init")})
	checkInvokeUnknownFunction(t, stub, [][]byte{[]byte("myFunction"), []byte("docType:Participant")})
}

//TestPurchaserParticipant_Invoke_addNewPurchaser
func TestPurchaserParticipant_Invoke_addNewPurchaserOK(t *testing.T) {
	sample := new(PurchaserParticipant)
	stub := shim.NewMockStub("sample", sample)
	checkInit(t, stub, [][]byte{[]byte("init")})
	checkInvoke(t, stub, getFirstPurchaserParticipantForTesting())
	newPurchaserID := "100001"
	checkState(t, stub, newPurchaserID, getNewPurchaserExpected())
	checkState(t, stub, "purchaserIDIndex", getExpectedPurchaserIDIndex("addNewPurchaser"))
}

//TestPurchaserParticipant_Invoke_addNewPurchaserUnknownField
func TestPurchaserParticipant_Invoke_addNewPurchaserUnknownField(t *testing.T) {
	sample := new(PurchaserParticipant)
	stub := shim.NewMockStub("sample", sample)
	checkInit(t, stub, [][]byte{[]byte("init")})
	res := stub.MockInvoke("1", getPurchaserParticipantWithUnknownFieldForTesting())
	if res.Status != shim.OK {
		checkError(t, "Purchaser Data is Corrupted", res.Message)
	} else {
		fmt.Println("Unknown Field Error was expected, but not raised")
		t.FailNow()
	}
}

//TestPurchaserParticipant_Invoke_addNewPurchaser
func TestPurchaserParticipant_Invoke_addNewPurchaserDuplicate(t *testing.T) {
	sample := new(PurchaserParticipant)
	stub := shim.NewMockStub("sample", sample)
	checkInit(t, stub, [][]byte{[]byte("init")})
	checkInvoke(t, stub, getFirstPurchaserParticipantForTesting())
	newPurchaserID := "100001"
	checkState(t, stub, newPurchaserID, getNewPurchaserExpected())
	checkState(t, stub, "purchaserIDIndex", getExpectedPurchaserIDIndex("addNewPurchaser"))
	res := stub.MockInvoke("1", getFirstPurchaserParticipantForTesting())
	if res.Status != shim.OK {
		checkError(t, "This Purchaser already exists: 100001", res.Message)
	} else {
		fmt.Println("Error was expected, but not raised")
		t.FailNow()
	}
}

//TestPurchaserParticipant_Invoke_removePurchaserOK  //change template
func TestPurchaserParticipant_Invoke_removePurchaserOK(t *testing.T) {
	sample := new(PurchaserParticipant)
	stub := shim.NewMockStub("sample", sample)
	checkInit(t, stub, [][]byte{[]byte("init")})
	checkInvoke(t, stub, getFirstPurchaserParticipantForTesting())
	checkInvoke(t, stub, getSecondPurchaserParticipantForTesting())
	checkReadAllPurchasersOK(t, stub)
	checkState(t, stub, "purchaserIDIndex", getExpectedPurchaserIDIndex("beforeRemovePurchaser"))
	checkInvoke(t, stub, getRemoveSecondPurchaserParticipantForTesting())
	remainingPurchaserID := "100001"
	checkReadPurchaserOK(t, stub, remainingPurchaserID)
	checkState(t, stub, "purchaserIDIndex", getExpectedPurchaserIDIndex("afterRemovePurchaser"))
}

//TestPurchaserParticipant_Invoke_removePurchaserNOK  //change template
func TestPurchaserParticipant_Invoke_removePurchaserNOK(t *testing.T) {
	sample := new(PurchaserParticipant)
	stub := shim.NewMockStub("sample", sample)
	checkInit(t, stub, [][]byte{[]byte("init")})
	checkInvoke(t, stub, getFirstPurchaserParticipantForTesting())
	firstPurchaserID := "100001"
	checkReadPurchaserOK(t, stub, firstPurchaserID)
	checkState(t, stub, "purchaserIDIndex", getExpectedPurchaserIDIndex("addNewPurchaser"))
	res := stub.MockInvoke("1", getRemoveSecondPurchaserParticipantForTesting())
	if res.Status != shim.OK {
		checkError(t, "Purchaser with ID: "+"100002"+" not found", res.Message)
	} else {
		fmt.Println("Error was expected, but not raised")
		t.FailNow()
	}
	checkState(t, stub, "purchaserIDIndex", getExpectedPurchaserIDIndex("addNewPurchaser"))
}

//TestPurchaserParticipant_Invoke_removeAllPurchaserssOK
func TestPurchaserParticipant_Invoke_removeAllPurchaserssOK(t *testing.T) {
	financier := new(PurchaserParticipant)
	stub := shim.NewMockStub("financier", financier)
	checkInit(t, stub, [][]byte{[]byte("init")})
	checkInvoke(t, stub, getFirstPurchaserParticipantForTesting())
	checkInvoke(t, stub, getSecondPurchaserParticipantForTesting())
	checkReadAllPurchasersOK(t, stub)
	checkState(t, stub, "purchaserIDIndex", getExpectedPurchaserIDIndex("beforeRemovePurchaser"))
	checkInvoke(t, stub, getRemoveAllPurchaserParticipantsForTesting())
	checkState(t, stub, "purchaserIDIndex", getExpectedPurchaserIDIndex(""))
}

//TestPurchaserParticipant_Query_readPurchaser
func TestPurchaserParticipant_Query_readPurchaser(t *testing.T) {
	sample := new(PurchaserParticipant)
	stub := shim.NewMockStub("sample", sample)
	checkInit(t, stub, [][]byte{[]byte("init")})
	sampleID := "100001"
	checkInvoke(t, stub, getFirstPurchaserParticipantForTesting())
	checkReadPurchaserOK(t, stub, sampleID)
	checkReadPurchaserNOK(t, stub, "")
}

//TestPurchaserParticipant_Query_readAllPurchasers
func TestPurchaserParticipant_Query_readAllPurchasers(t *testing.T) {
	sample := new(PurchaserParticipant)
	stub := shim.NewMockStub("sample", sample)
	checkInit(t, stub, [][]byte{[]byte("init")})
	checkInvoke(t, stub, getFirstPurchaserParticipantForTesting())
	checkInvoke(t, stub, getSecondPurchaserParticipantForTesting())
	checkReadAllPurchasersOK(t, stub)
}

/*
*
*	Helper Functions
*
 */
//Get first PurchaserParticipant for testing
func getFirstPurchaserParticipantForTesting() [][]byte {
	return [][]byte{[]byte("addNewPurchaser"),
		[]byte("{\"ID\":\"100001\"," +
			"\"docType\":\"Participant\"," +
			"\"alias\":\"BRITECH\"," +
			"\"description\":\"British Technology Pvt. Ltd.\"}")}
}

//Get second PurchaserParticipant for testing
func getSecondPurchaserParticipantForTesting() [][]byte {
	return [][]byte{[]byte("addNewPurchaser"),
		[]byte("{\"ID\":\"100002\"," +
			"\"docType\":\"Participant\"," +
			"\"alias\":\"DEMAGDELAG\"," +
			"\"description\":\"Demag Delewal AG\"}")}
}

//Get PurchaserParticipant with unknown field for testing
func getPurchaserParticipantWithUnknownFieldForTesting() [][]byte {
	return [][]byte{[]byte("addNewPurchaser"),
		[]byte("{\"ID\":\"100001\"," +
			"\"docuType\":\"Participant\"," +
			"\"alias\":\"BRITECH\"," +
			"\"description\":\"British Technology Pvt. Ltd.\"}")}
}

//Get remove second PurchaserParticipant for testing //change template
func getRemoveSecondPurchaserParticipantForTesting() [][]byte {
	return [][]byte{[]byte("removePurchaser"),
		[]byte("100002")}
}

//Get remove all PurchaserParticipants for testing
func getRemoveAllPurchaserParticipantsForTesting() [][]byte {
	return [][]byte{[]byte("removeAllPurchasers")}
}

//Get an expected value for testing
func getNewPurchaserExpected() []byte {
	var sample Purchaser
	sample.ID = "100001"
	sample.ObjectType = "Participant.Purchaser"
	sample.Alias = "BRITECH"
	sample.Description = "British Technology Pvt. Ltd."
	sampleJSON, err := json.Marshal(sample)
	if err != nil {
		fmt.Println("Error converting a Purchaser record to JSON")
		return nil
	}
	return []byte(sampleJSON)
}

//Get expected values of Purchasers for testing
func getExpectedPurchasers() []byte {
	var samples []Purchaser
	var sample Purchaser
	sample.ID = "100001"
	sample.ObjectType = "Participant.Purchaser"
	sample.Alias = "BRITECH"
	sample.Description = "British Technology Pvt. Ltd."
	samples = append(samples, sample)
	sample.ID = "100002"
	sample.ObjectType = "Participant.Purchaser"
	sample.Alias = "DEMAGDELAG"
	sample.Description = "Demag Delewal AG"
	samples = append(samples, sample)
	sampleJSON, err := json.Marshal(samples)
	if err != nil {
		fmt.Println("Error converting sampleancer records to JSON")
		return nil
	}
	return []byte(sampleJSON)
}

func getExpectedPurchaserIDIndex(funcName string) []byte {
	var purchaserIDIndex PurchaserIDIndex
	switch funcName {
	case "addNewPurchaser":
		purchaserIDIndex.IDs = append(purchaserIDIndex.IDs, "100001")
		purchaserIDIndexBytes, err := json.Marshal(purchaserIDIndex)
		if err != nil {
			fmt.Println("Error converting PurchaserIDIndex to JSON")
			return nil
		}
		return purchaserIDIndexBytes
	case "beforeRemovePurchaser":
		purchaserIDIndex.IDs = append(purchaserIDIndex.IDs, "100001")
		purchaserIDIndex.IDs = append(purchaserIDIndex.IDs, "100002")
		purchaserIDIndexBytes, err := json.Marshal(purchaserIDIndex)
		if err != nil {
			fmt.Println("Error converting PurchaserIDIndex to JSON")
			return nil
		}
		return purchaserIDIndexBytes
	case "afterRemovePurchaser":
		purchaserIDIndex.IDs = append(purchaserIDIndex.IDs, "100001")
		purchaserIDIndexBytes, err := json.Marshal(purchaserIDIndex)
		if err != nil {
			fmt.Println("Error converting PurchaserIDIndex to JSON")
			return nil
		}
		return purchaserIDIndexBytes
	default:
		purchaserIDIndexBytes, err := json.Marshal(purchaserIDIndex)
		if err != nil {
			fmt.Println("Error converting PurchaserIDIndex to JSON")
			return nil
		}
		return purchaserIDIndexBytes
	}
}

//checkInit - helper to check the Initialization of chaincode: PurchaserParticipant
func checkInit(t *testing.T, stub *shim.MockStub, args [][]byte) {
	res := stub.MockInit("1", args)
	if res.Status != shim.OK {
		fmt.Println("Init failed", string(res.Message))
		t.FailNow()
	}
}

//checkState - helper for checking the chaincode state for a given stateKey afgainst an expected value
func checkState(t *testing.T, stub *shim.MockStub, stateKey string, expectedState []byte) {
	actualState := stub.State[stateKey]
	if actualState == nil {
		fmt.Println("State for ", stateKey, ": failed to get value")
		t.FailNow()
	}

	if bytes.Compare(actualState, expectedState) != 0 {
		fmt.Println("Incorrect State for " + stateKey + ": \nExpected: " + string(expectedState) + "\nActual  : " + string(actualState))
		t.FailNow()
	}
}

//checkInvoke - helper for checking Invoke of chaincode
func checkInvoke(t *testing.T, stub *shim.MockStub, args [][]byte) {
	res := stub.MockInvoke("1", args)
	if res.Status != shim.OK {
		fmt.Println("Invoke", args, "failed", string(res.Message))
		t.FailNow()
	}
}

func checkInvokeUnknownFunction(t *testing.T, stub *shim.MockStub, args [][]byte) {
	res := stub.MockInvoke("1", args)
	if res.Status != shim.OK {
		expectedErr := "Received unknown function invocation"
		actualErr := string(res.Message)
		if !(strings.Contains(actualErr, expectedErr)) {
			fmt.Println("Invoke with unknown function must throw error: ", expectedErr, "\n Actual Error :", actualErr)
			t.FailNow()
		}
	} else {
		t.FailNow()
	}
}

//checkReadPurchaserOK - helper for positive test readPurchaser
func checkReadPurchaserOK(t *testing.T, stub *shim.MockStub, sampleancerID string) {
	res := stub.MockInvoke("1", [][]byte{[]byte("readPurchaser"), []byte(sampleancerID)})
	if res.Status != shim.OK {
		fmt.Println("func readPurchaser with ID: ", sampleancerID, " failed"+string(res.Message))
		t.FailNow()
	}
	if res.Payload == nil {
		fmt.Println("func readPurchaser with ID: ", sampleancerID, "failed to get value")
		t.FailNow()
	}
	if bytes.Compare(getNewPurchaserExpected(), []byte(res.Payload)) != 0 {
		fmt.Println("func readPurchaser with ID: ", sampleancerID, "Expected:", string(getNewPurchaserExpected()), "Actual:", string(res.Payload))
		t.FailNow()
	}
}

//checkReadPurchaserNOK - helper for negative testing of readPurchaser
func checkReadPurchaserNOK(t *testing.T, stub *shim.MockStub, sampleancerID string) {
	//with no sampleancerID
	res := stub.MockInvoke("1", [][]byte{[]byte("readPurchaser"), []byte("")})
	if res.Status != shim.OK {
		expectedErr := "retrievePurchaser: Corrupt sample record "
		actualErr := string(res.Message)
		if !(strings.Contains(actualErr, expectedErr)) {
			fmt.Println("func readPurchaser neagtive test: ", "Expected Error:", expectedErr, "Actual Error", actualErr)
			t.FailNow()
		}
	} else {
		t.FailNow()
	}
}
func checkReadAllPurchasersOK(t *testing.T, stub *shim.MockStub) {
	res := stub.MockInvoke("1", [][]byte{[]byte("readAllPurchasers")})
	if res.Status != shim.OK {
		fmt.Println("func readAllPurchasers failed", string(res.Message))
		t.FailNow()
	}
	if res.Payload == nil {
		fmt.Println("func readAllPurchasers failed to get value")
		t.FailNow()
	}
	if bytes.Compare(getExpectedPurchasers(), []byte(res.Payload)) != 0 {
		fmt.Println("func readAllPurchasers Expected:\n", string(getExpectedPurchasers()), "\nActual:\n", string(res.Payload))
		t.FailNow()
	}
}

func checkError(t *testing.T, exp string, act string) {
	if strings.Compare(exp, act) != 0 {
		fmt.Println("Unexpected Error! Expecting ", exp, "\n Actual :", act)
		t.FailNow()
	}
}
