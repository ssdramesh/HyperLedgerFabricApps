package main

import (
	"encoding/json"
	"errors"
	"fmt"
	"strings"

	"github.com/hyperledger/fabric/core/chaincode/shim"
	"github.com/hyperledger/fabric/protos/peer"
)

var logger = shim.NewLogger("CLDChaincode")

//PurchaserParticipant - Chaincode for Purchaser Participant
type PurchaserParticipant struct {
}

//Purchaser - Details of the participant type Purchaser
type Purchaser struct {
	ID          string `json:"ID"`
	ObjectType  string `json:"docType"`
	Alias       string `json:"alias"`
	Description string `json:"description"`
}

//PurchaserIDIndex - Index on IDs for retrieval all Purchasers
type PurchaserIDIndex struct {
	IDs []string `json:"IDs"`
}

func main() {
	err := shim.Start(new(PurchaserParticipant))
	if err != nil {
		fmt.Printf("Error starting PurchaserParticipant chaincode function main(): %s", err)
	} else {
		fmt.Printf("Starting PurchaserParticipant chaincode function main() executed successfully")
	}
}

//Init - The chaincode Init function: No  arguments, only initializes a ID array as Index for retrieval of all Purchasers
func (pur *PurchaserParticipant) Init(stub shim.ChaincodeStubInterface) peer.Response {
	var purchaserIDIndex PurchaserIDIndex
	record, _ := stub.GetState("purchaserIDIndex")
	//chaincode upgrade: Index might already exist, create new only if it does not
	if record == nil {
		bytes, _ := json.Marshal(purchaserIDIndex)
		stub.PutState("purchaserIDIndex", bytes)
	}
	return shim.Success(nil)
}

//Invoke - The chaincode Invoke function:
func (pur *PurchaserParticipant) Invoke(stub shim.ChaincodeStubInterface) peer.Response {
	function, args := stub.GetFunctionAndParameters()
	logger.Debug("function: ", function)
	switch function {
	case "addNewPurchaser":
		return pur.addNewPurchaser(stub, args)
	case "removePurchaser":
		return pur.removePurchaser(stub, args[0])
	case "removeAllPurchasers":
		return pur.removeAllPurchasers(stub)
	case "readPurchaser":
		return pur.readPurchaser(stub, args[0])
	case "readAllPurchasers":
		return pur.readAllPurchasers(stub)
	default:
		return shim.Error("Received unknown function invocation")
	}
}

//Invoke Route: addNewPurchaser
func (pur *PurchaserParticipant) addNewPurchaser(stub shim.ChaincodeStubInterface, args []string) peer.Response {
	sample, err := getPurchaserFromArgs(args)
	if err != nil {
		return shim.Error("Purchaser Data is Corrupted")
	}
	sample.ObjectType = "Participant.Purchaser"
	record, err := stub.GetState(sample.ID)
	if record != nil {
		return shim.Error("This Purchaser already exists: " + sample.ID)
	}
	_, err = pur.savePurchaser(stub, sample)
	if err != nil {
		return shim.Error(err.Error())
	}
	_, err = pur.updatePurchaserIDIndex(stub, sample)
	if err != nil {
		return shim.Error(err.Error())
	}
	return shim.Success(nil)
}

//Invoke Route: removePurchaser
func (pur *PurchaserParticipant) removePurchaser(stub shim.ChaincodeStubInterface, sampleStructParticipantID string) peer.Response {
	_, err := pur.deletePurchaser(stub, sampleStructParticipantID)
	if err != nil {
		return shim.Error(err.Error())
	}
	_, err = pur.deletePurchaserIDIndex(stub, sampleStructParticipantID)
	if err != nil {
		return shim.Error(err.Error())
	}
	return shim.Success(nil)
}

//Invoke Route: removeAllPurchasers
func (pur *PurchaserParticipant) removeAllPurchasers(stub shim.ChaincodeStubInterface) peer.Response {
	var purchaserStructIDs PurchaserIDIndex
	bytes, err := stub.GetState("purchaserIDIndex")
	if err != nil {
		return shim.Error("removeAllPurchasers: Error getting purchaserIDIndex array")
	}
	err = json.Unmarshal(bytes, &purchaserStructIDs)
	if err != nil {
		return shim.Error("removeAllPurchasers: Error unmarshalling purchaserIDIndex array JSON")
	}
	if len(purchaserStructIDs.IDs) == 0 {
		return shim.Error("removeAllPurchasers: No purchasers to remove")
	}
	for _, purchaserStructParticipantID := range purchaserStructIDs.IDs {
		_, err = pur.deletePurchaser(stub, purchaserStructParticipantID)
		if err != nil {
			return shim.Error("Failed to remove Purchaser with ID: " + purchaserStructParticipantID)
		}
		_, err = pur.deletePurchaserIDIndex(stub, purchaserStructParticipantID)
		if err != nil {
			return shim.Error(err.Error())
		}
	}
	pur.initHolder(stub)
	return shim.Success(nil)
}

//Query Route: readPurchaser
func (pur *PurchaserParticipant) readPurchaser(stub shim.ChaincodeStubInterface, sampleParticipantID string) peer.Response {
	sampleAsByteArray, err := pur.retrievePurchaser(stub, sampleParticipantID)
	if err != nil {
		return shim.Error(err.Error())
	}
	return shim.Success(sampleAsByteArray)
}

//Query Route: readAllPurchasers
func (pur *PurchaserParticipant) readAllPurchasers(stub shim.ChaincodeStubInterface) peer.Response {
	var sampleIDs PurchaserIDIndex
	bytes, err := stub.GetState("purchaserIDIndex")
	if err != nil {
		return shim.Error("readAllPurchasers: Error getting purchaserIDIndex array")
	}
	err = json.Unmarshal(bytes, &sampleIDs)
	if err != nil {
		return shim.Error("readAllPurchasers: Error unmarshalling purchaserIDIndex array JSON")
	}
	result := "["

	var sampleAsByteArray []byte

	for _, sampleID := range sampleIDs.IDs {
		sampleAsByteArray, err = pur.retrievePurchaser(stub, sampleID)
		if err != nil {
			return shim.Error("Failed to retrieve sample with ID: " + sampleID)
		}
		result += string(sampleAsByteArray) + ","
	}
	if len(result) == 1 {
		result = "[]"
	} else {
		result = result[:len(result)-1] + "]"
	}
	return shim.Success([]byte(result))
}

//Helper: Save sampleParticipant
func (pur *PurchaserParticipant) savePurchaser(stub shim.ChaincodeStubInterface, sample Purchaser) (bool, error) {
	bytes, err := json.Marshal(sample)
	if err != nil {
		return false, errors.New("Error converting sample record JSON")
	}
	err = stub.PutState(sample.ID, bytes)
	if err != nil {
		return false, errors.New("Error storing Purchaser record")
	}
	return true, nil
}

//Helper: Delete sampleStructParticipant
func (pur *PurchaserParticipant) deletePurchaser(stub shim.ChaincodeStubInterface, sampleStructParticipantID string) (bool, error) {
	_, err := pur.retrievePurchaser(stub, sampleStructParticipantID)
	if err != nil {
		return false, errors.New("Purchaser with ID: " + sampleStructParticipantID + " not found")
	}
	err = stub.DelState(sampleStructParticipantID)
	if err != nil {
		return false, errors.New("Error deleting Purchaser record")
	}
	return true, nil
}

//Helper: Update sample Holder - updates Index
func (pur *PurchaserParticipant) updatePurchaserIDIndex(stub shim.ChaincodeStubInterface, sample Purchaser) (bool, error) {
	var sampleIDs PurchaserIDIndex
	bytes, err := stub.GetState("purchaserIDIndex")
	if err != nil {
		return false, errors.New("upadetePurchaserIDIndex: Error getting purchaserIDIndex array Index from state")
	}
	err = json.Unmarshal(bytes, &sampleIDs)
	if err != nil {
		return false, errors.New("upadetePurchaserIDIndex: Error unmarshalling purchaserIDIndex array JSON")
	}
	sampleIDs.IDs = append(sampleIDs.IDs, sample.ID)
	bytes, err = json.Marshal(sampleIDs)
	if err != nil {
		return false, errors.New("updatePurchaserIDIndex: Error marshalling new sample ID")
	}
	err = stub.PutState("purchaserIDIndex", bytes)
	if err != nil {
		return false, errors.New("updatePurchaserIDIndex: Error storing new sample ID in purchaserIDIndex (Index)")
	}
	return true, nil
}

//Helper: delete ID from sampleStruct Holder
func (pur *PurchaserParticipant) deletePurchaserIDIndex(stub shim.ChaincodeStubInterface, sampleStructParticipantID string) (bool, error) {
	var sampleStructIDs PurchaserIDIndex
	bytes, err := stub.GetState("purchaserIDIndex")
	if err != nil {
		return false, errors.New("deletePurchaserIDIndex: Error getting purchaserIDIndex array Index from state")
	}
	err = json.Unmarshal(bytes, &sampleStructIDs)
	if err != nil {
		return false, errors.New("deletePurchaserIDIndex: Error unmarshalling purchaserIDIndex array JSON")
	}
	sampleStructIDs.IDs, err = deleteKeyFromStringArray(sampleStructIDs.IDs, sampleStructParticipantID)
	if err != nil {
		return false, errors.New(err.Error())
	}
	bytes, err = json.Marshal(sampleStructIDs)
	if err != nil {
		return false, errors.New("deletePurchaserIDIndex: Error marshalling new sampleStruct ID")
	}
	err = stub.PutState("purchaserIDIndex", bytes)
	if err != nil {
		return false, errors.New("deletePurchaserIDIndex: Error storing new sampleStruct ID in purchaserIDIndex (Index)")
	}
	return true, nil
}

//Helper: Initialize sample ID Holder
func (pur *PurchaserParticipant) initHolder(stub shim.ChaincodeStubInterface) bool {
	var purchaserIDIndex PurchaserIDIndex
	bytes, _ := json.Marshal(purchaserIDIndex)
	stub.DelState("purchaserIDIndex")
	stub.PutState("purchaserIDIndex", bytes)
	return true
}

//Helper: Retrieve sampleParticipant
func (pur *PurchaserParticipant) retrievePurchaser(stub shim.ChaincodeStubInterface, sampleParticipantID string) ([]byte, error) {
	var sample Purchaser
	var sampleAsByteArray []byte
	bytes, err := stub.GetState(sampleParticipantID)
	if err != nil {
		return sampleAsByteArray, errors.New("retrievePurchaser: Error retrieving sample with ID: " + sampleParticipantID)
	}
	err = json.Unmarshal(bytes, &sample)
	if err != nil {
		return sampleAsByteArray, errors.New("retrievePurchaser: Corrupt sample record " + string(bytes))
	}
	sampleAsByteArray, err = json.Marshal(sample)
	if err != nil {
		return sampleAsByteArray, errors.New("readPurchaser: Invalid sample Object - Not a  valid JSON")
	}
	return sampleAsByteArray, nil
}

//deleteKeyFromArray
func deleteKeyFromStringArray(array []string, key string) (newArray []string, err error) {
	for _, entry := range array {
		if entry != key {
			newArray = append(newArray, entry)
		}
	}
	if len(newArray) == len(array) {
		return newArray, errors.New("Specified Key: " + key + " not found in Array")
	}
	return newArray, nil
}

//getPurchaserFromArgs - construct a sample structure from string array of arguments
func getPurchaserFromArgs(args []string) (sample Purchaser, err error) {

	if strings.Contains(args[0], "\"ID\"") == false ||
		strings.Contains(args[0], "\"docType\"") == false ||
		strings.Contains(args[0], "\"alias\"") == false ||
		strings.Contains(args[0], "\"description\"") == false {
		return sample, errors.New("Unknown field: " + "Input JSON does not comply to schema")
	}

	err = json.Unmarshal([]byte(args[0]), &sample)
	if err != nil {
		return sample, err
	}
	return sample, nil

}
