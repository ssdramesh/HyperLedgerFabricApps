package main

import (
	"encoding/json"
	"errors"
	"fmt"
	"strings"

	"github.com/hyperledger/fabric/core/chaincode/shim"
	"github.com/hyperledger/fabric/protos/peer"
)

var logger = shim.NewLogger("CLDChaincode")

//Approved - Invoice is in status approved - start status
const Approved = "Approved"

//FinancingRequested - Intermediate State
const FinancingRequested = "Financing Requested"

//FinancingOffered - Intermediate State
const FinancingOffered = "Financing Offered"

//FinancingAccepted - Intermediate State
const FinancingAccepted = "Financing Accepted"

//FinancingCompleted - Final State
const FinancingCompleted = "Financing Completed"

//InvoiceAsset - Chaincode for asset Invoice
type InvoiceAsset struct {
}

//Invoice - Details of the asset type Invoice
type Invoice struct {
	ID               string `json:"ID"`
	SupplierID       string `json:"supplierID"`
	PurchaserID      string `json:"purchaserID"`
	FinancerID       string `json:"financerID"`
	PurchaseOrderID  string `json:"purchaseOrderID"`
	ObjectType       string `json:"docType"`
	Status           string `json:"status"`
	PostingDate      string `json:"postingDate"`
	ApprovalDate     string `json:"approvalDate"`
	PaymentDate      string `json:"paymentDate"`
	DisbursementDate string `json:"disbursementDate"`
	GrossAmount      string `json:"grossAmount"`
	DiscountPercent  string `json:"discountPercent"`
	FinancedAmount   string `json:"financedAmount"`
	Currency         string `json:"currency"`
}

//InvoiceIDIndex - Index on IDs for retrieval all Invoices
type InvoiceIDIndex struct {
	IDs []string `json:"IDs"`
}

func main() {
	err := shim.Start(new(InvoiceAsset))
	if err != nil {
		fmt.Printf("Error starting InvoiceAsset chaincode function main(): %s", err)
	} else {
		fmt.Printf("Starting InvoiceAsset chaincode function main() executed successfully")
	}
}

//Init - The chaincode Init function: No  arguments, only initializes a ID array as Index for retrieval of all Invoices
func (inv *InvoiceAsset) Init(stub shim.ChaincodeStubInterface) peer.Response {
	var invoiceIDIndex InvoiceIDIndex
	record, _ := stub.GetState("invoiceIDIndex")
	//chaincode upgrade: Index might already exist, create new only if it does not
	if record == nil {
		bytes, _ := json.Marshal(invoiceIDIndex)
		stub.PutState("invoiceIDIndex", bytes)
	}
	return shim.Success(nil)
}

//Invoke - The chaincode Invoke function:
func (inv *InvoiceAsset) Invoke(stub shim.ChaincodeStubInterface) peer.Response {
	function, args := stub.GetFunctionAndParameters()
	logger.Debug("function: ", function)
	switch function {
	case "addNewInvoice":
		return inv.addNewInvoice(stub, args)
	case "setFinancingRequested":
		return inv.setFinancingRequested(stub, args[0], args[1])
	case "setFinancingOffered":
		return inv.setFinancingOffered(stub, args[0])
	case "setFinancingAccepted":
		return inv.setFinancingAccepted(stub, args[0])
	case "setFinancingCompleted":
		return inv.setFinancingCompleted(stub, args[0])
	case "removeInvoice":
		return inv.removeInvoice(stub, args[0])
	case "removeAllInvoices":
		return inv.removeAllInvoices(stub)
	case "readInvoice":
		return inv.readInvoice(stub, args[0])
	case "readAllInvoices":
		return inv.readAllInvoices(stub)
	default:
		return shim.Error("Received unknown function invocation")
	}
}

//Invoke Route: addNewInvoice
func (inv *InvoiceAsset) addNewInvoice(stub shim.ChaincodeStubInterface, args []string) peer.Response {
	invoice, err := getInvoiceFromArgs(args)
	if err != nil {
		return shim.Error("Invoice Data is Corrupted")
	}
	invoice.ObjectType = "Asset.Invoice"
	record, err := stub.GetState(invoice.ID)
	if record != nil {
		return shim.Error("This Invoice already exists: " + invoice.ID)
	}
	_, err = inv.saveInvoice(stub, invoice)
	if err != nil {
		return shim.Error(err.Error())
	}
	_, err = inv.updateInvoiceIDIndex(stub, invoice)
	if err != nil {
		return shim.Error(err.Error())
	}
	return shim.Success(nil)
}

//Invoke Route: setFinancingRequested
func (inv *InvoiceAsset) setFinancingRequested(stub shim.ChaincodeStubInterface, invoiceID string, financerID string) peer.Response {
	var invoice Invoice
	_, err := stub.GetState(invoiceID)
	if err != nil {
		return shim.Error("Invoice with ID: " + invoiceID + "not found")
	}
	invoiceAsByteArray, err := inv.retrieveInvoice(stub, invoiceID)
	if err != nil {
		return shim.Error(err.Error())
	}
	err = json.Unmarshal(invoiceAsByteArray, &invoice)
	if err != nil {
		return shim.Error(err.Error())
	}
	if invoice.Status == FinancingOffered || invoice.Status == FinancingAccepted || invoice.Status == FinancingCompleted {
		return shim.Error("Financing cannot be requested for Invoice with ID: " + invoiceID)
	}
	invoice.Status = FinancingRequested
	invoice.FinancerID = financerID
	_, err = inv.saveInvoice(stub, invoice)
	if err != nil {
		return shim.Error(err.Error())
	}
	return shim.Success(nil)
}

//Invoke Route: setFinancingOffered
func (inv *InvoiceAsset) setFinancingOffered(stub shim.ChaincodeStubInterface, invoiceID string) peer.Response {
	var invoice Invoice
	_, err := stub.GetState(invoiceID)
	if err != nil {
		return shim.Error("Invoice with ID: " + invoiceID + "not found")
	}
	invoiceAsByteArray, err := inv.retrieveInvoice(stub, invoiceID)
	if err != nil {
		return shim.Error(err.Error())
	}
	err = json.Unmarshal(invoiceAsByteArray, &invoice)
	if err != nil {
		return shim.Error(err.Error())
	}
	if invoice.Status == FinancingOffered || invoice.Status == FinancingAccepted || invoice.Status == FinancingCompleted {
		return shim.Error("Financing cannot be requested for Invoice with ID: " + invoiceID)
	}
	invoice.Status = FinancingOffered
	_, err = inv.saveInvoice(stub, invoice)
	if err != nil {
		return shim.Error(err.Error())
	}
	return shim.Success(nil)
}

//Invoke Route: setFinancingAccepted
func (inv *InvoiceAsset) setFinancingAccepted(stub shim.ChaincodeStubInterface, invoiceID string) peer.Response {
	var invoice Invoice
	_, err := stub.GetState(invoiceID)
	if err != nil {
		return shim.Error("Invoice with ID: " + invoiceID + "not found")
	}
	invoiceAsByteArray, err := inv.retrieveInvoice(stub, invoiceID)
	if err != nil {
		return shim.Error(err.Error())
	}
	err = json.Unmarshal(invoiceAsByteArray, &invoice)
	if err != nil {
		return shim.Error(err.Error())
	}
	if invoice.Status != FinancingOffered {
		return shim.Error("Financing cannot be requested for Invoice with ID: " + invoiceID)
	}
	invoice.Status = FinancingAccepted
	_, err = inv.saveInvoice(stub, invoice)
	if err != nil {
		return shim.Error(err.Error())
	}
	return shim.Success(nil)
}

//Invoke Route: setFinancingCompleted
func (inv *InvoiceAsset) setFinancingCompleted(stub shim.ChaincodeStubInterface, invoiceID string) peer.Response {
	var invoice Invoice
	_, err := stub.GetState(invoiceID)
	if err != nil {
		return shim.Error("Invoice with ID: " + invoiceID + "not found")
	}
	invoiceAsByteArray, err := inv.retrieveInvoice(stub, invoiceID)
	if err != nil {
		return shim.Error(err.Error())
	}
	err = json.Unmarshal(invoiceAsByteArray, &invoice)
	if err != nil {
		return shim.Error(err.Error())
	}
	if invoice.Status != FinancingAccepted {
		return shim.Error("Financing cannot be requested for Invoice with ID: " + invoiceID)
	}
	invoice.Status = FinancingCompleted
	_, err = inv.saveInvoice(stub, invoice)
	if err != nil {
		return shim.Error(err.Error())
	}
	return shim.Success(nil)
}

//Invoke Route: removeInvoice
func (inv *InvoiceAsset) removeInvoice(stub shim.ChaincodeStubInterface, invoiceID string) peer.Response {
	_, err := inv.deleteInvoice(stub, invoiceID)
	if err != nil {
		return shim.Error(err.Error())
	}
	_, err = inv.deleteInvoiceIDIndex(stub, invoiceID)
	if err != nil {
		return shim.Error(err.Error())
	}
	return shim.Success(nil)
}

//Invoke Route: removeAllInvoices
func (inv *InvoiceAsset) removeAllInvoices(stub shim.ChaincodeStubInterface) peer.Response {
	var invoiceStructIDs InvoiceIDIndex
	bytes, err := stub.GetState("invoiceIDIndex")
	if err != nil {
		return shim.Error("removeAllInvoices: Error getting invoiceIDIndex array")
	}
	err = json.Unmarshal(bytes, &invoiceStructIDs)
	if err != nil {
		return shim.Error("removeAllInvoices: Error unmarshalling invoiceIDIndex array JSON")
	}
	if len(invoiceStructIDs.IDs) == 0 {
		return shim.Error("removeAllInvoices: No invoices to remove")
	}
	for _, invoiceStructParticipantID := range invoiceStructIDs.IDs {
		_, err = inv.deleteInvoice(stub, invoiceStructParticipantID)
		if err != nil {
			return shim.Error("Failed to remove Invoice with ID: " + invoiceStructParticipantID)
		}
		_, err = inv.deleteInvoiceIDIndex(stub, invoiceStructParticipantID)
		if err != nil {
			return shim.Error(err.Error())
		}
	}
	inv.initHolder(stub)
	return shim.Success(nil)
}

//Query Route: readInvoice
func (inv *InvoiceAsset) readInvoice(stub shim.ChaincodeStubInterface, invoiceID string) peer.Response {
	invoiceAsByteArray, err := inv.retrieveInvoice(stub, invoiceID)
	if err != nil {
		return shim.Error(err.Error())
	}
	return shim.Success(invoiceAsByteArray)
}

//Query Route: readAllInvoices
func (inv *InvoiceAsset) readAllInvoices(stub shim.ChaincodeStubInterface) peer.Response {
	var invoiceIDs InvoiceIDIndex
	bytes, err := stub.GetState("invoiceIDIndex")
	if err != nil {
		return shim.Error("readAllInvoices: Error getting invoiceIDIndex array")
	}
	err = json.Unmarshal(bytes, &invoiceIDs)
	if err != nil {
		return shim.Error("readAllInvoices: Error unmarshalling invoiceIDIndex array JSON")
	}
	result := "["

	var invoiceAsByteArray []byte

	for _, invoiceID := range invoiceIDs.IDs {
		invoiceAsByteArray, err = inv.retrieveInvoice(stub, invoiceID)
		if err != nil {
			return shim.Error("Failed to retrieve invoice with ID: " + invoiceID)
		}
		result += string(invoiceAsByteArray) + ","
	}
	if len(result) == 1 {
		result = "[]"
	} else {
		result = result[:len(result)-1] + "]"
	}
	return shim.Success([]byte(result))
}

//Helper: Save invoice
func (inv *InvoiceAsset) saveInvoice(stub shim.ChaincodeStubInterface, invoice Invoice) (bool, error) {
	bytes, err := json.Marshal(invoice)
	if err != nil {
		return false, errors.New("Error converting invoice record JSON")
	}
	err = stub.PutState(invoice.ID, bytes)
	if err != nil {
		return false, errors.New("Error storing Invoice record")
	}
	return true, nil
}

//Helper: Invoice invoiceStruct //change template
func (invoice *InvoiceAsset) deleteInvoice(stub shim.ChaincodeStubInterface, invoiceID string) (bool, error) {
	_, err := invoice.retrieveInvoice(stub, invoiceID)
	if err != nil {
		return false, errors.New("Invoice with ID: " + invoiceID + " not found")
	}
	err = stub.DelState(invoiceID)
	if err != nil {
		return false, errors.New("Error deleting Invoice record")
	}
	return true, nil
}

//Helper: Update invoice Holder - updates Index
func (inv *InvoiceAsset) updateInvoiceIDIndex(stub shim.ChaincodeStubInterface, invoice Invoice) (bool, error) {
	var invoiceIDs InvoiceIDIndex
	bytes, err := stub.GetState("invoiceIDIndex")
	if err != nil {
		return false, errors.New("updateInvoiceIDIndex: Error getting invoiceIDIndex array Index from state")
	}
	err = json.Unmarshal(bytes, &invoiceIDs)
	if err != nil {
		return false, errors.New("updateInvoiceIDIndex: Error unmarshalling invoiceIDIndex array JSON")
	}
	invoiceIDs.IDs = append(invoiceIDs.IDs, invoice.ID)
	bytes, err = json.Marshal(invoiceIDs)
	if err != nil {
		return false, errors.New("updateInvoiceIDIndex: Error marshalling new invoice ID")
	}
	err = stub.PutState("invoiceIDIndex", bytes)
	if err != nil {
		return false, errors.New("updateInvoiceIDIndex: Error storing new invoice ID in invoiceIDIndex (Index)")
	}
	return true, nil
}

//Helper: delete ID from invoiceStruct Holder
func (inv *InvoiceAsset) deleteInvoiceIDIndex(stub shim.ChaincodeStubInterface, invoiceID string) (bool, error) {
	var invoiceStructIDs InvoiceIDIndex
	bytes, err := stub.GetState("invoiceIDIndex")
	if err != nil {
		return false, errors.New("deleteInvoiceIDIndex: Error getting invoiceIDIndex array Index from state")
	}
	err = json.Unmarshal(bytes, &invoiceStructIDs)
	if err != nil {
		return false, errors.New("deleteInvoiceIDIndex: Error unmarshalling invoiceIDIndex array JSON")
	}
	invoiceStructIDs.IDs, err = deleteKeyFromStringArray(invoiceStructIDs.IDs, invoiceID)
	if err != nil {
		return false, errors.New(err.Error())
	}
	bytes, err = json.Marshal(invoiceStructIDs)
	if err != nil {
		return false, errors.New("deleteInvoiceIDIndex: Error marshalling new invoiceStruct ID")
	}
	err = stub.PutState("invoiceIDIndex", bytes)
	if err != nil {
		return false, errors.New("deleteInvoiceIDIndex: Error storing new invoiceStruct ID in invoiceIDIndex (Index)")
	}
	return true, nil
}

//Helper: Initialize truck ID Holder //change template
func (inv *InvoiceAsset) initHolder(stub shim.ChaincodeStubInterface) bool {
	var invoiceIDIndex InvoiceIDIndex
	bytes, _ := json.Marshal(invoiceIDIndex)
	stub.DelState("invoiceIDIndex")
	stub.PutState("invoiceIDIndex", bytes)
	return true
}

//deleteKeyFromArray
func deleteKeyFromStringArray(array []string, key string) (newArray []string, err error) {
	for _, entry := range array {
		if entry != key {
			newArray = append(newArray, entry)
		}
	}
	if len(newArray) == len(array) {
		return newArray, errors.New("Specified Key: " + key + " not found in Array")
	}
	return newArray, nil
}

//Helper: Retrieve invoice
func (inv *InvoiceAsset) retrieveInvoice(stub shim.ChaincodeStubInterface, invoiceID string) ([]byte, error) {
	var invoice Invoice
	var invoiceAsByteArray []byte
	bytes, err := stub.GetState(invoiceID)
	if err != nil {
		return invoiceAsByteArray, errors.New("retrieveInvoice: Error retrieving invoice with ID: " + invoiceID)
	}
	err = json.Unmarshal(bytes, &invoice)
	if err != nil {
		return invoiceAsByteArray, errors.New("retrieveInvoice: Corrupt invoice record " + string(bytes))
	}
	invoiceAsByteArray, err = json.Marshal(invoice)
	if err != nil {
		return invoiceAsByteArray, errors.New("readInvoice: Invalid invoice Object - Not a  valid JSON")
	}
	return invoiceAsByteArray, nil
}

//getInvoiceFromArgs - construct a invoice structure from string array of arguments
func getInvoiceFromArgs(args []string) (sample Invoice, err error) {

	if strings.Contains(args[0], "\"ID\"") == false ||
		strings.Contains(args[0], "\"supplierID\"") == false ||
		strings.Contains(args[0], "\"purchaserID\"") == false ||
		strings.Contains(args[0], "\"purchaseOrderID\"") == false ||
		strings.Contains(args[0], "\"docType\"") == false ||
		strings.Contains(args[0], "\"status\"") == false ||
		strings.Contains(args[0], "\"postingDate\"") == false ||
		strings.Contains(args[0], "\"approvalDate\"") == false ||
		strings.Contains(args[0], "\"paymentDate\"") == false ||
		strings.Contains(args[0], "\"grossAmount\"") == false ||
		strings.Contains(args[0], "\"currency\"") == false {
		return sample, errors.New("Unknown field: " + "Input JSON does not comply to schema")
	}

	err = json.Unmarshal([]byte(args[0]), &sample)
	if err != nil {
		return sample, err
	}
	return sample, nil

}
