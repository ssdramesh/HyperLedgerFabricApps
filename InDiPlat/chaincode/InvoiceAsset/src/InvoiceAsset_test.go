package main

import (
	"bytes"
	"encoding/json"
	"fmt"
	"strings"
	"testing"

	"github.com/hyperledger/fabric/core/chaincode/shim"
)

//TestInvoiceAsset_Init
func TestInvoiceAsset_Init(t *testing.T) {
	invoice := new(InvoiceAsset)
	stub := shim.NewMockStub("invoice", invoice)
	checkInit(t, stub, [][]byte{[]byte("init")})
	checkState(t, stub, "invoiceIDIndex", getExpectedInvoiceIDIndex("init"))
}

//TestInvoiceAsset_InvokeUnknownFunction
func TestInvoiceAsset_InvokeUnknownFunction(t *testing.T) {
	invoice := new(InvoiceAsset)
	stub := shim.NewMockStub("invoice", invoice)
	checkInit(t, stub, [][]byte{[]byte("init")})
	checkInvokeUnknownFunction(t, stub, [][]byte{[]byte("myFunction"), []byte("docType:Asset")})
}

//TestInvoiceAsset_Invoke_addNewInvoice
func TestInvoiceAsset_Invoke_addNewInvoiceOK(t *testing.T) {
	invoice := new(InvoiceAsset)
	stub := shim.NewMockStub("invoice", invoice)
	checkInit(t, stub, [][]byte{[]byte("init")})
	checkInvoke(t, stub, getFirstInvoiceAssetForTesting())
	newInvoiceID := "IN100001"
	checkState(t, stub, newInvoiceID, getNewInvoiceExpected())
	checkState(t, stub, "invoiceIDIndex", getExpectedInvoiceIDIndex("addNewInvoice"))
}

//TestInvoiceAsset_Invoke_addNewInvoiceUnknownField
func TestInvoiceAsset_Invoke_addNewInvoiceUnknownField(t *testing.T) {
	invoice := new(InvoiceAsset)
	stub := shim.NewMockStub("invoice", invoice)
	checkInit(t, stub, [][]byte{[]byte("init")})
	res := stub.MockInvoke("1", getInvoiceAssetWithUnknownFieldForTesting())
	if res.Status != shim.OK {
		checkError(t, "Invoice Data is Corrupted", res.Message)
	} else {
		fmt.Println("Unknown Field Error was expected, but not raised")
		t.FailNow()
	}
}

//TestInvoiceAsset_Invoke_addNewInvoice
func TestInvoiceAsset_Invoke_addNewInvoiceDuplicate(t *testing.T) {
	invoice := new(InvoiceAsset)
	stub := shim.NewMockStub("invoice", invoice)
	checkInit(t, stub, [][]byte{[]byte("init")})
	checkInvoke(t, stub, getFirstInvoiceAssetForTesting())
	newInvoiceID := "IN100001"
	checkState(t, stub, newInvoiceID, getNewInvoiceExpected())
	checkState(t, stub, "invoiceIDIndex", getExpectedInvoiceIDIndex("addNewInvoice"))
	res := stub.MockInvoke("1", getFirstInvoiceAssetForTesting())
	if res.Status != shim.OK {
		checkError(t, "This Invoice already exists: IN100001", res.Message)
	} else {
		fmt.Println("Error was expected, but not raised")
		t.FailNow()
	}
}

//TestInvoiceAsset_Invoke_setFinancingRequestedOK
func TestInvoiceAsset_Invoke_setFinancingRequestedOK(t *testing.T) {
	invoice := new(InvoiceAsset)
	stub := shim.NewMockStub("invoice", invoice)
	checkInit(t, stub, [][]byte{[]byte("init")})
	checkInvoke(t, stub, getFirstInvoiceAssetForTesting())
	invoiceID := "IN100001"
	financerID := "FI100001"
	res := stub.MockInvoke("1", [][]byte{[]byte("setFinancingRequested"), []byte(invoiceID), []byte(financerID)})
	if res.Status != shim.OK {
		checkError(t, "Financing cannot be requested for Invoice with ID: "+invoiceID, res.Message)
	}
	res = stub.MockInvoke("1", [][]byte{[]byte("readInvoice"), []byte(invoiceID)})
	if res.Status != shim.OK {
		fmt.Println("func readInvoice with ID: ", invoiceID, " failed"+string(res.Message))
		t.FailNow()
	}
	if res.Payload == nil {
		fmt.Println("func readInvoice with ID: ", invoiceID, "failed to get value")
		t.FailNow()
	} else {
		var resInv Invoice
		err := json.Unmarshal(res.Payload, &resInv)
		if err != nil {
			fmt.Println(err.Error())
			t.FailNow()
		}
		checkError(t, FinancingRequested, resInv.Status)
	}
}

//TestInvoiceAsset_Invoke_setFinancingRequestedOK
func TestInvoiceAsset_Invoke_setFinancingRequestedNOK(t *testing.T) {
	invoice := new(InvoiceAsset)
	stub := shim.NewMockStub("invoice", invoice)
	checkInit(t, stub, [][]byte{[]byte("init")})
	checkInvoke(t, stub, getInvoiceForNOKTestSetFinancingRequested())
	invoiceID := "IN100001"
	financerID := "FI100001"
	res := stub.MockInvoke("1", [][]byte{[]byte("setFinancingRequested"), []byte(invoiceID), []byte(financerID)})
	if res.Status != shim.OK {
		checkError(t, "Financing cannot be requested for Invoice with ID: "+invoiceID, res.Message)
	}
	res = stub.MockInvoke("1", [][]byte{[]byte("readInvoice"), []byte(invoiceID)})
	if res.Status != shim.OK {
		fmt.Println("func readInvoice with ID: ", invoiceID, " failed"+string(res.Message))
		t.FailNow()
	}
	if res.Payload == nil {
		fmt.Println("func readInvoice with ID: ", invoiceID, "failed to get value")
		t.FailNow()
	} else {
		var resInv Invoice
		err := json.Unmarshal(res.Payload, &resInv)
		if err != nil {
			fmt.Println(err.Error())
			t.FailNow()
		}
		checkError(t, FinancingAccepted, resInv.Status)
	}
}

//TestInvoiceAsset_Invoke_setFinancingOfferedOK
func TestInvoiceAsset_Invoke_setFinancingOfferedOK(t *testing.T) {
	invoice := new(InvoiceAsset)
	stub := shim.NewMockStub("invoice", invoice)
	checkInit(t, stub, [][]byte{[]byte("init")})
	checkInvoke(t, stub, getFirstInvoiceAssetForTesting())
	invoiceID := "IN100001"
	res := stub.MockInvoke("1", [][]byte{[]byte("setFinancingOffered"), []byte(invoiceID)})
	if res.Status != shim.OK {
		checkError(t, "Financing cannot be offered for Invoice with ID: "+invoiceID, res.Message)
	}
	res = stub.MockInvoke("1", [][]byte{[]byte("readInvoice"), []byte(invoiceID)})
	if res.Status != shim.OK {
		fmt.Println("func readInvoice with ID: ", invoiceID, " failed"+string(res.Message))
		t.FailNow()
	}
	if res.Payload == nil {
		fmt.Println("func readInvoice with ID: ", invoiceID, "failed to get value")
		t.FailNow()
	} else {
		var resInv Invoice
		err := json.Unmarshal(res.Payload, &resInv)
		if err != nil {
			fmt.Println(err.Error())
			t.FailNow()
		}
		checkError(t, FinancingOffered, resInv.Status)
	}
}

//TestInvoiceAsset_Invoke_setFinancingOfferedOK
func TestInvoiceAsset_Invoke_setFinancingOfferedNOK(t *testing.T) {
	invoice := new(InvoiceAsset)
	stub := shim.NewMockStub("invoice", invoice)
	checkInit(t, stub, [][]byte{[]byte("init")})
	checkInvoke(t, stub, getInvoiceForNOKTestSetFinancingOffered())
	invoiceID := "IN100001"
	res := stub.MockInvoke("1", [][]byte{[]byte("setFinancingOffered"), []byte(invoiceID)})
	if res.Status != shim.OK {
		checkError(t, "Financing cannot be requested for Invoice with ID: "+invoiceID, res.Message)
	}
	res = stub.MockInvoke("1", [][]byte{[]byte("readInvoice"), []byte(invoiceID)})
	if res.Status != shim.OK {
		fmt.Println("func readInvoice with ID: ", invoiceID, " failed"+string(res.Message))
		t.FailNow()
	}
	if res.Payload == nil {
		fmt.Println("func readInvoice with ID: ", invoiceID, "failed to get value")
		t.FailNow()
	} else {
		var resInv Invoice
		err := json.Unmarshal(res.Payload, &resInv)
		if err != nil {
			fmt.Println(err.Error())
			t.FailNow()
		}
		checkError(t, FinancingAccepted, resInv.Status)
	}
}

//TestInvoiceAsset_Invoke_setFinancingAcceptedOK
func TestInvoiceAsset_Invoke_setFinancingAcceptedOK(t *testing.T) {
	invoice := new(InvoiceAsset)
	stub := shim.NewMockStub("invoice", invoice)
	checkInit(t, stub, [][]byte{[]byte("init")})
	checkInvoke(t, stub, getInvoiceForOKTestSetFinancingAccepted())
	invoiceID := "IN100001"
	res := stub.MockInvoke("1", [][]byte{[]byte("setFinancingAccepted"), []byte(invoiceID)})
	if res.Status != shim.OK {
		checkError(t, "Financing cannot be offered for Invoice with ID: "+invoiceID, res.Message)
	}
	res = stub.MockInvoke("1", [][]byte{[]byte("readInvoice"), []byte(invoiceID)})
	if res.Status != shim.OK {
		fmt.Println("func readInvoice with ID: ", invoiceID, " failed"+string(res.Message))
		t.FailNow()
	}
	if res.Payload == nil {
		fmt.Println("func readInvoice with ID: ", invoiceID, "failed to get value")
		t.FailNow()
	} else {
		var resInv Invoice
		err := json.Unmarshal(res.Payload, &resInv)
		if err != nil {
			fmt.Println(err.Error())
			t.FailNow()
		}
		checkError(t, FinancingAccepted, resInv.Status)
	}
}

//TestInvoiceAsset_Invoke_setFinancingAcceptedOK
func TestInvoiceAsset_Invoke_setFinancingAcceptedNOK(t *testing.T) {
	invoice := new(InvoiceAsset)
	stub := shim.NewMockStub("invoice", invoice)
	checkInit(t, stub, [][]byte{[]byte("init")})
	checkInvoke(t, stub, getInvoiceForNOKTestSetFinancingAccepted())
	invoiceID := "IN100001"
	res := stub.MockInvoke("1", [][]byte{[]byte("setFinancingAccepted"), []byte(invoiceID)})
	if res.Status != shim.OK {
		checkError(t, "Financing cannot be requested for Invoice with ID: "+invoiceID, res.Message)
	}
	res = stub.MockInvoke("1", [][]byte{[]byte("readInvoice"), []byte(invoiceID)})
	if res.Status != shim.OK {
		fmt.Println("func readInvoice with ID: ", invoiceID, " failed"+string(res.Message))
		t.FailNow()
	}
	if res.Payload == nil {
		fmt.Println("func readInvoice with ID: ", invoiceID, "failed to get value")
		t.FailNow()
	} else {
		var resInv Invoice
		err := json.Unmarshal(res.Payload, &resInv)
		if err != nil {
			fmt.Println(err.Error())
			t.FailNow()
		}
		checkError(t, FinancingAccepted, resInv.Status)
	}
}

//TestInvoiceAsset_Invoke_setFinancingCompletedOK
func TestInvoiceAsset_Invoke_setFinancingCompletedOK(t *testing.T) {
	invoice := new(InvoiceAsset)
	stub := shim.NewMockStub("invoice", invoice)
	checkInit(t, stub, [][]byte{[]byte("init")})
	checkInvoke(t, stub, getInvoiceForOKTestSetFinancingCompleted())
	invoiceID := "IN100001"
	res := stub.MockInvoke("1", [][]byte{[]byte("setFinancingCompleted"), []byte(invoiceID)})
	if res.Status != shim.OK {
		checkError(t, "Financing cannot be offered for Invoice with ID: "+invoiceID, res.Message)
	}
	res = stub.MockInvoke("1", [][]byte{[]byte("readInvoice"), []byte(invoiceID)})
	if res.Status != shim.OK {
		fmt.Println("func readInvoice with ID: ", invoiceID, " failed"+string(res.Message))
		t.FailNow()
	}
	if res.Payload == nil {
		fmt.Println("func readInvoice with ID: ", invoiceID, "failed to get value")
		t.FailNow()
	} else {
		var resInv Invoice
		err := json.Unmarshal(res.Payload, &resInv)
		if err != nil {
			fmt.Println(err.Error())
			t.FailNow()
		}
		checkError(t, FinancingCompleted, resInv.Status)
	}
}

//TestInvoiceAsset_Invoke_setFinancingCompletedNOK
func TestInvoiceAsset_Invoke_setFinancingCompletedNOK(t *testing.T) {
	invoice := new(InvoiceAsset)
	stub := shim.NewMockStub("invoice", invoice)
	checkInit(t, stub, [][]byte{[]byte("init")})
	checkInvoke(t, stub, getInvoiceForNOKTestSetFinancingCompleted())
	invoiceID := "IN100001"
	res := stub.MockInvoke("1", [][]byte{[]byte("setFinancingCompleted"), []byte(invoiceID)})
	if res.Status != shim.OK {
		checkError(t, "Financing cannot be requested for Invoice with ID: "+invoiceID, res.Message)
	}
	res = stub.MockInvoke("1", [][]byte{[]byte("readInvoice"), []byte(invoiceID)})
	if res.Status != shim.OK {
		fmt.Println("func readInvoice with ID: ", invoiceID, " failed"+string(res.Message))
		t.FailNow()
	}
	if res.Payload == nil {
		fmt.Println("func readInvoice with ID: ", invoiceID, "failed to get value")
		t.FailNow()
	} else {
		var resInv Invoice
		err := json.Unmarshal(res.Payload, &resInv)
		if err != nil {
			fmt.Println(err.Error())
			t.FailNow()
		}
		checkError(t, FinancingCompleted, resInv.Status)
	}
}

//TestInvoiceAsset_Invoke_removeInvoiceOK  //change template
func TestInvoiceAsset_Invoke_removeInvoiceOK(t *testing.T) {
	invoice := new(InvoiceAsset)
	stub := shim.NewMockStub("invoice", invoice)
	checkInit(t, stub, [][]byte{[]byte("init")})
	checkInvoke(t, stub, getFirstInvoiceAssetForTesting())
	checkInvoke(t, stub, getSecondInvoiceAssetForTesting())
	checkReadAllInvoicesOK(t, stub)
	checkState(t, stub, "invoiceIDIndex", getExpectedInvoiceIDIndex("beforeRemoveInvoice"))
	checkInvoke(t, stub, getRemoveSecondInvoiceAssetForTesting())
	remainingInvoiceID := "IN100001"
	checkReadInvoiceOK(t, stub, remainingInvoiceID)
	checkState(t, stub, "invoiceIDIndex", getExpectedInvoiceIDIndex("afterRemoveInvoice"))
}

//TestInvoiceAsset_Invoke_removeInvoiceNOK  //change template
func TestInvoiceAsset_Invoke_removeInvoiceNOK(t *testing.T) {
	invoice := new(InvoiceAsset)
	stub := shim.NewMockStub("invoice", invoice)
	checkInit(t, stub, [][]byte{[]byte("init")})
	checkInvoke(t, stub, getFirstInvoiceAssetForTesting())
	firstInvoiceID := "IN100001"
	checkReadInvoiceOK(t, stub, firstInvoiceID)
	checkState(t, stub, "invoiceIDIndex", getExpectedInvoiceIDIndex("addNewInvoice"))
	res := stub.MockInvoke("1", getRemoveSecondInvoiceAssetForTesting())
	if res.Status != shim.OK {
		checkError(t, "Invoice with ID: "+"IN100002"+" not found", res.Message)
	} else {
		fmt.Println("Error was expected, but not raised")
		t.FailNow()
	}
	checkState(t, stub, "invoiceIDIndex", getExpectedInvoiceIDIndex("addNewInvoice"))
}

//TestInvoiceAsset_Invoke_removeAllInvoicessOK
func TestInvoiceAsset_Invoke_removeAllInvoicessOK(t *testing.T) {
	financier := new(InvoiceAsset)
	stub := shim.NewMockStub("financier", financier)
	checkInit(t, stub, [][]byte{[]byte("init")})
	checkInvoke(t, stub, getFirstInvoiceAssetForTesting())
	checkInvoke(t, stub, getSecondInvoiceAssetForTesting())
	checkReadAllInvoicesOK(t, stub)
	checkState(t, stub, "invoiceIDIndex", getExpectedInvoiceIDIndex("beforeRemoveInvoice"))
	checkInvoke(t, stub, getRemoveAllInvoiceAssetsForTesting())
	checkState(t, stub, "invoiceIDIndex", getExpectedInvoiceIDIndex(""))
}

//TestInvoiceAsset_Query_readInvoice
func TestInvoiceAsset_Query_readInvoice(t *testing.T) {
	invoice := new(InvoiceAsset)
	stub := shim.NewMockStub("invoice", invoice)
	checkInit(t, stub, [][]byte{[]byte("init")})
	invoiceID := "IN100001"
	checkInvoke(t, stub, getFirstInvoiceAssetForTesting())
	checkReadInvoiceOK(t, stub, invoiceID)
	checkReadInvoiceNOK(t, stub, "")
}

//TestInvoiceAsset_Query_readAllInvoices
func TestInvoiceAsset_Query_readAllInvoices(t *testing.T) {
	invoice := new(InvoiceAsset)
	stub := shim.NewMockStub("invoice", invoice)
	checkInit(t, stub, [][]byte{[]byte("init")})
	checkInvoke(t, stub, getFirstInvoiceAssetForTesting())
	checkInvoke(t, stub, getSecondInvoiceAssetForTesting())
	checkReadAllInvoicesOK(t, stub)
}

/*
*
*	Helper Functions
*
 */
//Get first InvoiceAsset for testing
func getFirstInvoiceAssetForTesting() [][]byte {
	return [][]byte{[]byte("addNewInvoice"),
		[]byte("{\"ID\":\"IN100001\"," +
			"\"supplierID\":\"SU100001\"," +
			"\"purchaserID\":\"PU100001\"," +
			"\"financerID\":\"FI100001\"," +
			"\"purchaseOrderID\":\"PO100001\"," +
			"\"docType\":\"Asset.Invoice\"," +
			"\"status\":\"Approved\"," +
			"\"postingDate\":\"12/01/2017\"," +
			"\"approvalDate\":\"12/01/2017\"," +
			"\"paymentDate\":\"12/01/2017\"," +
			"\"disbursementDate\":\"12/01/2017\"," +
			"\"grossAmount\":\"100.00\"," +
			"\"discountPercent\":\"2.00\"," +
			"\"financedAmount\":\"98.00\"," +
			"\"currency\":\"SGD\"}")}
}

func getInvoiceForOKTestSetFinancingOffered() [][]byte {
	return [][]byte{[]byte("addNewInvoice"),
		[]byte("{\"ID\":\"IN100001\"," +
			"\"supplierID\":\"SU100001\"," +
			"\"purchaserID\":\"PU100001\"," +
			"\"financerID\":\"FI100001\"," +
			"\"purchaseOrderID\":\"PO100001\"," +
			"\"docType\":\"Asset.Invoice\"," +
			"\"status\":\"Financing Requested\"," +
			"\"postingDate\":\"12/01/2017\"," +
			"\"approvalDate\":\"12/01/2017\"," +
			"\"paymentDate\":\"12/01/2017\"," +
			"\"disbursementDate\":\"12/01/2017\"," +
			"\"grossAmount\":\"100.00\"," +
			"\"discountPercent\":\"2.00\"," +
			"\"financedAmount\":\"98.00\"," +
			"\"currency\":\"SGD\"}")}
}

func getInvoiceForOKTestSetFinancingAccepted() [][]byte {
	return [][]byte{[]byte("addNewInvoice"),
		[]byte("{\"ID\":\"IN100001\"," +
			"\"supplierID\":\"SU100001\"," +
			"\"purchaserID\":\"PU100001\"," +
			"\"financerID\":\"FI100001\"," +
			"\"purchaseOrderID\":\"PO100001\"," +
			"\"docType\":\"Asset.Invoice\"," +
			"\"status\":\"Financing Offered\"," +
			"\"postingDate\":\"12/01/2017\"," +
			"\"approvalDate\":\"12/01/2017\"," +
			"\"paymentDate\":\"12/01/2017\"," +
			"\"disbursementDate\":\"12/01/2017\"," +
			"\"grossAmount\":\"100.00\"," +
			"\"discountPercent\":\"2.00\"," +
			"\"financedAmount\":\"98.00\"," +
			"\"currency\":\"SGD\"}")}
}

func getInvoiceForOKTestSetFinancingCompleted() [][]byte {
	return [][]byte{[]byte("addNewInvoice"),
		[]byte("{\"ID\":\"IN100001\"," +
			"\"supplierID\":\"SU100001\"," +
			"\"purchaserID\":\"PU100001\"," +
			"\"financerID\":\"FI100001\"," +
			"\"purchaseOrderID\":\"PO100001\"," +
			"\"docType\":\"Asset.Invoice\"," +
			"\"status\":\"Financing Accepted\"," +
			"\"postingDate\":\"12/01/2017\"," +
			"\"approvalDate\":\"12/01/2017\"," +
			"\"paymentDate\":\"12/01/2017\"," +
			"\"disbursementDate\":\"12/01/2017\"," +
			"\"grossAmount\":\"100.00\"," +
			"\"discountPercent\":\"2.00\"," +
			"\"financedAmount\":\"98.00\"," +
			"\"currency\":\"SGD\"}")}
}

func getInvoiceForNOKTestSetFinancingRequested() [][]byte {
	return [][]byte{[]byte("addNewInvoice"),
		[]byte("{\"ID\":\"IN100001\"," +
			"\"supplierID\":\"SU100001\"," +
			"\"purchaserID\":\"PU100001\"," +
			"\"financerID\":\"FI100001\"," +
			"\"purchaseOrderID\":\"PO100001\"," +
			"\"docType\":\"Asset.Invoice\"," +
			"\"status\":\"Financing Accepted\"," +
			"\"postingDate\":\"12/01/2017\"," +
			"\"approvalDate\":\"12/01/2017\"," +
			"\"paymentDate\":\"12/01/2017\"," +
			"\"disbursementDate\":\"12/01/2017\"," +
			"\"grossAmount\":\"100.00\"," +
			"\"discountPercent\":\"2.00\"," +
			"\"financedAmount\":\"98.00\"," +
			"\"currency\":\"SGD\"}")}
}

func getInvoiceForNOKTestSetFinancingOffered() [][]byte {
	return [][]byte{[]byte("addNewInvoice"),
		[]byte("{\"ID\":\"IN100001\"," +
			"\"supplierID\":\"SU100001\"," +
			"\"purchaserID\":\"PU100001\"," +
			"\"financerID\":\"FI100001\"," +
			"\"purchaseOrderID\":\"PO100001\"," +
			"\"docType\":\"Asset.Invoice\"," +
			"\"status\":\"Financing Accepted\"," +
			"\"postingDate\":\"12/01/2017\"," +
			"\"approvalDate\":\"12/01/2017\"," +
			"\"paymentDate\":\"12/01/2017\"," +
			"\"disbursementDate\":\"12/01/2017\"," +
			"\"grossAmount\":\"100.00\"," +
			"\"discountPercent\":\"2.00\"," +
			"\"financedAmount\":\"98.00\"," +
			"\"currency\":\"SGD\"}")}
}

func getInvoiceForNOKTestSetFinancingAccepted() [][]byte {
	return [][]byte{[]byte("addNewInvoice"),
		[]byte("{\"ID\":\"IN100001\"," +
			"\"supplierID\":\"SU100001\"," +
			"\"purchaserID\":\"PU100001\"," +
			"\"financerID\":\"FI100001\"," +
			"\"purchaseOrderID\":\"PO100001\"," +
			"\"docType\":\"Asset.Invoice\"," +
			"\"status\":\"Financing Accepted\"," +
			"\"postingDate\":\"12/01/2017\"," +
			"\"approvalDate\":\"12/01/2017\"," +
			"\"paymentDate\":\"12/01/2017\"," +
			"\"disbursementDate\":\"12/01/2017\"," +
			"\"grossAmount\":\"100.00\"," +
			"\"discountPercent\":\"2.00\"," +
			"\"financedAmount\":\"98.00\"," +
			"\"currency\":\"SGD\"}")}
}

func getInvoiceForNOKTestSetFinancingCompleted() [][]byte {
	return [][]byte{[]byte("addNewInvoice"),
		[]byte("{\"ID\":\"IN100001\"," +
			"\"supplierID\":\"SU100001\"," +
			"\"purchaserID\":\"PU100001\"," +
			"\"financerID\":\"FI100001\"," +
			"\"purchaseOrderID\":\"PO100001\"," +
			"\"docType\":\"Asset.Invoice\"," +
			"\"status\":\"Financing Completed\"," +
			"\"postingDate\":\"12/01/2017\"," +
			"\"approvalDate\":\"12/01/2017\"," +
			"\"paymentDate\":\"12/01/2017\"," +
			"\"disbursementDate\":\"12/01/2017\"," +
			"\"grossAmount\":\"100.00\"," +
			"\"discountPercent\":\"2.00\"," +
			"\"financedAmount\":\"98.00\"," +
			"\"currency\":\"SGD\"}")}
}

//Get InvoiceAsset with unknown field for testing
func getInvoiceAssetWithUnknownFieldForTesting() [][]byte {
	return [][]byte{[]byte("addNewInvoice"),
		[]byte("{\"ID\":\"IN100001\"," +
			"\"supplierID\":\"SU100001\"," +
			"\"purchaserID\":\"PU100001\"," +
			"\"financerID\":\"FI100001\"," +
			"\"purchaseOrderID\":\"PO100001\"," +
			"\"docuType\":\"Asset.Invoice\"," +
			"\"status\":\"Approved\"," +
			"\"postingDate\":\"12/01/2017\"," +
			"\"approvalDate\":\"12/01/2017\"," +
			"\"paymentDate\":\"12/01/2017\"," +
			"\"disbursementDate\":\"12/01/2017\"," +
			"\"grossAmount\":\"100.00\"," +
			"\"discountPercent\":\"2.00\"," +
			"\"financedAmount\":\"98.00\"," +
			"\"currency\":\"SGD\"}")}
}

//Get second InvoiceAsset for testing
func getSecondInvoiceAssetForTesting() [][]byte {
	return [][]byte{[]byte("addNewInvoice"),
		[]byte("{\"ID\":\"IN100002\"," +
			"\"supplierID\":\"SU100002\"," +
			"\"purchaserID\":\"PU100002\"," +
			"\"financerID\":\"FI100002\"," +
			"\"purchaseOrderID\":\"PO100002\"," +
			"\"docType\":\"Asset.Invoice\"," +
			"\"status\":\"Approved\"," +
			"\"postingDate\":\"12/01/2017\"," +
			"\"approvalDate\":\"12/01/2017\"," +
			"\"paymentDate\":\"12/01/2017\"," +
			"\"disbursementDate\":\"12/01/2017\"," +
			"\"grossAmount\":\"100.00\"," +
			"\"discountPercent\":\"2.00\"," +
			"\"financedAmount\":\"98.00\"," +
			"\"currency\":\"SGD\"}")}
}

//Get remove second InvoiceAsset for testing //change template
func getRemoveSecondInvoiceAssetForTesting() [][]byte {
	return [][]byte{[]byte("removeInvoice"),
		[]byte("IN100002")}
}

//Get remove all InvoiceAssets for testing
func getRemoveAllInvoiceAssetsForTesting() [][]byte {
	return [][]byte{[]byte("removeAllInvoices")}
}

//Get an expected value for testing
func getNewInvoiceExpected() []byte {
	var invoice Invoice
	invoice.ID = "IN100001"
	invoice.SupplierID = "SU100001"
	invoice.PurchaserID = "PU100001"
	invoice.FinancerID = "FI100001"
	invoice.PurchaseOrderID = "PO100001"
	invoice.ObjectType = "Asset.Invoice"
	invoice.Status = "Approved"
	invoice.PostingDate = "12/01/2017"
	invoice.ApprovalDate = "12/01/2017"
	invoice.PaymentDate = "12/01/2017"
	invoice.DisbursementDate = "12/01/2017"
	invoice.GrossAmount = "100.00"
	invoice.DiscountPercent = "2.00"
	invoice.FinancedAmount = "98.00"
	invoice.Currency = "SGD"
	invoiceJSON, err := json.Marshal(invoice)
	if err != nil {
		fmt.Println("Error converting a Invoice record to JSON")
		return nil
	}
	return []byte(invoiceJSON)
}

//Get expected values of Invoices for testing
func getExpectedInvoices() []byte {
	var invoices []Invoice
	var invoice Invoice
	invoice.ID = "IN100001"
	invoice.SupplierID = "SU100001"
	invoice.PurchaserID = "PU100001"
	invoice.FinancerID = "FI100001"
	invoice.PurchaseOrderID = "PO100001"
	invoice.ObjectType = "Asset.Invoice"
	invoice.Status = "Approved"
	invoice.PostingDate = "12/01/2017"
	invoice.ApprovalDate = "12/01/2017"
	invoice.PaymentDate = "12/01/2017"
	invoice.DisbursementDate = "12/01/2017"
	invoice.GrossAmount = "100.00"
	invoice.DiscountPercent = "2.00"
	invoice.FinancedAmount = "98.00"
	invoice.Currency = "SGD"
	invoices = append(invoices, invoice)
	invoice.ID = "IN100002"
	invoice.SupplierID = "SU100002"
	invoice.PurchaserID = "PU100002"
	invoice.FinancerID = "FI100002"
	invoice.PurchaseOrderID = "PO100002"
	invoice.ObjectType = "Asset.Invoice"
	invoice.Status = "Approved"
	invoice.PostingDate = "12/01/2017"
	invoice.ApprovalDate = "12/01/2017"
	invoice.PaymentDate = "12/01/2017"
	invoice.DisbursementDate = "12/01/2017"
	invoice.GrossAmount = "100.00"
	invoice.DiscountPercent = "2.00"
	invoice.FinancedAmount = "98.00"
	invoice.Currency = "SGD"
	invoices = append(invoices, invoice)
	invoiceJSON, err := json.Marshal(invoices)
	if err != nil {
		fmt.Println("Error converting invoice records to JSON")
		return nil
	}
	return []byte(invoiceJSON)
}

func getExpectedInvoiceIDIndex(funcName string) []byte {
	var invoiceIDIndex InvoiceIDIndex
	switch funcName {
	case "addNewInvoice":
		invoiceIDIndex.IDs = append(invoiceIDIndex.IDs, "IN100001")
		invoiceIDIndexBytes, err := json.Marshal(invoiceIDIndex)
		if err != nil {
			fmt.Println("Error converting InvoiceIDIndex to JSON")
			return nil
		}
		return invoiceIDIndexBytes
	case "beforeRemoveInvoice":
		invoiceIDIndex.IDs = append(invoiceIDIndex.IDs, "IN100001")
		invoiceIDIndex.IDs = append(invoiceIDIndex.IDs, "IN100002")
		invoiceIDIndexBytes, err := json.Marshal(invoiceIDIndex)
		if err != nil {
			fmt.Println("Error converting InvoiceIDIndex to JSON")
			return nil
		}
		return invoiceIDIndexBytes
	case "afterRemoveInvoice":
		invoiceIDIndex.IDs = append(invoiceIDIndex.IDs, "IN100001")
		invoiceIDIndexBytes, err := json.Marshal(invoiceIDIndex)
		if err != nil {
			fmt.Println("Error converting InvoiceIDIndex to JSON")
			return nil
		}
		return invoiceIDIndexBytes
	default:
		invoiceIDIndexBytes, err := json.Marshal(invoiceIDIndex)
		if err != nil {
			fmt.Println("Error converting InvoiceIDIndex to JSON")
			return nil
		}
		return invoiceIDIndexBytes
	}
}

//checkInit - helper to check the Initialization of chaincode: InvoiceAsset
func checkInit(t *testing.T, stub *shim.MockStub, args [][]byte) {
	res := stub.MockInit("1", args)
	if res.Status != shim.OK {
		fmt.Println("Init failed", string(res.Message))
		t.FailNow()
	}
}

//checkState - helper for checking the chaincode state for a given stateKey afgainst an expected value
func checkState(t *testing.T, stub *shim.MockStub, stateKey string, expectedState []byte) {
	actualState := stub.State[stateKey]
	if actualState == nil {
		fmt.Println("State for ", stateKey, ": failed to get value")
		t.FailNow()
	}

	if bytes.Compare(actualState, expectedState) != 0 {
		fmt.Println("Incorrect State for " + stateKey + ": \nExpected: " + string(expectedState) + "\nActual  : " + string(actualState))
		t.FailNow()
	}
}

//checkInvoke - helper for checking Invoke of chaincode
func checkInvoke(t *testing.T, stub *shim.MockStub, args [][]byte) {
	res := stub.MockInvoke("1", args)
	if res.Status != shim.OK {
		fmt.Println("Invoke", args, "failed", string(res.Message))
		t.FailNow()
	}
}

func checkInvokeUnknownFunction(t *testing.T, stub *shim.MockStub, args [][]byte) {
	res := stub.MockInvoke("1", args)
	if res.Status != shim.OK {
		expectedErr := "Received unknown function invocation"
		actualErr := string(res.Message)
		if !(strings.Contains(actualErr, expectedErr)) {
			fmt.Println("Invoke with unknown function must throw error: ", expectedErr, "\n Actual Error :", actualErr)
			t.FailNow()
		}
	} else {
		t.FailNow()
	}
}

//checkReadInvoiceOK - helper for positive test readInvoice
func checkReadInvoiceOK(t *testing.T, stub *shim.MockStub, invoiceID string) {
	res := stub.MockInvoke("1", [][]byte{[]byte("readInvoice"), []byte(invoiceID)})
	if res.Status != shim.OK {
		fmt.Println("func readInvoice with ID: ", invoiceID, " failed"+string(res.Message))
		t.FailNow()
	}
	if res.Payload == nil {
		fmt.Println("func readInvoice with ID: ", invoiceID, "failed to get value")
		t.FailNow()
	}
	if bytes.Compare(getNewInvoiceExpected(), []byte(res.Payload)) != 0 {
		fmt.Println("func readInvoice with ID: ", invoiceID, "Expected:", string(getNewInvoiceExpected()), "Actual:", string(res.Payload))
		t.FailNow()
	}
}

//checkReadInvoiceNOK - helper for negative testing of readInvoice
func checkReadInvoiceNOK(t *testing.T, stub *shim.MockStub, invoiceID string) {
	//with no invoiceID
	res := stub.MockInvoke("1", [][]byte{[]byte("readInvoice"), []byte("")})
	if res.Status != shim.OK {
		expectedErr := "retrieveInvoice: Corrupt invoice record "
		actualErr := string(res.Message)
		if !(strings.Contains(actualErr, expectedErr)) {
			fmt.Println("func readInvoice negative test: ", "Expected Error:", expectedErr, "Actual Error", actualErr)
			t.FailNow()
		}
	} else {
		t.FailNow()
	}
}

func checkReadAllInvoicesOK(t *testing.T, stub *shim.MockStub) {
	res := stub.MockInvoke("1", [][]byte{[]byte("readAllInvoices")})
	if res.Status != shim.OK {
		fmt.Println("func readAllInvoices failed", string(res.Message))
		t.FailNow()
	}
	if res.Payload == nil {
		fmt.Println("func readAllInvoices failed to get value")
		t.FailNow()
	}
	if bytes.Compare(getExpectedInvoices(), []byte(res.Payload)) != 0 {
		fmt.Println("func readAllInvoices Expected:\n", string(getExpectedInvoices()), "\nActual:\n", string(res.Payload))
		t.FailNow()
	}
}

func checkError(t *testing.T, exp string, act string) {
	if strings.Compare(exp, act) != 0 {
		fmt.Println("Unexpected Error! Expecting ", exp, "\n Actual :", act)
		t.FailNow()
	}
}
