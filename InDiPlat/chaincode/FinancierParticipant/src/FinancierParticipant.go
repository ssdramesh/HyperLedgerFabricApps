package main

import (
	"encoding/json"
	"errors"
	"fmt"
	"strings"

	"github.com/hyperledger/fabric/core/chaincode/shim"
	"github.com/hyperledger/fabric/protos/peer"
)

var logger = shim.NewLogger("CLDChaincode")

//FinancierParticipant - Chaincode for Financier Participant
type FinancierParticipant struct {
}

//Financier - Details of the participant type Financier
type Financier struct {
	ID          string `json:"ID"`
	ObjectType  string `json:"docType"`
	Alias       string `json:"alias"`
	Description string `json:"description"`
}

//FinancierIDIndex - Index on IDs for retrieval all Financiers
type FinancierIDIndex struct {
	IDs []string `json:"IDs"`
}

func main() {
	err := shim.Start(new(FinancierParticipant))
	if err != nil {
		fmt.Printf("Error starting FinancierParticipant chaincode function main(): %s", err)
	} else {
		fmt.Printf("Starting FinancierParticipant chaincode function main() executed successfully")
	}
}

//Init - The chaincode Init function: No  arguments, only initializes a ID array as Index for retrieval of all Financiers
func (fin *FinancierParticipant) Init(stub shim.ChaincodeStubInterface) peer.Response {
	var financierIDIndex FinancierIDIndex
	record, _ := stub.GetState("financierIDIndex")
	//chaincode upgrade: Index might already exist, create new only if it does not
	if record == nil {
		bytes, _ := json.Marshal(financierIDIndex)
		stub.PutState("financierIDIndex", bytes)
	}
	return shim.Success(nil)
}

//Invoke - The chaincode Invoke function:
func (fin *FinancierParticipant) Invoke(stub shim.ChaincodeStubInterface) peer.Response {
	function, args := stub.GetFunctionAndParameters()
	logger.Debug("function: ", function)
	switch function {
	case "addNewFinancier":
		return fin.addNewFinancier(stub, args)
	case "removeFinancier":
		return fin.removeFinancier(stub, args[0])
	case "removeAllFinanciers":
		return fin.removeAllFinanciers(stub)
	case "readFinancier":
		return fin.readFinancier(stub, args[0])
	case "readAllFinanciers":
		return fin.readAllFinanciers(stub)
	default:
		return shim.Error("Received unknown function invocation")
	}
}

//Invoke Route: addNewFinancier
func (fin *FinancierParticipant) addNewFinancier(stub shim.ChaincodeStubInterface, args []string) peer.Response {
	sample, err := getFinancierFromArgs(args)
	if err != nil {
		return shim.Error("Financier Data is Corrupted")
	}
	sample.ObjectType = "Participant.Financier"
	record, err := stub.GetState(sample.ID)
	if record != nil {
		return shim.Error("This Financier already exists: " + sample.ID)
	}
	_, err = fin.saveFinancier(stub, sample)
	if err != nil {
		return shim.Error(err.Error())
	}
	_, err = fin.updateFinancierIDIndex(stub, sample)
	if err != nil {
		return shim.Error(err.Error())
	}
	return shim.Success(nil)
}

//Invoke Route: removeFinancier
func (fin *FinancierParticipant) removeFinancier(stub shim.ChaincodeStubInterface, sampleStructParticipantID string) peer.Response {
	_, err := fin.deleteFinancier(stub, sampleStructParticipantID)
	if err != nil {
		return shim.Error(err.Error())
	}
	_, err = fin.deleteFinancierIDIndex(stub, sampleStructParticipantID)
	if err != nil {
		return shim.Error(err.Error())
	}
	return shim.Success(nil)
}

//Invoke Route: removeAllFinanciers
func (fin *FinancierParticipant) removeAllFinanciers(stub shim.ChaincodeStubInterface) peer.Response {
	var financierStructIDs FinancierIDIndex
	bytes, err := stub.GetState("financierIDIndex")
	if err != nil {
		return shim.Error("removeAllFinanciers: Error getting financierIDIndex array")
	}
	err = json.Unmarshal(bytes, &financierStructIDs)
	if err != nil {
		return shim.Error("removeAllFinanciers: Error unmarshalling financierIDIndex array JSON")
	}
	if len(financierStructIDs.IDs) == 0 {
		return shim.Error("removeAllFinanciers: No financiers to remove")
	}
	for _, financierStructParticipantID := range financierStructIDs.IDs {
		_, err = fin.deleteFinancier(stub, financierStructParticipantID)
		if err != nil {
			return shim.Error("Failed to remove Financier with ID: " + financierStructParticipantID)
		}
		_, err = fin.deleteFinancierIDIndex(stub, financierStructParticipantID)
		if err != nil {
			return shim.Error(err.Error())
		}
	}
	fin.initHolder(stub)
	return shim.Success(nil)
}

//Query Route: readFinancier
func (fin *FinancierParticipant) readFinancier(stub shim.ChaincodeStubInterface, sampleParticipantID string) peer.Response {
	sampleAsByteArray, err := fin.retrieveFinancier(stub, sampleParticipantID)
	if err != nil {
		return shim.Error(err.Error())
	}
	return shim.Success(sampleAsByteArray)
}

//Query Route: readAllFinanciers
func (fin *FinancierParticipant) readAllFinanciers(stub shim.ChaincodeStubInterface) peer.Response {
	var sampleIDs FinancierIDIndex
	bytes, err := stub.GetState("financierIDIndex")
	if err != nil {
		return shim.Error("readAllFinanciers: Error getting financierIDIndex array")
	}
	err = json.Unmarshal(bytes, &sampleIDs)
	if err != nil {
		return shim.Error("readAllFinanciers: Error unmarshalling financierIDIndex array JSON")
	}
	result := "["

	var sampleAsByteArray []byte

	for _, sampleID := range sampleIDs.IDs {
		sampleAsByteArray, err = fin.retrieveFinancier(stub, sampleID)
		if err != nil {
			return shim.Error("Failed to retrieve sample with ID: " + sampleID)
		}
		result += string(sampleAsByteArray) + ","
	}
	if len(result) == 1 {
		result = "[]"
	} else {
		result = result[:len(result)-1] + "]"
	}
	return shim.Success([]byte(result))
}

//Helper: Save sampleParticipant
func (fin *FinancierParticipant) saveFinancier(stub shim.ChaincodeStubInterface, sample Financier) (bool, error) {
	bytes, err := json.Marshal(sample)
	if err != nil {
		return false, errors.New("Error converting sample record JSON")
	}
	err = stub.PutState(sample.ID, bytes)
	if err != nil {
		return false, errors.New("Error storing Financier record")
	}
	return true, nil
}

//Helper: Delete sampleStructParticipant
func (fin *FinancierParticipant) deleteFinancier(stub shim.ChaincodeStubInterface, sampleStructParticipantID string) (bool, error) {
	_, err := fin.retrieveFinancier(stub, sampleStructParticipantID)
	if err != nil {
		return false, errors.New("Financier with ID: " + sampleStructParticipantID + " not found")
	}
	err = stub.DelState(sampleStructParticipantID)
	if err != nil {
		return false, errors.New("Error deleting Financier record")
	}
	return true, nil
}

//Helper: Update sample Holder - updates Index
func (fin *FinancierParticipant) updateFinancierIDIndex(stub shim.ChaincodeStubInterface, sample Financier) (bool, error) {
	var sampleIDs FinancierIDIndex
	bytes, err := stub.GetState("financierIDIndex")
	if err != nil {
		return false, errors.New("upadeteFinancierIDIndex: Error getting financierIDIndex array Index from state")
	}
	err = json.Unmarshal(bytes, &sampleIDs)
	if err != nil {
		return false, errors.New("upadeteFinancierIDIndex: Error unmarshalling financierIDIndex array JSON")
	}
	sampleIDs.IDs = append(sampleIDs.IDs, sample.ID)
	bytes, err = json.Marshal(sampleIDs)
	if err != nil {
		return false, errors.New("updateFinancierIDIndex: Error marshalling new sample ID")
	}
	err = stub.PutState("financierIDIndex", bytes)
	if err != nil {
		return false, errors.New("updateFinancierIDIndex: Error storing new sample ID in financierIDIndex (Index)")
	}
	return true, nil
}

//Helper: delete ID from sampleStruct Holder
func (fin *FinancierParticipant) deleteFinancierIDIndex(stub shim.ChaincodeStubInterface, sampleStructParticipantID string) (bool, error) {
	var sampleStructIDs FinancierIDIndex
	bytes, err := stub.GetState("financierIDIndex")
	if err != nil {
		return false, errors.New("deleteFinancierIDIndex: Error getting financierIDIndex array Index from state")
	}
	err = json.Unmarshal(bytes, &sampleStructIDs)
	if err != nil {
		return false, errors.New("deleteFinancierIDIndex: Error unmarshalling financierIDIndex array JSON")
	}
	sampleStructIDs.IDs, err = deleteKeyFromStringArray(sampleStructIDs.IDs, sampleStructParticipantID)
	if err != nil {
		return false, errors.New(err.Error())
	}
	bytes, err = json.Marshal(sampleStructIDs)
	if err != nil {
		return false, errors.New("deleteFinancierIDIndex: Error marshalling new sampleStruct ID")
	}
	err = stub.PutState("financierIDIndex", bytes)
	if err != nil {
		return false, errors.New("deleteFinancierIDIndex: Error storing new sampleStruct ID in financierIDIndex (Index)")
	}
	return true, nil
}

//Helper: Initialize sample ID Holder
func (fin *FinancierParticipant) initHolder(stub shim.ChaincodeStubInterface) bool {
	var financierIDIndex FinancierIDIndex
	bytes, _ := json.Marshal(financierIDIndex)
	stub.DelState("financierIDIndex")
	stub.PutState("financierIDIndex", bytes)
	return true
}

//Helper: Retrieve sampleParticipant
func (fin *FinancierParticipant) retrieveFinancier(stub shim.ChaincodeStubInterface, sampleParticipantID string) ([]byte, error) {
	var sample Financier
	var sampleAsByteArray []byte
	bytes, err := stub.GetState(sampleParticipantID)
	if err != nil {
		return sampleAsByteArray, errors.New("retrieveFinancier: Error retrieving sample with ID: " + sampleParticipantID)
	}
	err = json.Unmarshal(bytes, &sample)
	if err != nil {
		return sampleAsByteArray, errors.New("retrieveFinancier: Corrupt sample record " + string(bytes))
	}
	sampleAsByteArray, err = json.Marshal(sample)
	if err != nil {
		return sampleAsByteArray, errors.New("readFinancier: Invalid sample Object - Not a  valid JSON")
	}
	return sampleAsByteArray, nil
}

//deleteKeyFromArray
func deleteKeyFromStringArray(array []string, key string) (newArray []string, err error) {
	for _, entry := range array {
		if entry != key {
			newArray = append(newArray, entry)
		}
	}
	if len(newArray) == len(array) {
		return newArray, errors.New("Specified Key: " + key + " not found in Array")
	}
	return newArray, nil
}

//getFinancierFromArgs - construct a sample structure from string array of arguments
func getFinancierFromArgs(args []string) (sample Financier, err error) {

	if strings.Contains(args[0], "\"ID\"") == false ||
		strings.Contains(args[0], "\"docType\"") == false ||
		strings.Contains(args[0], "\"alias\"") == false ||
		strings.Contains(args[0], "\"description\"") == false {
		return sample, errors.New("Unknown field: " + "Input JSON does not comply to schema")
	}

	err = json.Unmarshal([]byte(args[0]), &sample)
	if err != nil {
		return sample, err
	}
	return sample, nil

}
