package main

import (
	"bytes"
	"encoding/json"
	"fmt"
	"strings"
	"testing"

	"github.com/hyperledger/fabric/core/chaincode/shim"
)

//TestFinancierParticipant_Init
func TestFinancierParticipant_Init(t *testing.T) {
	sample := new(FinancierParticipant)
	stub := shim.NewMockStub("sample", sample)
	checkInit(t, stub, [][]byte{[]byte("init")})
	checkState(t, stub, "financierIDIndex", getExpectedFinancierIDIndex("init"))
}

//TestFinancierParticipant_InvokeUnknownFunction
func TestFinancierParticipant_InvokeUnknownFunction(t *testing.T) {
	sample := new(FinancierParticipant)
	stub := shim.NewMockStub("sample", sample)
	checkInit(t, stub, [][]byte{[]byte("init")})
	checkInvokeUnknownFunction(t, stub, [][]byte{[]byte("myFunction"), []byte("docType:Participant")})
}

//TestFinancierParticipant_Invoke_addNewFinancier
func TestFinancierParticipant_Invoke_addNewFinancierOK(t *testing.T) {
	sample := new(FinancierParticipant)
	stub := shim.NewMockStub("sample", sample)
	checkInit(t, stub, [][]byte{[]byte("init")})
	checkInvoke(t, stub, getFirstFinancierParticipantForTesting())
	newFinancierID := "100001"
	checkState(t, stub, newFinancierID, getNewFinancierExpected())
	checkState(t, stub, "financierIDIndex", getExpectedFinancierIDIndex("addNewFinancier"))
}

//TestFinancierParticipant_Invoke_addNewFinancierUnknownField
func TestFinancierParticipant_Invoke_addNewFinancierUnknownField(t *testing.T) {
	sample := new(FinancierParticipant)
	stub := shim.NewMockStub("sample", sample)
	checkInit(t, stub, [][]byte{[]byte("init")})
	res := stub.MockInvoke("1", getFinancierParticipantWithUnknownFieldForTesting())
	if res.Status != shim.OK {
		checkError(t, "Financier Data is Corrupted", res.Message)
	} else {
		fmt.Println("Unknown Field Error was expected, but not raised")
		t.FailNow()
	}
}

//TestFinancierParticipant_Invoke_addNewFinancier
func TestFinancierParticipant_Invoke_addNewFinancierDuplicate(t *testing.T) {
	sample := new(FinancierParticipant)
	stub := shim.NewMockStub("sample", sample)
	checkInit(t, stub, [][]byte{[]byte("init")})
	checkInvoke(t, stub, getFirstFinancierParticipantForTesting())
	newFinancierID := "100001"
	checkState(t, stub, newFinancierID, getNewFinancierExpected())
	checkState(t, stub, "financierIDIndex", getExpectedFinancierIDIndex("addNewFinancier"))
	res := stub.MockInvoke("1", getFirstFinancierParticipantForTesting())
	if res.Status != shim.OK {
		checkError(t, "This Financier already exists: 100001", res.Message)
	} else {
		fmt.Println("Error was expected, but not raised")
		t.FailNow()
	}
}

//TestFinancierParticipant_Invoke_removeFinancierSingleOK  //change template
func TestFinancierParticipant_Invoke_removeFinancierSingleOK(t *testing.T) {
	sample := new(FinancierParticipant)
	stub := shim.NewMockStub("sample", sample)
	checkInit(t, stub, [][]byte{[]byte("init")})
	checkInvoke(t, stub, getSecondFinancierParticipantForTesting())
	checkInvoke(t, stub, getRemoveSecondFinancierParticipantForTesting())
	checkState(t, stub, "financierIDIndex", getExpectedFinancierIDIndex(""))
	checkReadFinancierNOK(t, stub, "100002")
}

//TestFinancierParticipant_Invoke_removeFinancierOK  //change template
func TestFinancierParticipant_Invoke_removeFinancierOK(t *testing.T) {
	sample := new(FinancierParticipant)
	stub := shim.NewMockStub("sample", sample)
	checkInit(t, stub, [][]byte{[]byte("init")})
	checkInvoke(t, stub, getFirstFinancierParticipantForTesting())
	checkInvoke(t, stub, getSecondFinancierParticipantForTesting())
	checkReadAllFinanciersOK(t, stub)
	checkState(t, stub, "financierIDIndex", getExpectedFinancierIDIndex("beforeRemoveFinancier"))
	checkInvoke(t, stub, getRemoveSecondFinancierParticipantForTesting())
	remainingFinancierID := "100001"
	checkReadFinancierOK(t, stub, remainingFinancierID)
	checkState(t, stub, "financierIDIndex", getExpectedFinancierIDIndex("afterRemoveFinancier"))
}

//TestFinancierParticipant_Invoke_removeFinancierNOK  //change template
func TestFinancierParticipant_Invoke_removeFinancierNOK(t *testing.T) {
	sample := new(FinancierParticipant)
	stub := shim.NewMockStub("sample", sample)
	checkInit(t, stub, [][]byte{[]byte("init")})
	checkInvoke(t, stub, getFirstFinancierParticipantForTesting())
	firstFinancierID := "100001"
	checkReadFinancierOK(t, stub, firstFinancierID)
	checkState(t, stub, "financierIDIndex", getExpectedFinancierIDIndex("addNewFinancier"))
	res := stub.MockInvoke("1", getRemoveSecondFinancierParticipantForTesting())
	if res.Status != shim.OK {
		checkError(t, "Financier with ID: "+"100002"+" not found", res.Message)
	} else {
		fmt.Println("Error was expected, but not raised")
		t.FailNow()
	}
	checkState(t, stub, "financierIDIndex", getExpectedFinancierIDIndex("addNewFinancier"))
}

//TestFinancierParticipant_Invoke_removeAllFinanciersOK  //change template
func TestFinancierParticipant_Invoke_removeAllFinanciersOK(t *testing.T) {
	financier := new(FinancierParticipant)
	stub := shim.NewMockStub("financier", financier)
	checkInit(t, stub, [][]byte{[]byte("init")})
	checkInvoke(t, stub, getFirstFinancierParticipantForTesting())
	checkInvoke(t, stub, getSecondFinancierParticipantForTesting())
	checkReadAllFinanciersOK(t, stub)
	checkState(t, stub, "financierIDIndex", getExpectedFinancierIDIndex("beforeRemoveFinancier"))
	checkInvoke(t, stub, getRemoveAllFinancierParticipantsForTesting())
	checkState(t, stub, "financierIDIndex", getExpectedFinancierIDIndex(""))
}

//TestFinancierParticipant_Query_readFinancier
func TestFinancierParticipant_Query_readFinancier(t *testing.T) {
	sample := new(FinancierParticipant)
	stub := shim.NewMockStub("sample", sample)
	checkInit(t, stub, [][]byte{[]byte("init")})
	sampleID := "100001"
	checkInvoke(t, stub, getFirstFinancierParticipantForTesting())
	checkReadFinancierOK(t, stub, sampleID)
	checkReadFinancierNOK(t, stub, "")
}

//TestFinancierParticipant_Query_readAllFinanciers
func TestFinancierParticipant_Query_readAllFinanciers(t *testing.T) {
	sample := new(FinancierParticipant)
	stub := shim.NewMockStub("sample", sample)
	checkInit(t, stub, [][]byte{[]byte("init")})
	checkInvoke(t, stub, getFirstFinancierParticipantForTesting())
	checkInvoke(t, stub, getSecondFinancierParticipantForTesting())
	checkReadAllFinanciersOK(t, stub)
}

/*
*
*	Helper Functions
*
 */
//Get first FinancierParticipant for testing
func getFirstFinancierParticipantForTesting() [][]byte {
	return [][]byte{[]byte("addNewFinancier"),
		[]byte("{\"ID\":\"100001\",\"docType\":\"Participant.Financier\",\"alias\":\"BRITECH\",\"description\":\"British Technology Pvt. Ltd.\"}")}
}

//Get second FinancierParticipant for testing
func getSecondFinancierParticipantForTesting() [][]byte {
	return [][]byte{[]byte("addNewFinancier"),
		[]byte("{\"ID\":\"100002\",\"docType\":\"Participant.Financier\",\"alias\":\"DEMAGDELAG\",\"description\":\"Demag Delawal AG\"}")}
}

//Get FinancierParticipant with unknown field for testing
func getFinancierParticipantWithUnknownFieldForTesting() [][]byte {
	return [][]byte{[]byte("addNewFinancier"),
		[]byte("{\"ID\":\"100001\",\"docuType\":\"Participant.Financier\",\"alias\":\"BRITECH\",\"description\":\"British Technology Pvt. Ltd.\"}")}
}

//Get remove second FinancierParticipant for testing //change template
func getRemoveSecondFinancierParticipantForTesting() [][]byte {
	return [][]byte{[]byte("removeFinancier"),
		[]byte("100002")}
}

//Get remove all FinancierParticipants for testing //change template
func getRemoveAllFinancierParticipantsForTesting() [][]byte {
	return [][]byte{[]byte("removeAllFinanciers")}
}

//Get an expected value for testing
func getNewFinancierExpected() []byte {
	var sample Financier
	sample.ID = "100001"
	sample.ObjectType = "Participant.Financier"
	sample.Alias = "BRITECH"
	sample.Description = "British Technology Pvt. Ltd."
	sampleJSON, err := json.Marshal(sample)
	if err != nil {
		fmt.Println("Error converting a Financier record to JSON")
		return nil
	}
	return []byte(sampleJSON)
}

//Get expected values of Financiers for testing
func getExpectedFinanciers() []byte {
	var samples []Financier
	var sample Financier
	sample.ID = "100001"
	sample.ObjectType = "Participant.Financier"
	sample.Alias = "BRITECH"
	sample.Description = "British Technology Pvt. Ltd."
	samples = append(samples, sample)
	sample.ID = "100002"
	sample.ObjectType = "Participant.Financier"
	sample.Alias = "DEMAGDELAG"
	sample.Description = "Demag Delawal AG"
	samples = append(samples, sample)
	sampleJSON, err := json.Marshal(samples)
	if err != nil {
		fmt.Println("Error converting sampleancer records to JSON")
		return nil
	}
	return []byte(sampleJSON)
}

func getExpectedFinancierIDIndex(funcName string) []byte {
	var financierIDIndex FinancierIDIndex
	switch funcName {
	case "addNewFinancier":
		financierIDIndex.IDs = append(financierIDIndex.IDs, "100001")
		financierIDIndexBytes, err := json.Marshal(financierIDIndex)
		if err != nil {
			fmt.Println("Error converting FinancierIDIndex to JSON")
			return nil
		}
		return financierIDIndexBytes
	case "beforeRemoveFinancier":
		financierIDIndex.IDs = append(financierIDIndex.IDs, "100001")
		financierIDIndex.IDs = append(financierIDIndex.IDs, "100002")
		financierIDIndexBytes, err := json.Marshal(financierIDIndex)
		if err != nil {
			fmt.Println("Error converting FinancierIDIndex to JSON")
			return nil
		}
		return financierIDIndexBytes
	case "afterRemoveFinancier":
		financierIDIndex.IDs = append(financierIDIndex.IDs, "100001")
		financierIDIndexBytes, err := json.Marshal(financierIDIndex)
		if err != nil {
			fmt.Println("Error converting FinancierIDIndex to JSON")
			return nil
		}
		return financierIDIndexBytes
	default:
		financierIDIndexBytes, err := json.Marshal(financierIDIndex)
		if err != nil {
			fmt.Println("Error converting FinancierIDIndex to JSON")
			return nil
		}
		return financierIDIndexBytes
	}
}

//checkInit - helper to check the Initialization of chaincode: FinancierParticipant
func checkInit(t *testing.T, stub *shim.MockStub, args [][]byte) {
	res := stub.MockInit("1", args)
	if res.Status != shim.OK {
		fmt.Println("Init failed", string(res.Message))
		t.FailNow()
	}
}

//checkState - helper for checking the chaincode state for a given stateKey afgainst an expected value
func checkState(t *testing.T, stub *shim.MockStub, stateKey string, expectedState []byte) {
	actualState := stub.State[stateKey]
	if actualState == nil {
		fmt.Println("State for ", stateKey, ": failed to get value")
		t.FailNow()
	}

	if bytes.Compare(actualState, expectedState) != 0 {
		fmt.Println("Incorrect State for " + stateKey + ": \nExpected: " + string(expectedState) + "\nActual  : " + string(actualState))
		t.FailNow()
	}
}

//checkInvoke - helper for checking Invoke of chaincode
func checkInvoke(t *testing.T, stub *shim.MockStub, args [][]byte) {
	res := stub.MockInvoke("1", args)
	if res.Status != shim.OK {
		fmt.Println("Invoke", args, "failed", string(res.Message))
		t.FailNow()
	}
}

func checkInvokeUnknownFunction(t *testing.T, stub *shim.MockStub, args [][]byte) {
	res := stub.MockInvoke("1", args)
	if res.Status != shim.OK {
		expectedErr := "Received unknown function invocation"
		actualErr := string(res.Message)
		if !(strings.Contains(actualErr, expectedErr)) {
			fmt.Println("Invoke with unknown function must throw error: ", expectedErr, "\n Actual Error :", actualErr)
			t.FailNow()
		}
	} else {
		t.FailNow()
	}
}

//checkReadFinancierOK - helper for positive test readFinancier
func checkReadFinancierOK(t *testing.T, stub *shim.MockStub, sampleancerID string) {
	res := stub.MockInvoke("1", [][]byte{[]byte("readFinancier"), []byte(sampleancerID)})
	if res.Status != shim.OK {
		fmt.Println("func readFinancier with ID: ", sampleancerID, " failed"+string(res.Message))
		t.FailNow()
	}
	if res.Payload == nil {
		fmt.Println("func readFinancier with ID: ", sampleancerID, "failed to get value")
		t.FailNow()
	}
	if bytes.Compare(getNewFinancierExpected(), []byte(res.Payload)) != 0 {
		fmt.Println("func readFinancier with ID: ", sampleancerID, "Expected:", string(getNewFinancierExpected()), "Actual:", string(res.Payload))
		t.FailNow()
	}
}

//checkReadFinancierNOK - helper for negative testing of readFinancier
func checkReadFinancierNOK(t *testing.T, stub *shim.MockStub, sampleancerID string) {
	//with no sampleancerID
	res := stub.MockInvoke("1", [][]byte{[]byte("readFinancier"), []byte("")})
	if res.Status != shim.OK {
		expectedErr := "retrieveFinancier: Corrupt sample record "
		actualErr := string(res.Message)
		if !(strings.Contains(actualErr, expectedErr)) {
			fmt.Println("func readFinancier neagtive test: ", "Expected Error:", expectedErr, "Actual Error", actualErr)
			t.FailNow()
		}
	} else {
		t.FailNow()
	}
}
func checkReadAllFinanciersOK(t *testing.T, stub *shim.MockStub) {
	res := stub.MockInvoke("1", [][]byte{[]byte("readAllFinanciers")})
	if res.Status != shim.OK {
		fmt.Println("func readAllFinanciers failed", string(res.Message))
		t.FailNow()
	}
	if res.Payload == nil {
		fmt.Println("func readAllFinanciers failed to get value")
		t.FailNow()
	}
	if bytes.Compare(getExpectedFinanciers(), []byte(res.Payload)) != 0 {
		fmt.Println("func readAllFinanciers Expected:\n", string(getExpectedFinanciers()), "\nActual:\n", string(res.Payload))
		t.FailNow()
	}
}

func checkError(t *testing.T, exp string, act string) {
	if strings.Compare(exp, act) != 0 {
		fmt.Println("Unexpected Error! Expecting ", exp, "\n Actual :", act)
		t.FailNow()
	}
}
