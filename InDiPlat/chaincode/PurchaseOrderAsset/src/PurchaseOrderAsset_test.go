package main

import (
	"bytes"
	"encoding/json"
	"fmt"
	"strings"
	"testing"

	"github.com/hyperledger/fabric/core/chaincode/shim"
)

//TestPurchaseOrderAsset_Init
func TestPurchaseOrderAsset_Init(t *testing.T) {
	purchaseOrder := new(PurchaseOrderAsset)
	stub := shim.NewMockStub("purchaseOrder", purchaseOrder)
	checkInit(t, stub, [][]byte{[]byte("init")})
	checkState(t, stub, "purchaseOrderIDIndex", getExpectedPurchaseOrderIDIndex("init"))
}

//TestPurchaseOrderAsset_InvokeUnknownFunction
func TestPurchaseOrderAsset_InvokeUnknownFunction(t *testing.T) {
	purchaseOrder := new(PurchaseOrderAsset)
	stub := shim.NewMockStub("purchaseOrder", purchaseOrder)
	checkInit(t, stub, [][]byte{[]byte("init")})
	checkInvokeUnknownFunction(t, stub, [][]byte{[]byte("myFunction"), []byte("docType:Asset")})
}

//TestPurchaseOrderAsset_Invoke_addNewPurchaseOrder
func TestPurchaseOrderAsset_Invoke_addNewPurchaseOrderOK(t *testing.T) {
	purchaseOrder := new(PurchaseOrderAsset)
	stub := shim.NewMockStub("purchaseOrder", purchaseOrder)
	checkInit(t, stub, [][]byte{[]byte("init")})
	checkInvoke(t, stub, getFirstPurchaseOrderAssetForTesting())
	newPurchaseOrderID := "PO100001"
	checkState(t, stub, newPurchaseOrderID, getNewPurchaseOrderExpected())
	checkState(t, stub, "purchaseOrderIDIndex", getExpectedPurchaseOrderIDIndex("addNewPurchaseOrder"))
}

//TestPurchaseOrderAsset_Invoke_addNewPurchaseOrderUnknownField
func TestPurchaseOrderAsset_Invoke_addNewPurchaseOrderUnknownField(t *testing.T) {
	purchaseOrder := new(PurchaseOrderAsset)
	stub := shim.NewMockStub("purchaseOrder", purchaseOrder)
	checkInit(t, stub, [][]byte{[]byte("init")})
	res := stub.MockInvoke("1", getPurchaseOrderAssetWithUnknownFieldForTesting())
	if res.Status != shim.OK {
		checkError(t, "PurchaseOrder Data is Corrupted", res.Message)
	} else {
		fmt.Println("Unknown Field Error was expected, but not raised")
		t.FailNow()
	}
}

//TestPurchaseOrderAsset_Invoke_addNewPurchaseOrder
func TestPurchaseOrderAsset_Invoke_addNewPurchaseOrderDuplicate(t *testing.T) {
	purchaseOrder := new(PurchaseOrderAsset)
	stub := shim.NewMockStub("purchaseOrder", purchaseOrder)
	checkInit(t, stub, [][]byte{[]byte("init")})
	checkInvoke(t, stub, getFirstPurchaseOrderAssetForTesting())
	newPurchaseOrderID := "PO100001"
	checkState(t, stub, newPurchaseOrderID, getNewPurchaseOrderExpected())
	checkState(t, stub, "purchaseOrderIDIndex", getExpectedPurchaseOrderIDIndex("addNewPurchaseOrder"))
	res := stub.MockInvoke("1", getFirstPurchaseOrderAssetForTesting())
	if res.Status != shim.OK {
		checkError(t, "This PurchaseOrder already exists: PO100001", res.Message)
	} else {
		fmt.Println("Error was expected, but not raised")
		t.FailNow()
	}
}

//TestPurchaseOrderAsset_Invoke_removePurchaseOrderOK  //change template
func TestPurchaseOrderAsset_Invoke_removePurchaseOrderOK(t *testing.T) {
	purchaseOrder := new(PurchaseOrderAsset)
	stub := shim.NewMockStub("purchaseOrder", purchaseOrder)
	checkInit(t, stub, [][]byte{[]byte("init")})
	checkInvoke(t, stub, getFirstPurchaseOrderAssetForTesting())
	checkInvoke(t, stub, getSecondPurchaseOrderAssetForTesting())
	checkReadAllPurchaseOrdersOK(t, stub)
	checkState(t, stub, "purchaseOrderIDIndex", getExpectedPurchaseOrderIDIndex("beforeRemovePurchaseOrder"))
	checkInvoke(t, stub, getRemoveSecondPurchaseOrderAssetForTesting())
	remainingPurchaseOrderID := "PO100001"
	checkReadPurchaseOrderOK(t, stub, remainingPurchaseOrderID)
	checkState(t, stub, "purchaseOrderIDIndex", getExpectedPurchaseOrderIDIndex("afterRemovePurchaseOrder"))
}

//TestPurchaseOrderAsset_Invoke_removePurchaseOrderNOK  //change template
func TestPurchaseOrderAsset_Invoke_removePurchaseOrderNOK(t *testing.T) {
	purchaseOrder := new(PurchaseOrderAsset)
	stub := shim.NewMockStub("purchaseOrder", purchaseOrder)
	checkInit(t, stub, [][]byte{[]byte("init")})
	checkInvoke(t, stub, getFirstPurchaseOrderAssetForTesting())
	firstPurchaseOrderID := "PO100001"
	checkReadPurchaseOrderOK(t, stub, firstPurchaseOrderID)
	checkState(t, stub, "purchaseOrderIDIndex", getExpectedPurchaseOrderIDIndex("addNewPurchaseOrder"))
	res := stub.MockInvoke("1", getRemoveSecondPurchaseOrderAssetForTesting())
	if res.Status != shim.OK {
		checkError(t, "PurchaseOrder with ID: "+"PO100002"+" not found", res.Message)
	} else {
		fmt.Println("Error was expected, but not raised")
		t.FailNow()
	}
	checkState(t, stub, "purchaseOrderIDIndex", getExpectedPurchaseOrderIDIndex("addNewPurchaseOrder"))
}

//TestPurchaseOrderAsset_Invoke_setPurchaseOrderPaidOK
func TestPurchaseOrderAsset_Invoke_setPurchaseOrderPaidOK(t *testing.T) {
	purchaseOrder := new(PurchaseOrderAsset)
	stub := shim.NewMockStub("purchaseOrder", purchaseOrder)
	checkInit(t, stub, [][]byte{[]byte("init")})
	checkInvoke(t, stub, getFirstPurchaseOrderAssetForTesting())
	purchaseOrderID := "PO100001"
	res := stub.MockInvoke("1", [][]byte{[]byte("setPurchaseOrderPaid"),
		[]byte(purchaseOrderID),
		[]byte("12/03/2017")})
	if res.Status != shim.OK {
		fmt.Println("Unknown Error! Purchase Order Status could not be changed to: Paid")
		t.FailNow()
	}
	res = stub.MockInvoke("1", [][]byte{[]byte("readPurchaseOrder"), []byte(purchaseOrderID)})
	if res.Status != shim.OK {
		fmt.Println("func readPurchaseOrder with ID: ", purchaseOrderID, " failed"+string(res.Message))
		t.FailNow()
	}
	if res.Payload == nil {
		fmt.Println("func readPurchaseOrder with ID: ", purchaseOrderID, "failed to get value")
		t.FailNow()
	} else {
		var resPO PurchaseOrder
		err := json.Unmarshal(res.Payload, &resPO)
		if err != nil {
			fmt.Println(err.Error())
			t.FailNow()
		}
		checkError(t, Paid, resPO.Status)
		checkError(t, "12/03/2017", resPO.PaymentDate)
	}
}

//TestPurchaseOrderAsset_Invoke_setPurchaseOrderPaidNOK
func TestPurchaseOrderAsset_Invoke_setPurchaseOrderPaidNOK(t *testing.T) {
	purchaseOrder := new(PurchaseOrderAsset)
	stub := shim.NewMockStub("purchaseOrder", purchaseOrder)
	checkInit(t, stub, [][]byte{[]byte("init")})
	checkInvoke(t, stub, getPurchaseOrderForSetPaidNOKTest())
	purchaseOrderID := "PO100001"
	res := stub.MockInvoke("1", [][]byte{[]byte("setPurchaseOrderPaid"),
		[]byte(purchaseOrderID),
		[]byte("12/03/2017")})
	if res.Status != shim.OK {
		checkError(t, "Purchase Order with ID: "+purchaseOrderID+" already in status 'Paid'", res.Message)
	}
	res = stub.MockInvoke("1", [][]byte{[]byte("readPurchaseOrder"), []byte(purchaseOrderID)})
	if res.Status != shim.OK {
		fmt.Println("func readPurchaseOrder with ID: ", purchaseOrderID, " failed"+string(res.Message))
		t.FailNow()
	}
	if res.Payload == nil {
		fmt.Println("func readPurchaseOrder with ID: ", purchaseOrderID, "failed to get value")
		t.FailNow()
	} else {
		var resPO PurchaseOrder
		err := json.Unmarshal(res.Payload, &resPO)
		if err != nil {
			fmt.Println(err.Error())
			t.FailNow()
		}
		checkError(t, Paid, resPO.Status)
		checkError(t, "12/01/2017", resPO.PaymentDate)
	}
}

//TestPurchaseOrderAsset_Invoke_setPurchaseOrderPaidWrongArgs
func TestPurchaseOrderAsset_Invoke_setPurchaseOrderPaidWrongArgs(t *testing.T) {
	purchaseOrder := new(PurchaseOrderAsset)
	stub := shim.NewMockStub("purchaseOrder", purchaseOrder)
	checkInit(t, stub, [][]byte{[]byte("init")})
	checkInvoke(t, stub, getPurchaseOrderForSetPaidNOKTest())
	purchaseOrderID := "PO100001"
	res := stub.MockInvoke("1", [][]byte{[]byte("setPurchaseOrderPaid"),
		[]byte(purchaseOrderID)})
	if res.Status != shim.OK {
		checkError(t, "Incorrect arguments for method 'setPurchaseOrderPaid': Expected: 2, Actual: 1", res.Message)
	}
}

//TestPurchaseOrderAsset_Invoke_removeAllPurchaseOrderssOK
func TestPurchaseOrderAsset_Invoke_removeAllPurchaseOrderssOK(t *testing.T) {
	financier := new(PurchaseOrderAsset)
	stub := shim.NewMockStub("financier", financier)
	checkInit(t, stub, [][]byte{[]byte("init")})
	checkInvoke(t, stub, getFirstPurchaseOrderAssetForTesting())
	checkInvoke(t, stub, getSecondPurchaseOrderAssetForTesting())
	checkReadAllPurchaseOrdersOK(t, stub)
	checkState(t, stub, "purchaseOrderIDIndex", getExpectedPurchaseOrderIDIndex("beforeRemovePurchaseOrder"))
	checkInvoke(t, stub, getRemoveAllPurchaseOrderAssetsForTesting())
	checkState(t, stub, "purchaseOrderIDIndex", getExpectedPurchaseOrderIDIndex(""))
}

//TestPurchaseOrderAsset_Query_readPurchaseOrder
func TestPurchaseOrderAsset_Query_readPurchaseOrder(t *testing.T) {
	purchaseOrder := new(PurchaseOrderAsset)
	stub := shim.NewMockStub("purchaseOrder", purchaseOrder)
	checkInit(t, stub, [][]byte{[]byte("init")})
	purchaseOrderID := "PO100001"
	checkInvoke(t, stub, getFirstPurchaseOrderAssetForTesting())
	checkReadPurchaseOrderOK(t, stub, purchaseOrderID)
	checkReadPurchaseOrderNOK(t, stub, "")
}

//TestPurchaseOrderAsset_Query_readAllPurchaseOrders
func TestPurchaseOrderAsset_Query_readAllPurchaseOrders(t *testing.T) {
	purchaseOrder := new(PurchaseOrderAsset)
	stub := shim.NewMockStub("purchaseOrder", purchaseOrder)
	checkInit(t, stub, [][]byte{[]byte("init")})
	checkInvoke(t, stub, getFirstPurchaseOrderAssetForTesting())
	checkInvoke(t, stub, getSecondPurchaseOrderAssetForTesting())
	checkReadAllPurchaseOrdersOK(t, stub)
}

/*
*
*	Helper Functions
*
 */
//Get first PurchaseOrderAsset for testing
func getFirstPurchaseOrderAssetForTesting() [][]byte {
	return [][]byte{[]byte("addNewPurchaseOrder"),
		[]byte("{\"ID\":\"PO100001\"," +
			"\"purchaserID\":\"PU100001\"," +
			"\"supplierID\":\"SU100001\"," +
			"\"docType\":\"Asset.PurchaseOrder\"," +
			"\"status\":\"Approved\"," +
			"\"postingDate\":\"12/01/2017\"," +
			"\"approvalDate\":\"12/01/2017\"," +
			"\"paymentDate\":\"12/01/2017\"," +
			"\"grossAmount\":\"12345.67\"," +
			"\"totalSubmittedAmount\":\"12345.67\"," +
			"\"currency\":\"SGD\"}")}
}

//Get PurchaseOrderAsset with unknown field for testing
func getPurchaseOrderAssetWithUnknownFieldForTesting() [][]byte {
	return [][]byte{[]byte("addNewPurchaseOrder"),
		[]byte("{\"ID\":\"PO100001\"," +
			"\"purchaserID\":\"PU100001\"," +
			"\"supplierID\":\"SU100001\"," +
			"\"docuType\":\"Asset.PurchaseOrder\"," +
			"\"status\":\"Approved\"," +
			"\"postingDate\":\"12/01/2017\"," +
			"\"approvalDate\":\"12/01/2017\"," +
			"\"paymentDate\":\"12/01/2017\"," +
			"\"grossAmount\":\"12345.67\"," +
			"\"totalSubmittedAmount\":\"12345.67\"," +
			"\"currency\":\"SGD\"}")}
}

//Get second PurchaseOrderAsset for testing
func getSecondPurchaseOrderAssetForTesting() [][]byte {
	return [][]byte{[]byte("addNewPurchaseOrder"),
		[]byte("{\"ID\":\"PO100002\"," +
			"\"purchaserID\":\"PU100002\"," +
			"\"supplierID\":\"SU100002\"," +
			"\"docType\":\"Asset.PurchaseOrder\"," +
			"\"status\":\"Approved\"," +
			"\"postingDate\":\"12/01/2017\"," +
			"\"approvalDate\":\"12/01/2017\"," +
			"\"paymentDate\":\"12/01/2017\"," +
			"\"grossAmount\":\"12345.67\"," +
			"\"totalSubmittedAmount\":\"12345.67\"," +
			"\"currency\":\"SGD\"}")}
}

//Get New PurchaseOrder for testing
func getPurchaseOrderForSetPaidNOKTest() [][]byte {
	return [][]byte{[]byte("addNewPurchaseOrder"),
		[]byte("{\"ID\":\"PO100001\"," +
			"\"purchaserID\":\"PU100001\"," +
			"\"supplierID\":\"SU100001\"," +
			"\"docType\":\"Asset.PurchaseOrder\"," +
			"\"status\":\"Paid\"," +
			"\"postingDate\":\"12/01/2017\"," +
			"\"approvalDate\":\"12/01/2017\"," +
			"\"paymentDate\":\"12/01/2017\"," +
			"\"grossAmount\":\"12345.67\"," +
			"\"totalSubmittedAmount\":\"12345.67\"," +
			"\"currency\":\"SGD\"}")}
}

//Get remove second PurchaseOrderAsset for testing //change template
func getRemoveSecondPurchaseOrderAssetForTesting() [][]byte {
	return [][]byte{[]byte("removePurchaseOrder"),
		[]byte("PO100002")}
}

//Get remove all PurchaseOrderAssets for testing
func getRemoveAllPurchaseOrderAssetsForTesting() [][]byte {
	return [][]byte{[]byte("removeAllPurchaseOrders")}
}

//Get an expected value for testing
func getNewPurchaseOrderExpected() []byte {
	var purchaseOrder PurchaseOrder
	purchaseOrder.ID = "PO100001"
	purchaseOrder.PurchaserID = "PU100001"
	purchaseOrder.SupplierID = "SU100001"
	purchaseOrder.ObjectType = "Asset.PurchaseOrder"
	purchaseOrder.Status = "Approved"
	purchaseOrder.PostingDate = "12/01/2017"
	purchaseOrder.ApprovalDate = "12/01/2017"
	purchaseOrder.PaymentDate = "12/01/2017"
	purchaseOrder.GrossAmount = "12345.67"
	purchaseOrder.TotalSubmittedAmount = "12345.67"
	purchaseOrder.Currency = "SGD"
	purchaseOrderJSON, err := json.Marshal(purchaseOrder)
	if err != nil {
		fmt.Println("Error converting a PurchaseOrder record to JSON")
		return nil
	}
	return []byte(purchaseOrderJSON)
}

//Get expected values of PurchaseOrders for testing
func getExpectedPurchaseOrders() []byte {
	var purchaseOrders []PurchaseOrder
	var purchaseOrder PurchaseOrder
	purchaseOrder.ID = "PO100001"
	purchaseOrder.PurchaserID = "PU100001"
	purchaseOrder.SupplierID = "SU100001"
	purchaseOrder.ObjectType = "Asset.PurchaseOrder"
	purchaseOrder.Status = "Approved"
	purchaseOrder.PostingDate = "12/01/2017"
	purchaseOrder.ApprovalDate = "12/01/2017"
	purchaseOrder.PaymentDate = "12/01/2017"
	purchaseOrder.GrossAmount = "12345.67"
	purchaseOrder.TotalSubmittedAmount = "12345.67"
	purchaseOrder.Currency = "SGD"
	purchaseOrders = append(purchaseOrders, purchaseOrder)
	purchaseOrder.ID = "PO100002"
	purchaseOrder.PurchaserID = "PU100002"
	purchaseOrder.SupplierID = "SU100002"
	purchaseOrder.ObjectType = "Asset.PurchaseOrder"
	purchaseOrder.Status = "Approved"
	purchaseOrder.PostingDate = "12/01/2017"
	purchaseOrder.ApprovalDate = "12/01/2017"
	purchaseOrder.PaymentDate = "12/01/2017"
	purchaseOrder.GrossAmount = "12345.67"
	purchaseOrder.TotalSubmittedAmount = "12345.67"
	purchaseOrder.Currency = "SGD"
	purchaseOrders = append(purchaseOrders, purchaseOrder)
	purchaseOrderJSON, err := json.Marshal(purchaseOrders)
	if err != nil {
		fmt.Println("Error converting purchaseOrder records to JSON")
		return nil
	}
	return []byte(purchaseOrderJSON)
}

func getExpectedPurchaseOrderIDIndex(funcName string) []byte {
	var purchaseOrderIDIndex PurchaseOrderIDIndex
	switch funcName {
	case "addNewPurchaseOrder":
		purchaseOrderIDIndex.IDs = append(purchaseOrderIDIndex.IDs, "PO100001")
		purchaseOrderIDIndexBytes, err := json.Marshal(purchaseOrderIDIndex)
		if err != nil {
			fmt.Println("Error converting PurchaseOrderIDIndex to JSON")
			return nil
		}
		return purchaseOrderIDIndexBytes
	case "beforeRemovePurchaseOrder":
		purchaseOrderIDIndex.IDs = append(purchaseOrderIDIndex.IDs, "PO100001")
		purchaseOrderIDIndex.IDs = append(purchaseOrderIDIndex.IDs, "PO100002")
		purchaseOrderIDIndexBytes, err := json.Marshal(purchaseOrderIDIndex)
		if err != nil {
			fmt.Println("Error converting PurchaseOrderIDIndex to JSON")
			return nil
		}
		return purchaseOrderIDIndexBytes
	case "afterRemovePurchaseOrder":
		purchaseOrderIDIndex.IDs = append(purchaseOrderIDIndex.IDs, "PO100001")
		purchaseOrderIDIndexBytes, err := json.Marshal(purchaseOrderIDIndex)
		if err != nil {
			fmt.Println("Error converting PurchaseOrderIDIndex to JSON")
			return nil
		}
		return purchaseOrderIDIndexBytes
	default:
		purchaseOrderIDIndexBytes, err := json.Marshal(purchaseOrderIDIndex)
		if err != nil {
			fmt.Println("Error converting PurchaseOrderIDIndex to JSON")
			return nil
		}
		return purchaseOrderIDIndexBytes
	}
}

//checkInit - helper to check the Initialization of chaincode: PurchaseOrderAsset
func checkInit(t *testing.T, stub *shim.MockStub, args [][]byte) {
	res := stub.MockInit("1", args)
	if res.Status != shim.OK {
		fmt.Println("Init failed", string(res.Message))
		t.FailNow()
	}
}

//checkState - helper for checking the chaincode state for a given stateKey afgainst an expected value
func checkState(t *testing.T, stub *shim.MockStub, stateKey string, expectedState []byte) {
	actualState := stub.State[stateKey]
	if actualState == nil {
		fmt.Println("State for ", stateKey, ": failed to get value")
		t.FailNow()
	}

	if bytes.Compare(actualState, expectedState) != 0 {
		fmt.Println("Incorrect State for " + stateKey + ": \nExpected: " + string(expectedState) + "\nActual  : " + string(actualState))
		t.FailNow()
	}
}

//checkInvoke - helper for checking Invoke of chaincode
func checkInvoke(t *testing.T, stub *shim.MockStub, args [][]byte) {
	res := stub.MockInvoke("1", args)
	if res.Status != shim.OK {
		fmt.Println("Invoke", args, "failed", string(res.Message))
		t.FailNow()
	}
}

func checkInvokeUnknownFunction(t *testing.T, stub *shim.MockStub, args [][]byte) {
	res := stub.MockInvoke("1", args)
	if res.Status != shim.OK {
		expectedErr := "Received unknown function invocation"
		actualErr := string(res.Message)
		if !(strings.Contains(actualErr, expectedErr)) {
			fmt.Println("Invoke with unknown function must throw error: ", expectedErr, "\n Actual Error :", actualErr)
			t.FailNow()
		}
	} else {
		t.FailNow()
	}
}

//checkReadPurchaseOrderOK - helper for positive test readPurchaseOrder
func checkReadPurchaseOrderOK(t *testing.T, stub *shim.MockStub, purchaseOrderID string) {
	res := stub.MockInvoke("1", [][]byte{[]byte("readPurchaseOrder"), []byte(purchaseOrderID)})
	if res.Status != shim.OK {
		fmt.Println("func readPurchaseOrder with ID: ", purchaseOrderID, " failed"+string(res.Message))
		t.FailNow()
	}
	if res.Payload == nil {
		fmt.Println("func readPurchaseOrder with ID: ", purchaseOrderID, "failed to get value")
		t.FailNow()
	}
	if bytes.Compare(getNewPurchaseOrderExpected(), []byte(res.Payload)) != 0 {
		fmt.Println("func readPurchaseOrder with ID: ", purchaseOrderID, "Expected:", string(getNewPurchaseOrderExpected()), "Actual:", string(res.Payload))
		t.FailNow()
	}
}

//checkReadPurchaseOrderNOK - helper for negative testing of readPurchaseOrder
func checkReadPurchaseOrderNOK(t *testing.T, stub *shim.MockStub, purchaseOrderID string) {
	//with no purchaseOrderID
	res := stub.MockInvoke("1", [][]byte{[]byte("readPurchaseOrder"), []byte("")})
	if res.Status != shim.OK {
		expectedErr := "retrievePurchaseOrder: Corrupt purchaseOrder record "
		actualErr := string(res.Message)
		if !(strings.Contains(actualErr, expectedErr)) {
			fmt.Println("func readPurchaseOrder negative test: ", "Expected Error:", expectedErr, "Actual Error", actualErr)
			t.FailNow()
		}
	} else {
		t.FailNow()
	}
}

func checkReadAllPurchaseOrdersOK(t *testing.T, stub *shim.MockStub) {
	res := stub.MockInvoke("1", [][]byte{[]byte("readAllPurchaseOrders")})
	if res.Status != shim.OK {
		fmt.Println("func readAllPurchaseOrders failed", string(res.Message))
		t.FailNow()
	}
	if res.Payload == nil {
		fmt.Println("func readAllPurchaseOrders failed to get value")
		t.FailNow()
	}
	if bytes.Compare(getExpectedPurchaseOrders(), []byte(res.Payload)) != 0 {
		fmt.Println("func readAllPurchaseOrders Expected:\n", string(getExpectedPurchaseOrders()), "\nActual:\n", string(res.Payload))
		t.FailNow()
	}
}

func checkError(t *testing.T, exp string, act string) {
	if strings.Compare(exp, act) != 0 {
		fmt.Println("Unexpected Error! Expecting ", exp, "\n Actual :", act)
		t.FailNow()
	}
}
