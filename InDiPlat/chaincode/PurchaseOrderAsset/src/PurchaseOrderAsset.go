package main

import (
	"encoding/json"
	"errors"
	"fmt"
	"strconv"
	"strings"

	"github.com/hyperledger/fabric/core/chaincode/shim"
	"github.com/hyperledger/fabric/protos/peer"
)

var logger = shim.NewLogger("CLDChaincode")

//Approved Purchse Order is Approved (Start State)
const Approved = "Approved"

//Paid All PurchaseOrders of this Purchase Order are fully paid (End State)
const Paid = "Paid"

//PurchaseOrderAsset - Chaincode for asset PurchaseOrder
type PurchaseOrderAsset struct {
}

//PurchaseOrder - Details of the asset type PurchaseOrder
type PurchaseOrder struct {
	ID                   string `json:"ID"`
	PurchaserID          string `json:"purchaserID"`
	SupplierID           string `json:"supplierID"`
	ObjectType           string `json:"docType"`
	Status               string `json:"status"`
	PostingDate          string `json:"postingDate"`
	ApprovalDate         string `json:"approvalDate"`
	PaymentDate          string `json:"paymentDate"`
	GrossAmount          string `json:"grossAmount"`
	TotalSubmittedAmount string `json:"totalSubmittedAmount"`
	Currency             string `json:"currency"`
}

//PurchaseOrderIDIndex - Index on IDs for retrieval all PurchaseOrders
type PurchaseOrderIDIndex struct {
	IDs []string `json:"IDs"`
}

func main() {
	err := shim.Start(new(PurchaseOrderAsset))
	if err != nil {
		fmt.Printf("Error starting PurchaseOrderAsset chaincode function main(): %s", err)
	} else {
		fmt.Printf("Starting PurchaseOrderAsset chaincode function main() executed successfully")
	}
}

//Init - The chaincode Init function: No  arguments, only initializes a ID array as Index for retrieval of all PurchaseOrders
func (pO *PurchaseOrderAsset) Init(stub shim.ChaincodeStubInterface) peer.Response {
	var purchaseOrderIDIndex PurchaseOrderIDIndex
	record, _ := stub.GetState("purchaseOrderIDIndex")
	//chaincode upgrade: Index might already exist, create new only if it does not
	if record == nil {
		bytes, _ := json.Marshal(purchaseOrderIDIndex)
		stub.PutState("purchaseOrderIDIndex", bytes)
	}
	return shim.Success(nil)
}

//Invoke - The chaincode Invoke function:
func (pO *PurchaseOrderAsset) Invoke(stub shim.ChaincodeStubInterface) peer.Response {
	function, args := stub.GetFunctionAndParameters()
	logger.Debug("function: ", function)
	switch function {
	case "addNewPurchaseOrder":
		return pO.addNewPurchaseOrder(stub, args)
	case "removePurchaseOrder":
		return pO.removePurchaseOrder(stub, args[0])
	case "removeAllPurchaseOrders":
		return pO.removeAllPurchaseOrders(stub)
	case "setPurchaseOrderPaid":
		if len(args) != 2 {
			return shim.Error("Incorrect arguments for method 'setPurchaseOrderPaid': Expected: 2, Actual: " + strconv.Itoa(len(args)))
		}
		return pO.setPurchaseOrderPaid(stub, args)
	case "readPurchaseOrder":
		return pO.readPurchaseOrder(stub, args[0])
	case "readAllPurchaseOrders":
		return pO.readAllPurchaseOrders(stub)
	default:
		return shim.Error("Received unknown function invocation")
	}
}

//Invoke Route: addNewPurchaseOrder
func (pO *PurchaseOrderAsset) addNewPurchaseOrder(stub shim.ChaincodeStubInterface, args []string) peer.Response {
	purchaseOrder, err := getPurchaseOrderFromArgs(args)
	if err != nil {
		return shim.Error("PurchaseOrder Data is Corrupted")
	}
	purchaseOrder.ObjectType = "Asset.PurchaseOrder"
	record, err := stub.GetState(purchaseOrder.ID)
	if record != nil {
		return shim.Error("This PurchaseOrder already exists: " + purchaseOrder.ID)
	}
	_, err = pO.savePurchaseOrder(stub, purchaseOrder)
	if err != nil {
		return shim.Error(err.Error())
	}
	_, err = pO.updatePurchaseOrderIDIndex(stub, purchaseOrder)
	if err != nil {
		return shim.Error(err.Error())
	}
	return shim.Success(nil)
}

//Invoke Route: setPurchaseOrderPaid
func (pO *PurchaseOrderAsset) setPurchaseOrderPaid(stub shim.ChaincodeStubInterface, args []string) peer.Response {
	var purchaseOrder PurchaseOrder
	_, err := stub.GetState(args[0])
	if err != nil {
		return shim.Error("Purchase Order with ID: " + args[0] + "not found")
	}
	purchaseOrderAsByteArray, err := pO.retrievePurchaseOrder(stub, args[0])
	if err != nil {
		return shim.Error(err.Error())
	}
	err = json.Unmarshal(purchaseOrderAsByteArray, &purchaseOrder)
	if err != nil {
		return shim.Error(err.Error())
	}
	if purchaseOrder.Status == Paid {
		return shim.Error("Purchase Order with ID: " + args[0] + " already in status 'Paid'")
	}
	purchaseOrder.Status = Paid
	purchaseOrder.PaymentDate = args[1]
	_, err = pO.savePurchaseOrder(stub, purchaseOrder)
	if err != nil {
		return shim.Error(err.Error())
	}
	return shim.Success(nil)
}

//Invoke Route: removePurchaseOrder
func (pO *PurchaseOrderAsset) removePurchaseOrder(stub shim.ChaincodeStubInterface, purchaseOrderID string) peer.Response {
	_, err := pO.deletePurchaseOrder(stub, purchaseOrderID)
	if err != nil {
		return shim.Error(err.Error())
	}
	_, err = pO.deletePurchaseOrderIDIndex(stub, purchaseOrderID)
	if err != nil {
		return shim.Error(err.Error())
	}
	return shim.Success(nil)
}

//Invoke Route: removeAllPurchaseOrders
func (pO *PurchaseOrderAsset) removeAllPurchaseOrders(stub shim.ChaincodeStubInterface) peer.Response {
	var purchaseOrderStructIDs PurchaseOrderIDIndex
	bytes, err := stub.GetState("purchaseOrderIDIndex")
	if err != nil {
		return shim.Error("removeAllPurchaseOrders: Error getting purchaseOrderIDIndex array")
	}
	err = json.Unmarshal(bytes, &purchaseOrderStructIDs)
	if err != nil {
		return shim.Error("removeAllPurchaseOrders: Error unmarshalling purchaseOrderIDIndex array JSON")
	}
	if len(purchaseOrderStructIDs.IDs) == 0 {
		return shim.Error("removeAllPurchaseOrders: No purchaseOrders to remove")
	}
	for _, purchaseOrderStructParticipantID := range purchaseOrderStructIDs.IDs {
		_, err = pO.deletePurchaseOrder(stub, purchaseOrderStructParticipantID)
		if err != nil {
			return shim.Error("Failed to remove PurchaseOrder with ID: " + purchaseOrderStructParticipantID)
		}
		_, err = pO.deletePurchaseOrderIDIndex(stub, purchaseOrderStructParticipantID)
		if err != nil {
			return shim.Error(err.Error())
		}
	}
	pO.initHolder(stub)
	return shim.Success(nil)
}

//Query Route: readPurchaseOrder
func (pO *PurchaseOrderAsset) readPurchaseOrder(stub shim.ChaincodeStubInterface, purchaseOrderID string) peer.Response {
	purchaseOrderAsByteArray, err := pO.retrievePurchaseOrder(stub, purchaseOrderID)
	if err != nil {
		return shim.Error(err.Error())
	}
	return shim.Success(purchaseOrderAsByteArray)
}

//Query Route: readAllPurchaseOrders
func (pO *PurchaseOrderAsset) readAllPurchaseOrders(stub shim.ChaincodeStubInterface) peer.Response {
	var purchaseOrderIDs PurchaseOrderIDIndex
	bytes, err := stub.GetState("purchaseOrderIDIndex")
	if err != nil {
		return shim.Error("readAllPurchaseOrders: Error getting purchaseOrderIDIndex array")
	}
	err = json.Unmarshal(bytes, &purchaseOrderIDs)
	if err != nil {
		return shim.Error("readAllPurchaseOrders: Error unmarshalling purchaseOrderIDIndex array JSON")
	}
	result := "["

	var purchaseOrderAsByteArray []byte

	for _, purchaseOrderID := range purchaseOrderIDs.IDs {
		purchaseOrderAsByteArray, err = pO.retrievePurchaseOrder(stub, purchaseOrderID)
		if err != nil {
			return shim.Error("Failed to retrieve purchaseOrder with ID: " + purchaseOrderID)
		}
		result += string(purchaseOrderAsByteArray) + ","
	}
	if len(result) == 1 {
		result = "[]"
	} else {
		result = result[:len(result)-1] + "]"
	}
	return shim.Success([]byte(result))
}

//Helper: Save Purchase Order
func (pO *PurchaseOrderAsset) savePurchaseOrder(stub shim.ChaincodeStubInterface, purchaseOrder PurchaseOrder) (bool, error) {
	bytes, err := json.Marshal(purchaseOrder)
	if err != nil {
		return false, errors.New("Error converting purchaseOrder record JSON")
	}
	err = stub.PutState(purchaseOrder.ID, bytes)
	if err != nil {
		return false, errors.New("Error storing PurchaseOrder record")
	}
	return true, nil
}

//Helper: PurchaseOrder purchaseOrderStruct //change template
func (purchaseOrder *PurchaseOrderAsset) deletePurchaseOrder(stub shim.ChaincodeStubInterface, purchaseOrderID string) (bool, error) {
	_, err := purchaseOrder.retrievePurchaseOrder(stub, purchaseOrderID)
	if err != nil {
		return false, errors.New("PurchaseOrder with ID: " + purchaseOrderID + " not found")
	}
	err = stub.DelState(purchaseOrderID)
	if err != nil {
		return false, errors.New("Error deleting PurchaseOrder record")
	}
	return true, nil
}

//Helper: Update purchaseOrder Holder - updates Index
func (pO *PurchaseOrderAsset) updatePurchaseOrderIDIndex(stub shim.ChaincodeStubInterface, purchaseOrder PurchaseOrder) (bool, error) {
	var purchaseOrderIDs PurchaseOrderIDIndex
	bytes, err := stub.GetState("purchaseOrderIDIndex")
	if err != nil {
		return false, errors.New("updatePurchaseOrderIDIndex: Error getting purchaseOrderIDIndex array Index from state")
	}
	err = json.Unmarshal(bytes, &purchaseOrderIDs)
	if err != nil {
		return false, errors.New("updatePurchaseOrderIDIndex: Error unmarshalling purchaseOrderIDIndex array JSON")
	}
	purchaseOrderIDs.IDs = append(purchaseOrderIDs.IDs, purchaseOrder.ID)
	bytes, err = json.Marshal(purchaseOrderIDs)
	if err != nil {
		return false, errors.New("updatePurchaseOrderIDIndex: Error marshalling new purchaseOrder ID")
	}
	err = stub.PutState("purchaseOrderIDIndex", bytes)
	if err != nil {
		return false, errors.New("updatePurchaseOrderIDIndex: Error storing new purchaseOrder ID in purchaseOrderIDIndex (Index)")
	}
	return true, nil
}

//Helper: delete ID from purchaseOrderStruct Holder
func (pO *PurchaseOrderAsset) deletePurchaseOrderIDIndex(stub shim.ChaincodeStubInterface, purchaseOrderID string) (bool, error) {
	var purchaseOrderStructIDs PurchaseOrderIDIndex
	bytes, err := stub.GetState("purchaseOrderIDIndex")
	if err != nil {
		return false, errors.New("deletePurchaseOrderIDIndex: Error getting purchaseOrderIDIndex array Index from state")
	}
	err = json.Unmarshal(bytes, &purchaseOrderStructIDs)
	if err != nil {
		return false, errors.New("deletePurchaseOrderIDIndex: Error unmarshalling purchaseOrderIDIndex array JSON")
	}
	purchaseOrderStructIDs.IDs, err = deleteKeyFromStringArray(purchaseOrderStructIDs.IDs, purchaseOrderID)
	if err != nil {
		return false, errors.New(err.Error())
	}
	bytes, err = json.Marshal(purchaseOrderStructIDs)
	if err != nil {
		return false, errors.New("deletePurchaseOrderIDIndex: Error marshalling new purchaseOrderStruct ID")
	}
	err = stub.PutState("purchaseOrderIDIndex", bytes)
	if err != nil {
		return false, errors.New("deletePurchaseOrderIDIndex: Error storing new purchaseOrderStruct ID in purchaseOrderIDIndex (Index)")
	}
	return true, nil
}

//Helper: Initialize truck ID Holder //change template
func (pO *PurchaseOrderAsset) initHolder(stub shim.ChaincodeStubInterface) bool {
	var purchaseOrderIDIndex PurchaseOrderIDIndex
	bytes, _ := json.Marshal(purchaseOrderIDIndex)
	stub.DelState("purchaseOrderIDIndex")
	stub.PutState("purchaseOrderIDIndex", bytes)
	return true
}

//deleteKeyFromArray
func deleteKeyFromStringArray(array []string, key string) (newArray []string, err error) {
	for _, entry := range array {
		if entry != key {
			newArray = append(newArray, entry)
		}
	}
	if len(newArray) == len(array) {
		return newArray, errors.New("Specified Key: " + key + " not found in Array")
	}
	return newArray, nil
}

//Helper: Retrieve purchaser
func (pO *PurchaseOrderAsset) retrievePurchaseOrder(stub shim.ChaincodeStubInterface, purchaseOrderID string) ([]byte, error) {
	var purchaseOrder PurchaseOrder
	var purchaseOrderAsByteArray []byte
	bytes, err := stub.GetState(purchaseOrderID)
	if err != nil {
		return purchaseOrderAsByteArray, errors.New("retrievePurchaseOrder: Error retrieving purchaseOrder with ID: " + purchaseOrderID)
	}
	err = json.Unmarshal(bytes, &purchaseOrder)
	if err != nil {
		return purchaseOrderAsByteArray, errors.New("retrievePurchaseOrder: Corrupt purchaseOrder record " + string(bytes))
	}
	purchaseOrderAsByteArray, err = json.Marshal(purchaseOrder)
	if err != nil {
		return purchaseOrderAsByteArray, errors.New("readPurchaseOrder: Invalid purchaseOrder Object - Not a  valid JSON")
	}
	return purchaseOrderAsByteArray, nil
}

//getPurchaseOrderFromArgs - construct a purchaseOrder structure from string array of arguments
func getPurchaseOrderFromArgs(args []string) (sample PurchaseOrder, err error) {

	if strings.Contains(args[0], "\"ID\"") == false ||
		strings.Contains(args[0], "\"purchaserID\"") == false ||
		strings.Contains(args[0], "\"supplierID\"") == false ||
		strings.Contains(args[0], "\"docType\"") == false ||
		strings.Contains(args[0], "\"status\"") == false ||
		strings.Contains(args[0], "\"postingDate\"") == false ||
		strings.Contains(args[0], "\"approvalDate\"") == false ||
		strings.Contains(args[0], "\"paymentDate\"") == false ||
		strings.Contains(args[0], "\"grossAmount\"") == false ||
		strings.Contains(args[0], "\"totalSubmittedAmount\"") == false ||
		strings.Contains(args[0], "\"currency\"") == false {
		return sample, errors.New("Unknown field: " + "Input JSON does not comply to schema")
	}

	err = json.Unmarshal([]byte(args[0]), &sample)
	if err != nil {
		return sample, err
	}
	return sample, nil

}
