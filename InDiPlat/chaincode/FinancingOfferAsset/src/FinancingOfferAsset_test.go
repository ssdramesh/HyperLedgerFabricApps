package main

import (
	"bytes"
	"encoding/json"
	"fmt"
	"strings"
	"testing"

	"github.com/hyperledger/fabric/core/chaincode/shim"
)

//TestFinancingOfferAsset_Init
func TestFinancingOfferAsset_Init(t *testing.T) {
	financingOffer := new(FinancingOfferAsset)
	stub := shim.NewMockStub("financingOffer", financingOffer)
	checkInit(t, stub, [][]byte{[]byte("init")})
	checkState(t, stub, "financingOfferIDIndex", getExpectedFinancingOfferIDIndex("init"))
}

//TestFinancingOfferAsset_InvokeUnknownFunction
func TestFinancingOfferAsset_InvokeUnknownFunction(t *testing.T) {
	financingOffer := new(FinancingOfferAsset)
	stub := shim.NewMockStub("financingOffer", financingOffer)
	checkInit(t, stub, [][]byte{[]byte("init")})
	checkInvokeUnknownFunction(t, stub, [][]byte{[]byte("myFunction"), []byte("docType:Asset")})
}

//TestFinancingOfferAsset_Invoke_addNewFinancingOffer
func TestFinancingOfferAsset_Invoke_addNewFinancingOfferOK(t *testing.T) {
	financingOffer := new(FinancingOfferAsset)
	stub := shim.NewMockStub("financingOffer", financingOffer)
	checkInit(t, stub, [][]byte{[]byte("init")})
	checkInvoke(t, stub, getFirstFinancingOfferAssetForTesting())
	newFinancingOfferID := "FO100001"
	checkState(t, stub, newFinancingOfferID, getNewFinancingOfferExpected())
	checkState(t, stub, "financingOfferIDIndex", getExpectedFinancingOfferIDIndex("addNewFinancingOffer"))
}

//TestFinancingOfferAsset_Invoke_addNewFinancingOfferUnknownField
func TestFinancingOfferAsset_Invoke_addNewFinancingOfferUnknownField(t *testing.T) {
	financingOffer := new(FinancingOfferAsset)
	stub := shim.NewMockStub("financingOffer", financingOffer)
	checkInit(t, stub, [][]byte{[]byte("init")})
	res := stub.MockInvoke("1", getFinancingOfferAssetWithUnknownFieldForTesting())
	if res.Status != shim.OK {
		checkError(t, "FinancingOffer Data is Corrupted", res.Message)
	} else {
		fmt.Println("Unknown Field Error was expected, but not raised")
		t.FailNow()
	}
}

//TestFinancingOfferAsset_Invoke_addNewFinancingOffer
func TestFinancingOfferAsset_Invoke_addNewFinancingOfferDuplicate(t *testing.T) {
	financingOffer := new(FinancingOfferAsset)
	stub := shim.NewMockStub("financingOffer", financingOffer)
	checkInit(t, stub, [][]byte{[]byte("init")})
	checkInvoke(t, stub, getFirstFinancingOfferAssetForTesting())
	newFinancingOfferID := "FO100001"
	checkState(t, stub, newFinancingOfferID, getNewFinancingOfferExpected())
	checkState(t, stub, "financingOfferIDIndex", getExpectedFinancingOfferIDIndex("addNewFinancingOffer"))
	res := stub.MockInvoke("1", getFirstFinancingOfferAssetForTesting())
	if res.Status != shim.OK {
		checkError(t, "This FinancingOffer already exists: FO100001", res.Message)
	} else {
		fmt.Println("Error was expected, but not raised")
		t.FailNow()
	}
}

//TestFinancingOfferAsset_Invoke_setFinancingOfferSubmittedOK
func TestFinancingOfferAsset_Invoke_setFinancingSubmittedOK(t *testing.T) {
	financingOffer := new(FinancingOfferAsset)
	stub := shim.NewMockStub("financingOffer", financingOffer)
	checkInit(t, stub, [][]byte{[]byte("init")})
	checkInvoke(t, stub, getFirstFinancingOfferAssetForTesting())
	financingOfferID := "FO100001"
	res := stub.MockInvoke("1", getFinancingOfferForSubmittedOK())
	if res.Status != shim.OK {
		checkError(t, "FinancingOffer with ID: "+financingOfferID+" cannot be submitted", res.Message)
	}
	res = stub.MockInvoke("1", [][]byte{[]byte("readFinancingOffer"), []byte(financingOfferID)})
	if res.Status != shim.OK {
		fmt.Println("func readFinancingOffer with ID: ", financingOfferID, " failed"+string(res.Message))
		t.FailNow()
	}
	if res.Payload == nil {
		fmt.Println("func readFinancingOffer with ID: ", financingOfferID, "failed to get value")
		t.FailNow()
	} else {
		var resInv FinancingOffer
		err := json.Unmarshal(res.Payload, &resInv)
		if err != nil {
			fmt.Println(err.Error())
			t.FailNow()
		}
		checkError(t, Submitted, resInv.Status)
	}
}

//TestFinancingOfferAsset_Invoke_setFinancingOfferSubmittedNOK
func TestFinancingOfferAsset_Invoke_setFinancingOfferSubmittedNOK(t *testing.T) {
	financingOffer := new(FinancingOfferAsset)
	stub := shim.NewMockStub("financingOffer", financingOffer)
	checkInit(t, stub, [][]byte{[]byte("init")})
	checkInvoke(t, stub, getFinancingOfferForNOKTestSetFinancingSubmitted())
	financingOfferID := "FO100001"
	res := stub.MockInvoke("1", getFinancingOfferForSubmittedNOK())
	if res.Status != shim.OK {
		checkError(t, "FinancingOffer with ID: "+financingOfferID+" cannot be submitted", res.Message)
	}
	res = stub.MockInvoke("1", [][]byte{[]byte("readFinancingOffer"), []byte(financingOfferID)})
	if res.Status != shim.OK {
		fmt.Println("func readFinancingOffer with ID: ", financingOfferID, " failed"+string(res.Message))
		t.FailNow()
	}
	if res.Payload == nil {
		fmt.Println("func readFinancingOffer with ID: ", financingOfferID, "failed to get value")
		t.FailNow()
	} else {
		var resInv FinancingOffer
		err := json.Unmarshal(res.Payload, &resInv)
		if err != nil {
			fmt.Println(err.Error())
			t.FailNow()
		}
		checkError(t, Submitted, resInv.Status)
	}
}

//TestFinancingOfferAsset_Invoke_setFinancingOfferAcceptedOK
func TestFinancingOfferAsset_Invoke_setFinancingAcceptedOK(t *testing.T) {
	financingOffer := new(FinancingOfferAsset)
	stub := shim.NewMockStub("financingOffer", financingOffer)
	checkInit(t, stub, [][]byte{[]byte("init")})
	checkInvoke(t, stub, getFinancingOfferAssetForAcceptedTesting())
	financingOfferID := "FO100001"
	res := stub.MockInvoke("1", [][]byte{[]byte("setFinancingOfferAccepted"), []byte(financingOfferID)})
	if res.Status != shim.OK {
		checkError(t, "FinancingOffer with ID: "+financingOfferID+" cannot be accepted", res.Message)
	}
	res = stub.MockInvoke("1", [][]byte{[]byte("readFinancingOffer"), []byte(financingOfferID)})
	if res.Status != shim.OK {
		fmt.Println("func readFinancingOffer with ID: ", financingOfferID, " failed"+string(res.Message))
		t.FailNow()
	}
	if res.Payload == nil {
		fmt.Println("func readFinancingOffer with ID: ", financingOfferID, "failed to get value")
		t.FailNow()
	} else {
		var resInv FinancingOffer
		err := json.Unmarshal(res.Payload, &resInv)
		if err != nil {
			fmt.Println(err.Error())
			t.FailNow()
		}
		checkError(t, Accepted, resInv.Status)
	}
}

//TestFinancingOfferAsset_Invoke_setFinancingOfferAcceptedNOK
func TestFinancingOfferAsset_Invoke_setFinancingOfferAcceptedNOK(t *testing.T) {
	financingOffer := new(FinancingOfferAsset)
	stub := shim.NewMockStub("financingOffer", financingOffer)
	checkInit(t, stub, [][]byte{[]byte("init")})
	checkInvoke(t, stub, getFinancingOfferForNOKTestSetFinancingAccepted())
	financingOfferID := "FO100001"
	res := stub.MockInvoke("1", [][]byte{[]byte("setFinancingOfferAccepted"), []byte(financingOfferID)})
	if res.Status != shim.OK {
		checkError(t, "FinancingOffer with ID: "+financingOfferID+" cannot be accepted", res.Message)
	}
	res = stub.MockInvoke("1", [][]byte{[]byte("readFinancingOffer"), []byte(financingOfferID)})
	if res.Status != shim.OK {
		fmt.Println("func readFinancingOffer with ID: ", financingOfferID, " failed"+string(res.Message))
		t.FailNow()
	}
	if res.Payload == nil {
		fmt.Println("func readFinancingOffer with ID: ", financingOfferID, "failed to get value")
		t.FailNow()
	} else {
		var resInv FinancingOffer
		err := json.Unmarshal(res.Payload, &resInv)
		if err != nil {
			fmt.Println(err.Error())
			t.FailNow()
		}
		checkError(t, Accepted, resInv.Status)
	}
}

//TestFinancingOfferAsset_Invoke_setFinancingOfferDeclinedOK
func TestFinancingOfferAsset_Invoke_setFinancingDeclinedOK(t *testing.T) {
	financingOffer := new(FinancingOfferAsset)
	stub := shim.NewMockStub("financingOffer", financingOffer)
	checkInit(t, stub, [][]byte{[]byte("init")})
	checkInvoke(t, stub, getFinancingOfferAssetForAcceptedTesting())
	financingOfferID := "FO100001"
	res := stub.MockInvoke("1", [][]byte{[]byte("setFinancingOfferDeclined"), []byte(financingOfferID)})
	if res.Status != shim.OK {
		checkError(t, "FinancingOffer with ID: "+financingOfferID+" cannot be declined", res.Message)
	}
	res = stub.MockInvoke("1", [][]byte{[]byte("readFinancingOffer"), []byte(financingOfferID)})
	if res.Status != shim.OK {
		fmt.Println("func readFinancingOffer with ID: ", financingOfferID, " failed"+string(res.Message))
		t.FailNow()
	}
	if res.Payload == nil {
		fmt.Println("func readFinancingOffer with ID: ", financingOfferID, "failed to get value")
		t.FailNow()
	} else {
		var resInv FinancingOffer
		err := json.Unmarshal(res.Payload, &resInv)
		if err != nil {
			fmt.Println(err.Error())
			t.FailNow()
		}
		checkError(t, Declined, resInv.Status)
	}
}

//TestFinancingOfferAsset_Invoke_setFinancingOfferDeclinedNOK
func TestFinancingOfferAsset_Invoke_setFinancingOfferDeclinedNOK(t *testing.T) {
	financingOffer := new(FinancingOfferAsset)
	stub := shim.NewMockStub("financingOffer", financingOffer)
	checkInit(t, stub, [][]byte{[]byte("init")})
	checkInvoke(t, stub, getFinancingOfferForNOKTestSetFinancingAccepted())
	financingOfferID := "FO100001"
	res := stub.MockInvoke("1", [][]byte{[]byte("setFinancingOfferDeclined"), []byte(financingOfferID)})
	if res.Status != shim.OK {
		checkError(t, "FinancingOffer with ID: "+financingOfferID+" cannot be declined", res.Message)
	}
	res = stub.MockInvoke("1", [][]byte{[]byte("readFinancingOffer"), []byte(financingOfferID)})
	if res.Status != shim.OK {
		fmt.Println("func readFinancingOffer with ID: ", financingOfferID, " failed"+string(res.Message))
		t.FailNow()
	}
	if res.Payload == nil {
		fmt.Println("func readFinancingOffer with ID: ", financingOfferID, "failed to get value")
		t.FailNow()
	} else {
		var resInv FinancingOffer
		err := json.Unmarshal(res.Payload, &resInv)
		if err != nil {
			fmt.Println(err.Error())
			t.FailNow()
		}
		checkError(t, Accepted, resInv.Status)
	}
}

//TestFinancingOfferAsset_Invoke_setFinancingOfferDisbursementInitiatedOK
func TestFinancingOfferAsset_Invoke_setFinancingDisbursementInitiatedOK(t *testing.T) {
	financingOffer := new(FinancingOfferAsset)
	stub := shim.NewMockStub("financingOffer", financingOffer)
	checkInit(t, stub, [][]byte{[]byte("init")})
	checkInvoke(t, stub, getFinancingOfferAssetForDisbursementInitiatedTesting())
	financingOfferID := "FO100001"
	res := stub.MockInvoke("1", getFinancingOfferSetDisbursementInitiated())
	if res.Status != shim.OK {
		checkError(t, "FinancingOffer with ID: "+financingOfferID+" cannot be paid out", res.Message)
	}
	res = stub.MockInvoke("1", [][]byte{[]byte("readFinancingOffer"), []byte(financingOfferID)})
	if res.Status != shim.OK {
		fmt.Println("func readFinancingOffer with ID: ", financingOfferID, " failed"+string(res.Message))
		t.FailNow()
	}
	if res.Payload == nil {
		fmt.Println("func readFinancingOffer with ID: ", financingOfferID, "failed to get value")
		t.FailNow()
	} else {
		var resInv FinancingOffer
		err := json.Unmarshal(res.Payload, &resInv)
		if err != nil {
			fmt.Println(err.Error())
			t.FailNow()
		}
		checkError(t, DisbursementInitiated, resInv.Status)
	}
}

//TestFinancingOfferAsset_Invoke_setFinancingOfferDisbursementInitiatedNOK
func TestFinancingOfferAsset_Invoke_setFinancingOfferDisbursementInitiatedNOK(t *testing.T) {
	financingOffer := new(FinancingOfferAsset)
	stub := shim.NewMockStub("financingOffer", financingOffer)
	checkInit(t, stub, [][]byte{[]byte("init")})
	checkInvoke(t, stub, getFinancingOfferForNOKTestSetFinancingDisbursementInitiated())
	financingOfferID := "FO100001"
	res := stub.MockInvoke("1", getFinancingOfferSetDisbursementInitiated())
	if res.Status != shim.OK {
		checkError(t, "FinancingOffer with ID: "+financingOfferID+" cannot be disbursement initiated", res.Message)
	}
	res = stub.MockInvoke("1", [][]byte{[]byte("readFinancingOffer"), []byte(financingOfferID)})
	if res.Status != shim.OK {
		fmt.Println("func readFinancingOffer with ID: ", financingOfferID, " failed"+string(res.Message))
		t.FailNow()
	}
	if res.Payload == nil {
		fmt.Println("func readFinancingOffer with ID: ", financingOfferID, "failed to get value")
		t.FailNow()
	} else {
		var resInv FinancingOffer
		err := json.Unmarshal(res.Payload, &resInv)
		if err != nil {
			fmt.Println(err.Error())
			t.FailNow()
		}
		checkError(t, DisbursementInitiated, resInv.Status)
	}
}

//TestFinancingOfferAsset_Invoke_removeFinancingOfferOK  //change template
func TestFinancingOfferAsset_Invoke_removeFinancingOfferOK(t *testing.T) {
	financingOffer := new(FinancingOfferAsset)
	stub := shim.NewMockStub("financingOffer", financingOffer)
	checkInit(t, stub, [][]byte{[]byte("init")})
	checkInvoke(t, stub, getFirstFinancingOfferAssetForTesting())
	checkInvoke(t, stub, getSecondFinancingOfferAssetForTesting())
	checkReadAllFinancingOffersOK(t, stub)
	checkState(t, stub, "financingOfferIDIndex", getExpectedFinancingOfferIDIndex("beforeRemoveFinancingOffer"))
	checkInvoke(t, stub, getRemoveSecondFinancingOfferAssetForTesting())
	remainingFinancingOfferID := "FO100001"
	checkReadFinancingOfferOK(t, stub, remainingFinancingOfferID)
	checkState(t, stub, "financingOfferIDIndex", getExpectedFinancingOfferIDIndex("afterRemoveFinancingOffer"))
}

//TestFinancingOfferAsset_Invoke_removeAllFinancingOfferssOK
func TestFinancingOfferAsset_Invoke_removeAllFinancingOfferssOK(t *testing.T) {
	financier := new(FinancingOfferAsset)
	stub := shim.NewMockStub("financier", financier)
	checkInit(t, stub, [][]byte{[]byte("init")})
	checkInvoke(t, stub, getFirstFinancingOfferAssetForTesting())
	checkInvoke(t, stub, getSecondFinancingOfferAssetForTesting())
	checkReadAllFinancingOffersOK(t, stub)
	checkState(t, stub, "financingOfferIDIndex", getExpectedFinancingOfferIDIndex("beforeRemoveFinancingOffer"))
	checkInvoke(t, stub, getRemoveAllFinancingOfferAssetsForTesting())
	checkState(t, stub, "financingOfferIDIndex", getExpectedFinancingOfferIDIndex(""))
}

//TestFinancingOfferAsset_Invoke_removeFinancingOfferNOK  //change template
func TestFinancingOfferAsset_Invoke_removeFinancingOfferNOK(t *testing.T) {
	financingOffer := new(FinancingOfferAsset)
	stub := shim.NewMockStub("financingOffer", financingOffer)
	checkInit(t, stub, [][]byte{[]byte("init")})
	checkInvoke(t, stub, getFirstFinancingOfferAssetForTesting())
	firstFinancingOfferID := "FO100001"
	checkReadFinancingOfferOK(t, stub, firstFinancingOfferID)
	checkState(t, stub, "financingOfferIDIndex", getExpectedFinancingOfferIDIndex("addNewFinancingOffer"))
	res := stub.MockInvoke("1", getRemoveSecondFinancingOfferAssetForTesting())
	if res.Status != shim.OK {
		checkError(t, "FinancingOffer with ID: "+"FO100002"+" not found", res.Message)
	} else {
		fmt.Println("Error was expected, but not raised")
		t.FailNow()
	}
	checkState(t, stub, "financingOfferIDIndex", getExpectedFinancingOfferIDIndex("addNewFinancingOffer"))
}

//TestFinancingOfferAsset_Query_readFinancingOffer
func TestFinancingOfferAsset_Query_readFinancingOffer(t *testing.T) {
	financingOffer := new(FinancingOfferAsset)
	stub := shim.NewMockStub("financingOffer", financingOffer)
	checkInit(t, stub, [][]byte{[]byte("init")})
	financingOfferID := "FO100001"
	checkInvoke(t, stub, getFirstFinancingOfferAssetForTesting())
	checkReadFinancingOfferOK(t, stub, financingOfferID)
	checkReadFinancingOfferNOK(t, stub, "")
}

//TestFinancingOfferAsset_Query_readAllFinancingOffers
func TestFinancingOfferAsset_Query_readAllFinancingOffers(t *testing.T) {
	financingOffer := new(FinancingOfferAsset)
	stub := shim.NewMockStub("financingOffer", financingOffer)
	checkInit(t, stub, [][]byte{[]byte("init")})
	checkInvoke(t, stub, getFirstFinancingOfferAssetForTesting())
	checkInvoke(t, stub, getSecondFinancingOfferAssetForTesting())
	checkReadAllFinancingOffersOK(t, stub)
}

/*
*	Helper Functions
 */
func getFirstFinancingOfferAssetForTesting() [][]byte {
	return [][]byte{[]byte("addNewFinancingOffer"),
		[]byte("{\"ID\":\"FO100001\"," +
			"\"supplierID\":\"SU100001\"," +
			"\"purchaserID\":\"PU100001\"," +
			"\"financerID\":\"FI100001\"," +
			"\"invoiceID\":\"IN100001\"," +
			"\"purchaseOrderID\":\"PO100001\"," +
			"\"docType\":\"FinancingOffer\"," +
			"\"status\":\"Created\"," +
			"\"invoiceGrossAmount\":\"100.00\"," +
			"\"discountPercent\":\"2.00\"," +
			"\"discountAmount\":\"2.00\"," +
			"\"fundedAmount\":\"98.00\"," +
			"\"offerValidityStartDate\":\"12/01/2017\"," +
			"\"offerValidityEndDate\":\"12/01/2017\"," +
			"\"disbursementDate\":\"12/01/2017\"," +
			"\"disbursementReference\":\"PAYREF001\"," +
			"\"currency\":\"SGD\"}")}
}

//Get FinancingOfferAsset with unknown field for testing
func getFinancingOfferAssetWithUnknownFieldForTesting() [][]byte {
	return [][]byte{[]byte("addNewFinancingOffer"),
		[]byte("{\"ID\":\"FO100001\"," +
			"\"supplierID\":\"SU100001\"," +
			"\"purchaserID\":\"PU100001\"," +
			"\"financerID\":\"FI100001\"," +
			"\"invoiceID\":\"IN100001\"," +
			"\"purchaseOrderID\":\"PO100001\"," +
			"\"docuType\":\"FinancingOffer\"," +
			"\"status\":\"Created\"," +
			"\"invoiceGrossAmount\":\"100.00\"," +
			"\"discountPercent\":\"2.00\"," +
			"\"discountAmount\":\"2.00\"," +
			"\"fundedAmount\":\"98.00\"," +
			"\"offerValidityStartDate\":\"12/01/2017\"," +
			"\"offerValidityEndDate\":\"12/01/2017\"," +
			"\"disbursementDate\":\"12/01/2017\"," +
			"\"disbursementReference\":\"PAYREF001\"," +
			"\"currency\":\"SGD\"}")}
}

func getSecondFinancingOfferAssetForTesting() [][]byte {
	return [][]byte{[]byte("addNewFinancingOffer"),
		[]byte("{\"ID\":\"FO100002\"," +
			"\"supplierID\":\"SU100001\"," +
			"\"purchaserID\":\"PU100001\"," +
			"\"financerID\":\"FI100001\"," +
			"\"invoiceID\":\"IN100001\"," +
			"\"purchaseOrderID\":\"PO100001\"," +
			"\"docType\":\"FinancingOffer\"," +
			"\"status\":\"Created\"," +
			"\"invoiceGrossAmount\":\"100.00\"," +
			"\"discountPercent\":\"2.00\"," +
			"\"discountAmount\":\"2.00\"," +
			"\"fundedAmount\":\"98.00\"," +
			"\"offerValidityStartDate\":\"12/01/2017\"," +
			"\"offerValidityEndDate\":\"12/01/2017\"," +
			"\"disbursementDate\":\"12/01/2017\"," +
			"\"disbursementReference\":\"PAYREF001\"," +
			"\"currency\":\"SGD\"}")}
}

func getFinancingOfferForSubmittedOK() [][]byte {
	return [][]byte{[]byte("setFinancingOfferSubmitted"),
		[]byte("{\"ID\":\"FO100001\"," +
			"\"supplierID\":\"SU100001\"," +
			"\"purchaserID\":\"PU100001\"," +
			"\"financerID\":\"FI100001\"," +
			"\"invoiceID\":\"IN100001\"," +
			"\"purchaseOrderID\":\"PO100001\"," +
			"\"docType\":\"FinancingOffer\"," +
			"\"status\":\"Created\"," +
			"\"invoiceGrossAmount\":\"100.00\"," +
			"\"discountPercent\":\"2.00\"," +
			"\"discountAmount\":\"2.00\"," +
			"\"fundedAmount\":\"98.00\"," +
			"\"offerValidityStartDate\":\"12/01/2017\"," +
			"\"offerValidityEndDate\":\"12/01/2017\"," +
			"\"disbursementDate\":\"12/01/2017\"," +
			"\"disbursementReference\":\"PAYREF001\"," +
			"\"currency\":\"SGD\"}")}
}

func getFinancingOfferForSubmittedNOK() [][]byte {
	return [][]byte{[]byte("setFinancingOfferSubmitted"),
		[]byte("{\"ID\":\"FO100001\"," +
			"\"supplierID\":\"SU100001\"," +
			"\"purchaserID\":\"PU100001\"," +
			"\"financerID\":\"FI100001\"," +
			"\"invoiceID\":\"IN100001\"," +
			"\"purchaseOrderID\":\"PO100001\"," +
			"\"docType\":\"FinancingOffer\"," +
			"\"status\":\"Submitted\"," +
			"\"invoiceGrossAmount\":\"100.00\"," +
			"\"discountPercent\":\"2.00\"," +
			"\"discountAmount\":\"2.00\"," +
			"\"fundedAmount\":\"98.00\"," +
			"\"offerValidityStartDate\":\"12/01/2017\"," +
			"\"offerValidityEndDate\":\"12/01/2017\"," +
			"\"disbursementDate\":\"12/01/2017\"," +
			"\"disbursementReference\":\"PAYREF001\"," +
			"\"currency\":\"SGD\"}")}
}

func getFinancingOfferForNOKTestSetFinancingSubmitted() [][]byte {
	return [][]byte{[]byte("addNewFinancingOffer"),
		[]byte("{\"ID\":\"FO100001\"," +
			"\"supplierID\":\"SU100001\"," +
			"\"purchaserID\":\"PU100001\"," +
			"\"financerID\":\"FI100001\"," +
			"\"invoiceID\":\"IN100001\"," +
			"\"purchaseOrderID\":\"PO100001\"," +
			"\"docType\":\"FinancingOffer\"," +
			"\"status\":\"Created\"," +
			"\"invoiceGrossAmount\":\"100.00\"," +
			"\"discountPercent\":\"2.00\"," +
			"\"discountAmount\":\"2.00\"," +
			"\"fundedAmount\":\"98.00\"," +
			"\"offerValidityStartDate\":\"12/01/2017\"," +
			"\"offerValidityEndDate\":\"12/01/2017\"," +
			"\"disbursementDate\":\"12/01/2017\"," +
			"\"disbursementReference\":\"PAYREF001\"," +
			"\"currency\":\"SGD\"}")}
}

func getFinancingOfferAssetForAcceptedTesting() [][]byte {
	return [][]byte{[]byte("addNewFinancingOffer"),
		[]byte("{\"ID\":\"FO100001\"," +
			"\"supplierID\":\"SU100001\"," +
			"\"purchaserID\":\"PU100001\"," +
			"\"financerID\":\"FI100001\"," +
			"\"invoiceID\":\"IN100001\"," +
			"\"purchaseOrderID\":\"PO100001\"," +
			"\"docType\":\"FinancingOffer\"," +
			"\"status\":\"Submitted\"," +
			"\"invoiceGrossAmount\":\"100.00\"," +
			"\"discountPercent\":\"2.00\"," +
			"\"discountAmount\":\"2.00\"," +
			"\"fundedAmount\":\"98.00\"," +
			"\"offerValidityStartDate\":\"12/01/2017\"," +
			"\"offerValidityEndDate\":\"12/01/2017\"," +
			"\"disbursementDate\":\"12/01/2017\"," +
			"\"disbursementReference\":\"PAYREF001\"," +
			"\"currency\":\"SGD\"}")}
}

func getFinancingOfferAssetForDisbursementInitiatedTesting() [][]byte {
	return [][]byte{[]byte("addNewFinancingOffer"),
		[]byte("{\"ID\":\"FO100001\"," +
			"\"supplierID\":\"SU100001\"," +
			"\"purchaserID\":\"PU100001\"," +
			"\"financerID\":\"FI100001\"," +
			"\"invoiceID\":\"IN100001\"," +
			"\"purchaseOrderID\":\"PO100001\"," +
			"\"docType\":\"FinancingOffer\"," +
			"\"status\":\"Accepted\"," +
			"\"invoiceGrossAmount\":\"100.00\"," +
			"\"discountPercent\":\"2.00\"," +
			"\"discountAmount\":\"2.00\"," +
			"\"fundedAmount\":\"98.00\"," +
			"\"offerValidityStartDate\":\"12/01/2017\"," +
			"\"offerValidityEndDate\":\"12/01/2017\"," +
			"\"disbursementDate\":\"12/01/2017\"," +
			"\"disbursementReference\":\"PAYREF001\"," +
			"\"currency\":\"SGD\"}")}
}

func getFinancingOfferForNOKTestSetFinancingAccepted() [][]byte {
	return [][]byte{[]byte("addNewFinancingOffer"),
		[]byte("{\"ID\":\"FO100001\"," +
			"\"supplierID\":\"SU100001\"," +
			"\"purchaserID\":\"PU100001\"," +
			"\"financerID\":\"FI100001\"," +
			"\"invoiceID\":\"IN100001\"," +
			"\"purchaseOrderID\":\"PO100001\"," +
			"\"docType\":\"FinancingOffer\"," +
			"\"status\":\"Accepted\"," +
			"\"invoiceGrossAmount\":\"100.00\"," +
			"\"discountPercent\":\"2.00\"," +
			"\"discountAmount\":\"2.00\"," +
			"\"fundedAmount\":\"98.00\"," +
			"\"offerValidityStartDate\":\"12/01/2017\"," +
			"\"offerValidityEndDate\":\"12/01/2017\"," +
			"\"disbursementDate\":\"12/01/2017\"," +
			"\"disbursementReference\":\"PAYREF001\"," +
			"\"currency\":\"SGD\"}")}
}

func getFinancingOfferForNOKTestSetFinancingDisbursementInitiated() [][]byte {
	return [][]byte{[]byte("addNewFinancingOffer"),
		[]byte("{\"ID\":\"FO100001\"," +
			"\"supplierID\":\"SU100001\"," +
			"\"purchaserID\":\"PU100001\"," +
			"\"financerID\":\"FI100001\"," +
			"\"invoiceID\":\"IN100001\"," +
			"\"purchaseOrderID\":\"PO100001\"," +
			"\"docType\":\"FinancingOffer\"," +
			"\"status\":\"Disbursement Initiated\"," +
			"\"invoiceGrossAmount\":\"100.00\"," +
			"\"discountPercent\":\"2.00\"," +
			"\"discountAmount\":\"2.00\"," +
			"\"fundedAmount\":\"98.00\"," +
			"\"offerValidityStartDate\":\"12/01/2017\"," +
			"\"offerValidityEndDate\":\"12/01/2017\"," +
			"\"disbursementDate\":\"12/01/2017\"," +
			"\"disbursementReference\":\"PAYREF001\"," +
			"\"currency\":\"SGD\"}")}
}

//Get remove second FinancingOfferAsset for testing //change template
func getFinancingOfferSetDisbursementInitiated() [][]byte {
	return [][]byte{[]byte("setFinancingOfferDisbursementInitiated"),
		[]byte("{\"ID\":\"FO100001\"," +
			"\"supplierID\":\"SU100001\"," +
			"\"purchaserID\":\"PU100001\"," +
			"\"financerID\":\"FI100001\"," +
			"\"invoiceID\":\"IN100001\"," +
			"\"purchaseOrderID\":\"PO100001\"," +
			"\"docType\":\"FinancingOffer\"," +
			"\"status\":\"Submitted\"," +
			"\"invoiceGrossAmount\":\"100.00\"," +
			"\"discountPercent\":\"2.00\"," +
			"\"discountAmount\":\"2.00\"," +
			"\"fundedAmount\":\"98.00\"," +
			"\"offerValidityStartDate\":\"12/01/2017\"," +
			"\"offerValidityEndDate\":\"12/01/2017\"," +
			"\"disbursementDate\":\"14/01/2017\"," +
			"\"disbursementReference\":\"MyREf\"," +
			"\"currency\":\"SGD\"}")}
}

//Get remove second FinancingOfferAsset for testing //change template
func getRemoveSecondFinancingOfferAssetForTesting() [][]byte {
	return [][]byte{[]byte("removeFinancingOffer"),
		[]byte("FO100002")}
}

//Get remove all FinanciingOfferAssets for testing
func getRemoveAllFinancingOfferAssetsForTesting() [][]byte {
	return [][]byte{[]byte("removeAllFinancingOffers")}
}

//Get an expected value for testing
func getNewFinancingOfferExpected() []byte {
	var financingOffer FinancingOffer
	financingOffer.ID = "FO100001"
	financingOffer.SupplierID = "SU100001"
	financingOffer.PurchaserID = "PU100001"
	financingOffer.FinancerID = "FI100001"
	financingOffer.InvoiceID = "IN100001"
	financingOffer.PurchaseOrderID = "PO100001"
	financingOffer.ObjectType = "Asset.FinancingOffer"
	financingOffer.Status = "Created"
	financingOffer.InvoiceGrossAmount = "100.00"
	financingOffer.DiscountPercent = "2.00"
	financingOffer.DiscountAmount = "2.00"
	financingOffer.FundedAmount = "98.00"
	financingOffer.OfferValidityStartDate = "12/01/2017"
	financingOffer.OfferValidityEndDate = "12/01/2017"
	financingOffer.DisbursementDate = "12/01/2017"
	financingOffer.DisbursementReference = "PAYREF001"
	financingOffer.Currency = "SGD"
	financingOfferJSON, err := json.Marshal(financingOffer)
	if err != nil {
		fmt.Println("Error converting a FinancingOffer record to JSON")
		return nil
	}
	return []byte(financingOfferJSON)
}

//Get expected values of FinancingOffers for testing
func getExpectedFinancingOffers() []byte {
	var financingOffers []FinancingOffer
	var financingOffer FinancingOffer
	financingOffer.ID = "FO100001"
	financingOffer.SupplierID = "SU100001"
	financingOffer.PurchaserID = "PU100001"
	financingOffer.FinancerID = "FI100001"
	financingOffer.InvoiceID = "IN100001"
	financingOffer.PurchaseOrderID = "PO100001"
	financingOffer.ObjectType = "Asset.FinancingOffer"
	financingOffer.Status = "Created"
	financingOffer.InvoiceGrossAmount = "100.00"
	financingOffer.DiscountPercent = "2.00"
	financingOffer.DiscountAmount = "2.00"
	financingOffer.FundedAmount = "98.00"
	financingOffer.OfferValidityStartDate = "12/01/2017"
	financingOffer.OfferValidityEndDate = "12/01/2017"
	financingOffer.DisbursementDate = "12/01/2017"
	financingOffer.DisbursementReference = "PAYREF001"
	financingOffer.Currency = "SGD"
	financingOffers = append(financingOffers, financingOffer)
	financingOffer.ID = "FO100002"
	financingOffer.SupplierID = "SU100001"
	financingOffer.PurchaserID = "PU100001"
	financingOffer.FinancerID = "FI100001"
	financingOffer.InvoiceID = "IN100001"
	financingOffer.PurchaseOrderID = "PO100001"
	financingOffer.ObjectType = "Asset.FinancingOffer"
	financingOffer.Status = "Created"
	financingOffer.InvoiceGrossAmount = "100.00"
	financingOffer.DiscountPercent = "2.00"
	financingOffer.DiscountAmount = "2.00"
	financingOffer.FundedAmount = "98.00"
	financingOffer.OfferValidityStartDate = "12/01/2017"
	financingOffer.OfferValidityEndDate = "12/01/2017"
	financingOffer.DisbursementDate = "12/01/2017"
	financingOffer.DisbursementReference = "PAYREF001"
	financingOffer.Currency = "SGD"
	financingOffers = append(financingOffers, financingOffer)
	financingOfferJSON, err := json.Marshal(financingOffers)
	if err != nil {
		fmt.Println("Error converting financingOffer records to JSON")
		return nil
	}
	return []byte(financingOfferJSON)
}

func getExpectedFinancingOfferIDIndex(funcName string) []byte {
	var financingOfferIDIndex FinancingOfferIDIndex
	switch funcName {
	case "addNewFinancingOffer":
		financingOfferIDIndex.IDs = append(financingOfferIDIndex.IDs, "FO100001")
		financingOfferIDIndexBytes, err := json.Marshal(financingOfferIDIndex)
		if err != nil {
			fmt.Println("Error converting FinancingOfferIDIndex to JSON")
			return nil
		}
		return financingOfferIDIndexBytes
	case "beforeRemoveFinancingOffer":
		financingOfferIDIndex.IDs = append(financingOfferIDIndex.IDs, "FO100001")
		financingOfferIDIndex.IDs = append(financingOfferIDIndex.IDs, "FO100002")
		financingOfferIDIndexBytes, err := json.Marshal(financingOfferIDIndex)
		if err != nil {
			fmt.Println("Error converting FinancingOfferIDIndex to JSON")
			return nil
		}
		return financingOfferIDIndexBytes
	case "afterRemoveFinancingOffer":
		financingOfferIDIndex.IDs = append(financingOfferIDIndex.IDs, "FO100001")
		financingOfferIDIndexBytes, err := json.Marshal(financingOfferIDIndex)
		if err != nil {
			fmt.Println("Error converting FinancingOfferIDIndex to JSON")
			return nil
		}
		return financingOfferIDIndexBytes
	default:
		financingOfferIDIndexBytes, err := json.Marshal(financingOfferIDIndex)
		if err != nil {
			fmt.Println("Error converting FinancingOfferIDIndex to JSON")
			return nil
		}
		return financingOfferIDIndexBytes
	}
}

//checkInit - helper to check the Initialization of chaincode: FinancingOfferAsset
func checkInit(t *testing.T, stub *shim.MockStub, args [][]byte) {
	res := stub.MockInit("1", args)
	if res.Status != shim.OK {
		fmt.Println("Init failed", string(res.Message))
		t.FailNow()
	}
}

//checkState - helper for checking the chaincode state for a given stateKey afgainst an expected value
func checkState(t *testing.T, stub *shim.MockStub, stateKey string, expectedState []byte) {
	actualState := stub.State[stateKey]
	if actualState == nil {
		fmt.Println("State for ", stateKey, ": failed to get value")
		t.FailNow()
	}

	if bytes.Compare(actualState, expectedState) != 0 {
		fmt.Println("Incorrect State for " + stateKey + ": \nExpected: " + string(expectedState) + "\nActual  : " + string(actualState))
		t.FailNow()
	}
}

//checkInvoke - helper for checking Invoke of chaincode
func checkInvoke(t *testing.T, stub *shim.MockStub, args [][]byte) {
	res := stub.MockInvoke("1", args)
	if res.Status != shim.OK {
		fmt.Println("Invoke", args, "failed", string(res.Message))
		t.FailNow()
	}
}

func checkInvokeUnknownFunction(t *testing.T, stub *shim.MockStub, args [][]byte) {
	res := stub.MockInvoke("1", args)
	if res.Status != shim.OK {
		expectedErr := "Received unknown function invocation"
		actualErr := string(res.Message)
		if !(strings.Contains(actualErr, expectedErr)) {
			fmt.Println("Invoke with unknown function must throw error: ", expectedErr, "\n Actual Error :", actualErr)
			t.FailNow()
		}
	} else {
		t.FailNow()
	}
}

//checkReadFinancingOfferOK - helper for positive test readFinancingOffer
func checkReadFinancingOfferOK(t *testing.T, stub *shim.MockStub, financingOfferID string) {
	res := stub.MockInvoke("1", [][]byte{[]byte("readFinancingOffer"), []byte(financingOfferID)})
	if res.Status != shim.OK {
		fmt.Println("func readFinancingOffer with ID: ", financingOfferID, " failed"+string(res.Message))
		t.FailNow()
	}
	if res.Payload == nil {
		fmt.Println("func readFinancingOffer with ID: ", financingOfferID, "failed to get value")
		t.FailNow()
	}
	if bytes.Compare(getNewFinancingOfferExpected(), []byte(res.Payload)) != 0 {
		fmt.Println("func readFinancingOffer with ID: ", financingOfferID, "Expected:", string(getNewFinancingOfferExpected()), "Actual:", string(res.Payload))
		t.FailNow()
	}
}

//checkReadFinancingOfferNOK - helper for negative testing of readFinancingOffer
func checkReadFinancingOfferNOK(t *testing.T, stub *shim.MockStub, financingOfferID string) {
	//with no financingOfferID
	res := stub.MockInvoke("1", [][]byte{[]byte("readFinancingOffer"), []byte("")})
	if res.Status != shim.OK {
		expectedErr := "retrieveFinancingOffer: Corrupt financingOffer record "
		actualErr := string(res.Message)
		if !(strings.Contains(actualErr, expectedErr)) {
			fmt.Println("func readFinancingOffer negative test: ", "Expected Error:", expectedErr, "Actual Error", actualErr)
			t.FailNow()
		}
	} else {
		t.FailNow()
	}
}

func checkReadAllFinancingOffersOK(t *testing.T, stub *shim.MockStub) {
	res := stub.MockInvoke("1", [][]byte{[]byte("readAllFinancingOffers")})
	if res.Status != shim.OK {
		fmt.Println("func readAllFinancingOffers failed", string(res.Message))
		t.FailNow()
	}
	if res.Payload == nil {
		fmt.Println("func readAllFinancingOffers failed to get value")
		t.FailNow()
	}
	if bytes.Compare(getExpectedFinancingOffers(), []byte(res.Payload)) != 0 {
		fmt.Println("func readAllFinancingOffers Expected:\n", string(getExpectedFinancingOffers()), "\nActual:\n", string(res.Payload))
		t.FailNow()
	}
}

func checkError(t *testing.T, exp string, act string) {
	if strings.Compare(exp, act) != 0 {
		fmt.Println("Unexpected Error! Expecting ", exp, "\n Actual :", act)
		t.FailNow()
	}
}
