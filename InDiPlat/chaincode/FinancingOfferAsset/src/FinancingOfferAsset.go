package main

import (
	"encoding/json"
	"errors"
	"fmt"
	"strings"

	"github.com/hyperledger/fabric/core/chaincode/shim"
	"github.com/hyperledger/fabric/protos/peer"
)

var logger = shim.NewLogger("CLDChaincode")

//Created - Created - Start State
const Created = "Created"

//Submitted - Submitted - Intermediate State
const Submitted = "Submitted"

//Accepted - Accepted - Intermediate State
const Accepted = "Accepted"

//Declined - Declined - End State
const Declined = "Declined"

//DisbursementInitiated - Disbursement Initiated - End State
const DisbursementInitiated = "Disbursement Initiated"

//FinancingOfferAsset - Chaincode for asset FinancingOffer
type FinancingOfferAsset struct {
}

//FinancingOffer - Details of the asset type FinancingOffer
type FinancingOffer struct {
	ID                     string `json:"ID"`
	SupplierID             string `json:"supplierID"`
	PurchaserID            string `json:"purchaserID"`
	FinancerID             string `json:"financerID"`
	InvoiceID              string `json:"invoiceID"`
	PurchaseOrderID        string `json:"purchaseOrderID"`
	ObjectType             string `json:"docType"`
	Status                 string `json:"status"`
	InvoiceGrossAmount     string `json:"invoiceGrossAmount"`
	DiscountPercent        string `json:"discountPercent"`
	DiscountAmount         string `json:"discountAmount"`
	FundedAmount           string `json:"fundedAmount"`
	OfferValidityStartDate string `json:"offerValidityStartDate"`
	OfferValidityEndDate   string `json:"offerValidityEndDate"`
	DisbursementDate       string `json:"disbursementDate"`
	DisbursementReference  string `json:"disbursementReference"`
	Currency               string `json:"currency"`
}

//FinancingOfferIDIndex - Index on IDs for retrieval all FinancingOffers
type FinancingOfferIDIndex struct {
	IDs []string `json:"IDs"`
}

func main() {
	err := shim.Start(new(FinancingOfferAsset))
	if err != nil {
		fmt.Printf("Error starting FinancingOfferAsset chaincode function main(): %s", err)
	} else {
		fmt.Printf("Starting FinancingOfferAsset chaincode function main() executed successfully")
	}
}

//Init - The chaincode Init function: No  arguments, only initializes a ID array as Index for retrieval of all FinancingOffers
func (finOffer *FinancingOfferAsset) Init(stub shim.ChaincodeStubInterface) peer.Response {
	var financingOfferIDIndex FinancingOfferIDIndex
	record, _ := stub.GetState("financingOfferIDIndex")
	//chaincode upgrade: Index might already exist, create new only if it does not
	if record == nil {
		bytes, _ := json.Marshal(financingOfferIDIndex)
		stub.PutState("financingOfferIDIndex", bytes)
	}
	return shim.Success(nil)
}

//Invoke - The chaincode Invoke function:
func (finOffer *FinancingOfferAsset) Invoke(stub shim.ChaincodeStubInterface) peer.Response {
	function, args := stub.GetFunctionAndParameters()
	logger.Debug("function: ", function)
	switch function {
	case "addNewFinancingOffer":
		return finOffer.addNewFinancingOffer(stub, args)
	case "setFinancingOfferSubmitted":
		return finOffer.setFinancingOfferSubmitted(stub, args)
	case "setFinancingOfferAccepted":
		return finOffer.setFinancingOfferAccepted(stub, args[0])
	case "setFinancingOfferDeclined":
		return finOffer.setFinancingOfferDeclined(stub, args[0])
	case "setFinancingOfferDisbursementInitiated":
		return finOffer.setFinancingOfferDisbursementInitiated(stub, args)
	case "removeFinancingOffer":
		return finOffer.removeFinancingOffer(stub, args[0])
	case "removeAllFinancingOffers":
		return finOffer.removeAllFinancingOffers(stub)
	case "readFinancingOffer":
		return finOffer.readFinancingOffer(stub, args[0])
	case "readAllFinancingOffers":
		return finOffer.readAllFinancingOffers(stub)
	default:
		return shim.Error("Received unknown function invocation")
	}
}

//Invoke Route: addNewFinancingOffer
func (finOffer *FinancingOfferAsset) addNewFinancingOffer(stub shim.ChaincodeStubInterface, args []string) peer.Response {
	financingOffer, err := getFinancingOfferFromArgs(args)
	if err != nil {
		return shim.Error("FinancingOffer Data is Corrupted")
	}
	financingOffer.ObjectType = "Asset.FinancingOffer"
	record, err := stub.GetState(financingOffer.ID)
	if record != nil {
		return shim.Error("This FinancingOffer already exists: " + financingOffer.ID)
	}
	_, err = finOffer.saveFinancingOffer(stub, financingOffer)
	if err != nil {
		return shim.Error(err.Error())
	}
	_, err = finOffer.updateFinancingOfferIDIndex(stub, financingOffer)
	if err != nil {
		return shim.Error(err.Error())
	}
	return shim.Success(nil)
}

//Invoke Route: setFinancingOfferSubmitted
func (finOffer *FinancingOfferAsset) setFinancingOfferSubmitted(stub shim.ChaincodeStubInterface, args []string) peer.Response {

	var financingOffer FinancingOffer

	input, err := getFinancingOfferFromArgs(args)
	if err != nil {
		return shim.Error("FinancingOffer Data is Corrupted")
	}
	_, err = stub.GetState(input.ID)
	if err != nil {
		return shim.Error("Financing Offer with ID: " + input.ID + "not found")
	}

	financingOfferAsByteArray, err := finOffer.retrieveFinancingOffer(stub, input.ID)
	if err != nil {
		return shim.Error(err.Error())
	}
	err = json.Unmarshal(financingOfferAsByteArray, &financingOffer)
	if err != nil {
		return shim.Error(err.Error())
	}

	if financingOffer.Status != Created {
		return shim.Error("FinancingOffer with ID: " + input.ID + " cannot be submitted")
	}
	financingOffer.Status = Submitted
	financingOffer.OfferValidityEndDate = input.OfferValidityEndDate
	financingOffer.OfferValidityStartDate = input.OfferValidityStartDate
	financingOffer.DisbursementDate = input.DisbursementDate
	financingOffer.DisbursementDate = input.DisbursementDate
	_, err = finOffer.saveFinancingOffer(stub, financingOffer)
	if err != nil {
		return shim.Error(err.Error())
	}
	return shim.Success(nil)
}

//Invoke Route: setFinancingOfferAccepted
func (finOffer *FinancingOfferAsset) setFinancingOfferAccepted(stub shim.ChaincodeStubInterface, financingOfferID string) peer.Response {
	var financingOffer FinancingOffer
	_, err := stub.GetState(financingOfferID)
	if err != nil {
		return shim.Error("Financing Offer with ID: " + financingOfferID + "not found")
	}
	financingOfferAsByteArray, err := finOffer.retrieveFinancingOffer(stub, financingOfferID)
	if err != nil {
		return shim.Error(err.Error())
	}
	err = json.Unmarshal(financingOfferAsByteArray, &financingOffer)
	if err != nil {
		return shim.Error(err.Error())
	}
	if financingOffer.Status != Submitted {
		return shim.Error("FinancingOffer with ID: " + financingOfferID + " cannot be accepted")
	}
	financingOffer.Status = Accepted
	_, err = finOffer.saveFinancingOffer(stub, financingOffer)
	if err != nil {
		return shim.Error(err.Error())
	}
	return shim.Success(nil)
}

//Invoke Route: setFinancingOfferDeclined
func (finOffer *FinancingOfferAsset) setFinancingOfferDeclined(stub shim.ChaincodeStubInterface, financingOfferID string) peer.Response {
	var financingOffer FinancingOffer
	_, err := stub.GetState(financingOfferID)
	if err != nil {
		return shim.Error("Financing Offer with ID: " + financingOfferID + "not found")
	}
	financingOfferAsByteArray, err := finOffer.retrieveFinancingOffer(stub, financingOfferID)
	if err != nil {
		return shim.Error(err.Error())
	}
	err = json.Unmarshal(financingOfferAsByteArray, &financingOffer)
	if err != nil {
		return shim.Error(err.Error())
	}
	if financingOffer.Status != Submitted {
		return shim.Error("FinancingOffer with ID: " + financingOfferID + " cannot be declined")
	}
	financingOffer.Status = Declined
	_, err = finOffer.saveFinancingOffer(stub, financingOffer)
	if err != nil {
		return shim.Error(err.Error())
	}
	return shim.Success(nil)
}

//Invoke Route: setFinancingOfferDisbursementInitiated
func (finOffer *FinancingOfferAsset) setFinancingOfferDisbursementInitiated(stub shim.ChaincodeStubInterface, args []string) peer.Response {

	var financingOffer FinancingOffer

	input, err := getFinancingOfferFromArgs(args)
	if err != nil {
		return shim.Error("FinancingOffer Data is Corrupted")
	}
	_, err = stub.GetState(input.ID)
	if err != nil {
		return shim.Error("Financing Offer with ID: " + input.ID + "not found")
	}

	financingOfferAsByteArray, err := finOffer.retrieveFinancingOffer(stub, input.ID)
	if err != nil {
		return shim.Error(err.Error())
	}
	err = json.Unmarshal(financingOfferAsByteArray, &financingOffer)
	if err != nil {
		return shim.Error(err.Error())
	}
	if financingOffer.Status != Accepted {
		return shim.Error("FinancingOffer with ID: " + input.ID + " cannot be disbursement initiated")
	}
	financingOffer.Status = DisbursementInitiated
	financingOffer.DisbursementDate = input.DisbursementDate
	financingOffer.DisbursementReference = input.DisbursementReference
	_, err = finOffer.saveFinancingOffer(stub, financingOffer)
	if err != nil {
		return shim.Error(err.Error())
	}
	return shim.Success(nil)
}

//Invoke Route: removeFinancingOffer
func (finOffer *FinancingOfferAsset) removeFinancingOffer(stub shim.ChaincodeStubInterface, financingOfferID string) peer.Response {
	_, err := finOffer.deleteFinancingOffer(stub, financingOfferID)
	if err != nil {
		return shim.Error(err.Error())
	}
	_, err = finOffer.deleteFinancingOfferIDIndex(stub, financingOfferID)
	if err != nil {
		return shim.Error(err.Error())
	}
	return shim.Success(nil)
}

//Invoke Route: removeAllFinancingOffers
func (finOffer *FinancingOfferAsset) removeAllFinancingOffers(stub shim.ChaincodeStubInterface) peer.Response {
	var financingOfferStructIDs FinancingOfferIDIndex
	bytes, err := stub.GetState("financingOfferIDIndex")
	if err != nil {
		return shim.Error("removeAllFinancingOffers: Error getting financingOfferIDIndex array")
	}
	err = json.Unmarshal(bytes, &financingOfferStructIDs)
	if err != nil {
		return shim.Error("removeAllFinancingOffers: Error unmarshalling financingOfferIDIndex array JSON")
	}
	if len(financingOfferStructIDs.IDs) == 0 {
		return shim.Error("removeAllFinancingOffers: No financingOffers to remove")
	}
	for _, financingOfferStructParticipantID := range financingOfferStructIDs.IDs {
		_, err = finOffer.deleteFinancingOffer(stub, financingOfferStructParticipantID)
		if err != nil {
			return shim.Error("Failed to remove FinancingOffer with ID: " + financingOfferStructParticipantID)
		}
		_, err = finOffer.deleteFinancingOfferIDIndex(stub, financingOfferStructParticipantID)
		if err != nil {
			return shim.Error(err.Error())
		}
	}
	finOffer.initHolder(stub)
	return shim.Success(nil)
}

//Query Route: readFinancingOffer
func (finOffer *FinancingOfferAsset) readFinancingOffer(stub shim.ChaincodeStubInterface, financingOfferID string) peer.Response {
	financingOfferAsByteArray, err := finOffer.retrieveFinancingOffer(stub, financingOfferID)
	if err != nil {
		return shim.Error(err.Error())
	}
	return shim.Success(financingOfferAsByteArray)
}

//Query Route: readAllFinancingOffers
func (finOffer *FinancingOfferAsset) readAllFinancingOffers(stub shim.ChaincodeStubInterface) peer.Response {
	var financingOfferIDs FinancingOfferIDIndex
	bytes, err := stub.GetState("financingOfferIDIndex")
	if err != nil {
		return shim.Error("readAllFinancingOffers: Error getting financingOfferIDIndex array")
	}
	err = json.Unmarshal(bytes, &financingOfferIDs)
	if err != nil {
		return shim.Error("readAllFinancingOffers: Error unmarshalling financingOfferIDIndex array JSON")
	}
	result := "["

	var financingOfferAsByteArray []byte

	for _, financingOfferID := range financingOfferIDs.IDs {
		financingOfferAsByteArray, err = finOffer.retrieveFinancingOffer(stub, financingOfferID)
		if err != nil {
			return shim.Error("Failed to retrieve financingOffer with ID: " + financingOfferID)
		}
		result += string(financingOfferAsByteArray) + ","
	}
	if len(result) == 1 {
		result = "[]"
	} else {
		result = result[:len(result)-1] + "]"
	}
	return shim.Success([]byte(result))
}

//Helper: Save FinancingOffer
func (finOffer *FinancingOfferAsset) saveFinancingOffer(stub shim.ChaincodeStubInterface, financingOffer FinancingOffer) (bool, error) {
	bytes, err := json.Marshal(financingOffer)
	if err != nil {
		return false, errors.New("Error converting financingOffer record JSON")
	}
	err = stub.PutState(financingOffer.ID, bytes)
	if err != nil {
		return false, errors.New("Error storing FinancingOffer record")
	}
	return true, nil
}

//Helper: FinancingOffer financingOfferStruct //change template
func (financingOffer *FinancingOfferAsset) deleteFinancingOffer(stub shim.ChaincodeStubInterface, financingOfferID string) (bool, error) {
	_, err := financingOffer.retrieveFinancingOffer(stub, financingOfferID)
	if err != nil {
		return false, errors.New("FinancingOffer with ID: " + financingOfferID + " not found")
	}
	err = stub.DelState(financingOfferID)
	if err != nil {
		return false, errors.New("Error deleting FinancingOffer record")
	}
	return true, nil
}

//Helper: Update financingOffer Holder - updates Index
func (finOffer *FinancingOfferAsset) updateFinancingOfferIDIndex(stub shim.ChaincodeStubInterface, financingOffer FinancingOffer) (bool, error) {
	var financingOfferIDs FinancingOfferIDIndex
	bytes, err := stub.GetState("financingOfferIDIndex")
	if err != nil {
		return false, errors.New("updateFinancingOfferIDIndex: Error getting financingOfferIDIndex array Index from state")
	}
	err = json.Unmarshal(bytes, &financingOfferIDs)
	if err != nil {
		return false, errors.New("updateFinancingOfferIDIndex: Error unmarshalling financingOfferIDIndex array JSON")
	}
	financingOfferIDs.IDs = append(financingOfferIDs.IDs, financingOffer.ID)
	bytes, err = json.Marshal(financingOfferIDs)
	if err != nil {
		return false, errors.New("updateFinancingOfferIDIndex: Error marshalling new financingOffer ID")
	}
	err = stub.PutState("financingOfferIDIndex", bytes)
	if err != nil {
		return false, errors.New("updateFinancingOfferIDIndex: Error storing new financingOffer ID in financingOfferIDIndex (Index)")
	}
	return true, nil
}

//Helper: delete ID from financingOfferStruct Holder
func (finOffer *FinancingOfferAsset) deleteFinancingOfferIDIndex(stub shim.ChaincodeStubInterface, financingOfferID string) (bool, error) {
	var financingOfferStructIDs FinancingOfferIDIndex
	bytes, err := stub.GetState("financingOfferIDIndex")
	if err != nil {
		return false, errors.New("deleteFinancingOfferIDIndex: Error getting financingOfferIDIndex array Index from state")
	}
	err = json.Unmarshal(bytes, &financingOfferStructIDs)
	if err != nil {
		return false, errors.New("deleteFinancingOfferIDIndex: Error unmarshalling financingOfferIDIndex array JSON")
	}
	financingOfferStructIDs.IDs, err = deleteKeyFromStringArray(financingOfferStructIDs.IDs, financingOfferID)
	if err != nil {
		return false, errors.New(err.Error())
	}
	bytes, err = json.Marshal(financingOfferStructIDs)
	if err != nil {
		return false, errors.New("deleteFinancingOfferIDIndex: Error marshalling new financingOfferStruct ID")
	}
	err = stub.PutState("financingOfferIDIndex", bytes)
	if err != nil {
		return false, errors.New("deleteFinancingOfferIDIndex: Error storing new financingOfferStruct ID in financingOfferIDIndex (Index)")
	}
	return true, nil
}

//Helper: Initialize truck ID Holder //change template
func (finOffer *FinancingOfferAsset) initHolder(stub shim.ChaincodeStubInterface) bool {
	var financingOfferIDIndex FinancingOfferIDIndex
	bytes, _ := json.Marshal(financingOfferIDIndex)
	stub.DelState("financingOfferIDIndex")
	stub.PutState("financingOfferIDIndex", bytes)
	return true
}

//deleteKeyFromArray
func deleteKeyFromStringArray(array []string, key string) (newArray []string, err error) {
	for _, entry := range array {
		if entry != key {
			newArray = append(newArray, entry)
		}
	}
	if len(newArray) == len(array) {
		return newArray, errors.New("Specified Key: " + key + " not found in Array")
	}
	return newArray, nil
}

//Helper: Retrieve purchaser
func (finOffer *FinancingOfferAsset) retrieveFinancingOffer(stub shim.ChaincodeStubInterface, financingOfferID string) ([]byte, error) {
	var financingOffer FinancingOffer
	var financingOfferAsByteArray []byte
	bytes, err := stub.GetState(financingOfferID)
	if err != nil {
		return financingOfferAsByteArray, errors.New("retrieveFinancingOffer: Error retrieving financingOffer with ID: " + financingOfferID)
	}
	err = json.Unmarshal(bytes, &financingOffer)
	if err != nil {
		return financingOfferAsByteArray, errors.New("retrieveFinancingOffer: Corrupt financingOffer record " + string(bytes))
	}
	financingOfferAsByteArray, err = json.Marshal(financingOffer)
	if err != nil {
		return financingOfferAsByteArray, errors.New("readFinancingOffer: Invalid financingOffer Object - Not a  valid JSON")
	}
	return financingOfferAsByteArray, nil
}

//getFinancingOfferFromArgs - construct a financingOffer structure from string array of arguments
func getFinancingOfferFromArgs(args []string) (sample FinancingOffer, err error) {

	if strings.Contains(args[0], "\"ID\"") == false ||
		strings.Contains(args[0], "\"supplierID\"") == false ||
		strings.Contains(args[0], "\"purchaserID\"") == false ||
		strings.Contains(args[0], "\"financerID\"") == false ||
		strings.Contains(args[0], "\"invoiceID\"") == false ||
		strings.Contains(args[0], "\"purchaseOrderID\"") == false ||
		strings.Contains(args[0], "\"docType\"") == false ||
		strings.Contains(args[0], "\"status\"") == false ||
		strings.Contains(args[0], "\"invoiceGrossAmount\"") == false ||
		strings.Contains(args[0], "\"discountPercent\"") == false ||
		strings.Contains(args[0], "\"discountAmount\"") == false ||
		strings.Contains(args[0], "\"fundedAmount\"") == false ||
		strings.Contains(args[0], "\"offerValidityStartDate\"") == false ||
		strings.Contains(args[0], "\"offerValidityEndDate\"") == false ||
		strings.Contains(args[0], "\"disbursementDate\"") == false ||
		strings.Contains(args[0], "\"disbursementReference\"") == false ||
		strings.Contains(args[0], "\"currency\"") == false {
		return sample, errors.New("Unknown field: " + "Input JSON does not comply to schema")
	}
	err = json.Unmarshal([]byte(args[0]), &sample)
	if err != nil {
		return sample, err
	}
	return sample, nil
}
