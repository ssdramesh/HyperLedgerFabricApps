package main

import (
	"bytes"
	"encoding/json"
	"fmt"
	"strings"
	"testing"

	"github.com/hyperledger/fabric/core/chaincode/shim"
)

//TestFinancingRequestAsset_Init
func TestFinancingRequestAsset_Init(t *testing.T) {
	financingRequest := new(FinancingRequestAsset)
	stub := shim.NewMockStub("financingRequest", financingRequest)
	checkInit(t, stub, [][]byte{[]byte("init")})
	checkState(t, stub, "financingRequestIDIndex", getExpectedFinancingRequestIDIndex("init"))
}

//TestFinancingRequestAsset_InvokeUnknownFunction
func TestFinancingRequestAsset_InvokeUnknownFunction(t *testing.T) {
	financingRequest := new(FinancingRequestAsset)
	stub := shim.NewMockStub("financingRequest", financingRequest)
	checkInit(t, stub, [][]byte{[]byte("init")})
	checkInvokeUnknownFunction(t, stub, [][]byte{[]byte("myFunction"), []byte("docType:Asset")})
}

//TestFinancingRequestAsset_Invoke_addNewFinancingRequest
func TestFinancingRequestAsset_Invoke_addNewFinancingRequestOK(t *testing.T) {
	financingRequest := new(FinancingRequestAsset)
	stub := shim.NewMockStub("financingRequest", financingRequest)
	checkInit(t, stub, [][]byte{[]byte("init")})
	checkInvoke(t, stub, getFirstFinancingRequestAssetForTesting())
	newFinancingRequestID := "FR100001"
	checkState(t, stub, newFinancingRequestID, getNewFinancingRequestExpected())
	checkState(t, stub, "financingRequestIDIndex", getExpectedFinancingRequestIDIndex("addNewFinancingRequest"))
}

//TestFinancingRequestAsset_Invoke_addNewFinancingRequestUnknownField
func TestFinancingRequestAsset_Invoke_addNewFinancingRequestUnknownField(t *testing.T) {
	financingRequest := new(FinancingRequestAsset)
	stub := shim.NewMockStub("financingRequest", financingRequest)
	checkInit(t, stub, [][]byte{[]byte("init")})
	res := stub.MockInvoke("1", getFinancingRequestAssetWithUnknownFieldForTesting())
	if res.Status != shim.OK {
		checkError(t, "FinancingRequest Data is Corrupted", res.Message)
	} else {
		fmt.Println("Unknown Field Error was expected, but not raised")
		t.FailNow()
	}
}

//TestFinancingRequestAsset_Invoke_addNewFinancingRequest
func TestFinancingRequestAsset_Invoke_addNewFinancingRequestDuplicate(t *testing.T) {
	financingRequest := new(FinancingRequestAsset)
	stub := shim.NewMockStub("financingRequest", financingRequest)
	checkInit(t, stub, [][]byte{[]byte("init")})
	checkInvoke(t, stub, getFirstFinancingRequestAssetForTesting())
	newFinancingRequestID := "FR100001"
	checkState(t, stub, newFinancingRequestID, getNewFinancingRequestExpected())
	checkState(t, stub, "financingRequestIDIndex", getExpectedFinancingRequestIDIndex("addNewFinancingRequest"))
	res := stub.MockInvoke("1", getFirstFinancingRequestAssetForTesting())
	if res.Status != shim.OK {
		checkError(t, "This FinancingRequest already exists: FR100001", res.Message)
	} else {
		fmt.Println("Error was expected, but not raised")
		t.FailNow()
	}
}

//TestFinancingRequestAsset_Invoke_setFinancingRequestInProcessOK
func TestFinancingRequestAsset_Invoke_setFinancingInProcessOK(t *testing.T) {
	financingRequest := new(FinancingRequestAsset)
	stub := shim.NewMockStub("financingRequest", financingRequest)
	checkInit(t, stub, [][]byte{[]byte("init")})
	checkInvoke(t, stub, getFirstFinancingRequestAssetForTesting())
	financingRequestID := "FR100001"
	res := stub.MockInvoke("1", getInProcessData(financingRequestID))
	if res.Status != shim.OK {
		checkError(t, "FinancingRequest with ID: "+financingRequestID+" cannot be set in process", res.Message)
	}
	res = stub.MockInvoke("1", [][]byte{[]byte("readFinancingRequest"), []byte(financingRequestID)})
	if res.Status != shim.OK {
		fmt.Println("func readFinancingRequest with ID: ", financingRequestID, " failed"+string(res.Message))
		t.FailNow()
	}
	if res.Payload == nil {
		fmt.Println("func readFinancingRequest with ID: ", financingRequestID, "failed to get value")
		t.FailNow()
	} else {
		var resFinReq FinancingRequest
		err := json.Unmarshal(res.Payload, &resFinReq)
		if err != nil {
			fmt.Println(err.Error())
			t.FailNow()
		}
		checkError(t, InProcess, resFinReq.Status)
		checkError(t, "02/01/2017", resFinReq.RequestedDisbursementDate)
		checkError(t, "1.75", resFinReq.RequestedDiscountPercent)
		checkError(t, "98.25", resFinReq.RequestedFinancingAmount)
	}
}

//TestFinancingRequestAsset_Invoke_setFinancingRequestInProcessNOK
func TestFinancingRequestAsset_Invoke_setFinancingRequestInProcessNOK(t *testing.T) {
	financingRequest := new(FinancingRequestAsset)
	stub := shim.NewMockStub("financingRequest", financingRequest)
	checkInit(t, stub, [][]byte{[]byte("init")})
	checkInvoke(t, stub, getFinancingRequestForSetInProcessNOKTest())
	financingRequestID := "FR100001"
	res := stub.MockInvoke("1", getInProcessData(financingRequestID))
	if res.Status != shim.OK {
		checkError(t, "FinancingRequest with ID: "+financingRequestID+" cannot be set in process", res.Message)
	}
	res = stub.MockInvoke("1", [][]byte{[]byte("readFinancingRequest"), []byte(financingRequestID)})
	if res.Status != shim.OK {
		fmt.Println("func readFinancingRequest with ID: ", financingRequestID, " failed"+string(res.Message))
		t.FailNow()
	}
	if res.Payload == nil {
		fmt.Println("func readFinancingRequest with ID: ", financingRequestID, "failed to get value")
		t.FailNow()
	} else {
		var resFinReq FinancingRequest
		err := json.Unmarshal(res.Payload, &resFinReq)
		if err != nil {
			fmt.Println(err.Error())
			t.FailNow()
		}
		checkError(t, Accepted, resFinReq.Status)
		checkError(t, "01/01/2017", resFinReq.RequestedDisbursementDate)
		checkError(t, "2.00", resFinReq.RequestedDiscountPercent)
		checkError(t, "98.00", resFinReq.RequestedFinancingAmount)
	}
}

//TestFinancingRequestAsset_Invoke_setFinancingRequestAcceptedOK
func TestFinancingRequestAsset_Invoke_setFinancingRequestAcceptedOK(t *testing.T) {
	financingRequest := new(FinancingRequestAsset)
	stub := shim.NewMockStub("financingRequest", financingRequest)
	checkInit(t, stub, [][]byte{[]byte("init")})
	checkInvoke(t, stub, getFirstFinancingRequestAssetForSetFinancingRequestAcceptedOK())
	financingRequestID := "FR100001"
	res := stub.MockInvoke("1", getAcceptData(financingRequestID))
	if res.Status != shim.OK {
		checkError(t, "FinancingRequest with ID: "+financingRequestID+" cannot be accepted", res.Message)
	}
	res = stub.MockInvoke("1", [][]byte{[]byte("readFinancingRequest"), []byte(financingRequestID)})
	if res.Status != shim.OK {
		fmt.Println("func readFinancingRequest with ID: ", financingRequestID, " failed"+string(res.Message))
		t.FailNow()
	}
	if res.Payload == nil {
		fmt.Println("func readFinancingRequest with ID: ", financingRequestID, "failed to get value")
		t.FailNow()
	} else {
		var resFinReq FinancingRequest
		err := json.Unmarshal(res.Payload, &resFinReq)
		if err != nil {
			fmt.Println(err.Error())
			t.FailNow()
		}
		checkError(t, Accepted, resFinReq.Status)
	}
}

//TestFinancingRequestAsset_Invoke_setFinancingRequestAcceptedNOK
func TestFinancingRequestAsset_Invoke_setFinancingRequestAccpetedNOK(t *testing.T) {
	financingRequest := new(FinancingRequestAsset)
	stub := shim.NewMockStub("financingRequest", financingRequest)
	checkInit(t, stub, [][]byte{[]byte("init")})
	checkInvoke(t, stub, getFinancingRequestForSetFinancingRequestAcceptedNOK())
	financingRequestID := "FR100001"
	res := stub.MockInvoke("1", getAcceptData(financingRequestID))
	if res.Status != shim.OK {
		checkError(t, "FinancingRequest with ID: "+financingRequestID+" cannot be accepted", res.Message)
	}
	res = stub.MockInvoke("1", [][]byte{[]byte("readFinancingRequest"), []byte(financingRequestID)})
	if res.Status != shim.OK {
		fmt.Println("func readFinancingRequest with ID: ", financingRequestID, " failed"+string(res.Message))
		t.FailNow()
	}
	if res.Payload == nil {
		fmt.Println("func readInvoice with ID: ", financingRequestID, "failed to get value")
		t.FailNow()
	} else {
		var resFinReq FinancingRequest
		err := json.Unmarshal(res.Payload, &resFinReq)
		if err != nil {
			fmt.Println(err.Error())
			t.FailNow()
		}
		checkError(t, Declined, resFinReq.Status)
	}
}

//TestFinancingRequestAsset_Invoke_setFinancingRequestDeclinedOK
func TestFinancingRequestAsset_Invoke_setFinancingRequestDeclinedOK(t *testing.T) {
	financingRequest := new(FinancingRequestAsset)
	stub := shim.NewMockStub("financingRequest", financingRequest)
	checkInit(t, stub, [][]byte{[]byte("init")})
	checkInvoke(t, stub, getFirstFinancingRequestAssetForTestingDeclined())
	financingRequestID := "FR100001"
	res := stub.MockInvoke("1", [][]byte{[]byte("setFinancingRequestDeclined"), []byte(financingRequestID)})
	if res.Status != shim.OK {
		checkError(t, "FinancingRequest with ID: "+financingRequestID+" cannot be declined", res.Message)
	}
	res = stub.MockInvoke("1", [][]byte{[]byte("readFinancingRequest"), []byte(financingRequestID)})
	if res.Status != shim.OK {
		fmt.Println("func readFinancingRequest with ID: ", financingRequestID, " failed"+string(res.Message))
		t.FailNow()
	}
	if res.Payload == nil {
		fmt.Println("func readFinancingRequest with ID: ", financingRequestID, "failed to get value")
		t.FailNow()
	} else {
		var resFinReq FinancingRequest
		err := json.Unmarshal(res.Payload, &resFinReq)
		if err != nil {
			fmt.Println(err.Error())
			t.FailNow()
		}
		checkError(t, Declined, resFinReq.Status)
	}
}

//TestFinancingRequestAsset_Invoke_setFinancingRequestDeclinedNOK
func TestFinancingRequestAsset_Invoke_setFinancingRequestDeclinedNOK(t *testing.T) {
	financingRequest := new(FinancingRequestAsset)
	stub := shim.NewMockStub("financingRequest", financingRequest)
	checkInit(t, stub, [][]byte{[]byte("init")})
	checkInvoke(t, stub, getFinancingRequestForSetFinancingRequestDeclinedNOK())
	financingRequestID := "FR100001"
	res := stub.MockInvoke("1", [][]byte{[]byte("setFinancingRequestDeclined"), []byte(financingRequestID)})
	if res.Status != shim.OK {
		checkError(t, "FinancingRequest with ID: "+financingRequestID+" cannot be declined", res.Message)
	}
	res = stub.MockInvoke("1", [][]byte{[]byte("readFinancingRequest"), []byte(financingRequestID)})
	if res.Status != shim.OK {
		fmt.Println("func readFinancingRequest with ID: ", financingRequestID, " failed"+string(res.Message))
		t.FailNow()
	}
	if res.Payload == nil {
		fmt.Println("func readInvoice with ID: ", financingRequestID, "failed to get value")
		t.FailNow()
	} else {
		var resFinReq FinancingRequest
		err := json.Unmarshal(res.Payload, &resFinReq)
		if err != nil {
			fmt.Println(err.Error())
			t.FailNow()
		}
		checkError(t, Accepted, resFinReq.Status)
	}
}

//TestFinancingRequestAsset_Invoke_setFinancingRequestDocumentsCheckedOK
func TestFinancingRequestAsset_Invoke_setFinancingRequestDocumentsCheckedOK(t *testing.T) {
	financingRequest := new(FinancingRequestAsset)
	stub := shim.NewMockStub("financingRequest", financingRequest)
	checkInit(t, stub, [][]byte{[]byte("init")})
	checkInvoke(t, stub, getFirstFinancingRequestAssetForTesting())
	financingRequestID := "FR100001"
	res := stub.MockInvoke("1", [][]byte{[]byte("setFinancingRequestDocumentsChecked"), []byte(financingRequestID)})
	if res.Status != shim.OK {
		checkError(t, "Documents check for FinancingRequest with ID: "+financingRequestID+" already done", res.Message)
	}
	res = stub.MockInvoke("1", [][]byte{[]byte("readFinancingRequest"), []byte(financingRequestID)})
	if res.Status != shim.OK {
		fmt.Println("func readFinancingRequest with ID: ", financingRequestID, " failed"+string(res.Message))
		t.FailNow()
	}
	if res.Payload == nil {
		fmt.Println("func readFinancingRequest with ID: ", financingRequestID, "failed to get value")
		t.FailNow()
	} else {
		var resFinReq FinancingRequest
		err := json.Unmarshal(res.Payload, &resFinReq)
		if err != nil {
			fmt.Println(err.Error())
			t.FailNow()
		}
		checkError(t, "true", resFinReq.DocumentsChecked)
	}
}

//TestFinancingRequestAsset_Invoke_setFinancingRequestDocumentsCheckedNOK
func TestFinancingRequestAsset_Invoke_setFinancingRequestDocumentsCheckedNOK(t *testing.T) {
	financingRequest := new(FinancingRequestAsset)
	stub := shim.NewMockStub("financingRequest", financingRequest)
	checkInit(t, stub, [][]byte{[]byte("init")})
	checkInvoke(t, stub, getFinancingRequestForSetFinancingRequestDocumentsCheckedNOK())
	financingRequestID := "FR100001"
	res := stub.MockInvoke("1", [][]byte{[]byte("setFinancingRequestDocumentsChecked"), []byte(financingRequestID)})
	if res.Status != shim.OK {
		checkError(t, "Documents check for FinancingRequest with ID: "+financingRequestID+" already done", res.Message)
	}
	res = stub.MockInvoke("1", [][]byte{[]byte("readFinancingRequest"), []byte(financingRequestID)})
	if res.Status != shim.OK {
		fmt.Println("func readInvoice with ID: ", financingRequestID, " failed"+string(res.Message))
		t.FailNow()
	}
	if res.Payload == nil {
		fmt.Println("func readInvoice with ID: ", financingRequestID, "failed to get value")
		t.FailNow()
	} else {
		var resFinReq FinancingRequest
		err := json.Unmarshal(res.Payload, &resFinReq)
		if err != nil {
			fmt.Println(err.Error())
			t.FailNow()
		}
		checkError(t, "true", resFinReq.DocumentsChecked)
	}
}

//TestFinancingRequestAsset_Invoke_setFinancingRequestDocumentsVerifiedOK
func TestFinancingRequestAsset_Invoke_setFinancingRequestDocumentsVerifiedOK(t *testing.T) {
	financingRequest := new(FinancingRequestAsset)
	stub := shim.NewMockStub("financingRequest", financingRequest)
	checkInit(t, stub, [][]byte{[]byte("init")})
	checkInvoke(t, stub, getFirstFinancingRequestAssetForTesting())
	financingRequestID := "FR100001"
	res := stub.MockInvoke("1", [][]byte{[]byte("setFinancingRequestDocumentsVerified"), []byte(financingRequestID)})
	if res.Status != shim.OK {
		checkError(t, "Documents verification for FinancingRequest with ID: "+financingRequestID+" already done", res.Message)
	}
	res = stub.MockInvoke("1", [][]byte{[]byte("readFinancingRequest"), []byte(financingRequestID)})
	if res.Status != shim.OK {
		fmt.Println("func readFinancingRequest with ID: ", financingRequestID, " failed"+string(res.Message))
		t.FailNow()
	}
	if res.Payload == nil {
		fmt.Println("func readFinancingRequest with ID: ", financingRequestID, "failed to get value")
		t.FailNow()
	} else {
		var resFinReq FinancingRequest
		err := json.Unmarshal(res.Payload, &resFinReq)
		if err != nil {
			fmt.Println(err.Error())
			t.FailNow()
		}
		checkError(t, "true", resFinReq.DocumentsVerified)
	}
}

//TestFinancingRequestAsset_Invoke_setFinancingRequestDocumentsVerifiedNOK
func TestFinancingRequestAsset_Invoke_setFinancingRequestDocumentsVerifiedNOK(t *testing.T) {
	financingRequest := new(FinancingRequestAsset)
	stub := shim.NewMockStub("financingRequest", financingRequest)
	checkInit(t, stub, [][]byte{[]byte("init")})
	checkInvoke(t, stub, getFinancingRequestForSetFinancingRequestDocumentsVerifiedNOK())
	financingRequestID := "FR100001"
	res := stub.MockInvoke("1", [][]byte{[]byte("setFinancingRequestDocumentsVerified"), []byte(financingRequestID)})
	if res.Status != shim.OK {
		checkError(t, "Documents verification for FinancingRequest with ID: "+financingRequestID+" already done", res.Message)
	}
	res = stub.MockInvoke("1", [][]byte{[]byte("readFinancingRequest"), []byte(financingRequestID)})
	if res.Status != shim.OK {
		fmt.Println("func readInvoice with ID: ", financingRequestID, " failed"+string(res.Message))
		t.FailNow()
	}
	if res.Payload == nil {
		fmt.Println("func readInvoice with ID: ", financingRequestID, "failed to get value")
		t.FailNow()
	} else {
		var resFinReq FinancingRequest
		err := json.Unmarshal(res.Payload, &resFinReq)
		if err != nil {
			fmt.Println(err.Error())
			t.FailNow()
		}
		checkError(t, "true", resFinReq.DocumentsVerified)
	}
}

//TestFinancingRequestAsset_Invoke_removeFinancingRequestOK  //change template
func TestFinancingRequestAsset_Invoke_removeFinancingRequestOK(t *testing.T) {
	financingRequest := new(FinancingRequestAsset)
	stub := shim.NewMockStub("financingRequest", financingRequest)
	checkInit(t, stub, [][]byte{[]byte("init")})
	checkInvoke(t, stub, getFirstFinancingRequestAssetForTesting())
	checkInvoke(t, stub, getSecondFinancingRequestAssetForTesting())
	checkReadAllFinancingRequestsOK(t, stub)
	checkState(t, stub, "financingRequestIDIndex", getExpectedFinancingRequestIDIndex("beforeRemoveFinancingRequest"))
	checkInvoke(t, stub, getRemoveSecondFinancingRequestAssetForTesting())
	remainingFinancingRequestID := "FR100001"
	checkReadFinancingRequestOK(t, stub, remainingFinancingRequestID)
	checkState(t, stub, "financingRequestIDIndex", getExpectedFinancingRequestIDIndex("afterRemoveFinancingRequest"))
}

//TestFinancingRequestAsset_Invoke_removeAllFinancingRequestssOK
func TestFinancingRequestAsset_Invoke_removeAllFinancingRequestssOK(t *testing.T) {
	financier := new(FinancingRequestAsset)
	stub := shim.NewMockStub("financier", financier)
	checkInit(t, stub, [][]byte{[]byte("init")})
	checkInvoke(t, stub, getFirstFinancingRequestAssetForTesting())
	checkInvoke(t, stub, getSecondFinancingRequestAssetForTesting())
	checkReadAllFinancingRequestsOK(t, stub)
	checkState(t, stub, "financingRequestIDIndex", getExpectedFinancingRequestIDIndex("beforeRemoveFinancingRequest"))
	checkInvoke(t, stub, getRemoveAllFinancingRequestAssetsForTesting())
	checkState(t, stub, "financingRequestIDIndex", getExpectedFinancingRequestIDIndex(""))
}

//TestFinancingRequestAsset_Invoke_removeFinancingRequestNOK  //change template
func TestFinancingRequestAsset_Invoke_removeFinancingRequestNOK(t *testing.T) {
	financingRequest := new(FinancingRequestAsset)
	stub := shim.NewMockStub("financingRequest", financingRequest)
	checkInit(t, stub, [][]byte{[]byte("init")})
	checkInvoke(t, stub, getFirstFinancingRequestAssetForTesting())
	firstFinancingRequestID := "FR100001"
	checkReadFinancingRequestOK(t, stub, firstFinancingRequestID)
	checkState(t, stub, "financingRequestIDIndex", getExpectedFinancingRequestIDIndex("addNewFinancingRequest"))
	res := stub.MockInvoke("1", getRemoveSecondFinancingRequestAssetForTesting())
	if res.Status != shim.OK {
		checkError(t, "FinancingRequest with ID: "+"FR100002"+" not found", res.Message)
	} else {
		fmt.Println("Error was expected, but not raised")
		t.FailNow()
	}
	checkState(t, stub, "financingRequestIDIndex", getExpectedFinancingRequestIDIndex("addNewFinancingRequest"))
}

//TestFinancingRequestAsset_Query_readFinancingRequest
func TestFinancingRequestAsset_Query_readFinancingRequest(t *testing.T) {
	financingRequest := new(FinancingRequestAsset)
	stub := shim.NewMockStub("financingRequest", financingRequest)
	checkInit(t, stub, [][]byte{[]byte("init")})
	financingRequestID := "FR100001"
	checkInvoke(t, stub, getFirstFinancingRequestAssetForTesting())
	checkReadFinancingRequestOK(t, stub, financingRequestID)
	checkReadFinancingRequestNOK(t, stub, "")
}

//TestFinancingRequestAsset_Query_readAllFinancingRequests
func TestFinancingRequestAsset_Query_readAllFinancingRequests(t *testing.T) {
	financingRequest := new(FinancingRequestAsset)
	stub := shim.NewMockStub("financingRequest", financingRequest)
	checkInit(t, stub, [][]byte{[]byte("init")})
	checkInvoke(t, stub, getFirstFinancingRequestAssetForTesting())
	checkInvoke(t, stub, getSecondFinancingRequestAssetForTesting())
	checkReadAllFinancingRequestsOK(t, stub)
}

/*
*
*	Helper Functions
*
 */
//Get first FinancingRequestAsset for testing
func getFirstFinancingRequestAssetForTesting() [][]byte {
	return [][]byte{[]byte("addNewFinancingRequest"),
		[]byte("{\"ID\":\"FR100001\"," +
			"\"supplierID\":\"SU100001\"," +
			"\"purchaserID\":\"PU100001\"," +
			"\"financerID\":\"FI100001\"," +
			"\"invoiceID\":\"IN100001\"," +
			"\"purchaseOrderID\":\"PO100001\"," +
			"\"docType\":\"Asset.FinancingRequest\"," +
			"\"status\":\"Created\"," +
			"\"invoiceGrossAmount\":\"100.00\"," +
			"\"invoiceDaysToCash\":\"15\"," +
			"\"invoicePaymentDate\":\"12/01/2017\"," +
			"\"requestCreationDate\":\"01/01/2017\"," +
			"\"requestedDisbursementDate\":\"01/01/2017\"," +
			"\"requestedDiscountPercent\":\"2.00\"," +
			"\"requestedFinancingAmount\":\"98.00\"," +
			"\"documentsChecked\":\"false\"," +
			"\"documentsVerified\":\"false\"," +
			"\"currency\":\"SGD\"}")}
}

//Get FinancingRequestAsset with unknown field for testing
func getFinancingRequestAssetWithUnknownFieldForTesting() [][]byte {
	return [][]byte{[]byte("addNewFinancingRequest"),
		[]byte("{\"ID\":\"FR100001\"," +
			"\"supplierID\":\"SU100001\"," +
			"\"purchaserID\":\"PU100001\"," +
			"\"financerID\":\"FI100001\"," +
			"\"invoiceID\":\"IN100001\"," +
			"\"purchaseOrderID\":\"PO100001\"," +
			"\"docuType\":\"Asset.FinancingRequest\"," +
			"\"status\":\"Created\"," +
			"\"invoiceGrossAmount\":\"100.00\"," +
			"\"invoiceDaysToCash\":\"15\"," +
			"\"invoicePaymentDate\":\"12/01/2017\"," +
			"\"requestCreationDate\":\"01/01/2017\"," +
			"\"requestedDisbursementDate\":\"01/01/2017\"," +
			"\"requestedDiscountPercent\":\"2.00\"," +
			"\"requestedFinancingAmount\":\"98.00\"," +
			"\"documentsChecked\":\"false\"," +
			"\"documentsVerified\":\"false\"," +
			"\"currency\":\"SGD\"}")}
}

//Get second FinancingRequestAsset for testing
func getSecondFinancingRequestAssetForTesting() [][]byte {
	return [][]byte{[]byte("addNewFinancingRequest"),
		[]byte("{\"ID\":\"FR100002\"," +
			"\"supplierID\":\"SU100002\"," +
			"\"purchaserID\":\"PU100002\"," +
			"\"financerID\":\"FI100002\"," +
			"\"invoiceID\":\"IN100002\"," +
			"\"purchaseOrderID\":\"PO100002\"," +
			"\"docType\":\"Asset.FinancingRequest\"," +
			"\"status\":\"Created\"," +
			"\"invoiceGrossAmount\":\"100.00\"," +
			"\"invoiceDaysToCash\":\"15\"," +
			"\"invoicePaymentDate\":\"12/01/2017\"," +
			"\"requestCreationDate\":\"01/01/2017\"," +
			"\"requestedDisbursementDate\":\"01/01/2017\"," +
			"\"requestedDiscountPercent\":\"2.00\"," +
			"\"requestedFinancingAmount\":\"98.00\"," +
			"\"documentsChecked\":\"false\"," +
			"\"documentsVerified\":\"false\"," +
			"\"currency\":\"SGD\"}")}
}

//Get data to be modified when request is set in process
func getInProcessData(financingRequestID string) [][]byte {
	return [][]byte{[]byte("setFinancingRequestInProcess"),
		[]byte("{\"ID\":\"FR100001\"," +
			"\"supplierID\":\"SU100001\"," +
			"\"purchaserID\":\"PU100001\"," +
			"\"financerID\":\"FI100001\"," +
			"\"invoiceID\":\"IN100001\"," +
			"\"purchaseOrderID\":\"PO100001\"," +
			"\"docType\":\"Asset.FinancingRequest\"," +
			"\"status\":\"Created\"," +
			"\"invoiceGrossAmount\":\"100.00\"," +
			"\"invoiceDaysToCash\":\"15\"," +
			"\"invoicePaymentDate\":\"12/01/2017\"," +
			"\"requestCreationDate\":\"01/01/2017\"," +
			"\"requestedDisbursementDate\":\"02/01/2017\"," +
			"\"requestedDiscountPercent\":\"1.75\"," +
			"\"requestedFinancingAmount\":\"98.25\"," +
			"\"documentsChecked\":\"false\"," +
			"\"documentsVerified\":\"false\"," +
			"\"currency\":\"SGD\"}")}

}

//Get data to be modified when request is set to accepted
func getAcceptData(financingRequestID string) [][]byte {
	return [][]byte{[]byte("setFinancingRequestAccepted"),
		[]byte("{\"ID\":\"FR100001\"," +
			"\"supplierID\":\"SU100001\"," +
			"\"purchaserID\":\"PU100001\"," +
			"\"financerID\":\"FI100001\"," +
			"\"invoiceID\":\"IN100001\"," +
			"\"purchaseOrderID\":\"PO100001\"," +
			"\"docType\":\"Asset.FinancingRequest\"," +
			"\"status\":\"Created\"," +
			"\"invoiceGrossAmount\":\"100.00\"," +
			"\"invoiceDaysToCash\":\"15\"," +
			"\"invoicePaymentDate\":\"12/01/2017\"," +
			"\"requestCreationDate\":\"01/01/2017\"," +
			"\"requestedDisbursementDate\":\"02/01/2017\"," +
			"\"requestedDiscountPercent\":\"1.75\"," +
			"\"requestedFinancingAmount\":\"99.98\"," +
			"\"documentsChecked\":\"false\"," +
			"\"documentsVerified\":\"false\"," +
			"\"currency\":\"SGD\"}")}
}

//Get FinancingRequest for SetInProcess NOKTest
func getFinancingRequestForSetInProcessNOKTest() [][]byte {
	return [][]byte{[]byte("addNewFinancingRequest"),
		[]byte("{\"ID\":\"FR100001\"," +
			"\"supplierID\":\"SU100001\"," +
			"\"purchaserID\":\"PU100001\"," +
			"\"financerID\":\"FI100001\"," +
			"\"invoiceID\":\"IN100001\"," +
			"\"purchaseOrderID\":\"PO100001\"," +
			"\"docType\":\"Asset.FinancingRequest\"," +
			"\"status\":\"Accepted\"," +
			"\"invoiceGrossAmount\":\"100.00\"," +
			"\"invoiceDaysToCash\":\"15\"," +
			"\"invoicePaymentDate\":\"12/01/2017\"," +
			"\"requestCreationDate\":\"01/01/2017\"," +
			"\"requestedDisbursementDate\":\"01/01/2017\"," +
			"\"requestedDiscountPercent\":\"2.00\"," +
			"\"requestedFinancingAmount\":\"98.00\"," +
			"\"documentsChecked\":\"false\"," +
			"\"documentsVerified\":\"false\"," +
			"\"currency\":\"SGD\"}")}
}

func getFirstFinancingRequestAssetForSetFinancingRequestAcceptedOK() [][]byte {
	return [][]byte{[]byte("addNewFinancingRequest"),
		[]byte("{\"ID\":\"FR100001\"," +
			"\"supplierID\":\"SU100001\"," +
			"\"purchaserID\":\"PU100001\"," +
			"\"financerID\":\"FI100001\"," +
			"\"invoiceID\":\"IN100001\"," +
			"\"purchaseOrderID\":\"PO100001\"," +
			"\"docType\":\"Asset.FinancingRequest\"," +
			"\"status\":\"In Process\"," +
			"\"invoiceGrossAmount\":\"100.00\"," +
			"\"invoiceDaysToCash\":\"15\"," +
			"\"invoicePaymentDate\":\"12/01/2017\"," +
			"\"requestCreationDate\":\"01/01/2017\"," +
			"\"requestedDisbursementDate\":\"01/01/2017\"," +
			"\"requestedDiscountPercent\":\"2.00\"," +
			"\"requestedFinancingAmount\":\"98.00\"," +
			"\"documentsChecked\":\"false\"," +
			"\"documentsVerified\":\"false\"," +
			"\"currency\":\"SGD\"}")}
}

func getFinancingRequestForSetFinancingRequestAcceptedNOK() [][]byte {
	return [][]byte{[]byte("addNewFinancingRequest"),
		[]byte("{\"ID\":\"FR100001\"," +
			"\"supplierID\":\"SU100001\"," +
			"\"purchaserID\":\"PU100001\"," +
			"\"financerID\":\"FI100001\"," +
			"\"invoiceID\":\"IN100001\"," +
			"\"purchaseOrderID\":\"PO100001\"," +
			"\"docType\":\"Asset.FinancingRequest\"," +
			"\"status\":\"Declined\"," +
			"\"invoiceGrossAmount\":\"100.00\"," +
			"\"invoiceDaysToCash\":\"15\"," +
			"\"invoicePaymentDate\":\"12/01/2017\"," +
			"\"requestCreationDate\":\"01/01/2017\"," +
			"\"requestedDisbursementDate\":\"01/01/2017\"," +
			"\"requestedDiscountPercent\":\"2.00\"," +
			"\"requestedFinancingAmount\":\"98.00\"," +
			"\"documentsChecked\":\"false\"," +
			"\"documentsVerified\":\"false\"," +
			"\"currency\":\"SGD\"}")}
}

func getFirstFinancingRequestAssetForTestingDeclined() [][]byte {
	return [][]byte{[]byte("addNewFinancingRequest"),
		[]byte("{\"ID\":\"FR100001\"," +
			"\"supplierID\":\"SU100001\"," +
			"\"purchaserID\":\"PU100001\"," +
			"\"financerID\":\"FI100001\"," +
			"\"invoiceID\":\"IN100001\"," +
			"\"purchaseOrderID\":\"PO100001\"," +
			"\"docType\":\"Asset.FinancingRequest\"," +
			"\"status\":\"In Process\"," +
			"\"invoiceGrossAmount\":\"100.00\"," +
			"\"invoiceDaysToCash\":\"15\"," +
			"\"invoicePaymentDate\":\"12/01/2017\"," +
			"\"requestCreationDate\":\"01/01/2017\"," +
			"\"requestedDisbursementDate\":\"01/01/2017\"," +
			"\"requestedDiscountPercent\":\"2.00\"," +
			"\"requestedFinancingAmount\":\"98.00\"," +
			"\"documentsChecked\":\"false\"," +
			"\"documentsVerified\":\"false\"," +
			"\"currency\":\"SGD\"}")}
}

func getFinancingRequestForSetFinancingRequestDeclinedNOK() [][]byte {
	return [][]byte{[]byte("addNewFinancingRequest"),
		[]byte("{\"ID\":\"FR100001\"," +
			"\"supplierID\":\"SU100001\"," +
			"\"purchaserID\":\"PU100001\"," +
			"\"financerID\":\"FI100001\"," +
			"\"invoiceID\":\"IN100001\"," +
			"\"purchaseOrderID\":\"PO100001\"," +
			"\"docType\":\"Asset.FinancingRequest\"," +
			"\"status\":\"Accepted\"," +
			"\"invoiceGrossAmount\":\"100.00\"," +
			"\"invoiceDaysToCash\":\"15\"," +
			"\"invoicePaymentDate\":\"12/01/2017\"," +
			"\"requestCreationDate\":\"01/01/2017\"," +
			"\"requestedDisbursementDate\":\"01/01/2017\"," +
			"\"requestedDiscountPercent\":\"2.00\"," +
			"\"requestedFinancingAmount\":\"98.00\"," +
			"\"documentsChecked\":\"false\"," +
			"\"documentsVerified\":\"false\"," +
			"\"currency\":\"SGD\"}")}
}

func getFinancingRequestForSetFinancingRequestDocumentsCheckedNOK() [][]byte {
	return [][]byte{[]byte("addNewFinancingRequest"),
		[]byte("{\"ID\":\"FR100001\"," +
			"\"supplierID\":\"SU100001\"," +
			"\"purchaserID\":\"PU100001\"," +
			"\"financerID\":\"FI100001\"," +
			"\"invoiceID\":\"IN100001\"," +
			"\"purchaseOrderID\":\"PO100001\"," +
			"\"docType\":\"Asset.FinancingRequest\"," +
			"\"status\":\"Accepted\"," +
			"\"invoiceGrossAmount\":\"100.00\"," +
			"\"invoiceDaysToCash\":\"15\"," +
			"\"invoicePaymentDate\":\"12/01/2017\"," +
			"\"requestCreationDate\":\"01/01/2017\"," +
			"\"requestedDisbursementDate\":\"01/01/2017\"," +
			"\"requestedDiscountPercent\":\"2.00\"," +
			"\"requestedFinancingAmount\":\"98.00\"," +
			"\"documentsChecked\":\"false\"," +
			"\"documentsVerified\":\"false\"," +
			"\"currency\":\"SGD\"}")}
}

func getFinancingRequestForSetFinancingRequestDocumentsVerifiedNOK() [][]byte {
	return [][]byte{[]byte("addNewFinancingRequest"),
		[]byte("{\"ID\":\"FR100001\"," +
			"\"supplierID\":\"SU100001\"," +
			"\"purchaserID\":\"PU100001\"," +
			"\"financerID\":\"FI100001\"," +
			"\"invoiceID\":\"IN100001\"," +
			"\"purchaseOrderID\":\"PO100001\"," +
			"\"docType\":\"Asset.FinancingRequest\"," +
			"\"status\":\"Accepted\"," +
			"\"invoiceGrossAmount\":\"100.00\"," +
			"\"invoiceDaysToCash\":\"15\"," +
			"\"invoicePaymentDate\":\"12/01/2017\"," +
			"\"requestCreationDate\":\"01/01/2017\"," +
			"\"requestedDisbursementDate\":\"01/01/2017\"," +
			"\"requestedDiscountPercent\":\"2.00\"," +
			"\"requestedFinancingAmount\":\"98.00\"," +
			"\"documentsChecked\":\"false\"," +
			"\"documentsVerified\":\"false\"," +
			"\"currency\":\"SGD\"}")}
}

//Get remove second FinancingRequestAsset for testing //change template
func getRemoveSecondFinancingRequestAssetForTesting() [][]byte {
	return [][]byte{[]byte("removeFinancingRequest"),
		[]byte("FR100002")}
}

//Get remove all FinanciingOfferAssets for testing
func getRemoveAllFinancingRequestAssetsForTesting() [][]byte {
	return [][]byte{[]byte("removeAllFinancingRequests")}
}

//Get an expected value for testing
func getNewFinancingRequestExpected() []byte {
	var financingRequest FinancingRequest
	financingRequest.ID = "FR100001"
	financingRequest.SupplierID = "SU100001"
	financingRequest.PurchaserID = "PU100001"
	financingRequest.FinancerID = "FI100001"
	financingRequest.InvoiceID = "IN100001"
	financingRequest.PurchaseOrderID = "PO100001"
	financingRequest.ObjectType = "Asset.FinancingRequest"
	financingRequest.Status = "Created"
	financingRequest.InvoiceGrossAmount = "100.00"
	financingRequest.InvoiceDaysToCash = "15"
	financingRequest.InvoicePaymentDate = "12/01/2017"
	financingRequest.RequestCreationDate = "01/01/2017"
	financingRequest.RequestedDisbursementDate = "01/01/2017"
	financingRequest.RequestedDiscountPercent = "2.00"
	financingRequest.RequestedFinancingAmount = "98.00"
	financingRequest.DocumentsChecked = "false"
	financingRequest.DocumentsVerified = "false"
	financingRequest.Currency = "SGD"
	financingRequestJSON, err := json.Marshal(financingRequest)
	if err != nil {
		fmt.Println("Error converting a FinancingRequest record to JSON")
		return nil
	}
	return []byte(financingRequestJSON)
}

//Get expected values of FinancingRequests for testing
func getExpectedFinancingRequests() []byte {
	var financingRequests []FinancingRequest
	var financingRequest FinancingRequest
	financingRequest.ID = "FR100001"
	financingRequest.SupplierID = "SU100001"
	financingRequest.PurchaserID = "PU100001"
	financingRequest.FinancerID = "FI100001"
	financingRequest.InvoiceID = "IN100001"
	financingRequest.PurchaseOrderID = "PO100001"
	financingRequest.ObjectType = "Asset.FinancingRequest"
	financingRequest.Status = "Created"
	financingRequest.InvoiceGrossAmount = "100.00"
	financingRequest.InvoiceDaysToCash = "15"
	financingRequest.InvoicePaymentDate = "12/01/2017"
	financingRequest.RequestCreationDate = "01/01/2017"
	financingRequest.RequestedDisbursementDate = "01/01/2017"
	financingRequest.RequestedDiscountPercent = "2.00"
	financingRequest.RequestedFinancingAmount = "98.00"
	financingRequest.DocumentsChecked = "false"
	financingRequest.DocumentsVerified = "false"
	financingRequest.Currency = "SGD"
	financingRequests = append(financingRequests, financingRequest)
	financingRequest.ID = "FR100002"
	financingRequest.SupplierID = "SU100002"
	financingRequest.PurchaserID = "PU100002"
	financingRequest.FinancerID = "FI100002"
	financingRequest.InvoiceID = "IN100002"
	financingRequest.PurchaseOrderID = "PO100002"
	financingRequest.ObjectType = "Asset.FinancingRequest"
	financingRequest.Status = "Created"
	financingRequest.InvoiceGrossAmount = "100.00"
	financingRequest.InvoiceDaysToCash = "15"
	financingRequest.InvoicePaymentDate = "12/01/2017"
	financingRequest.RequestCreationDate = "01/01/2017"
	financingRequest.RequestedDisbursementDate = "01/01/2017"
	financingRequest.RequestedDiscountPercent = "2.00"
	financingRequest.RequestedFinancingAmount = "98.00"
	financingRequest.DocumentsChecked = "false"
	financingRequest.DocumentsVerified = "false"
	financingRequest.Currency = "SGD"
	financingRequests = append(financingRequests, financingRequest)
	financingRequestJSON, err := json.Marshal(financingRequests)
	if err != nil {
		fmt.Println("Error converting financingRequest records to JSON")
		return nil
	}
	return []byte(financingRequestJSON)
}

func getExpectedFinancingRequestIDIndex(funcName string) []byte {
	var financingRequestIDIndex FinancingRequestIDIndex
	switch funcName {
	case "addNewFinancingRequest":
		financingRequestIDIndex.IDs = append(financingRequestIDIndex.IDs, "FR100001")
		financingRequestIDIndexBytes, err := json.Marshal(financingRequestIDIndex)
		if err != nil {
			fmt.Println("Error converting FinancingRequestIDIndex to JSON")
			return nil
		}
		return financingRequestIDIndexBytes
	case "beforeRemoveFinancingRequest":
		financingRequestIDIndex.IDs = append(financingRequestIDIndex.IDs, "FR100001")
		financingRequestIDIndex.IDs = append(financingRequestIDIndex.IDs, "FR100002")
		financingRequestIDIndexBytes, err := json.Marshal(financingRequestIDIndex)
		if err != nil {
			fmt.Println("Error converting FinancingRequestIDIndex to JSON")
			return nil
		}
		return financingRequestIDIndexBytes
	case "afterRemoveFinancingRequest":
		financingRequestIDIndex.IDs = append(financingRequestIDIndex.IDs, "FR100001")
		financingRequestIDIndexBytes, err := json.Marshal(financingRequestIDIndex)
		if err != nil {
			fmt.Println("Error converting FinancingRequestIDIndex to JSON")
			return nil
		}
		return financingRequestIDIndexBytes
	default:
		financingRequestIDIndexBytes, err := json.Marshal(financingRequestIDIndex)
		if err != nil {
			fmt.Println("Error converting FinancingRequestIDIndex to JSON")
			return nil
		}
		return financingRequestIDIndexBytes
	}
}

//checkInit - helper to check the Initialization of chaincode: FinancingRequestAsset
func checkInit(t *testing.T, stub *shim.MockStub, args [][]byte) {
	res := stub.MockInit("1", args)
	if res.Status != shim.OK {
		fmt.Println("Init failed", string(res.Message))
		t.FailNow()
	}
}

//checkState - helper for checking the chaincode state for a given stateKey against an expected value
func checkState(t *testing.T, stub *shim.MockStub, stateKey string, expectedState []byte) {
	actualState := stub.State[stateKey]
	if actualState == nil {
		fmt.Println("State for ", stateKey, ": failed to get value")
		t.FailNow()
	}

	if bytes.Compare(actualState, expectedState) != 0 {
		fmt.Println("Incorrect State for " + stateKey + ": \nExpected: " + string(expectedState) + "\nActual  : " + string(actualState))
		t.FailNow()
	}
}

//checkInvoke - helper for checking Invoke of chaincode
func checkInvoke(t *testing.T, stub *shim.MockStub, args [][]byte) {
	res := stub.MockInvoke("1", args)
	if res.Status != shim.OK {
		fmt.Println("Invoke", args, "failed", string(res.Message))
		t.FailNow()
	}
}

func checkInvokeUnknownFunction(t *testing.T, stub *shim.MockStub, args [][]byte) {
	res := stub.MockInvoke("1", args)
	if res.Status != shim.OK {
		expectedErr := "Received unknown function invocation"
		actualErr := string(res.Message)
		if !(strings.Contains(actualErr, expectedErr)) {
			fmt.Println("Invoke with unknown function must throw error: ", expectedErr, "\n Actual Error :", actualErr)
			t.FailNow()
		}
	} else {
		t.FailNow()
	}
}

//checkReadFinancingRequestOK - helper for positive test readFinancingRequest
func checkReadFinancingRequestOK(t *testing.T, stub *shim.MockStub, financingRequestID string) {
	res := stub.MockInvoke("1", [][]byte{[]byte("readFinancingRequest"), []byte(financingRequestID)})
	if res.Status != shim.OK {
		fmt.Println("func readFinancingRequest with ID: ", financingRequestID, " failed"+string(res.Message))
		t.FailNow()
	}
	if res.Payload == nil {
		fmt.Println("func readFinancingRequest with ID: ", financingRequestID, "failed to get value")
		t.FailNow()
	}
	if bytes.Compare(getNewFinancingRequestExpected(), []byte(res.Payload)) != 0 {
		fmt.Println("func readFinancingRequest with ID: ", financingRequestID, "Expected:", string(getNewFinancingRequestExpected()), "Actual:", string(res.Payload))
		t.FailNow()
	}
}

//checkReadFinancingRequestNOK - helper for negative testing of readFinancingRequest
func checkReadFinancingRequestNOK(t *testing.T, stub *shim.MockStub, financingRequestID string) {
	//with no financingRequestID
	res := stub.MockInvoke("1", [][]byte{[]byte("readFinancingRequest"), []byte("")})
	if res.Status != shim.OK {
		expectedErr := "retrieveFinancingRequest: Corrupt financingRequest record "
		actualErr := string(res.Message)
		if !(strings.Contains(actualErr, expectedErr)) {
			fmt.Println("func readFinancingRequest negative test: ", "Expected Error:", expectedErr, "Actual Error", actualErr)
			t.FailNow()
		}
	} else {
		t.FailNow()
	}
}

func checkReadAllFinancingRequestsOK(t *testing.T, stub *shim.MockStub) {
	res := stub.MockInvoke("1", [][]byte{[]byte("readAllFinancingRequests")})
	if res.Status != shim.OK {
		fmt.Println("func readAllFinancingRequests failed", string(res.Message))
		t.FailNow()
	}
	if res.Payload == nil {
		fmt.Println("func readAllFinancingRequests failed to get value")
		t.FailNow()
	}
	if bytes.Compare(getExpectedFinancingRequests(), []byte(res.Payload)) != 0 {
		fmt.Println("func readAllFinancingRequests Expected:\n", string(getExpectedFinancingRequests()), "\nActual:\n", string(res.Payload))
		t.FailNow()
	}
}

func checkError(t *testing.T, exp string, act string) {
	if strings.Compare(exp, act) != 0 {
		fmt.Println("Unexpected Error! Expecting ", exp, "\n Actual :", act)
		t.FailNow()
	}
}
