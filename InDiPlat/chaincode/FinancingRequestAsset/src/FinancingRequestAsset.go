package main

import (
	"encoding/json"
	"errors"
	"fmt"
	"strings"

	"github.com/hyperledger/fabric/core/chaincode/shim"
	"github.com/hyperledger/fabric/protos/peer"
)

var logger = shim.NewLogger("CLDChaincode")

//Created - Created - Start status
const Created = "Created"

//InProcess - In Process - Intermediate Status
const InProcess = "In Process"

//Accepted - Acceoted - End Status
const Accepted = "Accepted"

//Declined - Declined - End Status
const Declined = "Declined"

//FinancingRequestAsset - Chaincode for asset FinancingRequest
type FinancingRequestAsset struct {
}

//FinancingRequest - Details of the asset type FinancingRequest
type FinancingRequest struct {
	ID                        string `json:"ID"`
	SupplierID                string `json:"supplierID"`
	PurchaserID               string `json:"purchaserID"`
	FinancerID                string `json:"financerID"`
	InvoiceID                 string `json:"invoiceID"`
	PurchaseOrderID           string `json:"purchaseOrderID"`
	ObjectType                string `json:"docType"`
	Status                    string `json:"status"`
	InvoiceGrossAmount        string `json:"invoiceGrossAmount"`
	InvoiceDaysToCash         string `json:"invoiceDaysToCash"`
	InvoicePaymentDate        string `json:"invoicePaymentDate"`
	RequestCreationDate       string `json:"requestCreationDate"`
	RequestedDisbursementDate string `json:"requestedDisbursementDate"`
	RequestedDiscountPercent  string `json:"requestedDiscountPercent"`
	RequestedFinancingAmount  string `json:"requestedFinancingAmount"`
	DocumentsChecked          string `json:"documentsChecked"`
	DocumentsVerified         string `json:"documentsVerified"`
	Currency                  string `json:"currency"`
}

//FinancingRequestIDIndex - Index on IDs for retrieval all FinancingRequests
type FinancingRequestIDIndex struct {
	IDs []string `json:"IDs"`
}

func main() {
	err := shim.Start(new(FinancingRequestAsset))
	if err != nil {
		fmt.Printf("Error starting FinancingRequestAsset chaincode function main(): %s", err)
	} else {
		fmt.Printf("Starting FinancingRequestAsset chaincode function main() executed successfully")
	}
}

//Init - The chaincode Init function: No  arguments, only initializes a ID array as Index for retrieval of all FinancingRequests
func (finReq *FinancingRequestAsset) Init(stub shim.ChaincodeStubInterface) peer.Response {
	var financingRequestIDIndex FinancingRequestIDIndex
	record, _ := stub.GetState("financingRequestIDIndex")
	//chaincode upgrade: Index might already exist, create new only if it does not
	if record == nil {
		bytes, _ := json.Marshal(financingRequestIDIndex)
		stub.PutState("financingRequestIDIndex", bytes)
	}
	return shim.Success(nil)
}

//Invoke - The chaincode Invoke function:
func (finReq *FinancingRequestAsset) Invoke(stub shim.ChaincodeStubInterface) peer.Response {
	function, args := stub.GetFunctionAndParameters()
	logger.Debug("function: ", function)
	switch function {
	case "addNewFinancingRequest":
		return finReq.addNewFinancingRequest(stub, args)
	case "setFinancingRequestInProcess":
		return finReq.setFinancingRequestInProcess(stub, args)
	case "setFinancingRequestAccepted":
		return finReq.setFinancingRequestAccepted(stub, args)
	case "setFinancingRequestDeclined":
		return finReq.setFinancingRequestDeclined(stub, args[0])
	case "setFinancingRequestDocumentsChecked":
		return finReq.setFinancingRequestDocumentsChecked(stub, args[0])
	case "setFinancingRequestDocumentsVerified":
		return finReq.setFinancingRequestDocumentsVerified(stub, args[0])
	case "removeFinancingRequest":
		return finReq.removeFinancingRequest(stub, args[0])
	case "removeAllFinancingRequests":
		return finReq.removeAllFinancingRequests(stub)
	case "readFinancingRequest":
		return finReq.readFinancingRequest(stub, args[0])
	case "readAllFinancingRequests":
		return finReq.readAllFinancingRequests(stub)
	default:
		return shim.Error("Received unknown function invocation")
	}
}

//Invoke Route: addNewFinancingRequest
func (finReq *FinancingRequestAsset) addNewFinancingRequest(stub shim.ChaincodeStubInterface, args []string) peer.Response {
	financingRequest, err := getFinancingRequestFromArgs(args)
	if err != nil {
		return shim.Error("FinancingRequest Data is Corrupted")
	}
	financingRequest.ObjectType = "Asset.FinancingRequest"
	record, err := stub.GetState(financingRequest.ID)
	if record != nil {
		return shim.Error("This FinancingRequest already exists: " + financingRequest.ID)
	}
	_, err = finReq.saveFinancingRequest(stub, financingRequest)
	if err != nil {
		return shim.Error(err.Error())
	}
	_, err = finReq.updateFinancingRequestIDIndex(stub, financingRequest)
	if err != nil {
		return shim.Error(err.Error())
	}
	return shim.Success(nil)
}

//Invoke Route: setFinancingRequestInProcess
func (finReq *FinancingRequestAsset) setFinancingRequestInProcess(stub shim.ChaincodeStubInterface, args []string) peer.Response {

	var financingRequest FinancingRequest

	input, err := getFinancingRequestFromArgs(args)
	if err != nil {
		return shim.Error("FinancingRequest Data is Corrupted")
	}
	_, err = stub.GetState(input.ID)
	if err != nil {
		return shim.Error("Financing Request with ID: " + input.ID + "not found")
	}

	financingRequestAsByteArray, err := finReq.retrieveFinancingRequest(stub, input.ID)
	if err != nil {
		return shim.Error(err.Error())
	}
	err = json.Unmarshal(financingRequestAsByteArray, &financingRequest)
	if err != nil {
		return shim.Error(err.Error())
	}
	if financingRequest.Status == Accepted || financingRequest.Status == Declined {
		return shim.Error("FinancingRequest with ID: " + input.ID + " cannot be set in process")
	}
	financingRequest.Status = InProcess
	financingRequest.RequestedDisbursementDate = input.RequestedDisbursementDate
	financingRequest.RequestedDiscountPercent = input.RequestedDiscountPercent
	financingRequest.RequestedFinancingAmount = input.RequestedFinancingAmount
	_, err = finReq.saveFinancingRequest(stub, financingRequest)
	if err != nil {
		return shim.Error(err.Error())
	}
	return shim.Success(nil)
}

//Invoke Route: setFinancingRequestAccepted
func (finReq *FinancingRequestAsset) setFinancingRequestAccepted(stub shim.ChaincodeStubInterface, args []string) peer.Response {
	var financingRequest FinancingRequest

	input, err := getFinancingRequestFromArgs(args)
	if err != nil {
		return shim.Error("FinancingRequest Data is Corrupted")
	}

	_, err = stub.GetState(input.ID)
	if err != nil {
		return shim.Error("FinancingRequestAsset with ID: " + input.ID + "not found")
	}
	financingRequestAsByteArray, err := finReq.retrieveFinancingRequest(stub, input.ID)
	if err != nil {
		return shim.Error(err.Error())
	}
	err = json.Unmarshal(financingRequestAsByteArray, &financingRequest)
	if err != nil {
		return shim.Error(err.Error())
	}
	if financingRequest.Status != InProcess {
		return shim.Error("FinancingRequest with ID: " + input.ID + " cannot be accepted")
	}
	financingRequest.Status = Accepted
	financingRequest.RequestedDiscountPercent = input.RequestedDiscountPercent
	financingRequest.RequestedFinancingAmount = input.RequestedFinancingAmount
	_, err = finReq.saveFinancingRequest(stub, financingRequest)
	if err != nil {
		return shim.Error(err.Error())
	}
	return shim.Success(nil)
}

//Invoke Route: setFinancingRequestDeclined
func (finReq *FinancingRequestAsset) setFinancingRequestDeclined(stub shim.ChaincodeStubInterface, financingRequestID string) peer.Response {
	var financingRequest FinancingRequest
	_, err := stub.GetState(financingRequestID)
	if err != nil {
		return shim.Error("FinancingRequest with ID: " + financingRequestID + "not found")
	}
	financingRequestAsByteArray, err := finReq.retrieveFinancingRequest(stub, financingRequestID)
	if err != nil {
		return shim.Error(err.Error())
	}
	err = json.Unmarshal(financingRequestAsByteArray, &financingRequest)
	if err != nil {
		return shim.Error(err.Error())
	}
	if financingRequest.Status != InProcess {
		return shim.Error("FinancingRequest with ID: " + financingRequestID + " cannot be declined")
	}
	financingRequest.Status = Declined
	_, err = finReq.saveFinancingRequest(stub, financingRequest)
	if err != nil {
		return shim.Error(err.Error())
	}
	return shim.Success(nil)
}

//Invoke Route: setFinancingRequestDocumentsChecked
func (finReq *FinancingRequestAsset) setFinancingRequestDocumentsChecked(stub shim.ChaincodeStubInterface, financingRequestID string) peer.Response {
	var financingRequest FinancingRequest
	_, err := stub.GetState(financingRequestID)
	if err != nil {
		return shim.Error("FinancingRequest with ID: " + financingRequestID + "not found")
	}
	financingRequestAsByteArray, err := finReq.retrieveFinancingRequest(stub, financingRequestID)
	if err != nil {
		return shim.Error(err.Error())
	}
	err = json.Unmarshal(financingRequestAsByteArray, &financingRequest)
	if err != nil {
		return shim.Error(err.Error())
	}
	if financingRequest.DocumentsChecked != "false" {
		return shim.Error("Documents check for FinancingRequest with ID: " + financingRequestID + " already done")
	}
	financingRequest.DocumentsChecked = "true"
	_, err = finReq.saveFinancingRequest(stub, financingRequest)
	if err != nil {
		return shim.Error(err.Error())
	}
	return shim.Success(nil)
}

//Invoke Route: setFinancingRequestDocumentsVerified
func (finReq *FinancingRequestAsset) setFinancingRequestDocumentsVerified(stub shim.ChaincodeStubInterface, financingRequestID string) peer.Response {
	var financingRequest FinancingRequest
	_, err := stub.GetState(financingRequestID)
	if err != nil {
		return shim.Error("FinancingRequest with ID: " + financingRequestID + "not found")
	}
	financingRequestAsByteArray, err := finReq.retrieveFinancingRequest(stub, financingRequestID)
	if err != nil {
		return shim.Error(err.Error())
	}
	err = json.Unmarshal(financingRequestAsByteArray, &financingRequest)
	if err != nil {
		return shim.Error(err.Error())
	}
	if financingRequest.DocumentsVerified != "false" {
		return shim.Error("Documents verification for FinancingRequest with ID: " + financingRequestID + " already done")
	}
	financingRequest.DocumentsVerified = "true"
	_, err = finReq.saveFinancingRequest(stub, financingRequest)
	if err != nil {
		return shim.Error(err.Error())
	}
	return shim.Success(nil)
}

//Invoke Route: removeFinancingRequest
func (finReq *FinancingRequestAsset) removeFinancingRequest(stub shim.ChaincodeStubInterface, financingRequestID string) peer.Response {
	_, err := finReq.deleteFinancingRequest(stub, financingRequestID)
	if err != nil {
		return shim.Error(err.Error())
	}
	_, err = finReq.deleteFinancingRequestIDIndex(stub, financingRequestID)
	if err != nil {
		return shim.Error(err.Error())
	}
	return shim.Success(nil)
}

//Invoke Route: removeAllFinancingRequests
func (finReq *FinancingRequestAsset) removeAllFinancingRequests(stub shim.ChaincodeStubInterface) peer.Response {
	var financingRequestStructIDs FinancingRequestIDIndex
	bytes, err := stub.GetState("financingRequestIDIndex")
	if err != nil {
		return shim.Error("removeAllFinancingRequests: Error getting financingRequestIDIndex array")
	}
	err = json.Unmarshal(bytes, &financingRequestStructIDs)
	if err != nil {
		return shim.Error("removeAllFinancingRequests: Error unmarshalling financingRequestIDIndex array JSON")
	}
	if len(financingRequestStructIDs.IDs) == 0 {
		return shim.Error("removeAllFinancingRequests: No financingRequests to remove")
	}
	for _, financingRequestStructParticipantID := range financingRequestStructIDs.IDs {
		_, err = finReq.deleteFinancingRequest(stub, financingRequestStructParticipantID)
		if err != nil {
			return shim.Error("Failed to remove FinancingRequest with ID: " + financingRequestStructParticipantID)
		}
		_, err = finReq.deleteFinancingRequestIDIndex(stub, financingRequestStructParticipantID)
		if err != nil {
			return shim.Error(err.Error())
		}
	}
	finReq.initHolder(stub)
	return shim.Success(nil)
}

//Query Route: readFinancingRequest
func (finReq *FinancingRequestAsset) readFinancingRequest(stub shim.ChaincodeStubInterface, financingRequestID string) peer.Response {
	financingRequestAsByteArray, err := finReq.retrieveFinancingRequest(stub, financingRequestID)
	if err != nil {
		return shim.Error(err.Error())
	}
	return shim.Success(financingRequestAsByteArray)
}

//Query Route: readAllFinancingRequests
func (finReq *FinancingRequestAsset) readAllFinancingRequests(stub shim.ChaincodeStubInterface) peer.Response {
	var financingRequestIDs FinancingRequestIDIndex
	bytes, err := stub.GetState("financingRequestIDIndex")
	if err != nil {
		return shim.Error("readAllFinancingRequests: Error getting financingRequestIDIndex array")
	}
	err = json.Unmarshal(bytes, &financingRequestIDs)
	if err != nil {
		return shim.Error("readAllFinancingRequests: Error unmarshalling financingRequestIDIndex array JSON")
	}
	result := "["

	var financingRequestAsByteArray []byte

	for _, financingRequestID := range financingRequestIDs.IDs {
		financingRequestAsByteArray, err = finReq.retrieveFinancingRequest(stub, financingRequestID)
		if err != nil {
			return shim.Error("Failed to retrieve financingRequest with ID: " + financingRequestID)
		}
		result += string(financingRequestAsByteArray) + ","
	}
	if len(result) == 1 {
		result = "[]"
	} else {
		result = result[:len(result)-1] + "]"
	}
	return shim.Success([]byte(result))
}

//Helper: Save FinancingRequest
func (finReq *FinancingRequestAsset) saveFinancingRequest(stub shim.ChaincodeStubInterface, financingRequest FinancingRequest) (bool, error) {
	bytes, err := json.Marshal(financingRequest)
	if err != nil {
		return false, errors.New("Error converting financingRequest record JSON")
	}
	err = stub.PutState(financingRequest.ID, bytes)
	if err != nil {
		return false, errors.New("Error storing FinancingRequest record")
	}
	return true, nil
}

//Helper: FinancingRequest financingRequestStruct //change template
func (financingRequest *FinancingRequestAsset) deleteFinancingRequest(stub shim.ChaincodeStubInterface, financingRequestID string) (bool, error) {
	_, err := financingRequest.retrieveFinancingRequest(stub, financingRequestID)
	if err != nil {
		return false, errors.New("FinancingRequest with ID: " + financingRequestID + " not found")
	}
	err = stub.DelState(financingRequestID)
	if err != nil {
		return false, errors.New("Error deleting FinancingRequest record")
	}
	return true, nil
}

//Helper: Update financingRequest Holder - updates Index
func (finReq *FinancingRequestAsset) updateFinancingRequestIDIndex(stub shim.ChaincodeStubInterface, financingRequest FinancingRequest) (bool, error) {
	var financingRequestIDs FinancingRequestIDIndex
	bytes, err := stub.GetState("financingRequestIDIndex")
	if err != nil {
		return false, errors.New("updateFinancingRequestIDIndex: Error getting financingRequestIDIndex array Index from state")
	}
	err = json.Unmarshal(bytes, &financingRequestIDs)
	if err != nil {
		return false, errors.New("updateFinancingRequestIDIndex: Error unmarshalling financingRequestIDIndex array JSON")
	}
	financingRequestIDs.IDs = append(financingRequestIDs.IDs, financingRequest.ID)
	bytes, err = json.Marshal(financingRequestIDs)
	if err != nil {
		return false, errors.New("updateFinancingRequestIDIndex: Error marshalling new financingRequest ID")
	}
	err = stub.PutState("financingRequestIDIndex", bytes)
	if err != nil {
		return false, errors.New("updateFinancingRequestIDIndex: Error storing new financingRequest ID in financingRequestIDIndex (Index)")
	}
	return true, nil
}

//Helper: delete ID from financingRequestStruct Holder
func (finReq *FinancingRequestAsset) deleteFinancingRequestIDIndex(stub shim.ChaincodeStubInterface, financingRequestID string) (bool, error) {
	var financingRequestStructIDs FinancingRequestIDIndex
	bytes, err := stub.GetState("financingRequestIDIndex")
	if err != nil {
		return false, errors.New("deleteFinancingRequestIDIndex: Error getting financingRequestIDIndex array Index from state")
	}
	err = json.Unmarshal(bytes, &financingRequestStructIDs)
	if err != nil {
		return false, errors.New("deleteFinancingRequestIDIndex: Error unmarshalling financingRequestIDIndex array JSON")
	}
	financingRequestStructIDs.IDs, err = deleteKeyFromStringArray(financingRequestStructIDs.IDs, financingRequestID)
	if err != nil {
		return false, errors.New(err.Error())
	}
	bytes, err = json.Marshal(financingRequestStructIDs)
	if err != nil {
		return false, errors.New("deleteFinancingRequestIDIndex: Error marshalling new financingRequestStruct ID")
	}
	err = stub.PutState("financingRequestIDIndex", bytes)
	if err != nil {
		return false, errors.New("deleteFinancingRequestIDIndex: Error storing new financingRequestStruct ID in financingRequestIDIndex (Index)")
	}
	return true, nil
}

//Helper: Initialize ID Holder
func (finReq *FinancingRequestAsset) initHolder(stub shim.ChaincodeStubInterface) bool {
	var financingRequestIDIndex FinancingRequestIDIndex
	bytes, _ := json.Marshal(financingRequestIDIndex)
	stub.DelState("financingRequestIDIndex")
	stub.PutState("financingRequestIDIndex", bytes)
	return true
}

//deleteKeyFromArray
func deleteKeyFromStringArray(array []string, key string) (newArray []string, err error) {
	for _, entry := range array {
		if entry != key {
			newArray = append(newArray, entry)
		}
	}
	if len(newArray) == len(array) {
		return newArray, errors.New("Specified Key: " + key + " not found in Array")
	}
	return newArray, nil
}

//Helper: Retrieve purchaser
func (finReq *FinancingRequestAsset) retrieveFinancingRequest(stub shim.ChaincodeStubInterface, financingRequestID string) ([]byte, error) {
	var financingRequest FinancingRequest
	var financingRequestAsByteArray []byte
	bytes, err := stub.GetState(financingRequestID)
	if err != nil {
		return financingRequestAsByteArray, errors.New("retrieveFinancingRequest: Error retrieving financingRequest with ID: " + financingRequestID)
	}
	err = json.Unmarshal(bytes, &financingRequest)
	if err != nil {
		return financingRequestAsByteArray, errors.New("retrieveFinancingRequest: Corrupt financingRequest record " + string(bytes))
	}
	financingRequestAsByteArray, err = json.Marshal(financingRequest)
	if err != nil {
		return financingRequestAsByteArray, errors.New("readFinancingRequest: Invalid financingRequest Object - Not a  valid JSON")
	}
	return financingRequestAsByteArray, nil
}

//getFinancingRequestFromArgs - construct a financingRequest structure from string array of arguments
func getFinancingRequestFromArgs(args []string) (sample FinancingRequest, err error) {

	if strings.Contains(args[0], "\"ID\"") == false ||
		strings.Contains(args[0], "\"supplierID\"") == false ||
		strings.Contains(args[0], "\"purchaserID\"") == false ||
		strings.Contains(args[0], "\"financerID\"") == false ||
		strings.Contains(args[0], "\"invoiceID\"") == false ||
		strings.Contains(args[0], "\"purchaseOrderID\"") == false ||
		strings.Contains(args[0], "\"docType\"") == false ||
		strings.Contains(args[0], "\"status\"") == false ||
		strings.Contains(args[0], "\"invoiceGrossAmount\"") == false ||
		strings.Contains(args[0], "\"invoiceDaysToCash\"") == false ||
		strings.Contains(args[0], "\"invoicePaymentDate\"") == false ||
		strings.Contains(args[0], "\"requestCreationDate\"") == false ||
		strings.Contains(args[0], "\"requestedDisbursementDate\"") == false ||
		strings.Contains(args[0], "\"requestedDiscountPercent\"") == false ||
		strings.Contains(args[0], "\"requestedFinancingAmount\"") == false ||
		strings.Contains(args[0], "\"documentsChecked\"") == false ||
		strings.Contains(args[0], "\"documentsVerified\"") == false ||
		strings.Contains(args[0], "\"currency\"") == false {
		return sample, errors.New("Unknown field: " + "Input JSON does not comply to schema")
	}

	err = json.Unmarshal([]byte(args[0]), &sample)
	if err != nil {
		return sample, err
	}
	return sample, nil

}
