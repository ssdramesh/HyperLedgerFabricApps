package main

import (
	"bytes"
	"encoding/json"
	"fmt"
	"strings"
	"testing"

	"github.com/hyperledger/fabric/core/chaincode/shim"
)

//TestSupplierParticipant_Init
func TestSupplierParticipant_Init(t *testing.T) {
	sample := new(SupplierParticipant)
	stub := shim.NewMockStub("sample", sample)
	checkInit(t, stub, [][]byte{[]byte("init")})
	checkState(t, stub, "supplierIDIndex", getExpectedSupplierIDIndex("init"))
}

//TestSupplierParticipant_InvokeUnknownFunction
func TestSupplierParticipant_InvokeUnknownFunction(t *testing.T) {
	sample := new(SupplierParticipant)
	stub := shim.NewMockStub("sample", sample)
	checkInit(t, stub, [][]byte{[]byte("init")})
	checkInvokeUnknownFunction(t, stub, [][]byte{[]byte("myFunction"), []byte("docType:Participant")})
}

//TestSupplierParticipant_Invoke_addNewSupplier
func TestSupplierParticipant_Invoke_addNewSupplierOK(t *testing.T) {
	sample := new(SupplierParticipant)
	stub := shim.NewMockStub("sample", sample)
	checkInit(t, stub, [][]byte{[]byte("init")})
	checkInvoke(t, stub, getFirstSupplierParticipantForTesting())
	newSupplierID := "100001"
	checkState(t, stub, newSupplierID, getNewSupplierExpected())
	checkState(t, stub, "supplierIDIndex", getExpectedSupplierIDIndex("addNewSupplier"))
}

//TestSupplierParticipant_Invoke_addNewSupplierUnknownField
func TestSupplierParticipant_Invoke_addNewSupplierUnknownField(t *testing.T) {
	sample := new(SupplierParticipant)
	stub := shim.NewMockStub("sample", sample)
	checkInit(t, stub, [][]byte{[]byte("init")})
	res := stub.MockInvoke("1", getSupplierParticipantWithUnknownFieldForTesting())
	if res.Status != shim.OK {
		checkError(t, "Supplier Data is Corrupted", res.Message)
	} else {
		fmt.Println("Unknown Field Error was expected, but not raised")
		t.FailNow()
	}
}

//TestSupplierParticipant_Invoke_addNewSupplier
func TestSupplierParticipant_Invoke_addNewSupplierDuplicate(t *testing.T) {
	sample := new(SupplierParticipant)
	stub := shim.NewMockStub("sample", sample)
	checkInit(t, stub, [][]byte{[]byte("init")})
	checkInvoke(t, stub, getFirstSupplierParticipantForTesting())
	newSupplierID := "100001"
	checkState(t, stub, newSupplierID, getNewSupplierExpected())
	checkState(t, stub, "supplierIDIndex", getExpectedSupplierIDIndex("addNewSupplier"))
	res := stub.MockInvoke("1", getFirstSupplierParticipantForTesting())
	if res.Status != shim.OK {
		checkError(t, "This Supplier already exists: 100001", res.Message)
	} else {
		fmt.Println("Error was expected, but not raised")
		t.FailNow()
	}
}

//TestSupplierParticipant_Invoke_removeSupplierOK  //change template
func TestSupplierParticipant_Invoke_removeSupplierOK(t *testing.T) {
	sample := new(SupplierParticipant)
	stub := shim.NewMockStub("sample", sample)
	checkInit(t, stub, [][]byte{[]byte("init")})
	checkInvoke(t, stub, getFirstSupplierParticipantForTesting())
	checkInvoke(t, stub, getSecondSupplierParticipantForTesting())
	checkReadAllSuppliersOK(t, stub)
	checkState(t, stub, "supplierIDIndex", getExpectedSupplierIDIndex("beforeRemoveSupplier"))
	checkInvoke(t, stub, getRemoveSecondSupplierParticipantForTesting())
	remainingSupplierID := "100001"
	checkReadSupplierOK(t, stub, remainingSupplierID)
	checkState(t, stub, "supplierIDIndex", getExpectedSupplierIDIndex("afterRemoveSupplier"))
}

//TestSupplierParticipant_Invoke_removeSupplierNOK  //change template
func TestSupplierParticipant_Invoke_removeSupplierNOK(t *testing.T) {
	sample := new(SupplierParticipant)
	stub := shim.NewMockStub("sample", sample)
	checkInit(t, stub, [][]byte{[]byte("init")})
	checkInvoke(t, stub, getFirstSupplierParticipantForTesting())
	firstSupplierID := "100001"
	checkReadSupplierOK(t, stub, firstSupplierID)
	checkState(t, stub, "supplierIDIndex", getExpectedSupplierIDIndex("addNewSupplier"))
	res := stub.MockInvoke("1", getRemoveSecondSupplierParticipantForTesting())
	if res.Status != shim.OK {
		checkError(t, "Supplier with ID: "+"100002"+" not found", res.Message)
	} else {
		fmt.Println("Error was expected, but not raised")
		t.FailNow()
	}
	checkState(t, stub, "supplierIDIndex", getExpectedSupplierIDIndex("addNewSupplier"))
}

//TestSupplierParticipant_Invoke_removeAllSupplierssOK
func TestSupplierParticipant_Invoke_removeAllSupplierssOK(t *testing.T) {
	financier := new(SupplierParticipant)
	stub := shim.NewMockStub("financier", financier)
	checkInit(t, stub, [][]byte{[]byte("init")})
	checkInvoke(t, stub, getFirstSupplierParticipantForTesting())
	checkInvoke(t, stub, getSecondSupplierParticipantForTesting())
	checkReadAllSuppliersOK(t, stub)
	checkState(t, stub, "supplierIDIndex", getExpectedSupplierIDIndex("beforeRemoveSupplier"))
	checkInvoke(t, stub, getRemoveAllSupplierParticipantsForTesting())
	checkState(t, stub, "supplierIDIndex", getExpectedSupplierIDIndex(""))
}

//TestSupplierParticipant_Query_readSupplier
func TestSupplierParticipant_Query_readSupplier(t *testing.T) {
	sample := new(SupplierParticipant)
	stub := shim.NewMockStub("sample", sample)
	checkInit(t, stub, [][]byte{[]byte("init")})
	sampleID := "100001"
	checkInvoke(t, stub, getFirstSupplierParticipantForTesting())
	checkReadSupplierOK(t, stub, sampleID)
	checkReadSupplierNOK(t, stub, "")
}

//TestSupplierParticipant_Query_readAllSuppliers
func TestSupplierParticipant_Query_readAllSuppliers(t *testing.T) {
	sample := new(SupplierParticipant)
	stub := shim.NewMockStub("sample", sample)
	checkInit(t, stub, [][]byte{[]byte("init")})
	checkInvoke(t, stub, getFirstSupplierParticipantForTesting())
	checkInvoke(t, stub, getSecondSupplierParticipantForTesting())
	checkReadAllSuppliersOK(t, stub)
}

/*
*
*	Helper Functions
*
 */
//Get first SupplierParticipant for testing
func getFirstSupplierParticipantForTesting() [][]byte {
	return [][]byte{[]byte("addNewSupplier"),
		[]byte("{\"ID\":\"100001\"," +
			"\"docType\":\"Participant\"," +
			"\"alias\":\"BRITECH\"," +
			"\"description\":\"British Technology Pvt. Ltd.\"}")}
}

//Get second SupplierParticipant for testing
func getSecondSupplierParticipantForTesting() [][]byte {
	return [][]byte{[]byte("addNewSupplier"),
		[]byte("{\"ID\":\"100002\"," +
			"\"docType\":\"Participant\"," +
			"\"alias\":\"DEMAGDELAG\"," +
			"\"description\":\"Demag Delewal AG\"}")}
}

//Get SupplierParticipant with unknown field for testing
func getSupplierParticipantWithUnknownFieldForTesting() [][]byte {
	return [][]byte{[]byte("addNewSupplier"),
		[]byte("{\"ID\":\"100001\"," +
			"\"docuType\":\"Participant\"," +
			"\"alias\":\"BRITECH\"," +
			"\"description\":\"British Technology Pvt. Ltd.\"}")}
}

//Get remove second SupplierParticipant for testing //change template
func getRemoveSecondSupplierParticipantForTesting() [][]byte {
	return [][]byte{[]byte("removeSupplier"),
		[]byte("100002")}
}

//Get remove all SupplierParticipants for testing
func getRemoveAllSupplierParticipantsForTesting() [][]byte {
	return [][]byte{[]byte("removeAllSuppliers")}
}

//Get an expected value for testing
func getNewSupplierExpected() []byte {
	var sample Supplier
	sample.ID = "100001"
	sample.ObjectType = "Participant"
	sample.Alias = "BRITECH"
	sample.Description = "British Technology Pvt. Ltd."
	sampleJSON, err := json.Marshal(sample)
	if err != nil {
		fmt.Println("Error converting a Supplier record to JSON")
		return nil
	}
	return []byte(sampleJSON)
}

//Get expected values of Suppliers for testing
func getExpectedSuppliers() []byte {
	var samples []Supplier
	var sample Supplier
	sample.ID = "100001"
	sample.ObjectType = "Participant"
	sample.Alias = "BRITECH"
	sample.Description = "British Technology Pvt. Ltd."
	samples = append(samples, sample)
	sample.ID = "100002"
	sample.ObjectType = "Participant"
	sample.Alias = "DEMAGDELAG"
	sample.Description = "Demag Delewal AG"
	samples = append(samples, sample)
	sampleJSON, err := json.Marshal(samples)
	if err != nil {
		fmt.Println("Error converting sampleancer records to JSON")
		return nil
	}
	return []byte(sampleJSON)
}

func getExpectedSupplierIDIndex(funcName string) []byte {
	var supplierIDIndex SupplierIDIndex
	switch funcName {
	case "addNewSupplier":
		supplierIDIndex.IDs = append(supplierIDIndex.IDs, "100001")
		supplierIDIndexBytes, err := json.Marshal(supplierIDIndex)
		if err != nil {
			fmt.Println("Error converting SupplierIDIndex to JSON")
			return nil
		}
		return supplierIDIndexBytes
	case "beforeRemoveSupplier":
		supplierIDIndex.IDs = append(supplierIDIndex.IDs, "100001")
		supplierIDIndex.IDs = append(supplierIDIndex.IDs, "100002")
		supplierIDIndexBytes, err := json.Marshal(supplierIDIndex)
		if err != nil {
			fmt.Println("Error converting SupplierIDIndex to JSON")
			return nil
		}
		return supplierIDIndexBytes
	case "afterRemoveSupplier":
		supplierIDIndex.IDs = append(supplierIDIndex.IDs, "100001")
		supplierIDIndexBytes, err := json.Marshal(supplierIDIndex)
		if err != nil {
			fmt.Println("Error converting SupplierIDIndex to JSON")
			return nil
		}
		return supplierIDIndexBytes
	default:
		supplierIDIndexBytes, err := json.Marshal(supplierIDIndex)
		if err != nil {
			fmt.Println("Error converting SupplierIDIndex to JSON")
			return nil
		}
		return supplierIDIndexBytes
	}
}

//checkInit - helper to check the Initialization of chaincode: SupplierParticipant
func checkInit(t *testing.T, stub *shim.MockStub, args [][]byte) {
	res := stub.MockInit("1", args)
	if res.Status != shim.OK {
		fmt.Println("Init failed", string(res.Message))
		t.FailNow()
	}
}

//checkState - helper for checking the chaincode state for a given stateKey afgainst an expected value
func checkState(t *testing.T, stub *shim.MockStub, stateKey string, expectedState []byte) {
	actualState := stub.State[stateKey]
	if actualState == nil {
		fmt.Println("State for ", stateKey, ": failed to get value")
		t.FailNow()
	}

	if bytes.Compare(actualState, expectedState) != 0 {
		fmt.Println("Incorrect State for " + stateKey + ": \nExpected: " + string(expectedState) + "\nActual  : " + string(actualState))
		t.FailNow()
	}
}

//checkInvoke - helper for checking Invoke of chaincode
func checkInvoke(t *testing.T, stub *shim.MockStub, args [][]byte) {
	res := stub.MockInvoke("1", args)
	if res.Status != shim.OK {
		fmt.Println("Invoke", args, "failed", string(res.Message))
		t.FailNow()
	}
}

func checkInvokeUnknownFunction(t *testing.T, stub *shim.MockStub, args [][]byte) {
	res := stub.MockInvoke("1", args)
	if res.Status != shim.OK {
		expectedErr := "Received unknown function invocation"
		actualErr := string(res.Message)
		if !(strings.Contains(actualErr, expectedErr)) {
			fmt.Println("Invoke with unknown function must throw error: ", expectedErr, "\n Actual Error :", actualErr)
			t.FailNow()
		}
	} else {
		t.FailNow()
	}
}

//checkReadSupplierOK - helper for positive test readSupplier
func checkReadSupplierOK(t *testing.T, stub *shim.MockStub, sampleancerID string) {
	res := stub.MockInvoke("1", [][]byte{[]byte("readSupplier"), []byte(sampleancerID)})
	if res.Status != shim.OK {
		fmt.Println("func readSupplier with ID: ", sampleancerID, " failed"+string(res.Message))
		t.FailNow()
	}
	if res.Payload == nil {
		fmt.Println("func readSupplier with ID: ", sampleancerID, "failed to get value")
		t.FailNow()
	}
	if bytes.Compare(getNewSupplierExpected(), []byte(res.Payload)) != 0 {
		fmt.Println("func readSupplier with ID: ", sampleancerID, "Expected:", string(getNewSupplierExpected()), "Actual:", string(res.Payload))
		t.FailNow()
	}
}

//checkReadSupplierNOK - helper for negative testing of readSupplier
func checkReadSupplierNOK(t *testing.T, stub *shim.MockStub, sampleancerID string) {
	//with no sampleancerID
	res := stub.MockInvoke("1", [][]byte{[]byte("readSupplier"), []byte("")})
	if res.Status != shim.OK {
		expectedErr := "retrieveSupplier: Corrupt sample record "
		actualErr := string(res.Message)
		if !(strings.Contains(actualErr, expectedErr)) {
			fmt.Println("func readSupplier neagtive test: ", "Expected Error:", expectedErr, "Actual Error", actualErr)
			t.FailNow()
		}
	} else {
		t.FailNow()
	}
}
func checkReadAllSuppliersOK(t *testing.T, stub *shim.MockStub) {
	res := stub.MockInvoke("1", [][]byte{[]byte("readAllSuppliers")})
	if res.Status != shim.OK {
		fmt.Println("func readAllSuppliers failed", string(res.Message))
		t.FailNow()
	}
	if res.Payload == nil {
		fmt.Println("func readAllSuppliers failed to get value")
		t.FailNow()
	}
	if bytes.Compare(getExpectedSuppliers(), []byte(res.Payload)) != 0 {
		fmt.Println("func readAllSuppliers Expected:\n", string(getExpectedSuppliers()), "\nActual:\n", string(res.Payload))
		t.FailNow()
	}
}

func checkError(t *testing.T, exp string, act string) {
	if strings.Compare(exp, act) != 0 {
		fmt.Println("Unexpected Error! Expecting ", exp, "\n Actual :", act)
		t.FailNow()
	}
}
