package main

import (
	"encoding/json"
	"errors"
	"fmt"
	"strings"

	"github.com/hyperledger/fabric/core/chaincode/shim"
	"github.com/hyperledger/fabric/protos/peer"
)

var logger = shim.NewLogger("CLDChaincode")

//SupplierParticipant - Chaincode for Supplier Participant
type SupplierParticipant struct {
}

//Supplier - Details of the participant type Supplier
type Supplier struct {
	ID          string `json:"ID"`
	ObjectType  string `json:"docType"`
	Alias       string `json:"alias"`
	Description string `json:"description"`
}

//SupplierIDIndex - Index on IDs for retrieval all Suppliers
type SupplierIDIndex struct {
	IDs []string `json:"IDs"`
}

func main() {
	err := shim.Start(new(SupplierParticipant))
	if err != nil {
		fmt.Printf("Error starting SupplierParticipant chaincode function main(): %s", err)
	} else {
		fmt.Printf("Starting SupplierParticipant chaincode function main() executed successfully")
	}
}

//Init - The chaincode Init function: No  arguments, only initializes a ID array as Index for retrieval of all Suppliers
func (sup *SupplierParticipant) Init(stub shim.ChaincodeStubInterface) peer.Response {
	var supplierIDIndex SupplierIDIndex
	record, _ := stub.GetState("supplierIDIndex")
	//chaincode upgrade: Index might already exist, create new only if it does not
	if record == nil {
		bytes, _ := json.Marshal(supplierIDIndex)
		stub.PutState("supplierIDIndex", bytes)
	}
	return shim.Success(nil)
}

//Invoke - The chaincode Invoke function:
func (sup *SupplierParticipant) Invoke(stub shim.ChaincodeStubInterface) peer.Response {
	function, args := stub.GetFunctionAndParameters()
	logger.Debug("function: ", function)
	switch function {
	case "addNewSupplier":
		return sup.addNewSupplier(stub, args)
	case "removeSupplier":
		return sup.removeSupplier(stub, args[0])
	case "removeAllSuppliers":
		return sup.removeAllSuppliers(stub)
	case "readSupplier":
		return sup.readSupplier(stub, args[0])
	case "readAllSuppliers":
		return sup.readAllSuppliers(stub)
	default:
		return shim.Error("Received unknown function invocation")
	}
}

//Invoke Route: addNewSupplier
func (sup *SupplierParticipant) addNewSupplier(stub shim.ChaincodeStubInterface, args []string) peer.Response {
	sample, err := getSupplierFromArgs(args)
	if err != nil {
		return shim.Error("Supplier Data is Corrupted")
	}
	sample.ObjectType = "Participant"
	record, err := stub.GetState(sample.ID)
	if record != nil {
		return shim.Error("This Supplier already exists: " + sample.ID)
	}
	_, err = sup.saveSupplier(stub, sample)
	if err != nil {
		return shim.Error(err.Error())
	}
	_, err = sup.updateSupplierIDIndex(stub, sample)
	if err != nil {
		return shim.Error(err.Error())
	}
	return shim.Success(nil)
}

//Invoke Route: removeSupplier
func (sup *SupplierParticipant) removeSupplier(stub shim.ChaincodeStubInterface, sampleStructParticipantID string) peer.Response {
	_, err := sup.deleteSupplier(stub, sampleStructParticipantID)
	if err != nil {
		return shim.Error(err.Error())
	}
	_, err = sup.deleteSupplierIDIndex(stub, sampleStructParticipantID)
	if err != nil {
		return shim.Error(err.Error())
	}
	return shim.Success(nil)
}

//Invoke Route: removeAllSuppliers
func (sup *SupplierParticipant) removeAllSuppliers(stub shim.ChaincodeStubInterface) peer.Response {
	var supplierStructIDs SupplierIDIndex
	bytes, err := stub.GetState("supplierIDIndex")
	if err != nil {
		return shim.Error("removeAllSuppliers: Error getting supplierIDIndex array")
	}
	err = json.Unmarshal(bytes, &supplierStructIDs)
	if err != nil {
		return shim.Error("removeAllSuppliers: Error unmarshalling supplierIDIndex array JSON")
	}
	if len(supplierStructIDs.IDs) == 0 {
		return shim.Error("removeAllSuppliers: No suppliers to remove")
	}
	for _, supplierStructParticipantID := range supplierStructIDs.IDs {
		_, err = sup.deleteSupplier(stub, supplierStructParticipantID)
		if err != nil {
			return shim.Error("Failed to remove Supplier with ID: " + supplierStructParticipantID)
		}
		_, err = sup.deleteSupplierIDIndex(stub, supplierStructParticipantID)
		if err != nil {
			return shim.Error(err.Error())
		}
	}
	sup.initHolder(stub)
	return shim.Success(nil)
}

//Query Route: readSupplier
func (sup *SupplierParticipant) readSupplier(stub shim.ChaincodeStubInterface, sampleParticipantID string) peer.Response {
	sampleAsByteArray, err := sup.retrieveSupplier(stub, sampleParticipantID)
	if err != nil {
		return shim.Error(err.Error())
	}
	return shim.Success(sampleAsByteArray)
}

//Query Route: readAllSuppliers
func (sup *SupplierParticipant) readAllSuppliers(stub shim.ChaincodeStubInterface) peer.Response {
	var sampleIDs SupplierIDIndex
	bytes, err := stub.GetState("supplierIDIndex")
	if err != nil {
		return shim.Error("readAllSuppliers: Error getting supplierIDIndex array")
	}
	err = json.Unmarshal(bytes, &sampleIDs)
	if err != nil {
		return shim.Error("readAllSuppliers: Error unmarshalling supplierIDIndex array JSON")
	}
	result := "["

	var sampleAsByteArray []byte

	for _, sampleID := range sampleIDs.IDs {
		sampleAsByteArray, err = sup.retrieveSupplier(stub, sampleID)
		if err != nil {
			return shim.Error("Failed to retrieve sample with ID: " + sampleID)
		}
		result += string(sampleAsByteArray) + ","
	}
	if len(result) == 1 {
		result = "[]"
	} else {
		result = result[:len(result)-1] + "]"
	}
	return shim.Success([]byte(result))
}

//Helper: Save sampleParticipant
func (sup *SupplierParticipant) saveSupplier(stub shim.ChaincodeStubInterface, sample Supplier) (bool, error) {
	bytes, err := json.Marshal(sample)
	if err != nil {
		return false, errors.New("Error converting sample record JSON")
	}
	err = stub.PutState(sample.ID, bytes)
	if err != nil {
		return false, errors.New("Error storing Supplier record")
	}
	return true, nil
}

//Helper: Delete sampleStructParticipant
func (sup *SupplierParticipant) deleteSupplier(stub shim.ChaincodeStubInterface, sampleStructParticipantID string) (bool, error) {
	_, err := sup.retrieveSupplier(stub, sampleStructParticipantID)
	if err != nil {
		return false, errors.New("Supplier with ID: " + sampleStructParticipantID + " not found")
	}
	err = stub.DelState(sampleStructParticipantID)
	if err != nil {
		return false, errors.New("Error deleting Supplier record")
	}
	return true, nil
}

//Helper: Update sample Holder - updates Index
func (sup *SupplierParticipant) updateSupplierIDIndex(stub shim.ChaincodeStubInterface, sample Supplier) (bool, error) {
	var sampleIDs SupplierIDIndex
	bytes, err := stub.GetState("supplierIDIndex")
	if err != nil {
		return false, errors.New("upadeteSupplierIDIndex: Error getting supplierIDIndex array Index from state")
	}
	err = json.Unmarshal(bytes, &sampleIDs)
	if err != nil {
		return false, errors.New("upadeteSupplierIDIndex: Error unmarshalling supplierIDIndex array JSON")
	}
	sampleIDs.IDs = append(sampleIDs.IDs, sample.ID)
	bytes, err = json.Marshal(sampleIDs)
	if err != nil {
		return false, errors.New("updateSupplierIDIndex: Error marshalling new sample ID")
	}
	err = stub.PutState("supplierIDIndex", bytes)
	if err != nil {
		return false, errors.New("updateSupplierIDIndex: Error storing new sample ID in supplierIDIndex (Index)")
	}
	return true, nil
}

//Helper: delete ID from sampleStruct Holder
func (sup *SupplierParticipant) deleteSupplierIDIndex(stub shim.ChaincodeStubInterface, sampleStructParticipantID string) (bool, error) {
	var sampleStructIDs SupplierIDIndex
	bytes, err := stub.GetState("supplierIDIndex")
	if err != nil {
		return false, errors.New("deleteSupplierIDIndex: Error getting supplierIDIndex array Index from state")
	}
	err = json.Unmarshal(bytes, &sampleStructIDs)
	if err != nil {
		return false, errors.New("deleteSupplierIDIndex: Error unmarshalling supplierIDIndex array JSON")
	}
	sampleStructIDs.IDs, err = deleteKeyFromStringArray(sampleStructIDs.IDs, sampleStructParticipantID)
	if err != nil {
		return false, errors.New(err.Error())
	}
	bytes, err = json.Marshal(sampleStructIDs)
	if err != nil {
		return false, errors.New("deleteSupplierIDIndex: Error marshalling new sampleStruct ID")
	}
	err = stub.PutState("supplierIDIndex", bytes)
	if err != nil {
		return false, errors.New("deleteSupplierIDIndex: Error storing new sampleStruct ID in supplierIDIndex (Index)")
	}
	return true, nil
}

//Helper: Initialize sample ID Holder
func (sup *SupplierParticipant) initHolder(stub shim.ChaincodeStubInterface) bool {
	var supplierIDIndex SupplierIDIndex
	bytes, _ := json.Marshal(supplierIDIndex)
	stub.DelState("supplierIDIndex")
	stub.PutState("supplierIDIndex", bytes)
	return true
}

//Helper: Retrieve sampleParticipant
func (sup *SupplierParticipant) retrieveSupplier(stub shim.ChaincodeStubInterface, sampleParticipantID string) ([]byte, error) {
	var sample Supplier
	var sampleAsByteArray []byte
	bytes, err := stub.GetState(sampleParticipantID)
	if err != nil {
		return sampleAsByteArray, errors.New("retrieveSupplier: Error retrieving sample with ID: " + sampleParticipantID)
	}
	err = json.Unmarshal(bytes, &sample)
	if err != nil {
		return sampleAsByteArray, errors.New("retrieveSupplier: Corrupt sample record " + string(bytes))
	}
	sampleAsByteArray, err = json.Marshal(sample)
	if err != nil {
		return sampleAsByteArray, errors.New("readSupplier: Invalid sample Object - Not a  valid JSON")
	}
	return sampleAsByteArray, nil
}

//deleteKeyFromArray
func deleteKeyFromStringArray(array []string, key string) (newArray []string, err error) {
	for _, entry := range array {
		if entry != key {
			newArray = append(newArray, entry)
		}
	}
	if len(newArray) == len(array) {
		return newArray, errors.New("Specified Key: " + key + " not found in Array")
	}
	return newArray, nil
}

//getSupplierFromArgs - construct a sample structure from string array of arguments
func getSupplierFromArgs(args []string) (sample Supplier, err error) {

	if strings.Contains(args[0], "\"ID\"") == false ||
		strings.Contains(args[0], "\"docType\"") == false ||
		strings.Contains(args[0], "\"alias\"") == false ||
		strings.Contains(args[0], "\"description\"") == false {
		return sample, errors.New("Unknown field: " + "Input JSON does not comply to schema")
	}

	err = json.Unmarshal([]byte(args[0]), &sample)
	if err != nil {
		return sample, err
	}
	return sample, nil

}
