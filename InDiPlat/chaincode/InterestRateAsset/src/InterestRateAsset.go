package main

import (
	"encoding/json"
	"errors"
	"fmt"
	"strings"

	"github.com/hyperledger/fabric/core/chaincode/shim"
	"github.com/hyperledger/fabric/protos/peer"
)

var logger = shim.NewLogger("CLDChaincode")

//InterestRateAsset - Chaincode for asset InterestRate
type InterestRateAsset struct {
}

//InterestRate - Details of the asset type InterestRate
type InterestRate struct {
	ID           string `json:"ID"`
	FinancerID   string `json:"financerID"`
	ObjectType   string `json:"docType"`
	DaysToCash   string `json:"daysToCash"`
	DiscountRate string `json:"discountRate"`
}

//InterestRateIDIndex - Index on IDs for retrieval all InterestRates
type InterestRateIDIndex struct {
	IDs []string `json:"IDs"`
}

func main() {
	err := shim.Start(new(InterestRateAsset))
	if err != nil {
		fmt.Printf("Error starting InterestRateAsset chaincode function main(): %s", err)
	} else {
		fmt.Printf("Starting InterestRateAsset chaincode function main() executed successfully")
	}
}

//Init - The chaincode Init function: No  arguments, only initializes a ID array as Index for retrieval of all InterestRates
func (intRate *InterestRateAsset) Init(stub shim.ChaincodeStubInterface) peer.Response {
	var interestRateIDIndex InterestRateIDIndex
	record, _ := stub.GetState("interestRateIDIndex")
	//chaincode upgrade: Index might already exist, create new only if it does not
	if record == nil {
		bytes, _ := json.Marshal(interestRateIDIndex)
		stub.PutState("interestRateIDIndex", bytes)
	}
	return shim.Success(nil)
}

//Invoke - The chaincode Invoke function:
func (intRate *InterestRateAsset) Invoke(stub shim.ChaincodeStubInterface) peer.Response {
	function, args := stub.GetFunctionAndParameters()
	logger.Debug("function: ", function)
	switch function {
	case "addNewInterestRate":
		return intRate.addNewInterestRate(stub, args)
	case "updateInterestRate":
		return intRate.updateInterestRate(stub, args)
	case "removeInterestRate":
		return intRate.removeInterestRate(stub, args[0])
	case "removeAllInterestRates":
		return intRate.removeAllInterestRates(stub)
	case "readInterestRate":
		return intRate.readInterestRate(stub, args[0])
	case "readAllInterestRates":
		return intRate.readAllInterestRates(stub)
	default:
		return shim.Error("Received unknown function invocation")
	}
}

//Invoke Route: addNewInterestRate
func (intRate *InterestRateAsset) addNewInterestRate(stub shim.ChaincodeStubInterface, args []string) peer.Response {
	interestRate, err := getInterestRateFromArgs(args)
	if err != nil {
		return shim.Error("InterestRate Data is Corrupted")
	}
	interestRate.ObjectType = "Asset.InterestRate"
	record, err := stub.GetState(interestRate.ID)
	if record != nil {
		return shim.Error("This InterestRate already exists: " + interestRate.ID)
	}
	_, err = intRate.saveInterestRate(stub, interestRate)
	if err != nil {
		return shim.Error(err.Error())
	}
	_, err = intRate.updateInterestRateIDIndex(stub, interestRate)
	if err != nil {
		return shim.Error(err.Error())
	}
	return shim.Success(nil)
}

//Invoke Route: updateInterestRate
func (intRate *InterestRateAsset) updateInterestRate(stub shim.ChaincodeStubInterface, args []string) peer.Response {
	rateStructToUpdate, err := getInterestRateFromArgs(args)
	if err != nil {
		return shim.Error("Rate Data is corrupted")
	}
	record, err := stub.GetState(rateStructToUpdate.ID)
	if record == nil {
		return shim.Error("Rate with the specified ID not found")
	}
	_, err = intRate.saveInterestRate(stub, rateStructToUpdate)
	if err != nil {
		return shim.Error(err.Error())
	}
	return shim.Success(nil)
}

//Invoke Route: removeInterestRate
func (intRate *InterestRateAsset) removeInterestRate(stub shim.ChaincodeStubInterface, interestRateID string) peer.Response {
	_, err := intRate.deleteInterestRate(stub, interestRateID)
	if err != nil {
		return shim.Error(err.Error())
	}
	_, err = intRate.deleteInterestRateIDIndex(stub, interestRateID)
	if err != nil {
		return shim.Error(err.Error())
	}
	return shim.Success(nil)
}

//Invoke Route: removeAllInterestRates
func (intRate *InterestRateAsset) removeAllInterestRates(stub shim.ChaincodeStubInterface) peer.Response {
	var interestRateStructIDs InterestRateIDIndex
	bytes, err := stub.GetState("interestRateIDIndex")
	if err != nil {
		return shim.Error("removeAllInterestRates: Error getting interestRateIDIndex array")
	}
	err = json.Unmarshal(bytes, &interestRateStructIDs)
	if err != nil {
		return shim.Error("removeAllInterestRates: Error unmarshalling interestRateIDIndex array JSON")
	}
	if len(interestRateStructIDs.IDs) == 0 {
		return shim.Error("removeAllInterestRates: No interestRates to remove")
	}
	for _, interestRateStructParticipantID := range interestRateStructIDs.IDs {
		_, err = intRate.deleteInterestRate(stub, interestRateStructParticipantID)
		if err != nil {
			return shim.Error("Failed to remove InterestRate with ID: " + interestRateStructParticipantID)
		}
		_, err = intRate.deleteInterestRateIDIndex(stub, interestRateStructParticipantID)
		if err != nil {
			return shim.Error(err.Error())
		}
	}
	intRate.initHolder(stub)
	return shim.Success(nil)
}

//Query Route: readInterestRate
func (intRate *InterestRateAsset) readInterestRate(stub shim.ChaincodeStubInterface, interestRateID string) peer.Response {
	interestRateAsByteArray, err := intRate.retrieveInterestRate(stub, interestRateID)
	if err != nil {
		return shim.Error(err.Error())
	}
	return shim.Success(interestRateAsByteArray)
}

//Query Route: readAllInterestRates
func (intRate *InterestRateAsset) readAllInterestRates(stub shim.ChaincodeStubInterface) peer.Response {
	var interestRateIDs InterestRateIDIndex
	bytes, err := stub.GetState("interestRateIDIndex")
	if err != nil {
		return shim.Error("readAllInterestRates: Error getting interestRateIDIndex array")
	}
	err = json.Unmarshal(bytes, &interestRateIDs)
	if err != nil {
		return shim.Error("readAllInterestRates: Error unmarshalling interestRateIDIndex array JSON")
	}
	result := "["

	var interestRateAsByteArray []byte

	for _, interestRateID := range interestRateIDs.IDs {
		interestRateAsByteArray, err = intRate.retrieveInterestRate(stub, interestRateID)
		if err != nil {
			return shim.Error("Failed to retrieve interestRate with ID: " + interestRateID)
		}
		result += string(interestRateAsByteArray) + ","
	}
	if len(result) == 1 {
		result = "[]"
	} else {
		result = result[:len(result)-1] + "]"
	}
	return shim.Success([]byte(result))
}

//Helper: Save InterestRate
func (intRate *InterestRateAsset) saveInterestRate(stub shim.ChaincodeStubInterface, interestRate InterestRate) (bool, error) {
	bytes, err := json.Marshal(interestRate)
	if err != nil {
		return false, errors.New("Error converting interestRate record JSON")
	}
	err = stub.PutState(interestRate.ID, bytes)
	if err != nil {
		return false, errors.New("Error storing InterestRate record")
	}
	return true, nil
}

//Helper: InterestRate interestRateStruct //change template
func (interestRate *InterestRateAsset) deleteInterestRate(stub shim.ChaincodeStubInterface, interestRateID string) (bool, error) {
	_, err := interestRate.retrieveInterestRate(stub, interestRateID)
	if err != nil {
		return false, errors.New("InterestRate with ID: " + interestRateID + " not found")
	}
	err = stub.DelState(interestRateID)
	if err != nil {
		return false, errors.New("Error deleting InterestRate record")
	}
	return true, nil
}

//Helper: Update interestRate Holder - updates Index
func (intRate *InterestRateAsset) updateInterestRateIDIndex(stub shim.ChaincodeStubInterface, interestRate InterestRate) (bool, error) {
	var interestRateIDs InterestRateIDIndex
	bytes, err := stub.GetState("interestRateIDIndex")
	if err != nil {
		return false, errors.New("updateInterestRateIDIndex: Error getting interestRateIDIndex array Index from state")
	}
	err = json.Unmarshal(bytes, &interestRateIDs)
	if err != nil {
		return false, errors.New("updateInterestRateIDIndex: Error unmarshalling interestRateIDIndex array JSON")
	}
	interestRateIDs.IDs = append(interestRateIDs.IDs, interestRate.ID)
	bytes, err = json.Marshal(interestRateIDs)
	if err != nil {
		return false, errors.New("updateInterestRateIDIndex: Error marshalling new interestRate ID")
	}
	err = stub.PutState("interestRateIDIndex", bytes)
	if err != nil {
		return false, errors.New("updateInterestRateIDIndex: Error storing new interestRate ID in interestRateIDIndex (Index)")
	}
	return true, nil
}

//Helper: delete ID from interestRateStruct Holder
func (intRate *InterestRateAsset) deleteInterestRateIDIndex(stub shim.ChaincodeStubInterface, interestRateID string) (bool, error) {
	var interestRateStructIDs InterestRateIDIndex
	bytes, err := stub.GetState("interestRateIDIndex")
	if err != nil {
		return false, errors.New("deleteInterestRateIDIndex: Error getting interestRateIDIndex array Index from state")
	}
	err = json.Unmarshal(bytes, &interestRateStructIDs)
	if err != nil {
		return false, errors.New("deleteInterestRateIDIndex: Error unmarshalling interestRateIDIndex array JSON")
	}
	interestRateStructIDs.IDs, err = deleteKeyFromStringArray(interestRateStructIDs.IDs, interestRateID)
	if err != nil {
		return false, errors.New(err.Error())
	}
	bytes, err = json.Marshal(interestRateStructIDs)
	if err != nil {
		return false, errors.New("deleteInterestRateIDIndex: Error marshalling new interestRateStruct ID")
	}
	err = stub.PutState("interestRateIDIndex", bytes)
	if err != nil {
		return false, errors.New("deleteInterestRateIDIndex: Error storing new interestRateStruct ID in interestRateIDIndex (Index)")
	}
	return true, nil
}

//Helper: Initialize truck ID Holder //change template
func (intRate *InterestRateAsset) initHolder(stub shim.ChaincodeStubInterface) bool {
	var interestRateIDIndex InterestRateIDIndex
	bytes, _ := json.Marshal(interestRateIDIndex)
	stub.DelState("interestRateIDIndex")
	stub.PutState("interestRateIDIndex", bytes)
	return true
}

//deleteKeyFromArray
func deleteKeyFromStringArray(array []string, key string) (newArray []string, err error) {
	for _, entry := range array {
		if entry != key {
			newArray = append(newArray, entry)
		}
	}
	if len(newArray) == len(array) {
		return newArray, errors.New("Specified Key: " + key + " not found in Array")
	}
	return newArray, nil
}

//Helper: Retrieve purchaser
func (intRate *InterestRateAsset) retrieveInterestRate(stub shim.ChaincodeStubInterface, interestRateID string) ([]byte, error) {
	var interestRate InterestRate
	var interestRateAsByteArray []byte
	bytes, err := stub.GetState(interestRateID)
	if err != nil {
		return interestRateAsByteArray, errors.New("retrieveInterestRate: Error retrieving interestRate with ID: " + interestRateID)
	}
	err = json.Unmarshal(bytes, &interestRate)
	if err != nil {
		return interestRateAsByteArray, errors.New("retrieveInterestRate: Corrupt interestRate record " + string(bytes))
	}
	interestRateAsByteArray, err = json.Marshal(interestRate)
	if err != nil {
		return interestRateAsByteArray, errors.New("readInterestRate: Invalid interestRate Object - Not a  valid JSON")
	}
	return interestRateAsByteArray, nil
}

//getRateFromArgs - construct a rate structure from string array of arguments
func getInterestRateFromArgs(args []string) (sample InterestRate, err error) {

	if strings.Contains(args[0], "\"ID\"") == false ||
		strings.Contains(args[0], "\"financerID\"") == false ||
		strings.Contains(args[0], "\"docType\"") == false ||
		strings.Contains(args[0], "\"daysToCash\"") == false ||
		strings.Contains(args[0], "\"discountRate\"") == false {
		return sample, errors.New("Unknown field: " + "Input JSON does not comply to schema")
	}

	err = json.Unmarshal([]byte(args[0]), &sample)
	if err != nil {
		return sample, err
	}
	return sample, nil

}
