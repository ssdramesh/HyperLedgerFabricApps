package main

import (
	"bytes"
	"encoding/json"
	"fmt"
	"strings"
	"testing"

	"github.com/hyperledger/fabric/core/chaincode/shim"
)

//TestInterestRateAsset_Init
func TestInterestRateAsset_Init(t *testing.T) {
	interestRate := new(InterestRateAsset)
	stub := shim.NewMockStub("interestRate", interestRate)
	checkInit(t, stub, [][]byte{[]byte("init")})
	checkState(t, stub, "interestRateIDIndex", getExpectedInterestRateIDIndex("init"))
}

//TestInterestRateAsset_InvokeUnknownFunction
func TestInterestRateAsset_InvokeUnknownFunction(t *testing.T) {
	interestRate := new(InterestRateAsset)
	stub := shim.NewMockStub("interestRate", interestRate)
	checkInit(t, stub, [][]byte{[]byte("init")})
	checkInvokeUnknownFunction(t, stub, [][]byte{[]byte("myFunction"), []byte("docType:Asset")})
}

//TestInterestRateAsset_Invoke_addNewInterestRate
func TestInterestRateAsset_Invoke_addNewInterestRateOK(t *testing.T) {
	interestRate := new(InterestRateAsset)
	stub := shim.NewMockStub("interestRate", interestRate)
	checkInit(t, stub, [][]byte{[]byte("init")})
	checkInvoke(t, stub, getFirstInterestRateAssetForTesting())
	newInterestRateID := "RA100001"
	checkState(t, stub, newInterestRateID, getNewInterestRateExpected())
	checkState(t, stub, "interestRateIDIndex", getExpectedInterestRateIDIndex("addNewInterestRate"))
}

//TestInterestRateAsset_Invoke_addNewInterestRateUnknownField
func TestInterestRateAsset_Invoke_addNewInterestRateUnknownField(t *testing.T) {
	interestRate := new(InterestRateAsset)
	stub := shim.NewMockStub("interestRate", interestRate)
	checkInit(t, stub, [][]byte{[]byte("init")})
	res := stub.MockInvoke("1", getInterestRateAssetWithUnknownFieldForTesting())
	if res.Status != shim.OK {
		checkError(t, "InterestRate Data is Corrupted", res.Message)
	} else {
		fmt.Println("Unknown Field Error was expected, but not raised")
		t.FailNow()
	}
}

//TestInterestRateAsset_Invoke_addNewInterestRate
func TestInterestRateAsset_Invoke_addNewInterestRateDuplicate(t *testing.T) {
	interestRate := new(InterestRateAsset)
	stub := shim.NewMockStub("interestRate", interestRate)
	checkInit(t, stub, [][]byte{[]byte("init")})
	checkInvoke(t, stub, getFirstInterestRateAssetForTesting())
	newInterestRateID := "RA100001"
	checkState(t, stub, newInterestRateID, getNewInterestRateExpected())
	checkState(t, stub, "interestRateIDIndex", getExpectedInterestRateIDIndex("addNewInterestRate"))
	res := stub.MockInvoke("1", getFirstInterestRateAssetForTesting())
	if res.Status != shim.OK {
		checkError(t, "This InterestRate already exists: RA100001", res.Message)
	} else {
		fmt.Println("Error was expected, but not raised")
		t.FailNow()
	}
}

//TestRateAsset_Invoke_updateRateOK
func TestRateAsset_Invoke_updateInterestRateOK(t *testing.T) {
	rate := new(InterestRateAsset)
	stub := shim.NewMockStub("rate", rate)
	checkInit(t, stub, [][]byte{[]byte("init")})
	checkInvoke(t, stub, getFirstInterestRateAssetForTesting())
	newRateID := "RA100001"
	checkState(t, stub, newRateID, getNewInterestRateExpected())
	checkState(t, stub, "interestRateIDIndex", getExpectedInterestRateIDIndex("addNewInterestRate"))
	res := stub.MockInvoke("1", getInterestRateAssetForUpdateOKTesting())
	checkState(t, stub, newRateID, getUpdatedInterestRateExpected())
	checkState(t, stub, "interestRateIDIndex", getExpectedInterestRateIDIndex("updateInterestRate"))
	if res.Status != shim.OK {
		fmt.Println("Unknown error in updateInterestRate")
		t.FailNow()
	}
}

//TestRateAsset_Invoke_updateRateNOK
func TestRateAsset_Invoke_updateInterestRateNOK(t *testing.T) {
	rate := new(InterestRateAsset)
	stub := shim.NewMockStub("rate", rate)
	checkInit(t, stub, [][]byte{[]byte("init")})
	checkInvoke(t, stub, getFirstInterestRateAssetForTesting())
	newRateID := "RA100001"
	checkState(t, stub, newRateID, getNewInterestRateExpected())
	checkState(t, stub, "interestRateIDIndex", getExpectedInterestRateIDIndex("addNewInterestRate"))
	res := stub.MockInvoke("1", getInterestRateAssetForUpdateNOKTesting())
	if res.Status != shim.OK {
		checkError(t, "Rate with the specified ID not found", res.Message)
	} else {
		fmt.Println("Error was expected, but not raised")
		t.FailNow()
	}
}

//TestInterestRateAsset_Invoke_removeInterestRateOK  //change template
func TestInterestRateAsset_Invoke_removeInterestRateOK(t *testing.T) {
	interestRate := new(InterestRateAsset)
	stub := shim.NewMockStub("interestRate", interestRate)
	checkInit(t, stub, [][]byte{[]byte("init")})
	checkInvoke(t, stub, getFirstInterestRateAssetForTesting())
	checkInvoke(t, stub, getSecondInterestRateAssetForTesting())
	checkReadAllInterestRatesOK(t, stub)
	checkState(t, stub, "interestRateIDIndex", getExpectedInterestRateIDIndex("beforeRemoveInterestRate"))
	checkInvoke(t, stub, getRemoveSecondInterestRateAssetForTesting())
	remainingInterestRateID := "RA100001"
	checkReadInterestRateOK(t, stub, remainingInterestRateID)
	checkState(t, stub, "interestRateIDIndex", getExpectedInterestRateIDIndex("afterRemoveInterestRate"))
}

//TestInterestRateAsset_Invoke_removeInterestRateNOK
func TestInterestRateAsset_Invoke_removeInterestRateNOK(t *testing.T) {
	interestRate := new(InterestRateAsset)
	stub := shim.NewMockStub("interestRate", interestRate)
	checkInit(t, stub, [][]byte{[]byte("init")})
	checkInvoke(t, stub, getFirstInterestRateAssetForTesting())
	firstInterestRateID := "RA100001"
	checkReadInterestRateOK(t, stub, firstInterestRateID)
	checkState(t, stub, "interestRateIDIndex", getExpectedInterestRateIDIndex("addNewInterestRate"))
	res := stub.MockInvoke("1", getRemoveSecondInterestRateAssetForTesting())
	if res.Status != shim.OK {
		checkError(t, "InterestRate with ID: "+"RA100002"+" not found", res.Message)
	} else {
		fmt.Println("Error was expected, but not raised")
		t.FailNow()
	}
	checkState(t, stub, "interestRateIDIndex", getExpectedInterestRateIDIndex("addNewInterestRate"))
}

//TestInterestRateAsset_Invoke_removeAllInterestRatessOK
func TestInterestRateAsset_Invoke_removeAllInterestRatessOK(t *testing.T) {
	financier := new(InterestRateAsset)
	stub := shim.NewMockStub("financier", financier)
	checkInit(t, stub, [][]byte{[]byte("init")})
	checkInvoke(t, stub, getFirstInterestRateAssetForTesting())
	checkInvoke(t, stub, getSecondInterestRateAssetForTesting())
	checkReadAllInterestRatesOK(t, stub)
	checkState(t, stub, "interestRateIDIndex", getExpectedInterestRateIDIndex("beforeRemoveInterestRate"))
	checkInvoke(t, stub, getRemoveAllInterestRateAssetsForTesting())
	checkState(t, stub, "interestRateIDIndex", getExpectedInterestRateIDIndex(""))
}

//TestInterestRateAsset_Query_readInterestRate
func TestInterestRateAsset_Query_readInterestRate(t *testing.T) {
	interestRate := new(InterestRateAsset)
	stub := shim.NewMockStub("interestRate", interestRate)
	checkInit(t, stub, [][]byte{[]byte("init")})
	interestRateID := "RA100001"
	checkInvoke(t, stub, getFirstInterestRateAssetForTesting())
	checkReadInterestRateOK(t, stub, interestRateID)
	checkReadInterestRateNOK(t, stub, "")
}

//TestInterestRateAsset_Query_readAllInterestRates
func TestInterestRateAsset_Query_readAllInterestRates(t *testing.T) {
	interestRate := new(InterestRateAsset)
	stub := shim.NewMockStub("interestRate", interestRate)
	checkInit(t, stub, [][]byte{[]byte("init")})
	checkInvoke(t, stub, getFirstInterestRateAssetForTesting())
	checkInvoke(t, stub, getSecondInterestRateAssetForTesting())
	checkReadAllInterestRatesOK(t, stub)
}

/*
*
*	Helper Functions
*
 */
//Get first InterestRateAsset for testing
func getFirstInterestRateAssetForTesting() [][]byte {
	return [][]byte{[]byte("addNewInterestRate"),
		[]byte("{\"ID\":\"RA100001\"," +
			"\"financerID\":\"FI100001\"," +
			"\"docType\":\"Asset.InterestRate\"," +
			"\"daysToCash\":\"15\"," +
			"\"discountRate\":\"2.82\"}")}
}

//Get InterestRateAsset for Update OK testing
func getInterestRateAssetForUpdateOKTesting() [][]byte {
	return [][]byte{[]byte("updateInterestRate"),
		[]byte("{\"ID\":\"RA100001\"," +
			"\"financerID\":\"FI100001\"," +
			"\"docType\":\"Asset.InterestRate\"," +
			"\"daysToCash\":\"15\"," +
			"\"discountRate\":\"2.79\"}")}
}

//Get InterestRateAsset for Update NOK testing
func getInterestRateAssetForUpdateNOKTesting() [][]byte {
	return [][]byte{[]byte("updateInterestRate"),
		[]byte("{\"ID\":\"RA100003\"," +
			"\"financerID\":\"FI100001\"," +
			"\"docType\":\"Asset.InterestRate\"," +
			"\"daysToCash\":\"15\"," +
			"\"discountRate\":\"2.79\"}")}
}

//Get InterestRateAsset with unknown field for testing
func getInterestRateAssetWithUnknownFieldForTesting() [][]byte {
	return [][]byte{[]byte("addNewInterestRate"),
		[]byte("{\"ID\":\"RA100001\"," +
			"\"financerID\":\"FI100001\"," +
			"\"docuType\":\"Asset.InterestRate\"," +
			"\"daysToCash\":\"15\"," +
			"\"discountRate\":\"2.79\"}")}
}

//Get second InterestRateAsset for testing
func getSecondInterestRateAssetForTesting() [][]byte {
	return [][]byte{[]byte("addNewInterestRate"),
		[]byte("{\"ID\":\"RA100002\"," +
			"\"financerID\":\"FI100001\"," +
			"\"docType\":\"Asset.InterestRate\"," +
			"\"daysToCash\":\"30\"," +
			"\"discountRate\":\"2.72\"}")}
}

//Get remove second InterestRateAsset for testing //change template
func getRemoveSecondInterestRateAssetForTesting() [][]byte {
	return [][]byte{[]byte("removeInterestRate"),
		[]byte("RA100002")}
}

//Get remove all InterestRateAssets for testing
func getRemoveAllInterestRateAssetsForTesting() [][]byte {
	return [][]byte{[]byte("removeAllInterestRates")}
}

//Get an expected value for testing
func getNewInterestRateExpected() []byte {
	var rate InterestRate
	rate.ID = "RA100001"
	rate.FinancerID = "FI100001"
	rate.ObjectType = "Asset.InterestRate"
	rate.DaysToCash = "15"
	rate.DiscountRate = "2.82"
	rateJSON, err := json.Marshal(rate)
	if err != nil {
		fmt.Println("Error converting a Rate record to JSON")
		return nil
	}
	return []byte(rateJSON)
}

//Get an expected value for testing
func getUpdatedInterestRateExpected() []byte {
	var rate InterestRate
	rate.ID = "RA100001"
	rate.FinancerID = "FI100001"
	rate.ObjectType = "Asset.InterestRate"
	rate.DaysToCash = "15"
	rate.DiscountRate = "2.79"
	rateJSON, err := json.Marshal(rate)
	if err != nil {
		fmt.Println("Error converting a Rate record to JSON")
		return nil
	}
	return []byte(rateJSON)
}

//Get expected values of InterestRates for testing
func getExpectedInterestRates() []byte {
	var rates []InterestRate
	var rate InterestRate
	rate.ID = "RA100001"
	rate.FinancerID = "FI100001"
	rate.ObjectType = "Asset.InterestRate"
	rate.DaysToCash = "15"
	rate.DiscountRate = "2.82"
	rates = append(rates, rate)
	rate.ID = "RA100002"
	rate.FinancerID = "FI100001"
	rate.ObjectType = "Asset.InterestRate"
	rate.DaysToCash = "30"
	rate.DiscountRate = "2.72"
	rates = append(rates, rate)
	rateJSON, err := json.Marshal(rates)
	if err != nil {
		fmt.Println("Error converting rate records to JSON")
		return nil
	}
	return []byte(rateJSON)
}

func getExpectedInterestRateIDIndex(funcName string) []byte {
	var interestRateIDIndex InterestRateIDIndex
	switch funcName {
	case "addNewInterestRate":
		interestRateIDIndex.IDs = append(interestRateIDIndex.IDs, "RA100001")
		interestRateIDIndexBytes, err := json.Marshal(interestRateIDIndex)
		if err != nil {
			fmt.Println("Error converting InterestRateIDIndex to JSON")
			return nil
		}
		return interestRateIDIndexBytes
	case "updateInterestRate":
		interestRateIDIndex.IDs = append(interestRateIDIndex.IDs, "RA100001")
		interestRateIDIndexBytes, err := json.Marshal(interestRateIDIndex)
		if err != nil {
			fmt.Println("Error converting InterestRateIDIndex to JSON")
			return nil
		}
		return interestRateIDIndexBytes
	case "beforeRemoveInterestRate":
		interestRateIDIndex.IDs = append(interestRateIDIndex.IDs, "RA100001")
		interestRateIDIndex.IDs = append(interestRateIDIndex.IDs, "RA100002")
		interestRateIDIndexBytes, err := json.Marshal(interestRateIDIndex)
		if err != nil {
			fmt.Println("Error converting InterestRateIDIndex to JSON")
			return nil
		}
		return interestRateIDIndexBytes
	case "afterRemoveInterestRate":
		interestRateIDIndex.IDs = append(interestRateIDIndex.IDs, "RA100001")
		interestRateIDIndexBytes, err := json.Marshal(interestRateIDIndex)
		if err != nil {
			fmt.Println("Error converting InterestRateIDIndex to JSON")
			return nil
		}
		return interestRateIDIndexBytes
	default:
		interestRateIDIndexBytes, err := json.Marshal(interestRateIDIndex)
		if err != nil {
			fmt.Println("Error converting InterestRateIDIndex to JSON")
			return nil
		}
		return interestRateIDIndexBytes
	}
}

//checkInit - helper to check the Initialization of chaincode: InterestRateAsset
func checkInit(t *testing.T, stub *shim.MockStub, args [][]byte) {
	res := stub.MockInit("1", args)
	if res.Status != shim.OK {
		fmt.Println("Init failed", string(res.Message))
		t.FailNow()
	}
}

//checkState - helper for checking the chaincode state for a given stateKey afgainst an expected value
func checkState(t *testing.T, stub *shim.MockStub, stateKey string, expectedState []byte) {
	actualState := stub.State[stateKey]
	if actualState == nil {
		fmt.Println("State for ", stateKey, ": failed to get value")
		t.FailNow()
	}

	if bytes.Compare(actualState, expectedState) != 0 {
		fmt.Println("Incorrect State for " + stateKey + ": \nExpected: " + string(expectedState) + "\nActual  : " + string(actualState))
		t.FailNow()
	}
}

//checkInvoke - helper for checking Invoke of chaincode
func checkInvoke(t *testing.T, stub *shim.MockStub, args [][]byte) {
	res := stub.MockInvoke("1", args)
	if res.Status != shim.OK {
		fmt.Println("Invoke", args, "failed", string(res.Message))
		t.FailNow()
	}
}

func checkInvokeUnknownFunction(t *testing.T, stub *shim.MockStub, args [][]byte) {
	res := stub.MockInvoke("1", args)
	if res.Status != shim.OK {
		expectedErr := "Received unknown function invocation"
		actualErr := string(res.Message)
		if !(strings.Contains(actualErr, expectedErr)) {
			fmt.Println("Invoke with unknown function must throw error: ", expectedErr, "\n Actual Error :", actualErr)
			t.FailNow()
		}
	} else {
		t.FailNow()
	}
}

//checkReadInterestRateOK - helper for positive test readInterestRate
func checkReadInterestRateOK(t *testing.T, stub *shim.MockStub, interestRateID string) {
	res := stub.MockInvoke("1", [][]byte{[]byte("readInterestRate"), []byte(interestRateID)})
	if res.Status != shim.OK {
		fmt.Println("func readInterestRate with ID: ", interestRateID, " failed"+string(res.Message))
		t.FailNow()
	}
	if res.Payload == nil {
		fmt.Println("func readInterestRate with ID: ", interestRateID, "failed to get value")
		t.FailNow()
	}
	if bytes.Compare(getNewInterestRateExpected(), []byte(res.Payload)) != 0 {
		fmt.Println("func readInterestRate with ID: ", interestRateID, "Expected:", string(getNewInterestRateExpected()), "Actual:", string(res.Payload))
		t.FailNow()
	}
}

//checkReadInterestRateNOK - helper for negative testing of readInterestRate
func checkReadInterestRateNOK(t *testing.T, stub *shim.MockStub, interestRateID string) {
	//with no interestRateID
	res := stub.MockInvoke("1", [][]byte{[]byte("readInterestRate"), []byte("")})
	if res.Status != shim.OK {
		expectedErr := "retrieveInterestRate: Corrupt interestRate record "
		actualErr := string(res.Message)
		if !(strings.Contains(actualErr, expectedErr)) {
			fmt.Println("func readInterestRate negative test: ", "Expected Error:", expectedErr, "Actual Error", actualErr)
			t.FailNow()
		}
	} else {
		t.FailNow()
	}
}

func checkReadAllInterestRatesOK(t *testing.T, stub *shim.MockStub) {
	res := stub.MockInvoke("1", [][]byte{[]byte("readAllInterestRates")})
	if res.Status != shim.OK {
		fmt.Println("func readAllInterestRates failed", string(res.Message))
		t.FailNow()
	}
	if res.Payload == nil {
		fmt.Println("func readAllInterestRates failed to get value")
		t.FailNow()
	}
	if bytes.Compare(getExpectedInterestRates(), []byte(res.Payload)) != 0 {
		fmt.Println("func readAllInterestRates Expected:\n", string(getExpectedInterestRates()), "\nActual:\n", string(res.Payload))
		t.FailNow()
	}
}

func checkError(t *testing.T, exp string, act string) {
	if strings.Compare(exp, act) != 0 {
		fmt.Println("Unexpected Error! Expecting ", exp, "\n Actual :", act)
		t.FailNow()
	}
}
