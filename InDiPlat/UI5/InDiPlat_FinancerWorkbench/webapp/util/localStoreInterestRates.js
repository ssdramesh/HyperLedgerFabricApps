sap.ui.define(function() {
	"use strict";

	jQuery.sap.require("jquery.sap.storage");
	var oStorage = jQuery.sap.storage(jQuery.sap.storage.Type.local);
	var sampleDataID = "interestRates";

	return {

		init: function() {

			this.removeAll();
			oStorage.put(
				sampleDataID, 
				[
					{
						"ID"			:	"RT100001",
						"financerID"	:	"FI100001",
						"docType"		:	"Asset.InterestRate",
						"daysToCash"	:	"15",
						"discountRate"	:	"1.5"
					},
					{
						"ID"			:	"RT100002",
						"financerID"	:	"FI100001",
						"docType"		:	"Asset.InterestRate",
						"daysToCash"	:	"30",
						"discountRate"	:	"1.75"
					},
					{
						"ID"			:	"RT100003",
						"financerID"	:	"FI100001",
						"docType"		:	"Asset.InterestRate",
						"daysToCash"	:	"45",
						"discountRate"	:	"2.00"
					},
					{
						"ID"			:	"RT100004",
						"financerID"	:	"FI100001",
						"docType"		:	"Asset.InterestRate",
						"daysToCash"	:	"60",
						"discountRate"	:	"2.25"
					},
					{
						"ID"			:	"RT100005",
						"financerID"	:	"FI100001",
						"docType"		:	"Asset.InterestRate",
						"daysToCash"	:	"75",
						"discountRate"	:	"2.5"
					},
					{
						"ID"			:	"RT100006",
						"financerID"	:	"FI100001",
						"docType"		:	"Asset.InterestRate",
						"daysToCash"	:	"90",
						"discountRate"	:	"2.75"
					},
					{
						"ID"			:	"RT100007",
						"financerID"	:	"FI100002",
						"docType"		:	"Asset.InterestRate",
						"daysToCash"	:	"15",
						"discountRate"	:	"1.15"
					},
					{
						"ID"			:	"RT100008",
						"financerID"	:	"FI100002",
						"docType"		:	"Asset.InterestRate",
						"daysToCash"	:	"30",
						"discountRate"	:	"1.30"
					},
					{
						"ID"			:	"RT100009",
						"financerID"	:	"FI100002",
						"docType"		:	"Asset.InterestRate",
						"daysToCash"	:	"45",
						"discountRate"	:	"1.45"
					},
					{
						"ID"			:	"RT100010",
						"financerID"	:	"FI100002",
						"docType"		:	"Asset.InterestRate",
						"daysToCash"	:	"60",
						"discountRate"	:	"1.60"
					},
					{
						"ID"			:	"RT100011",
						"financerID"	:	"FI100002",
						"docType"		:	"Asset.InterestRate",
						"daysToCash"	:	"75",
						"discountRate"	:	"1.75"
					},
					{
						"ID"			:	"RT100012",
						"financerID"	:	"FI100002",
						"docType"		:	"Asset.InterestRate",
						"daysToCash"	:	"90",
						"discountRate"	:	"2.00"
					}
				]
			);
		},

		getSampleDataID: function() {

			return sampleDataID;
		},

		getSampleData: function() {

			return oStorage.get("interestRates");
		},

		put: function(newFinancer) {

			var sampleData = this.getSampleData();
			sampleData.push(newFinancer);
			oStorage.put(sampleDataID, sampleData);
		},
		
		updateInterestRate:function(ID, financerID, docType, daysToCash, discountRate){
		
			var interestRate = _.findWhere(this.getSampleData(), {ID:ID});
			interestRate.ID = ID;
			interestRate.financerID = financerID;
			interestRate.docType = docType;
			interestRate.daysToCash = daysToCash;
			interestRate.discountRate = discountRate;
			this.remove(ID);
			this.put(interestRate);
		},
		
		remove : function (id){
		
			var sampleData = this.getSampleData();
			sampleData = _.without(sampleData,_.findWhere(sampleData,{ID:id}));
			oStorage.put(sampleDataID, sampleData);
		},
		
		removeAll : function(){
		
			oStorage.put(sampleDataID,[]);	
		},		

		clearSampleData: function() {

			oStorage.clear();
		}
	};
});