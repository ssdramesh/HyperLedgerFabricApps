sap.ui.define(function() {
	"use strict";

	return {

		mapInterestRateToModel:function(responseData){
			return {
				ID						: responseData.ID,
				FinancerID				: responseData.financerID,
				ObjectType 				: responseData.docType,
				DaysToCash				: responseData.daysToCash,
				DiscountRate			: responseData.discountRate
			};
		},

		mapInterestRatesToModel:function(responseData){
			
			var items = [];
			if( responseData ){
				for ( var i = 0; i < responseData.length; i++ ){
					items.push(this.mapInterestRateToModel(responseData[i]));
				}
			}
			return items;
		},

		mapInterestRateToChaincode:function(oModel, newInterestRate){
			
			if ( newInterestRate=== true ) {
				return {
						"ID"					:	this.getNewInterestRateID(oModel.getProperty("/newInterestRate/ID")),
						"financerID"			:	oModel.getProperty("/newInterestRate/FinancerID"),
						"docType"				:	"Asset.InterestRate",
						"daysToCash"			:	oModel.getProperty("/newInterestRate/DaysToCash"),
						"discountRate"			:	oModel.getProperty("/newInterestRate/DiscountRate")
				};
			} else {
				return {
						"ID"					:	oModel.getProperty("/selectedInteresRate/ID"),
						"financerID"			:	oModel.getProperty("/selectedInteresRate/FinancerID"),
						"docType"				:	oModel.getProperty("/selectedInteresRate/ObjectType"),
						"daysToCash"			:	oModel.getProperty("/selectedInteresRate/DaysToCash"),
						"discountRate"			:	oModel.getProperty("/selectedInteresRate/DiscountRate")
				};				
			}
		},
		
		mapInterestRateToLocalStorage : function(oModel){
				
			return {
					"ID"					:	this.getNewInterestRateID(oModel.getProperty("/newInterestRate/ID")),
					"financerID"			:	oModel.getProperty("/newInterestRate/FinancerID"),
					"docType"				:	"Asset.InterestRate",
					"daysToCash"			:	oModel.getProperty("/newInterestRate/DaysToCash"),
					"discountRate"			:	oModel.getProperty("/newInterestRate/DiscountRate")
			};
		},
		
		getNewPurchaseOrderID:function(oModel){

			if ( typeof oModel.getProperty("/newInterestRate/ID") === "undefined" ||
		    		oModel.getProperty("/newInterestRate/ID") === ""
		    	){
			    var iD = "RT";
				var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
			    for( var i = 0; i < 8; i++ ){
			        iD += possible.charAt(Math.floor(Math.random() * possible.length));
			    }
		    } else {
				iD = oModel.getProperty("/newInterestRate/ID");
			}
			oModel.setProperty("/newInterestRate/ID",iD);
		    return iD;
		}
	};
});