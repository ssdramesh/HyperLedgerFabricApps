sap.ui.define(function() {
	"use strict";

	jQuery.sap.require("jquery.sap.storage");
	var oStorage = jQuery.sap.storage(jQuery.sap.storage.Type.local);
	var sampleDataID = "financingOffers";

	return {

		init: function() {

			this.removeAll();
			oStorage.put(
				sampleDataID, 
					[
						{
						    "ID": "FO100001",
						    "supplierID": "SU100001",
						    "purchaserID": "PU100001",
						    "financerID": "FI100001",
						    "invoiceID": "IN100001",
						    "purchaseOrderID": "PO100001",
						    "docType": "Asset.FinancingOffer",
						    "status": "Submitted",
						    "invoiceGrossAmount": "6754.34",
						    "discountPercent": "1.75",
						    "discountAmount": "118.2",
						    "fundedAmount": "6636.14",
						    "offerValidityStartDate": "01/03/2017",
						    "offerValidityEndDate": "01/10/2017",
						    "disbursementDate": "",
						    "disbursementReference": "",
						    "currency": "SGD"
						  },
						  {
						    "ID": "FO100002",
						    "supplierID": "SU100002",
						    "purchaserID": "PU100002",
						    "financerID": "FI100002",
						    "invoiceID": "IN100002",
						    "purchaseOrderID": "PO100002",
						    "docType": "Asset.FinancingOffer",
						    "status": "Submitted",
						    "invoiceGrossAmount": "5674.50",
						    "discountPercent": "1.45",
						    "discountAmount": "82.28",
						    "fundedAmount": "5592.22",
						    "offerValidityStartDate": "02/05/2017",
						    "offerValidityEndDate": "02/12/2017",
						    "disbursementDate": "",
						    "disbursementReference": "",
						    "currency": "SGD"
						  }
					]
			);
		},

		getSampleDataID: function() {

			return sampleDataID;
		},

		getSampleData: function() {

			return oStorage.get("financingOffers");
		},

		put: function(newFinancingOffer) {

			var sampleData = this.getSampleData();
			sampleData.push(newFinancingOffer);
			oStorage.put(sampleDataID, sampleData);
		},
		
		setFinancingOfferSubmitted:function(financingOfferID){
		
			var financingOffer = _.findWhere(this.getSampleData(), {ID:financingOfferID});
			financingOffer.status = "Submitted";
			this.remove(financingOfferID);
			this.put(financingOffer);
		},
		
		setFinancingOfferAccepted:function(financingOfferID){
		
			var financingOffer = _.findWhere(this.getSampleData(), {ID:financingOfferID});
			financingOffer.status = "Accepted";
			this.remove(financingOfferID);
			this.put(financingOffer);			
		},
		
		setFinancingOfferDeclined:function(financingOfferID){
		
			var financingOffer = _.findWhere(this.getSampleData(), {ID:financingOfferID});
			financingOffer.status = "Declined";
			this.remove(financingOfferID);
			this.put(financingOffer);			
		},
		
		setFinancingOfferDisbursementInitiated:function(financingOfferID, disbursementDate, disbursementReference){
		
			var financingOffer = _.findWhere(this.getSampleData(), {ID:financingOfferID});
			financingOffer.status = "Disbursement Initiated";
			financingOffer.disbursementDate = disbursementDate;
			financingOffer.disbursementReference = disbursementReference;
			this.remove(financingOfferID);
			this.put(financingOffer);			
		},
		
		remove : function (id){
		
			var sampleData = this.getSampleData();
			sampleData = _.without(sampleData,_.findWhere(sampleData,{ID:id}));
			oStorage.put(sampleDataID, sampleData);
		},
		
		removeAll : function(){
		
			oStorage.put(sampleDataID,[]);	
		},		

		clearSampleData: function() {

			oStorage.clear();
		}
	};
});