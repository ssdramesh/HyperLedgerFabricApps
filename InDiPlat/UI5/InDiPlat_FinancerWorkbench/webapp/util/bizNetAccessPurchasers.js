sap.ui.define([
	"Workbench/util/restBuilder",
	"Workbench/util/formatterPurchasers",
	"Workbench/util/localStorePurchasers"
], function(
		restBuilder, 
		formatterPurchasers, 
		localStorePurchasers
	) {
	"use strict";

	return {

		loadAllPurchasers:function(oComponent, oModel) {

			if ( !( oComponent.getModel("TestSwitch").getProperty("/testMode") ) ){
				restBuilder.execute(
					oModel.getProperty("/chaincode"), 
					"readAll",
					[],
					function(responseData){
						oModel.setProperty(
							"/purchaserCollection/items", 
							formatterPurchasers.mapPurchasersToModel(responseData));
					});				
			} else {
				var sData = localStorePurchasers.getSampleData();
					oModel.setProperty("/purchaserCollection/items", formatterPurchasers.mapPurchasersToModel(sData));
			}
		},

		loadPurchaser:function(oModel, ID){

			oModel.setProperty(
				"/selectedPurchaser",
				_.findWhere(oModel.getProperty("/purchaserCollection/items"), { ID: ID }, this));
		},

		addNewPurchaser:function(oComponent, oModel){
			
			if ( !( oComponent.getModel("TestSwitch").getProperty("/testMode") ) ){
				restBuilder.execute(
					oModel.getProperty("/chaincode"), 
					"addNew",
					formatterPurchasers.mapPurchaserToChaincode(oModel, true)
				);
			}  else {
				localStorePurchasers.put(formatterPurchasers.mapPurchaserToLocalStorage(oModel));
			}
			this.loadAllPurchasers(oComponent, oModel);
			return oModel.getProperty("/newPurchaser/ID");
		},

		removePurchaser : function(oComponent, oModel, purchaserID){
			
			if ( !( oComponent.getModel("TestSwitch").getProperty("/testMode") ) ) {
				restBuilder.execute(
					oModel.getProperty("/chaincode"), 
					"remove",
					[purchaserID]
				);
			} else {
				localStorePurchasers.remove(purchaserID);
			}
			this.loadAllPurchasers(oComponent, oModel);
			return true;
		},
		
		removeAllPurchasers : function(oComponent, oModel){
			
			if ( !( oComponent.getModel("TestSwitch").getProperty("/testMode") ) ) {
				restBuilder.execute(
					oModel.getProperty("/chaincode"), 
					"removeAll",
					[]
				);
			} else {
				localStorePurchasers.removeAll();
			}
			this.loadAllPurchasers(oComponent, oModel);
			oModel.setProperty("/selectedPurchaser",{});
			return true;
		},
		
		getPurchaserFromAlias:function(oComponent, alias){
			
			return _.findWhere(
						oComponent.getModel("Purchasers").getProperty("/purchaserCollection/items"),
						{Alias:alias}, 
						this
					);
		}
	};	
});