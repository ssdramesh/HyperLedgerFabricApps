sap.ui.define([
	"Workbench/util/restBuilder",
	"Workbench/util/formatterFinancingRequests",
	"Workbench/util/localStoreFinancingRequests"
], function(
		restBuilder, 
		formatterFinancingRequests, 
		localStoreFinancingRequests
	) {

	"use strict";

	return {

		//In the case of supplier filter by supplier!!
		loadAllFinancingRequests:function(oComponent, oModel) {

			
			if ( !( oComponent.getModel("TestSwitch").getProperty("/testMode") ) ){
				restBuilder.execute(
					oModel.getProperty("/chaincode"), 
					"readAll",
					[],
					function(responseData){
						oModel.setProperty(
							"/financingRequestCollection/items", 
							_.filter(
								formatterFinancingRequests.mapFinancingRequestsToModel(responseData), 
								function(request){
									return (request.FinancerID === oComponent.getModel("Login").getProperty("/id") && request.Status !== "Created");
								},
								this
							)
						);
					}
				);				
			} else {
				var sData = localStoreFinancingRequests.getSampleData();
					oModel.setProperty(
						"/financingRequestCollection/items", 
							_.filter(
								formatterFinancingRequests.mapFinancingRequestsToModel(sData), 
								function(request){
									return (request.FinancerID === oComponent.getModel("Login").getProperty("/id") && request.Status !== "Created");
								},
								this
							)						
						);
			}
		},		

		loadFinancingRequest:function(oModel, selectedFinancingRequestID){

			oModel.setProperty(
				"/selectedFinancingRequest",
				_.findWhere(
					oModel.getProperty("/financingRequestCollection/items"), 
					{
						ID: selectedFinancingRequestID
					},
					this
				)
			);
		},

		addNewFinancingRequest:function(oComponent, oModel){
			
			if ( !( oComponent.getModel("TestSwitch").getProperty("/testMode") ) ){
				restBuilder.execute(
					oModel.getProperty("/chaincode"), 
					"addNew",
					formatterFinancingRequests.mapFinancingRequestToChaincode(oModel, true)
				);
			}  else {
				localStoreFinancingRequests.put(formatterFinancingRequests.mapFinancingRequestToLocalStorage(oModel));
			}
			this.loadAllFinancingRequests(oComponent, oModel);
			return oModel.getProperty("/newFinancingRequest/ID");
		},
		
		setFinancingRequestAccepted : function(oComponent, oModel){
			
			if ( !( oComponent.getModel("TestSwitch").getProperty("/testMode") ) ) {
				restBuilder.execute(
					oModel.getProperty("/chaincode"), 
					"accept",
					formatterFinancingRequests.mapFinancingRequestToChaincode(oComponent, oModel, false)
				);
			} else {
				localStoreFinancingRequests.setFinancingRequestAccepted(
					oModel.getProperty("/selectedFinancingRequest/ID"), 
					oModel.getProperty("/selectedFinancingRequest/RequestedDiscountPercent"), 
					oModel.getProperty("/selectedFinancingRequest/RequestedFinancingAmount")
				);
			}
			this.loadAllFinancingRequests(oComponent, oModel);
			return true;
		},		
		
		setFinancingRequestDeclined : function(oComponent, oModel){
		
			if ( !( oComponent.getModel("TestSwitch").getProperty("/testMode") ) ) {
				restBuilder.execute(
					oModel.getProperty("/chaincode"), 
					"declined",
					[oModel.getProperty("/selectedFinancingRequest/FinancingRequestID")]
				);
			} else {
				localStoreFinancingRequests.setFinancingRequestDeclined(oModel.getProperty("/selectedFinancingRequest/ID"));
			}
			this.loadAllFinancingRequests(oComponent, oModel);
			return true;			
		},
		
		setFinancingRequestDocumentsChecked : function(oComponent, oModel, financingRequestID){
		
			if ( !( oComponent.getModel("TestSwitch").getProperty("/testMode") ) ) {
				restBuilder.execute(
					oModel.getProperty("/chaincode"), 
					"check",
					formatterFinancingRequests.mapFinancingRequestToChaincode(oComponent, oModel, false)
				);
			} else {
				localStoreFinancingRequests.setFinancingRequestDocumentsChecked(financingRequestID);
			}
			this.loadAllFinancingRequests(oComponent, oModel);
			return true;			
		},
		
		setFinancingRequestDocumentsVerified : function(oComponent, oModel, financingRequestID){
		
			if ( !( oComponent.getModel("TestSwitch").getProperty("/testMode") ) ) {
				restBuilder.execute(
					oModel.getProperty("/chaincode"), 
					"verify",
					formatterFinancingRequests.mapFinancingRequestToChaincode(oComponent, oModel, false)
				);
			} else {
				localStoreFinancingRequests.setFinancingRequestDocumentsVerified(financingRequestID);
			}
			this.loadAllFinancingRequests(oComponent, oModel);
			return true;			
		},		
		
		removeFinancingRequest : function(oComponent, oModel, financingRequestID){
			
			if ( !( oComponent.getModel("TestSwitch").getProperty("/testMode") ) ) {
				restBuilder.execute(
					oModel.getProperty("/chaincode"), 
					"remove",
					[financingRequestID]
				);
			} else {
				localStoreFinancingRequests.remove(financingRequestID);
			}
			this.loadAllFinancingRequests(oComponent, oModel);
			return true;
		},
		
		removeAllFinancingRequests : function(oComponent, oModel){
			
			if ( !( oComponent.getModel("TestSwitch").getProperty("/testMode") ) ) {
				restBuilder.execute(
					oModel.getProperty("/chaincode"), 
					"removeAll",
					[]
				);
			} else {
				localStoreFinancingRequests.removeAll();
			}
			this.loadAllFinancingRequests(oComponent, oModel);
			oModel.setProperty("/selectedFinancingRequest",{});
			return true;
		}
	};	
});