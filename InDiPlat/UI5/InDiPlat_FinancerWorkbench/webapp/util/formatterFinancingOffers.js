sap.ui.define([
	"Workbench/util/bizNetAccessFinancers"
	],function(bizNetAccessFinancers) {
	"use strict";

	return {

		mapFinancingOfferToModel:function(responseData){
			return {
				ID							: responseData.ID,
				SupplierID					: responseData.supplierID,
				PurchaserID					: responseData.purchaserID,
				FinancerID					: responseData.financerID,
				InvoiceID					: responseData.invoiceID,
				PurchaseOrderID				: responseData.purchaseOrderID,
				ObjectType 					: responseData.docType,
				Status						: responseData.status,
				InvoiceGrossAmount			: responseData.invoiceGrossAmount,
				DiscountPercent				: responseData.discountPercent,
				DiscountAmount				: responseData.discountAmount,
				FundedAmount				: responseData.fundedAmount,
				OfferValidityStartDate		: responseData.offerValidityStartDate,
				OfferValidityEndDate		: responseData.offerValidityEndDate,
				DisbursementDate			: responseData.disbursementDate,
				DisbursementReference		: responseData.disbursementReference,
				Currency					: responseData.currency
			};
		},

		mapFinancingOffersToModel:function(responseData){
			
			var items = [];
			if( responseData ){
				for ( var i = 0; i < responseData.length; i++ ){
					items.push(this.mapFinancingOfferToModel(responseData[i]));
				}
			}
			return items;
		},

		mapFinancingOfferToChaincode:function(oComponent, oModel, newFinancingOffer){

			var fin = bizNetAccessFinancers.getFinancerFromAlias(oComponent, oComponent.getModel("Login").getProperty("/alias"));
			
			if ( newFinancingOffer === true ) {
				return {
						"ID"						:	this.getNewFinancingOfferID(oModel),
						"supplierID"				:	oModel.getProperty("/newFinancingOffer/SupplierID"),
						"purchaserID"				:	oModel.getProperty("/newFinancingOffer/PurchaserID"),
						"financerID"				:	fin.ID,
						"invoiceID"					:	oModel.getProperty("/newFinancingOffer/InvoiceID"),
						"purchaseOrderID"			:	oModel.getProperty("/newFinancingOffer/PurchaseOrderID"),
						"docType"					:	"Asset.FinancingOffer",
						"status"					:	"Created",
						"invoiceGrossAmount"		:	oModel.getProperty("/newFinancingOffer/InvoiceGrossAmount"),
						"discountPercent"			:	oModel.getProperty("/newFinancingOffer/DiscountPercent"),
						"discountAmount"			:	oModel.getProperty("/newFinancingOffer/DiscountAmount"),
						"fundedAmount"				:	oModel.getProperty("/newFinancingOffer/FundedAmount"),
						"offerValidityStartDate"	:	oModel.getProperty("/newFinancingOffer/OfferValidityStartDate"),
						"offerValidityEndDate"		:	oModel.getProperty("/newFinancingOffer/OfferValidityEndDate"),
						"disbursementDate"			:	oModel.getProperty("/newFinancingOffer/DisbursementDate"),
						"disbursementReference"		:	oModel.getProperty("/newFinancingOffer/DisbursementReference"),
						"currency"					:	oModel.getProperty("/newFinancingOffer/Currency")
				};
			} else {
				return {
						"ID"						:	oModel.getProperty("/selectedFinancingOffer/ID"),
						"supplierID"				:	oModel.getProperty("/selectedFinancingOffer/SupplierID"),
						"purchaserID"				:	oModel.getProperty("/selectedFinancingOffer/PurchaserID"),
						"financerID"				:	fin.ID,
						"invoiceID"					:	oModel.getProperty("/selectedFinancingOffer/InvoiceID"),
						"purchaseOrderID"			:	oModel.getProperty("/selectedFinancingOffer/PurchaseOrderID"),
						"docType"					:	oModel.getProperty("/selectedFinancingOffer/ObjectType"),
						"status"					:	oModel.getProperty("/selectedFinancingOffer/Status"),
						"invoiceGrossAmount"		:	oModel.getProperty("/selectedFinancingOffer/InvoiceGrossAmount"),
						"discountPercent"			:	oModel.getProperty("/selectedFinancingOffer/DiscountPercent"),
						"discountAmount"			:	oModel.getProperty("/selectedFinancingOffer/DiscountAmount"),
						"fundedAmount"				:	oModel.getProperty("/selectedFinancingOffer/FundedAmount"),
						"offerValidityStartDate"	:	oModel.getProperty("/selectedFinancingOffer/OfferValidityStartDate"),
						"offerValidityEndDate"		:	oModel.getProperty("/selectedFinancingOffer/OfferValidityEndDate"),
						"disbursementDate"			:	oModel.getProperty("/selectedFinancingOffer/DisbursementDate"),
						"disbursementReference"		:	oModel.getProperty("/selectedFinancingOffer/DisbursementReference"),
						"currency"					:	oModel.getProperty("/selectedFinancingOffer/Currency")
				};				
			}
		},
		
		mapFinancingOfferToLocalStorage : function(oComponent, oModel){

			var fin = bizNetAccessFinancers.getFinancerFromAlias(oComponent, oComponent.getModel("Login").getProperty("/alias"));
				
			return {
					"ID"						:	this.getNewFinancingOfferID(oModel),
					"supplierID"				:	oModel.getProperty("/newFinancingOffer/SupplierID"),
					"purchaserID"				:	oModel.getProperty("/newFinancingOffer/PurchaserID"),
					"financerID"				:	fin.ID,
					"invoiceID"					:	oModel.getProperty("/newFinancingOffer/InvoiceID"),
					"purchaseOrderID"			:	oModel.getProperty("/newFinancingOffer/PurchaseOrderID"),
					"docType"					:	"Asset.FinancingOffer",
					"status"					:	"Created",
					"invoiceGrossAmount"		:	oModel.getProperty("/newFinancingOffer/InvoiceGrossAmount"),
					"discountPercent"			:	oModel.getProperty("/newFinancingOffer/DiscountPercent"),
					"discountAmount"			:	oModel.getProperty("/newFinancingOffer/DiscountAmount"),
					"fundedAmount"				:	oModel.getProperty("/newFinancingOffer/FundedAmount"),
					"offerValidityStartDate"	:	oModel.getProperty("/newFinancingOffer/OfferValidityStartDate"),
					"offerValidityEndDate"		:	oModel.getProperty("/newFinancingOffer/OfferValidityEndDate"),
					"disbursementDate"			:	oModel.getProperty("/newFinancingOffer/DisbursementDate"),
					"disbursementReference"		:	oModel.getProperty("/newFinancingOffer/DisbursementReference"),
					"currency"					:	oModel.getProperty("/newFinancingOffer/Currency")
			};
		},
		
		getNewFinancingOfferID:function(oModel){

			if ( typeof oModel.getProperty("/newFinancingOffer/ID") === "undefined" ||
		    		oModel.getProperty("/newFinancingOffer/ID") === ""
		    	){
			    var iD = "FO";
				var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
			    for( var i = 0; i < 8; i++ ){
			        iD += possible.charAt(Math.floor(Math.random() * possible.length));
			    }
		    } else {
				iD = oModel.getProperty("/newFinancingOffer/ID");
			}
			oModel.setProperty("/newFinancingOffer/ID",iD);
		    return iD;
		}
	};
});