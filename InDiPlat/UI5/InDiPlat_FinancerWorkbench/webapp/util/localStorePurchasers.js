sap.ui.define(function() {
	"use strict";

	jQuery.sap.require("jquery.sap.storage");
	var oStorage = jQuery.sap.storage(jQuery.sap.storage.Type.local);
	var sampleDataID = "purchasers";

	return {

		init: function() {

			this.removeAll();
			oStorage.put(
				sampleDataID, 
				[
					{
						"ID"          : "PU100001",
						"docType"     : "Participant.Purchaser",
						"alias"       : "TOYOTACORPSG",
						"description" : "Toyota Corporation, Singapore"
					},
					{
						"ID"          : "PU100002",
						"docType"     : "Participant.Purchaser",
						"alias"       : "SINGTELSG",
						"description" : "Singtel, Singapore"
					},
					{
						"ID"          : "PU100003",
						"docType"     : "Participant.Purchaser",
						"alias"       : "EXXONMOBIL",
						"description" : "Exxon Mobil Asia Pte. Ltd."
					},
					{
						"ID"          : "PU100004",
						"docType"     : "Participant.Purchaser",
						"alias"       : "ROBERTBOSCHAG",
						"description" : "Robert Bosch AG, Germany"
					}
				]
			);
		},

		getSampleDataID: function() {

			return sampleDataID;
		},

		getSampleData: function() {

			return oStorage.get("purchasers");
		},

		put: function(newPurchaser) {

			var sampleData = this.getSampleData();
			sampleData.push(newPurchaser);
			oStorage.put(sampleDataID, sampleData);
		},
		
		remove : function (id){
		
			var sampleData = this.getSampleData();
			sampleData = _.without(sampleData,_.findWhere(sampleData,{ID:id}));
			oStorage.put(sampleDataID, sampleData);
		},
		
		removeAll : function(){
		
			oStorage.put(sampleDataID,[]);	
		},		

		clearSampleData: function() {

			oStorage.clear();
		}
	};
});