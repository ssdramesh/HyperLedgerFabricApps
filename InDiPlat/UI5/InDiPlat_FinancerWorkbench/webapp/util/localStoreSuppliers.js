sap.ui.define(function() {
	"use strict";

	jQuery.sap.require("jquery.sap.storage");
	var oStorage = jQuery.sap.storage(jQuery.sap.storage.Type.local);
	var sampleDataID = "suppliers";

	return {

		init: function() {

			this.removeAll();
			oStorage.put(
				sampleDataID, 
				[
					{
					    "ID": "SU100001",
					    "docType": "Participant",
					    "alias": "KSBPUMPSLTD",
					    "description": "KSB Pumps Ltd, Singapore"
					  },
					  {
					    "ID": "SU100002",
					    "docType": "Participant",
					    "alias": "ASUSTECH",
					    "description": "ASUS Technologies, Singapore"
					  },
					  {
					    "ID": "SU100003",
					    "docType": "Participant",
					    "alias": "HILTIMOTORS",
					    "description": "Hilti Asia Pte. Ltd."
					  },
					  {
					    "ID": "SU100004",
					    "docType": "Participant",
					    "alias": "DEMAGDELAG",
					    "description": "Demag Delewal AG, Germany"
					  }
				]
			);
		},

		getSampleDataID: function() {

			return sampleDataID;
		},

		getSampleData: function() {

			return oStorage.get("suppliers");
		},

		put: function(newSupplier) {

			var sampleData = this.getSampleData();
			sampleData.push(newAsset);
			oStorage.put(sampleDataID, sampleData);
		},
		
		remove : function (id){
		
			var sampleData = this.getSampleData();
			sampleData = _.without(sampleData,_.findWhere(sampleData,{ID:id}));
			oStorage.put(sampleDataID, sampleData);
		},
		
		removeAll : function(){
		
			oStorage.put(sampleDataID,[]);	
		},		

		clearSampleData: function() {

			oStorage.clear();
		}
	};
});