sap.ui.define(function() {
	"use strict";

	jQuery.sap.require("jquery.sap.storage");
	var oStorage = jQuery.sap.storage(jQuery.sap.storage.Type.local);
	var sampleDataID = "financers";

	return {

		init: function() {

			this.removeAll();
			oStorage.put(
				sampleDataID, 
				[
					{
						"ID"          : "FI100001",
						"docType"     : "Participant.Financer",
						"alias"       : "COMMERZBANK",
						"description" : "Commerzbank Germany"
					},
					{
						"ID"          : "FI100002",
						"docType"     : "Participant.Financer",
						"alias"       : "STANCHART",
						"description" : "Standard Chartered, Singapore"
					},
					{
						"ID"          : "FI100003",
						"docType"     : "Participant.Financer",
						"alias"       : "SUPCHAFIN",
						"description" : "Supcha Finance Asia Pte. Ltd."
					},
					{
						"ID"          : "FI100004",
						"docType"     : "Participant.Financer",
						"alias"       : "UOB Bank",
						"description" : "UOB Bank, Singapore"
					}
				]
			);
		},

		getSampleDataID: function() {

			return sampleDataID;
		},

		getSampleData: function() {

			return oStorage.get("financers");
		},

		put: function(newFinancer) {

			var sampleData = this.getSampleData();
			sampleData.push(newFinancer);
			oStorage.put(sampleDataID, sampleData);
		},
		
		remove : function (id){
		
			var sampleData = this.getSampleData();
			sampleData = _.without(sampleData,_.findWhere(sampleData,{ID:id}));
			oStorage.put(sampleDataID, sampleData);
		},
		
		removeAll : function(){
		
			oStorage.put(sampleDataID,[]);	
		},		

		clearSampleData: function() {

			oStorage.clear();
		}
	};
});