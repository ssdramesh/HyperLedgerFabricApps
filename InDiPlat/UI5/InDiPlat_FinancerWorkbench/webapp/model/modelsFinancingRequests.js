sap.ui.define([
	"sap/ui/model/json/JSONModel"
], function(JSONModel) {
	"use strict";

	return {

		createFinancingRequestsModel: function(){
			
			var oModel = new JSONModel();
			oModel.setDefaultBindingMode("TwoWay");
			oModel.setData({
				chaincode:{
					serviceUrl:"",
					serviceKey:{},
					entity:{}					
				},
				supplier			: {},
				financingRequestCollection	: {
					cols:[
						{name:"ID"},
						{name:"SupplierID"},
						{name:"PurchaserID"},
						{name:"FinancerID"},
						{name:"InvoiceID"},
						{name:"PurchaseOrderID"},
						{name:"ObjectType"},
						{name:"Status"},
						{name:"InvoiceGrossAmount"},
						{name:"InvoiceRequestDaysToCash"},
						{name:"InvoicePaymentDate"},
						{name:"RequestCreationDate"},
						{name:"RequestedDisbursementDate"},
						{name:"RequestedDiscountPercent"},
						{name:"RequestedFinancingAmount"},
						{name:"DocumentsChecked"},
						{name:"DocumentsVerified"},
						{name:"Currency"}
					],
					items:[]
				},
				selectedFinancingRequest	: {},		
				newFinancingRequest		: {
					ID							:	"",
					SupplierID					:	"",
					PurchaserID					:	"",
					FinancerID					:	"",
					InvoiceID					:	"",
					PurchaseOrderID				:	"",
					ObjectType					:	"",
					Status						:	"",
					InvoiceGrossAmount			:	"",
					InvoiceRequestDaysToCash	:	"",
					InvoicePaymentDate			:	"",
					RequestCreationDate			:	"",
					RequestedDisbursementDate	:	"",
					RequestedDiscountPercent	:	"",
					RequestedFinancingAmount	:	"",
					DocumentsChecked			:	"",
					DocumentsVerified			:	"",
					Currency					:	""
				},
				selectedFinancingRequestID	: "",
				searchFinancingRequestID : ""
			});
			return oModel;			
		}
	};
});