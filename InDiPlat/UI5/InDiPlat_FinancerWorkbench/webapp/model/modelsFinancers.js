sap.ui.define([
	"sap/ui/model/json/JSONModel"
], function(JSONModel) {
	"use strict";

	return {

		createFinancersModel: function(){
			
			var oModel = new JSONModel();
			oModel.setDefaultBindingMode("TwoWay");
			oModel.setData({
				chaincode:{
					serviceUrl:"",
					serviceKey:{},
					entity:{}					
				},
				financerCollection	: {
					cols:[
						{name:"ID"},
						{name:"ObjectType"},
						{name:"Alias"},
						{name:"Description"}
					],
					items:[]
				},
				financerRateCollection:{
					cols:[
						{name:"Financer"},
						{name:"15D"},
						{name:"30D"},
						{name:"45D"},
						{name:"60D"},
						{name:"75D"},
						{name:"90D"},
						{name:">90D"}
					],
					items:[]
				},
				selectedFinancer	: {},		
				newFinancer		: {
					ID						:	"",
					ObjectType				:	"",
					Alias					:	"",
					Description				:	""
				},
				selectedFinancerID	: "",
				searchFinancerID : ""
			});
			return oModel;			
		}
	};
});