sap.ui.define([
	"sap/ui/model/json/JSONModel"
], function(JSONModel) {
	"use strict";

	return {

		createPurchaseOrdersModel: function(){
			
			var oModel = new JSONModel();
			oModel.setDefaultBindingMode("TwoWay");
			oModel.setData({
				chaincode:{
					serviceUrl:"",
					serviceKey:{},
					entity:{}					
				},
				purchaseOrderCollection	: {
					cols:[
						{name:"ID"},
						{name:"PurchaserID"},
						{name:"SupplierID"},
						{name:"ObjectType"},
						{name:"Status"},
						{name:"PostingDate"},
						{name:"ApprovalDate"},
						{name:"PaymentDate"},
						{name:"GrossAmount"},
						{name:"TotalSubmittedAmount"},
						{name:"Currency"}
					],
					items:[]
				},
				selectedPurchaseOrder	: {},		
				newPurchaseOrder		: {
					ID						:	"",
					PurchaserID				:	"",
					SupplierID				:	"",
					ObjectType				:	"",
					Status					:	"",
					PostingDate				:	"",
					ApprovalDate			:	"",
					PaymentDate				:	"",
					GrossAmount				:	"",
					TotalSubmittedAmount	:	"",
					Currency				:	""
				},
				selectedPurchaseOrderID	: "",
				searchPurchaseOrderID : ""
			});
			return oModel;			
		}
	};
});