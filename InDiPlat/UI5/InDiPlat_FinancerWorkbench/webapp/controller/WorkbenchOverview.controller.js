sap.ui.define([
	"Workbench/controller/BaseController",
	"Workbench/util/messageProvider",
	"Workbench/util/localStoreFinancers",
	"Workbench/util/localStoreFinancingRequests",
	"Workbench/util/localStoreInvoices",
	"Workbench/model/modelsBase",
	"Workbench/model/modelsFinancers",
	"Workbench/model/modelsFinancingRequests",
	"Workbench/model/modelsInvoices",
	"Workbench/util/bizNetAccessFinancers",
	"Workbench/util/bizNetAccessFinancingRequests",
	"Workbench/util/bizNetAccessFinancingOffers",
	"Workbench/util/bizNetAccessInvoices",
	"sap/ui/core/routing/History",
	"sap/ui/model/Filter",
	"sap/ui/model/FilterOperator",
	"sap/ui/Device"
], function (
	BaseController,
	messageProvider,
	localStoreFinancers,
	localStoreFinancingRequests,
	localStoreInvoices,
	modelsBase,
	modelsFinancers,
	modelsFinancingRequests,
	modelsInvoices,
	bizNetAccessFinancers,
	bizNetAccessFinancingRequests,
	bizNetAccessFinancingOffers,
	bizNetAccessInvoices,
	History,
	Filter,
	FilterOperator,
	Device
) {

	"use strict";

	return BaseController.extend("Workbench.controller.WorkbenchOverview", {

		onInit: function () {

			var oList = this.byId("financingRequestsList");
			this._oList = oList;
			this._oListFilterState = {
				aFilter: [],
				aSearch: []
			};
			if (typeof this.getOwnerComponent().getModel("Messages") === "undefined") {
				this.getOwnerComponent().setModel(modelsBase.createMessagesModel(), "Messages");
			}
			messageProvider.init(this.getOwnerComponent().getModel("Messages"));
			this._loadModels();
		},

		onSelectionChange: function (oEvent) {

			this._showDetail(oEvent.getParameter("listItem") || oEvent.getSource());
		},

		// // fuction is never called. Is it relevant? No FRs can be created anyway
		// onAdd: function () {

		// 	this.getRouter().navTo(
		// 		"financingRequest", {
		// 			alias: this.getOwnerComponent().getModel("Login").getProperty("/alias"),
		// 			invoiceId: this.getOwnerComponent().getModel("Invoices").getProperty("/selectedInvoice/ID"),
		// 			financingRequestId: "___new"
		// 		}
		// 	);
		// },

		onSearch: function (oEvent) {

			var sQuery = oEvent.getParameter("query");
			if (sQuery) {
				this._oListFilterState.aSearch = [new Filter("ID", FilterOperator.Contains, sQuery)];
			} else {
				this._oListFilterState.aSearch = [];
			}
			this._applyFilterSearch();
			var bReplace = !Device.system.phone;
			var oModel = this.getModel("FinancingRequests");
			oModel.setProperty(
				"/selectedFinancingRequest",
				_.findWhere(oModel.getProperty("/financingRequestCollection/items"), {
						ID: oModel.getProperty("/searchfinancingRequestID")
					},
					this));
			this.getRouter().navTo("financingRequest", {
				invoiceId: oModel.getProperty("/selectedFinancingRequest").ID
			}, bReplace);
		},

		_showDetail: function (oItem) {

			var bReplace = !Device.system.phone;
			var oModel = this.getModel("FinancingRequests");
			this.getRouter().navTo("financingRequest", {
				alias: this.getModel("Login").getProperty("/alias"),
				financingRequestId: oModel.getObject(oItem.getBindingContextPath()).ID
			}, bReplace);
		},

		_applyFilterSearch: function () {
			var aFilters = this._oListFilterState.aSearch.concat(this._oListFilterState.aFilter),
				oViewModel = this.getModel("FinancingRequests");
			this._oList.getBinding("items").filter(aFilters, "Application");
			if (aFilters.length !== 0) {
				oViewModel.setProperty("/noDataText", this.getResourceBundle().getText("_labelNoFinancingRequestsText"));
			}
		},

		_loadModels: function () {

			localStoreFinancers.init();
			localStoreFinancingRequests.init();
			localStoreInvoices.init();

			//Load all Financers and logged in Financer
			this.getOwnerComponent().setModel(modelsFinancers.createFinancersModel(), "Financers");
			this.loadEntityMetaData("financer", this.getModel("Financers"));
			bizNetAccessFinancers.loadAllFinancers(this.getOwnerComponent(), this.getModel("Financers"));

			//Set currently logged in Financer
			if (this.getModel("Login").getProperty("/alias") !== "") {
				var fin = bizNetAccessFinancers.getFinancerFromAlias(this, this.getModel("Login").getProperty("/alias"));
				this.getOwnerComponent().getModel("Login").setProperty("/id", fin.ID);
				bizNetAccessFinancers.loadFinancer(this.getModel("Financers"), fin.ID);
			}

			//Load all financingRequests of the financer			
			if (typeof fin !== "undefined") {
				this.getModel("FinancingRequests").setProperty("/financer", fin);
			}
			this.loadEntityMetaData("financingRequest", this.getModel("FinancingRequests"));
			bizNetAccessFinancingRequests.loadAllFinancingRequests(this.getOwnerComponent(), this.getModel("FinancingRequests"));

			//Load all financingOffers made by the financer 
			this.loadEntityMetaData("financingOffer", this.getModel("FinancingOffers"));
			bizNetAccessFinancingOffers.loadAllFinancingOffers(this.getOwnerComponent(), this.getModel("FinancingOffers"));

			//Load invoices that are touched by the financer
			this.loadEntityMetaData("invoice", this.getModel("Invoices"));
			bizNetAccessInvoices.loadAllInvoices(this.getOwnerComponent(), this.getModel("Invoices"));
		},

		onFinancingOffersOverview: function () {

			var oModel = this.getModel("FinancingRequests");
			this.getRouter().navTo("financingOffers", {
				alias: this.getModel("Login").getProperty("/alias"),
				financingRequestId: oModel.getProperty("/selectedFinancingRequestID")
			}, true);
		},

		_onToggle: function () {

			this.getOwnerComponent().getModel("TestSwitch").setProperty(
				"/testMode", !this.getOwnerComponent().getModel("TestSwitch").getProperty("/testMode")
			);
			var location = this.getRunMode().location;
			messageProvider.addMessage(
				"Success",
				"Switching Network for Blockchain Data to: ",
				"No Description",
				location,
				1,
				"",
				"http://www.sap.com"
			);
			this._loadModels();
		},

		onMessagePress: function () {

			this.onShowMessageDialog("InDiPlat Log");
		},

		onRefreshFinancingRequests: function () {

			bizNetAccessFinancingRequests.loadAllFinancingRequests(this.getOwnerComponent(), this.getModel("FinancingRequests"));
		},

		onNavBack: function () {
			this.getRouter().navTo("home");
		}
	});
});