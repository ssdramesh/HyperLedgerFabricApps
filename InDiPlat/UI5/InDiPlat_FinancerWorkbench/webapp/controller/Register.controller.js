sap.ui.define([
	"Workbench/controller/BaseController",
	"Workbench/util/messageProvider",
	"Workbench/util/localStoreFinancers",
	"Workbench/model/modelsBase",
	"Workbench/model/modelsFinancers",
	"Workbench/util/bizNetAccessFinancers"
], function(BaseController, messageProvider, localStoreFinancers, modelsBase, modelsFinancers, bizNetAccessFinancers) {
	"use strict";
	return BaseController.extend("Workbench.controller.Register", {
		onInit: function() {
			this._loadModel();
		},
		onAddNew: function() {
			bizNetAccessFinancers.addNewFinancer(this.getOwnerComponent(), this.getModel("Financers"));
		},
		_loadModel: function() {
			localStoreFinancers.init();
			if (typeof this.getOwnerComponent().getModel("Messages") === "undefined") {
				this.getOwnerComponent().setModel(modelsBase.createMessagesModel(), "Messages");
			}
			messageProvider.init(this.getOwnerComponent().getModel("Messages"));
			if (typeof this.getModel("Financers") === "undefined") {
				this.getOwnerComponent().setModel(modelsFinancers.createFinancersModel(), "Financers");
			}
			this.loadEntityMetaData("financer", this.getModel("Financers"));
			bizNetAccessFinancers.loadAllFinancers(this.getOwnerComponent(), this.getModel("Financers"));
		},

		onNavBack: function() {
			this.getRouter().navTo("home", {}, true);
		}
	});
});