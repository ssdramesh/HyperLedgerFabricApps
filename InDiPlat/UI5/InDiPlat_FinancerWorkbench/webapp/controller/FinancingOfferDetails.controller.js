sap.ui.define([
	"Workbench/controller/BaseController",
	"Workbench/util/localStoreFinancingOffers",
	"Workbench/util/bizNetAccessFinancingOffers",
	"Workbench/util/bizNetAccessInvoices"
], function (
	BaseController,
	localStoreFinancingOffers,
	bizNetAccessFinancingOffers,
	bizNetAccessInvoices
) {

	"use strict";

	return BaseController.extend("Workbench.controller.FinancingOfferDetails", {

		onInit: function () {

			this.getOwnerComponent().getRouter().getRoute("financingOffer").attachPatternMatched(this._onObjectMatched, this);
			this._setActions();
		},

		onAfterRendering: function () {

			this._setActions();
		},

		onAddNewFinancingOffer: function () {

			var oModel = this.getView().getModel("FinancingOffers");
			if (oModel.getProperty("/newFinancingOffer/OfferValidityStartDate") === "") {
				sap.m.MessageToast.show("Please enter Validity Start Date");
				return;
			}
			if (oModel.getProperty("/newFinancingOffer/OfferValidityEndDate") === "") {
				sap.m.MessageToast.show("Please enter Validity End Date");
				return;
			}
			if (oModel.getProperty("/newFinancingOffer/DisbursementDate") === "") {
				sap.m.MessageToast.show("Please enter Dibursement Date");
				return;
			}

			var financingOfferId = bizNetAccessFinancingOffers.addNewFinancingOffer(this.getOwnerComponent(), oModel);
			this.getView().byId("__barFinancingOffer").setSelectedKey("Details");
			bizNetAccessFinancingOffers.loadFinancingOffer(this.getView().getModel("FinancingOffers"), financingOfferId);

			this._clearNewFinancingOffer();
			this._setActions();
		},

		onSetFinancingOfferSubmitted: function () {

			var oModel = this.getModel("FinancingOffers");
			bizNetAccessFinancingOffers.setFinancingOfferSubmitted(this.getOwnerComponent(), oModel, oModel.getProperty(
				"/selectedFinancingOfferID"));
			bizNetAccessInvoices.setFinancingOffered(this.getOwnerComponent(), this.getModel("Invoices"), oModel.getProperty(
				"/selectedFinancingOffer/InvoiceID"));
			bizNetAccessFinancingOffers.loadAllFinancingOffers(this.getOwnerComponent(), this.getModel("FinancingOffers"));
			oModel.setProperty("/selectedFinancialOffer", bizNetAccessFinancingOffers.loadFinancingOffer(oModel, oModel.getProperty(
				"/selectedFinancingOfferID")));
			this.getView().byId("__barFinancingOffer").setSelectedKey("Details");
			this._setActions();
		},

		onSetFinancingOfferDisbursementInitiated: function () {

			var oModel = this.getModel("FinancingOffers");
			if (oModel.getProperty("/selectedFinancingOffer/DisbursementDate") === "") {
				sap.m.MessageToast.show("Please enter Dibursement Date");
				return;
			}
			if (oModel.getProperty("/selectedFinancingOffer/DisbursementDate") === "") {
				sap.m.MessageToast.show("Please enter Dibursement Reference for Payment");
				return;
			}
			bizNetAccessFinancingOffers.setFinancingOfferDisbursementInitiated(this.getOwnerComponent(), oModel, oModel.getProperty(
				"/selectedFinancingOfferID"));
			bizNetAccessInvoices.setFinancingCompleted(this.getOwnerComponent(), this.getModel("Invoices"), oModel.getProperty(
				"/selectedFinancingOffer/InvoiceID"));
			bizNetAccessFinancingOffers.loadAllFinancingOffers(this.getOwnerComponent(), this.getModel("FinancingOffers"));
			oModel.setProperty("/selectedFinancialOffer", bizNetAccessFinancingOffers.loadFinancingOffer(oModel, oModel.getProperty(
				"/selectedFinancingOfferID")));
			this.getView().byId("__barFinancingOffer").setSelectedKey("Details");
			this._setActions();
		},

		onCalculateFinancingOffer: function () {

			var financingOffer = this.getModel("FinancingOffers").getProperty("/newFinancingOffer");
			var gAmount, dRate, dAmount, fAmount;
			gAmount = parseFloat(financingOffer.InvoiceGrossAmount).toFixed(2);
			dRate = parseFloat(financingOffer.DiscountPercent).toFixed(2);
			dAmount = parseFloat(gAmount * (dRate / 100)).toFixed(2);
			fAmount = parseFloat(gAmount - dAmount).toFixed(2);
			this.getModel("FinancingOffers").setProperty("/newFinancingOffer/DiscountAmount", dAmount);
			this.getModel("FinancingOffers").setProperty("/newFinancingOffer/FundedAmount", fAmount);
			return {
				discountAmount: dAmount,
				fundedAmount: fAmount
			};
		},

		_onObjectMatched: function (oEvent) {

			var pId = oEvent.getParameter("arguments").financingOfferId;
			if (pId === "___new") {
				this._copyFinancingRequestToFinancingOffer(this.getModel("FinancingRequests").getProperty("/selectedFinancingRequest"), this.getModel(
					"FinancingOffers"));
				this.getView().byId("__barFinancingOffer").setSelectedKey("New");
				this._setActions();
			} else {
				this.getModel("FinancingOffers").setProperty("/selectedFinancingOfferID", pId);
				bizNetAccessFinancingOffers.loadFinancingOffer(this.getModel("FinancingOffers"), oEvent.getParameter("arguments").financingOfferId);
				this.getView().byId("__barFinancingOffer").setSelectedKey("Details");
				this._setActions();
			}
		},

		_clearNewFinancingOffer: function () {

			var oModel = this.getView().getModel("FinancingOffers");
			oModel.setProperty("/newFinancingOffer/SupplierID", "");
			oModel.setProperty("/newFinancingOffer/PurchaserID", "");
			oModel.setProperty("/newFinancingOffer/FinancerID", "");
			oModel.setProperty("/newFinancingOffer/InvoiceID", "");
			oModel.setProperty("/newFinancingOffer/PurchaseOrderID", "");
			oModel.setProperty("/newFinancingOffer/InvoiceGrossAmount", "");
			oModel.setProperty("/newFinancingOffer/DiscountPercent", "");
			oModel.setProperty("/newFinancingOffer/DiscountAmount", "");
			oModel.setProperty("/newFinancingOffer/FundedAmount", "");
			oModel.setProperty("/newFinancingOffer/DisbursementDate", "");
			oModel.setProperty("/newFinancingOffer/Currency", "");
		},

		_copyFinancingRequestToFinancingOffer: function (financingRequest, oModel) {

			oModel.setProperty("/newFinancingOffer/SupplierID", financingRequest.SupplierID);
			oModel.setProperty("/newFinancingOffer/PurchaserID", financingRequest.PurchaserID);
			oModel.setProperty("/newFinancingOffer/FinancerID", financingRequest.FinancerID);
			oModel.setProperty("/newFinancingOffer/InvoiceID", financingRequest.ID);
			oModel.setProperty("/newFinancingOffer/PurchaseOrderID", financingRequest.PurchaseOrderID);
			oModel.setProperty("/newFinancingOffer/InvoiceGrossAmount", financingRequest.InvoiceGrossAmount);
			oModel.setProperty("/newFinancingOffer/DiscountPercent", financingRequest.RequestedDiscountPercent);
			oModel.setProperty("/newFinancingOffer/DiscountAmount", this.onCalculateFinancingOffer().discountAmount);
			oModel.setProperty("/newFinancingOffer/FundedAmount", this.onCalculateFinancingOffer().fundedAmount);
			oModel.setProperty("/newFinancingOffer/DisbursementDate", financingRequest.RequestedDisbursementDate);
			oModel.setProperty("/newFinancingOffer/Currency", financingRequest.Currency);
		},

		_setActions: function () {

			switch (this.getView().byId("__barFinancingOffer").getSelectedKey()) {
			case "New":
				this.getView().byId("__buttonCreateFinancingOffer").setVisible(true);
				this.getView().byId("__buttonCalculateFinancingOffer").setVisible(true);
				this.getView().byId("__buttonSubmitFinancingOffer").setVisible(false);
				this.getView().byId("__buttonDisburseFinancingOffer").setVisible(false);
				return;
			case "Details":
				var selectedOffer = this.getModel("FinancingOffers").getProperty("/selectedFinancingOffer");
				this.getView().byId("__buttonCreateFinancingOffer").setVisible(false);
				switch (selectedOffer.Status) {
				case "Created":
					this.getView().byId("__buttonCalculateFinancingOffer").setVisible(false);
					this.getView().byId("__buttonSubmitFinancingOffer").setVisible(true);
					this.getView().byId("__buttonDisburseFinancingOffer").setVisible(false);
					return;
				case "Submitted":
					this.getView().byId("__buttonCalculateFinancingOffer").setVisible(false);
					this.getView().byId("__buttonSubmitFinancingOffer").setVisible(false);
					this.getView().byId("__buttonDisburseFinancingOffer").setVisible(false);
					return;
				case "Accepted":
					this.getView().byId("__buttonCalculateFinancingOffer").setVisible(false);
					this.getView().byId("__buttonSubmitFinancingOffer").setVisible(false);
					this.getView().byId("__buttonDisburseFinancingOffer").setVisible(true);
					return;
				case "Disbursement Initiated":
					this.getView().byId("__buttonCalculateFinancingOffer").setVisible(false);
					this.getView().byId("__buttonSubmitFinancingOffer").setVisible(false);
					this.getView().byId("__buttonDisburseFinancingOffer").setVisible(false);
					return;
				}
			}
		}

	});
});