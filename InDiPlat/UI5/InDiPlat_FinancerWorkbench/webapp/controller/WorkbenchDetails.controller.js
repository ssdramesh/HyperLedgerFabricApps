sap.ui.define([
	"Workbench/controller/BaseController",
	"Workbench/model/modelsFinancers",
	"Workbench/util/localStoreFinancingRequests",
	"Workbench/util/formatterFinancingOffers",
	"Workbench/util/bizNetAccessFinancingRequests",
	"Workbench/util/bizNetAccessFinancingOffers"
], function (
	BaseController,
	modelsFinancers,
	localStoreFinancingRequests,
	formatterFinancingOffers,
	bizNetAccessFinancingRequests,
	bizNetAccessFinancingOffers
) {

	"use strict";

	return BaseController.extend("Workbench.controller.WorkbenchDetails", {

		onInit: function () {

			this.getOwnerComponent().getRouter().getRoute("financingRequest").attachPatternMatched(this._onObjectMatched, this);
		},

		onAfterRendering: function () {

			this._setActions();
		},

		onCalculateFinancingRequest: function () {

			var oModel = this.getOwnerComponent().getModel("FinancingRequests");
			var gAmountNew, dRateNew, gAmount, dRate;
			if (oModel.getProperty("/newFinancingRequest/InvoiceGrossAmount") !== "") {
				gAmountNew = parseFloat(oModel.getProperty("/newFinancingRequest/InvoiceGrossAmount")).toFixed(2);
			} else {
				gAmountNew = 0;
			}
			if (oModel.getProperty("/newFinancingRequest/RequestedDiscountPercent") !== "") {
				dRateNew = parseFloat(oModel.getProperty("/newFinancingRequest/RequestedDiscountPercent")).toFixed(2);
			} else {
				dRateNew = 0;
			}
			if (gAmountNew !== 0 && dRateNew !== 0) {
				oModel.setProperty("/newFinancingRequest/RequestedFinancingAmount", parseFloat((gAmountNew * (1 - dRateNew / 100))).toFixed(2).toString());
				return;
			} else {
				if (oModel.getProperty("/selectedFinancingRequest/InvoiceGrossAmount") !== "") {
					gAmount = parseFloat(oModel.getProperty("/selectedFinancingRequest/InvoiceGrossAmount")).toFixed(2);
				} else {
					gAmount = 0;
				}
				if (oModel.getProperty("/selectedFinancingRequest/RequestedDiscountPercent") !== 0) {
					dRate = parseFloat(oModel.getProperty("/selectedFinancingRequest/RequestedDiscountPercent")).toFixed(2);
				} else {
					dRate = 0;
				}
				if (gAmount !== 0 && dRate !== 0) {
					oModel.setProperty("/selectedFinancingRequest/RequestedFinancingAmount", (gAmount * (1 - dRate / 100)).toString());
					return;
				} else {
					sap.m.MessageToast.show("ERROR! Please input valid Requested Discount Rate and Invoice Gross Amount");
				}
			}
		},

		onSetFinancingRequestAccepted: function () {

			var oModel = this.getModel("FinancingRequests");
			if (oModel.getProperty("/selectedFinancingRequest/Status") === "Accepted") {
				sap.m.MessageToast.show("Financing Request already Accepted");
				return;
			}
			if (oModel.getProperty("/selectedFinancingRequest/Status") === "Declined") {
				sap.m.MessageToast.show("Financing Request already Declined");
				return;
			}
			bizNetAccessFinancingRequests.setFinancingRequestAccepted(this.getOwnerComponent(), oModel);
			bizNetAccessFinancingRequests.loadFinancingRequest(oModel, oModel.getProperty("/selectedFinancingRequest/ID"));
			this.getView().byId("__barFinancingRequest").setSelectedKey("Details");
			this._setActions();
			var newOfferID = bizNetAccessFinancingOffers.addNewFinancingOffer(this.getOwnerComponent(), this._getNewFinancingOfferModel(oModel));
			this.getRouter().navTo("financingOffers", {
				alias: this.getModel("Login").getProperty("/alias"),
				financingRequestId: oModel.getProperty("/selectedfinancingRequestID"),
				financingOfferId: newOfferID
			}, true);
		},

		onSetFinancingRequestDeclined: function () {
			var oModel = this.getModel("FinancingRequests");
			if (oModel.getProperty("/selectedFinancingRequest/Status") === "Accepted") {
				sap.m.MessageToast.show("Financing Request already Accepted");
				return;
			}
			if (oModel.getProperty("/selectedFinancingRequest/Status") === "Declined") {
				sap.m.MessageToast.show("Financing Request already Declined");
				return;
			}
			bizNetAccessFinancingRequests.setFinancingRequestDeclined(this.getOwnerComponent(), oModel);
			bizNetAccessFinancingRequests.loadFinancingRequest(oModel, oModel.getProperty("/selectedFinancingRequest/ID"));
			this.getView().byId("__barFinancingRequest").setSelectedKey("Details");
			this._setActions();
		},

		onSetFinancingRequestDocumentStatus: function () {

			var oModel = this.getModel("FinancingRequests");
			bizNetAccessFinancingRequests.setFinancingRequestDocumentsChecked(this.getOwnerComponent(), oModel, oModel.getProperty(
				"/selectedFinancingRequestID"));
			bizNetAccessFinancingRequests.setFinancingRequestDocumentsVerified(this.getOwnerComponent(), oModel, oModel.getProperty(
				"/selectedFinancingRequestID"));
			bizNetAccessFinancingRequests.loadFinancingRequest(oModel, oModel.getProperty("/selectedFinancingRequest/ID"));
			this.getView().byId("__barFinancingRequest").setSelectedKey("Details");
			this._setActions();
		},

		_onObjectMatched: function (oEvent) {

			var pId = oEvent.getParameter("arguments").financingRequestId;
			this.getModel("FinancingRequests").setProperty("/selectedFinancingRequestID", pId);
			bizNetAccessFinancingRequests.loadFinancingRequest(this.getView().getModel("FinancingRequests"), oEvent.getParameter("arguments").financingRequestId);
			this._setActions();
		},

		_getCurrentDate: function () {

			var today = new Date();
			return (today.getMonth() + 1) + "/" + today.getDate() + "/" + today.getFullYear();
		},

		_getNewFinancingOfferModel: function (oModel) {

			var offerModel = this.getOwnerComponent().getModel("FinancingOffers");
			offerModel.setProperty("/newFinancingOffer/ID", "");
			offerModel.setProperty("/newFinancingOffer/SupplierID", oModel.getProperty("/selectedFinancingRequest/SupplierID")); //= oModel.getProperty("/selectedFinancingRequest/SupplierID"); 
			offerModel.setProperty("/newFinancingOffer/PurchaserID", oModel.getProperty("/selectedFinancingRequest/PurchaserID"));
			offerModel.setProperty("/newFinancingOffer/FinancerID", oModel.getProperty("/selectedFinancingRequest/FinancerID"));
			offerModel.setProperty("/newFinancingOffer/InvoiceID", oModel.getProperty("/selectedFinancingRequest/InvoiceID"));
			offerModel.setProperty("/newFinancingOffer/PurchaseOrderID", oModel.getProperty("/selectedFinancingRequest/PurchaseOrderID"));
			offerModel.setProperty("/newFinancingOffer/ObjectType", "Asset.FinancingOffer");
			offerModel.setProperty("/newFinancingOffer/Status", "Created");
			offerModel.setProperty("/newFinancingOffer/InvoiceGrossAmount", oModel.getProperty("/selectedFinancingRequest/InvoiceGrossAmount"));
			offerModel.setProperty("/newFinancingOffer/DiscountPercent", oModel.getProperty(
				"/selectedFinancingRequest/RequestedDiscountPercent"));
			offerModel.setProperty("/newFinancingOffer/DiscountAmount", this._getDiscountAmount(oModel));
			offerModel.setProperty("/newFinancingOffer/FundedAmount", oModel.getProperty("/selectedFinancingRequest/RequestedFinancingAmount"));
			offerModel.setProperty("/newFinancingOffer/OfferValidityStartDate", this._getCurrentDate());
			offerModel.setProperty("/newFinancingOffer/OfferValidityEndDate", "");
			offerModel.setProperty("/newFinancingOffer/DisbursementDate", "");
			offerModel.setProperty("/newFinancingOffer/DisbursementReference", "");
			offerModel.setProperty("/newFinancingOffer/Currency", oModel.getProperty("/selectedFinancingRequest/Currency"));
			return offerModel;
		},

		_getDiscountAmount: function (oModel) {

			return parseInt(oModel.getProperty("/selectedFinancingRequest/InvoiceGrossAmount"), 10) - parseInt(oModel.getProperty(
				"/selectedFinancingRequest/RequestedFinancingAmount"), 10);
		},

		_setActions: function () {

			var financingRequest = this.getModel("FinancingRequests").getProperty("/selectedFinancingRequest");

			if (_.isEmpty(financingRequest)) {
				return;
			}
			switch (financingRequest.Status) {
			case "In Process":
				this.getView().byId("__buttonAcceptFinancingRequest").setVisible(true);
				this.getView().byId("__buttonDeclineFinancingRequest").setVisible(true);
				this.getView().byId("__buttonCalculateFinancingRequest").setVisible(true);
				if (financingRequest.DocumentsChecked === "true" && financingRequest.DocumentsVerified === "true") {
					this.getView().byId("__buttonSetDocumentStatusOK").setVisible(false);

				} else {
					this.getView().byId("__buttonSetDocumentStatusOK").setVisible(true);
				}
				return;
			case "Accepted":
				this.getView().byId("__buttonSetDocumentStatusOK").setVisible(false);
				this.getView().byId("__buttonAcceptFinancingRequest").setVisible(false);
				this.getView().byId("__buttonDeclineFinancingRequest").setVisible(false);
				this.getView().byId("__buttonCalculateFinancingRequest").setVisible(false);
				return;
			case "Declined":
				this.getView().byId("__buttonSetDocumentStatusOK").setVisible(false);
				this.getView().byId("__buttonAcceptFinancingRequest").setVisible(false);
				this.getView().byId("__buttonDeclineFinancingRequest").setVisible(false);
				this.getView().byId("__buttonCalculateFinancingRequest").setVisible(false);
				return;
			}
		}
	});
});