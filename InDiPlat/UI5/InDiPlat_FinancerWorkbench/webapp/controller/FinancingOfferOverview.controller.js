sap.ui.define([
	"Workbench/controller/BaseController",
	"Workbench/util/messageProvider",
	"Workbench/util/localStoreFinancingOffers",
	"Workbench/model/modelsBase",
	"Workbench/model/modelsFinancingOffers",
	"Workbench/util/bizNetAccessFinancingOffers",
	"sap/ui/core/routing/History",
	"sap/ui/model/Filter",
	"sap/ui/model/FilterOperator",
	"sap/ui/Device"	
], function(
		BaseController,
		messageProvider,
		localStoreFinancingOffers,
		modelsBase,
		modelsFinancingOffers,
		bizNetAccessFinancingOffers,
		History, 
		Filter, 
		FilterOperator, 
		Device		
	) {
	"use strict";

	return BaseController.extend("Workbench.controller.FinancingOfferOverview", {

		onInit : function(){
			
			var oList = this.byId("financingOffersList");
			this._oList = oList;
			this._oListFilterState = {
				aFilter : [],
				aSearch : []
			};
			this._loadModels();
			this.onAdd();
		},
		
		onSelectionChange : function (oEvent) {
			
			this._showDetail(oEvent.getParameter("listItem") || oEvent.getSource());
		},
		
		onAdd : function() {
			
			this.getRouter().navTo(
				"financingOffer", 
				{
					alias				: this.getOwnerComponent().getModel("Login").getProperty("/alias"),
					financingOfferId	: "___new"
				}
			);
		},
		
		onAddNewFinancingOffer: function(){
			
			
		},
		
		onSearch : function(oEvent) {
			
			var sQuery = oEvent.getParameter("query");
			if (sQuery) {
				this._oListFilterState.aSearch = [new Filter("ID", FilterOperator.Contains, sQuery)];
			} else {
				this._oListFilterState.aSearch = [];
			}
			this._applyFilterSearch();			
			var bReplace = !Device.system.phone;
			var oModel = this.getModel("FinancingOffers");
			oModel.setProperty(
				"/selectedFinancingOffer",
				_.findWhere(oModel.getProperty("/financingOfferCollection/items"), 
					{
						ID: oModel.getProperty("/searchfinancingOfferID")
					},
				this));
			this.getRouter().navTo("financingOffer", {
				invoiceId : oModel.getProperty("/selectedFinancingOffer").ID
			}, bReplace);			
		},
		
		_showDetail : function(oItem) {
			
			var bReplace = !Device.system.phone;
			var oModel = this.getModel("FinancingOffers");
			this.getRouter().navTo("financingOffer", {
				alias				: this.getModel("Login").getProperty("/alias"),
				financingOfferId	: oModel.getObject(oItem.getBindingContextPath()).ID
			}, bReplace);
		},
		
		_applyFilterSearch : function () {
			var aFilters = this._oListFilterState.aSearch.concat(this._oListFilterState.aFilter),
				oViewModel = this.getModel("FinancingOffers");
			this._oList.getBinding("items").filter(aFilters, "Application");
			if (aFilters.length !== 0) {
				oViewModel.setProperty("/noDataText", this.getResourceBundle().getText("_labelNoFinancingOffersText"));
			}
		},
		
		_loadModels : function () {
			
			localStoreFinancingOffers.init();

//			Setup Message Provider			
			if (typeof this.getOwnerComponent().getModel("Messages") === "undefined") {
				this.getOwnerComponent().setModel(modelsBase.createMessagesModel(), "Messages");
			}
			messageProvider.init(this.getOwnerComponent().getModel("Messages"));
			
//			Load all existing Financing Offers made by Financer
			// this.getOwnerComponent().setModel(modelsFinancingOffers.createFinancingOffersModel(),"FinancingOffers");
			this.loadEntityMetaData("financingOffer", this.getModel("FinancingOffers"));
			bizNetAccessFinancingOffers.loadAllFinancingOffers(this.getOwnerComponent(), this.getModel("FinancingOffers"));

		},		
		
		onNavBack : function() {
			this.getRouter().navTo("financingRequests");
		}
	});
});