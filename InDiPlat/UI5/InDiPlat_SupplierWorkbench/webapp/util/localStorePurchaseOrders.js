sap.ui.define(function() {
	"use strict";

	jQuery.sap.require("jquery.sap.storage");
	var oStorage = jQuery.sap.storage(jQuery.sap.storage.Type.local);
	var sampleDataID = "purchaseOrders";

	return {

		init: function() {

			this.removeAll();
			oStorage.put(
				sampleDataID, 
				[
					{
						"ID"                    : "PO100001",
						"purchaserID"           : "PU100001",
						"supplierID"            : "SU100001",
						"docType"               : "Asset.PurchaseOrder",
						"status"                : "Approved",
						"postingDate"           : "01/02/2017",
						"approvalDate"          : "01/02/2017",
						"paymentDate"           : "02/02/2017",
						"grossAmount"           : "6754.34",
						"totalSubmittedAmount"  : "6754.34",
						"currency"              : "SGD"
					},
					{
						"ID"                    : "PO100002",
						"purchaserID"           : "PU100002",
						"supplierID"            : "SU100002",
						"docType"               : "Asset.PurchaseOrder",
						"status"                : "Approved",
						"postingDate"           : "02/03/2017",
						"approvalDate"          : "02/04/2017",
						"paymentDate"           : "03/20/2017",
						"grossAmount"           : "5674.50",
						"totalSubmittedAmount"  : "5674.50",
						"currency"              : "SGD"
					}
				]
			);
		},

		getSampleDataID: function() {

			return sampleDataID;
		},

		getSampleData: function() {

			return oStorage.get("purchaseOrders");
		},

		put: function(newPurchaseOrder) {

			var sampleData = this.getSampleData();
			sampleData.push(newPurchaseOrder);
			oStorage.put(sampleDataID, sampleData);
		},
		
		remove : function (id){
		
			var sampleData = this.getSampleData();
			sampleData = _.without(sampleData,_.findWhere(sampleData,{ID:id}));
			oStorage.put(sampleDataID, sampleData);
		},
		
		setPurchaseOrderPaid : function(purchaseOrderID, paymentDate){
			
			var purchaseOrder = _.findWhere(this.getSampleData(),{ID:purchaseOrderID}, this);
			this.remove(purchaseOrderID);
			purchaseOrder.paymentDate = paymentDate;
			purchaseOrder.status = "Paid";
			this.put(purchaseOrder);
		},
		
		removeAll : function(){
		
			oStorage.put(sampleDataID,[]);	
		},		

		clearSampleData: function() {

			oStorage.clear();
		}
	};
});