sap.ui.define([
	"Workbench/util/restBuilder",
	"Workbench/util/formatterSuppliers",
	"Workbench/util/localStoreSuppliers"
], function (
	restBuilder,
	formatterSuppliers,
	localStoreSuppliers
) {
	"use strict";

	return {

		loadAllSuppliers: function (oComponent, oModel) {

			if (!(oComponent.getModel("TestSwitch").getProperty("/testMode"))) {
				restBuilder.execute(
					oModel.getProperty("/chaincode"),
					"readAll", [],
					function (responseData) {
						oModel.setProperty("/supplierCollection/items", formatterSuppliers.mapSuppliersToModel(responseData));
					});
			} else {
				var sData = localStoreSuppliers.getSampleData();
				oModel.setProperty("/supplierCollection/items", formatterSuppliers.mapSuppliersToModel(sData));
			}
		},

		loadSupplier: function (oModel, ID) {

			oModel.setProperty(
				"/selectedSupplier",
				_.findWhere(oModel.getProperty("/supplierCollection/items"), {
					ID: ID
				}, this));
		},

		addNewSupplier: function (oComponent, oModel) {

			if (!(oComponent.getModel("TestSwitch").getProperty("/testMode"))) {
				restBuilder.execute(
					oModel.getProperty("/chaincode"),
					"addNew",
					formatterSuppliers.mapSupplierToChaincode(oModel, true)
				);
			} else {
				localStoreSuppliers.put(formatterSuppliers.mapSupplierToLocalStorage(oModel));
			}
			this.loadAllSuppliers(oComponent, oModel);
			return oModel.getProperty("/newSupplier/ID");
		},

		removeSupplier: function (oComponent, oModel, supplierID) {

			if (!(oComponent.getModel("TestSwitch").getProperty("/testMode"))) {
				restBuilder.execute(
					oModel.getProperty("/chaincode"),
					"remove", {
						ID: supplierID
					}
				);
			} else {
				localStoreSuppliers.remove(supplierID);
			}
			this.loadAllSuppliers(oComponent, oModel);
			return true;
		},

		removeAllSuppliers: function (oComponent, oModel) {

			if (!(oComponent.getModel("TestSwitch").getProperty("/testMode"))) {
				restBuilder.execute(
					oModel.getProperty("/chaincode"),
					"removeAll", []
				);
			} else {
				localStoreSuppliers.removeAll();
			}
			this.loadAllSuppliers(oComponent, oModel);
			oModel.setProperty("/selectedSupplier", {});
			return true;
		},

		getSupplierFromAlias: function (oComponent, alias) {

			return _.findWhere(
				oComponent.getModel("Suppliers").getProperty("/supplierCollection/items"), {
					Alias: alias
				},
				this
			);
		}
	};
});