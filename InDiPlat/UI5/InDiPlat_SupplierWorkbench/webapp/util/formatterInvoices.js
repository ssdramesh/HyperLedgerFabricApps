sap.ui.define([
		"Workbench/util/bizNetAccessSuppliers"
	],function(bizNetAccessSuppliers) {
	"use strict";

	return {

		mapInvoiceToModel:function(responseData){
			return {
				ID						: responseData.ID,
				SupplierID				: responseData.supplierID,
				PurchaserID				: responseData.purchaserID,
				FinancerID				: responseData.financerID,
				PurchaseOrderID			: responseData.purchaseOrderID,
				ObjectType 				: responseData.docType,
				Status					: responseData.status,
				PostingDate				: responseData.postingDate,
				ApprovalDate			: responseData.approvalDate,
				PaymentDate				: responseData.paymentDate,
				DisbursementDate		: responseData.disbursementDate,
				GrossAmount				: responseData.grossAmount,
				DiscountPercent			: responseData.discountPercent,
				FinancedAmount			: responseData.financeedAmount,
				Currency				: responseData.currency
			};
		},

		mapInvoicesToModel:function(responseData){
			
			var items = [];
			if( responseData ){
				for ( var i = 0; i < responseData.length; i++ ){
					items.push(this.mapInvoiceToModel(responseData[i]));
				}
			}
			return items;
		},

		mapInvoiceToChaincode:function(oComponent, oModel, newInvoice){
			var sup = bizNetAccessSuppliers.getSupplierFromAlias(oComponent, oComponent.getModel("Login").getProperty("/alias"));
			
			if ( newInvoice === true ) {
				return {
						"ID"					:	this.getNewInvoiceID(oModel),
						"supplierID"			:	sup.ID,
						"purchaserID"			:	oModel.getProperty("/newInvoice/PurchaserID"),
						"financerID"			:	oModel.getProperty("/newInvoice/FinancerID"),
						"purchaseOrderID"		:	oModel.getProperty("/newInvoice/PurchaseOrderID"),
						"docType"				:	"Asset.Invoice",
						"status"				:	"Approved",
						"postingDate"			:	oModel.getProperty("/newInvoice/PostingDate")	,
						"approvalDate"			:	oModel.getProperty("/newInvoice/ApprovalDate"),
						"paymentDate"			:	oModel.getProperty("/newInvoice/PaymentDate"),
						"disbursementDate"		:	oModel.getProperty("/newInvoice/DisbursementDate"),
						"grossAmount"			:	oModel.getProperty("/newInvoice/GrossAmount"),
						"discountPercent"		:	oModel.getProperty("/newInvoice/DiscountPercent"),
						"financedAmount"		:	oModel.getProperty("/newInvoice/FinancedAmount"),
						"currency"				:	oModel.getProperty("/newInvoice/Currency")
				};
			} else {
				return {
						"ID"					:	oModel.getProperty("/selectedInvoice/ID"),
						"supplierID"			:	sup.ID,
						"purchaserID"			:	oModel.getProperty("/selectedInvoice/PurchaserID"),
						"financerID"			:	oModel.getProperty("/selectedInvoice/FinancerID"),
						"purchaseOrderID"		:	oModel.getProperty("/selectedInvoice/PurchaseOrderID"),
						"docType"				:	oModel.getProperty("/selectedInvoice/ObjectType"),
						"status"				:	oModel.getProperty("/selectedInvoice/Status"),
						"postingDate"			:	oModel.getProperty("/selectedInvoice/PostingDate")	,
						"approvalDate"			:	oModel.getProperty("/selectedInvoice/ApprovalDate"),
						"paymentDate"			:	oModel.getProperty("/selectedInvoice/PaymentDate"),
						"disbursementDate"		:	oModel.getProperty("/selectedInvoice/DisbursementDate"),
						"grossAmount"			:	oModel.getProperty("/selectedInvoice/GrossAmount"),
						"discountPercent"		:	oModel.getProperty("/selectedInvoice/DiscountPercent"),
						"financedAmount"		:	oModel.getProperty("/selectedInvoice/FinancedAmount"),
						"currency"				:	oModel.getProperty("/selectedInvoice/Currency")
				};				
			}
		},
		
		mapInvoiceToLocalStorage : function(oComponent, oModel){
			
			var sup = bizNetAccessSuppliers.getSupplierFromAlias(oComponent, oComponent.getModel("Login").getProperty("/alias"));
			return {
					"ID"					:	this.getNewInvoiceID(oModel),
					"supplierID"			:	sup.ID,
					"purchaserID"			:	oModel.getProperty("/newInvoice/PurchaserID"),
					"financerID"			:	oModel.getProperty("/newInvoice/FinancerID"),
					"purchaseOrderID"		:	oModel.getProperty("/newInvoice/PurchaseOrderID"),
					"docType"				:	"Asset.Invoice",
					"status"				:	"Approved",
					"postingDate"			:	oModel.getProperty("/newInvoice/PostingDate"),
					"approvalDate"			:	oModel.getProperty("/newInvoice/ApprovalDate"),
					"paymentDate"			:	oModel.getProperty("/newInvoice/PaymentDate"),
					"disbursementDate"		:	oModel.getProperty("/newInvoice/DisbursementDate"),
					"grossAmount"			:	oModel.getProperty("/newInvoice/GrossAmount"),
					"discountPercent"		:	oModel.getProperty("/newInvoice/DiscountPercent"),
					"financedAmount"		:	oModel.getProperty("/newInvoice/FinancedAmount"),
					"currency"				:	oModel.getProperty("/newInvoice/Currency")
			};
		},
		
		getNewInvoiceID:function(oModel){

			if ( typeof oModel.getProperty("/newInvoice/ID") === "undefined" ||
		    		oModel.getProperty("/newInvoice/ID") === ""
		    	){
			    var iD = "IN";
				var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
			    for( var i = 0; i < 8; i++ ){
			        iD += possible.charAt(Math.floor(Math.random() * possible.length));
			    }
		    } else {
				iD = oModel.getProperty("/newInvoice/ID");
			}
			oModel.setProperty("/newInvoice/ID",iD);
		    return iD;
		}
	};
});