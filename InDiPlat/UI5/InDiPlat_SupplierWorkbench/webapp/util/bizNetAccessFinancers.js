sap.ui.define([
	"Workbench/util/restBuilder",
	"Workbench/util/formatterFinancers",
	"Workbench/util/localStoreFinancers"
], function (
	restBuilder,
	formatterFinancers,
	localStoreFinancers
) {
	"use strict";

	return {

		loadAllFinancers: function (oComponent, oModel) {

			if (!(oComponent.getModel("TestSwitch").getProperty("/testMode"))) {
				restBuilder.execute(
					oModel.getProperty("/chaincode"),
					"readAll", [],
					function (responseData) {
						oModel.setProperty("/financerCollection/items", formatterFinancers.mapFinancersToModel(responseData));
					});
			} else {
				var sData = localStoreFinancers.getSampleData();
				oModel.setProperty("/financerCollection/items", formatterFinancers.mapFinancersToModel(sData));
			}
		},

		loadFinancer: function (oModel, ID) {

			oModel.setProperty(
				"/selectedFinancer",
				_.findWhere(oModel.getProperty("/financerCollection/items"), {
					ID: ID
				}, this));
		},

		addNewFinancer: function (oComponent, oModel) {

			if (!(oComponent.getModel("TestSwitch").getProperty("/testMode"))) {
				restBuilder.execute(
					oModel.getProperty("/chaincode"),
					"addNew",
					formatterFinancers.mapFinancerToChaincode(oModel, true)
				);
			} else {
				localStoreFinancers.put(formatterFinancers.mapFinancerToLocalStorage(oModel));
			}
			this.loadAllFinancers(oComponent, oModel);
			return oModel.getProperty("/newFinancer/ID");
		},

		removeFinancer: function (oComponent, oModel, financerID) {

			if (!(oComponent.getModel("TestSwitch").getProperty("/testMode"))) {
				restBuilder.execute(
					oModel.getProperty("/chaincode"),
					"remove", {
						ID: financerID
					}
				);
			} else {
				localStoreFinancers.remove(financerID);
			}
			this.loadAllFinancers(oComponent, oModel);
			return true;
		},

		removeAllFinancers: function (oComponent, oModel) {

			if (!(oComponent.getModel("TestSwitch").getProperty("/testMode"))) {
				restBuilder.execute(
					oModel.getProperty("/chaincode"),
					"removeAll", []
				);
			} else {
				localStoreFinancers.removeAll();
			}
			this.loadAllFinancers(oComponent, oModel);
			oModel.setProperty("/selectedFinancer", {});
			return true;
		},

		getFinancerFromAlias: function (oComponent, alias) {

			return _.findWhere(
				oComponent.getModel("Financers").getProperty("/financerCollection/items"), {
					Alias: alias
				},
				this
			);
		}
	};
});