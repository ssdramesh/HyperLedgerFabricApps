sap.ui.define([
	"Workbench/util/restBuilder",
	"Workbench/util/formatterPurchaseOrders",
	"Workbench/util/localStorePurchaseOrders"
], function (
	restBuilder,
	formatterPurchaseOrders,
	localStorePurchaseOrders
) {

	"use strict";

	return {

		loadAllPurchaseOrders: function (oComponent, oModel) {

			if (!(oComponent.getModel("TestSwitch").getProperty("/testMode"))) {
				restBuilder.execute(
					oModel.getProperty("/chaincode"),
					"readAll", [],
					function (responseData) {
						oModel.setProperty(
							"/purchaseOrderCollection/items",
							_.where(
								formatterPurchaseOrders.mapPurchaseOrdersToModel(responseData), {
									SupplierID: oModel.getProperty("/supplier/ID")
								},
								this
							)
						);
					});
			} else {
				var sData = localStorePurchaseOrders.getSampleData();
				oModel.setProperty(
					"/purchaseOrderCollection/items",
					_.where(
						formatterPurchaseOrders.mapPurchaseOrdersToModel(sData), {
							PurchaserID: oComponent.getModel("Login").getProperty("/id")
						},
						this
					)
				);
			}
		},

		loadPurchaseOrder: function (oModel, selectedPurchaseOrderID) {

			oModel.setProperty(
				"/selectedPurchaseOrder",
				_.findWhere(
					oModel.getProperty("/purchaseOrderCollection/items"), {
						ID: selectedPurchaseOrderID
					},
					this
				)
			);
		},

		addNewPurchaseOrder: function (oComponent, oModel) {

			if (!(oComponent.getModel("TestSwitch").getProperty("/testMode"))) {
				restBuilder.execute(
					oModel.getProperty("/chaincode"),
					"addNew",
					formatterPurchaseOrders.mapPurchaseOrderToChaincode(oModel, true)
				);
			} else {
				localStorePurchaseOrders.put(formatterPurchaseOrders.mapPurchaseOrderToLocalStorage(oModel));
			}
			this.loadAllPurchaseOrders(oComponent, oModel);
			return oModel.getProperty("/newPurchaseOrder/ID");
		},

		setPurchaseOrderPaid: function (oComponent, oModel, purchaseOrderID, paymentDate) {

			if (!(oComponent.getModel("TestSwitch").getProperty("/testMode"))) {
				restBuilder.execute(
					oModel.getProperty("/chaincode"),
					"setPurchaseOrderPaid",
					formatterPurchaseOrders.mapPurchaseOrderToChaincode(oModel)
				);
			} else {
				localStorePurchaseOrders.setPurchaseOrderPaid(purchaseOrderID, paymentDate);
			}
			this.loadAllPurchaseOrders(oComponent, oModel);
			return true;
		},

		removePurchaseOrder: function (oComponent, oModel, purchaseOrderID) {

			if (!(oComponent.getModel("TestSwitch").getProperty("/testMode"))) {
				restBuilder.execute(
					oModel.getProperty("/chaincode"),
					"remove", {
						ID: purchaseOrderID
					}
				);
			} else {
				localStorePurchaseOrders.remove(purchaseOrderID);
			}
			this.loadAllPurchaseOrders(oComponent, oModel);
			return true;
		},

		removeAllPurchaseOrders: function (oComponent, oModel) {

			if (!(oComponent.getModel("TestSwitch").getProperty("/testMode"))) {
				restBuilder.execute(
					oModel.getProperty("/chaincode"),
					"removeAll", []
				);
			} else {
				localStorePurchaseOrders.removeAll();
			}
			this.loadAllPurchaseOrders(oComponent, oModel);
			oModel.setProperty("/selectedPurchaseOrder", {});
			return true;
		}
	};
});