sap.ui.define([
	"Workbench/util/restBuilder",
	"Workbench/util/formatterInvoices",
	"Workbench/util/localStoreInvoices"
], function (
	restBuilder,
	formatterInvoices,
	localStoreInvoices
) {

	"use strict";

	return {

		loadAllInvoices: function (oComponent, oModel) {

			if (!(oComponent.getModel("TestSwitch").getProperty("/testMode"))) {
				restBuilder.execute(
					oModel.getProperty("/chaincode"),
					"readAll", [],
					function (responseData) {
						oModel.setProperty(
							"/invoiceCollection/items",
							_.where(
								formatterInvoices.mapInvoicesToModel(responseData), {
									SupplierID: oComponent.getModel("Login").getProperty("/id")
								},
								this
							)
						);
					});
			} else {
				var sData = localStoreInvoices.getSampleData();
				oModel.setProperty(
					"/invoiceCollection/items",
					_.where(
						formatterInvoices.mapInvoicesToModel(sData), {
							SupplierID: oComponent.getModel("Login").getProperty("/id")
						},
						this
					)
				);
			}
		},

		loadInvoice: function (oModel, selectedInvoiceID) {

			oModel.setProperty(
				"/selectedInvoice",
				_.findWhere(
					oModel.getProperty("/invoiceCollection/items"), {
						ID: selectedInvoiceID
					},
					this
				)
			);
		},

		addNewInvoice: function (oComponent, oModel) {

			if (!(oComponent.getModel("TestSwitch").getProperty("/testMode"))) {
				restBuilder.execute(
					oModel.getProperty("/chaincode"),
					"addNew",
					formatterInvoices.mapInvoiceToChaincode(oComponent, oModel, true)
				);
			} else {
				localStoreInvoices.put(formatterInvoices.mapInvoiceToLocalStorage(oComponent, oModel));
			}
			this.loadAllInvoices(oComponent, oModel);
			return oModel.getProperty("/newInvoice/ID");
		},

		setFinancingRequested: function (oComponent, oModel, invoiceID, financerID) {

			oModel.setProperty("/selectedInvoice/FinancerID", financerID);
			oModel.setProperty("/newInvoice/FinancerID", financerID);
			if (!(oComponent.getModel("TestSwitch").getProperty("/testMode"))) {
				restBuilder.execute(
					oModel.getProperty("/chaincode"),
					"requested",
					formatterInvoices.mapInvoiceToChaincode(oComponent, oModel, false)
				);
			} else {
				localStoreInvoices.setFinancingRequested(invoiceID, financerID);
			}
			this.loadAllInvoices(oComponent, oModel);
			return true;
		},

		setFinancingOffered: function (oComponent, oModel, invoiceID) {

			if (!(oComponent.getModel("TestSwitch").getProperty("/testMode"))) {
				restBuilder.execute(
					oModel.getProperty("/chaincode"),
					"offered",
					formatterInvoices.mapInvoiceToChaincode(oModel)
				);
			} else {
				localStoreInvoices.setFinancingOffered(invoiceID);
			}
			this.loadAllInvoices(oComponent, oModel);
			return true;
		},

		setFinancingAccepted: function (oComponent, oModel, invoiceID) {

			if (!(oComponent.getModel("TestSwitch").getProperty("/testMode"))) {
				restBuilder.execute(
					oModel.getProperty("/chaincode"),
					"accepted",
					formatterInvoices.mapInvoiceToChaincode(oModel)
				);
			} else {
				localStoreInvoices.setFinancingAccepted(invoiceID);
			}
			this.loadAllInvoices(oComponent, oModel);
			return true;
		},

		setFinancingCompleted: function (oComponent, oModel, invoiceID) {

			if (!(oComponent.getModel("TestSwitch").getProperty("/testMode"))) {
				restBuilder.execute(
					oModel.getProperty("/chaincode"),
					"completed",
					formatterInvoices.mapInvoiceToChaincode(oModel)
				);
			} else {
				localStoreInvoices.setFinancingCompleted(invoiceID);
			}
			this.loadAllInvoices(oComponent, oModel);
			return true;
		},

		removeInvoice: function (oComponent, oModel, invoiceID) {

			if (!(oComponent.getModel("TestSwitch").getProperty("/testMode"))) {
				restBuilder.execute(
					oModel.getProperty("/chaincode"),
					"remove", {
						ID: invoiceID
					}
				);
			} else {
				localStoreInvoices.remove(invoiceID);
			}
			this.loadAllInvoices(oComponent, oModel);
			return true;
		},

		removeAllInvoices: function (oComponent, oModel) {

			if (!(oComponent.getModel("TestSwitch").getProperty("/testMode"))) {
				restBuilder.execute(
					oModel.getProperty("/chaincode"),
					"removeAll", []
				);
			} else {
				localStoreInvoices.removeAll();
			}
			this.loadAllInvoices(oComponent, oModel);
			oModel.setProperty("/selectedInvoice", {});
			return true;
		}
	};
});