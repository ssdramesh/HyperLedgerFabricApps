sap.ui.define([
	"Workbench/util/bizNetAccessSuppliers"
], function (bizNetAccessSuppliers) {
	"use strict";

	return {

		mapFinancingRequestToModel: function (responseData) {
			return {
				ID: responseData.ID,
				SupplierID: responseData.supplierID,
				PurchaserID: responseData.purchaserID,
				FinancerID: responseData.financerID,
				InvoiceID: responseData.invoiceID,
				PurchaseOrderID: responseData.purchaseOrderID,
				ObjectType: responseData.docType,
				Status: responseData.status,
				InvoiceGrossAmount: responseData.invoiceGrossAmount,
				InvoiceDaysToCash: responseData.invoiceDaysToCash,
				InvoicePaymentDate: responseData.invoicePaymentDate,
				RequestCreationDate: responseData.requestCreationDate,
				RequestedDisbursementDate: responseData.requestedDisbursementDate,
				RequestedDiscountPercent: responseData.requestedDiscountPercent,
				RequestedFinancingAmount: responseData.requestedFinancingAmount,
				DocumentsChecked: responseData.documentsChecked,
				DocumentsVerified: responseData.documentsVerified,
				Currency: responseData.currency
			};
		},

		mapFinancingRequestsToModel: function (responseData) {

			var items = [];
			if (responseData) {
				for (var i = 0; i < responseData.length; i++) {
					items.push(this.mapFinancingRequestToModel(responseData[i]));
				}
			}
			return items;
		},

		mapFinancingRequestToChaincode: function (oComponent, oModel, newFinancingRequest) {

			var sup = bizNetAccessSuppliers.getSupplierFromAlias(oComponent, oComponent.getModel("Login").getProperty("/alias"));

			if (newFinancingRequest === true) {
				return {
					"ID": this.getNewFinancingRequestID(oModel),
					"supplierID": sup.ID,
					"purchaserID": oModel.getProperty("/newFinancingRequest/PurchaserID"),
					"financerID": oModel.getProperty("/newFinancingRequest/FinancerID"),
					"invoiceID": oModel.getProperty("/newFinancingRequest/InvoiceID"),
					"purchaseOrderID": oModel.getProperty("/newFinancingRequest/PurchaseOrderID"),
					"docType": "Asset.FinancingRequest",
					"status": "Created",
					"invoiceGrossAmount": oModel.getProperty("/newFinancingRequest/InvoiceGrossAmount"),
					"invoiceDaysToCash": oModel.getProperty("/newFinancingRequest/InvoiceDaysToCash"),
					"invoicePaymentDate": oModel.getProperty("/newFinancingRequest/InvoicePaymentDate"),
					"requestCreationDate": this._getCurrentDate(),
					"requestedDisbursementDate": oModel.getProperty("/newFinancingRequest/RequestedDisbursementDate"),
					"requestedDiscountPercent": oModel.getProperty("/newFinancingRequest/RequestedDiscountPercent"),
					"requestedFinancingAmount": oModel.getProperty("/newFinancingRequest/RequestedFinancingAmount"),
					"documentsChecked": oModel.getProperty("/newFinancingRequest/DocumentsChecked"),
					"documentsVerified": oModel.getProperty("/newFinancingRequest/DocumentsVerified"),
					"currency": oModel.getProperty("/newFinancingRequest/Currency")
				};
			} else {
				return {
					"ID": oModel.getProperty("/selectedFinancingRequest/ID"),
					"supplierID": sup.ID,
					"purchaserID": oModel.getProperty("/selectedFinancingRequest/PurchaserID"),
					"financerID": oModel.getProperty("/selectedFinancingRequest/FinancerID"),
					"invoiceID": oModel.getProperty("/selectedFinancingRequest/InvoiceID"),
					"purchaseOrderID": oModel.getProperty("/selectedFinancingRequest/PurchaseOrderID"),
					"docType": oModel.getProperty("/selectedFinancingRequest/ObjectType"),
					"status": oModel.getProperty("/selectedFinancingRequest/Status"),
					"invoiceGrossAmount": oModel.getProperty("/selectedFinancingRequest/InvoiceGrossAmount"),
					"invoiceDaysToCash": oModel.getProperty("/selectedFinancingRequest/InvoiceDaysToCash"),
					"invoicePaymentDate": oModel.getProperty("/selectedFinancingRequest/InvoicePaymentDate"),
					"requestCreationDate": this._getCurrentDate(),
					"requestedDisbursementDate": oModel.getProperty("/selectedFinancingRequest/RequestedDisbursementDate"),
					"requestedDiscountPercent": oModel.getProperty("/selectedFinancingRequest/RequestedDiscountPercent"),
					"requestedFinancingAmount": oModel.getProperty("/selectedFinancingRequest/RequestedFinancingAmount"),
					"documentsChecked": oModel.getProperty("/selectedFinancingRequest/DocumentsChecked"),
					"documentsVerified": oModel.getProperty("/selectedFinancingRequest/DocumentsVerified"),
					"currency": oModel.getProperty("/selectedFinancingRequest/Currency")
				};
			}
		},

		mapFinancingRequestToLocalStorage: function (oComponent, oModel) {

			var sup = bizNetAccessSuppliers.getSupplierFromAlias(oComponent, oComponent.getModel("Login").getProperty("/alias"));

			return {
				"ID": this.getNewFinancingRequestID(oModel),
				"supplierID": sup.ID,
				"purchaserID": oModel.getProperty("/newFinancingRequest/PurchaserID"),
				"financerID": oModel.getProperty("/newFinancingRequest/FinancerID"),
				"invoiceID": oModel.getProperty("/newFinancingRequest/InvoiceID"),
				"purchaseOrderID": oModel.getProperty("/newFinancingRequest/PurchaseOrderID"),
				"docType": "Asset.FinancingRequest",
				"status": "Created",
				"invoiceGrossAmount": oModel.getProperty("/newFinancingRequest/InvoiceGrossAmount"),
				"invoiceDaysToCash": oModel.getProperty("/newFinancingRequest/InvoiceDaysToCash"),
				"invoicePaymentDate": oModel.getProperty("/newFinancingRequest/InvoicePaymentDate"),
				"requestCreationDate": this._getCurrentDate(),
				"requestedDisbursementDate": oModel.getProperty("/newFinancingRequest/RequestedDisbursementDate"),
				"requestedDiscountPercent": oModel.getProperty("/newFinancingRequest/RequestedDiscountPercent"),
				"requestedFinancingAmount": oModel.getProperty("/newFinancingRequest/RequestedFinancingAmount"),
				"documentsChecked": false,
				"documentsVerified": false,
				"currency": oModel.getProperty("/newFinancingRequest/Currency")
			};
		},

		_getCurrentDate: function () {

			var today = new Date();
			return (today.getMonth() + 1) + "/" + today.getDate() + "/" + today.getFullYear();
		},

		getNewFinancingRequestID: function (oModel) {

			if (typeof oModel.getProperty("/newFinancingRequest/ID") === "undefined" ||
				oModel.getProperty("/newFinancingRequest/ID") === ""
			) {
				var iD = "FR";
				var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
				for (var i = 0; i < 8; i++) {
					iD += possible.charAt(Math.floor(Math.random() * possible.length));
				}
			} else {
				iD = oModel.getProperty("/newFinancingRequest/ID");
			}
			oModel.setProperty("/newFinancingRequest/ID", iD);
			return iD;
		}
	};
});