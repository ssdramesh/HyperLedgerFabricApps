sap.ui.define([
	"Workbench/util/restBuilder",
	"Workbench/util/formatterFinancingOffers",
	"Workbench/util/localStoreFinancingOffers"
], function (
	restBuilder,
	formatterFinancingOffers,
	localStoreFinancingOffers
) {

	"use strict";

	return {

		//For Supplier side, we filter by Status
		loadAllFinancingOffers: function (oComponent, oModel) {

			if (!(oComponent.getModel("TestSwitch").getProperty("/testMode"))) {
				restBuilder.execute(
					oModel.getProperty("/chaincode"),
					"readAll", [],
					function (responseData) {
						oModel.setProperty(
							"/financingOfferCollection/items",
							_.where(
								formatterFinancingOffers.mapFinancingOffersToModel(responseData), {
									SupplierID: oModel.getProperty("/supplier/ID"),
									Status: "Submitted"
								},
								this
							)
						);
					});
			} else {
				var sData = localStoreFinancingOffers.getSampleData();
				oModel.setProperty(
					"/financingOfferCollection/items",
					_.where(
						formatterFinancingOffers.mapFinancingOffersToModel(sData), {
							SupplierID: oComponent.getModel("Login").getProperty("/id"),
							Status: "Submitted"
						},
						this
					)
				);
			}
		},

		loadFinancingOffer: function (oModel, selectedFinancingOfferID) {

			oModel.setProperty(
				"/selectedFinancingOffer",
				_.findWhere(
					oModel.getProperty("/financingOfferCollection/items"), {
						ID: selectedFinancingOfferID
					},
					this
				)
			);
		},

		addNewFinancingOffer: function (oComponent, oModel) {

			if (!(oComponent.getModel("TestSwitch").getProperty("/testMode"))) {
				restBuilder.execute(
					oModel.getProperty("/chaincode"),
					"addNew",
					formatterFinancingOffers.mapFinancingOfferToChaincode(oModel, true)
				);
			} else {
				localStoreFinancingOffers.put(formatterFinancingOffers.mapFinancingOfferToLocalStorage(oModel));
			}
			this.loadAllFinancingOffers(oComponent, oModel);
			return oModel.getProperty("/newFinancingOffer/ID");
		},

		setFinancingOfferSubmitted: function (oComponent, oModel, financingOfferID) {

			if (!(oComponent.getModel("TestSwitch").getProperty("/testMode"))) {
				restBuilder.execute(
					oModel.getProperty("/chaincode"),
					"submit",
					formatterFinancingOffers.mapFinancingOfferToChaincode(oModel)
				);
			} else {
				localStoreFinancingOffers.setFinancingOfferSubmitted(financingOfferID);
			}
			this.loadAllFinancingOffers(oComponent, oModel);
			return true;
		},

		setFinancingOfferAccepted: function (oComponent, oModel, financingOfferID) {

			if (!(oComponent.getModel("TestSwitch").getProperty("/testMode"))) {
				restBuilder.execute(
					oModel.getProperty("/chaincode"),
					"accept",
					formatterFinancingOffers.mapFinancingOfferToChaincode(oModel)
				);
			} else {
				localStoreFinancingOffers.setFinancingOfferAccepted(financingOfferID);
			}
			this.loadAllFinancingOffers(oComponent, oModel);
			return true;
		},

		setFinancingOfferDeclined: function (oComponent, oModel, financingOfferID) {

			if (!(oComponent.getModel("TestSwitch").getProperty("/testMode"))) {
				restBuilder.execute(
					oModel.getProperty("/chaincode"),
					"decline",
					formatterFinancingOffers.mapFinancingOfferToChaincode(oModel)
				);
			} else {
				localStoreFinancingOffers.setFinancingOfferDeclined(financingOfferID);
			}
			this.loadAllFinancingOffers(oComponent, oModel);
			return true;
		},

		setFinancingOfferDisbursementInitiated: function (oComponent, oModel, financingOfferID) {

			if (!(oComponent.getModel("TestSwitch").getProperty("/testMode"))) {
				restBuilder.execute(
					oModel.getProperty("/chaincode"),
					"disburse",
					formatterFinancingOffers.mapFinancingOfferToChaincode(oModel)
				);
			} else {
				localStoreFinancingOffers.setFinancingOfferDisbursementInitiated(financingOfferID);
			}
			this.loadAllFinancingOffers(oComponent, oModel);
			return true;
		},

		removeFinancingOffer: function (oComponent, oModel, financingOfferID) {

			if (!(oComponent.getModel("TestSwitch").getProperty("/testMode"))) {
				restBuilder.execute(
					oModel.getProperty("/chaincode"),
					"remove", {
						ID: financingOfferID
					}
				);
			} else {
				localStoreFinancingOffers.remove(financingOfferID);
			}
			this.loadAllFinancingOffers(oComponent, oModel);
			return true;
		},

		removeAllFinancingOffers: function (oComponent, oModel) {

			if (!(oComponent.getModel("TestSwitch").getProperty("/testMode"))) {
				restBuilder.execute(
					oModel.getProperty("/chaincode"),
					"removeAll", []
				);
			} else {
				localStoreFinancingOffers.removeAll();
			}
			this.loadAllFinancingOffers(oComponent, oModel);
			oModel.setProperty("/selectedFinancingOffer", {});
			return true;
		}
	};
});