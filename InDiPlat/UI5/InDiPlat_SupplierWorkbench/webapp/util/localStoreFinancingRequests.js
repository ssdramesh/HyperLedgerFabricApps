sap.ui.define(function() {
	"use strict";

	jQuery.sap.require("jquery.sap.storage");
	var oStorage = jQuery.sap.storage(jQuery.sap.storage.Type.local);
	var sampleDataID = "financingRequests";

	return {

		init: function() {

			this.removeAll();
			oStorage.put(
				sampleDataID, 
				[
					{
					    "ID": "FR100001",
					    "supplierID": "SU100001",
					    "purchaserID": "PU100001",
					    "financerID": "FI100001",
					    "invoiceID": "IN100001",
					    "purchaseOrderID": "PO100001",
					    "docType": "Asset.FinancingRequest",
					    "status": "Created",
					    "invoiceGrossAmount": "6754.34",
					    "invoiceDaysToCash": "30",
					    "invoicePaymentDate": "01/02/2017",
					    "requestCreationDate": "01/03/2017",
					    "requestedDisbursementDate": "01/03/2017",
					    "requestedDiscountPercent": "1.75",
					    "requestedFinancingAmount": "6765.34",
					    "documentsChecked": "true",
					    "documentsVerified": "true",
					    "currency": "SGD"
					  },
					  {
					    "ID": "FR100002",
					    "supplierID": "SU100002",
					    "purchaserID": "PU100002",
					    "financerID": "FI100002",
					    "invoiceID": "IN100002",
					    "purchaseOrderID": "PO100002",
					    "docType": "Asset.FinancingRequest",
					    "status": "Created",
					    "invoiceGrossAmount": "5674.50",
					    "invoiceDaysToCash": "45",
					    "invoicePaymentDate": "03/20/2017",
					    "requestCreationDate": "02/04/2017",
					    "requestedDisbursementDate": "02/05/2017",
					    "requestedDiscountPercent": "1.45",
					    "requestedFinancingAmount": "5674.50",
					    "documentsChecked": "true",
					    "documentsVerified": "true",
					    "currency": "SGD"
					  }
				]
			);
		},

		getSampleDataID: function() {

			return sampleDataID;
		},

		getSampleData: function() {

			return oStorage.get("financingRequests");
		},

		put: function(newFinancingRequest) {

			var sampleData = this.getSampleData();
			sampleData.push(newFinancingRequest);
			oStorage.put(sampleDataID, sampleData);
		},
		
		setFinancingRequestInProcess:function(financingRequestID, requestedDisbursementDate, requestedDiscountPercent, requestedFinancingAmount){
		
			var financingRequest = _.findWhere(this.getSampleData(), {ID:financingRequestID});
			financingRequest.status = "In Process";
			financingRequest.requestedDisbursementDate = requestedDisbursementDate;
			financingRequest.requestedDiscountPercent  = requestedDiscountPercent;
			financingRequest.requestedFinancingAmount  = requestedFinancingAmount;
			this.remove(financingRequestID);
			this.put(financingRequest);
		},
		
		setFinancingRequestAccepted:function(financingRequestID, requestedDiscountPercent, requestedFinancingAmount){
		
			var financingRequest = _.findWhere(this.getSampleData(), {ID:financingRequestID});
			financingRequest.status = "Accepted";
			financingRequest.requestedDiscountPercent = requestedDiscountPercent;
			financingRequest.requestedFinancingAmount = requestedFinancingAmount;
			this.remove(financingRequestID);
			this.put(financingRequest);
		},
		
		setFinancingRequestDeclined:function(financingRequestID){
		
			var financingRequest = _.findWhere(this.getSampleData(), {ID:financingRequestID});
			financingRequest.status = "Declined";
			this.remove(financingRequestID);
			this.put(financingRequest);
		},		
		
		setFinancingRequestDocumentsChecked:function(financingRequestID){
		
			var financingRequest = _.findWhere(this.getSampleData(), {ID:financingRequestID});
			financingRequest.documentsChecked = "true";
			this.remove(financingRequestID);
			this.put(financingRequest);
		},		
		
		setFinancingRequestDocumentsVerified:function(financingRequestID){
		
			var financingRequest = _.findWhere(this.getSampleData(), {ID:financingRequestID});
			financingRequest.documentsVerified = "true";
			this.remove(financingRequestID);
			this.put(financingRequest);
		},		
		
		remove : function (id){
		
			var sampleData = this.getSampleData();
			sampleData = _.without(sampleData,_.findWhere(sampleData,{ID:id}));
			oStorage.put(sampleDataID, sampleData);
		},
		
		removeAll : function(){
		
			oStorage.put(sampleDataID,[]);	
		},		

		clearSampleData: function() {

			oStorage.clear();
		}
	};
});