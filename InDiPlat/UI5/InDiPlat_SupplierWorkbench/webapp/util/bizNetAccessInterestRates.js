sap.ui.define([
	"Workbench/util/restBuilder",
	"Workbench/util/formatterInterestRates",
	"Workbench/util/localStoreInterestRates"
], function (
	restBuilder,
	formatterInterestRates,
	localStoreInterestRates
) {
	"use strict";

	return {

		loadAllInterestRates: function (oComponent, oModel) {

			if (!(oComponent.getModel("TestSwitch").getProperty("/testMode"))) {
				restBuilder.execute(
					oModel.getProperty("/chaincode"),
					"readAll", [],
					function (responseData) {
						oModel.setProperty("/interestRateCollection/items", formatterInterestRates.mapInterestRatesToModel(responseData));
					});
			} else {
				var sData = localStoreInterestRates.getSampleData();
				oModel.setProperty("/interestRateCollection/items", formatterInterestRates.mapInterestRatesToModel(sData));
			}
		},

		loadInterestRate: function (oModel, ID) {

			oModel.setProperty(
				"/selectedInterestRate",
				_.findWhere(oModel.getProperty("/interestRateCollection/items"), {
					ID: ID
				}, this));
		},

		addNewInterestRate: function (oComponent, oModel) {

			if (!(oComponent.getModel("TestSwitch").getProperty("/testMode"))) {
				restBuilder.execute(
					oModel.getProperty("/chaincode"),
					"addNew",
					formatterInterestRates.mapInterestRateToChaincode(oModel, true)
				);
			} else {
				localStoreInterestRates.put(formatterInterestRates.mapInterestRateToLocalStorage(oModel));
			}
			this.loadAllInterestRates(oComponent, oModel);
			return oModel.getProperty("/newInterestRate/ID");
		},

		updateInterestRate: function (oComponent, oModel) {

			if (!(oComponent.getModel("TestSwitch").getProperty("/testMode"))) {
				restBuilder.execute(
					oModel.getProperty("/chaincode"),
					"update",
					formatterInterestRates.mapInterestRateToChaincode(oModel)
				);
			} else {
				localStoreInterestRates.put(formatterInterestRates.mapInterestRateToLocalStorage(oModel));
			}
			this.loadAllInterestRates(oComponent, oModel);
			return oModel.getProperty("/newInterestRate/ID");
		},

		removeInterestRate: function (oComponent, oModel, interestRateID) {

			if (!(oComponent.getModel("TestSwitch").getProperty("/testMode"))) {
				restBuilder.execute(
					oModel.getProperty("/chaincode"),
					"remove", {
						ID: interestRateID
					}
				);
			} else {
				localStoreInterestRates.remove(interestRateID);
			}
			this.loadAllInterestRates(oComponent, oModel);
			return true;
		},

		removeAllInterestRates: function (oComponent, oModel) {

			if (!(oComponent.getModel("TestSwitch").getProperty("/testMode"))) {
				restBuilder.execute(
					oModel.getProperty("/chaincode"),
					"removeAll", []
				);
			} else {
				localStoreInterestRates.removeAll();
			}
			this.loadAllInterestRates(oComponent, oModel);
			oModel.setProperty("/selectedInterestRate", {});
			return true;
		}
	};
});