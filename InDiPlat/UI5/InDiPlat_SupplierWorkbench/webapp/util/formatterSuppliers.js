sap.ui.define(function() {
	"use strict";

	return {

		mapSupplierToModel:function(responseData){
			return {
				ID						: responseData.ID,
				ObjectType 				: responseData.docType,
				Alias					: responseData.alias,
				Description				: responseData.description
			};
		},

		mapSuppliersToModel:function(responseData){
			
			var items = [];
			if( responseData ){
				for ( var i = 0; i < responseData.length; i++ ){
					items.push(this.mapSupplierToModel(responseData[i]));
				}
			}
			return items;
		},

		mapSupplierToChaincode:function(oModel, newSupplier){
			if ( newSupplier === true ) {
				return {
						"ID"			:	this.getNewSupplierID(oModel),
						"docType"		:	"Participant.Supplier",
						"alias"			:	oModel.getProperty("/newSupplier/Alias"),
						"description"	:	oModel.getProperty("/newSupplier/Description")
				};
			} else {
				return {
						"ID"			:	oModel.getProperty("/selectedSupplier/ID"),
						"docType"		:	oModel.getProperty("/selectedSupplier/ObjectType"),
						"alias"			:	oModel.getProperty("/selectedSupplier/Alias"),
						"description"	:	oModel.getProperty("/selectedSupplier/Description")
				};				
			}
		},
		
		mapSupplierToLocalStorage : function(oModel){
			return {
					"ID"					:	this.getNewSupplierID(oModel.getProperty("/newSupplier/ID")),
					"docType"				:	"Participant.Supplier",
					"alias"					:	oModel.getProperty("/newSupplier/Alias"),
					"description"			:	oModel.getProperty("/newSupplier/Description")
			};
		},
		
		getNewSupplierID:function(oModel){
			if ( typeof oModel.getProperty("/newSupplier/ID") === "undefined" ||
		    		oModel.getProperty("/newSupplier/ID") === ""
		    	){
			    var iD = "SU"; //adapt entity abbreviation
				var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
			    for( var i = 0; i < 8; i++ ){
			        iD += possible.charAt(Math.floor(Math.random() * possible.length));
			    }
		    } else {
				iD = oModel.getProperty("/newSupplier/ID");
			}
			oModel.setProperty("/newSupplier/ID",iD);
		    return iD;
		}
	};
});