sap.ui.define(function() {
	"use strict";

	return {

		mapPurchaseOrderToModel:function(responseData){
			return {
				ID						: responseData.ID,
				PurchaserID				: responseData.purchaserID,
				SupplierID				: responseData.supplierID,
				ObjectType 				: responseData.docType,
				Status					: responseData.status,
				PostingDate				: responseData.postingDate,
				ApprovalDate			: responseData.approvalDate,
				PaymentDate				: responseData.paymentDate,
				GrossAmount				: responseData.grossAmount,
				TotalSubmittedAmount	: responseData.totalSubmittedAmount,
				Currency				: responseData.currency
			};
		},

		mapPurchaseOrdersToModel:function(responseData){
			
			var items = [];
			if( responseData ){
				for ( var i = 0; i < responseData.length; i++ ){
					items.push(this.mapPurchaseOrderToModel(responseData[i]));
				}
			}
			return items;
		},

		mapPurchaseOrderToChaincode:function(oModel, newPurchaseOrder){
			
			if ( newPurchaseOrder === true ) {
				return {
						"ID"					:	this.getNewPurchaseOrderID(oModel),
						"purchaserID"			:	oModel.getProperty("/purchaser/ID"),
						"supplierID"			:	oModel.getProperty("/newPurchaseOrder/SupplierID"),
						"docType"				:	"Asset.PurchaseOrder",
						"status"				:	"Approved",
						"postingDate"			:	oModel.getProperty("/newPurchaseOrder/PostingDate")	,
						"approvalDate"			:	oModel.getProperty("/newPurchaseOrder/ApprovalDate"),
						"paymentDate"			:	oModel.getProperty("/newPurchaseOrder/PaymentDate"),
						"grossAmount"			:	oModel.getProperty("/newPurchaseOrder/GrossAmount"),
						"totalSubmittedAmount"	:	oModel.getProperty("/newPurchaseOrder/TotalSubmittedAmount"),
						"currency"				:	oModel.getProperty("/newPurchaseOrder/Currency")
				};
			} else {
				return {
						"ID"					:	oModel.getProperty("/selectedPurchaseOrder/ID"),
						"purchaserID"			:	oModel.getProperty("/purchaser/ID"),
						"supplierID"			:	oModel.getProperty("/selectedPurchaseOrder/SupplierID"),
						"docType"				:	oModel.getProperty("/selectedPurchaseOrder/ObjectType"),
						"status"				:	oModel.getProperty("/selectedPurchaseOrder/Status"),
						"postingDate"			:	oModel.getProperty("/selectedPurchaseOrder/PostingDate"),
						"approvalDate"			:	oModel.getProperty("/selectedPurchaseOrder/ApprovalDate"),
						"paymentDate"			:	oModel.getProperty("/selectedPurchaseOrder/PaymentDate"),
						"grossAmount"			:	oModel.getProperty("/selectedPurchaseOrder/GrossAmount"),
						"totalSubmittedAmount"	:	oModel.getProperty("/selectedPurchaseOrder/TotalSubmittedAmount"),
						"currency"				:	oModel.getProperty("/selectedPurchaseOrder/Currency")
				};				
			}
		},
		
		mapPurchaseOrderToLocalStorage : function(oModel){
				
			return {
					"ID"					:	this.getNewPurchaseOrderID(oModel),
					"purchaserID"			:	oModel.getProperty("/purchaser/ID"),
					"supplierID"			:	oModel.getProperty("/newPurchaseOrder/SupplierID"),
					"docType"				:	"Asset.PurchaseOrder",
					"status"				:	"Approved",
					"postingDate"			:	oModel.getProperty("/newPurchaseOrder/PostingDate"),
					"approvalDate"			:	oModel.getProperty("/newPurchaseOrder/ApprovalDate"),
					"paymentDate"			:	oModel.getProperty("/newPurchaseOrder/PaymentDate"),
					"grossAmount"			:	oModel.getProperty("/newPurchaseOrder/GrossAmount"),
					"totalSubmittedAmount"	:	oModel.getProperty("/newPurchaseOrder/TotalSubmittedAmount"),
					"currency"				:	oModel.getProperty("/newPurchaseOrder/Currency")
			};
		},
		
		getNewPurchaseOrderID:function(oModel){

			if ( typeof oModel.getProperty("/newPurchaseOrder/ID") === "undefined" ||
		    		oModel.getProperty("/newPurchaseOrder/ID") === ""
		    	){
			    var iD = "PU";
				var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
			    for( var i = 0; i < 8; i++ ){
			        iD += possible.charAt(Math.floor(Math.random() * possible.length));
			    }
		    } else {
				iD = oModel.getProperty("/newPurchaseOrder/ID");
			}
			oModel.setProperty("/newPurchaseOrder/ID",iD);
		    return iD;
		}
	};
});