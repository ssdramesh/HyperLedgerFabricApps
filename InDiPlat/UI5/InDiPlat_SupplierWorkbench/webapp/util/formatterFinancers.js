sap.ui.define(function() {
	"use strict";

	return {

		mapFinancerToModel:function(responseData){
			return {
				ID						: responseData.ID,
				ObjectType 				: responseData.docType,
				Alias					: responseData.alias,
				Description				: responseData.description
			};
		},

		mapFinancersToModel:function(responseData){
			
			var items = [];
			if( responseData ){
				for ( var i = 0; i < responseData.length; i++ ){
					items.push(this.mapFinancerToModel(responseData[i]));
				}
			}
			return items;
		},

		mapFinancerToChaincode:function(oModel, newFinancer){
			
			if ( newFinancer === true ) {
				return {
						"ID"					:	this.getNewFinancerID(oModel.getProperty("/newFinancer/ID")),
						"docType"				:	"Participant.Financer",
						"alias"					:	oModel.getProperty("/newFinancer/Alias"),
						"description"			:	oModel.getProperty("/newFinancer/Description")
				};
			} else {
				return {
						"ID"					:	oModel.getProperty("/selectedFinancer/ID"),
						"docType"				:	oModel.getProperty("/selectedFinancer/ObjectType"),
						"alias"					:	oModel.getProperty("/selectedFinancer/Alias"),
						"description"			:	oModel.getProperty("/selectedFinancer/Description")
				};				
			}
		},
		
		mapFinancerToLocalStorage : function(oModel){
				
			return {
					"ID"					:	this.getNewFinancerID(oModel.getProperty("/newFinancer/ID")),
					"docType"				:	"Participant.Financer",
					"alias"					:	oModel.getProperty("/newFinancer/Alias"),
					"description"			:	oModel.getProperty("/newFinancer/Description")
			};
		},
		
		getNewPurchaseOrderID:function(oModel){

			if ( typeof oModel.getProperty("/newFinancer/ID") === "undefined" ||
		    		oModel.getProperty("/newFinancer/ID") === ""
		    	){
			    var iD = "FI";
				var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
			    for( var i = 0; i < 8; i++ ){
			        iD += possible.charAt(Math.floor(Math.random() * possible.length));
			    }
		    } else {
				iD = oModel.getProperty("/newFinancer/ID");
			}
			oModel.setProperty("/newFinancer/ID",iD);
		    return iD;
		}
	};
});