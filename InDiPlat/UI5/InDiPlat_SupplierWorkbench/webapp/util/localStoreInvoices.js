sap.ui.define(function() {
	"use strict";

	jQuery.sap.require("jquery.sap.storage");
	var oStorage = jQuery.sap.storage(jQuery.sap.storage.Type.local);
	var sampleDataID = "invoices";

	return {

		init: function() {

			this.removeAll();
			oStorage.put(
				sampleDataID, 
				[
					{
					    "ID": "IN100001",
					    "supplierID": "SU100001",
					    "purchaserID": "PU100001",
					    "financerID": "FI100001",
					    "purchaseOrderID": "PO100001",
					    "docType": "Asset.Invoice",
					    "status": "Approved",
					    "postingDate": "01/02/2017",
					    "approvalDate": "01/02/2017",
					    "paymentDate": "02/02/2017",
					    "disbursementDate": "",
					    "grossAmount": "6754.34",
					    "discountPercent": "",
					    "financedAmount": "",
					    "currency": "SGD"
					  },
					  {
					    "ID": "IN100002",
					    "supplierID": "SU100001",
					    "purchaserID": "PU100002",
					    "financerID": "FI100002",
					    "purchaseOrderID": "PO100002",
					    "docType": "Asset.Invoice",
					    "status": "Approved",
					    "postingDate": "02/03/2017",
					    "approvalDate": "02/04/2017",
					    "paymentDate": "03/20/2017",
					    "disbursementDate": "",
					    "grossAmount": "5674.50",
					    "discountPercent": "",
					    "financedAmount": "",
					    "currency": "SGD"
					  }
				]
			);
		},

		getSampleDataID: function() {

			return sampleDataID;
		},

		getSampleData: function() {

			return oStorage.get("invoices");
		},

		put: function(newInvoice) {

			var sampleData = this.getSampleData();
			sampleData.push(newInvoice);
			oStorage.put(sampleDataID, sampleData);
		},
		
		setFinancingRequested:function(invoiceID, financerID){
		
			var invoice = _.findWhere(this.getSampleData(), {ID:invoiceID});
			invoice.status = "Financing Requested";
			invoice.financerID = financerID;
			this.remove(invoiceID);
			this.put(invoice);
		},
		
		setFinancingOffered:function(invoiceID){
		
			var invoice = _.findWhere(this.getSampleData(), {ID:invoiceID});
			invoice.status = "Financing Offered";
			this.remove(invoiceID);
			this.put(invoice);			
		},
		setFinancingAccepted:function(invoiceID){
		
			var invoice = _.findWhere(this.getSampleData(), {ID:invoiceID});
			invoice.status = "Financing Accepted";
			this.remove(invoiceID);
			this.put(invoice);			
		},
		
		setFinancingCompleted:function(invoiceID){
		
			var invoice = _.findWhere(this.getSampleData(), {ID:invoiceID});
			invoice.status = "Financing Completed";
			this.remove(invoiceID);
			this.put(invoice);			
		},
		
		remove : function (id){
		
			var sampleData = this.getSampleData();
			sampleData = _.without(sampleData,_.findWhere(sampleData,{ID:id}));
			oStorage.put(sampleDataID, sampleData);
		},
		
		removeAll : function(){
		
			oStorage.put(sampleDataID,[]);	
		},		

		clearSampleData: function() {

			oStorage.clear();
		}
	};
});