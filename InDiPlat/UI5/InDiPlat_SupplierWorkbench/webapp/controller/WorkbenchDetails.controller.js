sap.ui.define([
	"Workbench/controller/BaseController",
	"Workbench/model/modelsFinancingOffers",
	"Workbench/model/modelsSuppliers",
	"Workbench/model/modelsPurchasers",
	"Workbench/util/messageProvider",
	"Workbench/util/localStoreSuppliers",
	"Workbench/util/localStoreInvoices",
	"Workbench/util/localStoreFinancingOffers",
	"Workbench/util/localStorePurchasers",
	"Workbench/util/bizNetAccessSuppliers",
	"Workbench/util/bizNetAccessInvoices",
	"Workbench/util/bizNetAccessFinancingOffers",
	"Workbench/util/bizNetAccessPurchasers"
], function (
	BaseController,
	modelsFinancingOffers,
	modelsSuppliers,
	modelsPurchasers,
	messageProvider,
	localStoreSuppliers,
	localStoreInvoices,
	localStoreFinancingOffers,
	localStorePurchasers,
	bizNetAccessSuppliers,
	bizNetAccessInvoices,
	bizNetAccessFinancingOffers,
	bizNetAccessPurchasers
) {

	"use strict";

	return BaseController.extend("Workbench.controller.WorkbenchDetails", {

		onInit: function () {

			this.getOwnerComponent().getRouter().getRoute("invoice").attachPatternMatched(this._onObjectMatched, this);
			messageProvider.init(this.getOwnerComponent().getModel("Messages"));
			this._loadModels();
		},

		onAfterRendering: function () {

			this._setActions();
		},

		onAddNew: function () {

			var oModel = this.getView().getModel("Invoices");
			if (oModel.getProperty("/newInvoice/PurchaserID") === "" ||
				oModel.getProperty("/newInvoice/PostingDate") === "" ||
				oModel.getProperty("/newInvoice/ApprovalDate") === "" ||
				oModel.getProperty("/newInvoice/PaymentDate") === "" ||
				oModel.getProperty("/newInvoice/GrossAmount") === "" ||
				oModel.getProperty("/newInvoice/Currency") === "") {

				sap.m.MessageToast.show("Please enter valid data in all fields", {});
			} else {
				var invoiceId = bizNetAccessInvoices.addNewInvoice(this.getOwnerComponent(), oModel);
				this.getView().byId("__barInvoice").setSelectedKey("Details");
				bizNetAccessInvoices.loadInvoice(this.getView().getModel("Invoices"), invoiceId);
				this.getView().getModel("Invoices").setProperty("/newInvoice", {});
			}
		},

		onRequestFinancing: function () {

			var oModel = this.getModel("Invoices");
			this.getRouter().navTo("financingRequests", {
				alias: this.getModel("Login").getProperty("/alias"),
				invoiceId: oModel.getProperty("/selectedInvoiceID")
			}, true);
		},

		onFinancingOffersOverview: function () {

			var oModel = this.getModel("Invoices");
			this.getRouter().navTo("financingOffers", {
				alias: this.getModel("Login").getProperty("/alias"),
				invoiceId: oModel.getProperty("/selectedInvoiceID")
			}, true);
		},

		removeInvoice: function () {

			var oModel = this.getView().getModel("Invoices");
			bizNetAccessInvoices.removeInvoice(this.getOwnerComponent(), oModel, oModel.getProperty("/selectedInvoiceID"));
		},

		_onObjectMatched: function (oEvent) {

			var pId = oEvent.getParameter("arguments").invoiceId;
			if (pId === "___new") {
				this.getView().byId("__barInvoice").setSelectedKey("New");
			} else {
				this.getModel("Invoices").setProperty("/selectedInvoiceID", pId);
				this.getModel("Invoices").setProperty("/searchInvoiceID", pId);
				bizNetAccessInvoices.loadInvoice(this.getModel("Invoices"), pId);
			}
			this._setActions();
		},

		_setActions: function () {

			var invoice = this.getModel("Invoices").getProperty("/selectedInvoice");
			if (_.isEmpty(invoice)) {
				this.getView().byId("__buttonFinancingRequestsOverview").setVisible(false);
				this.getView().byId("__buttonFinancingOffersOverview").setVisible(false);
			}
			switch (invoice.Status) {
			case "Approved":
				this.getView().byId("__buttonFinancingRequestsOverview").setVisible(true);
				this.getView().byId("__buttonFinancingOffersOverview").setVisible(this._haveFinancingOffers());
				break;
			case "Financing Requested":
				this.getView().byId("__buttonFinancingRequestsOverview").setVisible(true);
				this.getView().byId("__buttonFinancingOffersOverview").setVisible(this._haveFinancingOffers());
				break;
			case "Financing Offered":
				this.getView().byId("__buttonFinancingRequestsOverview").setVisible(true);
				this.getView().byId("__buttonFinancingOffersOverview").setVisible(this._haveFinancingOffers());
				break;
			case "Financing Accepted":
				this.getView().byId("__buttonFinancingRequestsOverview").setVisible(false);
				this.getView().byId("__buttonFinancingOffersOverview").setVisible(this._haveFinancingOffers());
				break;
			case "Financing Completed":
				this.getView().byId("__buttonFinancingRequestsOverview").setVisible(false);
				this.getView().byId("__buttonFinancingOffersOverview").setVisible(this._haveFinancingOffers());
				break;
			}
		},

		_loadModels: function () {

			//Load all Suppliers and logged in supplier			
			this.getOwnerComponent().setModel(modelsSuppliers.createSuppliersModel(), "Suppliers");
			this.loadEntityMetaData("supplier", this.getModel("Suppliers"));
			bizNetAccessSuppliers.loadAllSuppliers(this.getOwnerComponent(), this.getModel("Suppliers"));

			//Set currently logged in Supplier
			if (this.getModel("Login").getProperty("/alias") !== "") {
				var sup = bizNetAccessSuppliers.getSupplierFromAlias(this, this.getModel("Login").getProperty("/alias"));
				this.getOwnerComponent().getModel("Login").setProperty("/id", sup.ID);
				bizNetAccessSuppliers.loadSupplier(this.getModel("Suppliers"), sup.ID);
			}

			if (typeof this.getOwnerComponent().getModel("FinancingOffers") === "undefined") {
				this.getOwnerComponent().setModel(modelsFinancingOffers.createFinancingOffersModel(), "FinancingOffers");
				this.loadEntityMetaData("financingOffer", this.getModel("FinancingOffers"));
				localStoreFinancingOffers.init();
			}
			if (typeof sup !== "undefined") {
				this.getModel("FinancingOffers").setProperty("/supplier", sup);
			}
			bizNetAccessFinancingOffers.loadAllFinancingOffers(this.getOwnerComponent(), this.getModel("FinancingOffers"));
		},

		_haveFinancingOffers: function () {

			if (this.getModel("FinancingOffers").getProperty("/financingOfferCollection/items").length > 0) {
				return true;
			} else {
				return false;
			}
		}
	});
});