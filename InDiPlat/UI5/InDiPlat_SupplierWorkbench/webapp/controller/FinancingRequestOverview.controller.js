sap.ui.define([
	"Workbench/controller/BaseController",
	"Workbench/util/messageProvider",
	"Workbench/util/localStoreFinancingRequests",
	"Workbench/model/modelsBase",
	"Workbench/model/modelsFinancingRequests",
	"Workbench/util/bizNetAccessFinancingRequests",
	"sap/ui/core/routing/History",
	"sap/ui/model/Filter",
	"sap/ui/model/FilterOperator",
	"sap/ui/Device"	
], function(
		BaseController,
		messageProvider,
		localStoreFinancingRequests,
		modelsBase,
		modelsFinancingRequests,
		bizNetAccessFinancingRequests,
		History, 
		Filter, 
		FilterOperator, 
		Device		
	) {
		
	"use strict";

	return BaseController.extend("Workbench.controller.FinancingRequestOverview", {

		onInit : function(){
			
			var oList = this.byId("financingRequestsList");
			this._oList = oList;
			this._oListFilterState = {
				aFilter : [],
				aSearch : []
			};
			this._loadModels();
		},
		
		onSelectionChange : function (oEvent) {
			
			this._showDetail(oEvent.getParameter("listItem") || oEvent.getSource());
		},
		
		onAdd : function() {
			
			this.getRouter().navTo(
				"financingRequest", 
				{
					alias				: this.getOwnerComponent().getModel("Login").getProperty("/alias"),
					invoiceId			: this.getOwnerComponent().getModel("Invoices").getProperty("/selectedInvoice/ID"),
					financingRequestId	: "___new"
				}
			);
		},
		
		onSearch : function(oEvent) {
			
			var sQuery = oEvent.getParameter("query");
			if (sQuery) {
				this._oListFilterState.aSearch = [new Filter("ID", FilterOperator.Contains, sQuery)];
			} else {
				this._oListFilterState.aSearch = [];
			}
			this._applyFilterSearch();			
			var bReplace = !Device.system.phone;
			var oModel = this.getModel("FinancingRequests");
			oModel.setProperty(
				"/selectedFinancingRequest",
				_.findWhere(oModel.getProperty("/financingRequestCollection/items"), 
					{
						ID: oModel.getProperty("/searchfinancingRequestID")
					},
				this));
			this.getRouter().navTo("financingRequest", {
				invoiceId : oModel.getProperty("/selectedFinancingRequest").ID
			}, bReplace);			
		},
		
		_showDetail : function(oItem) {
			
			var bReplace = !Device.system.phone;
			var oModel = this.getModel("FinancingRequests");
			this.getRouter().navTo("financingRequest", {
				alias				: this.getModel("Login").getProperty("/alias"),
				invoiceId			: this.getModel("Invoices").getProperty("/selectedInvoice/ID"),
				financingRequestId	: oModel.getObject(oItem.getBindingContextPath()).ID
			}, bReplace);
		},
		
		_applyFilterSearch : function () {
			var aFilters = this._oListFilterState.aSearch.concat(this._oListFilterState.aFilter),
				oViewModel = this.getModel("FinancingRequests");
			this._oList.getBinding("items").filter(aFilters, "Application");
			if (aFilters.length !== 0) {
				oViewModel.setProperty("/noDataText", this.getResourceBundle().getText("_labelNoFinancingRequestsText"));
			}
		},
		
		_loadModels : function () {
			
			localStoreFinancingRequests.init();

//			Setup Message Provider			
			if (typeof this.getOwnerComponent().getModel("Messages") === "undefined") {
				this.getOwnerComponent().setModel(modelsBase.createMessagesModel(), "Messages");
			}
			messageProvider.init(this.getOwnerComponent().getModel("Messages"));
			
//			Load all existing Financing Requests of Supplier
			this.getOwnerComponent().setModel(modelsFinancingRequests.createFinancingRequestsModel(),"FinancingRequests");
			this.loadEntityMetaData("financingRequest", this.getModel("FinancingRequests"));
			bizNetAccessFinancingRequests.loadAllFinancingRequests(this.getOwnerComponent(), this.getModel("FinancingRequests"));
		},		
		
		onNavBack : function() {
			this.getRouter().navTo("invoices");
		}
	});
});