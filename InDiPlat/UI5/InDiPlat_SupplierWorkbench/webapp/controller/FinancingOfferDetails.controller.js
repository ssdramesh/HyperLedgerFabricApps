sap.ui.define([
	"Workbench/controller/BaseController",
	"Workbench/util/localStoreFinancingOffers",
	"Workbench/util/bizNetAccessFinancingOffers",
	"Workbench/util/bizNetAccessInvoices"
], function (
	BaseController,
	localStoreFinancingOffers,
	bizNetAccessFinancingOffers,
	bizNetAccessInvoices
) {

	"use strict";

	return BaseController.extend("Workbench.controller.FinancingOfferDetails", {

		onInit: function () {

			this.getOwnerComponent().getRouter().getRoute("financingOffer").attachPatternMatched(this._onObjectMatched, this);
		},

		onAfterRendering: function () {

			this._setActions();
		},

		onSetFinancingOfferAccepted: function () {

			var oModel = this.getModel("FinancingOffers");
			bizNetAccessFinancingOffers.setFinancingOfferAccepted(this.getOwnerComponent(), oModel, oModel.getProperty(
				"/selectedFinancingOfferID"));
			bizNetAccessInvoices.setFinancingAccepted(this.getOwnerComponent(), this.getModel("Invoices"), oModel.getProperty(
				"/selectedFinancingOffer/InvoiceID"));
			bizNetAccessFinancingOffers.loadAllFinancingOffers(this.getOwnerComponent(), this.getModel("FinancingOffers"));
			this.getRouter().navTo("financingOffer", {
				alias: this.getModel("Login").getProperty("/alias"),
				invoiceId: this.getModel("Invoices").getProperty("/selectedInvoice/ID"),
				financingOfferId: "___new"
			}, true);
		},

		onSetFinancingOfferDeclined: function () {

			var oModel = this.getModel("FinancingOffers");
			bizNetAccessFinancingOffers.setFinancingOfferDeclined(this.getOwnerComponent(), oModel, oModel.getProperty(
				"/selectedFinancingOfferID"));
			bizNetAccessFinancingOffers.loadAllFinancingOffers(this.getOwnerComponent(), this.getModel("FinancingOffers"));
			oModel.setProperty("/selectedFinancingOfferID", {});
		},

		_onObjectMatched: function (oEvent) {

			var pId = oEvent.getParameter("arguments").financingOfferId;
			if (pId === "___new") {
				this.getModel("FinancingOffers").setProperty("/selectedFinancingOffer", {});
				this._setActions();
			} else {
				this.getModel("FinancingOffers").setProperty("/selectedFinancingOfferID", pId);
				bizNetAccessFinancingOffers.loadFinancingOffer(this.getModel("FinancingOffers"), oEvent.getParameter("arguments").financingOfferId);
				this._setActions();
			}
		},

		_setActions: function () {

			var financingOffer = this.getModel("FinancingOffers").getProperty("/selectedFinancingOffer");

			if (_.isEmpty(financingOffer)) {
				this.getView().byId("__buttonAcceptFinancingOffer").setVisible(false);
				this.getView().byId("__buttonDeclineFinancingOffer").setVisible(false);
				return;
			} else {
				this.getView().byId("__buttonAcceptFinancingOffer").setVisible(true);
				this.getView().byId("__buttonDeclineFinancingOffer").setVisible(true);
				return;
			}
		}

	});
});