sap.ui.define([
	"Workbench/controller/BaseController"
], function(BaseController) {
	"use strict";

	return BaseController.extend("Workbench.controller.Login", {
	
		onLogin: function(){
			
			var oModel = this.getModel("Login");
			this.getOwnerComponent().getRouter().navTo("invoices",{alias:oModel.getProperty("/alias")});			
		},
		
		onRegister: function(){
			
			this.getRouter().navTo("registerNew", {});     
		}
	});
});