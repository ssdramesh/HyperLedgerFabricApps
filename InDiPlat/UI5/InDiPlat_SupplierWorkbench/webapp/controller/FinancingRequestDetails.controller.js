sap.ui.define([
	"Workbench/controller/BaseController",
	"Workbench/model/modelsFinancers",
	"Workbench/model/modelsInterestRates",	
	"Workbench/util/localStoreFinancingRequests",
	"Workbench/util/localStoreFinancers",
	"Workbench/util/localStoreInterestRates",	
	"Workbench/util/bizNetAccessInvoices",
	"Workbench/util/bizNetAccessFinancingRequests",
	"Workbench/util/bizNetAccessFinancers",
	"Workbench/util/bizNetAccessInterestRates"	
], function(
		BaseController,
		modelsFinancers,
		modelsInterestRates,
		localStoreFinancingRequests, 
		localStoreFinancers,
		localStoreInterestRates,
		bizNetAccessInvoices,
		bizNetAccessFinancingRequests,
		bizNetAccessFinancers,
		bizNetAccessInterestRates
	) {
	
	"use strict";
	
	return BaseController.extend("Workbench.controller.FinancingRequestDetails", {
		
		onInit: function() {
			
			this.getOwnerComponent().getRouter().getRoute("financingRequest").attachPatternMatched(this._onObjectMatched, this);
			this._loadFinancerRates();
		},
		
		onAfterRendering: function() {
			
			this._setActions();
		},
		
		onAddNew: function() {

			var oModel = this.getView().getModel("FinancingRequests");
			if (	oModel.getProperty("/newFinancingRequest/InvoiceDaysToCash")			=== "" || 
					oModel.getProperty("/newFinancingRequest/RequestedDisbursementDate")	=== "" || 
					oModel.getProperty("/newFinancingRequest/RequestedDiscountPercent") 	=== "" || 
					oModel.getProperty("/newFinancingRequest/RequestedFinancingAmount") 	=== ""
				) {
				sap.m.MessageToast.show("Please enter valid data in all fields", {});
			} else {
				var financingRequestId = bizNetAccessFinancingRequests.addNewFinancingRequest(this.getOwnerComponent(), oModel);
				bizNetAccessFinancingRequests.loadFinancingRequest(this.getView().getModel("FinancingRequests"), financingRequestId);
				this.getRouter().navTo("financingRequest", {
					alias				: this.getModel("Login").getProperty("/alias"),
					invoiceId			: this.getModel("Invoices").getProperty("/selectedInvoice/ID"),
					financingRequestId	: financingRequestId
				}, true);				
			}
		},
		
		onSetFinancingRequestInProcess: function() {
		
			var oModel = this.getModel("FinancingRequests");
			bizNetAccessFinancingRequests.setFinancingRequestInProcess(this.getOwnerComponent(), oModel);
			if ( this.getView().byId("__barFinancingRequest").getSelectedKey() === "New" ){
				bizNetAccessInvoices.setFinancingRequested(this.getOwnerComponent(), this.getModel("Invoices"), oModel.getProperty("/newFinancingRequest/InvoiceID"), oModel.getProperty("/newFinancingRequest/FinancerID"));
			} else {
				bizNetAccessInvoices.setFinancingRequested(this.getOwnerComponent(), this.getModel("Invoices"), oModel.getProperty("/selectedFinancingRequest/InvoiceID"), oModel.getProperty("/selectedFinancingRequest/FinancerID"));
			}
		},
		
		onCalculateFinancingRequest: function() {
			
			var oModel = this.getOwnerComponent().getModel("FinancingRequests");
			var gAmountNew, dRateNew, gAmount, dRate;
			if (oModel.getProperty("/newFinancingRequest/InvoiceGrossAmount") !== ""){
				gAmountNew = parseFloat(oModel.getProperty("/newFinancingRequest/InvoiceGrossAmount")).toFixed(2);
			} else {
				gAmountNew = 0;
			}
			if (oModel.getProperty("/newFinancingRequest/RequestedDiscountPercent") !== ""){
				dRateNew = parseFloat(oModel.getProperty("/newFinancingRequest/RequestedDiscountPercent")).toFixed(2);
			} else {
				dRateNew = 0;
			}
			if ( gAmountNew !== 0 && dRateNew !== 0 ){
				oModel.setProperty("/newFinancingRequest/RequestedFinancingAmount", (gAmountNew*(1-dRateNew/100)).toString());
				return;
			} else {
				if (oModel.getProperty("/selectedFinancingRequest/InvoiceGrossAmount") !== ""){
					gAmount = parseFloat(oModel.getProperty("/selectedFinancingRequest/InvoiceGrossAmount")).toFixed(2);
				} else {
					gAmount = 0;
				}
				if (oModel.getProperty("/selectedFinancingRequest/RequestedDiscountPercent") !== 0){
					dRate = parseFloat(oModel.getProperty("/selectedFinancingRequest/RequestedDiscountPercent")).toFixed(2);
				} else {
					dRate = 0;
				}
				if ( gAmount !== 0 && dRate !== 0 ){
					oModel.setProperty("/selectedFinancingRequest/RequestedFinancingAmount", (gAmount*(1-dRate/100)).toString());
					return;
				} else {
					sap.m.MessageToast.show("ERROR! Please input valid Requested Discount Rate and Invoice Gross Amount");
				}
			}
		},
		
		onFinancerSelection: function(oEvent){
			
			var oListItem = oEvent.getParameter("listItem") || oEvent.getSource();
			
			var fModel = this.getOwnerComponent().getModel("Financers");
			var fRModel = this.getOwnerComponent().getModel("FinancingRequests");
			
			var financer = fModel.getObject(oListItem.getBindingContextPath()).Financer;
			
			fRModel.setProperty(
				"/newFinancingRequest/FinancerID",
				_.findWhere(fModel.getProperty("/financerCollection/items"),{Description:financer}).ID
			);
			
			fRModel.setProperty(
				"/newFinancingRequest/RequestedDiscountPercent",
				this._getDiscountRate(fRModel, _.findWhere(fModel.getProperty("/financerCollection/items"),{Description:financer}).ID)
			);
			
			fRModel.setProperty(
				"/newFinancingRequest/InvoiceDaysToCash",
				this._getDaysToCash(fRModel.getProperty("/newFinancingRequest/InvoicePaymentDate"))
			);			
		},
		
		_onObjectMatched: function(oEvent) {
			
			var pId = oEvent.getParameter("arguments").financingRequestId;
			if (pId === "___new") {
				this.getView().byId("__barFinancingRequest").setSelectedKey("New");
				this._copyInvoiceToFinancingRequest(this.getView().getModel("Invoices").getProperty("/selectedInvoice"), this.getView().getModel("FinancingRequests"));
				this._setNewHeader();
				this._loadFinancerRates();
				this._setActions(true);
			} else {
				this.getModel("FinancingRequests").setProperty("/selectedFinancingRequestID", pId);
				bizNetAccessFinancingRequests.loadFinancingRequest(this.getView().getModel("FinancingRequests"), oEvent.getParameter("arguments").financingRequestId);
				this._setActions(false);
			}
		},
		
		_getDiscountRate:function(oModel, financerID){
			
			var daysToCash = this._getDaysToCash(oModel.getProperty("/newFinancingRequest/InvoicePaymentDate"));
			var rates = this.getModel("InterestRates").getProperty("/interestRateCollection/items");
			return _.findWhere(rates,{FinancerID:financerID, DaysToCash:daysToCash}).DiscountRate;
		},
		
		_getDaysToCash:function(paymentDate){
			
			var paymentDateTime = new Date(this.getOwnerComponent().getModel("FinancingRequests").getProperty("/newFinancingRequest/InvoicePaymentDate")).getTime();
			var todayDateTime  = new Date(this._getCurrentDate()).getTime();
			
			var diffDays = parseInt((paymentDateTime-todayDateTime)/(24*3600*1000));

			if (diffDays < 15){ return "15"; }
			if ( 15 <= diffDays <= 30 ){ return "30"; }
			if ( 31 <= diffDays <= 45 ){ return "45"; }
			if ( 46 <= diffDays <= 60 ){ return "60"; }
			if ( 61 <= diffDays <= 75 ){ return "75"; }
			if ( 76 <= diffDays <= 90 ){ return "90"; }
			if ( 91 <= diffDays){ return ">90D"; }
		},
		
		_getCurrentDate:function(){
			
			var today = new Date();
			return (today.getMonth()+1) + "/" + today.getDate() + "/" + today.getFullYear();
		},		
		
		_setActions: function(bNew) {
			
			var financingRequest = this.getModel("FinancingRequests").getProperty("/selectedFinancingRequest");
			
			if (bNew){
				this.getView().byId("__buttonCreateNewFinancingRequest").setVisible(true);
				this.getView().byId("__actionCalculateFinancingRequest").setVisible(true);
				this.getView().byId("__actionSetFinancingRequestInProcess").setVisible(false);
				return;
			} else if (financingRequest === {}) {
				this.getView().byId("__buttonCreateNewFinancingRequest").setVisible(false);
				this.getView().byId("__actionCalculateFinancingRequest").setVisible(false);
				this.getView().byId("__actionSetFinancingRequestInProcess").setVisible(false);
				return;
			} else if (financingRequest.Status === "Created") {
				this.getView().byId("__buttonCreateNewFinancingRequest").setVisible(false);
				this.getView().byId("__actionCalculateFinancingRequest").setVisible(true);
				this.getView().byId("__actionSetFinancingRequestInProcess").setVisible(true);
				return;
			} else {
				this.getView().byId("__buttonCreateNewFinancingRequest").setVisible(false);
				this.getView().byId("__actionCalculateFinancingRequest").setVisible(false);
				this.getView().byId("__actionSetFinancingRequestInProcess").setVisible(false);
				return;
			}
		},

		_setNewHeader:function(){

			this.getView().byId("__headerFinancingRequest").setObjectTitle("New Financing Request");
			var newID = this.getModel("FinancingRequests").getProperty("/newFinancingRequest/ID");
			if ( newID !== "" ){
				this.getView().byId("__attributeFinancingRequestID").setTitle(newID);
			}
		},
		
		_copyInvoiceToFinancingRequest: function(invoice, oModel) {
		
			if ( this.getView().byId("__barFinancingRequest").getSelectedKey() === "New" ){
				oModel.setProperty("/newFinancingRequest/SupplierID", invoice.SupplierID);
				oModel.setProperty("/newFinancingRequest/PurchaserID", invoice.PurchaserID);
				oModel.setProperty("/newFinancingRequest/InvoiceID", invoice.ID);
				oModel.setProperty("/newFinancingRequest/PurchaseOrderID", invoice.PurchaseOrderID);
				oModel.setProperty("/newFinancingRequest/InvoiceGrossAmount", invoice.GrossAmount);
				oModel.setProperty("/newFinancingRequest/InvoiceDaysToCash", "");
				oModel.setProperty("/newFinancingRequest/InvoicePaymentDate", invoice.PaymentDate);
				oModel.setProperty("/newFinancingRequest/RequestedDisbursementDate", "");
				oModel.setProperty("/newFinancingRequest/RequestedDiscountPercent", "");
				oModel.setProperty("/newFinancingRequest/RequestedFinancingAmount", "");
				oModel.setProperty("/newFinancingRequest/DocumentsChecked", "false");
				oModel.setProperty("/newFinancingRequest/DocumentsVerified", "false");
				oModel.setProperty("/newFinancingRequest/Currency", invoice.Currency);
			}
		},
		
		_loadFinancerRates: function(){
			
			localStoreFinancers.init();
			localStoreInterestRates.init();
			
//			Load all Financers (for displaying interest rates in new Request)
			this.getOwnerComponent().setModel(modelsFinancers.createFinancersModel(),"Financers");
			this.loadEntityMetaData("financer", this.getModel("Financers"));
			bizNetAccessFinancers.loadAllFinancers(this.getOwnerComponent(), this.getModel("Financers"));

//			Load all InterestRates of all Financers (for displaying interest rates in new Request)
			this.getOwnerComponent().setModel(modelsInterestRates.createInterestRatesModel(),"InterestRates");
			this.loadEntityMetaData("interestRate", this.getModel("InterestRates"));
			bizNetAccessInterestRates.loadAllInterestRates(this.getOwnerComponent(), this.getModel("InterestRates"));


			var financers = this.getModel("Financers").getProperty("/financerCollection/items");
			var rates = this.getModel("InterestRates").getProperty("/interestRateCollection/items");
			var dispRates = [];
			
			for ( var i = 0; i < financers.length; i++ ){
				dispRates.push({
					"Financer":financers[i].Description,
					"15D": this._getRate(rates,financers[i].ID,"15"),
					"30D": this._getRate(rates,financers[i].ID,"30"),
					"45D": this._getRate(rates,financers[i].ID,"45"),
					"60D": this._getRate(rates,financers[i].ID,"60"),
					"75D": this._getRate(rates,financers[i].ID,"75"),
					"90D": this._getRate(rates,financers[i].ID,"90"),
					">90D":"on request"
				});
			}
			this.getModel("Financers").setProperty("/financerRateCollection/items", dispRates);
		},
		
		_getRate:function(rates,financerID,daysToCash){
			
			var rate = _.findWhere(rates,{FinancerID:financerID, DaysToCash:daysToCash});
			if (typeof rate === "undefined"){
				return "-";
			} else {
				return rate.DiscountRate + " %";
			}
		},
		
		_clearNewFinancingRequest: function() {
			
			var oModel = this.getView().getModel("FinancingRequests");
			oModel.setProperty("/newFinancingRequest/ID", "");
			oModel.setProperty("/newFinancingRequest/SupplierID", "");
			oModel.setProperty("/newFinancingRequest/PurchaserID", "");
			oModel.setProperty("/newFinancingRequest/FinancerID", "");
			oModel.setProperty("/newFinancingRequest/InvoiceID", "");
			oModel.setProperty("/newFinancingRequest/PurchaseOrderID", "");
			oModel.setProperty("/newFinancingRequest/ObjectType", "");
			oModel.setProperty("/newFinancingRequest/Status", "");
			oModel.setProperty("/newFinancingRequest/InvoiceGrossAmount", "");
			oModel.setProperty("/newFinancingRequest/InvoiceDaysToCash", "");
			oModel.setProperty("/newFinancingRequest/InvoicePaymentDate", "");
			oModel.setProperty("/newFinancingRequest/RequestCreationDate", "");
			oModel.setProperty("/newFinancingRequest/RequestedDisbursementDate", "");
			oModel.setProperty("/newFinancingRequest/RequestedDiscountPercent", "");
			oModel.setProperty("/newFinancingRequest/RequestedFinancingAmount", "");
			oModel.setProperty("/newFinancingRequest/DocumentsChecked", "");
			oModel.setProperty("/newFinancingRequest/DocumentsVerified", "");
			oModel.setProperty("/newFinancingRequest/Currency", "");
		},

		onTabFilterNew: function() {
			
			if ( this.getView().byId("__barFinancingRequest").getSelectedKey() === "New" ){
				this._setNewHeader();
				this._loadFinancerRates();
				this._copyInvoiceToFinancingRequest(this.getView().getModel("Invoices").getProperty("/selectedInvoice"), this.getView().getModel("FinancingRequests"));
				this._setActions(true);
			} else {
				this.getRouter().navTo("financingRequest", {
					alias				: this.getModel("Login").getProperty("/alias"),
					invoiceId			: this.getModel("Invoices").getProperty("/selectedInvoice/ID"),
					financingRequestId	: this.getModel("FinancingRequests").getProperty("/selectedFinancingRequestID")
				}, true);
				this._setActions(false);
			}
		}
	});
});