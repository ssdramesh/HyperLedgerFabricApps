sap.ui.define([
	"Workbench/controller/BaseController",
	"Workbench/util/bizNetAccessInvoices",
	"Workbench/util/bizNetAccessSuppliers",
	"Workbench/util/bizNetAccessPurchasers",
	"Workbench/util/bizNetAccessPurchaseOrders",
	"Workbench/model/modelsBase",
	"Workbench/model/modelsInvoices",
	"Workbench/model/modelsSuppliers",
	"Workbench/model/modelsPurchasers",
	"Workbench/model/modelsPurchaseOrders",
	"Workbench/util/messageProvider",
	"Workbench/util/localStoreInvoices",
	"Workbench/util/localStoreSuppliers",
	"Workbench/util/localStorePurchasers",
	"Workbench/util/localStorePurchaseOrders",
	"sap/ui/core/routing/History",
	"sap/ui/model/Filter",
	"sap/ui/model/FilterOperator",
	"sap/ui/Device"
], function (
	BaseController,
	bizNetAccessInvoices,
	bizNetAccessSuppliers,
	bizNetAccessPurchasers,
	bizNetAccessPurchaseOrders,
	modelsBase,
	modelsInvoices,
	modelsSuppliers,
	modelsPurchasers,
	modelsPurchaseOrders,
	messageProvider,
	localStoreInvoices,
	localStoreSuppliers,
	localStorePurchasers,
	localStorePurchaseOrders,
	History,
	Filter,
	FilterOperator,
	Device
) {
	"use strict";

	return BaseController.extend("Workbench.controller.WorkbenchOverview", {

		onInit: function () {

			var oList = this.byId("invoicesList");
			this._oList = oList;
			this._oListFilterState = {
				aFilter: [],
				aSearch: []
			};

			if (typeof this.getOwnerComponent().getModel("Messages") === "undefined") {
				this.getOwnerComponent().setModel(modelsBase.createMessagesModel(), "Messages");
			}
			messageProvider.init(this.getOwnerComponent().getModel("Messages"));
			this._loadModels();
		},

		onSelectionChange: function (oEvent) {

			this._showDetail(oEvent.getParameter("listItem") || oEvent.getSource());
		},

		onAdd: function () {

			this.getRouter().navTo(
				"invoice", {
					alias: this.getOwnerComponent().getModel("Login").getProperty("/alias"),
					invoiceId: "___new"
				}
			);
		},

		onSearch: function (oEvent) {

			var sQuery = oEvent.getParameter("query");
			if (sQuery) {
				this._oListFilterState.aSearch = [new Filter("ID", FilterOperator.Contains, sQuery)];
			} else {
				this._oListFilterState.aSearch = [];
			}
			this._applyFilterSearch();
			var bReplace = !Device.system.phone;
			var oModel = this.getModel("Invoices");
			oModel.setProperty(
				"/selectedInvoice",
				_.findWhere(oModel.getProperty("/invoiceCollection/items"), {
						ID: oModel.getProperty("/searchInvoiceID")
					},
					this));
			this.getRouter().navTo("invoice", {
				invoiceId: oModel.getProperty("/selectedInvoice").ID
			}, bReplace);
		},

		_showDetail: function (oItem) {

			var bReplace = !Device.system.phone;
			var oModel = this.getModel("Invoices");
			this.getRouter().navTo("invoice", {
				alias: this.getModel("Login").getProperty("/alias"),
				invoiceId: oModel.getObject(oItem.getBindingContextPath()).ID
			}, bReplace);
		},

		_applyFilterSearch: function () {
			var aFilters = this._oListFilterState.aSearch.concat(this._oListFilterState.aFilter),
				oViewModel = this.getModel("Invoices");
			this._oList.getBinding("items").filter(aFilters, "Application");
			if (aFilters.length !== 0) {
				oViewModel.setProperty("/noDataText", this.getResourceBundle().getText("_labelNoInvoicesText"));
			}
		},

		_loadModels: function () {

			localStoreInvoices.init();
			localStoreSuppliers.init();
			localStorePurchasers.init();
			localStorePurchaseOrders.init();

			//Load all Suppliers and logged in supplier			
			this.getOwnerComponent().setModel(modelsSuppliers.createSuppliersModel(), "Suppliers");
			this.loadEntityMetaData("supplier", this.getModel("Suppliers"));
			bizNetAccessSuppliers.loadAllSuppliers(this.getOwnerComponent(), this.getModel("Suppliers"));

			//Initiate Purchasers	(for value help)	
			this.getOwnerComponent().setModel(modelsPurchasers.createPurchasersModel(), "Purchasers");
			this.loadEntityMetaData("purchaser", this.getModel("Purchasers"));
			bizNetAccessPurchasers.loadAllPurchasers(this.getOwnerComponent(), this.getModel("Purchasers"));

			//Set currently logged in Supplier
			if (this.getModel("Login").getProperty("/alias") !== "") {
				var sup = bizNetAccessSuppliers.getSupplierFromAlias(this, this.getModel("Login").getProperty("/alias"));
				this.getOwnerComponent().getModel("Login").setProperty("/id", sup.ID);
				bizNetAccessSuppliers.loadSupplier(this.getModel("Suppliers"), sup.ID);
			}

			//Load all invoices of supplier			
			this.getOwnerComponent().setModel(modelsInvoices.createInvoicesModel(), "Invoices");
			if (typeof sup !== "undefined") {
				this.getModel("Invoices").setProperty("/supplier", sup);
			}
			this.loadEntityMetaData("invoice", this.getModel("Invoices"));
			bizNetAccessInvoices.loadAllInvoices(this.getOwnerComponent(), this.getModel("Invoices"));

			//Load all Purchase Orders for the supplier
			this.getOwnerComponent().setModel(modelsPurchaseOrders.createPurchaseOrdersModel(), "PurchaseOrders");
			if (typeof sup !== "undefined") {
				this.getModel("PurchaseOrders").setProperty("/supplier", sup);
			}
			this.loadEntityMetaData("purchaseOrder", this.getModel("PurchaseOrders"));
			bizNetAccessPurchaseOrders.loadAllPurchaseOrders(this.getOwnerComponent(), this.getModel("PurchaseOrders"));
		},

		_onToggle: function () {

			this.getOwnerComponent().getModel("TestSwitch").setProperty(
				"/testMode", !this.getOwnerComponent().getModel("TestSwitch").getProperty("/testMode")
			);
			var location = this.getRunMode().location;
			messageProvider.addMessage(
				"Success",
				"Switching Network for Blockchain Data to: ",
				"No Description",
				location,
				1,
				"",
				"http://www.sap.com"
			);
			this._loadModels();
		},

		onMessagePress: function () {

			this.onShowMessageDialog("InDiPlat Log");
		},

		onRefreshInvoices: function () {

			bizNetAccessInvoices.loadAllInvoices(this.getOwnerComponent(), this.getModel("Invoices"));
		},

		onNavBack: function () {
			this.getRouter().navTo("home");
		}
	});
});