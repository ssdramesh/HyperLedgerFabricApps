sap.ui.define([
	"Workbench/controller/BaseController",
	"Workbench/util/messageProvider",
	"Workbench/util/localStoreSuppliers",
	"Workbench/model/modelsBase",
	"Workbench/model/modelsSuppliers",
	"Workbench/util/bizNetAccessSuppliers"
], function(
		BaseController, 
		messageProvider, 
		localStoreSuppliers, 
		modelsBase, 
		modelsSuppliers, 
		bizNetAccessSuppliers
	) {
	
	"use strict";
	
	return BaseController.extend("Workbench.controller.Register", {
	
		onInit: function() {
	
			this._loadModel();
		},
	
		onAddNew: function() {
	
			bizNetAccessSuppliers.addNewSupplier(this.getOwnerComponent(), this.getModel("Suppliers"));
		},
	
		_loadModel: function() {
	
			localStoreSuppliers.init();
			if (typeof this.getOwnerComponent().getModel("Messages") === "undefined") {
				this.getOwnerComponent().setModel(modelsBase.createMessagesModel(), "Messages");
			}
			messageProvider.init(this.getOwnerComponent().getModel("Messages"));
			
			if (typeof this.getModel("Suppliers") === "undefined") {
				this.getOwnerComponent().setModel(modelsSuppliers.createSuppliersModel(), "Suppliers");
			}
			this.loadEntityMetaData("Supplier", this.getModel("Suppliers"));
			bizNetAccessSuppliers.loadAllSuppliers(this.getOwnerComponent(), this.getModel("Suppliers"));
		},

		onNavBack: function() {
	
			this.getRouter().navTo("home", {}, true);
		}
	});
});