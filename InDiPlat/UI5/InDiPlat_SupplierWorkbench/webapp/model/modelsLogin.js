sap.ui.define([
	"sap/ui/model/json/JSONModel"
], function(JSONModel) {
	"use strict";

	return {

		createLoginModel: function() {
			
			var oModel = new JSONModel();
			oModel.setDefaultBindingMode("TwoWay");
			oModel.setData({
				alias		: "",
				id			: "",
				password	: ""
			});
			return oModel;
		}
	};
});