sap.ui.define([
	"sap/ui/model/json/JSONModel"
], function(JSONModel) {
	"use strict";

	return {

		createFinancingOffersModel: function(){
			
			var oModel = new JSONModel();
			oModel.setDefaultBindingMode("TwoWay");
			oModel.setData({
				chaincode:{
					serviceUrl:"",
					serviceKey:"",
					entity:{}					
				},
				supplier			: {},
				financingOfferCollection	: {
					cols:[
						{name:"ID"},
						{name:"SupplierID"},
						{name:"PurchaserID"},
						{name:"FinancerID"},
						{name:"InvoiceID"},
						{name:"PurchaseOrderID"},
						{name:"ObjectType"},
						{name:"Status"},
						{name:"InvoiceGrossAmount"},
						{name:"DiscountPercent"},
						{name:"DiscountAmount"},
						{name:"FundedAmount"},
						{name:"OfferValidityStartDate"},
						{name:"OfferValidityEndDate"},
						{name:"DisbursementDate"},
						{name:"DisbursementReference"},
						{name:"Currency"}
					],
					items:[]
				},
				selectedFinancingOffer	: {},		
				newFinancingOffer		: {
					ID						:	"",
					SupplierID				:	"",
					PurchaserID				:	"",
					FinancerID				:	"",
					InvoiceID				:	"",
					PurchaseOrderID			:	"",
					ObjectType				:	"",
					Status					:	"",
					InvoiceGrossAmount		:	"",
					DiscountPercent			:	"",
					DiscountAmount			:	"",
					FundedAmount			:	"",
					OfferValidityStartDate	:	"",
					OfferValidityEndDate	:	"",
					DisbursementDate		:	"",
					DisbursementReference	:	"",
					Currency				:	""
				},
				selectedFinancingOfferID	: "",
				searchFinancingOfferID : ""
			});
			return oModel;			
		}
	};
});