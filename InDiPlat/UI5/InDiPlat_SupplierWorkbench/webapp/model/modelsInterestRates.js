sap.ui.define([
	"sap/ui/model/json/JSONModel"
], function(JSONModel) {
	"use strict";

	return {

		createInterestRatesModel: function(){
			
			var oModel = new JSONModel();
			oModel.setDefaultBindingMode("TwoWay");
			oModel.setData({
				chaincode:{
					serviceUrl:"",
					serviceKey:"",
					entity:{}					
				},
				interestRateCollection	: {
					cols:[
						{name:"ID"},
						{name:"FinancerID"},
						{name:"ObjectType"},
						{name:"DaysToCash"},
						{name:"DiscountRate"}
					],
					items:[]
				},
				selectedInterestRate	: {},		
				newInterestRate		: {
					ID				:	"",
					FinancerID		:	"",
					ObjectType		:	"",
					DaysToCash		:	"",
					DiscountRate	:	""
				},
				selectedInterestRateID	: "",
				searchInterestRateID : ""
			});
			return oModel;			
		}
	};
});