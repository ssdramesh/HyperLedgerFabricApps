sap.ui.define(function() {
	"use strict";

	return {

		mapPurchaserToModel:function(responseData){
			return {
				ID						: responseData.ID,
				ObjectType 				: responseData.docType,
				Alias					: responseData.alias,
				Description				: responseData.description
			};
		},

		mapPurchasersToModel:function(responseData){
			
			var items = [];
			if( responseData ){
				for ( var i = 0; i < responseData.length; i++ ){
					items.push(this.mapPurchaserToModel(responseData[i]));
				}
			}
			return items;
		},

		mapPurchaserToChaincode:function(oModel, newPurchaser){
			
			if ( newPurchaser === true ) {
				return {
						"ID"			:	this.getNewPurchaserID(oModel),
						"docType"		:	"Participant.Purchaser",
						"alias"			:	oModel.getProperty("/newPurchaser/Alias"),
						"description"	:	oModel.getProperty("/newPurchaser/Description")
				};
			} else {
				return {
						"ID"			:	oModel.getProperty("/selectedPurchaser/ID"),
						"docType"		:	oModel.getProperty("/selectedPurchaser/ObjectType"),
						"alias"			:	oModel.getProperty("/selectedPurchaser/Alias"),
						"description"	:	oModel.getProperty("/selectedPurchaser/Description")
				};				
			}
		},
		
		mapPurchaserToLocalStorage : function(oModel){
				
			return {
					"ID"					:	this.getNewPurchaserID(oModel),
					"docType"				:	"Participant.Purchaser",
					"alias"					:	oModel.getProperty("/newPurchaser/Alias"),
					"description"			:	oModel.getProperty("/newPurchaser/Description")
			};
		},
		
		getNewPurchaserID:function(oModel){

			if ( typeof oModel.getProperty("/newPurchaser/ID") === "undefined" ||
		    		oModel.getProperty("/newPurchaser/ID") === ""
		    	){
			    var iD = "PU";
				var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
			    for( var i = 0; i < 8; i++ ){
			        iD += possible.charAt(Math.floor(Math.random() * possible.length));
			    }
		    } else {
				iD = oModel.getProperty("/newPurchaser/ID");
			}
			oModel.setProperty("/newPurchaser/ID",iD);
		    return iD;
		}
	};
});