sap.ui.define([
		"Workbench/util/bizNetAccessPurchasers"
	],function(bizNetAccessPurchasers) {
	"use strict";

	return {

		mapPurchaseOrderToModel:function(responseData){
			return {
				ID						: responseData.ID,
				PurchaserID				: responseData.purchaserID,
				SupplierID				: responseData.supplierID,
				ObjectType 				: responseData.docType,
				Status					: responseData.status,
				PostingDate				: responseData.postingDate,
				ApprovalDate			: responseData.approvalDate,
				PaymentDate				: responseData.paymentDate,
				GrossAmount				: responseData.grossAmount,
				TotalSubmittedAmount	: responseData.totalSubmittedAmount,
				Currency				: responseData.currency
			};
		},

		mapPurchaseOrdersToModel:function(responseData){
			
			var items = [];
			if( responseData ){
				for ( var i = 0; i < responseData.length; i++ ){
					items.push(this.mapPurchaseOrderToModel(responseData[i]));
				}
			}
			return items;
		},

		mapPurchaseOrderToChaincode:function(oComponent, oModel, newPurchaseOrder){
			
			var pur = bizNetAccessPurchasers.getPurchaserFromAlias(oComponent, oComponent.getModel("Login").getProperty("/alias"));
			if ( newPurchaseOrder === true ) {
				return {
						"ID"					:	this.getNewPurchaseOrderID(oModel),
						"purchaserID"			:	pur.ID,
						"supplierID"			:	oModel.getProperty("/newPurchaseOrder/SupplierID"),
						"docType"				:	"Asset.PurchaseOrder",
						"status"				:	"Approved",
						"postingDate"			:	oModel.getProperty("/newPurchaseOrder/PostingDate")	,
						"approvalDate"			:	oModel.getProperty("/newPurchaseOrder/ApprovalDate"),
						"paymentDate"			:	oModel.getProperty("/newPurchaseOrder/PaymentDate"),
						"grossAmount"			:	oModel.getProperty("/newPurchaseOrder/GrossAmount"),
						"totalSubmittedAmount"	:	oModel.getProperty("/newPurchaseOrder/TotalSubmittedAmount"),
						"currency"				:	oModel.getProperty("/newPurchaseOrder/Currency")
				};
			} else {
				return {
						"ID"					:	oModel.getProperty("/selectedPurchaseOrder/ID"),
						"purchaserID"			:	pur.ID,
						"supplierID"			:	oModel.getProperty("/selectedPurchaseOrder/SupplierID"),
						"docType"				:	oModel.getProperty("/selectedPurchaseOrder/ObjectType"),
						"status"				:	oModel.getProperty("/selectedPurchaseOrder/Status"),
						"postingDate"			:	oModel.getProperty("/selectedPurchaseOrder/PostingDate"),
						"approvalDate"			:	oModel.getProperty("/selectedPurchaseOrder/ApprovalDate"),
						"paymentDate"			:	oModel.getProperty("/selectedPurchaseOrder/PaymentDate"),
						"grossAmount"			:	oModel.getProperty("/selectedPurchaseOrder/GrossAmount"),
						"totalSubmittedAmount"	:	oModel.getProperty("/selectedPurchaseOrder/TotalSubmittedAmount"),
						"currency"				:	oModel.getProperty("/selectedPurchaseOrder/Currency")
				};				
			}
		},
		
		mapPurchaseOrderToLocalStorage : function(oComponent, oModel){
			
			var pur = bizNetAccessPurchasers.getPurchaserFromAlias(oComponent, oComponent.getModel("Login").getProperty("/alias"));	
			return {
					"ID"					:	this.getNewPurchaseOrderID(oModel),
					"purchaserID"			:	pur.ID,
					"supplierID"			:	oModel.getProperty("/newPurchaseOrder/SupplierID"),
					"docType"				:	"Asset.PurchaseOrder",
					"status"				:	"Approved",
					"postingDate"			:	oModel.getProperty("/newPurchaseOrder/PostingDate"),
					"approvalDate"			:	oModel.getProperty("/newPurchaseOrder/ApprovalDate"),
					"paymentDate"			:	oModel.getProperty("/newPurchaseOrder/PaymentDate"),
					"grossAmount"			:	oModel.getProperty("/newPurchaseOrder/GrossAmount"),
					"totalSubmittedAmount"	:	oModel.getProperty("/newPurchaseOrder/TotalSubmittedAmount"),
					"currency"				:	oModel.getProperty("/newPurchaseOrder/Currency")
			};
		},
		
		getNewPurchaseOrderID:function(oModel){

			if ( typeof oModel.getProperty("/newPurchaseOrder/ID") === "undefined" ||
		    		oModel.getProperty("/newPurchaseOrder/ID") === ""
		    	){
			    var iD = "PO";
				var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
			    for( var i = 0; i < 8; i++ ){
			        iD += possible.charAt(Math.floor(Math.random() * possible.length));
			    }
		    } else {
				iD = oModel.getProperty("/newPurchaseOrder/ID");
			}
			oModel.setProperty("/newPurchaseOrder/ID",iD);
		    return iD;
		}
	};
});