sap.ui.define([
	"Workbench/controller/BaseController",
	"Workbench/util/localStorePurchaseOrders",
	"Workbench/util/bizNetAccessPurchaseOrders"
], function (
	BaseController,
	localStorePurchaseOrders,
	bizNetAccessPurchaseOrders
) {
	"use strict";

	return BaseController.extend("Workbench.controller.WorkbenchDetails", {

		onInit: function () {

			this.getOwnerComponent().getRouter().getRoute("purchaseOrder").attachPatternMatched(this._onObjectMatched, this);
		},

		_onObjectMatched: function (oEvent) {

			var pId = oEvent.getParameter("arguments").purchaseOrderId;
			if (pId === "___new") {
				this.getView().byId("__barPurchaseOrder").setSelectedKey("New");
			} else {
				this.getModel("PurchaseOrders").setProperty("/selectedPurchaseOrderID", pId);
				this.getModel("PurchaseOrders").setProperty("/searchPurchaseOrderID", pId);
				bizNetAccessPurchaseOrders.loadPurchaseOrder(this.getView().getModel("PurchaseOrders"), oEvent.getParameter("arguments").purchaseOrderId);
			}
		},

		addNew: function () {

			var oModel = this.getView().getModel("PurchaseOrders");
			if (oModel.getProperty("/newPurchaseOrder/SupplierID") === "" ||
				oModel.getProperty("/newPurchaseOrder/PostingDate") === "" ||
				oModel.getProperty("/newPurchaseOrder/ApprovalDate") === "" ||
				oModel.getProperty("/newPurchaseOrder/GrossAmount") === "" ||
				oModel.getProperty("/newPurchaseOrder/TotalSubmittedAmount") === "" ||
				oModel.getProperty("/newPurchaseOrder/Currency") === ""
			) {
				sap.m.MessageToast.show("Please enter valid data in all fields", {});
			} else {
				var purchaseOrderId = bizNetAccessPurchaseOrders.addNewPurchaseOrder(this.getOwnerComponent(), oModel);
				this.getView().byId("__barPurchaseOrder").setSelectedKey("Details");
				bizNetAccessPurchaseOrders.loadPurchaseOrder(this.getView().getModel("PurchaseOrders"), purchaseOrderId);
				this.getView().getModel("PurchaseOrders").setProperty("/newPurchaseOrder", {});
			}
		},

		onPaid: function () {

			var oModel = this.getModel("PurchaseOrders");
			if (oModel.getProperty("/selectedPurchaseOrder/PaymentDate") !== "") {
				var ok = bizNetAccessPurchaseOrders.setPurchaseOrderPaid(
					this.getOwnerComponent(),
					oModel,
					oModel.getProperty("/selectedPurchaseOrderID"),
					oModel.getProperty("/selectedPurchaseOrder/PaymentDate")
				);
			} else {
				sap.m.MessageToast.show("Please enter valid payment date", {});
			}
			if (ok) {
				this.getView().byId("__pickerPurchaseOrderPaymentDate").setVisible(false);
				this.getView().byId("__buttonSetPaid").setEnabled(false);
			}
		},

		removePurchaseOrder: function () {

			var oModel = this.getView().getModel("PurchaseOrders");
			bizNetAccessPurchaseOrders.removePurchaseOrder(this.getOwnerComponent(), oModel, oModel.getProperty("/selectedPurchaseOrder/ID"));
			oModel.setProperty("/selectedPurchaseOrder", {});
		}

	});

});