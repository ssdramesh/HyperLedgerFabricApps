sap.ui.define([
	"Workbench/controller/BaseController"
], function(BaseController) {
	"use strict";
	
	return BaseController.extend("Workbench.controller.WorkbenchEmpty", {
	});

});