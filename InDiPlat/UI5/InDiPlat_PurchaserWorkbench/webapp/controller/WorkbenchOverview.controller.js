sap.ui.define([
	"Workbench/controller/BaseController",
	"Workbench/util/bizNetAccessPurchaseOrders",
	"Workbench/util/bizNetAccessPurchasers",
	"Workbench/util/bizNetAccessSuppliers",
	"Workbench/model/modelsBase",
	"Workbench/model/modelsPurchaseOrders",
	"Workbench/model/modelsPurchasers",
	"Workbench/model/modelsSuppliers",
	"Workbench/util/messageProvider",
	"Workbench/util/localStorePurchaseOrders",
	"Workbench/util/localStorePurchasers",
	"Workbench/util/localStoreSuppliers",
	"sap/ui/core/routing/History",
	"sap/ui/model/Filter",
	"sap/ui/model/FilterOperator",
	"sap/ui/Device"
], function (
	BaseController,
	bizNetAccessPurchaseOrders,
	bizNetAccessPurchasers,
	bizNetAccessSuppliers,
	modelsBase,
	modelsPurchaseOrders,
	modelsPurchasers,
	modelsSuppliers,
	messageProvider,
	localStorePurchaseOrders,
	localStorePurchasers,
	localStoreSuppliers,
	History,
	Filter,
	FilterOperator,
	Device
) {
	"use strict";

	return BaseController.extend("Workbench.controller.WorkbenchOverview", {

		onInit: function () {

			var oList = this.byId("purchaseOrdersList");
			this._oList = oList;
			this._oListFilterState = {
				aFilter: [],
				aSearch: []
			};

			//Check and update the initiation of Message Provider
			if (typeof this.getOwnerComponent().getModel("Messages") === "undefined") {
				this.getOwnerComponent().setModel(modelsBase.createMessagesModel(), "Messages");
			}
			messageProvider.init(this.getOwnerComponent().getModel("Messages"));
			this._loadModels();
		},

		onSelectionChange: function (oEvent) {

			this._showDetail(oEvent.getParameter("listItem") || oEvent.getSource());
		},

		onAdd: function () {

			this.getRouter().navTo(
				"purchaseOrder", {
					alias: this.getOwnerComponent().getModel("Login").getProperty("/alias"),
					purchaseOrderId: "___new"
				}
			);
		},

		onSearch: function (oEvent) {

			var sQuery = oEvent.getParameter("query");
			if (sQuery) {
				this._oListFilterState.aSearch = [new Filter("ID", FilterOperator.Contains, sQuery)];
			} else {
				this._oListFilterState.aSearch = [];
			}
			this._applyFilterSearch();
			var bReplace = !Device.system.phone;
			var oModel = this.getModel("PurchaseOrders");
			oModel.setProperty(
				"/selectedPurchaseOrder",
				_.findWhere(oModel.getProperty("/purchaseOrderCollection/items"), {
						ID: oModel.getProperty("/searchPurchaseOrderID")
					},
					this));
			this.getRouter().navTo("purchaseOrder", {
				purchaseOrderId: oModel.getProperty("/selectedPurchaseOrder").ID
			}, bReplace);
		},

		_showDetail: function (oItem) {

			var bReplace = !Device.system.phone;
			var oModel = this.getModel("PurchaseOrders");
			this.getRouter().navTo("purchaseOrder", {
				alias: this.getModel("Login").getProperty("/alias"),
				purchaseOrderId: oModel.getObject(oItem.getBindingContextPath()).ID
			}, bReplace);
		},

		_applyFilterSearch: function () {
			var aFilters = this._oListFilterState.aSearch.concat(this._oListFilterState.aFilter),
				oViewModel = this.getModel("PurchaseOrders");
			this._oList.getBinding("items").filter(aFilters, "Application");
			if (aFilters.length !== 0) {
				oViewModel.setProperty("/noDataText", this.getResourceBundle().getText("_labelNoPurchaseOrdersText"));
			}
		},

		_loadModels: function () {

			//Initiate local storage			
			localStorePurchaseOrders.init();
			localStorePurchasers.init();
			localStoreSuppliers.init();

			//Initiate Purchasers			
			this.getOwnerComponent().setModel(modelsPurchasers.createPurchasersModel(), "Purchasers");
			this.loadEntityMetaData("purchaser", this.getModel("Purchasers"));
			bizNetAccessPurchasers.loadAllPurchasers(this.getOwnerComponent(), this.getModel("Purchasers"));

			//Initiate Suppliers (for value help)
			this.getOwnerComponent().setModel(modelsSuppliers.createSuppliersModel(), "Suppliers");
			this.loadEntityMetaData("supplier", this.getModel("Suppliers"));
			bizNetAccessSuppliers.loadAllSuppliers(this.getOwnerComponent(), this.getModel("Suppliers"));

			//Set currently logged in Purchaser
			if (this.getModel("Login").getProperty("/alias") !== "") {
				var pur = bizNetAccessPurchasers.getPurchaserFromAlias(this, this.getModel("Login").getProperty("/alias"));
				this.getOwnerComponent().getModel("Login").setProperty("/id", pur.ID);
				bizNetAccessPurchasers.loadPurchaser(this.getModel("Purchasers"), pur.ID);
			}

			//Load all purchase orders of purchaser			
			this.getOwnerComponent().setModel(modelsPurchaseOrders.createPurchaseOrdersModel(), "PurchaseOrders");
			if (typeof pur !== "undefined") {
				this.getModel("PurchaseOrders").setProperty("/purchaser", pur);
			}
			this.loadEntityMetaData("purchaseOrder", this.getModel("PurchaseOrders"));
			bizNetAccessPurchaseOrders.loadAllPurchaseOrders(this.getOwnerComponent(), this.getModel("PurchaseOrders"));
		},

		_onToggle: function () {

			this.getOwnerComponent().getModel("TestSwitch").setProperty(
				"/testMode", !this.getOwnerComponent().getModel("TestSwitch").getProperty("/testMode")
			);
			var location = this.getRunMode().location;
			messageProvider.addMessage(
				"Success",
				"Switching Network for Blockchain Data to: ",
				"No Description",
				location,
				1,
				"",
				"http://www.sap.com"
			);
			this._loadModels();
		},

		onMessagePress: function () {

			this.onShowMessageDialog("InDiPlat Log");
		},

		onRefreshPurchaseOrders: function () {

			bizNetAccessPurchaseOrders.loadAllPurchaseOrders(this.getOwnerComponent(), this.getModel("PurchaseOrders"));
		},

		onNavBack: function () {

			this.getRouter().navTo("home");
		}
	});
});