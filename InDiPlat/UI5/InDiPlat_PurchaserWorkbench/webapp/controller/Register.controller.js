sap.ui.define([
	"Workbench/controller/BaseController",
	"Workbench/util/messageProvider",
	"Workbench/util/localStorePurchasers",
	"Workbench/model/modelsBase",
	"Workbench/model/modelsPurchasers",
	"Workbench/util/bizNetAccessPurchasers"
], function(BaseController, messageProvider, localStorePurchasers, modelsBase, modelsPurchasers, bizNetAccessPurchasers) {
	"use strict";
	return BaseController.extend("Workbench.controller.Register", {
		onInit: function() {
			this._loadModel();
		},
		onAddNew: function() {
			bizNetAccessPurchasers.addNewPurchaser(this.getOwnerComponent(), this.getModel("Purchasers"));
		},
		_loadModel: function() {
			localStorePurchasers.init();
			if (typeof this.getOwnerComponent().getModel("Messages") === "undefined") {
				this.getOwnerComponent().setModel(modelsBase.createMessagesModel(), "Messages");
			}
			messageProvider.init(this.getOwnerComponent().getModel("Messages"));
			if (typeof this.getModel("Purchasers") === "undefined") {
				this.getOwnerComponent().setModel(modelsPurchasers.createPurchasersModel(), "Purchasers");
			}
			this.loadEntityMetaData("purchaser", this.getModel("Purchasers"));
			bizNetAccessPurchasers.loadAllPurchasers(this.getOwnerComponent(), this.getModel("Purchasers"));
		},

		onNavBack: function() {
			this.getRouter().navTo("home", {}, true);
		}
	});
});