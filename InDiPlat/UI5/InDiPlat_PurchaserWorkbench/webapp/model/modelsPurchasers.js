sap.ui.define([
	"sap/ui/model/json/JSONModel"
], function(JSONModel) {
	"use strict";

	return {

		createPurchasersModel: function(){
			
			var oModel = new JSONModel();
			oModel.setDefaultBindingMode("TwoWay");
			oModel.setData({
				chaincode:{
					serviceUrl:"",
					serviceKey:{},
					entity:{}					
				},
				purchaserCollection	: {
					cols:[
						{name:"ID"},
						{name:"ObjectType"},
						{name:"Alias"},
						{name:"Description"}
					],
					items:[]
				},
				selectedPurchaser	: {},		
				newPurchaser		: {
					ID						:	"",
					ObjectType				:	"",
					Alias					:	"",
					Description				:	""
				},
				selectedPurchaserID	: "",
				searchPurchaserID : ""
			});
			return oModel;			
		}
	};
});