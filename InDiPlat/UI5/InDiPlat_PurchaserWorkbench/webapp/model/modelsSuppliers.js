sap.ui.define([
	"sap/ui/model/json/JSONModel"
], function(JSONModel) {
	"use strict";

	return {

		createSuppliersModel: function(){
			
			var oModel = new JSONModel();
			oModel.setDefaultBindingMode("TwoWay");
			oModel.setData({
//Check and adapt for serviceKey:{}<-TODO							
				chaincode:{
					serviceUrl:"",
					serviceKey:{},
					entity:{}					
				},
				supplierCollection	: {
					cols:[
						{name:"ID"},
						{name:"ObjectType"},
						{name:"Alias"},
						{name:"Description"}
					],
					items:[]
				},
				selectedSupplier	: {},		
				newSupplier		: {
					ID						:	"",
					ObjectType				:	"",
					Alias					:	"",
					Description				:	""
				},
				selectedSupplierID	: "",
				searchSupplierID : ""
			});
			return oModel;			
		}
	};
});