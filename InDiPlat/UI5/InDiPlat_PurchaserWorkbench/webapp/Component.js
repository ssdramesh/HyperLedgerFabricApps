sap.ui.define([
	"sap/ui/core/UIComponent",
	"sap/ui/Device",
	"Workbench/model/modelsBase",
	"Workbench/model/modelsLogin",
	"Workbench/model/modelsPurchaseOrders",
	"Workbench/model/modelsPurchasers",
	"sap/ui/model/json/JSONModel"
], function(
		UIComponent, 
		Device, 
		modelsBase,
		modelsLogin,
		modelsPurchaseOrders,
		modelsPurchasers,
		JSONModel
	) {
	"use strict";

	return UIComponent.extend("Workbench.Component", {

		metadata: {
			manifest: "json"
		},

		/**
		 * The component is initialized by UI5 automatically during the startup of the app and calls the init method once.
		 * @public
		 * @override
		 */
		init: function() {
			// call the base component's init function
			UIComponent.prototype.init.apply(this, arguments);

			//Turn UI testing mode globally ON/OFF using the following
			this.setModel(modelsBase.createTestSwitchModel(false),"TestSwitch");

			// set the device model
			this.setModel(modelsBase.createDeviceModel(), "device");
			
			//set the login model at start
			this.setModel(modelsLogin.createLoginModel(), "Login");
			
			//set the messages model
			this.setModel(modelsBase.createMessagesModel(),"Messages");
			
			//Chaincode Deployment Descriptor
			var that = this;
			$.ajax("./model/chaincodeDetails.json", {
				dataType: "json",
				async:false,
				success: function(data) {
					var oModel = new JSONModel(data);
					that.setModel(oModel, "ChaincodeDetails");
				}
			});				
			
			// Initialize router
			this.getRouter().initialize();			
		}
	});
});