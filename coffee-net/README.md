# What's in here?
This is a demo for illustrating the connected supply chain using blockchain for the plantation to cup journey of coffee.
This demo is part of the Leonardo Center APJ at Singapore

Use-case Cluster: Blockchain for Supply Chain (Coffee)

# Context
Health Insurance Claim sample form

# Business Network Specification
The business network covers the asset (coffee bags, finished coffee product batches) transfers between various key participants of the supply chain like plantation farmer, coffee processing plant, dealers and supermarkets

# Model
![Coffee Network Data Model](./Model/coffeeNetModel.png)

# Deployed Systems

[Hyperledger Node Dashboard](https://hyperledger-fabric-dashboard.cfapps.sap.hana.ondemand.com/95b3f99d-e7a6-4315-847d-638092f78fd8/cd81abc5-43da-4cea-bc3e-f58fd1de2097/b8237e25-c762-4cb1-9c94-17035b551164/node)
[Hyperledger Channel Dashboard](https://hyperledger-fabric-dashboard.cfapps.sap.hana.ondemand.com/95b3f99d-e7a6-4315-847d-638092f78fd8/cd81abc5-43da-4cea-bc3e-f58fd1de2097/756cd9a3-a908-473b-ba16-3b9254583e25/channel)

# Demo URLs
[Journey of Coffee](https://coffeenetleotrace-g85556318.dispatcher.jp1.hana.ondemand.com/webapp/index.html?hc_reset)
[Asset Handovers (Touches)](https://coffeenetleotouches-g85556318.dispatcher.jp1.hana.ondemand.com/webapp/index.html?hc_reset)

# Demo Script(s)

## Pre-requisite:
This demo is deployed in the internal canary landscape.  Therefore, the user must be logged in to the SAP-Corporate Network (WiFi- SAP Corporate or BigEdge F5 logged-in)

A short script for demo:
1. Open the Journey of Coffee URL.  Type in BAT1001 in the search field.  The journey of coffee is shown on the map and the all the asset handovers are listed.
2. Click on the asset handover (first entry) for the processing plant.  The sourcing quality and quantity attributes open up for the coffee processing plant.
3. Click on one of teh bags, to find out the journey of this indivisual bag from plantation (farmer) to the coffee processing plant.
4. Open the [URL](https://coffeenetleotouches-g85556318.dispatcher.jp1.hana.ondemand.com/webapp/index.html?hc_reset) for all the asset handovers.  Click on one of teh touches on the left side in the master list.  On the right side you can see where the asset transfer happended.  Press the check button to verify if the quantities handed over match (No pilfrage)

# Published Links
No links are published yet in Innobook.

# Contact(s):
Ramesh Suraparaju
