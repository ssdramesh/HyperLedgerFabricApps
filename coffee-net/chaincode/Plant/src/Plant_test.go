package main

import (
	"bytes"
	"encoding/json"
	"fmt"
	"strings"
	"testing"

	"github.com/hyperledger/fabric/core/chaincode/shim"
)

//TestPlantParticipant_Init
func TestPlantParticipant_Init(t *testing.T) {
	plant := new(PlantParticipant)
	stub := shim.NewMockStub("plant", plant)
	checkInit(t, stub, [][]byte{[]byte("init")})
	checkState(t, stub, "plantIDIndex", getExpectedPlantIDIndex("init"))
}

//TestPlantParticipant_InvokeUnknownFunction
func TestPlantParticipant_InvokeUnknownFunction(t *testing.T) {
	plant := new(PlantParticipant)
	stub := shim.NewMockStub("plant", plant)
	checkInit(t, stub, [][]byte{[]byte("init")})
	checkInvokeUnknownFunction(t, stub, [][]byte{[]byte("myFunction"), []byte("docType:Participant")})
}

//TestPlantParticipant_Invoke_addNewPlant
func TestPlantParticipant_Invoke_addNewPlantOK(t *testing.T) {
	plant := new(PlantParticipant)
	stub := shim.NewMockStub("plant", plant)
	checkInit(t, stub, [][]byte{[]byte("init")})
	checkInvoke(t, stub, getFirstPlantParticipantForTesting())
	newPlantID := "100001"
	checkState(t, stub, newPlantID, getNewPlantExpected())
	checkState(t, stub, "plantIDIndex", getExpectedPlantIDIndex("addNewPlant"))
}

//TestPlantParticipant_Invoke_addNewPlantUnknownField
func TestPlantParticipant_Invoke_addNewPlantUnknownField(t *testing.T) {
	plant := new(PlantParticipant)
	stub := shim.NewMockStub("plant", plant)
	checkInit(t, stub, [][]byte{[]byte("init")})
	res := stub.MockInvoke("1", getPlantParticipantWithUnknownFieldForTesting())
	if res.Status != shim.OK {
		checkError(t, "Plant Data is Corrupted", res.Message)
	} else {
		fmt.Println("Unknown Field Error was expected, but not raised")
		t.FailNow()
	}
}

//TestPlantParticipant_Invoke_addNewPlant
func TestPlantParticipant_Invoke_addNewPlantDuplicate(t *testing.T) {
	plant := new(PlantParticipant)
	stub := shim.NewMockStub("plant", plant)
	checkInit(t, stub, [][]byte{[]byte("init")})
	checkInvoke(t, stub, getFirstPlantParticipantForTesting())
	newPlantID := "100001"
	checkState(t, stub, newPlantID, getNewPlantExpected())
	checkState(t, stub, "plantIDIndex", getExpectedPlantIDIndex("addNewPlant"))
	res := stub.MockInvoke("1", getFirstPlantParticipantForTesting())
	if res.Status != shim.OK {
		checkError(t, "This Plant already exists: 100001", res.Message)
	} else {
		fmt.Println("Error was expected, but not raised")
		t.FailNow()
	}
}

//TestPlantParticipant_Invoke_removePlantOK  //change template
func TestPlantParticipant_Invoke_removePlantOK(t *testing.T) {
	plant := new(PlantParticipant)
	stub := shim.NewMockStub("plant", plant)
	checkInit(t, stub, [][]byte{[]byte("init")})
	checkInvoke(t, stub, getFirstPlantParticipantForTesting())
	checkInvoke(t, stub, getSecondPlantParticipantForTesting())
	checkReadAllPlantsOK(t, stub)
	checkState(t, stub, "plantIDIndex", getExpectedPlantIDIndex("beforeRemovePlant"))
	checkInvoke(t, stub, getRemoveSecondPlantParticipantForTesting())
	remainingPlantID := "100001"
	checkReadPlantOK(t, stub, remainingPlantID)
	checkState(t, stub, "plantIDIndex", getExpectedPlantIDIndex("afterRemovePlant"))
}

//TestPlantParticipant_Invoke_removePlantNOK  //change template
func TestPlantParticipant_Invoke_removePlantNOK(t *testing.T) {
	plant := new(PlantParticipant)
	stub := shim.NewMockStub("plant", plant)
	checkInit(t, stub, [][]byte{[]byte("init")})
	checkInvoke(t, stub, getFirstPlantParticipantForTesting())
	firstPlantID := "100001"
	checkReadPlantOK(t, stub, firstPlantID)
	checkState(t, stub, "plantIDIndex", getExpectedPlantIDIndex("addNewPlant"))
	res := stub.MockInvoke("1", getRemoveSecondPlantParticipantForTesting())
	if res.Status != shim.OK {
		checkError(t, "Plant with ID: "+"100002"+" not found", res.Message)
	} else {
		fmt.Println("Error was expected, but not raised")
		t.FailNow()
	}
	checkState(t, stub, "plantIDIndex", getExpectedPlantIDIndex("addNewPlant"))
}

//TestPlantParticipant_Invoke_removeAllPlantsOK  //change template
func TestPlantParticipant_Invoke_removeAllPlantsOK(t *testing.T) {
	plant := new(PlantParticipant)
	stub := shim.NewMockStub("plant", plant)
	checkInit(t, stub, [][]byte{[]byte("init")})
	checkInvoke(t, stub, getFirstPlantParticipantForTesting())
	checkInvoke(t, stub, getSecondPlantParticipantForTesting())
	checkReadAllPlantsOK(t, stub)
	checkState(t, stub, "plantIDIndex", getExpectedPlantIDIndex("beforeRemovePlant"))
	checkInvoke(t, stub, getRemoveAllPlantParticipantsForTesting())
	checkState(t, stub, "plantIDIndex", getExpectedPlantIDIndex(""))
}

//TestPlantParticipant_Invoke_removePlantNOK  //change template
func TestPlantParticipant_Invoke_removeAllPlantsNOK(t *testing.T) {
	plant := new(PlantParticipant)
	stub := shim.NewMockStub("plant", plant)
	checkInit(t, stub, [][]byte{[]byte("init")})
	res := stub.MockInvoke("1", getRemoveAllPlantParticipantsForTesting())
	if res.Status != shim.OK {
		checkError(t, "removeAllPlants: No plants to remove", res.Message)
	} else {
		fmt.Println("Error was expected, but not raised")
		t.FailNow()
	}
	checkState(t, stub, "plantIDIndex", getExpectedPlantIDIndex(""))
}

//TestPlantParticipant_Query_readPlant
func TestPlantParticipant_Query_readPlant(t *testing.T) {
	plant := new(PlantParticipant)
	stub := shim.NewMockStub("plant", plant)
	checkInit(t, stub, [][]byte{[]byte("init")})
	plantID := "100001"
	checkInvoke(t, stub, getFirstPlantParticipantForTesting())
	checkReadPlantOK(t, stub, plantID)
	checkReadPlantNOK(t, stub, "")
}

//TestPlantParticipant_Query_readAllPlants
func TestPlantParticipant_Query_readAllPlants(t *testing.T) {
	plant := new(PlantParticipant)
	stub := shim.NewMockStub("plant", plant)
	checkInit(t, stub, [][]byte{[]byte("init")})
	checkInvoke(t, stub, getFirstPlantParticipantForTesting())
	checkInvoke(t, stub, getSecondPlantParticipantForTesting())
	checkReadAllPlantsOK(t, stub)
}

/*
*
*	Helper Functions
*
 */
//Get first PlantParticipant for testing
func getFirstPlantParticipantForTesting() [][]byte {
	return [][]byte{[]byte("addNewPlant"),
		[]byte("{\"ID\":\"100001\",\"docType\":\"Participant.PlantParticipant\",\"alias\":\"BRITECH\",\"description\":\"British Technology Pvt. Ltd.\",\"latitude\":\"12.1234567\",\"longitude\":\"120.1234567\"}")}
}

//Get second PlantParticipant for testing
func getSecondPlantParticipantForTesting() [][]byte {
	return [][]byte{[]byte("addNewPlant"),
		[]byte("{\"ID\":\"100002\",\"docType\":\"Participant.PlantParticipant\",\"alias\":\"DEMAGDELAG\",\"description\":\"Demag Delewal AG\",\"latitude\":\"11.1234567\",\"longitude\":\"110.1234567\"}")}
}

//Get PlantParticipant with unknown field for testing
func getPlantParticipantWithUnknownFieldForTesting() [][]byte {
	return [][]byte{[]byte("addNewPlant"),
		[]byte("{\"ID\":\"100001\",\"docuType\":\"Participant.PlantParticipant\",\"alias\":\"BRITECH\",\"description\":\"British Technology Pvt. Ltd.\",\"latitude\":\"12.1234567\",\"longitude\":\"120.1234567\"}")}
}

//Get remove second PlantParticipant for testing //change template
func getRemoveSecondPlantParticipantForTesting() [][]byte {
	return [][]byte{[]byte("removePlant"),
		[]byte("100002")}
}

//Get remove all PlantParticipants for testing //change template
func getRemoveAllPlantParticipantsForTesting() [][]byte {
	return [][]byte{[]byte("removeAllPlants")}
}

//Get an expected value for testing
func getNewPlantExpected() []byte {
	var plant Plant
	plant.ID = "100001"
	plant.ObjectType = "Participant.PlantParticipant"
	plant.Alias = "BRITECH"
	plant.Description = "British Technology Pvt. Ltd."
	plant.Latitude = "12.1234567"
	plant.Longitude = "120.1234567"
	plantJSON, err := json.Marshal(plant)
	if err != nil {
		fmt.Println("Error converting a Plant record to JSON")
		return nil
	}
	return []byte(plantJSON)
}

//Get expected values of Plants for testing
func getExpectedPlants() []byte {
	var plants []Plant
	var plant Plant
	plant.ID = "100001"
	plant.ObjectType = "Participant.PlantParticipant"
	plant.Alias = "BRITECH"
	plant.Description = "British Technology Pvt. Ltd."
	plant.Latitude = "12.1234567"
	plant.Longitude = "120.1234567"
	plants = append(plants, plant)
	plant.ID = "100002"
	plant.ObjectType = "Participant.PlantParticipant"
	plant.Alias = "DEMAGDELAG"
	plant.Description = "Demag Delewal AG"
	plant.Latitude = "11.1234567"
	plant.Longitude = "110.1234567"
	plants = append(plants, plant)
	plantJSON, err := json.Marshal(plants)
	if err != nil {
		fmt.Println("Error converting plantancer records to JSON")
		return nil
	}
	return []byte(plantJSON)
}

func getExpectedPlantIDIndex(funcName string) []byte {
	var plantIDIndex PlantIDIndex
	switch funcName {
	case "addNewPlant":
		plantIDIndex.IDs = append(plantIDIndex.IDs, "100001")
		plantIDIndexBytes, err := json.Marshal(plantIDIndex)
		if err != nil {
			fmt.Println("Error converting PlantIDIndex to JSON")
			return nil
		}
		return plantIDIndexBytes
	case "beforeRemovePlant":
		plantIDIndex.IDs = append(plantIDIndex.IDs, "100001")
		plantIDIndex.IDs = append(plantIDIndex.IDs, "100002")
		plantIDIndexBytes, err := json.Marshal(plantIDIndex)
		if err != nil {
			fmt.Println("Error converting PlantIDIndex to JSON")
			return nil
		}
		return plantIDIndexBytes
	case "afterRemovePlant":
		plantIDIndex.IDs = append(plantIDIndex.IDs, "100001")
		plantIDIndexBytes, err := json.Marshal(plantIDIndex)
		if err != nil {
			fmt.Println("Error converting PlantIDIndex to JSON")
			return nil
		}
		return plantIDIndexBytes
	default:
		plantIDIndexBytes, err := json.Marshal(plantIDIndex)
		if err != nil {
			fmt.Println("Error converting PlantIDIndex to JSON")
			return nil
		}
		return plantIDIndexBytes
	}
}

//checkInit - helper to check the Initialization of chaincode: PlantParticipant
func checkInit(t *testing.T, stub *shim.MockStub, args [][]byte) {
	res := stub.MockInit("1", args)
	if res.Status != shim.OK {
		fmt.Println("Init failed", string(res.Message))
		t.FailNow()
	}
}

//checkState - helper for checking the chaincode state for a given stateKey afgainst an expected value
func checkState(t *testing.T, stub *shim.MockStub, stateKey string, expectedState []byte) {
	actualState := stub.State[stateKey]
	if actualState == nil {
		fmt.Println("State for ", stateKey, ": failed to get value")
		t.FailNow()
	}

	if bytes.Compare(actualState, expectedState) != 0 {
		fmt.Println("Incorrect State for " + stateKey + ": \nExpected: " + string(expectedState) + "\nActual  : " + string(actualState))
		t.FailNow()
	}
}

//checkInvoke - helper for checking Invoke of chaincode
func checkInvoke(t *testing.T, stub *shim.MockStub, args [][]byte) {
	res := stub.MockInvoke("1", args)
	if res.Status != shim.OK {
		fmt.Println("Invoke", args, "failed", string(res.Message))
		t.FailNow()
	}
}

func checkInvokeUnknownFunction(t *testing.T, stub *shim.MockStub, args [][]byte) {
	res := stub.MockInvoke("1", args)
	if res.Status != shim.OK {
		expectedErr := "Received unknown function invocation"
		actualErr := string(res.Message)
		if !(strings.Contains(actualErr, expectedErr)) {
			fmt.Println("Invoke with unknown function must throw error: ", expectedErr, "\n Actual Error :", actualErr)
			t.FailNow()
		}
	} else {
		t.FailNow()
	}
}

//checkReadPlantOK - helper for positive test readPlant
func checkReadPlantOK(t *testing.T, stub *shim.MockStub, plantancerID string) {
	res := stub.MockInvoke("1", [][]byte{[]byte("readPlant"), []byte(plantancerID)})
	if res.Status != shim.OK {
		fmt.Println("func readPlant with ID: ", plantancerID, " failed"+string(res.Message))
		t.FailNow()
	}
	if res.Payload == nil {
		fmt.Println("func readPlant with ID: ", plantancerID, "failed to get value")
		t.FailNow()
	}
	if bytes.Compare(getNewPlantExpected(), []byte(res.Payload)) != 0 {
		fmt.Println("func readPlant with ID: ", plantancerID, "Expected:", string(getNewPlantExpected()), "Actual:", string(res.Payload))
		t.FailNow()
	}
}

//checkReadPlantNOK - helper for negative testing of readPlant
func checkReadPlantNOK(t *testing.T, stub *shim.MockStub, plantancerID string) {
	//with no plantancerID
	res := stub.MockInvoke("1", [][]byte{[]byte("readPlant"), []byte("")})
	if res.Status != shim.OK {
		expectedErr := "retrievePlant: Corrupt plant record "
		actualErr := string(res.Message)
		if !(strings.Contains(actualErr, expectedErr)) {
			fmt.Println("func readPlant neagtive test: ", "Expected Error:", expectedErr, "Actual Error", actualErr)
			t.FailNow()
		}
	} else {
		t.FailNow()
	}
}
func checkReadAllPlantsOK(t *testing.T, stub *shim.MockStub) {
	res := stub.MockInvoke("1", [][]byte{[]byte("readAllPlants")})
	if res.Status != shim.OK {
		fmt.Println("func readAllPlants failed", string(res.Message))
		t.FailNow()
	}
	if res.Payload == nil {
		fmt.Println("func readAllPlants failed to get value")
		t.FailNow()
	}
	if bytes.Compare(getExpectedPlants(), []byte(res.Payload)) != 0 {
		fmt.Println("func readAllPlants Expected:\n", string(getExpectedPlants()), "\nActual:\n", string(res.Payload))
		t.FailNow()
	}
}

func checkError(t *testing.T, exp string, act string) {
	if strings.Compare(exp, act) != 0 {
		fmt.Println("Unexpected Error! Expecting ", exp, "\n Actual :", act)
		t.FailNow()
	}
}
