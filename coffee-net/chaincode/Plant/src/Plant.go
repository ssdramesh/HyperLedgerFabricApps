package main

import (
	"encoding/json"
	"errors"
	"fmt"
	"strings"

	"github.com/hyperledger/fabric/core/chaincode/shim"
	"github.com/hyperledger/fabric/protos/peer"
)

var logger = shim.NewLogger("CLDChaincode")

//PlantParticipant - Chaincode for Plant Participant
type PlantParticipant struct {
}

//Plant - Details of the participant type Plant
type Plant struct {
	ID          string `json:"ID"`
	ObjectType  string `json:"docType"`
	Alias       string `json:"alias"`
	Description string `json:"description"`
	Latitude    string `json:"latitude"`
	Longitude   string `json:"longitude"`
}

//PlantIDIndex - Index on IDs for retrieval all Plants
type PlantIDIndex struct {
	IDs []string `json:"IDs"`
}

func main() {
	err := shim.Start(new(PlantParticipant))
	if err != nil {
		fmt.Printf("Error starting PlantParticipant chaincode function main(): %s", err)
	} else {
		fmt.Printf("Starting PlantParticipant chaincode function main() executed successfully")
	}
}

//Init - The chaincode Init function: No  arguments, only initializes a ID array as Index for retrieval of all Plants
func (plt *PlantParticipant) Init(stub shim.ChaincodeStubInterface) peer.Response {
	var plantIDIndex PlantIDIndex
	record, _ := stub.GetState("plantIDIndex")
	//chaincode upgrade: Index might already exist, create new only if it does not
	if record == nil {
		bytes, _ := json.Marshal(plantIDIndex)
		stub.PutState("plantIDIndex", bytes)
	}
	return shim.Success(nil)
}

//Invoke - The chaincode Invoke function:
func (plt *PlantParticipant) Invoke(stub shim.ChaincodeStubInterface) peer.Response {
	function, args := stub.GetFunctionAndParameters()
	logger.Debug("function: ", function)
	switch function {
	case "addNewPlant":
		return plt.addNewPlant(stub, args)
	case "removePlant":
		return plt.removePlant(stub, args[0])
	case "removeAllPlants":
		return plt.removeAllPlants(stub)
	case "readPlant":
		return plt.readPlant(stub, args[0])
	case "readAllPlants":
		return plt.readAllPlants(stub)
	default:
		return shim.Error("Received unknown function invocation")
	}
}

//Invoke Route: addNewPlant
func (plt *PlantParticipant) addNewPlant(stub shim.ChaincodeStubInterface, args []string) peer.Response {
	plant, err := getPlantFromArgs(args)
	if err != nil {
		return shim.Error("Plant Data is Corrupted")
	}
	plant.ObjectType = "Participant.PlantParticipant"
	record, err := stub.GetState(plant.ID)
	if record != nil {
		return shim.Error("This Plant already exists: " + plant.ID)
	}
	_, err = plt.savePlant(stub, plant)
	if err != nil {
		return shim.Error(err.Error())
	}
	_, err = plt.updatePlantIDIndex(stub, plant)
	if err != nil {
		return shim.Error(err.Error())
	}
	return shim.Success(nil)
}

//Invoke Route: removePlant
func (plt *PlantParticipant) removePlant(stub shim.ChaincodeStubInterface, plantStructParticipantID string) peer.Response {
	_, err := plt.deletePlant(stub, plantStructParticipantID)
	if err != nil {
		return shim.Error(err.Error())
	}
	_, err = plt.deletePlantIDIndex(stub, plantStructParticipantID)
	if err != nil {
		return shim.Error(err.Error())
	}
	return shim.Success(nil)
}

//Invoke Route: removeAllPlants
func (plt *PlantParticipant) removeAllPlants(stub shim.ChaincodeStubInterface) peer.Response {
	var plantStructIDs PlantIDIndex
	bytes, err := stub.GetState("plantIDIndex")
	if err != nil {
		return shim.Error("removeAllPlants: Error getting plantIDIndex array")
	}
	err = json.Unmarshal(bytes, &plantStructIDs)
	if err != nil {
		return shim.Error("removeAllPlants: Error unmarshalling plantIDIndex array JSON")
	}
	if len(plantStructIDs.IDs) == 0 {
		return shim.Error("removeAllPlants: No plants to remove")
	}
	for _, plantStructParticipantID := range plantStructIDs.IDs {
		_, err = plt.deletePlant(stub, plantStructParticipantID)
		if err != nil {
			return shim.Error("Failed to remove Plant with ID: " + plantStructParticipantID)
		}
		_, err = plt.deletePlantIDIndex(stub, plantStructParticipantID)
		if err != nil {
			return shim.Error(err.Error())
		}
	}
	plt.initHolder(stub)
	return shim.Success(nil)
}

//Query Route: readPlant
func (plt *PlantParticipant) readPlant(stub shim.ChaincodeStubInterface, plantParticipantID string) peer.Response {
	plantAsByteArray, err := plt.retrievePlant(stub, plantParticipantID)
	if err != nil {
		return shim.Error(err.Error())
	}
	return shim.Success(plantAsByteArray)
}

//Query Route: readAllPlants
func (plt *PlantParticipant) readAllPlants(stub shim.ChaincodeStubInterface) peer.Response {
	var plantIDs PlantIDIndex
	bytes, err := stub.GetState("plantIDIndex")
	if err != nil {
		return shim.Error("readAllPlants: Error getting plantIDIndex array")
	}
	err = json.Unmarshal(bytes, &plantIDs)
	if err != nil {
		return shim.Error("readAllPlants: Error unmarshalling plantIDIndex array JSON")
	}
	result := "["

	var plantAsByteArray []byte

	for _, plantID := range plantIDs.IDs {
		plantAsByteArray, err = plt.retrievePlant(stub, plantID)
		if err != nil {
			return shim.Error("Failed to retrieve plant with ID: " + plantID)
		}
		result += string(plantAsByteArray) + ","
	}
	if len(result) == 1 {
		result = "[]"
	} else {
		result = result[:len(result)-1] + "]"
	}
	return shim.Success([]byte(result))
}

//Helper: Save plantParticipant
func (plt *PlantParticipant) savePlant(stub shim.ChaincodeStubInterface, plant Plant) (bool, error) {
	bytes, err := json.Marshal(plant)
	if err != nil {
		return false, errors.New("Error converting plant record JSON")
	}
	err = stub.PutState(plant.ID, bytes)
	if err != nil {
		return false, errors.New("Error storing Plant record")
	}
	return true, nil
}

//Helper: Delete plantStructParticipant
func (plt *PlantParticipant) deletePlant(stub shim.ChaincodeStubInterface, plantStructParticipantID string) (bool, error) {
	_, err := plt.retrievePlant(stub, plantStructParticipantID)
	if err != nil {
		return false, errors.New("Plant with ID: " + plantStructParticipantID + " not found")
	}
	err = stub.DelState(plantStructParticipantID)
	if err != nil {
		return false, errors.New("Error deleting Plant record")
	}
	return true, nil
}

//Helper: Update plant Holder - updates Index
func (plt *PlantParticipant) updatePlantIDIndex(stub shim.ChaincodeStubInterface, plant Plant) (bool, error) {
	var plantIDs PlantIDIndex
	bytes, err := stub.GetState("plantIDIndex")
	if err != nil {
		return false, errors.New("upadetePlantIDIndex: Error getting plantIDIndex array Index from state")
	}
	err = json.Unmarshal(bytes, &plantIDs)
	if err != nil {
		return false, errors.New("upadetePlantIDIndex: Error unmarshalling plantIDIndex array JSON")
	}
	plantIDs.IDs = append(plantIDs.IDs, plant.ID)
	bytes, err = json.Marshal(plantIDs)
	if err != nil {
		return false, errors.New("updatePlantIDIndex: Error marshalling new plant ID")
	}
	err = stub.PutState("plantIDIndex", bytes)
	if err != nil {
		return false, errors.New("updatePlantIDIndex: Error storing new plant ID in plantIDIndex (Index)")
	}
	return true, nil
}

//Helper: delete ID from plantStruct Holder
func (plt *PlantParticipant) deletePlantIDIndex(stub shim.ChaincodeStubInterface, plantStructParticipantID string) (bool, error) {
	var plantStructIDs PlantIDIndex
	bytes, err := stub.GetState("plantIDIndex")
	if err != nil {
		return false, errors.New("deletePlantIDIndex: Error getting plantIDIndex array Index from state")
	}
	err = json.Unmarshal(bytes, &plantStructIDs)
	if err != nil {
		return false, errors.New("deletePlantIDIndex: Error unmarshalling plantIDIndex array JSON")
	}
	plantStructIDs.IDs, err = deleteKeyFromStringArray(plantStructIDs.IDs, plantStructParticipantID)
	if err != nil {
		return false, errors.New(err.Error())
	}
	bytes, err = json.Marshal(plantStructIDs)
	if err != nil {
		return false, errors.New("deletePlantIDIndex: Error marshalling new plantStruct ID")
	}
	err = stub.PutState("plantIDIndex", bytes)
	if err != nil {
		return false, errors.New("deletePlantIDIndex: Error storing new plantStruct ID in plantIDIndex (Index)")
	}
	return true, nil
}

//Helper: Initialize plant ID Holder
func (plt *PlantParticipant) initHolder(stub shim.ChaincodeStubInterface) bool {
	var plantIDIndex PlantIDIndex
	bytes, _ := json.Marshal(plantIDIndex)
	stub.DelState("plantIDIndex")
	stub.PutState("plantIDIndex", bytes)
	return true
}

//Helper: Retrieve plantParticipant
func (plt *PlantParticipant) retrievePlant(stub shim.ChaincodeStubInterface, plantParticipantID string) ([]byte, error) {
	var plant Plant
	var plantAsByteArray []byte
	bytes, err := stub.GetState(plantParticipantID)
	if err != nil {
		return plantAsByteArray, errors.New("retrievePlant: Error retrieving plant with ID: " + plantParticipantID)
	}
	err = json.Unmarshal(bytes, &plant)
	if err != nil {
		return plantAsByteArray, errors.New("retrievePlant: Corrupt plant record " + string(bytes))
	}
	plantAsByteArray, err = json.Marshal(plant)
	if err != nil {
		return plantAsByteArray, errors.New("readPlant: Invalid plant Object - Not a  valid JSON")
	}
	return plantAsByteArray, nil
}

//deleteKeyFromArray
func deleteKeyFromStringArray(array []string, key string) (newArray []string, err error) {
	for _, entry := range array {
		if entry != key {
			newArray = append(newArray, entry)
		}
	}
	if len(newArray) == len(array) {
		return newArray, errors.New("Specified Key: " + key + " not found in Array")
	}
	return newArray, nil
}

//getPlantFromArgs - construct a plant structure from string array of arguments
func getPlantFromArgs(args []string) (plant Plant, err error) {

	if strings.Contains(args[0], "\"ID\"") == false ||
		strings.Contains(args[0], "\"docType\"") == false ||
		strings.Contains(args[0], "\"alias\"") == false ||
		strings.Contains(args[0], "\"description\"") == false ||
		strings.Contains(args[0], "\"latitude\"") == false ||
		strings.Contains(args[0], "\"longitude\"") == false {
		return plant, errors.New("Unknown field: Input JSON does not comply to schema")
	}

	err = json.Unmarshal([]byte(args[0]), &plant)
	if err != nil {
		return plant, err
	}
	return plant, nil
}
