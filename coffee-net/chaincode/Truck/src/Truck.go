package main

import (
	"encoding/json"
	"errors"
	"fmt"
	"strings"

	"github.com/hyperledger/fabric/core/chaincode/shim"
	"github.com/hyperledger/fabric/protos/peer"
)

var logger = shim.NewLogger("CLDChaincode")

//TruckParticipant - Chaincode for Truck Participant
type TruckParticipant struct {
}

//Truck - Details of the participant type Truck
type Truck struct {
	ID          string `json:"ID"`
	ObjectType  string `json:"docType"`
	Alias       string `json:"alias"`
	Description string `json:"description"`
}

//TruckIDIndex - Index on IDs for retrieval all Trucks
type TruckIDIndex struct {
	IDs []string `json:"IDs"`
}

func main() {
	err := shim.Start(new(TruckParticipant))
	if err != nil {
		fmt.Printf("Error starting TruckParticipant chaincode function main(): %s", err)
	} else {
		fmt.Printf("Starting TruckParticipant chaincode function main() executed successfully")
	}
}

//Init - The chaincode Init function: No  arguments, only initializes a ID array as Index for retrieval of all Trucks
func (trk *TruckParticipant) Init(stub shim.ChaincodeStubInterface) peer.Response {
	var truckIDIndex TruckIDIndex
	record, _ := stub.GetState("truckIDIndex")
	//chaincode upgrade: Index might already exist, create new only if it does not
	if record == nil {
		bytes, _ := json.Marshal(truckIDIndex)
		stub.PutState("truckIDIndex", bytes)
	}
	return shim.Success(nil)
}

//Invoke - The chaincode Invoke function:
func (trk *TruckParticipant) Invoke(stub shim.ChaincodeStubInterface) peer.Response {
	function, args := stub.GetFunctionAndParameters()
	logger.Debug("function: ", function)
	switch function {
	case "addNewTruck":
		return trk.addNewTruck(stub, args)
	case "removeTruck":
		return trk.removeTruck(stub, args[0])
	case "removeAllTrucks":
		return trk.removeAllTrucks(stub)
	case "readTruck":
		return trk.readTruck(stub, args[0])
	case "readAllTrucks":
		return trk.readAllTrucks(stub)
	default:
		return shim.Error("Received unknown function invocation")
	}
}

//Invoke Route: addNewTruck
func (trk *TruckParticipant) addNewTruck(stub shim.ChaincodeStubInterface, args []string) peer.Response {
	truck, err := getTruckFromArgs(args)
	if err != nil {
		return shim.Error("Truck Data is Corrupted")
	}
	truck.ObjectType = "Participant.TruckParticipant"
	record, err := stub.GetState(truck.ID)
	if record != nil {
		return shim.Error("This Truck already exists: " + truck.ID)
	}
	_, err = trk.saveTruck(stub, truck)
	if err != nil {
		return shim.Error(err.Error())
	}
	_, err = trk.updateTruckIDIndex(stub, truck)
	if err != nil {
		return shim.Error(err.Error())
	}
	return shim.Success(nil)
}

//Invoke Route: removeTruck
func (trk *TruckParticipant) removeTruck(stub shim.ChaincodeStubInterface, truckStructParticipantID string) peer.Response {
	_, err := trk.deleteTruck(stub, truckStructParticipantID)
	if err != nil {
		return shim.Error(err.Error())
	}
	_, err = trk.deleteTruckIDIndex(stub, truckStructParticipantID)
	if err != nil {
		return shim.Error(err.Error())
	}
	return shim.Success(nil)
}

//Invoke Route: removeAllTrucks
func (trk *TruckParticipant) removeAllTrucks(stub shim.ChaincodeStubInterface) peer.Response {
	var truckStructIDs TruckIDIndex
	bytes, err := stub.GetState("truckIDIndex")
	if err != nil {
		return shim.Error("removeAllTrucks: Error getting truckIDIndex array")
	}
	err = json.Unmarshal(bytes, &truckStructIDs)
	if err != nil {
		return shim.Error("removeAllTrucks: Error unmarshalling truckIDIndex array JSON")
	}
	if len(truckStructIDs.IDs) == 0 {
		return shim.Error("removeAllTrucks: No trucks to remove")
	}
	for _, truckStructParticipantID := range truckStructIDs.IDs {
		_, err = trk.deleteTruck(stub, truckStructParticipantID)
		if err != nil {
			return shim.Error("Failed to remove Truck with ID: " + truckStructParticipantID)
		}
		_, err = trk.deleteTruckIDIndex(stub, truckStructParticipantID)
		if err != nil {
			return shim.Error(err.Error())
		}
	}
	trk.initHolder(stub)
	return shim.Success(nil)
}

//Query Route: readTruck
func (trk *TruckParticipant) readTruck(stub shim.ChaincodeStubInterface, truckParticipantID string) peer.Response {
	truckAsByteArray, err := trk.retrieveTruck(stub, truckParticipantID)
	if err != nil {
		return shim.Error(err.Error())
	}
	return shim.Success(truckAsByteArray)
}

//Query Route: readAllTrucks
func (trk *TruckParticipant) readAllTrucks(stub shim.ChaincodeStubInterface) peer.Response {
	var truckIDs TruckIDIndex
	bytes, err := stub.GetState("truckIDIndex")
	if err != nil {
		return shim.Error("readAllTrucks: Error getting truckIDIndex array")
	}
	err = json.Unmarshal(bytes, &truckIDs)
	if err != nil {
		return shim.Error("readAllTrucks: Error unmarshalling truckIDIndex array JSON")
	}
	result := "["

	var truckAsByteArray []byte

	for _, truckID := range truckIDs.IDs {
		truckAsByteArray, err = trk.retrieveTruck(stub, truckID)
		if err != nil {
			return shim.Error("Failed to retrieve truck with ID: " + truckID)
		}
		result += string(truckAsByteArray) + ","
	}
	if len(result) == 1 {
		result = "[]"
	} else {
		result = result[:len(result)-1] + "]"
	}
	return shim.Success([]byte(result))
}

//Helper: Save truckParticipant
func (trk *TruckParticipant) saveTruck(stub shim.ChaincodeStubInterface, truck Truck) (bool, error) {
	bytes, err := json.Marshal(truck)
	if err != nil {
		return false, errors.New("Error converting truck record JSON")
	}
	err = stub.PutState(truck.ID, bytes)
	if err != nil {
		return false, errors.New("Error storing Truck record")
	}
	return true, nil
}

//Helper: Delete truckStructParticipant
func (trk *TruckParticipant) deleteTruck(stub shim.ChaincodeStubInterface, truckStructParticipantID string) (bool, error) {
	_, err := trk.retrieveTruck(stub, truckStructParticipantID)
	if err != nil {
		return false, errors.New("Truck with ID: " + truckStructParticipantID + " not found")
	}
	err = stub.DelState(truckStructParticipantID)
	if err != nil {
		return false, errors.New("Error deleting Truck record")
	}
	return true, nil
}

//Helper: Update truck Holder - updates Index
func (trk *TruckParticipant) updateTruckIDIndex(stub shim.ChaincodeStubInterface, truck Truck) (bool, error) {
	var truckIDs TruckIDIndex
	bytes, err := stub.GetState("truckIDIndex")
	if err != nil {
		return false, errors.New("upadeteTruckIDIndex: Error getting truckIDIndex array Index from state")
	}
	err = json.Unmarshal(bytes, &truckIDs)
	if err != nil {
		return false, errors.New("upadeteTruckIDIndex: Error unmarshalling truckIDIndex array JSON")
	}
	truckIDs.IDs = append(truckIDs.IDs, truck.ID)
	bytes, err = json.Marshal(truckIDs)
	if err != nil {
		return false, errors.New("updateTruckIDIndex: Error marshalling new truck ID")
	}
	err = stub.PutState("truckIDIndex", bytes)
	if err != nil {
		return false, errors.New("updateTruckIDIndex: Error storing new truck ID in truckIDIndex (Index)")
	}
	return true, nil
}

//Helper: delete ID from truckStruct Holder
func (trk *TruckParticipant) deleteTruckIDIndex(stub shim.ChaincodeStubInterface, truckStructParticipantID string) (bool, error) {
	var truckStructIDs TruckIDIndex
	bytes, err := stub.GetState("truckIDIndex")
	if err != nil {
		return false, errors.New("deleteTruckIDIndex: Error getting truckIDIndex array Index from state")
	}
	err = json.Unmarshal(bytes, &truckStructIDs)
	if err != nil {
		return false, errors.New("deleteTruckIDIndex: Error unmarshalling truckIDIndex array JSON")
	}
	truckStructIDs.IDs, err = deleteKeyFromStringArray(truckStructIDs.IDs, truckStructParticipantID)
	if err != nil {
		return false, errors.New(err.Error())
	}
	bytes, err = json.Marshal(truckStructIDs)
	if err != nil {
		return false, errors.New("deleteTruckIDIndex: Error marshalling new truckStruct ID")
	}
	err = stub.PutState("truckIDIndex", bytes)
	if err != nil {
		return false, errors.New("deleteTruckIDIndex: Error storing new truckStruct ID in truckIDIndex (Index)")
	}
	return true, nil
}

//Helper: Initialize truck ID Holder
func (trk *TruckParticipant) initHolder(stub shim.ChaincodeStubInterface) bool {
	var truckIDIndex TruckIDIndex
	bytes, _ := json.Marshal(truckIDIndex)
	stub.DelState("truckIDIndex")
	stub.PutState("truckIDIndex", bytes)
	return true
}

//Helper: Retrieve truckParticipant
func (trk *TruckParticipant) retrieveTruck(stub shim.ChaincodeStubInterface, truckParticipantID string) ([]byte, error) {
	var truck Truck
	var truckAsByteArray []byte
	bytes, err := stub.GetState(truckParticipantID)
	if err != nil {
		return truckAsByteArray, errors.New("retrieveTruck: Error retrieving truck with ID: " + truckParticipantID)
	}
	err = json.Unmarshal(bytes, &truck)
	if err != nil {
		return truckAsByteArray, errors.New("retrieveTruck: Corrupt truck record " + string(bytes))
	}
	truckAsByteArray, err = json.Marshal(truck)
	if err != nil {
		return truckAsByteArray, errors.New("readTruck: Invalid truck Object - Not a  valid JSON")
	}
	return truckAsByteArray, nil
}

//deleteKeyFromArray
func deleteKeyFromStringArray(array []string, key string) (newArray []string, err error) {
	for _, entry := range array {
		if entry != key {
			newArray = append(newArray, entry)
		}
	}
	if len(newArray) == len(array) {
		return newArray, errors.New("Specified Key: " + key + " not found in Array")
	}
	return newArray, nil
}

//getTruckFromArgs - construct a truck structure from string array of arguments
func getTruckFromArgs(args []string) (truck Truck, err error) {

	if strings.Contains(args[0], "\"ID\"") == false ||
		strings.Contains(args[0], "\"docType\"") == false ||
		strings.Contains(args[0], "\"alias\"") == false ||
		strings.Contains(args[0], "\"description\"") == false {
		return truck, errors.New("Unknown field: Input JSON does not comply to schema")
	}

	err = json.Unmarshal([]byte(args[0]), &truck)
	if err != nil {
		return truck, err
	}
	return truck, nil
}
