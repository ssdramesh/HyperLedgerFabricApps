package main

import (
	"bytes"
	"encoding/json"
	"fmt"
	"strings"
	"testing"

	"github.com/hyperledger/fabric/core/chaincode/shim"
)

//TestTruckParticipant_Init
func TestTruckParticipant_Init(t *testing.T) {
	truck := new(TruckParticipant)
	stub := shim.NewMockStub("truck", truck)
	checkInit(t, stub, [][]byte{[]byte("init")})
	checkState(t, stub, "truckIDIndex", getExpectedTruckIDIndex("init"))
}

//TestTruckParticipant_InvokeUnknownFunction
func TestTruckParticipant_InvokeUnknownFunction(t *testing.T) {
	truck := new(TruckParticipant)
	stub := shim.NewMockStub("truck", truck)
	checkInit(t, stub, [][]byte{[]byte("init")})
	checkInvokeUnknownFunction(t, stub, [][]byte{[]byte("myFunction"), []byte("docType:Participant")})
}

//TestTruckParticipant_Invoke_addNewTruck
func TestTruckParticipant_Invoke_addNewTruckOK(t *testing.T) {
	truck := new(TruckParticipant)
	stub := shim.NewMockStub("truck", truck)
	checkInit(t, stub, [][]byte{[]byte("init")})
	checkInvoke(t, stub, getFirstTruckParticipantForTesting())
	newTruckID := "100001"
	checkState(t, stub, newTruckID, getNewTruckExpected())
	checkState(t, stub, "truckIDIndex", getExpectedTruckIDIndex("addNewTruck"))
}

//TestTruckParticipant_Invoke_addNewTruckUnknownField
func TestTruckParticipant_Invoke_addNewTruckUnknownField(t *testing.T) {
	truck := new(TruckParticipant)
	stub := shim.NewMockStub("truck", truck)
	checkInit(t, stub, [][]byte{[]byte("init")})
	res := stub.MockInvoke("1", getTruckParticipantWithUnknownFieldForTesting())
	if res.Status != shim.OK {
		checkError(t, "Truck Data is Corrupted", res.Message)
	} else {
		fmt.Println("Unknown Field Error was expected, but not raised")
		t.FailNow()
	}
}

//TestTruckParticipant_Invoke_addNewTruck
func TestTruckParticipant_Invoke_addNewTruckDuplicate(t *testing.T) {
	truck := new(TruckParticipant)
	stub := shim.NewMockStub("truck", truck)
	checkInit(t, stub, [][]byte{[]byte("init")})
	checkInvoke(t, stub, getFirstTruckParticipantForTesting())
	newTruckID := "100001"
	checkState(t, stub, newTruckID, getNewTruckExpected())
	checkState(t, stub, "truckIDIndex", getExpectedTruckIDIndex("addNewTruck"))
	res := stub.MockInvoke("1", getFirstTruckParticipantForTesting())
	if res.Status != shim.OK {
		checkError(t, "This Truck already exists: 100001", res.Message)
	} else {
		fmt.Println("Error was expected, but not raised")
		t.FailNow()
	}
}

//TestTruckParticipant_Invoke_removeTruckOK  //change template
func TestTruckParticipant_Invoke_removeTruckOK(t *testing.T) {
	truck := new(TruckParticipant)
	stub := shim.NewMockStub("truck", truck)
	checkInit(t, stub, [][]byte{[]byte("init")})
	checkInvoke(t, stub, getFirstTruckParticipantForTesting())
	checkInvoke(t, stub, getSecondTruckParticipantForTesting())
	checkReadAllTrucksOK(t, stub)
	checkState(t, stub, "truckIDIndex", getExpectedTruckIDIndex("beforeRemoveTruck"))
	checkInvoke(t, stub, getRemoveSecondTruckParticipantForTesting())
	remainingTruckID := "100001"
	checkReadTruckOK(t, stub, remainingTruckID)
	checkState(t, stub, "truckIDIndex", getExpectedTruckIDIndex("afterRemoveTruck"))
}

//TestTruckParticipant_Invoke_removeTruckNOK  //change template
func TestTruckParticipant_Invoke_removeTruckNOK(t *testing.T) {
	truck := new(TruckParticipant)
	stub := shim.NewMockStub("truck", truck)
	checkInit(t, stub, [][]byte{[]byte("init")})
	checkInvoke(t, stub, getFirstTruckParticipantForTesting())
	firstTruckID := "100001"
	checkReadTruckOK(t, stub, firstTruckID)
	checkState(t, stub, "truckIDIndex", getExpectedTruckIDIndex("addNewTruck"))
	res := stub.MockInvoke("1", getRemoveSecondTruckParticipantForTesting())
	if res.Status != shim.OK {
		checkError(t, "Truck with ID: "+"100002"+" not found", res.Message)
	} else {
		fmt.Println("Error was expected, but not raised")
		t.FailNow()
	}
	checkState(t, stub, "truckIDIndex", getExpectedTruckIDIndex("addNewTruck"))
}

//TestTruckParticipant_Invoke_removeAllTrucksOK  //change template
func TestTruckParticipant_Invoke_removeAllTrucksOK(t *testing.T) {
	truck := new(TruckParticipant)
	stub := shim.NewMockStub("truck", truck)
	checkInit(t, stub, [][]byte{[]byte("init")})
	checkInvoke(t, stub, getFirstTruckParticipantForTesting())
	checkInvoke(t, stub, getSecondTruckParticipantForTesting())
	checkReadAllTrucksOK(t, stub)
	checkState(t, stub, "truckIDIndex", getExpectedTruckIDIndex("beforeRemoveTruck"))
	checkInvoke(t, stub, getRemoveAllTruckParticipantsForTesting())
	checkState(t, stub, "truckIDIndex", getExpectedTruckIDIndex(""))
}

//TestTruckParticipant_Invoke_removeTruckNOK  //change template
func TestTruckParticipant_Invoke_removeAllTrucksNOK(t *testing.T) {
	truck := new(TruckParticipant)
	stub := shim.NewMockStub("truck", truck)
	checkInit(t, stub, [][]byte{[]byte("init")})
	res := stub.MockInvoke("1", getRemoveAllTruckParticipantsForTesting())
	if res.Status != shim.OK {
		checkError(t, "removeAllTrucks: No trucks to remove", res.Message)
	} else {
		fmt.Println("Error was expected, but not raised")
		t.FailNow()
	}
	checkState(t, stub, "truckIDIndex", getExpectedTruckIDIndex(""))
}

//TestTruckParticipant_Query_readTruck
func TestTruckParticipant_Query_readTruck(t *testing.T) {
	truck := new(TruckParticipant)
	stub := shim.NewMockStub("truck", truck)
	checkInit(t, stub, [][]byte{[]byte("init")})
	truckID := "100001"
	checkInvoke(t, stub, getFirstTruckParticipantForTesting())
	checkReadTruckOK(t, stub, truckID)
	checkReadTruckNOK(t, stub, "")
}

//TestTruckParticipant_Query_readAllTrucks
func TestTruckParticipant_Query_readAllTrucks(t *testing.T) {
	truck := new(TruckParticipant)
	stub := shim.NewMockStub("truck", truck)
	checkInit(t, stub, [][]byte{[]byte("init")})
	checkInvoke(t, stub, getFirstTruckParticipantForTesting())
	checkInvoke(t, stub, getSecondTruckParticipantForTesting())
	checkReadAllTrucksOK(t, stub)
}

/*
*
*	Helper Functions
*
 */
//Get first TruckParticipant for testing
func getFirstTruckParticipantForTesting() [][]byte {
	return [][]byte{[]byte("addNewTruck"),
		[]byte("{\"ID\":\"100001\",\"docType\":\"Participant.TruckParticipant\",\"alias\":\"BRITECH\",\"description\":\"British Technology Pvt. Ltd.\"}")}
}

//Get second TruckParticipant for testing
func getSecondTruckParticipantForTesting() [][]byte {
	return [][]byte{[]byte("addNewTruck"),
		[]byte("{\"ID\":\"100002\",\"docType\":\"Participant.TruckParticipant\",\"alias\":\"DEMAGDELAG\",\"description\":\"Demag Delewal AG\"}")}
}

//Get TruckParticipant with unknown field for testing
func getTruckParticipantWithUnknownFieldForTesting() [][]byte {
	return [][]byte{[]byte("addNewTruck"),
		[]byte("{\"ID\":\"100001\",\"docuType\":\"Participant.TruckParticipant\",\"alias\":\"BRITECH\",\"description\":\"British Technology Pvt. Ltd.\"}")}
}

//Get remove second TruckParticipant for testing //change template
func getRemoveSecondTruckParticipantForTesting() [][]byte {
	return [][]byte{[]byte("removeTruck"),
		[]byte("100002")}
}

//Get update TruckParticipant for OK testing
func getUpdateTruckParticipantForOKTesting() [][]byte {
	return [][]byte{[]byte("updateTruck"),
		[]byte("{\"ID\":\"100001\",\"docType\":\"Participant.TruckParticipant\",\"alias\":\"BRITECH (New)\",\"description\":\"British Technology Pvt. Ltd.\"}")}
}

//Get remove all TruckParticipants for testing //change template
func getRemoveAllTruckParticipantsForTesting() [][]byte {
	return [][]byte{[]byte("removeAllTrucks")}
}

//Get an expected value for testing
func getNewTruckExpected() []byte {
	var truck Truck
	truck.ID = "100001"
	truck.ObjectType = "Participant.TruckParticipant"
	truck.Alias = "BRITECH"
	truck.Description = "British Technology Pvt. Ltd."
	truckJSON, err := json.Marshal(truck)
	if err != nil {
		fmt.Println("Error converting a Truck record to JSON")
		return nil
	}
	return []byte(truckJSON)
}

//Get an expected value for testing
func getUpdatedTruckExpected() []byte {
	var truck Truck
	truck.ID = "100001"
	truck.ObjectType = "Participant.TruckParticipant"
	truck.Alias = "BRITECH (New)"
	truck.Description = "British Technology Pvt. Ltd."
	truckJSON, err := json.Marshal(truck)
	if err != nil {
		fmt.Println("Error converting a Truck record to JSON")
		return nil
	}
	return []byte(truckJSON)
}

//Get expected values of Trucks for testing
func getExpectedTrucks() []byte {
	var trucks []Truck
	var truck Truck
	truck.ID = "100001"
	truck.ObjectType = "Participant.TruckParticipant"
	truck.Alias = "BRITECH"
	truck.Description = "British Technology Pvt. Ltd."
	trucks = append(trucks, truck)
	truck.ID = "100002"
	truck.ObjectType = "Participant.TruckParticipant"
	truck.Alias = "DEMAGDELAG"
	truck.Description = "Demag Delewal AG"
	trucks = append(trucks, truck)
	truckJSON, err := json.Marshal(trucks)
	if err != nil {
		fmt.Println("Error converting truckancer records to JSON")
		return nil
	}
	return []byte(truckJSON)
}

func getExpectedTruckIDIndex(funcName string) []byte {
	var truckIDIndex TruckIDIndex
	switch funcName {
	case "addNewTruck":
		truckIDIndex.IDs = append(truckIDIndex.IDs, "100001")
		truckIDIndexBytes, err := json.Marshal(truckIDIndex)
		if err != nil {
			fmt.Println("Error converting TruckIDIndex to JSON")
			return nil
		}
		return truckIDIndexBytes
	case "beforeRemoveTruck":
		truckIDIndex.IDs = append(truckIDIndex.IDs, "100001")
		truckIDIndex.IDs = append(truckIDIndex.IDs, "100002")
		truckIDIndexBytes, err := json.Marshal(truckIDIndex)
		if err != nil {
			fmt.Println("Error converting TruckIDIndex to JSON")
			return nil
		}
		return truckIDIndexBytes
	case "afterRemoveTruck":
		truckIDIndex.IDs = append(truckIDIndex.IDs, "100001")
		truckIDIndexBytes, err := json.Marshal(truckIDIndex)
		if err != nil {
			fmt.Println("Error converting TruckIDIndex to JSON")
			return nil
		}
		return truckIDIndexBytes
	default:
		truckIDIndexBytes, err := json.Marshal(truckIDIndex)
		if err != nil {
			fmt.Println("Error converting TruckIDIndex to JSON")
			return nil
		}
		return truckIDIndexBytes
	}
}

//checkInit - helper to check the Initialization of chaincode: TruckParticipant
func checkInit(t *testing.T, stub *shim.MockStub, args [][]byte) {
	res := stub.MockInit("1", args)
	if res.Status != shim.OK {
		fmt.Println("Init failed", string(res.Message))
		t.FailNow()
	}
}

//checkState - helper for checking the chaincode state for a given stateKey afgainst an expected value
func checkState(t *testing.T, stub *shim.MockStub, stateKey string, expectedState []byte) {
	actualState := stub.State[stateKey]
	if actualState == nil {
		fmt.Println("State for ", stateKey, ": failed to get value")
		t.FailNow()
	}

	if bytes.Compare(actualState, expectedState) != 0 {
		fmt.Println("Incorrect State for " + stateKey + ": \nExpected: " + string(expectedState) + "\nActual  : " + string(actualState))
		t.FailNow()
	}
}

//checkInvoke - helper for checking Invoke of chaincode
func checkInvoke(t *testing.T, stub *shim.MockStub, args [][]byte) {
	res := stub.MockInvoke("1", args)
	if res.Status != shim.OK {
		fmt.Println("Invoke", args, "failed", string(res.Message))
		t.FailNow()
	}
}

func checkInvokeUnknownFunction(t *testing.T, stub *shim.MockStub, args [][]byte) {
	res := stub.MockInvoke("1", args)
	if res.Status != shim.OK {
		expectedErr := "Received unknown function invocation"
		actualErr := string(res.Message)
		if !(strings.Contains(actualErr, expectedErr)) {
			fmt.Println("Invoke with unknown function must throw error: ", expectedErr, "\n Actual Error :", actualErr)
			t.FailNow()
		}
	} else {
		t.FailNow()
	}
}

//checkReadTruckOK - helper for positive test readTruck
func checkReadTruckOK(t *testing.T, stub *shim.MockStub, truckancerID string) {
	res := stub.MockInvoke("1", [][]byte{[]byte("readTruck"), []byte(truckancerID)})
	if res.Status != shim.OK {
		fmt.Println("func readTruck with ID: ", truckancerID, " failed"+string(res.Message))
		t.FailNow()
	}
	if res.Payload == nil {
		fmt.Println("func readTruck with ID: ", truckancerID, "failed to get value")
		t.FailNow()
	}
	if bytes.Compare(getNewTruckExpected(), []byte(res.Payload)) != 0 {
		fmt.Println("func readTruck with ID: ", truckancerID, "Expected:", string(getNewTruckExpected()), "Actual:", string(res.Payload))
		t.FailNow()
	}
}

//checkReadTruckNOK - helper for negative testing of readTruck
func checkReadTruckNOK(t *testing.T, stub *shim.MockStub, truckancerID string) {
	//with no truckancerID
	res := stub.MockInvoke("1", [][]byte{[]byte("readTruck"), []byte("")})
	if res.Status != shim.OK {
		expectedErr := "retrieveTruck: Corrupt truck record "
		actualErr := string(res.Message)
		if !(strings.Contains(actualErr, expectedErr)) {
			fmt.Println("func readTruck neagtive test: ", "Expected Error:", expectedErr, "Actual Error", actualErr)
			t.FailNow()
		}
	} else {
		t.FailNow()
	}
}
func checkReadAllTrucksOK(t *testing.T, stub *shim.MockStub) {
	res := stub.MockInvoke("1", [][]byte{[]byte("readAllTrucks")})
	if res.Status != shim.OK {
		fmt.Println("func readAllTrucks failed", string(res.Message))
		t.FailNow()
	}
	if res.Payload == nil {
		fmt.Println("func readAllTrucks failed to get value")
		t.FailNow()
	}
	if bytes.Compare(getExpectedTrucks(), []byte(res.Payload)) != 0 {
		fmt.Println("func readAllTrucks Expected:\n", string(getExpectedTrucks()), "\nActual:\n", string(res.Payload))
		t.FailNow()
	}
}

func checkError(t *testing.T, exp string, act string) {
	if strings.Compare(exp, act) != 0 {
		fmt.Println("Unexpected Error! Expecting ", exp, "\n Actual :", act)
		t.FailNow()
	}
}
