package main

import (
	"bytes"
	"encoding/json"
	"fmt"
	"strings"
	"testing"

	"github.com/hyperledger/fabric/core/chaincode/shim"
)

//TestTouchAsset_Init
func TestTouchAsset_Init(t *testing.T) {
	touch := new(TouchAsset)
	stub := shim.NewMockStub("touch", touch)
	checkInit(t, stub, [][]byte{[]byte("init")})
	checkState(t, stub, "touchIDIndex", getExpectedTouchIDIndex("init"))
}

//TestTouchAsset_InvokeUnknownFunction
func TestTouchAsset_InvokeUnknownFunction(t *testing.T) {
	touch := new(TouchAsset)
	stub := shim.NewMockStub("touch", touch)
	checkInit(t, stub, [][]byte{[]byte("init")})
	checkInvokeUnknownFunction(t, stub, [][]byte{[]byte("myFunction"), []byte("docType:Asset")})
}

//TestTouchAsset_Invoke_addNewTouch
func TestTouchAsset_Invoke_addNewTouchOK(t *testing.T) {
	touch := new(TouchAsset)
	stub := shim.NewMockStub("touch", touch)
	checkInit(t, stub, [][]byte{[]byte("init")})
	checkInvoke(t, stub, getFirstTouchAssetForTesting())
	newTouchID := "100001"
	checkState(t, stub, newTouchID, getNewTouchExpected())
	checkState(t, stub, "touchIDIndex", getExpectedTouchIDIndex("addNewTouch"))
}

//TestTouchAsset_Invoke_addNewTouchUnknownField
func TestTouchAsset_Invoke_addNewTouchUnknownField(t *testing.T) {
	touch := new(TouchAsset)
	stub := shim.NewMockStub("touch", touch)
	checkInit(t, stub, [][]byte{[]byte("init")})
	res := stub.MockInvoke("1", getTouchAssetWithUnknownFieldForTesting())
	if res.Status != shim.OK {
		checkError(t, "Touch Data is Corrupted", res.Message)
	} else {
		fmt.Println("Unknown Field Error was expected, but not raised")
		t.FailNow()
	}
}

//TestTouchAsset_Invoke_addNewTouch
func TestTouchAsset_Invoke_addNewTouchDuplicate(t *testing.T) {
	touch := new(TouchAsset)
	stub := shim.NewMockStub("touch", touch)
	checkInit(t, stub, [][]byte{[]byte("init")})
	checkInvoke(t, stub, getFirstTouchAssetForTesting())
	newTouchID := "100001"
	checkState(t, stub, newTouchID, getNewTouchExpected())
	checkState(t, stub, "touchIDIndex", getExpectedTouchIDIndex("addNewTouch"))
	res := stub.MockInvoke("1", getFirstTouchAssetForTesting())
	if res.Status != shim.OK {
		checkError(t, "This Touch already exists: 100001", res.Message)
	} else {
		fmt.Println("Error was expected, but not raised")
		t.FailNow()
	}
}

//TestTouchAsset_Invoke_removeTouchOK  //change template
func TestTouchAsset_Invoke_removeTouchOK(t *testing.T) {
	touch := new(TouchAsset)
	stub := shim.NewMockStub("touch", touch)
	checkInit(t, stub, [][]byte{[]byte("init")})
	checkInvoke(t, stub, getFirstTouchAssetForTesting())
	checkInvoke(t, stub, getSecondTouchAssetForTesting())
	checkReadAllTouchsOK(t, stub)
	checkState(t, stub, "touchIDIndex", getExpectedTouchIDIndex("beforeRemoveTouch"))
	checkInvoke(t, stub, getRemoveSecondTouchAssetForTesting())
	remainingTouchID := "100001"
	checkReadTouchOK(t, stub, remainingTouchID)
	checkState(t, stub, "touchIDIndex", getExpectedTouchIDIndex("afterRemoveTouch"))
}

//TestTouchAsset_Invoke_removeTouchNOK  //change template
func TestTouchAsset_Invoke_removeTouchNOK(t *testing.T) {
	touch := new(TouchAsset)
	stub := shim.NewMockStub("touch", touch)
	checkInit(t, stub, [][]byte{[]byte("init")})
	checkInvoke(t, stub, getFirstTouchAssetForTesting())
	firstTouchID := "100001"
	checkReadTouchOK(t, stub, firstTouchID)
	checkState(t, stub, "touchIDIndex", getExpectedTouchIDIndex("addNewTouch"))
	res := stub.MockInvoke("1", getRemoveSecondTouchAssetForTesting())
	if res.Status != shim.OK {
		checkError(t, "Touch with ID: "+"100002"+" not found", res.Message)
	} else {
		fmt.Println("Error was expected, but not raised")
		t.FailNow()
	}
	checkState(t, stub, "touchIDIndex", getExpectedTouchIDIndex("addNewTouch"))
}

//TestTouchAsset_Invoke_removeAllTouchsOK  //change template
func TestTouchAsset_Invoke_removeAllTouchsOK(t *testing.T) {
	touch := new(TouchAsset)
	stub := shim.NewMockStub("touch", touch)
	checkInit(t, stub, [][]byte{[]byte("init")})
	checkInvoke(t, stub, getFirstTouchAssetForTesting())
	checkInvoke(t, stub, getSecondTouchAssetForTesting())
	checkReadAllTouchsOK(t, stub)
	checkState(t, stub, "touchIDIndex", getExpectedTouchIDIndex("beforeRemoveTouch"))
	checkInvoke(t, stub, getRemoveAllTouchAssetsForTesting())
	checkState(t, stub, "touchIDIndex", getExpectedTouchIDIndex(""))
}

//TestTouchAsset_Invoke_removeTouchNOK  //change template
func TestTouchAsset_Invoke_removeAllTouchsNOK(t *testing.T) {
	touch := new(TouchAsset)
	stub := shim.NewMockStub("touch", touch)
	checkInit(t, stub, [][]byte{[]byte("init")})
	res := stub.MockInvoke("1", getRemoveAllTouchAssetsForTesting())
	if res.Status != shim.OK {
		checkError(t, "removeAllTouchs: No touchs to remove", res.Message)
	} else {
		fmt.Println("Error was expected, but not raised")
		t.FailNow()
	}
	checkState(t, stub, "touchIDIndex", getExpectedTouchIDIndex(""))
}

//TestTouchAsset_Query_readTouch
func TestTouchAsset_Query_readTouch(t *testing.T) {
	touch := new(TouchAsset)
	stub := shim.NewMockStub("touch", touch)
	checkInit(t, stub, [][]byte{[]byte("init")})
	touchID := "100001"
	checkInvoke(t, stub, getFirstTouchAssetForTesting())
	checkReadTouchOK(t, stub, touchID)
	checkReadTouchNOK(t, stub, "")
}

//TestTouchAsset_Query_readAllTouchs
func TestTouchAsset_Query_readAllTouchs(t *testing.T) {
	touch := new(TouchAsset)
	stub := shim.NewMockStub("touch", touch)
	checkInit(t, stub, [][]byte{[]byte("init")})
	checkInvoke(t, stub, getFirstTouchAssetForTesting())
	checkInvoke(t, stub, getSecondTouchAssetForTesting())
	checkReadAllTouchsOK(t, stub)
}

/*
*
*	Helper Functions
*
 */
//Get first TouchAsset for testing
func getFirstTouchAssetForTesting() [][]byte {
	return [][]byte{[]byte("addNewTouch"),
		[]byte("{\"ID\":\"100001\",\"assetID\":\"100001\",\"participantID\":\"100001\",\"docType\":\"Asset.TouchAsset\",\"assetDocType\":\"Asset.BatchAsset\",\"participantDocType\":\"Participant.FarmerParticipant\",\"dateTime\":\"12/01/2018\",\"weight\":\"100\",\"latitude\":\"12.1234567\",\"longitude\":\"120.1234567\"}")}
}

//Get TouchAsset with unknown field for testing
func getTouchAssetWithUnknownFieldForTesting() [][]byte {
	return [][]byte{[]byte("addNewTouch"),
		[]byte("{\"ID\":\"100001\",\"assetID\":\"100001\",\"participantID\":\"100001\",\"docuType\":\"Asset.TouchAsset\",\"assetDocType\":\"Asset.BatchAsset\",\"participantDocType\":\"Participant.FarmerParticipant\",\"dateTime\":\"12/01/2018\",\"weight\":\"100\",\"latitude\":\"12.1234567\",\"longitude\":\"120.1234567\"}")}
}

//Get second TouchAsset for testing
func getSecondTouchAssetForTesting() [][]byte {
	return [][]byte{[]byte("addNewTouch"),
		[]byte("{\"ID\":\"100002\",\"assetID\":\"100002\",\"participantID\":\"100002\",\"docType\":\"Asset.TouchAsset\",\"assetDocType\":\"Asset.BatchAsset\",\"participantDocType\":\"Participant.DealerParticipant\",\"dateTime\":\"12/01/2018\",\"weight\":\"200\",\"latitude\":\"11.1234567\",\"longitude\":\"110.1234567\"}")}
}

//Get remove second TouchAsset for testing //change template
func getRemoveSecondTouchAssetForTesting() [][]byte {
	return [][]byte{[]byte("removeTouch"),
		[]byte("100002")}
}

//Get remove all TouchAssets for testing //change template
func getRemoveAllTouchAssetsForTesting() [][]byte {
	return [][]byte{[]byte("removeAllTouchs")}
}

//Get an expected value for testing
func getNewTouchExpected() []byte {
	var touch Touch
	touch.ID = "100001"
	touch.AssetID = "100001"
	touch.ParticipantID = "100001"
	touch.ObjectType = "Asset.TouchAsset"
	touch.AssetType = "Asset.BatchAsset"
	touch.ParticipantType = "Participant.FarmerParticipant"
	touch.DateTime = "12/01/2018"
	touch.Weight = "100"
	touch.Latitude = "12.1234567"
	touch.Longitude = "120.1234567"
	touchJSON, err := json.Marshal(touch)
	if err != nil {
		fmt.Println("Error converting a Touch record to JSON")
		return nil
	}
	return []byte(touchJSON)
}

//Get expected values of Touchs for testing
func getExpectedTouchs() []byte {
	var touchs []Touch
	var touch Touch
	touch.ID = "100001"
	touch.AssetID = "100001"
	touch.ParticipantID = "100001"
	touch.ObjectType = "Asset.TouchAsset"
	touch.AssetType = "Asset.BatchAsset"
	touch.ParticipantType = "Participant.FarmerParticipant"
	touch.DateTime = "12/01/2018"
	touch.Weight = "100"
	touch.Latitude = "12.1234567"
	touch.Longitude = "120.1234567"
	touchs = append(touchs, touch)
	touch.ID = "100002"
	touch.AssetID = "100002"
	touch.ParticipantID = "100002"
	touch.ObjectType = "Asset.TouchAsset"
	touch.AssetType = "Asset.BatchAsset"
	touch.ParticipantType = "Participant.DealerParticipant"
	touch.DateTime = "12/01/2018"
	touch.Weight = "200"
	touch.Latitude = "11.1234567"
	touch.Longitude = "110.1234567"
	touchs = append(touchs, touch)
	touchJSON, err := json.Marshal(touchs)
	if err != nil {
		fmt.Println("Error converting touch records to JSON")
		return nil
	}
	return []byte(touchJSON)
}

func getExpectedTouchIDIndex(funcName string) []byte {
	var touchIDIndex TouchIDIndex
	switch funcName {
	case "addNewTouch":
		touchIDIndex.IDs = append(touchIDIndex.IDs, "100001")
		touchIDIndexBytes, err := json.Marshal(touchIDIndex)
		if err != nil {
			fmt.Println("Error converting TouchIDIndex to JSON")
			return nil
		}
		return touchIDIndexBytes
	case "beforeRemoveTouch":
		touchIDIndex.IDs = append(touchIDIndex.IDs, "100001")
		touchIDIndex.IDs = append(touchIDIndex.IDs, "100002")
		touchIDIndexBytes, err := json.Marshal(touchIDIndex)
		if err != nil {
			fmt.Println("Error converting TouchIDIndex to JSON")
			return nil
		}
		return touchIDIndexBytes
	case "afterRemoveTouch":
		touchIDIndex.IDs = append(touchIDIndex.IDs, "100001")
		touchIDIndexBytes, err := json.Marshal(touchIDIndex)
		if err != nil {
			fmt.Println("Error converting TouchIDIndex to JSON")
			return nil
		}
		return touchIDIndexBytes
	default:
		touchIDIndexBytes, err := json.Marshal(touchIDIndex)
		if err != nil {
			fmt.Println("Error converting TouchIDIndex to JSON")
			return nil
		}
		return touchIDIndexBytes
	}
}

//checkInit - helper to check the Initialization of chaincode: TouchAsset
func checkInit(t *testing.T, stub *shim.MockStub, args [][]byte) {
	res := stub.MockInit("1", args)
	if res.Status != shim.OK {
		fmt.Println("Init failed", string(res.Message))
		t.FailNow()
	}
}

//checkState - helper for checking the chaincode state for a given stateKey afgainst an expected value
func checkState(t *testing.T, stub *shim.MockStub, stateKey string, expectedState []byte) {
	actualState := stub.State[stateKey]
	if actualState == nil {
		fmt.Println("State for ", stateKey, ": failed to get value")
		t.FailNow()
	}

	if bytes.Compare(actualState, expectedState) != 0 {
		fmt.Println("Incorrect State for " + stateKey + ": \nExpected: " + string(expectedState) + "\nActual  : " + string(actualState))
		t.FailNow()
	}
}

//checkInvoke - helper for checking Invoke of chaincode
func checkInvoke(t *testing.T, stub *shim.MockStub, args [][]byte) {
	res := stub.MockInvoke("1", args)
	if res.Status != shim.OK {
		fmt.Println("Invoke", args, "failed", string(res.Message))
		t.FailNow()
	}
}

func checkInvokeUnknownFunction(t *testing.T, stub *shim.MockStub, args [][]byte) {
	res := stub.MockInvoke("1", args)
	if res.Status != shim.OK {
		expectedErr := "Received unknown function invocation"
		actualErr := string(res.Message)
		if !(strings.Contains(actualErr, expectedErr)) {
			fmt.Println("Invoke with unknown function must throw error: ", expectedErr, "\n Actual Error :", actualErr)
			t.FailNow()
		}
	} else {
		t.FailNow()
	}
}

//checkReadTouchOK - helper for positive test readTouch
func checkReadTouchOK(t *testing.T, stub *shim.MockStub, touchID string) {
	res := stub.MockInvoke("1", [][]byte{[]byte("readTouch"), []byte(touchID)})
	if res.Status != shim.OK {
		fmt.Println("func readTouch with ID: ", touchID, " failed"+string(res.Message))
		t.FailNow()
	}
	if res.Payload == nil {
		fmt.Println("func readTouch with ID: ", touchID, "failed to get value")
		t.FailNow()
	}
	if bytes.Compare(getNewTouchExpected(), []byte(res.Payload)) != 0 {
		fmt.Println("func readTouch with ID: ", touchID, "Expected:", string(getNewTouchExpected()), "Actual:", string(res.Payload))
		t.FailNow()
	}
}

//checkReadTouchNOK - helper for negative testing of readTouch
func checkReadTouchNOK(t *testing.T, stub *shim.MockStub, touchID string) {
	//with no touchID
	res := stub.MockInvoke("1", [][]byte{[]byte("readTouch"), []byte("")})
	if res.Status != shim.OK {
		expectedErr := "retrieveTouch: Corrupt touch record "
		actualErr := string(res.Message)
		if !(strings.Contains(actualErr, expectedErr)) {
			fmt.Println("func readTouch negative test: ", "Expected Error:", expectedErr, "Actual Error", actualErr)
			t.FailNow()
		}
	} else {
		t.FailNow()
	}
}

func checkReadAllTouchsOK(t *testing.T, stub *shim.MockStub) {
	res := stub.MockInvoke("1", [][]byte{[]byte("readAllTouchs")})
	if res.Status != shim.OK {
		fmt.Println("func readAllTouchs failed", string(res.Message))
		t.FailNow()
	}
	if res.Payload == nil {
		fmt.Println("func readAllTouchs failed to get value")
		t.FailNow()
	}
	if bytes.Compare(getExpectedTouchs(), []byte(res.Payload)) != 0 {
		fmt.Println("func readAllTouchs Expected:\n", string(getExpectedTouchs()), "\nActual:\n", string(res.Payload))
		t.FailNow()
	}
}

func checkError(t *testing.T, exp string, act string) {
	if strings.Compare(exp, act) != 0 {
		fmt.Println("Unexpected Error! Expecting ", exp, "\n Actual :", act)
		t.FailNow()
	}
}
