package main

import (
	"encoding/json"
	"errors"
	"fmt"
	"strings"

	"github.com/hyperledger/fabric/core/chaincode/shim"
	"github.com/hyperledger/fabric/protos/peer"
)

var logger = shim.NewLogger("CLDChaincode")

//DealerParticipant - Chaincode for Dealer Participant
type DealerParticipant struct {
}

//Dealer - Details of the participant type Dealer
type Dealer struct {
	ID          string `json:"ID"`
	ObjectType  string `json:"docType"`
	Alias       string `json:"alias"`
	Description string `json:"description"`
}

//DealerIDIndex - Index on IDs for retrieval all Dealers
type DealerIDIndex struct {
	IDs []string `json:"IDs"`
}

func main() {
	err := shim.Start(new(DealerParticipant))
	if err != nil {
		fmt.Printf("Error starting DealerParticipant chaincode function main(): %s", err)
	} else {
		fmt.Printf("Starting DealerParticipant chaincode function main() executed successfully")
	}
}

//Init - The chaincode Init function: No  arguments, only initializes a ID array as Index for retrieval of all Dealers
func (dlr *DealerParticipant) Init(stub shim.ChaincodeStubInterface) peer.Response {
	var dealerIDIndex DealerIDIndex
	record, _ := stub.GetState("dealerIDIndex")
	//chaincode upgrade: Index might already exist, create new only if it does not
	if record == nil {
		bytes, _ := json.Marshal(dealerIDIndex)
		stub.PutState("dealerIDIndex", bytes)
	}
	return shim.Success(nil)
}

//Invoke - The chaincode Invoke function:
func (dlr *DealerParticipant) Invoke(stub shim.ChaincodeStubInterface) peer.Response {
	function, args := stub.GetFunctionAndParameters()
	logger.Debug("function: ", function)
	switch function {
	case "addNewDealer":
		return dlr.addNewDealer(stub, args)
	case "removeDealer":
		return dlr.removeDealer(stub, args[0])
	case "removeAllDealers":
		return dlr.removeAllDealers(stub)
	case "readDealer":
		return dlr.readDealer(stub, args[0])
	case "readAllDealers":
		return dlr.readAllDealers(stub)
	default:
		return shim.Error("Received unknown function invocation")
	}
}

//Invoke Route: addNewDealer
func (dlr *DealerParticipant) addNewDealer(stub shim.ChaincodeStubInterface, args []string) peer.Response {
	dealer, err := getDealerFromArgs(args)
	if err != nil {
		return shim.Error("Dealer Data is Corrupted")
	}
	dealer.ObjectType = "Participant.DealerParticipant"
	record, err := stub.GetState(dealer.ID)
	if record != nil {
		return shim.Error("This Dealer already exists: " + dealer.ID)
	}
	_, err = dlr.saveDealer(stub, dealer)
	if err != nil {
		return shim.Error(err.Error())
	}
	_, err = dlr.updateDealerIDIndex(stub, dealer)
	if err != nil {
		return shim.Error(err.Error())
	}
	return shim.Success(nil)
}

//Invoke Route: removeDealer
func (dlr *DealerParticipant) removeDealer(stub shim.ChaincodeStubInterface, dealerStructParticipantID string) peer.Response {
	_, err := dlr.deleteDealer(stub, dealerStructParticipantID)
	if err != nil {
		return shim.Error(err.Error())
	}
	_, err = dlr.deleteDealerIDIndex(stub, dealerStructParticipantID)
	if err != nil {
		return shim.Error(err.Error())
	}
	return shim.Success(nil)
}

//Invoke Route: removeAllDealers
func (dlr *DealerParticipant) removeAllDealers(stub shim.ChaincodeStubInterface) peer.Response {
	var dealerStructIDs DealerIDIndex
	bytes, err := stub.GetState("dealerIDIndex")
	if err != nil {
		return shim.Error("removeAllDealers: Error getting dealerIDIndex array")
	}
	err = json.Unmarshal(bytes, &dealerStructIDs)
	if err != nil {
		return shim.Error("removeAllDealers: Error unmarshalling dealerIDIndex array JSON")
	}
	if len(dealerStructIDs.IDs) == 0 {
		return shim.Error("removeAllDealers: No dealers to remove")
	}
	for _, dealerStructParticipantID := range dealerStructIDs.IDs {
		_, err = dlr.deleteDealer(stub, dealerStructParticipantID)
		if err != nil {
			return shim.Error("Failed to remove Dealer with ID: " + dealerStructParticipantID)
		}
		_, err = dlr.deleteDealerIDIndex(stub, dealerStructParticipantID)
		if err != nil {
			return shim.Error(err.Error())
		}
	}
	dlr.initHolder(stub)
	return shim.Success(nil)
}

//Query Route: readDealer
func (dlr *DealerParticipant) readDealer(stub shim.ChaincodeStubInterface, dealerParticipantID string) peer.Response {
	dealerAsByteArray, err := dlr.retrieveDealer(stub, dealerParticipantID)
	if err != nil {
		return shim.Error(err.Error())
	}
	return shim.Success(dealerAsByteArray)
}

//Query Route: readAllDealers
func (dlr *DealerParticipant) readAllDealers(stub shim.ChaincodeStubInterface) peer.Response {
	var dealerIDs DealerIDIndex
	bytes, err := stub.GetState("dealerIDIndex")
	if err != nil {
		return shim.Error("readAllDealers: Error getting dealerIDIndex array")
	}
	err = json.Unmarshal(bytes, &dealerIDs)
	if err != nil {
		return shim.Error("readAllDealers: Error unmarshalling dealerIDIndex array JSON")
	}
	result := "["

	var dealerAsByteArray []byte

	for _, dealerID := range dealerIDs.IDs {
		dealerAsByteArray, err = dlr.retrieveDealer(stub, dealerID)
		if err != nil {
			return shim.Error("Failed to retrieve dealer with ID: " + dealerID)
		}
		result += string(dealerAsByteArray) + ","
	}
	if len(result) == 1 {
		result = "[]"
	} else {
		result = result[:len(result)-1] + "]"
	}
	return shim.Success([]byte(result))
}

//Helper: Save dealerParticipant
func (dlr *DealerParticipant) saveDealer(stub shim.ChaincodeStubInterface, dealer Dealer) (bool, error) {
	bytes, err := json.Marshal(dealer)
	if err != nil {
		return false, errors.New("Error converting dealer record JSON")
	}
	err = stub.PutState(dealer.ID, bytes)
	if err != nil {
		return false, errors.New("Error storing Dealer record")
	}
	return true, nil
}

//Helper: Delete dealerStructParticipant
func (dlr *DealerParticipant) deleteDealer(stub shim.ChaincodeStubInterface, dealerStructParticipantID string) (bool, error) {
	_, err := dlr.retrieveDealer(stub, dealerStructParticipantID)
	if err != nil {
		return false, errors.New("Dealer with ID: " + dealerStructParticipantID + " not found")
	}
	err = stub.DelState(dealerStructParticipantID)
	if err != nil {
		return false, errors.New("Error deleting Dealer record")
	}
	return true, nil
}

//Helper: Update dealer Holder - updates Index
func (dlr *DealerParticipant) updateDealerIDIndex(stub shim.ChaincodeStubInterface, dealer Dealer) (bool, error) {
	var dealerIDs DealerIDIndex
	bytes, err := stub.GetState("dealerIDIndex")
	if err != nil {
		return false, errors.New("upadeteDealerIDIndex: Error getting dealerIDIndex array Index from state")
	}
	err = json.Unmarshal(bytes, &dealerIDs)
	if err != nil {
		return false, errors.New("upadeteDealerIDIndex: Error unmarshalling dealerIDIndex array JSON")
	}
	dealerIDs.IDs = append(dealerIDs.IDs, dealer.ID)
	bytes, err = json.Marshal(dealerIDs)
	if err != nil {
		return false, errors.New("updateDealerIDIndex: Error marshalling new dealer ID")
	}
	err = stub.PutState("dealerIDIndex", bytes)
	if err != nil {
		return false, errors.New("updateDealerIDIndex: Error storing new dealer ID in dealerIDIndex (Index)")
	}
	return true, nil
}

//Helper: delete ID from dealerStruct Holder
func (dlr *DealerParticipant) deleteDealerIDIndex(stub shim.ChaincodeStubInterface, dealerStructParticipantID string) (bool, error) {
	var dealerStructIDs DealerIDIndex
	bytes, err := stub.GetState("dealerIDIndex")
	if err != nil {
		return false, errors.New("deleteDealerIDIndex: Error getting dealerIDIndex array Index from state")
	}
	err = json.Unmarshal(bytes, &dealerStructIDs)
	if err != nil {
		return false, errors.New("deleteDealerIDIndex: Error unmarshalling dealerIDIndex array JSON")
	}
	dealerStructIDs.IDs, err = deleteKeyFromStringArray(dealerStructIDs.IDs, dealerStructParticipantID)
	if err != nil {
		return false, errors.New(err.Error())
	}
	bytes, err = json.Marshal(dealerStructIDs)
	if err != nil {
		return false, errors.New("deleteDealerIDIndex: Error marshalling new dealerStruct ID")
	}
	err = stub.PutState("dealerIDIndex", bytes)
	if err != nil {
		return false, errors.New("deleteDealerIDIndex: Error storing new dealerStruct ID in dealerIDIndex (Index)")
	}
	return true, nil
}

//Helper: Initialize dealer ID Holder
func (dlr *DealerParticipant) initHolder(stub shim.ChaincodeStubInterface) bool {
	var dealerIDIndex DealerIDIndex
	bytes, _ := json.Marshal(dealerIDIndex)
	stub.DelState("dealerIDIndex")
	stub.PutState("dealerIDIndex", bytes)
	return true
}

//Helper: Retrieve dealerParticipant
func (dlr *DealerParticipant) retrieveDealer(stub shim.ChaincodeStubInterface, dealerParticipantID string) ([]byte, error) {
	var dealer Dealer
	var dealerAsByteArray []byte
	bytes, err := stub.GetState(dealerParticipantID)
	if err != nil {
		return dealerAsByteArray, errors.New("retrieveDealer: Error retrieving dealer with ID: " + dealerParticipantID)
	}
	err = json.Unmarshal(bytes, &dealer)
	if err != nil {
		return dealerAsByteArray, errors.New("retrieveDealer: Corrupt dealer record " + string(bytes))
	}
	dealerAsByteArray, err = json.Marshal(dealer)
	if err != nil {
		return dealerAsByteArray, errors.New("readDealer: Invalid dealer Object - Not a  valid JSON")
	}
	return dealerAsByteArray, nil
}

//deleteKeyFromArray
func deleteKeyFromStringArray(array []string, key string) (newArray []string, err error) {
	for _, entry := range array {
		if entry != key {
			newArray = append(newArray, entry)
		}
	}
	if len(newArray) == len(array) {
		return newArray, errors.New("Specified Key: " + key + " not found in Array")
	}
	return newArray, nil
}

//getDealerFromArgs - construct a dealer structure from string array of arguments
func getDealerFromArgs(args []string) (dealer Dealer, err error) {

	if strings.Contains(args[0], "\"ID\"") == false ||
		strings.Contains(args[0], "\"docType\"") == false ||
		strings.Contains(args[0], "\"alias\"") == false ||
		strings.Contains(args[0], "\"description\"") == false {
		return dealer, errors.New("Unknown field: Input JSON does not comply to schema")
	}

	err = json.Unmarshal([]byte(args[0]), &dealer)
	if err != nil {
		return dealer, err
	}
	return dealer, nil
}
