package main

import (
	"bytes"
	"encoding/json"
	"fmt"
	"strings"
	"testing"

	"github.com/hyperledger/fabric/core/chaincode/shim"
)

//TestDealerParticipant_Init
func TestDealerParticipant_Init(t *testing.T) {
	dealer := new(DealerParticipant)
	stub := shim.NewMockStub("dealer", dealer)
	checkInit(t, stub, [][]byte{[]byte("init")})
	checkState(t, stub, "dealerIDIndex", getExpectedDealerIDIndex("init"))
}

//TestDealerParticipant_InvokeUnknownFunction
func TestDealerParticipant_InvokeUnknownFunction(t *testing.T) {
	dealer := new(DealerParticipant)
	stub := shim.NewMockStub("dealer", dealer)
	checkInit(t, stub, [][]byte{[]byte("init")})
	checkInvokeUnknownFunction(t, stub, [][]byte{[]byte("myFunction"), []byte("docType:Participant")})
}

//TestDealerParticipant_Invoke_addNewDealer
func TestDealerParticipant_Invoke_addNewDealerOK(t *testing.T) {
	dealer := new(DealerParticipant)
	stub := shim.NewMockStub("dealer", dealer)
	checkInit(t, stub, [][]byte{[]byte("init")})
	checkInvoke(t, stub, getFirstDealerParticipantForTesting())
	newDealerID := "100001"
	checkState(t, stub, newDealerID, getNewDealerExpected())
	checkState(t, stub, "dealerIDIndex", getExpectedDealerIDIndex("addNewDealer"))
}

//TestDealerParticipant_Invoke_addNewDealerUnknownField
func TestDealerParticipant_Invoke_addNewDealerUnknownField(t *testing.T) {
	dealer := new(DealerParticipant)
	stub := shim.NewMockStub("dealer", dealer)
	checkInit(t, stub, [][]byte{[]byte("init")})
	res := stub.MockInvoke("1", getDealerParticipantWithUnknownFieldForTesting())
	if res.Status != shim.OK {
		checkError(t, "Dealer Data is Corrupted", res.Message)
	} else {
		fmt.Println("Unknown Field Error was expected, but not raised")
		t.FailNow()
	}
}

//TestDealerParticipant_Invoke_addNewDealer
func TestDealerParticipant_Invoke_addNewDealerDuplicate(t *testing.T) {
	dealer := new(DealerParticipant)
	stub := shim.NewMockStub("dealer", dealer)
	checkInit(t, stub, [][]byte{[]byte("init")})
	checkInvoke(t, stub, getFirstDealerParticipantForTesting())
	newDealerID := "100001"
	checkState(t, stub, newDealerID, getNewDealerExpected())
	checkState(t, stub, "dealerIDIndex", getExpectedDealerIDIndex("addNewDealer"))
	res := stub.MockInvoke("1", getFirstDealerParticipantForTesting())
	if res.Status != shim.OK {
		checkError(t, "This Dealer already exists: 100001", res.Message)
	} else {
		fmt.Println("Error was expected, but not raised")
		t.FailNow()
	}
}

//TestDealerParticipant_Invoke_removeDealerOK  //change template
func TestDealerParticipant_Invoke_removeDealerOK(t *testing.T) {
	dealer := new(DealerParticipant)
	stub := shim.NewMockStub("dealer", dealer)
	checkInit(t, stub, [][]byte{[]byte("init")})
	checkInvoke(t, stub, getFirstDealerParticipantForTesting())
	checkInvoke(t, stub, getSecondDealerParticipantForTesting())
	checkReadAllDealersOK(t, stub)
	checkState(t, stub, "dealerIDIndex", getExpectedDealerIDIndex("beforeRemoveDealer"))
	checkInvoke(t, stub, getRemoveSecondDealerParticipantForTesting())
	remainingDealerID := "100001"
	checkReadDealerOK(t, stub, remainingDealerID)
	checkState(t, stub, "dealerIDIndex", getExpectedDealerIDIndex("afterRemoveDealer"))
}

//TestDealerParticipant_Invoke_removeDealerNOK  //change template
func TestDealerParticipant_Invoke_removeDealerNOK(t *testing.T) {
	dealer := new(DealerParticipant)
	stub := shim.NewMockStub("dealer", dealer)
	checkInit(t, stub, [][]byte{[]byte("init")})
	checkInvoke(t, stub, getFirstDealerParticipantForTesting())
	firstDealerID := "100001"
	checkReadDealerOK(t, stub, firstDealerID)
	checkState(t, stub, "dealerIDIndex", getExpectedDealerIDIndex("addNewDealer"))
	res := stub.MockInvoke("1", getRemoveSecondDealerParticipantForTesting())
	if res.Status != shim.OK {
		checkError(t, "Dealer with ID: "+"100002"+" not found", res.Message)
	} else {
		fmt.Println("Error was expected, but not raised")
		t.FailNow()
	}
	checkState(t, stub, "dealerIDIndex", getExpectedDealerIDIndex("addNewDealer"))
}

//TestDealerParticipant_Invoke_removeAllDealersOK  //change template
func TestDealerParticipant_Invoke_removeAllDealersOK(t *testing.T) {
	dealer := new(DealerParticipant)
	stub := shim.NewMockStub("dealer", dealer)
	checkInit(t, stub, [][]byte{[]byte("init")})
	checkInvoke(t, stub, getFirstDealerParticipantForTesting())
	checkInvoke(t, stub, getSecondDealerParticipantForTesting())
	checkReadAllDealersOK(t, stub)
	checkState(t, stub, "dealerIDIndex", getExpectedDealerIDIndex("beforeRemoveDealer"))
	checkInvoke(t, stub, getRemoveAllDealerParticipantsForTesting())
	checkState(t, stub, "dealerIDIndex", getExpectedDealerIDIndex(""))
}

//TestDealerParticipant_Invoke_removeDealerNOK  //change template
func TestDealerParticipant_Invoke_removeAllDealersNOK(t *testing.T) {
	dealer := new(DealerParticipant)
	stub := shim.NewMockStub("dealer", dealer)
	checkInit(t, stub, [][]byte{[]byte("init")})
	res := stub.MockInvoke("1", getRemoveAllDealerParticipantsForTesting())
	if res.Status != shim.OK {
		checkError(t, "removeAllDealers: No dealers to remove", res.Message)
	} else {
		fmt.Println("Error was expected, but not raised")
		t.FailNow()
	}
	checkState(t, stub, "dealerIDIndex", getExpectedDealerIDIndex(""))
}

//TestDealerParticipant_Query_readDealer
func TestDealerParticipant_Query_readDealer(t *testing.T) {
	dealer := new(DealerParticipant)
	stub := shim.NewMockStub("dealer", dealer)
	checkInit(t, stub, [][]byte{[]byte("init")})
	dealerID := "100001"
	checkInvoke(t, stub, getFirstDealerParticipantForTesting())
	checkReadDealerOK(t, stub, dealerID)
	checkReadDealerNOK(t, stub, "")
}

//TestDealerParticipant_Query_readAllDealers
func TestDealerParticipant_Query_readAllDealers(t *testing.T) {
	dealer := new(DealerParticipant)
	stub := shim.NewMockStub("dealer", dealer)
	checkInit(t, stub, [][]byte{[]byte("init")})
	checkInvoke(t, stub, getFirstDealerParticipantForTesting())
	checkInvoke(t, stub, getSecondDealerParticipantForTesting())
	checkReadAllDealersOK(t, stub)
}

/*
*
*	Helper Functions
*
 */
//Get first DealerParticipant for testing
func getFirstDealerParticipantForTesting() [][]byte {
	return [][]byte{[]byte("addNewDealer"),
		[]byte("{\"ID\":\"100001\",\"docType\":\"Participant.DealerParticipant\",\"alias\":\"BRITECH\",\"description\":\"British Technology Pvt. Ltd.\"}")}
}

//Get second DealerParticipant for testing
func getSecondDealerParticipantForTesting() [][]byte {
	return [][]byte{[]byte("addNewDealer"),
		[]byte("{\"ID\":\"100002\",\"docType\":\"Participant.DealerParticipant\",\"alias\":\"DEMAGDELAG\",\"description\":\"Demag Delewal AG\"}")}
}

//Get DealerParticipant with unknown field for testing
func getDealerParticipantWithUnknownFieldForTesting() [][]byte {
	return [][]byte{[]byte("addNewDealer"),
		[]byte("{\"ID\":\"100001\",\"docuType\":\"Participant.DealerParticipant\",\"alias\":\"BRITECH\",\"description\":\"British Technology Pvt. Ltd.\"}")}
}

//Get remove second DealerParticipant for testing //change template
func getRemoveSecondDealerParticipantForTesting() [][]byte {
	return [][]byte{[]byte("removeDealer"),
		[]byte("100002")}
}

//Get remove all DealerParticipants for testing //change template
func getRemoveAllDealerParticipantsForTesting() [][]byte {
	return [][]byte{[]byte("removeAllDealers")}
}

//Get an expected value for testing
func getNewDealerExpected() []byte {
	var dealer Dealer
	dealer.ID = "100001"
	dealer.ObjectType = "Participant.DealerParticipant"
	dealer.Alias = "BRITECH"
	dealer.Description = "British Technology Pvt. Ltd."
	dealerJSON, err := json.Marshal(dealer)
	if err != nil {
		fmt.Println("Error converting a Dealer record to JSON")
		return nil
	}
	return []byte(dealerJSON)
}

//Get expected values of Dealers for testing
func getExpectedDealers() []byte {
	var dealers []Dealer
	var dealer Dealer
	dealer.ID = "100001"
	dealer.ObjectType = "Participant.DealerParticipant"
	dealer.Alias = "BRITECH"
	dealer.Description = "British Technology Pvt. Ltd."
	dealers = append(dealers, dealer)
	dealer.ID = "100002"
	dealer.ObjectType = "Participant.DealerParticipant"
	dealer.Alias = "DEMAGDELAG"
	dealer.Description = "Demag Delewal AG"
	dealers = append(dealers, dealer)
	dealerJSON, err := json.Marshal(dealers)
	if err != nil {
		fmt.Println("Error converting dealerancer records to JSON")
		return nil
	}
	return []byte(dealerJSON)
}

func getExpectedDealerIDIndex(funcName string) []byte {
	var dealerIDIndex DealerIDIndex
	switch funcName {
	case "addNewDealer":
		dealerIDIndex.IDs = append(dealerIDIndex.IDs, "100001")
		dealerIDIndexBytes, err := json.Marshal(dealerIDIndex)
		if err != nil {
			fmt.Println("Error converting DealerIDIndex to JSON")
			return nil
		}
		return dealerIDIndexBytes
	case "beforeRemoveDealer":
		dealerIDIndex.IDs = append(dealerIDIndex.IDs, "100001")
		dealerIDIndex.IDs = append(dealerIDIndex.IDs, "100002")
		dealerIDIndexBytes, err := json.Marshal(dealerIDIndex)
		if err != nil {
			fmt.Println("Error converting DealerIDIndex to JSON")
			return nil
		}
		return dealerIDIndexBytes
	case "afterRemoveDealer":
		dealerIDIndex.IDs = append(dealerIDIndex.IDs, "100001")
		dealerIDIndexBytes, err := json.Marshal(dealerIDIndex)
		if err != nil {
			fmt.Println("Error converting DealerIDIndex to JSON")
			return nil
		}
		return dealerIDIndexBytes
	default:
		dealerIDIndexBytes, err := json.Marshal(dealerIDIndex)
		if err != nil {
			fmt.Println("Error converting DealerIDIndex to JSON")
			return nil
		}
		return dealerIDIndexBytes
	}
}

//checkInit - helper to check the Initialization of chaincode: DealerParticipant
func checkInit(t *testing.T, stub *shim.MockStub, args [][]byte) {
	res := stub.MockInit("1", args)
	if res.Status != shim.OK {
		fmt.Println("Init failed", string(res.Message))
		t.FailNow()
	}
}

//checkState - helper for checking the chaincode state for a given stateKey afgainst an expected value
func checkState(t *testing.T, stub *shim.MockStub, stateKey string, expectedState []byte) {
	actualState := stub.State[stateKey]
	if actualState == nil {
		fmt.Println("State for ", stateKey, ": failed to get value")
		t.FailNow()
	}

	if bytes.Compare(actualState, expectedState) != 0 {
		fmt.Println("Incorrect State for " + stateKey + ": \nExpected: " + string(expectedState) + "\nActual  : " + string(actualState))
		t.FailNow()
	}
}

//checkInvoke - helper for checking Invoke of chaincode
func checkInvoke(t *testing.T, stub *shim.MockStub, args [][]byte) {
	res := stub.MockInvoke("1", args)
	if res.Status != shim.OK {
		fmt.Println("Invoke", args, "failed", string(res.Message))
		t.FailNow()
	}
}

func checkInvokeUnknownFunction(t *testing.T, stub *shim.MockStub, args [][]byte) {
	res := stub.MockInvoke("1", args)
	if res.Status != shim.OK {
		expectedErr := "Received unknown function invocation"
		actualErr := string(res.Message)
		if !(strings.Contains(actualErr, expectedErr)) {
			fmt.Println("Invoke with unknown function must throw error: ", expectedErr, "\n Actual Error :", actualErr)
			t.FailNow()
		}
	} else {
		t.FailNow()
	}
}

//checkReadDealerOK - helper for positive test readDealer
func checkReadDealerOK(t *testing.T, stub *shim.MockStub, dealerancerID string) {
	res := stub.MockInvoke("1", [][]byte{[]byte("readDealer"), []byte(dealerancerID)})
	if res.Status != shim.OK {
		fmt.Println("func readDealer with ID: ", dealerancerID, " failed"+string(res.Message))
		t.FailNow()
	}
	if res.Payload == nil {
		fmt.Println("func readDealer with ID: ", dealerancerID, "failed to get value")
		t.FailNow()
	}
	if bytes.Compare(getNewDealerExpected(), []byte(res.Payload)) != 0 {
		fmt.Println("func readDealer with ID: ", dealerancerID, "Expected:", string(getNewDealerExpected()), "Actual:", string(res.Payload))
		t.FailNow()
	}
}

//checkReadDealerNOK - helper for negative testing of readDealer
func checkReadDealerNOK(t *testing.T, stub *shim.MockStub, dealerancerID string) {
	//with no dealerancerID
	res := stub.MockInvoke("1", [][]byte{[]byte("readDealer"), []byte("")})
	if res.Status != shim.OK {
		expectedErr := "retrieveDealer: Corrupt dealer record "
		actualErr := string(res.Message)
		if !(strings.Contains(actualErr, expectedErr)) {
			fmt.Println("func readDealer neagtive test: ", "Expected Error:", expectedErr, "Actual Error", actualErr)
			t.FailNow()
		}
	} else {
		t.FailNow()
	}
}
func checkReadAllDealersOK(t *testing.T, stub *shim.MockStub) {
	res := stub.MockInvoke("1", [][]byte{[]byte("readAllDealers")})
	if res.Status != shim.OK {
		fmt.Println("func readAllDealers failed", string(res.Message))
		t.FailNow()
	}
	if res.Payload == nil {
		fmt.Println("func readAllDealers failed to get value")
		t.FailNow()
	}
	if bytes.Compare(getExpectedDealers(), []byte(res.Payload)) != 0 {
		fmt.Println("func readAllDealers Expected:\n", string(getExpectedDealers()), "\nActual:\n", string(res.Payload))
		t.FailNow()
	}
}

func checkError(t *testing.T, exp string, act string) {
	if strings.Compare(exp, act) != 0 {
		fmt.Println("Unexpected Error! Expecting ", exp, "\n Actual :", act)
		t.FailNow()
	}
}
