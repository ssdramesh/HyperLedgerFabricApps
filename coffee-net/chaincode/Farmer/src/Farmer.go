package main

import (
	"encoding/json"
	"errors"
	"fmt"
	"strings"

	"github.com/hyperledger/fabric/core/chaincode/shim"
	"github.com/hyperledger/fabric/protos/peer"
)

var logger = shim.NewLogger("CLDChaincode")

//FarmerParticipant - Chaincode for Farmer Participant
type FarmerParticipant struct {
}

//Farmer - Details of the participant type Farmer
type Farmer struct {
	ID          string `json:"ID"`
	ObjectType  string `json:"docType"`
	Alias       string `json:"alias"`
	Description string `json:"description"`
}

//FarmerIDIndex - Index on IDs for retrieval all Farmers
type FarmerIDIndex struct {
	IDs []string `json:"IDs"`
}

func main() {
	err := shim.Start(new(FarmerParticipant))
	if err != nil {
		fmt.Printf("Error starting FarmerParticipant chaincode function main(): %s", err)
	} else {
		fmt.Printf("Starting FarmerParticipant chaincode function main() executed successfully")
	}
}

//Init - The chaincode Init function: No  arguments, only initializes a ID array as Index for retrieval of all Farmers
func (fmr *FarmerParticipant) Init(stub shim.ChaincodeStubInterface) peer.Response {
	var farmerIDIndex FarmerIDIndex
	record, _ := stub.GetState("farmerIDIndex")
	//chaincode upgrade: Index might already exist, create new only if it does not
	if record == nil {
		bytes, _ := json.Marshal(farmerIDIndex)
		stub.PutState("farmerIDIndex", bytes)
	}
	return shim.Success(nil)
}

//Invoke - The chaincode Invoke function:
func (fmr *FarmerParticipant) Invoke(stub shim.ChaincodeStubInterface) peer.Response {
	function, args := stub.GetFunctionAndParameters()
	logger.Debug("function: ", function)
	switch function {
	case "addNewFarmer":
		return fmr.addNewFarmer(stub, args)
	case "removeFarmer":
		return fmr.removeFarmer(stub, args[0])
	case "removeAllFarmers":
		return fmr.removeAllFarmers(stub)
	case "readFarmer":
		return fmr.readFarmer(stub, args[0])
	case "readAllFarmers":
		return fmr.readAllFarmers(stub)
	default:
		return shim.Error("Received unknown function invocation")
	}
}

//Invoke Route: addNewFarmer
func (fmr *FarmerParticipant) addNewFarmer(stub shim.ChaincodeStubInterface, args []string) peer.Response {
	farmer, err := getFarmerFromArgs(args)
	if err != nil {
		return shim.Error("Farmer Data is Corrupted")
	}
	farmer.ObjectType = "Participant.FarmerParticipant"
	record, err := stub.GetState(farmer.ID)
	if record != nil {
		return shim.Error("This Farmer already exists: " + farmer.ID)
	}
	_, err = fmr.saveFarmer(stub, farmer)
	if err != nil {
		return shim.Error(err.Error())
	}
	_, err = fmr.updateFarmerIDIndex(stub, farmer)
	if err != nil {
		return shim.Error(err.Error())
	}
	return shim.Success(nil)
}

//Invoke Route: removeFarmer
func (fmr *FarmerParticipant) removeFarmer(stub shim.ChaincodeStubInterface, farmerStructParticipantID string) peer.Response {
	_, err := fmr.deleteFarmer(stub, farmerStructParticipantID)
	if err != nil {
		return shim.Error(err.Error())
	}
	_, err = fmr.deleteFarmerIDIndex(stub, farmerStructParticipantID)
	if err != nil {
		return shim.Error(err.Error())
	}
	return shim.Success(nil)
}

//Invoke Route: removeAllFarmers
func (fmr *FarmerParticipant) removeAllFarmers(stub shim.ChaincodeStubInterface) peer.Response {
	var farmerStructIDs FarmerIDIndex
	bytes, err := stub.GetState("farmerIDIndex")
	if err != nil {
		return shim.Error("removeAllFarmers: Error getting farmerIDIndex array")
	}
	err = json.Unmarshal(bytes, &farmerStructIDs)
	if err != nil {
		return shim.Error("removeAllFarmers: Error unmarshalling farmerIDIndex array JSON")
	}
	if len(farmerStructIDs.IDs) == 0 {
		return shim.Error("removeAllFarmers: No farmers to remove")
	}
	for _, farmerStructParticipantID := range farmerStructIDs.IDs {
		_, err = fmr.deleteFarmer(stub, farmerStructParticipantID)
		if err != nil {
			return shim.Error("Failed to remove Farmer with ID: " + farmerStructParticipantID)
		}
		_, err = fmr.deleteFarmerIDIndex(stub, farmerStructParticipantID)
		if err != nil {
			return shim.Error(err.Error())
		}
	}
	fmr.initHolder(stub)
	return shim.Success(nil)
}

//Query Route: readFarmer
func (fmr *FarmerParticipant) readFarmer(stub shim.ChaincodeStubInterface, farmerParticipantID string) peer.Response {
	farmerAsByteArray, err := fmr.retrieveFarmer(stub, farmerParticipantID)
	if err != nil {
		return shim.Error(err.Error())
	}
	return shim.Success(farmerAsByteArray)
}

//Query Route: readAllFarmers
func (fmr *FarmerParticipant) readAllFarmers(stub shim.ChaincodeStubInterface) peer.Response {
	var farmerIDs FarmerIDIndex
	bytes, err := stub.GetState("farmerIDIndex")
	if err != nil {
		return shim.Error("readAllFarmers: Error getting farmerIDIndex array")
	}
	err = json.Unmarshal(bytes, &farmerIDs)
	if err != nil {
		return shim.Error("readAllFarmers: Error unmarshalling farmerIDIndex array JSON")
	}
	result := "["

	var farmerAsByteArray []byte

	for _, farmerID := range farmerIDs.IDs {
		farmerAsByteArray, err = fmr.retrieveFarmer(stub, farmerID)
		if err != nil {
			return shim.Error("Failed to retrieve farmer with ID: " + farmerID)
		}
		result += string(farmerAsByteArray) + ","
	}
	if len(result) == 1 {
		result = "[]"
	} else {
		result = result[:len(result)-1] + "]"
	}
	return shim.Success([]byte(result))
}

//Helper: Save farmerParticipant
func (fmr *FarmerParticipant) saveFarmer(stub shim.ChaincodeStubInterface, farmer Farmer) (bool, error) {
	bytes, err := json.Marshal(farmer)
	if err != nil {
		return false, errors.New("Error converting farmer record JSON")
	}
	err = stub.PutState(farmer.ID, bytes)
	if err != nil {
		return false, errors.New("Error storing Farmer record")
	}
	return true, nil
}

//Helper: Delete farmerStructParticipant
func (fmr *FarmerParticipant) deleteFarmer(stub shim.ChaincodeStubInterface, farmerStructParticipantID string) (bool, error) {
	_, err := fmr.retrieveFarmer(stub, farmerStructParticipantID)
	if err != nil {
		return false, errors.New("Farmer with ID: " + farmerStructParticipantID + " not found")
	}
	err = stub.DelState(farmerStructParticipantID)
	if err != nil {
		return false, errors.New("Error deleting Farmer record")
	}
	return true, nil
}

//Helper: Update farmer Holder - updates Index
func (fmr *FarmerParticipant) updateFarmerIDIndex(stub shim.ChaincodeStubInterface, farmer Farmer) (bool, error) {
	var farmerIDs FarmerIDIndex
	bytes, err := stub.GetState("farmerIDIndex")
	if err != nil {
		return false, errors.New("upadeteFarmerIDIndex: Error getting farmerIDIndex array Index from state")
	}
	err = json.Unmarshal(bytes, &farmerIDs)
	if err != nil {
		return false, errors.New("upadeteFarmerIDIndex: Error unmarshalling farmerIDIndex array JSON")
	}
	farmerIDs.IDs = append(farmerIDs.IDs, farmer.ID)
	bytes, err = json.Marshal(farmerIDs)
	if err != nil {
		return false, errors.New("updateFarmerIDIndex: Error marshalling new farmer ID")
	}
	err = stub.PutState("farmerIDIndex", bytes)
	if err != nil {
		return false, errors.New("updateFarmerIDIndex: Error storing new farmer ID in farmerIDIndex (Index)")
	}
	return true, nil
}

//Helper: delete ID from farmerStruct Holder
func (fmr *FarmerParticipant) deleteFarmerIDIndex(stub shim.ChaincodeStubInterface, farmerStructParticipantID string) (bool, error) {
	var farmerStructIDs FarmerIDIndex
	bytes, err := stub.GetState("farmerIDIndex")
	if err != nil {
		return false, errors.New("deleteFarmerIDIndex: Error getting farmerIDIndex array Index from state")
	}
	err = json.Unmarshal(bytes, &farmerStructIDs)
	if err != nil {
		return false, errors.New("deleteFarmerIDIndex: Error unmarshalling farmerIDIndex array JSON")
	}
	farmerStructIDs.IDs, err = deleteKeyFromStringArray(farmerStructIDs.IDs, farmerStructParticipantID)
	if err != nil {
		return false, errors.New(err.Error())
	}
	bytes, err = json.Marshal(farmerStructIDs)
	if err != nil {
		return false, errors.New("deleteFarmerIDIndex: Error marshalling new farmerStruct ID")
	}
	err = stub.PutState("farmerIDIndex", bytes)
	if err != nil {
		return false, errors.New("deleteFarmerIDIndex: Error storing new farmerStruct ID in farmerIDIndex (Index)")
	}
	return true, nil
}

//Helper: Initialize farmer ID Holder
func (fmr *FarmerParticipant) initHolder(stub shim.ChaincodeStubInterface) bool {
	var farmerIDIndex FarmerIDIndex
	bytes, _ := json.Marshal(farmerIDIndex)
	stub.DelState("farmerIDIndex")
	stub.PutState("farmerIDIndex", bytes)
	return true
}

//Helper: Retrieve farmerParticipant
func (fmr *FarmerParticipant) retrieveFarmer(stub shim.ChaincodeStubInterface, farmerParticipantID string) ([]byte, error) {
	var farmer Farmer
	var farmerAsByteArray []byte
	bytes, err := stub.GetState(farmerParticipantID)
	if err != nil {
		return farmerAsByteArray, errors.New("retrieveFarmer: Error retrieving farmer with ID: " + farmerParticipantID)
	}
	err = json.Unmarshal(bytes, &farmer)
	if err != nil {
		return farmerAsByteArray, errors.New("retrieveFarmer: Corrupt farmer record " + string(bytes))
	}
	farmerAsByteArray, err = json.Marshal(farmer)
	if err != nil {
		return farmerAsByteArray, errors.New("readFarmer: Invalid farmer Object - Not a  valid JSON")
	}
	return farmerAsByteArray, nil
}

//deleteKeyFromArray
func deleteKeyFromStringArray(array []string, key string) (newArray []string, err error) {
	for _, entry := range array {
		if entry != key {
			newArray = append(newArray, entry)
		}
	}
	if len(newArray) == len(array) {
		return newArray, errors.New("Specified Key: " + key + " not found in Array")
	}
	return newArray, nil
}

//getFarmerFromArgs - construct a farmer structure from string array of arguments
func getFarmerFromArgs(args []string) (farmer Farmer, err error) {

	if strings.Contains(args[0], "\"ID\"") == false ||
		strings.Contains(args[0], "\"docType\"") == false ||
		strings.Contains(args[0], "\"alias\"") == false ||
		strings.Contains(args[0], "\"description\"") == false {
		return farmer, errors.New("Unknown field: Input JSON does not comply to schema")
	}

	err = json.Unmarshal([]byte(args[0]), &farmer)
	if err != nil {
		return farmer, err
	}
	return farmer, nil
}
