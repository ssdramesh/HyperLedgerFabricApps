package main

import (
	"bytes"
	"encoding/json"
	"fmt"
	"strings"
	"testing"

	"github.com/hyperledger/fabric/core/chaincode/shim"
)

//TestFarmerParticipant_Init
func TestFarmerParticipant_Init(t *testing.T) {
	farmer := new(FarmerParticipant)
	stub := shim.NewMockStub("farmer", farmer)
	checkInit(t, stub, [][]byte{[]byte("init")})
	checkState(t, stub, "farmerIDIndex", getExpectedFarmerIDIndex("init"))
}

//TestFarmerParticipant_InvokeUnknownFunction
func TestFarmerParticipant_InvokeUnknownFunction(t *testing.T) {
	farmer := new(FarmerParticipant)
	stub := shim.NewMockStub("farmer", farmer)
	checkInit(t, stub, [][]byte{[]byte("init")})
	checkInvokeUnknownFunction(t, stub, [][]byte{[]byte("myFunction"), []byte("docType:Participant")})
}

//TestFarmerParticipant_Invoke_addNewFarmer
func TestFarmerParticipant_Invoke_addNewFarmerOK(t *testing.T) {
	farmer := new(FarmerParticipant)
	stub := shim.NewMockStub("farmer", farmer)
	checkInit(t, stub, [][]byte{[]byte("init")})
	checkInvoke(t, stub, getFirstFarmerParticipantForTesting())
	newFarmerID := "100001"
	checkState(t, stub, newFarmerID, getNewFarmerExpected())
	checkState(t, stub, "farmerIDIndex", getExpectedFarmerIDIndex("addNewFarmer"))
}

//TestFarmerParticipant_Invoke_addNewFarmerUnknownField
func TestFarmerParticipant_Invoke_addNewFarmerUnknownField(t *testing.T) {
	farmer := new(FarmerParticipant)
	stub := shim.NewMockStub("farmer", farmer)
	checkInit(t, stub, [][]byte{[]byte("init")})
	res := stub.MockInvoke("1", getFarmerParticipantWithUnknownFieldForTesting())
	if res.Status != shim.OK {
		checkError(t, "Farmer Data is Corrupted", res.Message)
	} else {
		fmt.Println("Unknown Field Error was expected, but not raised")
		t.FailNow()
	}
}

//TestFarmerParticipant_Invoke_addNewFarmer
func TestFarmerParticipant_Invoke_addNewFarmerDuplicate(t *testing.T) {
	farmer := new(FarmerParticipant)
	stub := shim.NewMockStub("farmer", farmer)
	checkInit(t, stub, [][]byte{[]byte("init")})
	checkInvoke(t, stub, getFirstFarmerParticipantForTesting())
	newFarmerID := "100001"
	checkState(t, stub, newFarmerID, getNewFarmerExpected())
	checkState(t, stub, "farmerIDIndex", getExpectedFarmerIDIndex("addNewFarmer"))
	res := stub.MockInvoke("1", getFirstFarmerParticipantForTesting())
	if res.Status != shim.OK {
		checkError(t, "This Farmer already exists: 100001", res.Message)
	} else {
		fmt.Println("Error was expected, but not raised")
		t.FailNow()
	}
}

//TestFarmerParticipant_Invoke_removeFarmerOK  //change template
func TestFarmerParticipant_Invoke_removeFarmerOK(t *testing.T) {
	farmer := new(FarmerParticipant)
	stub := shim.NewMockStub("farmer", farmer)
	checkInit(t, stub, [][]byte{[]byte("init")})
	checkInvoke(t, stub, getFirstFarmerParticipantForTesting())
	checkInvoke(t, stub, getSecondFarmerParticipantForTesting())
	checkReadAllFarmersOK(t, stub)
	checkState(t, stub, "farmerIDIndex", getExpectedFarmerIDIndex("beforeRemoveFarmer"))
	checkInvoke(t, stub, getRemoveSecondFarmerParticipantForTesting())
	remainingFarmerID := "100001"
	checkReadFarmerOK(t, stub, remainingFarmerID)
	checkState(t, stub, "farmerIDIndex", getExpectedFarmerIDIndex("afterRemoveFarmer"))
}

//TestFarmerParticipant_Invoke_removeFarmerNOK  //change template
func TestFarmerParticipant_Invoke_removeFarmerNOK(t *testing.T) {
	farmer := new(FarmerParticipant)
	stub := shim.NewMockStub("farmer", farmer)
	checkInit(t, stub, [][]byte{[]byte("init")})
	checkInvoke(t, stub, getFirstFarmerParticipantForTesting())
	firstFarmerID := "100001"
	checkReadFarmerOK(t, stub, firstFarmerID)
	checkState(t, stub, "farmerIDIndex", getExpectedFarmerIDIndex("addNewFarmer"))
	res := stub.MockInvoke("1", getRemoveSecondFarmerParticipantForTesting())
	if res.Status != shim.OK {
		checkError(t, "Farmer with ID: "+"100002"+" not found", res.Message)
	} else {
		fmt.Println("Error was expected, but not raised")
		t.FailNow()
	}
	checkState(t, stub, "farmerIDIndex", getExpectedFarmerIDIndex("addNewFarmer"))
}

//TestFarmerParticipant_Invoke_removeAllFarmersOK  //change template
func TestFarmerParticipant_Invoke_removeAllFarmersOK(t *testing.T) {
	farmer := new(FarmerParticipant)
	stub := shim.NewMockStub("farmer", farmer)
	checkInit(t, stub, [][]byte{[]byte("init")})
	checkInvoke(t, stub, getFirstFarmerParticipantForTesting())
	checkInvoke(t, stub, getSecondFarmerParticipantForTesting())
	checkReadAllFarmersOK(t, stub)
	checkState(t, stub, "farmerIDIndex", getExpectedFarmerIDIndex("beforeRemoveFarmer"))
	checkInvoke(t, stub, getRemoveAllFarmerParticipantsForTesting())
	checkState(t, stub, "farmerIDIndex", getExpectedFarmerIDIndex(""))
}

//TestFarmerParticipant_Invoke_removeFarmerNOK  //change template
func TestFarmerParticipant_Invoke_removeAllFarmersNOK(t *testing.T) {
	farmer := new(FarmerParticipant)
	stub := shim.NewMockStub("farmer", farmer)
	checkInit(t, stub, [][]byte{[]byte("init")})
	res := stub.MockInvoke("1", getRemoveAllFarmerParticipantsForTesting())
	if res.Status != shim.OK {
		checkError(t, "removeAllFarmers: No farmers to remove", res.Message)
	} else {
		fmt.Println("Error was expected, but not raised")
		t.FailNow()
	}
	checkState(t, stub, "farmerIDIndex", getExpectedFarmerIDIndex(""))
}

//TestFarmerParticipant_Query_readFarmer
func TestFarmerParticipant_Query_readFarmer(t *testing.T) {
	farmer := new(FarmerParticipant)
	stub := shim.NewMockStub("farmer", farmer)
	checkInit(t, stub, [][]byte{[]byte("init")})
	farmerID := "100001"
	checkInvoke(t, stub, getFirstFarmerParticipantForTesting())
	checkReadFarmerOK(t, stub, farmerID)
	checkReadFarmerNOK(t, stub, "")
}

//TestFarmerParticipant_Query_readAllFarmers
func TestFarmerParticipant_Query_readAllFarmers(t *testing.T) {
	farmer := new(FarmerParticipant)
	stub := shim.NewMockStub("farmer", farmer)
	checkInit(t, stub, [][]byte{[]byte("init")})
	checkInvoke(t, stub, getFirstFarmerParticipantForTesting())
	checkInvoke(t, stub, getSecondFarmerParticipantForTesting())
	checkReadAllFarmersOK(t, stub)
}

/*
*
*	Helper Functions
*
 */
//Get first FarmerParticipant for testing
func getFirstFarmerParticipantForTesting() [][]byte {
	return [][]byte{[]byte("addNewFarmer"),
		[]byte("{\"ID\":\"100001\",\"docType\":\"Participant.FarmerParticipant\",\"alias\":\"BRITECH\",\"description\":\"British Technology Pvt. Ltd.\"}")}
}

//Get second FarmerParticipant for testing
func getSecondFarmerParticipantForTesting() [][]byte {
	return [][]byte{[]byte("addNewFarmer"),
		[]byte("{\"ID\":\"100002\",\"docType\":\"Participant.FarmerParticipant\",\"alias\":\"DEMAGDELAG\",\"description\":\"Demag Delewal AG\"}")}
}

//Get FarmerParticipant with unknown field for testing
func getFarmerParticipantWithUnknownFieldForTesting() [][]byte {
	return [][]byte{[]byte("addNewFarmer"),
		[]byte("{\"ID\":\"100001\",\"docuType\":\"Participant.FarmerParticipant\",\"alias\":\"BRITECH\",\"description\":\"British Technology Pvt. Ltd.\"}")}
}

//Get remove second FarmerParticipant for testing //change template
func getRemoveSecondFarmerParticipantForTesting() [][]byte {
	return [][]byte{[]byte("removeFarmer"),
		[]byte("100002")}
}

//Get remove all FarmerParticipants for testing //change template
func getRemoveAllFarmerParticipantsForTesting() [][]byte {
	return [][]byte{[]byte("removeAllFarmers")}
}

//Get an expected value for testing
func getNewFarmerExpected() []byte {
	var farmer Farmer
	farmer.ID = "100001"
	farmer.ObjectType = "Participant.FarmerParticipant"
	farmer.Alias = "BRITECH"
	farmer.Description = "British Technology Pvt. Ltd."
	farmerJSON, err := json.Marshal(farmer)
	if err != nil {
		fmt.Println("Error converting a Farmer record to JSON")
		return nil
	}
	return []byte(farmerJSON)
}

//Get expected values of Farmers for testing
func getExpectedFarmers() []byte {
	var farmers []Farmer
	var farmer Farmer
	farmer.ID = "100001"
	farmer.ObjectType = "Participant.FarmerParticipant"
	farmer.Alias = "BRITECH"
	farmer.Description = "British Technology Pvt. Ltd."
	farmers = append(farmers, farmer)
	farmer.ID = "100002"
	farmer.ObjectType = "Participant.FarmerParticipant"
	farmer.Alias = "DEMAGDELAG"
	farmer.Description = "Demag Delewal AG"
	farmers = append(farmers, farmer)
	farmerJSON, err := json.Marshal(farmers)
	if err != nil {
		fmt.Println("Error converting farmerancer records to JSON")
		return nil
	}
	return []byte(farmerJSON)
}

func getExpectedFarmerIDIndex(funcName string) []byte {
	var farmerIDIndex FarmerIDIndex
	switch funcName {
	case "addNewFarmer":
		farmerIDIndex.IDs = append(farmerIDIndex.IDs, "100001")
		farmerIDIndexBytes, err := json.Marshal(farmerIDIndex)
		if err != nil {
			fmt.Println("Error converting FarmerIDIndex to JSON")
			return nil
		}
		return farmerIDIndexBytes
	case "beforeRemoveFarmer":
		farmerIDIndex.IDs = append(farmerIDIndex.IDs, "100001")
		farmerIDIndex.IDs = append(farmerIDIndex.IDs, "100002")
		farmerIDIndexBytes, err := json.Marshal(farmerIDIndex)
		if err != nil {
			fmt.Println("Error converting FarmerIDIndex to JSON")
			return nil
		}
		return farmerIDIndexBytes
	case "afterRemoveFarmer":
		farmerIDIndex.IDs = append(farmerIDIndex.IDs, "100001")
		farmerIDIndexBytes, err := json.Marshal(farmerIDIndex)
		if err != nil {
			fmt.Println("Error converting FarmerIDIndex to JSON")
			return nil
		}
		return farmerIDIndexBytes
	default:
		farmerIDIndexBytes, err := json.Marshal(farmerIDIndex)
		if err != nil {
			fmt.Println("Error converting FarmerIDIndex to JSON")
			return nil
		}
		return farmerIDIndexBytes
	}
}

//checkInit - helper to check the Initialization of chaincode: FarmerParticipant
func checkInit(t *testing.T, stub *shim.MockStub, args [][]byte) {
	res := stub.MockInit("1", args)
	if res.Status != shim.OK {
		fmt.Println("Init failed", string(res.Message))
		t.FailNow()
	}
}

//checkState - helper for checking the chaincode state for a given stateKey afgainst an expected value
func checkState(t *testing.T, stub *shim.MockStub, stateKey string, expectedState []byte) {
	actualState := stub.State[stateKey]
	if actualState == nil {
		fmt.Println("State for ", stateKey, ": failed to get value")
		t.FailNow()
	}

	if bytes.Compare(actualState, expectedState) != 0 {
		fmt.Println("Incorrect State for " + stateKey + ": \nExpected: " + string(expectedState) + "\nActual  : " + string(actualState))
		t.FailNow()
	}
}

//checkInvoke - helper for checking Invoke of chaincode
func checkInvoke(t *testing.T, stub *shim.MockStub, args [][]byte) {
	res := stub.MockInvoke("1", args)
	if res.Status != shim.OK {
		fmt.Println("Invoke", args, "failed", string(res.Message))
		t.FailNow()
	}
}

func checkInvokeUnknownFunction(t *testing.T, stub *shim.MockStub, args [][]byte) {
	res := stub.MockInvoke("1", args)
	if res.Status != shim.OK {
		expectedErr := "Received unknown function invocation"
		actualErr := string(res.Message)
		if !(strings.Contains(actualErr, expectedErr)) {
			fmt.Println("Invoke with unknown function must throw error: ", expectedErr, "\n Actual Error :", actualErr)
			t.FailNow()
		}
	} else {
		t.FailNow()
	}
}

//checkReadFarmerOK - helper for positive test readFarmer
func checkReadFarmerOK(t *testing.T, stub *shim.MockStub, farmerancerID string) {
	res := stub.MockInvoke("1", [][]byte{[]byte("readFarmer"), []byte(farmerancerID)})
	if res.Status != shim.OK {
		fmt.Println("func readFarmer with ID: ", farmerancerID, " failed"+string(res.Message))
		t.FailNow()
	}
	if res.Payload == nil {
		fmt.Println("func readFarmer with ID: ", farmerancerID, "failed to get value")
		t.FailNow()
	}
	if bytes.Compare(getNewFarmerExpected(), []byte(res.Payload)) != 0 {
		fmt.Println("func readFarmer with ID: ", farmerancerID, "Expected:", string(getNewFarmerExpected()), "Actual:", string(res.Payload))
		t.FailNow()
	}
}

//checkReadFarmerNOK - helper for negative testing of readFarmer
func checkReadFarmerNOK(t *testing.T, stub *shim.MockStub, farmerancerID string) {
	//with no farmerancerID
	res := stub.MockInvoke("1", [][]byte{[]byte("readFarmer"), []byte("")})
	if res.Status != shim.OK {
		expectedErr := "retrieveFarmer: Corrupt farmer record "
		actualErr := string(res.Message)
		if !(strings.Contains(actualErr, expectedErr)) {
			fmt.Println("func readFarmer neagtive test: ", "Expected Error:", expectedErr, "Actual Error", actualErr)
			t.FailNow()
		}
	} else {
		t.FailNow()
	}
}
func checkReadAllFarmersOK(t *testing.T, stub *shim.MockStub) {
	res := stub.MockInvoke("1", [][]byte{[]byte("readAllFarmers")})
	if res.Status != shim.OK {
		fmt.Println("func readAllFarmers failed", string(res.Message))
		t.FailNow()
	}
	if res.Payload == nil {
		fmt.Println("func readAllFarmers failed to get value")
		t.FailNow()
	}
	if bytes.Compare(getExpectedFarmers(), []byte(res.Payload)) != 0 {
		fmt.Println("func readAllFarmers Expected:\n", string(getExpectedFarmers()), "\nActual:\n", string(res.Payload))
		t.FailNow()
	}
}

func checkError(t *testing.T, exp string, act string) {
	if strings.Compare(exp, act) != 0 {
		fmt.Println("Unexpected Error! Expecting ", exp, "\n Actual :", act)
		t.FailNow()
	}
}
