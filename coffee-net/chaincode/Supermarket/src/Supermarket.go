package main

import (
	"encoding/json"
	"errors"
	"fmt"
	"strings"

	"github.com/hyperledger/fabric/core/chaincode/shim"
	"github.com/hyperledger/fabric/protos/peer"
)

var logger = shim.NewLogger("CLDChaincode")

//SupermarketParticipant - Chaincode for Supermarket Participant
type SupermarketParticipant struct {
}

//Supermarket - Details of the participant type Supermarket
type Supermarket struct {
	ID          string `json:"ID"`
	ObjectType  string `json:"docType"`
	Alias       string `json:"alias"`
	Description string `json:"description"`
}

//SupermarketIDIndex - Index on IDs for retrieval all Supermarkets
type SupermarketIDIndex struct {
	IDs []string `json:"IDs"`
}

func main() {
	err := shim.Start(new(SupermarketParticipant))
	if err != nil {
		fmt.Printf("Error starting SupermarketParticipant chaincode function main(): %s", err)
	} else {
		fmt.Printf("Starting SupermarketParticipant chaincode function main() executed successfully")
	}
}

//Init - The chaincode Init function: No  arguments, only initializes a ID array as Index for retrieval of all Supermarkets
func (sm *SupermarketParticipant) Init(stub shim.ChaincodeStubInterface) peer.Response {
	var supermarketIDIndex SupermarketIDIndex
	record, _ := stub.GetState("supermarketIDIndex")
	//chaincode upgrade: Index might already exist, create new only if it does not
	if record == nil {
		bytes, _ := json.Marshal(supermarketIDIndex)
		stub.PutState("supermarketIDIndex", bytes)
	}
	return shim.Success(nil)
}

//Invoke - The chaincode Invoke function:
func (sm *SupermarketParticipant) Invoke(stub shim.ChaincodeStubInterface) peer.Response {
	function, args := stub.GetFunctionAndParameters()
	logger.Debug("function: ", function)
	switch function {
	case "addNewSupermarket":
		return sm.addNewSupermarket(stub, args)
	case "removeSupermarket":
		return sm.removeSupermarket(stub, args[0])
	case "removeAllSupermarkets":
		return sm.removeAllSupermarkets(stub)
	case "readSupermarket":
		return sm.readSupermarket(stub, args[0])
	case "readAllSupermarkets":
		return sm.readAllSupermarkets(stub)
	default:
		return shim.Error("Received unknown function invocation")
	}
}

//Invoke Route: addNewSupermarket
func (sm *SupermarketParticipant) addNewSupermarket(stub shim.ChaincodeStubInterface, args []string) peer.Response {
	supermarket, err := getSupermarketFromArgs(args)
	if err != nil {
		return shim.Error("Supermarket Data is Corrupted")
	}
	supermarket.ObjectType = "Participant.SupermarketParticipant"
	record, err := stub.GetState(supermarket.ID)
	if record != nil {
		return shim.Error("This Supermarket already exists: " + supermarket.ID)
	}
	_, err = sm.saveSupermarket(stub, supermarket)
	if err != nil {
		return shim.Error(err.Error())
	}
	_, err = sm.updateSupermarketIDIndex(stub, supermarket)
	if err != nil {
		return shim.Error(err.Error())
	}
	return shim.Success(nil)
}

//Invoke Route: removeSupermarket
func (sm *SupermarketParticipant) removeSupermarket(stub shim.ChaincodeStubInterface, supermarketStructParticipantID string) peer.Response {
	_, err := sm.deleteSupermarket(stub, supermarketStructParticipantID)
	if err != nil {
		return shim.Error(err.Error())
	}
	_, err = sm.deleteSupermarketIDIndex(stub, supermarketStructParticipantID)
	if err != nil {
		return shim.Error(err.Error())
	}
	return shim.Success(nil)
}

//Invoke Route: removeAllSupermarkets
func (sm *SupermarketParticipant) removeAllSupermarkets(stub shim.ChaincodeStubInterface) peer.Response {
	var supermarketStructIDs SupermarketIDIndex
	bytes, err := stub.GetState("supermarketIDIndex")
	if err != nil {
		return shim.Error("removeAllSupermarkets: Error getting supermarketIDIndex array")
	}
	err = json.Unmarshal(bytes, &supermarketStructIDs)
	if err != nil {
		return shim.Error("removeAllSupermarkets: Error unmarshalling supermarketIDIndex array JSON")
	}
	if len(supermarketStructIDs.IDs) == 0 {
		return shim.Error("removeAllSupermarkets: No supermarkets to remove")
	}
	for _, supermarketStructParticipantID := range supermarketStructIDs.IDs {
		_, err = sm.deleteSupermarket(stub, supermarketStructParticipantID)
		if err != nil {
			return shim.Error("Failed to remove Supermarket with ID: " + supermarketStructParticipantID)
		}
		_, err = sm.deleteSupermarketIDIndex(stub, supermarketStructParticipantID)
		if err != nil {
			return shim.Error(err.Error())
		}
	}
	sm.initHolder(stub)
	return shim.Success(nil)
}

//Query Route: readSupermarket
func (sm *SupermarketParticipant) readSupermarket(stub shim.ChaincodeStubInterface, supermarketParticipantID string) peer.Response {
	supermarketAsByteArray, err := sm.retrieveSupermarket(stub, supermarketParticipantID)
	if err != nil {
		return shim.Error(err.Error())
	}
	return shim.Success(supermarketAsByteArray)
}

//Query Route: readAllSupermarkets
func (sm *SupermarketParticipant) readAllSupermarkets(stub shim.ChaincodeStubInterface) peer.Response {
	var supermarketIDs SupermarketIDIndex
	bytes, err := stub.GetState("supermarketIDIndex")
	if err != nil {
		return shim.Error("readAllSupermarkets: Error getting supermarketIDIndex array")
	}
	err = json.Unmarshal(bytes, &supermarketIDs)
	if err != nil {
		return shim.Error("readAllSupermarkets: Error unmarshalling supermarketIDIndex array JSON")
	}
	result := "["

	var supermarketAsByteArray []byte

	for _, supermarketID := range supermarketIDs.IDs {
		supermarketAsByteArray, err = sm.retrieveSupermarket(stub, supermarketID)
		if err != nil {
			return shim.Error("Failed to retrieve supermarket with ID: " + supermarketID)
		}
		result += string(supermarketAsByteArray) + ","
	}
	if len(result) == 1 {
		result = "[]"
	} else {
		result = result[:len(result)-1] + "]"
	}
	return shim.Success([]byte(result))
}

//Helper: Save supermarketParticipant
func (sm *SupermarketParticipant) saveSupermarket(stub shim.ChaincodeStubInterface, supermarket Supermarket) (bool, error) {
	bytes, err := json.Marshal(supermarket)
	if err != nil {
		return false, errors.New("Error converting supermarket record JSON")
	}
	err = stub.PutState(supermarket.ID, bytes)
	if err != nil {
		return false, errors.New("Error storing Supermarket record")
	}
	return true, nil
}

//Helper: Delete supermarketStructParticipant
func (sm *SupermarketParticipant) deleteSupermarket(stub shim.ChaincodeStubInterface, supermarketStructParticipantID string) (bool, error) {
	_, err := sm.retrieveSupermarket(stub, supermarketStructParticipantID)
	if err != nil {
		return false, errors.New("Supermarket with ID: " + supermarketStructParticipantID + " not found")
	}
	err = stub.DelState(supermarketStructParticipantID)
	if err != nil {
		return false, errors.New("Error deleting Supermarket record")
	}
	return true, nil
}

//Helper: Update supermarket Holder - updates Index
func (sm *SupermarketParticipant) updateSupermarketIDIndex(stub shim.ChaincodeStubInterface, supermarket Supermarket) (bool, error) {
	var supermarketIDs SupermarketIDIndex
	bytes, err := stub.GetState("supermarketIDIndex")
	if err != nil {
		return false, errors.New("upadeteSupermarketIDIndex: Error getting supermarketIDIndex array Index from state")
	}
	err = json.Unmarshal(bytes, &supermarketIDs)
	if err != nil {
		return false, errors.New("upadeteSupermarketIDIndex: Error unmarshalling supermarketIDIndex array JSON")
	}
	supermarketIDs.IDs = append(supermarketIDs.IDs, supermarket.ID)
	bytes, err = json.Marshal(supermarketIDs)
	if err != nil {
		return false, errors.New("updateSupermarketIDIndex: Error marshalling new supermarket ID")
	}
	err = stub.PutState("supermarketIDIndex", bytes)
	if err != nil {
		return false, errors.New("updateSupermarketIDIndex: Error storing new supermarket ID in supermarketIDIndex (Index)")
	}
	return true, nil
}

//Helper: delete ID from supermarketStruct Holder
func (sm *SupermarketParticipant) deleteSupermarketIDIndex(stub shim.ChaincodeStubInterface, supermarketStructParticipantID string) (bool, error) {
	var supermarketStructIDs SupermarketIDIndex
	bytes, err := stub.GetState("supermarketIDIndex")
	if err != nil {
		return false, errors.New("deleteSupermarketIDIndex: Error getting supermarketIDIndex array Index from state")
	}
	err = json.Unmarshal(bytes, &supermarketStructIDs)
	if err != nil {
		return false, errors.New("deleteSupermarketIDIndex: Error unmarshalling supermarketIDIndex array JSON")
	}
	supermarketStructIDs.IDs, err = deleteKeyFromStringArray(supermarketStructIDs.IDs, supermarketStructParticipantID)
	if err != nil {
		return false, errors.New(err.Error())
	}
	bytes, err = json.Marshal(supermarketStructIDs)
	if err != nil {
		return false, errors.New("deleteSupermarketIDIndex: Error marshalling new supermarketStruct ID")
	}
	err = stub.PutState("supermarketIDIndex", bytes)
	if err != nil {
		return false, errors.New("deleteSupermarketIDIndex: Error storing new supermarketStruct ID in supermarketIDIndex (Index)")
	}
	return true, nil
}

//Helper: Initialize supermarket ID Holder
func (sm *SupermarketParticipant) initHolder(stub shim.ChaincodeStubInterface) bool {
	var supermarketIDIndex SupermarketIDIndex
	bytes, _ := json.Marshal(supermarketIDIndex)
	stub.DelState("supermarketIDIndex")
	stub.PutState("supermarketIDIndex", bytes)
	return true
}

//Helper: Retrieve supermarketParticipant
func (sm *SupermarketParticipant) retrieveSupermarket(stub shim.ChaincodeStubInterface, supermarketParticipantID string) ([]byte, error) {
	var supermarket Supermarket
	var supermarketAsByteArray []byte
	bytes, err := stub.GetState(supermarketParticipantID)
	if err != nil {
		return supermarketAsByteArray, errors.New("retrieveSupermarket: Error retrieving supermarket with ID: " + supermarketParticipantID)
	}
	err = json.Unmarshal(bytes, &supermarket)
	if err != nil {
		return supermarketAsByteArray, errors.New("retrieveSupermarket: Corrupt supermarket record " + string(bytes))
	}
	supermarketAsByteArray, err = json.Marshal(supermarket)
	if err != nil {
		return supermarketAsByteArray, errors.New("readSupermarket: Invalid supermarket Object - Not a  valid JSON")
	}
	return supermarketAsByteArray, nil
}

//deleteKeyFromArray
func deleteKeyFromStringArray(array []string, key string) (newArray []string, err error) {
	for _, entry := range array {
		if entry != key {
			newArray = append(newArray, entry)
		}
	}
	if len(newArray) == len(array) {
		return newArray, errors.New("Specified Key: " + key + " not found in Array")
	}
	return newArray, nil
}

//getSupermarketFromArgs - construct a supermarket structure from string array of arguments
func getSupermarketFromArgs(args []string) (supermarket Supermarket, err error) {

	if strings.Contains(args[0], "\"ID\"") == false ||
		strings.Contains(args[0], "\"docType\"") == false ||
		strings.Contains(args[0], "\"alias\"") == false ||
		strings.Contains(args[0], "\"description\"") == false {
		return supermarket, errors.New("Unknown field: Input JSON does not comply to schema")
	}

	err = json.Unmarshal([]byte(args[0]), &supermarket)
	if err != nil {
		return supermarket, err
	}
	return supermarket, nil
}
