package main

import (
	"bytes"
	"encoding/json"
	"fmt"
	"strings"
	"testing"

	"github.com/hyperledger/fabric/core/chaincode/shim"
)

//TestSupermarketParticipant_Init
func TestSupermarketParticipant_Init(t *testing.T) {
	supermarket := new(SupermarketParticipant)
	stub := shim.NewMockStub("supermarket", supermarket)
	checkInit(t, stub, [][]byte{[]byte("init")})
	checkState(t, stub, "supermarketIDIndex", getExpectedSupermarketIDIndex("init"))
}

//TestSupermarketParticipant_InvokeUnknownFunction
func TestSupermarketParticipant_InvokeUnknownFunction(t *testing.T) {
	supermarket := new(SupermarketParticipant)
	stub := shim.NewMockStub("supermarket", supermarket)
	checkInit(t, stub, [][]byte{[]byte("init")})
	checkInvokeUnknownFunction(t, stub, [][]byte{[]byte("myFunction"), []byte("docType:Participant")})
}

//TestSupermarketParticipant_Invoke_addNewSupermarket
func TestSupermarketParticipant_Invoke_addNewSupermarketOK(t *testing.T) {
	supermarket := new(SupermarketParticipant)
	stub := shim.NewMockStub("supermarket", supermarket)
	checkInit(t, stub, [][]byte{[]byte("init")})
	checkInvoke(t, stub, getFirstSupermarketParticipantForTesting())
	newSupermarketID := "100001"
	checkState(t, stub, newSupermarketID, getNewSupermarketExpected())
	checkState(t, stub, "supermarketIDIndex", getExpectedSupermarketIDIndex("addNewSupermarket"))
}

//TestSupermarketParticipant_Invoke_addNewSupermarketUnknownField
func TestSupermarketParticipant_Invoke_addNewSupermarketUnknownField(t *testing.T) {
	supermarket := new(SupermarketParticipant)
	stub := shim.NewMockStub("supermarket", supermarket)
	checkInit(t, stub, [][]byte{[]byte("init")})
	res := stub.MockInvoke("1", getSupermarketParticipantWithUnknownFieldForTesting())
	if res.Status != shim.OK {
		checkError(t, "Supermarket Data is Corrupted", res.Message)
	} else {
		fmt.Println("Unknown Field Error was expected, but not raised")
		t.FailNow()
	}
}

//TestSupermarketParticipant_Invoke_addNewSupermarket
func TestSupermarketParticipant_Invoke_addNewSupermarketDuplicate(t *testing.T) {
	supermarket := new(SupermarketParticipant)
	stub := shim.NewMockStub("supermarket", supermarket)
	checkInit(t, stub, [][]byte{[]byte("init")})
	checkInvoke(t, stub, getFirstSupermarketParticipantForTesting())
	newSupermarketID := "100001"
	checkState(t, stub, newSupermarketID, getNewSupermarketExpected())
	checkState(t, stub, "supermarketIDIndex", getExpectedSupermarketIDIndex("addNewSupermarket"))
	res := stub.MockInvoke("1", getFirstSupermarketParticipantForTesting())
	if res.Status != shim.OK {
		checkError(t, "This Supermarket already exists: 100001", res.Message)
	} else {
		fmt.Println("Error was expected, but not raised")
		t.FailNow()
	}
}

//TestSupermarketParticipant_Invoke_removeSupermarketOK  //change template
func TestSupermarketParticipant_Invoke_removeSupermarketOK(t *testing.T) {
	supermarket := new(SupermarketParticipant)
	stub := shim.NewMockStub("supermarket", supermarket)
	checkInit(t, stub, [][]byte{[]byte("init")})
	checkInvoke(t, stub, getFirstSupermarketParticipantForTesting())
	checkInvoke(t, stub, getSecondSupermarketParticipantForTesting())
	checkReadAllSupermarketsOK(t, stub)
	checkState(t, stub, "supermarketIDIndex", getExpectedSupermarketIDIndex("beforeRemoveSupermarket"))
	checkInvoke(t, stub, getRemoveSecondSupermarketParticipantForTesting())
	remainingSupermarketID := "100001"
	checkReadSupermarketOK(t, stub, remainingSupermarketID)
	checkState(t, stub, "supermarketIDIndex", getExpectedSupermarketIDIndex("afterRemoveSupermarket"))
}

//TestSupermarketParticipant_Invoke_removeSupermarketNOK  //change template
func TestSupermarketParticipant_Invoke_removeSupermarketNOK(t *testing.T) {
	supermarket := new(SupermarketParticipant)
	stub := shim.NewMockStub("supermarket", supermarket)
	checkInit(t, stub, [][]byte{[]byte("init")})
	checkInvoke(t, stub, getFirstSupermarketParticipantForTesting())
	firstSupermarketID := "100001"
	checkReadSupermarketOK(t, stub, firstSupermarketID)
	checkState(t, stub, "supermarketIDIndex", getExpectedSupermarketIDIndex("addNewSupermarket"))
	res := stub.MockInvoke("1", getRemoveSecondSupermarketParticipantForTesting())
	if res.Status != shim.OK {
		checkError(t, "Supermarket with ID: "+"100002"+" not found", res.Message)
	} else {
		fmt.Println("Error was expected, but not raised")
		t.FailNow()
	}
	checkState(t, stub, "supermarketIDIndex", getExpectedSupermarketIDIndex("addNewSupermarket"))
}

//TestSupermarketParticipant_Invoke_removeAllSupermarketsOK  //change template
func TestSupermarketParticipant_Invoke_removeAllSupermarketsOK(t *testing.T) {
	supermarket := new(SupermarketParticipant)
	stub := shim.NewMockStub("supermarket", supermarket)
	checkInit(t, stub, [][]byte{[]byte("init")})
	checkInvoke(t, stub, getFirstSupermarketParticipantForTesting())
	checkInvoke(t, stub, getSecondSupermarketParticipantForTesting())
	checkReadAllSupermarketsOK(t, stub)
	checkState(t, stub, "supermarketIDIndex", getExpectedSupermarketIDIndex("beforeRemoveSupermarket"))
	checkInvoke(t, stub, getRemoveAllSupermarketParticipantsForTesting())
	checkState(t, stub, "supermarketIDIndex", getExpectedSupermarketIDIndex(""))
}

//TestSupermarketParticipant_Invoke_removeSupermarketNOK  //change template
func TestSupermarketParticipant_Invoke_removeAllSupermarketsNOK(t *testing.T) {
	supermarket := new(SupermarketParticipant)
	stub := shim.NewMockStub("supermarket", supermarket)
	checkInit(t, stub, [][]byte{[]byte("init")})
	res := stub.MockInvoke("1", getRemoveAllSupermarketParticipantsForTesting())
	if res.Status != shim.OK {
		checkError(t, "removeAllSupermarkets: No supermarkets to remove", res.Message)
	} else {
		fmt.Println("Error was expected, but not raised")
		t.FailNow()
	}
	checkState(t, stub, "supermarketIDIndex", getExpectedSupermarketIDIndex(""))
}

//TestSupermarketParticipant_Query_readSupermarket
func TestSupermarketParticipant_Query_readSupermarket(t *testing.T) {
	supermarket := new(SupermarketParticipant)
	stub := shim.NewMockStub("supermarket", supermarket)
	checkInit(t, stub, [][]byte{[]byte("init")})
	supermarketID := "100001"
	checkInvoke(t, stub, getFirstSupermarketParticipantForTesting())
	checkReadSupermarketOK(t, stub, supermarketID)
	checkReadSupermarketNOK(t, stub, "")
}

//TestSupermarketParticipant_Query_readAllSupermarkets
func TestSupermarketParticipant_Query_readAllSupermarkets(t *testing.T) {
	supermarket := new(SupermarketParticipant)
	stub := shim.NewMockStub("supermarket", supermarket)
	checkInit(t, stub, [][]byte{[]byte("init")})
	checkInvoke(t, stub, getFirstSupermarketParticipantForTesting())
	checkInvoke(t, stub, getSecondSupermarketParticipantForTesting())
	checkReadAllSupermarketsOK(t, stub)
}

/*
*
*	Helper Functions
*
 */
//Get first SupermarketParticipant for testing
func getFirstSupermarketParticipantForTesting() [][]byte {
	return [][]byte{[]byte("addNewSupermarket"),
		[]byte("{\"ID\":\"100001\",\"docType\":\"Participant.SupermarketParticipant\",\"alias\":\"BRITECH\",\"description\":\"British Technology Pvt. Ltd.\"}")}
}

//Get second SupermarketParticipant for testing
func getSecondSupermarketParticipantForTesting() [][]byte {
	return [][]byte{[]byte("addNewSupermarket"),
		[]byte("{\"ID\":\"100002\",\"docType\":\"Participant.SupermarketParticipant\",\"alias\":\"DEMAGDELAG\",\"description\":\"Demag Delewal AG\"}")}
}

//Get SupermarketParticipant with unknown field for testing
func getSupermarketParticipantWithUnknownFieldForTesting() [][]byte {
	return [][]byte{[]byte("addNewSupermarket"),
		[]byte("{\"ID\":\"100001\",\"docuType\":\"Participant.SupermarketParticipant\",\"alias\":\"BRITECH\",\"description\":\"British Technology Pvt. Ltd.\"}")}
}

//Get remove second SupermarketParticipant for testing //change template
func getRemoveSecondSupermarketParticipantForTesting() [][]byte {
	return [][]byte{[]byte("removeSupermarket"),
		[]byte("100002")}
}

//Get update SupermarketParticipant for OK testing
func getUpdateSupermarketParticipantForOKTesting() [][]byte {
	return [][]byte{[]byte("updateSupermarket"),
		[]byte("{\"ID\":\"100001\",\"docType\":\"Participant.SupermarketParticipant\",\"alias\":\"BRITECH (New)\",\"description\":\"British Technology Pvt. Ltd.\"}")}
}

//Get remove all SupermarketParticipants for testing //change template
func getRemoveAllSupermarketParticipantsForTesting() [][]byte {
	return [][]byte{[]byte("removeAllSupermarkets")}
}

//Get an expected value for testing
func getNewSupermarketExpected() []byte {
	var supermarket Supermarket
	supermarket.ID = "100001"
	supermarket.ObjectType = "Participant.SupermarketParticipant"
	supermarket.Alias = "BRITECH"
	supermarket.Description = "British Technology Pvt. Ltd."
	supermarketJSON, err := json.Marshal(supermarket)
	if err != nil {
		fmt.Println("Error converting a Supermarket record to JSON")
		return nil
	}
	return []byte(supermarketJSON)
}

//Get an expected value for testing
func getUpdatedSupermarketExpected() []byte {
	var supermarket Supermarket
	supermarket.ID = "100001"
	supermarket.ObjectType = "Participant.SupermarketParticipant"
	supermarket.Alias = "BRITECH (New)"
	supermarket.Description = "British Technology Pvt. Ltd."
	supermarketJSON, err := json.Marshal(supermarket)
	if err != nil {
		fmt.Println("Error converting a Supermarket record to JSON")
		return nil
	}
	return []byte(supermarketJSON)
}

//Get expected values of Supermarkets for testing
func getExpectedSupermarkets() []byte {
	var supermarkets []Supermarket
	var supermarket Supermarket
	supermarket.ID = "100001"
	supermarket.ObjectType = "Participant.SupermarketParticipant"
	supermarket.Alias = "BRITECH"
	supermarket.Description = "British Technology Pvt. Ltd."
	supermarkets = append(supermarkets, supermarket)
	supermarket.ID = "100002"
	supermarket.ObjectType = "Participant.SupermarketParticipant"
	supermarket.Alias = "DEMAGDELAG"
	supermarket.Description = "Demag Delewal AG"
	supermarkets = append(supermarkets, supermarket)
	supermarketJSON, err := json.Marshal(supermarkets)
	if err != nil {
		fmt.Println("Error converting supermarketancer records to JSON")
		return nil
	}
	return []byte(supermarketJSON)
}

func getExpectedSupermarketIDIndex(funcName string) []byte {
	var supermarketIDIndex SupermarketIDIndex
	switch funcName {
	case "addNewSupermarket":
		supermarketIDIndex.IDs = append(supermarketIDIndex.IDs, "100001")
		supermarketIDIndexBytes, err := json.Marshal(supermarketIDIndex)
		if err != nil {
			fmt.Println("Error converting SupermarketIDIndex to JSON")
			return nil
		}
		return supermarketIDIndexBytes
	case "beforeRemoveSupermarket":
		supermarketIDIndex.IDs = append(supermarketIDIndex.IDs, "100001")
		supermarketIDIndex.IDs = append(supermarketIDIndex.IDs, "100002")
		supermarketIDIndexBytes, err := json.Marshal(supermarketIDIndex)
		if err != nil {
			fmt.Println("Error converting SupermarketIDIndex to JSON")
			return nil
		}
		return supermarketIDIndexBytes
	case "afterRemoveSupermarket":
		supermarketIDIndex.IDs = append(supermarketIDIndex.IDs, "100001")
		supermarketIDIndexBytes, err := json.Marshal(supermarketIDIndex)
		if err != nil {
			fmt.Println("Error converting SupermarketIDIndex to JSON")
			return nil
		}
		return supermarketIDIndexBytes
	default:
		supermarketIDIndexBytes, err := json.Marshal(supermarketIDIndex)
		if err != nil {
			fmt.Println("Error converting SupermarketIDIndex to JSON")
			return nil
		}
		return supermarketIDIndexBytes
	}
}

//checkInit - helper to check the Initialization of chaincode: SupermarketParticipant
func checkInit(t *testing.T, stub *shim.MockStub, args [][]byte) {
	res := stub.MockInit("1", args)
	if res.Status != shim.OK {
		fmt.Println("Init failed", string(res.Message))
		t.FailNow()
	}
}

//checkState - helper for checking the chaincode state for a given stateKey afgainst an expected value
func checkState(t *testing.T, stub *shim.MockStub, stateKey string, expectedState []byte) {
	actualState := stub.State[stateKey]
	if actualState == nil {
		fmt.Println("State for ", stateKey, ": failed to get value")
		t.FailNow()
	}

	if bytes.Compare(actualState, expectedState) != 0 {
		fmt.Println("Incorrect State for " + stateKey + ": \nExpected: " + string(expectedState) + "\nActual  : " + string(actualState))
		t.FailNow()
	}
}

//checkInvoke - helper for checking Invoke of chaincode
func checkInvoke(t *testing.T, stub *shim.MockStub, args [][]byte) {
	res := stub.MockInvoke("1", args)
	if res.Status != shim.OK {
		fmt.Println("Invoke", args, "failed", string(res.Message))
		t.FailNow()
	}
}

func checkInvokeUnknownFunction(t *testing.T, stub *shim.MockStub, args [][]byte) {
	res := stub.MockInvoke("1", args)
	if res.Status != shim.OK {
		expectedErr := "Received unknown function invocation"
		actualErr := string(res.Message)
		if !(strings.Contains(actualErr, expectedErr)) {
			fmt.Println("Invoke with unknown function must throw error: ", expectedErr, "\n Actual Error :", actualErr)
			t.FailNow()
		}
	} else {
		t.FailNow()
	}
}

//checkReadSupermarketOK - helper for positive test readSupermarket
func checkReadSupermarketOK(t *testing.T, stub *shim.MockStub, supermarketancerID string) {
	res := stub.MockInvoke("1", [][]byte{[]byte("readSupermarket"), []byte(supermarketancerID)})
	if res.Status != shim.OK {
		fmt.Println("func readSupermarket with ID: ", supermarketancerID, " failed"+string(res.Message))
		t.FailNow()
	}
	if res.Payload == nil {
		fmt.Println("func readSupermarket with ID: ", supermarketancerID, "failed to get value")
		t.FailNow()
	}
	if bytes.Compare(getNewSupermarketExpected(), []byte(res.Payload)) != 0 {
		fmt.Println("func readSupermarket with ID: ", supermarketancerID, "Expected:", string(getNewSupermarketExpected()), "Actual:", string(res.Payload))
		t.FailNow()
	}
}

//checkReadSupermarketNOK - helper for negative testing of readSupermarket
func checkReadSupermarketNOK(t *testing.T, stub *shim.MockStub, supermarketancerID string) {
	//with no supermarketancerID
	res := stub.MockInvoke("1", [][]byte{[]byte("readSupermarket"), []byte("")})
	if res.Status != shim.OK {
		expectedErr := "retrieveSupermarket: Corrupt supermarket record "
		actualErr := string(res.Message)
		if !(strings.Contains(actualErr, expectedErr)) {
			fmt.Println("func readSupermarket neagtive test: ", "Expected Error:", expectedErr, "Actual Error", actualErr)
			t.FailNow()
		}
	} else {
		t.FailNow()
	}
}
func checkReadAllSupermarketsOK(t *testing.T, stub *shim.MockStub) {
	res := stub.MockInvoke("1", [][]byte{[]byte("readAllSupermarkets")})
	if res.Status != shim.OK {
		fmt.Println("func readAllSupermarkets failed", string(res.Message))
		t.FailNow()
	}
	if res.Payload == nil {
		fmt.Println("func readAllSupermarkets failed to get value")
		t.FailNow()
	}
	if bytes.Compare(getExpectedSupermarkets(), []byte(res.Payload)) != 0 {
		fmt.Println("func readAllSupermarkets Expected:\n", string(getExpectedSupermarkets()), "\nActual:\n", string(res.Payload))
		t.FailNow()
	}
}

func checkError(t *testing.T, exp string, act string) {
	if strings.Compare(exp, act) != 0 {
		fmt.Println("Unexpected Error! Expecting ", exp, "\n Actual :", act)
		t.FailNow()
	}
}
