package main

import (
	"bytes"
	"encoding/json"
	"fmt"
	"strings"
	"testing"

	"github.com/hyperledger/fabric/core/chaincode/shim"
)

//TestBatchAsset_Init
func TestBatchAsset_Init(t *testing.T) {
	batch := new(BatchAsset)
	stub := shim.NewMockStub("batch", batch)
	checkInit(t, stub, [][]byte{[]byte("init")})
	checkState(t, stub, "batchIDIndex", getExpectedBatchIDIndex("init"))
}

//TestBatchAsset_InvokeUnknownFunction
func TestBatchAsset_InvokeUnknownFunction(t *testing.T) {
	batch := new(BatchAsset)
	stub := shim.NewMockStub("batch", batch)
	checkInit(t, stub, [][]byte{[]byte("init")})
	checkInvokeUnknownFunction(t, stub, [][]byte{[]byte("myFunction"), []byte("docType:Asset")})
}

//TestBatchAsset_Invoke_addNewBatch
func TestBatchAsset_Invoke_addNewBatchOK(t *testing.T) {
	batch := new(BatchAsset)
	stub := shim.NewMockStub("batch", batch)
	checkInit(t, stub, [][]byte{[]byte("init")})
	checkInvoke(t, stub, getFirstBatchAssetForTesting())
	newBatchID := "100001"
	checkState(t, stub, newBatchID, getNewBatchExpected())
	checkState(t, stub, "batchIDIndex", getExpectedBatchIDIndex("addNewBatch"))
}

//TestBatchAsset_Invoke_addNewBatchUnknownField
func TestBatchAsset_Invoke_addNewBatchUnknownField(t *testing.T) {
	batch := new(BatchAsset)
	stub := shim.NewMockStub("batch", batch)
	checkInit(t, stub, [][]byte{[]byte("init")})
	res := stub.MockInvoke("1", getBatchAssetWithUnknownFieldForTesting())
	if res.Status != shim.OK {
		checkError(t, "Batch Data is Corrupted", res.Message)
	} else {
		fmt.Println("Unknown Field Error was expected, but not raised")
		t.FailNow()
	}
}

//TestBatchAsset_Invoke_addNewBatch
func TestBatchAsset_Invoke_addNewBatchDuplicate(t *testing.T) {
	batch := new(BatchAsset)
	stub := shim.NewMockStub("batch", batch)
	checkInit(t, stub, [][]byte{[]byte("init")})
	checkInvoke(t, stub, getFirstBatchAssetForTesting())
	newBatchID := "100001"
	checkState(t, stub, newBatchID, getNewBatchExpected())
	checkState(t, stub, "batchIDIndex", getExpectedBatchIDIndex("addNewBatch"))
	res := stub.MockInvoke("1", getFirstBatchAssetForTesting())
	if res.Status != shim.OK {
		checkError(t, "This Batch already exists: 100001", res.Message)
	} else {
		fmt.Println("Error was expected, but not raised")
		t.FailNow()
	}
}

//TestBatchAsset_Invoke_removeBatchOK  //change template
func TestBatchAsset_Invoke_removeBatchOK(t *testing.T) {
	batch := new(BatchAsset)
	stub := shim.NewMockStub("batch", batch)
	checkInit(t, stub, [][]byte{[]byte("init")})
	checkInvoke(t, stub, getFirstBatchAssetForTesting())
	checkInvoke(t, stub, getSecondBatchAssetForTesting())
	checkReadAllBatchsOK(t, stub)
	checkState(t, stub, "batchIDIndex", getExpectedBatchIDIndex("beforeRemoveBatch"))
	checkInvoke(t, stub, getRemoveSecondBatchAssetForTesting())
	remainingBatchID := "100001"
	checkReadBatchOK(t, stub, remainingBatchID)
	checkState(t, stub, "batchIDIndex", getExpectedBatchIDIndex("afterRemoveBatch"))
}

//TestBatchAsset_Invoke_removeBatchNOK  //change template
func TestBatchAsset_Invoke_removeBatchNOK(t *testing.T) {
	batch := new(BatchAsset)
	stub := shim.NewMockStub("batch", batch)
	checkInit(t, stub, [][]byte{[]byte("init")})
	checkInvoke(t, stub, getFirstBatchAssetForTesting())
	firstBatchID := "100001"
	checkReadBatchOK(t, stub, firstBatchID)
	checkState(t, stub, "batchIDIndex", getExpectedBatchIDIndex("addNewBatch"))
	res := stub.MockInvoke("1", getRemoveSecondBatchAssetForTesting())
	if res.Status != shim.OK {
		checkError(t, "Batch with ID: "+"100002"+" not found", res.Message)
	} else {
		fmt.Println("Error was expected, but not raised")
		t.FailNow()
	}
	checkState(t, stub, "batchIDIndex", getExpectedBatchIDIndex("addNewBatch"))
}

//TestBatchAsset_Invoke_removeAllBatchsOK  //change template
func TestBatchAsset_Invoke_removeAllBatchsOK(t *testing.T) {
	batch := new(BatchAsset)
	stub := shim.NewMockStub("batch", batch)
	checkInit(t, stub, [][]byte{[]byte("init")})
	checkInvoke(t, stub, getFirstBatchAssetForTesting())
	checkInvoke(t, stub, getSecondBatchAssetForTesting())
	checkReadAllBatchsOK(t, stub)
	checkState(t, stub, "batchIDIndex", getExpectedBatchIDIndex("beforeRemoveBatch"))
	checkInvoke(t, stub, getRemoveAllBatchAssetsForTesting())
	checkState(t, stub, "batchIDIndex", getExpectedBatchIDIndex(""))
}

//TestBatchAsset_Invoke_removeBatchNOK  //change template
func TestBatchAsset_Invoke_removeAllBatchsNOK(t *testing.T) {
	batch := new(BatchAsset)
	stub := shim.NewMockStub("batch", batch)
	checkInit(t, stub, [][]byte{[]byte("init")})
	res := stub.MockInvoke("1", getRemoveAllBatchAssetsForTesting())
	if res.Status != shim.OK {
		checkError(t, "removeAllBatchs: No batchs to remove", res.Message)
	} else {
		fmt.Println("Error was expected, but not raised")
		t.FailNow()
	}
	checkState(t, stub, "batchIDIndex", getExpectedBatchIDIndex(""))
}

//TestBatchAsset_Query_readBatch
func TestBatchAsset_Query_readBatch(t *testing.T) {
	batch := new(BatchAsset)
	stub := shim.NewMockStub("batch", batch)
	checkInit(t, stub, [][]byte{[]byte("init")})
	batchID := "100001"
	checkInvoke(t, stub, getFirstBatchAssetForTesting())
	checkReadBatchOK(t, stub, batchID)
	checkReadBatchNOK(t, stub, "")
}

//TestBatchAsset_Query_readAllBatchs
func TestBatchAsset_Query_readAllBatchs(t *testing.T) {
	batch := new(BatchAsset)
	stub := shim.NewMockStub("batch", batch)
	checkInit(t, stub, [][]byte{[]byte("init")})
	checkInvoke(t, stub, getFirstBatchAssetForTesting())
	checkInvoke(t, stub, getSecondBatchAssetForTesting())
	checkReadAllBatchsOK(t, stub)
}

/*
*
*	Helper Functions
*
 */
//Get first BatchAsset for testing
func getFirstBatchAssetForTesting() [][]byte {
	return [][]byte{[]byte("addNewBatch"),
		[]byte("{\"ID\":\"100001\",\"docType\":\"Asset.BatchAsset\",\"grade\":\"arabica\",\"bagCollection\":[{\"ID\":\"100001\"},{\"ID\":\"100002\"}]}")}
}

//Get BatchAsset with unknown field for testing
func getBatchAssetWithUnknownFieldForTesting() [][]byte {
	return [][]byte{[]byte("addNewBatch"),
		[]byte("{\"ID\":\"100001\",\"docuType\":\"Asset.BatchAsset\",\"grade\":\"arabica\",\"bagCollection\":[{\"ID\":\"100001\"},{\"ID\":\"100002\"}]}")}
}

//Get second BatchAsset for testing
func getSecondBatchAssetForTesting() [][]byte {
	return [][]byte{[]byte("addNewBatch"),
		[]byte("{\"ID\":\"100002\",\"docType\":\"Asset.BatchAsset\",\"grade\":\"nigerian\",\"bagCollection\":[{\"ID\":\"100003\"},{\"ID\":\"100004\"}]}")}
}

//Get remove second BatchAsset for testing //change template
func getRemoveSecondBatchAssetForTesting() [][]byte {
	return [][]byte{[]byte("removeBatch"),
		[]byte("100002")}
}

//Get remove all BatchAssets for testing //change template
func getRemoveAllBatchAssetsForTesting() [][]byte {
	return [][]byte{[]byte("removeAllBatchs")}
}

//Get an expected value for testing
func getNewBatchExpected() []byte {
	var batch Batch
	var bag Bag
	var bagCollection []Bag
	bag.ID = "100001"
	bagCollection = append(bagCollection, bag)
	bag.ID = "100002"
	bagCollection = append(bagCollection, bag)
	batch.ID = "100001"
	batch.ObjectType = "Asset.BatchAsset"
	batch.Grade = "arabica"
	batch.BagCollection = bagCollection
	batchJSON, err := json.Marshal(batch)
	if err != nil {
		fmt.Println("Error converting a Batch record to JSON")
		return nil
	}
	return []byte(batchJSON)
}

//Get expected values of Batchs for testing
func getExpectedBatchs() []byte {
	var batchStructs []Batch
	var batchStruct Batch
	var bag Bag
	var bag1 Bag
	var bagCollection []Bag
	var bagCollection1 []Bag
	bag.ID = "100001"
	bagCollection = append(bagCollection, bag)
	bag.ID = "100002"
	bagCollection = append(bagCollection, bag)
	batchStruct.ID = "100001"
	batchStruct.ObjectType = "Asset.BatchAsset"
	batchStruct.Grade = "arabica"
	batchStruct.BagCollection = bagCollection
	batchStructs = append(batchStructs, batchStruct)
	bag1.ID = "100003"
	bagCollection1 = append(bagCollection1, bag1)
	bag1.ID = "100004"
	bagCollection1 = append(bagCollection1, bag1)
	batchStruct.ID = "100002"
	batchStruct.ObjectType = "Asset.BatchAsset"
	batchStruct.Grade = "nigerian"
	batchStruct.BagCollection = bagCollection1
	batchStructs = append(batchStructs, batchStruct)
	batchStructJSON, err := json.Marshal(batchStructs)
	if err != nil {
		fmt.Println("Error converting batch records to JSON")
		return nil
	}
	return []byte(batchStructJSON)
}

func getExpectedBatchIDIndex(funcName string) []byte {
	var batchIDIndex BatchIDIndex
	switch funcName {
	case "addNewBatch":
		batchIDIndex.IDs = append(batchIDIndex.IDs, "100001")
		batchIDIndexBytes, err := json.Marshal(batchIDIndex)
		if err != nil {
			fmt.Println("Error converting BatchIDIndex to JSON")
			return nil
		}
		return batchIDIndexBytes
	case "beforeRemoveBatch":
		batchIDIndex.IDs = append(batchIDIndex.IDs, "100001")
		batchIDIndex.IDs = append(batchIDIndex.IDs, "100002")
		batchIDIndexBytes, err := json.Marshal(batchIDIndex)
		if err != nil {
			fmt.Println("Error converting BatchIDIndex to JSON")
			return nil
		}
		return batchIDIndexBytes
	case "afterRemoveBatch":
		batchIDIndex.IDs = append(batchIDIndex.IDs, "100001")
		batchIDIndexBytes, err := json.Marshal(batchIDIndex)
		if err != nil {
			fmt.Println("Error converting BatchIDIndex to JSON")
			return nil
		}
		return batchIDIndexBytes
	default:
		batchIDIndexBytes, err := json.Marshal(batchIDIndex)
		if err != nil {
			fmt.Println("Error converting BatchIDIndex to JSON")
			return nil
		}
		return batchIDIndexBytes
	}
}

//checkInit - helper to check the Initialization of chaincode: BatchAsset
func checkInit(t *testing.T, stub *shim.MockStub, args [][]byte) {
	res := stub.MockInit("1", args)
	if res.Status != shim.OK {
		fmt.Println("Init failed", string(res.Message))
		t.FailNow()
	}
}

//checkState - helper for checking the chaincode state for a given stateKey afgainst an expected value
func checkState(t *testing.T, stub *shim.MockStub, stateKey string, expectedState []byte) {
	actualState := stub.State[stateKey]
	if actualState == nil {
		fmt.Println("State for ", stateKey, ": failed to get value")
		t.FailNow()
	}

	if bytes.Compare(actualState, expectedState) != 0 {
		fmt.Println("Incorrect State for " + stateKey + ": \nExpected: " + string(expectedState) + "\nActual  : " + string(actualState))
		t.FailNow()
	}
}

//checkInvoke - helper for checking Invoke of chaincode
func checkInvoke(t *testing.T, stub *shim.MockStub, args [][]byte) {
	res := stub.MockInvoke("1", args)
	if res.Status != shim.OK {
		fmt.Println("Invoke", args, "failed", string(res.Message))
		t.FailNow()
	}
}

func checkInvokeUnknownFunction(t *testing.T, stub *shim.MockStub, args [][]byte) {
	res := stub.MockInvoke("1", args)
	if res.Status != shim.OK {
		expectedErr := "Received unknown function invocation"
		actualErr := string(res.Message)
		if !(strings.Contains(actualErr, expectedErr)) {
			fmt.Println("Invoke with unknown function must throw error: ", expectedErr, "\n Actual Error :", actualErr)
			t.FailNow()
		}
	} else {
		t.FailNow()
	}
}

//checkReadBatchOK - helper for positive test readBatch
func checkReadBatchOK(t *testing.T, stub *shim.MockStub, batchID string) {
	res := stub.MockInvoke("1", [][]byte{[]byte("readBatch"), []byte(batchID)})
	if res.Status != shim.OK {
		fmt.Println("func readBatch with ID: ", batchID, " failed"+string(res.Message))
		t.FailNow()
	}
	if res.Payload == nil {
		fmt.Println("func readBatch with ID: ", batchID, "failed to get value")
		t.FailNow()
	}
	if bytes.Compare(getNewBatchExpected(), []byte(res.Payload)) != 0 {
		fmt.Println("func readBatch with ID: ", batchID, "Expected:", string(getNewBatchExpected()), "Actual:", string(res.Payload))
		t.FailNow()
	}
}

//checkReadBatchNOK - helper for negative testing of readBatch
func checkReadBatchNOK(t *testing.T, stub *shim.MockStub, batchID string) {
	//with no batchID
	res := stub.MockInvoke("1", [][]byte{[]byte("readBatch"), []byte("")})
	if res.Status != shim.OK {
		expectedErr := "retrieveBatch: Corrupt batch record "
		actualErr := string(res.Message)
		if !(strings.Contains(actualErr, expectedErr)) {
			fmt.Println("func readBatch negative test: ", "Expected Error:", expectedErr, "Actual Error", actualErr)
			t.FailNow()
		}
	} else {
		t.FailNow()
	}
}

func checkReadAllBatchsOK(t *testing.T, stub *shim.MockStub) {
	res := stub.MockInvoke("1", [][]byte{[]byte("readAllBatchs")})
	if res.Status != shim.OK {
		fmt.Println("func readAllBatchs failed", string(res.Message))
		t.FailNow()
	}
	if res.Payload == nil {
		fmt.Println("func readAllBatchs failed to get value")
		t.FailNow()
	}
	if bytes.Compare(getExpectedBatchs(), []byte(res.Payload)) != 0 {
		fmt.Println("func readAllBatchs Expected:\n", string(getExpectedBatchs()), "\nActual:\n", string(res.Payload))
		t.FailNow()
	}
}

func checkError(t *testing.T, exp string, act string) {
	if strings.Compare(exp, act) != 0 {
		fmt.Println("Unexpected Error! Expecting ", exp, "\n Actual :", act)
		t.FailNow()
	}
}
