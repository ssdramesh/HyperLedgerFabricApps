package main

import (
	"encoding/json"
	"errors"
	"fmt"
	"strings"

	"github.com/hyperledger/fabric/core/chaincode/shim"
	"github.com/hyperledger/fabric/protos/peer"
)

var logger = shim.NewLogger("CLDChaincode")

//TripAsset - Chaincode for asset Trip
type TripAsset struct {
}

//Trip - Details of the asset type Trip
type Trip struct {
	ID         string `json:"ID"`
	ObjectType string `json:"docType"`
	Weight     string `json:"weight"`
	Date       string `json:"date"`
}

//TripIDIndex - Index on IDs for retrieval all Trips
type TripIDIndex struct {
	IDs []string `json:"IDs"`
}

func main() {
	err := shim.Start(new(TripAsset))
	if err != nil {
		fmt.Printf("Error starting TripAsset chaincode function main(): %s", err)
	} else {
		fmt.Printf("Starting TripAsset chaincode function main() executed successfully")
	}
}

//Init - The chaincode Init function: No  arguments, only initializes a ID array as Index for retrieval of all Trips
func (tp *TripAsset) Init(stub shim.ChaincodeStubInterface) peer.Response {
	var trpIDIndex TripIDIndex
	record, _ := stub.GetState("trpIDIndex")
	//chaincode upgrade: Index might already exist, create new only if it does not
	if record == nil {
		bytes, _ := json.Marshal(trpIDIndex)
		stub.PutState("trpIDIndex", bytes)
	}
	return shim.Success(nil)
}

//Invoke - The chaincode Invoke function:
func (tp *TripAsset) Invoke(stub shim.ChaincodeStubInterface) peer.Response {
	function, args := stub.GetFunctionAndParameters()
	logger.Debug("function: ", function)
	switch function {
	case "addNewTrip":
		return tp.addNewTrip(stub, args)
	case "removeTrip":
		return tp.removeTrip(stub, args[0])
	case "removeAllTrips":
		return tp.removeAllTrips(stub)
	case "readTrip":
		return tp.readTrip(stub, args[0])
	case "readAllTrips":
		return tp.readAllTrips(stub)
	default:
		return shim.Error("Received unknown function invocation")
	}
}

//Invoke Route: addNewTrip
func (tp *TripAsset) addNewTrip(stub shim.ChaincodeStubInterface, args []string) peer.Response {
	trp, err := getTripFromArgs(args)
	if err != nil {
		return shim.Error("Trip Data is Corrupted")
	}
	trp.ObjectType = "Asset.TripAsset"
	record, err := stub.GetState(trp.ID)
	if record != nil {
		return shim.Error("This Trip already exists: " + trp.ID)
	}
	_, err = tp.saveTrip(stub, trp)
	if err != nil {
		return shim.Error(err.Error())
	}
	_, err = tp.updateTripIDIndex(stub, trp)
	if err != nil {
		return shim.Error(err.Error())
	}
	return shim.Success(nil)
}

//Invoke Route: removeTrip
func (tp *TripAsset) removeTrip(stub shim.ChaincodeStubInterface, trpID string) peer.Response {
	_, err := tp.deleteTrip(stub, trpID)
	if err != nil {
		return shim.Error(err.Error())
	}
	_, err = tp.deleteTripIDIndex(stub, trpID)
	if err != nil {
		return shim.Error(err.Error())
	}
	return shim.Success(nil)
}

//Invoke Route: removeAllTrips
func (tp *TripAsset) removeAllTrips(stub shim.ChaincodeStubInterface) peer.Response {
	var trpStructIDs TripIDIndex
	bytes, err := stub.GetState("trpIDIndex")
	if err != nil {
		return shim.Error("removeAllTrips: Error getting trpIDIndex array")
	}
	err = json.Unmarshal(bytes, &trpStructIDs)
	if err != nil {
		return shim.Error("removeAllTrips: Error unmarshalling trpIDIndex array JSON")
	}
	if len(trpStructIDs.IDs) == 0 {
		return shim.Error("removeAllTrips: No trps to remove")
	}
	for _, trpStructID := range trpStructIDs.IDs {
		_, err = tp.deleteTrip(stub, trpStructID)
		if err != nil {
			return shim.Error("Failed to remove Trip with ID: " + trpStructID)
		}
		_, err = tp.deleteTripIDIndex(stub, trpStructID)
		if err != nil {
			return shim.Error(err.Error())
		}
	}
	tp.initHolder(stub)
	return shim.Success(nil)
}

//Query Route: readTrip
func (tp *TripAsset) readTrip(stub shim.ChaincodeStubInterface, trpID string) peer.Response {
	trpAsByteArray, err := tp.retrieveTrip(stub, trpID)
	if err != nil {
		return shim.Error(err.Error())
	}
	return shim.Success(trpAsByteArray)
}

//Query Route: readAllTrips
func (tp *TripAsset) readAllTrips(stub shim.ChaincodeStubInterface) peer.Response {
	var trpIDs TripIDIndex
	bytes, err := stub.GetState("trpIDIndex")
	if err != nil {
		return shim.Error("readAllTrips: Error getting trpIDIndex array")
	}
	err = json.Unmarshal(bytes, &trpIDs)
	if err != nil {
		return shim.Error("readAllTrips: Error unmarshalling trpIDIndex array JSON")
	}
	result := "["

	var trpAsByteArray []byte

	for _, trpID := range trpIDs.IDs {
		trpAsByteArray, err = tp.retrieveTrip(stub, trpID)
		if err != nil {
			return shim.Error("Failed to retrieve trp with ID: " + trpID)
		}
		result += string(trpAsByteArray) + ","
	}
	if len(result) == 1 {
		result = "[]"
	} else {
		result = result[:len(result)-1] + "]"
	}
	return shim.Success([]byte(result))
}

//Helper: Save TripAsset
func (tp *TripAsset) saveTrip(stub shim.ChaincodeStubInterface, trp Trip) (bool, error) {
	bytes, err := json.Marshal(trp)
	if err != nil {
		return false, errors.New("Error converting trp record JSON")
	}
	err = stub.PutState(trp.ID, bytes)
	if err != nil {
		return false, errors.New("Error storing Trip record")
	}
	return true, nil
}

//Helper: delete TripAsset
func (tp *TripAsset) deleteTrip(stub shim.ChaincodeStubInterface, trpID string) (bool, error) {
	_, err := tp.retrieveTrip(stub, trpID)
	if err != nil {
		return false, errors.New("Trip with ID: " + trpID + " not found")
	}
	err = stub.DelState(trpID)
	if err != nil {
		return false, errors.New("Error deleting Trip record")
	}
	return true, nil
}

//Helper: Update trp Holder - updates Index
func (tp *TripAsset) updateTripIDIndex(stub shim.ChaincodeStubInterface, trp Trip) (bool, error) {
	var trpIDs TripIDIndex
	bytes, err := stub.GetState("trpIDIndex")
	if err != nil {
		return false, errors.New("updateTripIDIndex: Error getting trpIDIndex array Index from state")
	}
	err = json.Unmarshal(bytes, &trpIDs)
	if err != nil {
		return false, errors.New("updateTripIDIndex: Error unmarshalling trpIDIndex array JSON")
	}
	trpIDs.IDs = append(trpIDs.IDs, trp.ID)
	bytes, err = json.Marshal(trpIDs)
	if err != nil {
		return false, errors.New("updateTripIDIndex: Error marshalling new trp ID")
	}
	err = stub.PutState("trpIDIndex", bytes)
	if err != nil {
		return false, errors.New("updateTripIDIndex: Error storing new trp ID in trpIDIndex (Index)")
	}
	return true, nil
}

//Helper: delete ID from trpStruct Holder
func (tp *TripAsset) deleteTripIDIndex(stub shim.ChaincodeStubInterface, trpID string) (bool, error) {
	var trpStructIDs TripIDIndex
	bytes, err := stub.GetState("trpIDIndex")
	if err != nil {
		return false, errors.New("deleteTripIDIndex: Error getting trpIDIndex array Index from state")
	}
	err = json.Unmarshal(bytes, &trpStructIDs)
	if err != nil {
		return false, errors.New("deleteTripIDIndex: Error unmarshalling trpIDIndex array JSON")
	}
	trpStructIDs.IDs, err = deleteKeyFromStringArray(trpStructIDs.IDs, trpID)
	if err != nil {
		return false, errors.New(err.Error())
	}
	bytes, err = json.Marshal(trpStructIDs)
	if err != nil {
		return false, errors.New("deleteTripIDIndex: Error marshalling new trpStruct ID")
	}
	err = stub.PutState("trpIDIndex", bytes)
	if err != nil {
		return false, errors.New("deleteTripIDIndex: Error storing new trpStruct ID in trpIDIndex (Index)")
	}
	return true, nil
}

//Helper: Initialize truck ID Holder //change template
func (tp *TripAsset) initHolder(stub shim.ChaincodeStubInterface) bool {
	var trpIDIndex TripIDIndex
	bytes, _ := json.Marshal(trpIDIndex)
	stub.DelState("trpIDIndex")
	stub.PutState("trpIDIndex", bytes)
	return true
}

//deleteKeyFromArray
func deleteKeyFromStringArray(array []string, key string) (newArray []string, err error) {
	for _, entry := range array {
		if entry != key {
			newArray = append(newArray, entry)
		}
	}
	if len(newArray) == len(array) {
		return newArray, errors.New("Specified Key: " + key + " not found in Array")
	}
	return newArray, nil
}

//Helper: Retrieve purchaser
func (tp *TripAsset) retrieveTrip(stub shim.ChaincodeStubInterface, trpID string) ([]byte, error) {
	var trp Trip
	var trpAsByteArray []byte
	bytes, err := stub.GetState(trpID)
	if err != nil {
		return trpAsByteArray, errors.New("retrieveTrip: Error retrieving trp with ID: " + trpID)
	}
	err = json.Unmarshal(bytes, &trp)
	if err != nil {
		return trpAsByteArray, errors.New("retrieveTrip: Corrupt trp record " + string(bytes))
	}
	trpAsByteArray, err = json.Marshal(trp)
	if err != nil {
		return trpAsByteArray, errors.New("readTrip: Invalid trp Object - Not a  valid JSON")
	}
	return trpAsByteArray, nil
}

//getTripFromArgs - construct a trp structure from string array of arguments
func getTripFromArgs(args []string) (trp Trip, err error) {

	if strings.Contains(args[0], "\"ID\"") == false ||
		strings.Contains(args[0], "\"docType\"") == false ||
		strings.Contains(args[0], "\"weight\"") == false ||
		strings.Contains(args[0], "\"date\"") == false {
		return trp, errors.New("Unknown field: Input JSON does not comply to schema")
	}

	err = json.Unmarshal([]byte(args[0]), &trp)
	if err != nil {
		return trp, err
	}
	return trp, nil

}
