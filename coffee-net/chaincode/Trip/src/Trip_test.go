package main

import (
	"bytes"
	"encoding/json"
	"fmt"
	"strings"
	"testing"

	"github.com/hyperledger/fabric/core/chaincode/shim"
)

//TestTripAsset_Init
func TestTripAsset_Init(t *testing.T) {
	trp := new(TripAsset)
	stub := shim.NewMockStub("trp", trp)
	checkInit(t, stub, [][]byte{[]byte("init")})
	checkState(t, stub, "trpIDIndex", getExpectedTripIDIndex("init"))
}

//TestTripAsset_InvokeUnknownFunction
func TestTripAsset_InvokeUnknownFunction(t *testing.T) {
	trp := new(TripAsset)
	stub := shim.NewMockStub("trp", trp)
	checkInit(t, stub, [][]byte{[]byte("init")})
	checkInvokeUnknownFunction(t, stub, [][]byte{[]byte("myFunction"), []byte("docType:Asset")})
}

//TestTripAsset_Invoke_addNewTrip
func TestTripAsset_Invoke_addNewTripOK(t *testing.T) {
	trp := new(TripAsset)
	stub := shim.NewMockStub("trp", trp)
	checkInit(t, stub, [][]byte{[]byte("init")})
	checkInvoke(t, stub, getFirstTripAssetForTesting())
	newTripID := "100001"
	checkState(t, stub, newTripID, getNewTripExpected())
	checkState(t, stub, "trpIDIndex", getExpectedTripIDIndex("addNewTrip"))
}

//TestTripAsset_Invoke_addNewTripUnknownField
func TestTripAsset_Invoke_addNewTripUnknownField(t *testing.T) {
	trp := new(TripAsset)
	stub := shim.NewMockStub("trp", trp)
	checkInit(t, stub, [][]byte{[]byte("init")})
	res := stub.MockInvoke("1", getTripAssetWithUnknownFieldForTesting())
	if res.Status != shim.OK {
		checkError(t, "Trip Data is Corrupted", res.Message)
	} else {
		fmt.Println("Unknown Field Error was expected, but not raised")
		t.FailNow()
	}
}

//TestTripAsset_Invoke_addNewTrip
func TestTripAsset_Invoke_addNewTripDuplicate(t *testing.T) {
	trp := new(TripAsset)
	stub := shim.NewMockStub("trp", trp)
	checkInit(t, stub, [][]byte{[]byte("init")})
	checkInvoke(t, stub, getFirstTripAssetForTesting())
	newTripID := "100001"
	checkState(t, stub, newTripID, getNewTripExpected())
	checkState(t, stub, "trpIDIndex", getExpectedTripIDIndex("addNewTrip"))
	res := stub.MockInvoke("1", getFirstTripAssetForTesting())
	if res.Status != shim.OK {
		checkError(t, "This Trip already exists: 100001", res.Message)
	} else {
		fmt.Println("Error was expected, but not raised")
		t.FailNow()
	}
}

//TestTripAsset_Invoke_removeTripOK  //change template
func TestTripAsset_Invoke_removeTripOK(t *testing.T) {
	trp := new(TripAsset)
	stub := shim.NewMockStub("trp", trp)
	checkInit(t, stub, [][]byte{[]byte("init")})
	checkInvoke(t, stub, getFirstTripAssetForTesting())
	checkInvoke(t, stub, getSecondTripAssetForTesting())
	checkReadAllTripsOK(t, stub)
	checkState(t, stub, "trpIDIndex", getExpectedTripIDIndex("beforeRemoveTrip"))
	checkInvoke(t, stub, getRemoveSecondTripAssetForTesting())
	remainingTripID := "100001"
	checkReadTripOK(t, stub, remainingTripID)
	checkState(t, stub, "trpIDIndex", getExpectedTripIDIndex("afterRemoveTrip"))
}

//TestTripAsset_Invoke_removeTripNOK  //change template
func TestTripAsset_Invoke_removeTripNOK(t *testing.T) {
	trp := new(TripAsset)
	stub := shim.NewMockStub("trp", trp)
	checkInit(t, stub, [][]byte{[]byte("init")})
	checkInvoke(t, stub, getFirstTripAssetForTesting())
	firstTripID := "100001"
	checkReadTripOK(t, stub, firstTripID)
	checkState(t, stub, "trpIDIndex", getExpectedTripIDIndex("addNewTrip"))
	res := stub.MockInvoke("1", getRemoveSecondTripAssetForTesting())
	if res.Status != shim.OK {
		checkError(t, "Trip with ID: "+"100002"+" not found", res.Message)
	} else {
		fmt.Println("Error was expected, but not raised")
		t.FailNow()
	}
	checkState(t, stub, "trpIDIndex", getExpectedTripIDIndex("addNewTrip"))
}

//TestTripAsset_Invoke_removeAllTripsOK  //change template
func TestTripAsset_Invoke_removeAllTripsOK(t *testing.T) {
	trp := new(TripAsset)
	stub := shim.NewMockStub("trp", trp)
	checkInit(t, stub, [][]byte{[]byte("init")})
	checkInvoke(t, stub, getFirstTripAssetForTesting())
	checkInvoke(t, stub, getSecondTripAssetForTesting())
	checkReadAllTripsOK(t, stub)
	checkState(t, stub, "trpIDIndex", getExpectedTripIDIndex("beforeRemoveTrip"))
	checkInvoke(t, stub, getRemoveAllTripAssetsForTesting())
	checkState(t, stub, "trpIDIndex", getExpectedTripIDIndex(""))
}

//TestTripAsset_Invoke_removeTripNOK  //change template
func TestTripAsset_Invoke_removeAllTripsNOK(t *testing.T) {
	trp := new(TripAsset)
	stub := shim.NewMockStub("trp", trp)
	checkInit(t, stub, [][]byte{[]byte("init")})
	res := stub.MockInvoke("1", getRemoveAllTripAssetsForTesting())
	if res.Status != shim.OK {
		checkError(t, "removeAllTrips: No trps to remove", res.Message)
	} else {
		fmt.Println("Error was expected, but not raised")
		t.FailNow()
	}
	checkState(t, stub, "trpIDIndex", getExpectedTripIDIndex(""))
}

//TestTripAsset_Query_readTrip
func TestTripAsset_Query_readTrip(t *testing.T) {
	trp := new(TripAsset)
	stub := shim.NewMockStub("trp", trp)
	checkInit(t, stub, [][]byte{[]byte("init")})
	trpID := "100001"
	checkInvoke(t, stub, getFirstTripAssetForTesting())
	checkReadTripOK(t, stub, trpID)
	checkReadTripNOK(t, stub, "")
}

//TestTripAsset_Query_readAllTrips
func TestTripAsset_Query_readAllTrips(t *testing.T) {
	trp := new(TripAsset)
	stub := shim.NewMockStub("trp", trp)
	checkInit(t, stub, [][]byte{[]byte("init")})
	checkInvoke(t, stub, getFirstTripAssetForTesting())
	checkInvoke(t, stub, getSecondTripAssetForTesting())
	checkReadAllTripsOK(t, stub)
}

/*
*
*	Helper Functions
*
 */
//Get first TripAsset for testing
func getFirstTripAssetForTesting() [][]byte {
	return [][]byte{[]byte("addNewTrip"),
		[]byte("{\"ID\":\"100001\",\"docType\":\"Asset.TripAsset\",\"weight\":\"100\",\"date\":\"12/01/2018\"}")}
}

//Get TripAsset with unknown field for testing
func getTripAssetWithUnknownFieldForTesting() [][]byte {
	return [][]byte{[]byte("addNewTrip"),
		[]byte("{\"ID\":\"100001\",\"docuType\":\"Asset.TripAsset\",\"weight\":\"100\",\"date\":\"12/01/2018\"}")}
}

//Get second TripAsset for testing
func getSecondTripAssetForTesting() [][]byte {
	return [][]byte{[]byte("addNewTrip"),
		[]byte("{\"ID\":\"100002\",\"docType\":\"Asset.TripAsset\",\"weight\":\"200\",\"date\":\"12/01/2018\"}")}
}

//Get remove second TripAsset for testing //change template
func getRemoveSecondTripAssetForTesting() [][]byte {
	return [][]byte{[]byte("removeTrip"),
		[]byte("100002")}
}

//Get remove all TripAssets for testing //change template
func getRemoveAllTripAssetsForTesting() [][]byte {
	return [][]byte{[]byte("removeAllTrips")}
}

//Get an expected value for testing
func getNewTripExpected() []byte {
	var trp Trip
	trp.ID = "100001"
	trp.ObjectType = "Asset.TripAsset"
	trp.Weight = "100"
	trp.Date = "12/01/2018"
	trpJSON, err := json.Marshal(trp)
	if err != nil {
		fmt.Println("Error converting a Trip record to JSON")
		return nil
	}
	return []byte(trpJSON)
}

//Get expected values of Trips for testing
func getExpectedTrips() []byte {
	var trps []Trip
	var trp Trip
	trp.ID = "100001"
	trp.ObjectType = "Asset.TripAsset"
	trp.Weight = "100"
	trp.Date = "12/01/2018"
	trps = append(trps, trp)
	trp.ID = "100002"
	trp.ObjectType = "Asset.TripAsset"
	trp.Weight = "200"
	trp.Date = "12/01/2018"
	trps = append(trps, trp)
	trpJSON, err := json.Marshal(trps)
	if err != nil {
		fmt.Println("Error converting trp records to JSON")
		return nil
	}
	return []byte(trpJSON)
}

func getExpectedTripIDIndex(funcName string) []byte {
	var trpIDIndex TripIDIndex
	switch funcName {
	case "addNewTrip":
		trpIDIndex.IDs = append(trpIDIndex.IDs, "100001")
		trpIDIndexBytes, err := json.Marshal(trpIDIndex)
		if err != nil {
			fmt.Println("Error converting TripIDIndex to JSON")
			return nil
		}
		return trpIDIndexBytes
	case "beforeRemoveTrip":
		trpIDIndex.IDs = append(trpIDIndex.IDs, "100001")
		trpIDIndex.IDs = append(trpIDIndex.IDs, "100002")
		trpIDIndexBytes, err := json.Marshal(trpIDIndex)
		if err != nil {
			fmt.Println("Error converting TripIDIndex to JSON")
			return nil
		}
		return trpIDIndexBytes
	case "afterRemoveTrip":
		trpIDIndex.IDs = append(trpIDIndex.IDs, "100001")
		trpIDIndexBytes, err := json.Marshal(trpIDIndex)
		if err != nil {
			fmt.Println("Error converting TripIDIndex to JSON")
			return nil
		}
		return trpIDIndexBytes
	default:
		trpIDIndexBytes, err := json.Marshal(trpIDIndex)
		if err != nil {
			fmt.Println("Error converting TripIDIndex to JSON")
			return nil
		}
		return trpIDIndexBytes
	}
}

//checkInit - helper to check the Initialization of chaincode: TripAsset
func checkInit(t *testing.T, stub *shim.MockStub, args [][]byte) {
	res := stub.MockInit("1", args)
	if res.Status != shim.OK {
		fmt.Println("Init failed", string(res.Message))
		t.FailNow()
	}
}

//checkState - helper for checking the chaincode state for a given stateKey afgainst an expected value
func checkState(t *testing.T, stub *shim.MockStub, stateKey string, expectedState []byte) {
	actualState := stub.State[stateKey]
	if actualState == nil {
		fmt.Println("State for ", stateKey, ": failed to get value")
		t.FailNow()
	}

	if bytes.Compare(actualState, expectedState) != 0 {
		fmt.Println("Incorrect State for " + stateKey + ": \nExpected: " + string(expectedState) + "\nActual  : " + string(actualState))
		t.FailNow()
	}
}

//checkInvoke - helper for checking Invoke of chaincode
func checkInvoke(t *testing.T, stub *shim.MockStub, args [][]byte) {
	res := stub.MockInvoke("1", args)
	if res.Status != shim.OK {
		fmt.Println("Invoke", args, "failed", string(res.Message))
		t.FailNow()
	}
}

func checkInvokeUnknownFunction(t *testing.T, stub *shim.MockStub, args [][]byte) {
	res := stub.MockInvoke("1", args)
	if res.Status != shim.OK {
		expectedErr := "Received unknown function invocation"
		actualErr := string(res.Message)
		if !(strings.Contains(actualErr, expectedErr)) {
			fmt.Println("Invoke with unknown function must throw error: ", expectedErr, "\n Actual Error :", actualErr)
			t.FailNow()
		}
	} else {
		t.FailNow()
	}
}

//checkReadTripOK - helper for positive test readTrip
func checkReadTripOK(t *testing.T, stub *shim.MockStub, trpID string) {
	res := stub.MockInvoke("1", [][]byte{[]byte("readTrip"), []byte(trpID)})
	if res.Status != shim.OK {
		fmt.Println("func readTrip with ID: ", trpID, " failed"+string(res.Message))
		t.FailNow()
	}
	if res.Payload == nil {
		fmt.Println("func readTrip with ID: ", trpID, "failed to get value")
		t.FailNow()
	}
	if bytes.Compare(getNewTripExpected(), []byte(res.Payload)) != 0 {
		fmt.Println("func readTrip with ID: ", trpID, "Expected:", string(getNewTripExpected()), "Actual:", string(res.Payload))
		t.FailNow()
	}
}

//checkReadTripNOK - helper for negative testing of readTrip
func checkReadTripNOK(t *testing.T, stub *shim.MockStub, trpID string) {
	//with no trpID
	res := stub.MockInvoke("1", [][]byte{[]byte("readTrip"), []byte("")})
	if res.Status != shim.OK {
		expectedErr := "retrieveTrip: Corrupt trp record "
		actualErr := string(res.Message)
		if !(strings.Contains(actualErr, expectedErr)) {
			fmt.Println("func readTrip negative test: ", "Expected Error:", expectedErr, "Actual Error", actualErr)
			t.FailNow()
		}
	} else {
		t.FailNow()
	}
}

func checkReadAllTripsOK(t *testing.T, stub *shim.MockStub) {
	res := stub.MockInvoke("1", [][]byte{[]byte("readAllTrips")})
	if res.Status != shim.OK {
		fmt.Println("func readAllTrips failed", string(res.Message))
		t.FailNow()
	}
	if res.Payload == nil {
		fmt.Println("func readAllTrips failed to get value")
		t.FailNow()
	}
	if bytes.Compare(getExpectedTrips(), []byte(res.Payload)) != 0 {
		fmt.Println("func readAllTrips Expected:\n", string(getExpectedTrips()), "\nActual:\n", string(res.Payload))
		t.FailNow()
	}
}

func checkError(t *testing.T, exp string, act string) {
	if strings.Compare(exp, act) != 0 {
		fmt.Println("Unexpected Error! Expecting ", exp, "\n Actual :", act)
		t.FailNow()
	}
}
