package main

import (
	"bytes"
	"encoding/json"
	"fmt"
	"strings"
	"testing"

	"github.com/hyperledger/fabric/core/chaincode/shim"
)

//TestCoffeeBeanBagAsset_Init
func TestCoffeeBeanBagAsset_Init(t *testing.T) {
	coffeeBeanBag := new(CoffeeBeanBagAsset)
	stub := shim.NewMockStub("coffeeBeanBag", coffeeBeanBag)
	checkInit(t, stub, [][]byte{[]byte("init")})
	checkState(t, stub, "coffeeBeanBagIDIndex", getExpectedCoffeeBeanBagIDIndex("init"))
}

//TestCoffeeBeanBagAsset_InvokeUnknownFunction
func TestCoffeeBeanBagAsset_InvokeUnknownFunction(t *testing.T) {
	coffeeBeanBag := new(CoffeeBeanBagAsset)
	stub := shim.NewMockStub("coffeeBeanBag", coffeeBeanBag)
	checkInit(t, stub, [][]byte{[]byte("init")})
	checkInvokeUnknownFunction(t, stub, [][]byte{[]byte("myFunction"), []byte("docType:Asset")})
}

//TestCoffeeBeanBagAsset_Invoke_addNewCoffeeBeanBag
func TestCoffeeBeanBagAsset_Invoke_addNewCoffeeBeanBagOK(t *testing.T) {
	coffeeBeanBag := new(CoffeeBeanBagAsset)
	stub := shim.NewMockStub("coffeeBeanBag", coffeeBeanBag)
	checkInit(t, stub, [][]byte{[]byte("init")})
	checkInvoke(t, stub, getFirstCoffeeBeanBagAssetForTesting())
	newCoffeeBeanBagID := "100001"
	checkState(t, stub, newCoffeeBeanBagID, getNewCoffeeBeanBagExpected())
	checkState(t, stub, "coffeeBeanBagIDIndex", getExpectedCoffeeBeanBagIDIndex("addNewCoffeeBeanBag"))
}

//TestCoffeeBeanBagAsset_Invoke_addNewCoffeeBeanBagUnknownField
func TestCoffeeBeanBagAsset_Invoke_addNewCoffeeBeanBagUnknownField(t *testing.T) {
	coffeeBeanBag := new(CoffeeBeanBagAsset)
	stub := shim.NewMockStub("coffeeBeanBag", coffeeBeanBag)
	checkInit(t, stub, [][]byte{[]byte("init")})
	res := stub.MockInvoke("1", getCoffeeBeanBagAssetWithUnknownFieldForTesting())
	if res.Status != shim.OK {
		checkError(t, "CoffeeBeanBag Data is Corrupted", res.Message)
	} else {
		fmt.Println("Unknown Field Error was expected, but not raised")
		t.FailNow()
	}
}

//TestCoffeeBeanBagAsset_Invoke_addNewCoffeeBeanBag
func TestCoffeeBeanBagAsset_Invoke_addNewCoffeeBeanBagDuplicate(t *testing.T) {
	coffeeBeanBag := new(CoffeeBeanBagAsset)
	stub := shim.NewMockStub("coffeeBeanBag", coffeeBeanBag)
	checkInit(t, stub, [][]byte{[]byte("init")})
	checkInvoke(t, stub, getFirstCoffeeBeanBagAssetForTesting())
	newCoffeeBeanBagID := "100001"
	checkState(t, stub, newCoffeeBeanBagID, getNewCoffeeBeanBagExpected())
	checkState(t, stub, "coffeeBeanBagIDIndex", getExpectedCoffeeBeanBagIDIndex("addNewCoffeeBeanBag"))
	res := stub.MockInvoke("1", getFirstCoffeeBeanBagAssetForTesting())
	if res.Status != shim.OK {
		checkError(t, "This CoffeeBeanBag already exists: 100001", res.Message)
	} else {
		fmt.Println("Error was expected, but not raised")
		t.FailNow()
	}
}

//TestCoffeeBeanBagAsset_Invoke_removeCoffeeBeanBagOK  //change template
func TestCoffeeBeanBagAsset_Invoke_removeCoffeeBeanBagOK(t *testing.T) {
	coffeeBeanBag := new(CoffeeBeanBagAsset)
	stub := shim.NewMockStub("coffeeBeanBag", coffeeBeanBag)
	checkInit(t, stub, [][]byte{[]byte("init")})
	checkInvoke(t, stub, getFirstCoffeeBeanBagAssetForTesting())
	checkInvoke(t, stub, getSecondCoffeeBeanBagAssetForTesting())
	checkReadAllCoffeeBeanBagsOK(t, stub)
	checkState(t, stub, "coffeeBeanBagIDIndex", getExpectedCoffeeBeanBagIDIndex("beforeRemoveCoffeeBeanBag"))
	checkInvoke(t, stub, getRemoveSecondCoffeeBeanBagAssetForTesting())
	remainingCoffeeBeanBagID := "100001"
	checkReadCoffeeBeanBagOK(t, stub, remainingCoffeeBeanBagID)
	checkState(t, stub, "coffeeBeanBagIDIndex", getExpectedCoffeeBeanBagIDIndex("afterRemoveCoffeeBeanBag"))
}

//TestCoffeeBeanBagAsset_Invoke_removeCoffeeBeanBagNOK  //change template
func TestCoffeeBeanBagAsset_Invoke_removeCoffeeBeanBagNOK(t *testing.T) {
	coffeeBeanBag := new(CoffeeBeanBagAsset)
	stub := shim.NewMockStub("coffeeBeanBag", coffeeBeanBag)
	checkInit(t, stub, [][]byte{[]byte("init")})
	checkInvoke(t, stub, getFirstCoffeeBeanBagAssetForTesting())
	firstCoffeeBeanBagID := "100001"
	checkReadCoffeeBeanBagOK(t, stub, firstCoffeeBeanBagID)
	checkState(t, stub, "coffeeBeanBagIDIndex", getExpectedCoffeeBeanBagIDIndex("addNewCoffeeBeanBag"))
	res := stub.MockInvoke("1", getRemoveSecondCoffeeBeanBagAssetForTesting())
	if res.Status != shim.OK {
		checkError(t, "CoffeeBeanBag with ID: "+"100002"+" not found", res.Message)
	} else {
		fmt.Println("Error was expected, but not raised")
		t.FailNow()
	}
	checkState(t, stub, "coffeeBeanBagIDIndex", getExpectedCoffeeBeanBagIDIndex("addNewCoffeeBeanBag"))
}

//TestCoffeeBeanBagAsset_Invoke_removeAllCoffeeBeanBagsOK  //change template
func TestCoffeeBeanBagAsset_Invoke_removeAllCoffeeBeanBagsOK(t *testing.T) {
	coffeeBeanBag := new(CoffeeBeanBagAsset)
	stub := shim.NewMockStub("coffeeBeanBag", coffeeBeanBag)
	checkInit(t, stub, [][]byte{[]byte("init")})
	checkInvoke(t, stub, getFirstCoffeeBeanBagAssetForTesting())
	checkInvoke(t, stub, getSecondCoffeeBeanBagAssetForTesting())
	checkReadAllCoffeeBeanBagsOK(t, stub)
	checkState(t, stub, "coffeeBeanBagIDIndex", getExpectedCoffeeBeanBagIDIndex("beforeRemoveCoffeeBeanBag"))
	checkInvoke(t, stub, getRemoveAllCoffeeBeanBagAssetsForTesting())
	checkState(t, stub, "coffeeBeanBagIDIndex", getExpectedCoffeeBeanBagIDIndex(""))
}

//TestCoffeeBeanBagAsset_Invoke_removeCoffeeBeanBagNOK  //change template
func TestCoffeeBeanBagAsset_Invoke_removeAllCoffeeBeanBagsNOK(t *testing.T) {
	coffeeBeanBag := new(CoffeeBeanBagAsset)
	stub := shim.NewMockStub("coffeeBeanBag", coffeeBeanBag)
	checkInit(t, stub, [][]byte{[]byte("init")})
	res := stub.MockInvoke("1", getRemoveAllCoffeeBeanBagAssetsForTesting())
	if res.Status != shim.OK {
		checkError(t, "removeAllCoffeeBeanBags: No coffeeBeanBags to remove", res.Message)
	} else {
		fmt.Println("Error was expected, but not raised")
		t.FailNow()
	}
	checkState(t, stub, "coffeeBeanBagIDIndex", getExpectedCoffeeBeanBagIDIndex(""))
}

//TestCoffeeBeanBagAsset_Query_readCoffeeBeanBag
func TestCoffeeBeanBagAsset_Query_readCoffeeBeanBag(t *testing.T) {
	coffeeBeanBag := new(CoffeeBeanBagAsset)
	stub := shim.NewMockStub("coffeeBeanBag", coffeeBeanBag)
	checkInit(t, stub, [][]byte{[]byte("init")})
	coffeeBeanBagID := "100001"
	checkInvoke(t, stub, getFirstCoffeeBeanBagAssetForTesting())
	checkReadCoffeeBeanBagOK(t, stub, coffeeBeanBagID)
	checkReadCoffeeBeanBagNOK(t, stub, "")
}

//TestCoffeeBeanBagAsset_Query_readAllCoffeeBeanBags
func TestCoffeeBeanBagAsset_Query_readAllCoffeeBeanBags(t *testing.T) {
	coffeeBeanBag := new(CoffeeBeanBagAsset)
	stub := shim.NewMockStub("coffeeBeanBag", coffeeBeanBag)
	checkInit(t, stub, [][]byte{[]byte("init")})
	checkInvoke(t, stub, getFirstCoffeeBeanBagAssetForTesting())
	checkInvoke(t, stub, getSecondCoffeeBeanBagAssetForTesting())
	checkReadAllCoffeeBeanBagsOK(t, stub)
}

/*
*
*	Helper Functions
*
 */
//Get first CoffeeBeanBagAsset for testing
func getFirstCoffeeBeanBagAssetForTesting() [][]byte {
	return [][]byte{[]byte("addNewCoffeeBeanBag"),
		[]byte("{\"ID\":\"100001\",\"docType\":\"Asset.CoffeeBeanBagAsset\",\"weight\":\"100\",\"moisture\":\"50\",\"temperature\":\"30\"}")}
}

//Get CoffeeBeanBagAsset with unknown field for testing
func getCoffeeBeanBagAssetWithUnknownFieldForTesting() [][]byte {
	return [][]byte{[]byte("addNewCoffeeBeanBag"),
		[]byte("{\"ID\":\"100001\",\"docuType\":\"Asset.CoffeeBeanBagAsset\",\"weight\":\"100\",\"moisture\":\"50\",\"temperature\":\"30\"}")}
}

//Get second CoffeeBeanBagAsset for testing
func getSecondCoffeeBeanBagAssetForTesting() [][]byte {
	return [][]byte{[]byte("addNewCoffeeBeanBag"),
		[]byte("{\"ID\":\"100002\",\"docType\":\"Asset.CoffeeBeanBagAsset\",\"weight\":\"200\",\"moisture\":\"25\",\"temperature\":\"35\"}")}
}

//Get remove second CoffeeBeanBagAsset for testing //change template
func getRemoveSecondCoffeeBeanBagAssetForTesting() [][]byte {
	return [][]byte{[]byte("removeCoffeeBeanBag"),
		[]byte("100002")}
}

//Get remove all CoffeeBeanBagAssets for testing //change template
func getRemoveAllCoffeeBeanBagAssetsForTesting() [][]byte {
	return [][]byte{[]byte("removeAllCoffeeBeanBags")}
}

//Get an expected value for testing
func getNewCoffeeBeanBagExpected() []byte {
	var coffeeBeanBag CoffeeBeanBag
	coffeeBeanBag.ID = "100001"
	coffeeBeanBag.ObjectType = "Asset.CoffeeBeanBagAsset"
	coffeeBeanBag.Weight = "100"
	coffeeBeanBag.Moisture = "50"
	coffeeBeanBag.Temperature = "30"
	coffeeBeanBagJSON, err := json.Marshal(coffeeBeanBag)
	if err != nil {
		fmt.Println("Error converting a CoffeeBeanBag record to JSON")
		return nil
	}
	return []byte(coffeeBeanBagJSON)
}

//Get expected values of CoffeeBeanBags for testing
func getExpectedCoffeeBeanBags() []byte {
	var coffeeBeanBags []CoffeeBeanBag
	var coffeeBeanBag CoffeeBeanBag
	coffeeBeanBag.ID = "100001"
	coffeeBeanBag.ObjectType = "Asset.CoffeeBeanBagAsset"
	coffeeBeanBag.Weight = "100"
	coffeeBeanBag.Moisture = "50"
	coffeeBeanBag.Temperature = "30"
	coffeeBeanBags = append(coffeeBeanBags, coffeeBeanBag)
	coffeeBeanBag.ID = "100002"
	coffeeBeanBag.ObjectType = "Asset.CoffeeBeanBagAsset"
	coffeeBeanBag.Weight = "200"
	coffeeBeanBag.Moisture = "25"
	coffeeBeanBag.Temperature = "35"
	coffeeBeanBags = append(coffeeBeanBags, coffeeBeanBag)
	coffeeBeanBagJSON, err := json.Marshal(coffeeBeanBags)
	if err != nil {
		fmt.Println("Error converting coffeeBeanBag records to JSON")
		return nil
	}
	return []byte(coffeeBeanBagJSON)
}

func getExpectedCoffeeBeanBagIDIndex(funcName string) []byte {
	var coffeeBeanBagIDIndex CoffeeBeanBagIDIndex
	switch funcName {
	case "addNewCoffeeBeanBag":
		coffeeBeanBagIDIndex.IDs = append(coffeeBeanBagIDIndex.IDs, "100001")
		coffeeBeanBagIDIndexBytes, err := json.Marshal(coffeeBeanBagIDIndex)
		if err != nil {
			fmt.Println("Error converting CoffeeBeanBagIDIndex to JSON")
			return nil
		}
		return coffeeBeanBagIDIndexBytes
	case "beforeRemoveCoffeeBeanBag":
		coffeeBeanBagIDIndex.IDs = append(coffeeBeanBagIDIndex.IDs, "100001")
		coffeeBeanBagIDIndex.IDs = append(coffeeBeanBagIDIndex.IDs, "100002")
		coffeeBeanBagIDIndexBytes, err := json.Marshal(coffeeBeanBagIDIndex)
		if err != nil {
			fmt.Println("Error converting CoffeeBeanBagIDIndex to JSON")
			return nil
		}
		return coffeeBeanBagIDIndexBytes
	case "afterRemoveCoffeeBeanBag":
		coffeeBeanBagIDIndex.IDs = append(coffeeBeanBagIDIndex.IDs, "100001")
		coffeeBeanBagIDIndexBytes, err := json.Marshal(coffeeBeanBagIDIndex)
		if err != nil {
			fmt.Println("Error converting CoffeeBeanBagIDIndex to JSON")
			return nil
		}
		return coffeeBeanBagIDIndexBytes
	default:
		coffeeBeanBagIDIndexBytes, err := json.Marshal(coffeeBeanBagIDIndex)
		if err != nil {
			fmt.Println("Error converting CoffeeBeanBagIDIndex to JSON")
			return nil
		}
		return coffeeBeanBagIDIndexBytes
	}
}

//checkInit - helper to check the Initialization of chaincode: CoffeeBeanBagAsset
func checkInit(t *testing.T, stub *shim.MockStub, args [][]byte) {
	res := stub.MockInit("1", args)
	if res.Status != shim.OK {
		fmt.Println("Init failed", string(res.Message))
		t.FailNow()
	}
}

//checkState - helper for checking the chaincode state for a given stateKey afgainst an expected value
func checkState(t *testing.T, stub *shim.MockStub, stateKey string, expectedState []byte) {
	actualState := stub.State[stateKey]
	if actualState == nil {
		fmt.Println("State for ", stateKey, ": failed to get value")
		t.FailNow()
	}

	if bytes.Compare(actualState, expectedState) != 0 {
		fmt.Println("Incorrect State for " + stateKey + ": \nExpected: " + string(expectedState) + "\nActual  : " + string(actualState))
		t.FailNow()
	}
}

//checkInvoke - helper for checking Invoke of chaincode
func checkInvoke(t *testing.T, stub *shim.MockStub, args [][]byte) {
	res := stub.MockInvoke("1", args)
	if res.Status != shim.OK {
		fmt.Println("Invoke", args, "failed", string(res.Message))
		t.FailNow()
	}
}

func checkInvokeUnknownFunction(t *testing.T, stub *shim.MockStub, args [][]byte) {
	res := stub.MockInvoke("1", args)
	if res.Status != shim.OK {
		expectedErr := "Received unknown function invocation"
		actualErr := string(res.Message)
		if !(strings.Contains(actualErr, expectedErr)) {
			fmt.Println("Invoke with unknown function must throw error: ", expectedErr, "\n Actual Error :", actualErr)
			t.FailNow()
		}
	} else {
		t.FailNow()
	}
}

//checkReadCoffeeBeanBagOK - helper for positive test readCoffeeBeanBag
func checkReadCoffeeBeanBagOK(t *testing.T, stub *shim.MockStub, coffeeBeanBagID string) {
	res := stub.MockInvoke("1", [][]byte{[]byte("readCoffeeBeanBag"), []byte(coffeeBeanBagID)})
	if res.Status != shim.OK {
		fmt.Println("func readCoffeeBeanBag with ID: ", coffeeBeanBagID, " failed"+string(res.Message))
		t.FailNow()
	}
	if res.Payload == nil {
		fmt.Println("func readCoffeeBeanBag with ID: ", coffeeBeanBagID, "failed to get value")
		t.FailNow()
	}
	if bytes.Compare(getNewCoffeeBeanBagExpected(), []byte(res.Payload)) != 0 {
		fmt.Println("func readCoffeeBeanBag with ID: ", coffeeBeanBagID, "Expected:", string(getNewCoffeeBeanBagExpected()), "Actual:", string(res.Payload))
		t.FailNow()
	}
}

//checkReadCoffeeBeanBagNOK - helper for negative testing of readCoffeeBeanBag
func checkReadCoffeeBeanBagNOK(t *testing.T, stub *shim.MockStub, coffeeBeanBagID string) {
	//with no coffeeBeanBagID
	res := stub.MockInvoke("1", [][]byte{[]byte("readCoffeeBeanBag"), []byte("")})
	if res.Status != shim.OK {
		expectedErr := "retrieveCoffeeBeanBag: Corrupt coffeeBeanBag record "
		actualErr := string(res.Message)
		if !(strings.Contains(actualErr, expectedErr)) {
			fmt.Println("func readCoffeeBeanBag negative test: ", "Expected Error:", expectedErr, "Actual Error", actualErr)
			t.FailNow()
		}
	} else {
		t.FailNow()
	}
}

func checkReadAllCoffeeBeanBagsOK(t *testing.T, stub *shim.MockStub) {
	res := stub.MockInvoke("1", [][]byte{[]byte("readAllCoffeeBeanBags")})
	if res.Status != shim.OK {
		fmt.Println("func readAllCoffeeBeanBags failed", string(res.Message))
		t.FailNow()
	}
	if res.Payload == nil {
		fmt.Println("func readAllCoffeeBeanBags failed to get value")
		t.FailNow()
	}
	if bytes.Compare(getExpectedCoffeeBeanBags(), []byte(res.Payload)) != 0 {
		fmt.Println("func readAllCoffeeBeanBags Expected:\n", string(getExpectedCoffeeBeanBags()), "\nActual:\n", string(res.Payload))
		t.FailNow()
	}
}

func checkError(t *testing.T, exp string, act string) {
	if strings.Compare(exp, act) != 0 {
		fmt.Println("Unexpected Error! Expecting ", exp, "\n Actual :", act)
		t.FailNow()
	}
}
