package main

import (
	"encoding/json"
	"errors"
	"fmt"
	"strings"

	"github.com/hyperledger/fabric/core/chaincode/shim"
	"github.com/hyperledger/fabric/protos/peer"
)

var logger = shim.NewLogger("CLDChaincode")

//CoffeeBeanBagAsset - Chaincode for asset CoffeeBeanBag
type CoffeeBeanBagAsset struct {
}

//CoffeeBeanBag - Details of the asset type CoffeeBeanBag
type CoffeeBeanBag struct {
	ID          string `json:"ID"`
	ObjectType  string `json:"docType"`
	Weight      string `json:"weight"`
	Moisture    string `json:"moisture"`
	Temperature string `json:"temperature"`
}

//CoffeeBeanBagIDIndex - Index on IDs for retrieval all CoffeeBeanBags
type CoffeeBeanBagIDIndex struct {
	IDs []string `json:"IDs"`
}

func main() {
	err := shim.Start(new(CoffeeBeanBagAsset))
	if err != nil {
		fmt.Printf("Error starting CoffeeBeanBagAsset chaincode function main(): %s", err)
	} else {
		fmt.Printf("Starting CoffeeBeanBagAsset chaincode function main() executed successfully")
	}
}

//Init - The chaincode Init function: No  arguments, only initializes a ID array as Index for retrieval of all CoffeeBeanBags
func (cbb *CoffeeBeanBagAsset) Init(stub shim.ChaincodeStubInterface) peer.Response {
	var coffeeBeanBagIDIndex CoffeeBeanBagIDIndex
	record, _ := stub.GetState("coffeeBeanBagIDIndex")
	//chaincode upgrade: Index might already exist, create new only if it does not
	if record == nil {
		bytes, _ := json.Marshal(coffeeBeanBagIDIndex)
		stub.PutState("coffeeBeanBagIDIndex", bytes)
	}
	return shim.Success(nil)
}

//Invoke - The chaincode Invoke function:
func (cbb *CoffeeBeanBagAsset) Invoke(stub shim.ChaincodeStubInterface) peer.Response {
	function, args := stub.GetFunctionAndParameters()
	logger.Debug("function: ", function)
	switch function {
	case "addNewCoffeeBeanBag":
		return cbb.addNewCoffeeBeanBag(stub, args)
	case "removeCoffeeBeanBag":
		return cbb.removeCoffeeBeanBag(stub, args[0])
	case "removeAllCoffeeBeanBags":
		return cbb.removeAllCoffeeBeanBags(stub)
	case "readCoffeeBeanBag":
		return cbb.readCoffeeBeanBag(stub, args[0])
	case "readAllCoffeeBeanBags":
		return cbb.readAllCoffeeBeanBags(stub)
	default:
		return shim.Error("Received unknown function invocation")
	}
}

//Invoke Route: addNewCoffeeBeanBag
func (cbb *CoffeeBeanBagAsset) addNewCoffeeBeanBag(stub shim.ChaincodeStubInterface, args []string) peer.Response {
	coffeeBeanBag, err := getCoffeeBeanBagFromArgs(args)
	if err != nil {
		return shim.Error("CoffeeBeanBag Data is Corrupted")
	}
	coffeeBeanBag.ObjectType = "Asset.CoffeeBeanBagAsset"
	record, err := stub.GetState(coffeeBeanBag.ID)
	if record != nil {
		return shim.Error("This CoffeeBeanBag already exists: " + coffeeBeanBag.ID)
	}
	_, err = cbb.saveCoffeeBeanBag(stub, coffeeBeanBag)
	if err != nil {
		return shim.Error(err.Error())
	}
	_, err = cbb.updateCoffeeBeanBagIDIndex(stub, coffeeBeanBag)
	if err != nil {
		return shim.Error(err.Error())
	}
	return shim.Success(nil)
}

//Invoke Route: removeCoffeeBeanBag
func (cbb *CoffeeBeanBagAsset) removeCoffeeBeanBag(stub shim.ChaincodeStubInterface, coffeeBeanBagID string) peer.Response {
	_, err := cbb.deleteCoffeeBeanBag(stub, coffeeBeanBagID)
	if err != nil {
		return shim.Error(err.Error())
	}
	_, err = cbb.deleteCoffeeBeanBagIDIndex(stub, coffeeBeanBagID)
	if err != nil {
		return shim.Error(err.Error())
	}
	return shim.Success(nil)
}

//Invoke Route: removeAllCoffeeBeanBags
func (cbb *CoffeeBeanBagAsset) removeAllCoffeeBeanBags(stub shim.ChaincodeStubInterface) peer.Response {
	var coffeeBeanBagStructIDs CoffeeBeanBagIDIndex
	bytes, err := stub.GetState("coffeeBeanBagIDIndex")
	if err != nil {
		return shim.Error("removeAllCoffeeBeanBags: Error getting coffeeBeanBagIDIndex array")
	}
	err = json.Unmarshal(bytes, &coffeeBeanBagStructIDs)
	if err != nil {
		return shim.Error("removeAllCoffeeBeanBags: Error unmarshalling coffeeBeanBagIDIndex array JSON")
	}
	if len(coffeeBeanBagStructIDs.IDs) == 0 {
		return shim.Error("removeAllCoffeeBeanBags: No coffeeBeanBags to remove")
	}
	for _, coffeeBeanBagStructID := range coffeeBeanBagStructIDs.IDs {
		_, err = cbb.deleteCoffeeBeanBag(stub, coffeeBeanBagStructID)
		if err != nil {
			return shim.Error("Failed to remove CoffeeBeanBag with ID: " + coffeeBeanBagStructID)
		}
		_, err = cbb.deleteCoffeeBeanBagIDIndex(stub, coffeeBeanBagStructID)
		if err != nil {
			return shim.Error(err.Error())
		}
	}
	cbb.initHolder(stub)
	return shim.Success(nil)
}

//Query Route: readCoffeeBeanBag
func (cbb *CoffeeBeanBagAsset) readCoffeeBeanBag(stub shim.ChaincodeStubInterface, coffeeBeanBagID string) peer.Response {
	coffeeBeanBagAsByteArray, err := cbb.retrieveCoffeeBeanBag(stub, coffeeBeanBagID)
	if err != nil {
		return shim.Error(err.Error())
	}
	return shim.Success(coffeeBeanBagAsByteArray)
}

//Query Route: readAllCoffeeBeanBags
func (cbb *CoffeeBeanBagAsset) readAllCoffeeBeanBags(stub shim.ChaincodeStubInterface) peer.Response {
	var coffeeBeanBagIDs CoffeeBeanBagIDIndex
	bytes, err := stub.GetState("coffeeBeanBagIDIndex")
	if err != nil {
		return shim.Error("readAllCoffeeBeanBags: Error getting coffeeBeanBagIDIndex array")
	}
	err = json.Unmarshal(bytes, &coffeeBeanBagIDs)
	if err != nil {
		return shim.Error("readAllCoffeeBeanBags: Error unmarshalling coffeeBeanBagIDIndex array JSON")
	}
	result := "["

	var coffeeBeanBagAsByteArray []byte

	for _, coffeeBeanBagID := range coffeeBeanBagIDs.IDs {
		coffeeBeanBagAsByteArray, err = cbb.retrieveCoffeeBeanBag(stub, coffeeBeanBagID)
		if err != nil {
			return shim.Error("Failed to retrieve coffeeBeanBag with ID: " + coffeeBeanBagID)
		}
		result += string(coffeeBeanBagAsByteArray) + ","
	}
	if len(result) == 1 {
		result = "[]"
	} else {
		result = result[:len(result)-1] + "]"
	}
	return shim.Success([]byte(result))
}

//Helper: Save CoffeeBeanBagAsset
func (cbb *CoffeeBeanBagAsset) saveCoffeeBeanBag(stub shim.ChaincodeStubInterface, coffeeBeanBag CoffeeBeanBag) (bool, error) {
	bytes, err := json.Marshal(coffeeBeanBag)
	if err != nil {
		return false, errors.New("Error converting coffeeBeanBag record JSON")
	}
	err = stub.PutState(coffeeBeanBag.ID, bytes)
	if err != nil {
		return false, errors.New("Error storing CoffeeBeanBag record")
	}
	return true, nil
}

//Helper: delete CoffeeBeanBagAsset
func (cbb *CoffeeBeanBagAsset) deleteCoffeeBeanBag(stub shim.ChaincodeStubInterface, coffeeBeanBagID string) (bool, error) {
	_, err := cbb.retrieveCoffeeBeanBag(stub, coffeeBeanBagID)
	if err != nil {
		return false, errors.New("CoffeeBeanBag with ID: " + coffeeBeanBagID + " not found")
	}
	err = stub.DelState(coffeeBeanBagID)
	if err != nil {
		return false, errors.New("Error deleting CoffeeBeanBag record")
	}
	return true, nil
}

//Helper: Update coffeeBeanBag Holder - updates Index
func (cbb *CoffeeBeanBagAsset) updateCoffeeBeanBagIDIndex(stub shim.ChaincodeStubInterface, coffeeBeanBag CoffeeBeanBag) (bool, error) {
	var coffeeBeanBagIDs CoffeeBeanBagIDIndex
	bytes, err := stub.GetState("coffeeBeanBagIDIndex")
	if err != nil {
		return false, errors.New("updateCoffeeBeanBagIDIndex: Error getting coffeeBeanBagIDIndex array Index from state")
	}
	err = json.Unmarshal(bytes, &coffeeBeanBagIDs)
	if err != nil {
		return false, errors.New("updateCoffeeBeanBagIDIndex: Error unmarshalling coffeeBeanBagIDIndex array JSON")
	}
	coffeeBeanBagIDs.IDs = append(coffeeBeanBagIDs.IDs, coffeeBeanBag.ID)
	bytes, err = json.Marshal(coffeeBeanBagIDs)
	if err != nil {
		return false, errors.New("updateCoffeeBeanBagIDIndex: Error marshalling new coffeeBeanBag ID")
	}
	err = stub.PutState("coffeeBeanBagIDIndex", bytes)
	if err != nil {
		return false, errors.New("updateCoffeeBeanBagIDIndex: Error storing new coffeeBeanBag ID in coffeeBeanBagIDIndex (Index)")
	}
	return true, nil
}

//Helper: delete ID from coffeeBeanBagStruct Holder
func (cbb *CoffeeBeanBagAsset) deleteCoffeeBeanBagIDIndex(stub shim.ChaincodeStubInterface, coffeeBeanBagID string) (bool, error) {
	var coffeeBeanBagStructIDs CoffeeBeanBagIDIndex
	bytes, err := stub.GetState("coffeeBeanBagIDIndex")
	if err != nil {
		return false, errors.New("deleteCoffeeBeanBagIDIndex: Error getting coffeeBeanBagIDIndex array Index from state")
	}
	err = json.Unmarshal(bytes, &coffeeBeanBagStructIDs)
	if err != nil {
		return false, errors.New("deleteCoffeeBeanBagIDIndex: Error unmarshalling coffeeBeanBagIDIndex array JSON")
	}
	coffeeBeanBagStructIDs.IDs, err = deleteKeyFromStringArray(coffeeBeanBagStructIDs.IDs, coffeeBeanBagID)
	if err != nil {
		return false, errors.New(err.Error())
	}
	bytes, err = json.Marshal(coffeeBeanBagStructIDs)
	if err != nil {
		return false, errors.New("deleteCoffeeBeanBagIDIndex: Error marshalling new coffeeBeanBagStruct ID")
	}
	err = stub.PutState("coffeeBeanBagIDIndex", bytes)
	if err != nil {
		return false, errors.New("deleteCoffeeBeanBagIDIndex: Error storing new coffeeBeanBagStruct ID in coffeeBeanBagIDIndex (Index)")
	}
	return true, nil
}

//Helper: Initialize truck ID Holder //change template
func (cbb *CoffeeBeanBagAsset) initHolder(stub shim.ChaincodeStubInterface) bool {
	var coffeeBeanBagIDIndex CoffeeBeanBagIDIndex
	bytes, _ := json.Marshal(coffeeBeanBagIDIndex)
	stub.DelState("coffeeBeanBagIDIndex")
	stub.PutState("coffeeBeanBagIDIndex", bytes)
	return true
}

//deleteKeyFromArray
func deleteKeyFromStringArray(array []string, key string) (newArray []string, err error) {
	for _, entry := range array {
		if entry != key {
			newArray = append(newArray, entry)
		}
	}
	if len(newArray) == len(array) {
		return newArray, errors.New("Specified Key: " + key + " not found in Array")
	}
	return newArray, nil
}

//Helper: Retrieve purchaser
func (cbb *CoffeeBeanBagAsset) retrieveCoffeeBeanBag(stub shim.ChaincodeStubInterface, coffeeBeanBagID string) ([]byte, error) {
	var coffeeBeanBag CoffeeBeanBag
	var coffeeBeanBagAsByteArray []byte
	bytes, err := stub.GetState(coffeeBeanBagID)
	if err != nil {
		return coffeeBeanBagAsByteArray, errors.New("retrieveCoffeeBeanBag: Error retrieving coffeeBeanBag with ID: " + coffeeBeanBagID)
	}
	err = json.Unmarshal(bytes, &coffeeBeanBag)
	if err != nil {
		return coffeeBeanBagAsByteArray, errors.New("retrieveCoffeeBeanBag: Corrupt coffeeBeanBag record " + string(bytes))
	}
	coffeeBeanBagAsByteArray, err = json.Marshal(coffeeBeanBag)
	if err != nil {
		return coffeeBeanBagAsByteArray, errors.New("readCoffeeBeanBag: Invalid coffeeBeanBag Object - Not a  valid JSON")
	}
	return coffeeBeanBagAsByteArray, nil
}

//getCoffeeBeanBagFromArgs - construct a coffeeBeanBag structure from string array of arguments
func getCoffeeBeanBagFromArgs(args []string) (coffeeBeanBag CoffeeBeanBag, err error) {

	if strings.Contains(args[0], "\"ID\"") == false ||
		strings.Contains(args[0], "\"docType\"") == false ||
		strings.Contains(args[0], "\"weight\"") == false ||
		strings.Contains(args[0], "\"moisture\"") == false ||
		strings.Contains(args[0], "\"temperature\"") == false {
		return coffeeBeanBag, errors.New("Unknown field: Input JSON does not comply to schema")
	}

	err = json.Unmarshal([]byte(args[0]), &coffeeBeanBag)
	if err != nil {
		return coffeeBeanBag, err
	}
	return coffeeBeanBag, nil

}
