# What's in here?
A collection of links to other repositories where the actual UI5 apps.
This Demo has two phases.  So, there are two sets of repositories correspondingly.

# Phase 1
FKOM 2018.  The coffee network apps that were demoed at FKOM 2018 are at the following repositories:

## Setup:
[Setup UI5 Repo](https://github.wdf.sap.corp/I047582/FKOM18_Leo_Setup)

## Touches:
[Touches UI5 Repo](https://github.wdf.sap.corp/I047582/FKOM18_Leo_Touches)

## Trace:
[Trace UI5 Repo](https://github.wdf.sap.corp/I047582/FKOM_Leo_Trace)

# Phase 2:
Leonardo Center Singapore.  The coffee network apps are now part of Leonardo Center at Singapore:

## Setup:
[Setup UI5 Repo](https://github.wdf.sap.corp/I047582/coffeeNetLeo-Setup)

## Touches:
[Touches UI5 Repo](https://github.wdf.sap.corp/I047582/coffeeNetLeo-Touches)

## Trace:
[Trace UI5 Repo](https://github.wdf.sap.corp/I047582/coffeeNetLeo-Trace)



# Steps to deploy
1. Clone into SAP WebIDE for SAP Cloud Platform tenant of your choice.
3. Deploy to SAP Cloud Platform
4. Run
