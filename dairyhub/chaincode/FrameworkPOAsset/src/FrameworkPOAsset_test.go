package main

//Template Version: 1.1-20180704
import (
	"bytes"
	"encoding/json"
	"fmt"
	"strings"
	"testing"

	"github.com/hyperledger/fabric/core/chaincode/shim"
)

//TestFrameworkPOAsset_Init
func TestFrameworkPOAsset_Init(t *testing.T) {
	frameworkPO := new(FrameworkPOAsset)
	stub := shim.NewMockStub("frameworkPO", frameworkPO)
	checkInit(t, stub, [][]byte{[]byte("init")})
	checkState(t, stub, "frameworkPOIDIndex", getExpectedFrameworkPOIDIndex("init"))
}

//TestFrameworkPOAsset_InvokeUnknownFunction
func TestFrameworkPOAsset_InvokeUnknownFunction(t *testing.T) {
	frameworkPO := new(FrameworkPOAsset)
	stub := shim.NewMockStub("frameworkPO", frameworkPO)
	checkInit(t, stub, [][]byte{[]byte("init")})
	checkInvokeUnknownFunction(t, stub, [][]byte{[]byte("myFunction"), []byte("docType:Asset")})
}

//TestFrameworkPOAsset_Invoke_addNewFrameworkPO
func TestFrameworkPOAsset_Invoke_addNewFrameworkPOOK(t *testing.T) {
	frameworkPO := new(FrameworkPOAsset)
	stub := shim.NewMockStub("frameworkPO", frameworkPO)
	checkInit(t, stub, [][]byte{[]byte("init")})
	checkInvoke(t, stub, getFirstFrameworkPOAssetForTesting())
	newFrameworkPOID := "100001"
	checkState(t, stub, newFrameworkPOID, getNewFrameworkPOExpected())
	checkState(t, stub, "frameworkPOIDIndex", getExpectedFrameworkPOIDIndex("addNewFrameworkPO"))
}

//TestFrameworkPOAsset_Invoke_addNewFrameworkPO
func TestFrameworkPOAsset_Invoke_addNewFrameworkPODuplicate(t *testing.T) {
	frameworkPO := new(FrameworkPOAsset)
	stub := shim.NewMockStub("frameworkPO", frameworkPO)
	checkInit(t, stub, [][]byte{[]byte("init")})
	checkInvoke(t, stub, getFirstFrameworkPOAssetForTesting())
	newFrameworkPOID := "100001"
	checkState(t, stub, newFrameworkPOID, getNewFrameworkPOExpected())
	checkState(t, stub, "frameworkPOIDIndex", getExpectedFrameworkPOIDIndex("addNewFrameworkPO"))
	res := stub.MockInvoke("1", getFirstFrameworkPOAssetForTesting())
	if res.Status != shim.OK {
		checkError(t, "This FrameworkPO already exists: 100001", res.Message)
	} else {
		fmt.Println("Error was expected, but not raised")
		t.FailNow()
	}
}

func TestFrameworkPOAsset_Invoke_updateFrameworkPOOK(t *testing.T) {
	FrameworkPO := new(FrameworkPOAsset)
	stub := shim.NewMockStub("FrameworkPO", FrameworkPO)
	checkInit(t, stub, [][]byte{[]byte("init")})
	checkInvoke(t, stub, getFirstFrameworkPOAssetForTesting())
	newFrameworkPOID := "100001"
	checkState(t, stub, newFrameworkPOID, getNewFrameworkPOExpected())
	checkInvoke(t, stub, getFirstFrameworkPOAssetForUpdateTestingOK())
	checkReadFrameworkPOAssetAfterUpdateOK(t, stub, newFrameworkPOID)
}

//TestFrameworkPOAsset_Invoke_removeFrameworkPOOK  //change template
func TestFrameworkPOAsset_Invoke_removeFrameworkPOOK(t *testing.T) {
	frameworkPO := new(FrameworkPOAsset)
	stub := shim.NewMockStub("frameworkPO", frameworkPO)
	checkInit(t, stub, [][]byte{[]byte("init")})
	checkInvoke(t, stub, getFirstFrameworkPOAssetForTesting())
	checkInvoke(t, stub, getSecondFrameworkPOAssetForTesting())
	checkReadAllFrameworkPOsOK(t, stub)
	checkState(t, stub, "frameworkPOIDIndex", getExpectedFrameworkPOIDIndex("beforeRemoveFrameworkPO"))
	checkInvoke(t, stub, getRemoveSecondFrameworkPOAssetForTesting())
	remainingFrameworkPOID := "100001"
	checkReadFrameworkPOOK(t, stub, remainingFrameworkPOID)
	checkState(t, stub, "frameworkPOIDIndex", getExpectedFrameworkPOIDIndex("afterRemoveFrameworkPO"))
}

//TestFrameworkPOAsset_Invoke_removeFrameworkPONOK  //change template
func TestFrameworkPOAsset_Invoke_removeFrameworkPONOK(t *testing.T) {
	frameworkPO := new(FrameworkPOAsset)
	stub := shim.NewMockStub("frameworkPO", frameworkPO)
	checkInit(t, stub, [][]byte{[]byte("init")})
	checkInvoke(t, stub, getFirstFrameworkPOAssetForTesting())
	firstFrameworkPOID := "100001"
	checkReadFrameworkPOOK(t, stub, firstFrameworkPOID)
	checkState(t, stub, "frameworkPOIDIndex", getExpectedFrameworkPOIDIndex("addNewFrameworkPO"))
	res := stub.MockInvoke("1", getRemoveSecondFrameworkPOAssetForTesting())
	if res.Status != shim.OK {
		checkError(t, "FrameworkPO with ID: "+"100002"+" not found", res.Message)
	} else {
		fmt.Println("Error was expected, but not raised")
		t.FailNow()
	}
	checkState(t, stub, "frameworkPOIDIndex", getExpectedFrameworkPOIDIndex("addNewFrameworkPO"))
}

//TestFrameworkPOAsset_Invoke_removeAllFrameworkPOsOK  //change template
func TestFrameworkPOAsset_Invoke_removeAllFrameworkPOsOK(t *testing.T) {
	frameworkPO := new(FrameworkPOAsset)
	stub := shim.NewMockStub("frameworkPO", frameworkPO)
	checkInit(t, stub, [][]byte{[]byte("init")})
	checkInvoke(t, stub, getFirstFrameworkPOAssetForTesting())
	checkInvoke(t, stub, getSecondFrameworkPOAssetForTesting())
	checkReadAllFrameworkPOsOK(t, stub)
	checkState(t, stub, "frameworkPOIDIndex", getExpectedFrameworkPOIDIndex("beforeRemoveFrameworkPO"))
	checkInvoke(t, stub, getRemoveAllFrameworkPOAssetsForTesting())
	checkState(t, stub, "frameworkPOIDIndex", getExpectedFrameworkPOIDIndex(""))
}

//TestFrameworkPOAsset_Invoke_removeFrameworkPONOK  //change template
func TestFrameworkPOAsset_Invoke_removeAllFrameworkPOsNOK(t *testing.T) {
	frameworkPO := new(FrameworkPOAsset)
	stub := shim.NewMockStub("frameworkPO", frameworkPO)
	checkInit(t, stub, [][]byte{[]byte("init")})
	res := stub.MockInvoke("1", getRemoveAllFrameworkPOAssetsForTesting())
	if res.Status != shim.OK {
		checkError(t, "removeAllFrameworkPOs: No frameworkPOs to remove", res.Message)
	} else {
		fmt.Println("Error was expected, but not raised")
		t.FailNow()
	}
	checkState(t, stub, "frameworkPOIDIndex", getExpectedFrameworkPOIDIndex(""))
}

//TestFrameworkPOAsset_Query_readFrameworkPO
func TestFrameworkPOAsset_Query_readFrameworkPO(t *testing.T) {
	frameworkPO := new(FrameworkPOAsset)
	stub := shim.NewMockStub("frameworkPO", frameworkPO)
	checkInit(t, stub, [][]byte{[]byte("init")})
	frameworkPOID := "100001"
	checkInvoke(t, stub, getFirstFrameworkPOAssetForTesting())
	checkReadFrameworkPOOK(t, stub, frameworkPOID)
	checkReadFrameworkPONOK(t, stub, "")
}

//TestFrameworkPOAsset_Query_readAllFrameworkPOs
func TestFrameworkPOAsset_Query_readAllFrameworkPOs(t *testing.T) {
	frameworkPO := new(FrameworkPOAsset)
	stub := shim.NewMockStub("frameworkPO", frameworkPO)
	checkInit(t, stub, [][]byte{[]byte("init")})
	checkInvoke(t, stub, getFirstFrameworkPOAssetForTesting())
	checkInvoke(t, stub, getSecondFrameworkPOAssetForTesting())
	checkReadAllFrameworkPOsOK(t, stub)
}

/*
*
*	Helper Functions
*
 */
//Get first FrameworkPOAsset for testing
func getFirstFrameworkPOAssetForTesting() [][]byte {
	return [][]byte{[]byte("addNewFrameworkPO"),
		[]byte("{\"ID\":\"100001\",\"docType\":\"Asset.FrameworkPOAsset\",\"status\":\"0\",\"creationDate\":\"12/01/2018\", \"startDate\":\"startDate001\", \"endDate\":\"endDate001\", \"price\":\"price001\", \"priceUnit\":\"priceUnit001\", \"dairyType\":\"dairyType001\", \"dtsScore\":\"dtsScore001\", \"volumeLimit\":\"volumeLimit001\", \"buyerID\":\"buyerID001\", \"sellerID\":\"sellerID001\"}")}
}

//Get first FrameworkPOAsset for testing
func getFirstFrameworkPOAssetForUpdateTestingOK() [][]byte {
	return [][]byte{[]byte("updateFrameworkPO"),
		[]byte("{\"ID\":\"100001\",\"docType\":\"Asset.FrameworkPOAsset\",\"status\":\"1\",\"creationDate\":\"12/01/2018\", \"startDate\":\"startDate001\", \"endDate\":\"endDate001\", \"price\":\"price001\", \"priceUnit\":\"priceUnit001\", \"dairyType\":\"dairyType001\", \"dtsScore\":\"dtsScore001\", \"volumeLimit\":\"volumeLimit001\", \"buyerID\":\"buyerID001\", \"sellerID\":\"sellerID001\"}")}
}

//Get second FrameworkPOAsset for testing
func getSecondFrameworkPOAssetForTesting() [][]byte {
	return [][]byte{[]byte("addNewFrameworkPO"),
		[]byte("{\"ID\":\"100002\",\"docType\":\"Asset.FrameworkPOAsset\",\"status\":\"0\",\"creationDate\":\"12/01/2018\", \"startDate\":\"startDate002\", \"endDate\":\"endDate002\", \"price\":\"price002\", \"priceUnit\":\"priceUnit002\", \"dairyType\":\"dairyType002\", \"dtsScore\":\"dtsScore002\", \"volumeLimit\":\"volumeLimit002\", \"buyerID\":\"buyerID002\", \"sellerID\":\"sellerID002\"}")}
}

//Get remove second FrameworkPOAsset for testing //change template
func getRemoveSecondFrameworkPOAssetForTesting() [][]byte {
	return [][]byte{[]byte("removeFrameworkPO"),
		[]byte("100002")}
}

//Get remove all FrameworkPOAssets for testing //change template
func getRemoveAllFrameworkPOAssetsForTesting() [][]byte {
	return [][]byte{[]byte("removeAllFrameworkPOs")}
}

//Get an expected value for testing
func getNewFrameworkPOExpected() []byte {
	var frameworkPO FrameworkPO
	frameworkPO.ID = "100001"
	frameworkPO.ObjectType = "Asset.FrameworkPOAsset"
	frameworkPO.Status = "0"
	frameworkPO.CreationDate = "12/01/2018"
	frameworkPO.StartDate = "startDate001"
	frameworkPO.EndDate = "endDate001"
	frameworkPO.Price = "price001"
	frameworkPO.PriceUnit = "priceUnit001"
	frameworkPO.DairyType = "dairyType001"
	frameworkPO.DTSScore = "dtsScore001"
	frameworkPO.VolumeLimit = "volumeLimit001"
	frameworkPO.BuyerID = "buyerID001"
	frameworkPO.SellerID = "sellerID001"
	frameworkPOJSON, err := json.Marshal(frameworkPO)
	if err != nil {
		fmt.Println("Error converting a FrameworkPO record to JSON")
		return nil
	}
	return []byte(frameworkPOJSON)
}

//Get an expected value for testing
func getUpdatedFrameworkPOExpected() []byte {
	var frameworkPO FrameworkPO
	frameworkPO.ID = "100001"
	frameworkPO.ObjectType = "Asset.FrameworkPOAsset"
	frameworkPO.Status = "1"
	frameworkPO.CreationDate = "12/01/2018"
	frameworkPO.StartDate = "startDate001"
	frameworkPO.EndDate = "endDate001"
	frameworkPO.Price = "price001"
	frameworkPO.PriceUnit = "priceUnit001"
	frameworkPO.DairyType = "dairyType001"
	frameworkPO.DTSScore = "dtsScore001"
	frameworkPO.VolumeLimit = "volumeLimit001"
	frameworkPO.BuyerID = "buyerID001"
	frameworkPO.SellerID = "sellerID001"
	frameworkPOJSON, err := json.Marshal(frameworkPO)
	if err != nil {
		fmt.Println("Error converting a FrameworkPO record to JSON")
		return nil
	}
	return []byte(frameworkPOJSON)
}

//Get expected values of FrameworkPOs for testing
func getExpectedFrameworkPOs() []byte {
	var frameworkPOs []FrameworkPO
	var frameworkPO FrameworkPO
	frameworkPO.ID = "100001"
	frameworkPO.ObjectType = "Asset.FrameworkPOAsset"
	frameworkPO.Status = "0"
	frameworkPO.CreationDate = "12/01/2018"
	frameworkPO.StartDate = "startDate001"
	frameworkPO.EndDate = "endDate001"
	frameworkPO.Price = "price001"
	frameworkPO.PriceUnit = "priceUnit001"
	frameworkPO.DairyType = "dairyType001"
	frameworkPO.DTSScore = "dtsScore001"
	frameworkPO.VolumeLimit = "volumeLimit001"
	frameworkPO.BuyerID = "buyerID001"
	frameworkPO.SellerID = "sellerID001"
	frameworkPOs = append(frameworkPOs, frameworkPO)
	frameworkPO.ID = "100002"
	frameworkPO.ObjectType = "Asset.FrameworkPOAsset"
	frameworkPO.Status = "0"
	frameworkPO.CreationDate = "12/01/2018"
	frameworkPO.StartDate = "startDate002"
	frameworkPO.EndDate = "endDate002"
	frameworkPO.Price = "price002"
	frameworkPO.PriceUnit = "priceUnit002"
	frameworkPO.DairyType = "dairyType002"
	frameworkPO.DTSScore = "dtsScore002"
	frameworkPO.VolumeLimit = "volumeLimit002"
	frameworkPO.BuyerID = "buyerID002"
	frameworkPO.SellerID = "sellerID002"
	frameworkPOs = append(frameworkPOs, frameworkPO)
	frameworkPOJSON, err := json.Marshal(frameworkPOs)
	if err != nil {
		fmt.Println("Error converting frameworkPO records to JSON")
		return nil
	}
	return []byte(frameworkPOJSON)
}

func getExpectedFrameworkPOIDIndex(funcName string) []byte {
	var frameworkPOIDIndex FrameworkPOIDIndex
	switch funcName {
	case "addNewFrameworkPO":
		frameworkPOIDIndex.IDs = append(frameworkPOIDIndex.IDs, "100001")
		frameworkPOIDIndexBytes, err := json.Marshal(frameworkPOIDIndex)
		if err != nil {
			fmt.Println("Error converting FrameworkPOIDIndex to JSON")
			return nil
		}
		return frameworkPOIDIndexBytes
	case "beforeRemoveFrameworkPO":
		frameworkPOIDIndex.IDs = append(frameworkPOIDIndex.IDs, "100001")
		frameworkPOIDIndex.IDs = append(frameworkPOIDIndex.IDs, "100002")
		frameworkPOIDIndexBytes, err := json.Marshal(frameworkPOIDIndex)
		if err != nil {
			fmt.Println("Error converting FrameworkPOIDIndex to JSON")
			return nil
		}
		return frameworkPOIDIndexBytes
	case "afterRemoveFrameworkPO":
		frameworkPOIDIndex.IDs = append(frameworkPOIDIndex.IDs, "100001")
		frameworkPOIDIndexBytes, err := json.Marshal(frameworkPOIDIndex)
		if err != nil {
			fmt.Println("Error converting FrameworkPOIDIndex to JSON")
			return nil
		}
		return frameworkPOIDIndexBytes
	default:
		frameworkPOIDIndexBytes, err := json.Marshal(frameworkPOIDIndex)
		if err != nil {
			fmt.Println("Error converting FrameworkPOIDIndex to JSON")
			return nil
		}
		return frameworkPOIDIndexBytes
	}
}

//checkInit - helper to check the Initialization of chaincode: FrameworkPOAsset
func checkInit(t *testing.T, stub *shim.MockStub, args [][]byte) {
	res := stub.MockInit("1", args)
	if res.Status != shim.OK {
		fmt.Println("Init failed", string(res.Message))
		t.FailNow()
	}
}

//checkState - helper for checking the chaincode state for a given stateKey afgainst an expected value
func checkState(t *testing.T, stub *shim.MockStub, stateKey string, expectedState []byte) {
	actualState := stub.State[stateKey]
	if actualState == nil {
		fmt.Println("State for ", stateKey, ": failed to get value")
		t.FailNow()
	}

	if bytes.Compare(actualState, expectedState) != 0 {
		fmt.Println("Incorrect State for " + stateKey + ": \nExpected: " + string(expectedState) + "\nActual  : " + string(actualState))
		t.FailNow()
	}
}

//checkInvoke - helper for checking Invoke of chaincode
func checkInvoke(t *testing.T, stub *shim.MockStub, args [][]byte) {
	res := stub.MockInvoke("1", args)
	if res.Status != shim.OK {
		fmt.Println("Invoke", args, "failed", string(res.Message))
		t.FailNow()
	}
}

func checkInvokeUnknownFunction(t *testing.T, stub *shim.MockStub, args [][]byte) {
	res := stub.MockInvoke("1", args)
	if res.Status != shim.OK {
		expectedErr := "Received unknown function invocation"
		actualErr := string(res.Message)
		if !(strings.Contains(actualErr, expectedErr)) {
			fmt.Println("Invoke with unknown function must throw error: ", expectedErr, "\n Actual Error :", actualErr)
			t.FailNow()
		}
	} else {
		t.FailNow()
	}
}

//checkReadFrameworkPOAssetAfterUpdateOK - helper for positive test readFrameworkPO after update
func checkReadFrameworkPOAssetAfterUpdateOK(t *testing.T, stub *shim.MockStub, frameworkPOID string) {
	res := stub.MockInvoke("1", [][]byte{[]byte("readFrameworkPO"), []byte(frameworkPOID)})
	if res.Status != shim.OK {
		fmt.Println("func readFrameworkPO with ID: ", frameworkPOID, " failed"+string(res.Message))
		t.FailNow()
	}
	if res.Payload == nil {
		fmt.Println("func readFrameworkPO with ID: ", frameworkPOID, "failed to get value")
		t.FailNow()
	}
	if bytes.Compare(getUpdatedFrameworkPOExpected(), []byte(res.Payload)) != 0 {
		fmt.Println("func readFrameworkPO with ID: ", frameworkPOID, "Expected:", string(getUpdatedFrameworkPOExpected()), "Actual:", string(res.Payload))
		t.FailNow()
	}
}

//checkReadFrameworkPOOK - helper for positive test readFrameworkPO
func checkReadFrameworkPOOK(t *testing.T, stub *shim.MockStub, frameworkPOID string) {
	res := stub.MockInvoke("1", [][]byte{[]byte("readFrameworkPO"), []byte(frameworkPOID)})
	if res.Status != shim.OK {
		fmt.Println("func readFrameworkPO with ID: ", frameworkPOID, " failed"+string(res.Message))
		t.FailNow()
	}
	if res.Payload == nil {
		fmt.Println("func readFrameworkPO with ID: ", frameworkPOID, "failed to get value")
		t.FailNow()
	}
	if bytes.Compare(getNewFrameworkPOExpected(), []byte(res.Payload)) != 0 {
		fmt.Println("func readFrameworkPO with ID: ", frameworkPOID, "Expected:", string(getNewFrameworkPOExpected()), "Actual:", string(res.Payload))
		t.FailNow()
	}
}

//checkReadFrameworkPONOK - helper for negative testing of readFrameworkPO
func checkReadFrameworkPONOK(t *testing.T, stub *shim.MockStub, frameworkPOID string) {
	//with no frameworkPOID
	res := stub.MockInvoke("1", [][]byte{[]byte("readFrameworkPO"), []byte("")})
	if res.Status != shim.OK {
		expectedErr := "retrieveFrameworkPO: Corrupt frameworkPO record "
		actualErr := string(res.Message)
		if !(strings.Contains(actualErr, expectedErr)) {
			fmt.Println("func readFrameworkPO negative test: ", "Expected Error:", expectedErr, "Actual Error", actualErr)
			t.FailNow()
		}
	} else {
		t.FailNow()
	}
}

func checkReadAllFrameworkPOsOK(t *testing.T, stub *shim.MockStub) {
	res := stub.MockInvoke("1", [][]byte{[]byte("readAllFrameworkPOs")})
	if res.Status != shim.OK {
		fmt.Println("func readAllFrameworkPOs failed", string(res.Message))
		t.FailNow()
	}
	if res.Payload == nil {
		fmt.Println("func readAllFrameworkPOs failed to get value")
		t.FailNow()
	}
	if bytes.Compare(getExpectedFrameworkPOs(), []byte(res.Payload)) != 0 {
		fmt.Println("func readAllFrameworkPOs Expected:\n", string(getExpectedFrameworkPOs()), "\nActual:\n", string(res.Payload))
		t.FailNow()
	}
}

func checkError(t *testing.T, exp string, act string) {
	if strings.Compare(exp, act) != 0 {
		fmt.Println("Unexpected Error! Expecting ", exp, "\n Actual :", act)
		t.FailNow()
	}
}
