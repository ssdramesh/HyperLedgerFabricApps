package main

//Template Version: 1.1-20180704
import (
	"encoding/json"
	"errors"
	"fmt"

	"github.com/hyperledger/fabric/core/chaincode/shim"
	"github.com/hyperledger/fabric/protos/peer"
	"github.com/tidwall/gjson"
)

var logger = shim.NewLogger("CLDChaincode")

//FrameworkPOAsset - Chaincode for asset FrameworkPO
type FrameworkPOAsset struct {
}

//FrameworkPO - Details of the asset type FrameworkPO
type FrameworkPO struct {
	ID           string `json:"ID"`
	ObjectType   string `json:"docType"`
	Status       string `json:"status"`
	CreationDate string `json:"creationDate"`
	StartDate    string `json:"startDate"`
	EndDate      string `json:"endDate"`
	Price        string `json:"price"`
	PriceUnit    string `json:"priceUnit"`
	DairyType    string `json:"dairyType"`
	DTSScore     string `json:"dtsScore"`
	VolumeLimit  string `json:"volumeLimit"`
	BuyerID      string `json:"buyerID"`
	SellerID     string `json:"sellerID"`
}

//FrameworkPOIDIndex - Index on IDs for retrieval all FrameworkPOs
type FrameworkPOIDIndex struct {
	IDs []string `json:"IDs"`
}

func main() {
	err := shim.Start(new(FrameworkPOAsset))
	if err != nil {
		fmt.Printf("Error starting FrameworkPOAsset chaincode function main(): %s", err)
	} else {
		fmt.Printf("Starting FrameworkPOAsset chaincode function main() executed successfully")
	}
}

//Init - The chaincode Init function: No  arguments, only initializes a ID array as Index for retrieval of all FrameworkPOs
func (fpo *FrameworkPOAsset) Init(stub shim.ChaincodeStubInterface) peer.Response {
	var frameworkPOIDIndex FrameworkPOIDIndex
	record, _ := stub.GetState("frameworkPOIDIndex")
	//chaincode upgrade: Index might already exist, create new only if it does not
	if record == nil {
		bytes, _ := json.Marshal(frameworkPOIDIndex)
		stub.PutState("frameworkPOIDIndex", bytes)
	}
	return shim.Success(nil)
}

//Invoke - The chaincode Invoke function:
func (fpo *FrameworkPOAsset) Invoke(stub shim.ChaincodeStubInterface) peer.Response {
	function, args := stub.GetFunctionAndParameters()
	logger.Debug("function: ", function)
	switch function {
	case "addNewFrameworkPO":
		return fpo.addNewFrameworkPO(stub, args)
	case "updateFrameworkPO":
		return fpo.updateFrameworkPO(stub, args)
	case "removeFrameworkPO":
		return fpo.removeFrameworkPO(stub, args[0])
	case "removeAllFrameworkPOs":
		return fpo.removeAllFrameworkPOs(stub)
	case "readFrameworkPO":
		return fpo.readFrameworkPO(stub, args[0])
	case "readAllFrameworkPOs":
		return fpo.readAllFrameworkPOs(stub)
	default:
		return shim.Error("Received unknown function invocation")
	}
}

//Invoke Route: addNewFrameworkPO
func (fpo *FrameworkPOAsset) addNewFrameworkPO(stub shim.ChaincodeStubInterface, args []string) peer.Response {
	frameworkPO, err := getFrameworkPOFromArgs(args)
	if err != nil {
		return shim.Error("FrameworkPO Data is Corrupted")
	}
	frameworkPO.ObjectType = "Asset.FrameworkPOAsset"
	record, err := stub.GetState(frameworkPO.ID)
	if record != nil {
		return shim.Error("This FrameworkPO already exists: " + frameworkPO.ID)
	}
	_, err = fpo.saveFrameworkPO(stub, frameworkPO)
	if err != nil {
		return shim.Error(err.Error())
	}
	_, err = fpo.updateFrameworkPOIDIndex(stub, frameworkPO)
	if err != nil {
		return shim.Error(err.Error())
	}
	return shim.Success(nil)
}

//Invoke Route: updateFrameowrkPO
func (fpo *FrameworkPOAsset) updateFrameworkPO(stub shim.ChaincodeStubInterface, args []string) peer.Response {

	input, err := getFrameworkPOFromArgs(args)
	if err != nil {
		return shim.Error("FrameowrkPO Data is Corrupted")
	}
	record, err := stub.GetState(input.ID)
	if record == nil {
		return shim.Error("This FrameworkPO does not exist: " + input.ID)
	}
	_, err = fpo.saveFrameworkPO(stub, input)
	if err != nil {
		return shim.Error(err.Error())
	}
	return shim.Success(nil)
}

//Invoke Route: removeFrameworkPO
func (fpo *FrameworkPOAsset) removeFrameworkPO(stub shim.ChaincodeStubInterface, frameworkPOID string) peer.Response {
	_, err := fpo.deleteFrameworkPO(stub, frameworkPOID)
	if err != nil {
		return shim.Error(err.Error())
	}
	_, err = fpo.deleteFrameworkPOIDIndex(stub, frameworkPOID)
	if err != nil {
		return shim.Error(err.Error())
	}
	return shim.Success(nil)
}

//Invoke Route: removeAllFrameworkPOs
func (fpo *FrameworkPOAsset) removeAllFrameworkPOs(stub shim.ChaincodeStubInterface) peer.Response {
	var frameworkPOIDIndex FrameworkPOIDIndex
	bytes, err := stub.GetState("frameworkPOIDIndex")
	if err != nil {
		return shim.Error("removeAllFrameworkPOs: Error getting frameworkPOIDIndex array")
	}
	err = json.Unmarshal(bytes, &frameworkPOIDIndex)
	if err != nil {
		return shim.Error("removeAllFrameworkPOs: Error unmarshalling frameworkPOIDIndex array JSON")
	}
	if len(frameworkPOIDIndex.IDs) == 0 {
		return shim.Error("removeAllFrameworkPOs: No frameworkPOs to remove")
	}
	for _, frameworkPOStructID := range frameworkPOIDIndex.IDs {
		_, err = fpo.deleteFrameworkPO(stub, frameworkPOStructID)
		if err != nil {
			return shim.Error("Failed to remove FrameworkPO with ID: " + frameworkPOStructID)
		}
		_, err = fpo.deleteFrameworkPOIDIndex(stub, frameworkPOStructID)
		if err != nil {
			return shim.Error(err.Error())
		}
	}
	fpo.initHolder(stub)
	return shim.Success(nil)
}

//Query Route: readFrameworkPO
func (fpo *FrameworkPOAsset) readFrameworkPO(stub shim.ChaincodeStubInterface, frameworkPOID string) peer.Response {
	frameworkPOAsByteArray, err := fpo.retrieveFrameworkPO(stub, frameworkPOID)
	if err != nil {
		return shim.Error(err.Error())
	}
	return shim.Success(frameworkPOAsByteArray)
}

//Query Route: readAllFrameworkPOs
func (fpo *FrameworkPOAsset) readAllFrameworkPOs(stub shim.ChaincodeStubInterface) peer.Response {
	var frameworkPOIDs FrameworkPOIDIndex
	bytes, err := stub.GetState("frameworkPOIDIndex")
	if err != nil {
		return shim.Error("readAllFrameworkPOs: Error getting frameworkPOIDIndex array")
	}
	err = json.Unmarshal(bytes, &frameworkPOIDs)
	if err != nil {
		return shim.Error("readAllFrameworkPOs: Error unmarshalling frameworkPOIDIndex array JSON")
	}
	result := "["

	var frameworkPOAsByteArray []byte

	for _, frameworkPOID := range frameworkPOIDs.IDs {
		frameworkPOAsByteArray, err = fpo.retrieveFrameworkPO(stub, frameworkPOID)
		if err != nil {
			return shim.Error("Failed to retrieve frameworkPO with ID: " + frameworkPOID)
		}
		result += string(frameworkPOAsByteArray) + ","
	}
	if len(result) == 1 {
		result = "[]"
	} else {
		result = result[:len(result)-1] + "]"
	}
	return shim.Success([]byte(result))
}

//Helper: Save FrameworkPOAsset
func (fpo *FrameworkPOAsset) saveFrameworkPO(stub shim.ChaincodeStubInterface, frameworkPO FrameworkPO) (bool, error) {
	bytes, err := json.Marshal(frameworkPO)
	if err != nil {
		return false, errors.New("Error converting frameworkPO record JSON")
	}
	err = stub.PutState(frameworkPO.ID, bytes)
	if err != nil {
		return false, errors.New("Error storing FrameworkPO record")
	}
	return true, nil
}

//Helper: delete FrameworkPOAsset
func (fpo *FrameworkPOAsset) deleteFrameworkPO(stub shim.ChaincodeStubInterface, frameworkPOID string) (bool, error) {
	_, err := fpo.retrieveFrameworkPO(stub, frameworkPOID)
	if err != nil {
		return false, errors.New("FrameworkPO with ID: " + frameworkPOID + " not found")
	}
	err = stub.DelState(frameworkPOID)
	if err != nil {
		return false, errors.New("Error deleting FrameworkPO record")
	}
	return true, nil
}

//Helper: Update frameworkPO Holder - updates Index
func (fpo *FrameworkPOAsset) updateFrameworkPOIDIndex(stub shim.ChaincodeStubInterface, frameworkPO FrameworkPO) (bool, error) {
	var frameworkPOIDs FrameworkPOIDIndex
	bytes, err := stub.GetState("frameworkPOIDIndex")
	if err != nil {
		return false, errors.New("updateFrameworkPOIDIndex: Error getting frameworkPOIDIndex array Index from state")
	}
	err = json.Unmarshal(bytes, &frameworkPOIDs)
	if err != nil {
		return false, errors.New("updateFrameworkPOIDIndex: Error unmarshalling frameworkPOIDIndex array JSON")
	}
	frameworkPOIDs.IDs = append(frameworkPOIDs.IDs, frameworkPO.ID)
	bytes, err = json.Marshal(frameworkPOIDs)
	if err != nil {
		return false, errors.New("updateFrameworkPOIDIndex: Error marshalling new frameworkPO ID")
	}
	err = stub.PutState("frameworkPOIDIndex", bytes)
	if err != nil {
		return false, errors.New("updateFrameworkPOIDIndex: Error storing new frameworkPO ID in frameworkPOIDIndex (Index)")
	}
	return true, nil
}

//Helper: delete ID from frameworkPOStruct Holder
func (fpo *FrameworkPOAsset) deleteFrameworkPOIDIndex(stub shim.ChaincodeStubInterface, frameworkPOID string) (bool, error) {
	var frameworkPOIDIndex FrameworkPOIDIndex
	bytes, err := stub.GetState("frameworkPOIDIndex")
	if err != nil {
		return false, errors.New("deleteFrameworkPOIDIndex: Error getting frameworkPOIDIndex array Index from state")
	}
	err = json.Unmarshal(bytes, &frameworkPOIDIndex)
	if err != nil {
		return false, errors.New("deleteFrameworkPOIDIndex: Error unmarshalling frameworkPOIDIndex array JSON")
	}
	frameworkPOIDIndex.IDs, err = deleteKeyFromStringArray(frameworkPOIDIndex.IDs, frameworkPOID)
	if err != nil {
		return false, errors.New(err.Error())
	}
	bytes, err = json.Marshal(frameworkPOIDIndex)
	if err != nil {
		return false, errors.New("deleteFrameworkPOIDIndex: Error marshalling new frameworkPOStruct ID")
	}
	err = stub.PutState("frameworkPOIDIndex", bytes)
	if err != nil {
		return false, errors.New("deleteFrameworkPOIDIndex: Error storing new frameworkPOStruct ID in frameworkPOIDIndex (Index)")
	}
	return true, nil
}

//Helper: Initialize truck ID Holder //change template
func (fpo *FrameworkPOAsset) initHolder(stub shim.ChaincodeStubInterface) bool {
	var frameworkPOIDIndex FrameworkPOIDIndex
	bytes, _ := json.Marshal(frameworkPOIDIndex)
	stub.DelState("frameworkPOIDIndex")
	stub.PutState("frameworkPOIDIndex", bytes)
	return true
}

//deleteKeyFromArray
func deleteKeyFromStringArray(array []string, key string) (newArray []string, err error) {
	for _, entry := range array {
		if entry != key {
			newArray = append(newArray, entry)
		}
	}
	if len(newArray) == len(array) {
		return newArray, errors.New("Specified Key: " + key + " not found in Array")
	}
	return newArray, nil
}

//Helper: Retrieve purchaser
func (fpo *FrameworkPOAsset) retrieveFrameworkPO(stub shim.ChaincodeStubInterface, frameworkPOID string) ([]byte, error) {
	var frameworkPO FrameworkPO
	var frameworkPOAsByteArray []byte
	bytes, err := stub.GetState(frameworkPOID)
	if err != nil {
		return frameworkPOAsByteArray, errors.New("retrieveFrameworkPO: Error retrieving frameworkPO with ID: " + frameworkPOID)
	}
	err = json.Unmarshal(bytes, &frameworkPO)
	if err != nil {
		return frameworkPOAsByteArray, errors.New("retrieveFrameworkPO: Corrupt frameworkPO record " + string(bytes))
	}
	frameworkPOAsByteArray, err = json.Marshal(frameworkPO)
	if err != nil {
		return frameworkPOAsByteArray, errors.New("readFrameworkPO: Invalid frameworkPO Object - Not a  valid JSON")
	}
	return frameworkPOAsByteArray, nil
}

//getFrameworkPOFromArgs - construct a frameworkPO structure from string array of arguments
func getFrameworkPOFromArgs(args []string) (frameworkPO FrameworkPO, err error) {

	if !gjson.Valid(args[0]) {
		return frameworkPO, errors.New("Invalid json")
	}

	err = gjson.Unmarshal([]byte(args[0]), &frameworkPO)
	if err != nil {
		return frameworkPO, err
	}
	return frameworkPO, nil
}
