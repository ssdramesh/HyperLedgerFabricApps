package main

//Template Version: 1.1-20180704
import (
	"bytes"
	"encoding/json"
	"fmt"
	"strings"
	"testing"

	"github.com/hyperledger/fabric/core/chaincode/shim"
)

//TestTransactionAsset_Init
func TestTransactionAsset_Init(t *testing.T) {
	transaction := new(TransactionAsset)
	stub := shim.NewMockStub("transaction", transaction)
	checkInit(t, stub, [][]byte{[]byte("init")})
	checkState(t, stub, "transactionIDIndex", getExpectedTransactionIDIndex("init"))
}

//TestTransactionAsset_InvokeUnknownFunction
func TestTransactionAsset_InvokeUnknownFunction(t *testing.T) {
	transaction := new(TransactionAsset)
	stub := shim.NewMockStub("transaction", transaction)
	checkInit(t, stub, [][]byte{[]byte("init")})
	checkInvokeUnknownFunction(t, stub, [][]byte{[]byte("myFunction"), []byte("docType:Asset")})
}

//TestTransactionAsset_Invoke_addNewTransaction
func TestTransactionAsset_Invoke_addNewTransactionOK(t *testing.T) {
	transaction := new(TransactionAsset)
	stub := shim.NewMockStub("transaction", transaction)
	checkInit(t, stub, [][]byte{[]byte("init")})
	checkInvoke(t, stub, getFirstTransactionAssetForTesting())
	newTransactionID := "100001"
	checkState(t, stub, newTransactionID, getNewTransactionExpected())
	checkState(t, stub, "transactionIDIndex", getExpectedTransactionIDIndex("addNewTransaction"))
}

//TestTransactionAsset_Invoke_addNewTransaction
func TestTransactionAsset_Invoke_addNewTransactionDuplicate(t *testing.T) {
	transaction := new(TransactionAsset)
	stub := shim.NewMockStub("transaction", transaction)
	checkInit(t, stub, [][]byte{[]byte("init")})
	checkInvoke(t, stub, getFirstTransactionAssetForTesting())
	newTransactionID := "100001"
	checkState(t, stub, newTransactionID, getNewTransactionExpected())
	checkState(t, stub, "transactionIDIndex", getExpectedTransactionIDIndex("addNewTransaction"))
	res := stub.MockInvoke("1", getFirstTransactionAssetForTesting())
	if res.Status != shim.OK {
		checkError(t, "This Transaction already exists: 100001", res.Message)
	} else {
		fmt.Println("Error was expected, but not raised")
		t.FailNow()
	}
}

func TestTransactionAsset_Invoke_updateTransactionOK(t *testing.T) {
	transaction := new(TransactionAsset)
	stub := shim.NewMockStub("transaction", transaction)
	checkInit(t, stub, [][]byte{[]byte("init")})
	checkInvoke(t, stub, getFirstTransactionAssetForTesting())
	newTransactionID := "100001"
	checkState(t, stub, newTransactionID, getNewTransactionExpected())
	checkInvoke(t, stub, getFirstTransactionAssetForUpdateTestingOK())
	checkReadTransactionAssetAfterUpdateOK(t, stub, newTransactionID)
}

//TestTransactionAsset_Invoke_removeTransactionOK  //change template
func TestTransactionAsset_Invoke_removeTransactionOK(t *testing.T) {
	transaction := new(TransactionAsset)
	stub := shim.NewMockStub("transaction", transaction)
	checkInit(t, stub, [][]byte{[]byte("init")})
	checkInvoke(t, stub, getFirstTransactionAssetForTesting())
	checkInvoke(t, stub, getSecondTransactionAssetForTesting())
	checkReadAllTransactionsOK(t, stub)
	checkState(t, stub, "transactionIDIndex", getExpectedTransactionIDIndex("beforeRemoveTransaction"))
	checkInvoke(t, stub, getRemoveSecondTransactionAssetForTesting())
	remainingTransactionID := "100001"
	checkReadTransactionOK(t, stub, remainingTransactionID)
	checkState(t, stub, "transactionIDIndex", getExpectedTransactionIDIndex("afterRemoveTransaction"))
}

//TestTransactionAsset_Invoke_removeTransactionNOK  //change template
func TestTransactionAsset_Invoke_removeTransactionNOK(t *testing.T) {
	transaction := new(TransactionAsset)
	stub := shim.NewMockStub("transaction", transaction)
	checkInit(t, stub, [][]byte{[]byte("init")})
	checkInvoke(t, stub, getFirstTransactionAssetForTesting())
	firstTransactionID := "100001"
	checkReadTransactionOK(t, stub, firstTransactionID)
	checkState(t, stub, "transactionIDIndex", getExpectedTransactionIDIndex("addNewTransaction"))
	res := stub.MockInvoke("1", getRemoveSecondTransactionAssetForTesting())
	if res.Status != shim.OK {
		checkError(t, "Transaction with ID: "+"100002"+" not found", res.Message)
	} else {
		fmt.Println("Error was expected, but not raised")
		t.FailNow()
	}
	checkState(t, stub, "transactionIDIndex", getExpectedTransactionIDIndex("addNewTransaction"))
}

//TestTransactionAsset_Invoke_removeAllTransactionsOK  //change template
func TestTransactionAsset_Invoke_removeAllTransactionsOK(t *testing.T) {
	transaction := new(TransactionAsset)
	stub := shim.NewMockStub("transaction", transaction)
	checkInit(t, stub, [][]byte{[]byte("init")})
	checkInvoke(t, stub, getFirstTransactionAssetForTesting())
	checkInvoke(t, stub, getSecondTransactionAssetForTesting())
	checkReadAllTransactionsOK(t, stub)
	checkState(t, stub, "transactionIDIndex", getExpectedTransactionIDIndex("beforeRemoveTransaction"))
	checkInvoke(t, stub, getRemoveAllTransactionAssetsForTesting())
	checkState(t, stub, "transactionIDIndex", getExpectedTransactionIDIndex(""))
}

//TestTransactionAsset_Invoke_removeTransactionNOK  //change template
func TestTransactionAsset_Invoke_removeAllTransactionsNOK(t *testing.T) {
	transaction := new(TransactionAsset)
	stub := shim.NewMockStub("transaction", transaction)
	checkInit(t, stub, [][]byte{[]byte("init")})
	res := stub.MockInvoke("1", getRemoveAllTransactionAssetsForTesting())
	if res.Status != shim.OK {
		checkError(t, "removeAllTransactions: No transactions to remove", res.Message)
	} else {
		fmt.Println("Error was expected, but not raised")
		t.FailNow()
	}
	checkState(t, stub, "transactionIDIndex", getExpectedTransactionIDIndex(""))
}

//TestTransactionAsset_Query_readTransaction
func TestTransactionAsset_Query_readTransaction(t *testing.T) {
	transaction := new(TransactionAsset)
	stub := shim.NewMockStub("transaction", transaction)
	checkInit(t, stub, [][]byte{[]byte("init")})
	transactionID := "100001"
	checkInvoke(t, stub, getFirstTransactionAssetForTesting())
	checkReadTransactionOK(t, stub, transactionID)
	checkReadTransactionNOK(t, stub, "")
}

//TestTransactionAsset_Query_readAllTransactions
func TestTransactionAsset_Query_readAllTransactions(t *testing.T) {
	transaction := new(TransactionAsset)
	stub := shim.NewMockStub("transaction", transaction)
	checkInit(t, stub, [][]byte{[]byte("init")})
	checkInvoke(t, stub, getFirstTransactionAssetForTesting())
	checkInvoke(t, stub, getSecondTransactionAssetForTesting())
	checkReadAllTransactionsOK(t, stub)
}

/*
*
*	Helper Functions
*
 */
//Get first TransactionAsset for testing
func getFirstTransactionAssetForTesting() [][]byte {
	return [][]byte{[]byte("addNewTransaction"),
		[]byte("{\"ID\":\"100001\",\"docType\":\"Asset.TransactionAsset\",\"status\":\"0\",\"creationDate\":\"12/01/2018\", \"frameworkPOID\":\"frameworkPOID001\", \"dealType\":\"dealType001\", \"swapType\":\"swapType001\", \"volume\":\"volume001\", \"dtsScore\":\"dtsScore001\", \"swappedByProduct\":\"swappedByProduct001\", \"swappedToProduct\":\"swappedToProduct001\", \"swapFromLocation\":\"swapFromLocation001\", \"swapToLocation\":\"swapToLocation001\", \"swapDate\":\"09/14/2018\", \"swapFutureDate\":\"12/14/2018\", \"buyerID\":\"buyerID001\", \"sellerID\":\"sellerID001\"}")}
}

//Get first TransactionAsset for update testing
func getFirstTransactionAssetForUpdateTestingOK() [][]byte {
	return [][]byte{[]byte("updateTransaction"),
		[]byte("{\"ID\":\"100001\",\"docType\":\"Asset.TransactionAsset\",\"status\":\"1\",\"creationDate\":\"12/01/2018\", \"frameworkPOID\":\"frameworkPOID001\", \"dealType\":\"dealType001\", \"swapType\":\"swapType001\", \"volume\":\"volume001\", \"dtsScore\":\"dtsScore001\", \"swappedByProduct\":\"swappedByProduct001\", \"swappedToProduct\":\"swappedToProduct001\", \"swapFromLocation\":\"swapFromLocation001\", \"swapToLocation\":\"swapToLocation001\", \"swapDate\":\"09/14/2018\", \"swapFutureDate\":\"12/14/2018\", \"buyerID\":\"buyerID001\", \"sellerID\":\"sellerID001\"}")}
}

//Get second TransactionAsset for testing
func getSecondTransactionAssetForTesting() [][]byte {
	return [][]byte{[]byte("addNewTransaction"),
		[]byte("{\"ID\":\"100002\",\"docType\":\"Asset.TransactionAsset\",\"status\":\"0\",\"creationDate\":\"12/01/2018\", \"frameworkPOID\":\"frameworkPOID002\", \"dealType\":\"dealType002\", \"swapType\":\"swapType002\", \"volume\":\"volume002\", \"dtsScore\":\"dtsScore002\", \"swappedByProduct\":\"swappedByProduct002\", \"swappedToProduct\":\"swappedToProduct002\", \"swapFromLocation\":\"swapFromLocation002\", \"swapToLocation\":\"swapToLocation002\",  \"swapDate\":\"09/14/2018\", \"swapFutureDate\":\"12/14/2018\", \"buyerID\":\"buyerID002\", \"sellerID\":\"sellerID002\"}")}
}

//Get remove second TransactionAsset for testing //change template
func getRemoveSecondTransactionAssetForTesting() [][]byte {
	return [][]byte{[]byte("removeTransaction"),
		[]byte("100002")}
}

//Get remove all TransactionAssets for testing //change template
func getRemoveAllTransactionAssetsForTesting() [][]byte {
	return [][]byte{[]byte("removeAllTransactions")}
}

//Get an expected value for testing
func getNewTransactionExpected() []byte {
	var transaction Transaction
	transaction.ID = "100001"
	transaction.ObjectType = "Asset.TransactionAsset"
	transaction.Status = "0"
	transaction.CreationDate = "12/01/2018"
	transaction.FrameworkPOID = "frameworkPOID001"
	transaction.DealType = "dealType001"
	transaction.SwapType = "swapType001"
	transaction.Volume = "volume001"
	transaction.DTSScore = "dtsScore001"
	transaction.SwappedByProduct = "swappedByProduct001"
	transaction.SwappedToProduct = "swappedToProduct001"
	transaction.SwapFromLocation = "swapFromLocation001"
	transaction.SwapToLocation = "swapToLocation001"
	transaction.SwapDate = "09/14/2018"
	transaction.SwapFutureDate = "12/14/2018"
	transaction.BuyerID = "buyerID001"
	transaction.SellerID = "sellerID001"
	transactionJSON, err := json.Marshal(transaction)
	if err != nil {
		fmt.Println("Error converting a Transaction record to JSON")
		return nil
	}
	return []byte(transactionJSON)
}

//Get an expected value for testing
func getUpdatedTransactionExpected() []byte {
	var transaction Transaction
	transaction.ID = "100001"
	transaction.ObjectType = "Asset.TransactionAsset"
	transaction.Status = "1"
	transaction.CreationDate = "12/01/2018"
	transaction.FrameworkPOID = "frameworkPOID001"
	transaction.DealType = "dealType001"
	transaction.SwapType = "swapType001"
	transaction.Volume = "volume001"
	transaction.DTSScore = "dtsScore001"
	transaction.SwappedByProduct = "swappedByProduct001"
	transaction.SwappedToProduct = "swappedToProduct001"
	transaction.SwapFromLocation = "swapFromLocation001"
	transaction.SwapToLocation = "swapToLocation001"
	transaction.SwapDate = "09/14/2018"
	transaction.SwapFutureDate = "12/14/2018"
	transaction.BuyerID = "buyerID001"
	transaction.SellerID = "sellerID001"
	transactionJSON, err := json.Marshal(transaction)
	if err != nil {
		fmt.Println("Error converting a Transaction record to JSON")
		return nil
	}
	return []byte(transactionJSON)
}

//Get expected values of Transactions for testing
func getExpectedTransactions() []byte {
	var transactions []Transaction
	var transaction Transaction
	transaction.ID = "100001"
	transaction.ObjectType = "Asset.TransactionAsset"
	transaction.Status = "0"
	transaction.CreationDate = "12/01/2018"
	transaction.FrameworkPOID = "frameworkPOID001"
	transaction.DealType = "dealType001"
	transaction.SwapType = "swapType001"
	transaction.Volume = "volume001"
	transaction.DTSScore = "dtsScore001"
	transaction.SwappedByProduct = "swappedByProduct001"
	transaction.SwappedToProduct = "swappedToProduct001"
	transaction.SwapFromLocation = "swapFromLocation001"
	transaction.SwapToLocation = "swapToLocation001"
	transaction.SwapDate = "09/14/2018"
	transaction.SwapFutureDate = "12/14/2018"
	transaction.BuyerID = "buyerID001"
	transaction.SellerID = "sellerID001"
	transactions = append(transactions, transaction)
	transaction.ID = "100002"
	transaction.ObjectType = "Asset.TransactionAsset"
	transaction.Status = "0"
	transaction.CreationDate = "12/01/2018"
	transaction.FrameworkPOID = "frameworkPOID002"
	transaction.DealType = "dealType002"
	transaction.SwapType = "swapType002"
	transaction.Volume = "volume002"
	transaction.DTSScore = "dtsScore002"
	transaction.SwappedByProduct = "swappedByProduct002"
	transaction.SwappedToProduct = "swappedToProduct002"
	transaction.SwapFromLocation = "swapFromLocation002"
	transaction.SwapToLocation = "swapToLocation002"
	transaction.SwapDate = "09/14/2018"
	transaction.SwapFutureDate = "12/14/2018"
	transaction.BuyerID = "buyerID002"
	transaction.SellerID = "sellerID002"
	transactions = append(transactions, transaction)
	transactionJSON, err := json.Marshal(transactions)
	if err != nil {
		fmt.Println("Error converting transaction records to JSON")
		return nil
	}
	return []byte(transactionJSON)
}

func getExpectedTransactionIDIndex(funcName string) []byte {
	var transactionIDIndex TransactionIDIndex
	switch funcName {
	case "addNewTransaction":
		transactionIDIndex.IDs = append(transactionIDIndex.IDs, "100001")
		transactionIDIndexBytes, err := json.Marshal(transactionIDIndex)
		if err != nil {
			fmt.Println("Error converting TransactionIDIndex to JSON")
			return nil
		}
		return transactionIDIndexBytes
	case "beforeRemoveTransaction":
		transactionIDIndex.IDs = append(transactionIDIndex.IDs, "100001")
		transactionIDIndex.IDs = append(transactionIDIndex.IDs, "100002")
		transactionIDIndexBytes, err := json.Marshal(transactionIDIndex)
		if err != nil {
			fmt.Println("Error converting TransactionIDIndex to JSON")
			return nil
		}
		return transactionIDIndexBytes
	case "afterRemoveTransaction":
		transactionIDIndex.IDs = append(transactionIDIndex.IDs, "100001")
		transactionIDIndexBytes, err := json.Marshal(transactionIDIndex)
		if err != nil {
			fmt.Println("Error converting TransactionIDIndex to JSON")
			return nil
		}
		return transactionIDIndexBytes
	default:
		transactionIDIndexBytes, err := json.Marshal(transactionIDIndex)
		if err != nil {
			fmt.Println("Error converting TransactionIDIndex to JSON")
			return nil
		}
		return transactionIDIndexBytes
	}
}

//checkInit - helper to check the Initialization of chaincode: TransactionAsset
func checkInit(t *testing.T, stub *shim.MockStub, args [][]byte) {
	res := stub.MockInit("1", args)
	if res.Status != shim.OK {
		fmt.Println("Init failed", string(res.Message))
		t.FailNow()
	}
}

//checkState - helper for checking the chaincode state for a given stateKey afgainst an expected value
func checkState(t *testing.T, stub *shim.MockStub, stateKey string, expectedState []byte) {
	actualState := stub.State[stateKey]
	if actualState == nil {
		fmt.Println("State for ", stateKey, ": failed to get value")
		t.FailNow()
	}

	if bytes.Compare(actualState, expectedState) != 0 {
		fmt.Println("Incorrect State for " + stateKey + ": \nExpected: " + string(expectedState) + "\nActual  : " + string(actualState))
		t.FailNow()
	}
}

//checkInvoke - helper for checking Invoke of chaincode
func checkInvoke(t *testing.T, stub *shim.MockStub, args [][]byte) {
	res := stub.MockInvoke("1", args)
	if res.Status != shim.OK {
		fmt.Println("Invoke", args, "failed", string(res.Message))
		t.FailNow()
	}
}

func checkInvokeUnknownFunction(t *testing.T, stub *shim.MockStub, args [][]byte) {
	res := stub.MockInvoke("1", args)
	if res.Status != shim.OK {
		expectedErr := "Received unknown function invocation"
		actualErr := string(res.Message)
		if !(strings.Contains(actualErr, expectedErr)) {
			fmt.Println("Invoke with unknown function must throw error: ", expectedErr, "\n Actual Error :", actualErr)
			t.FailNow()
		}
	} else {
		t.FailNow()
	}
}

//checkReadTransactionOK - helper for positive test readTransaction
func checkReadTransactionOK(t *testing.T, stub *shim.MockStub, transactionID string) {
	res := stub.MockInvoke("1", [][]byte{[]byte("readTransaction"), []byte(transactionID)})
	if res.Status != shim.OK {
		fmt.Println("func readTransaction with ID: ", transactionID, " failed"+string(res.Message))
		t.FailNow()
	}
	if res.Payload == nil {
		fmt.Println("func readTransaction with ID: ", transactionID, "failed to get value")
		t.FailNow()
	}
	if bytes.Compare(getNewTransactionExpected(), []byte(res.Payload)) != 0 {
		fmt.Println("func readTransaction with ID: ", transactionID, "Expected:", string(getNewTransactionExpected()), "Actual:", string(res.Payload))
		t.FailNow()
	}
}

//checkReadTransactionAssetAfterUpdateOK - helper for positive test readTransaction after update
func checkReadTransactionAssetAfterUpdateOK(t *testing.T, stub *shim.MockStub, transactionID string) {
	res := stub.MockInvoke("1", [][]byte{[]byte("readTransaction"), []byte(transactionID)})
	if res.Status != shim.OK {
		fmt.Println("func readTransaction with ID: ", transactionID, " failed"+string(res.Message))
		t.FailNow()
	}
	if res.Payload == nil {
		fmt.Println("func readTransaction with ID: ", transactionID, "failed to get value")
		t.FailNow()
	}
	if bytes.Compare(getUpdatedTransactionExpected(), []byte(res.Payload)) != 0 {
		fmt.Println("func readTransaction with ID: ", transactionID, "Expected:", string(getUpdatedTransactionExpected()), "Actual:", string(res.Payload))
		t.FailNow()
	}
}

//checkReadTransactionNOK - helper for negative testing of readTransaction
func checkReadTransactionNOK(t *testing.T, stub *shim.MockStub, transactionID string) {
	//with no transactionID
	res := stub.MockInvoke("1", [][]byte{[]byte("readTransaction"), []byte("")})
	if res.Status != shim.OK {
		expectedErr := "retrieveTransaction: Corrupt transaction record "
		actualErr := string(res.Message)
		if !(strings.Contains(actualErr, expectedErr)) {
			fmt.Println("func readTransaction negative test: ", "Expected Error:", expectedErr, "Actual Error", actualErr)
			t.FailNow()
		}
	} else {
		t.FailNow()
	}
}

func checkReadAllTransactionsOK(t *testing.T, stub *shim.MockStub) {
	res := stub.MockInvoke("1", [][]byte{[]byte("readAllTransactions")})
	if res.Status != shim.OK {
		fmt.Println("func readAllTransactions failed", string(res.Message))
		t.FailNow()
	}
	if res.Payload == nil {
		fmt.Println("func readAllTransactions failed to get value")
		t.FailNow()
	}
	if bytes.Compare(getExpectedTransactions(), []byte(res.Payload)) != 0 {
		fmt.Println("func readAllTransactions Expected:\n", string(getExpectedTransactions()), "\nActual:\n", string(res.Payload))
		t.FailNow()
	}
}

func checkError(t *testing.T, exp string, act string) {
	if strings.Compare(exp, act) != 0 {
		fmt.Println("Unexpected Error! Expecting ", exp, "\n Actual :", act)
		t.FailNow()
	}
}
