package main

//Template Version: 1.1-20180704
import (
	"encoding/json"
	"errors"
	"fmt"

	"github.com/hyperledger/fabric/core/chaincode/shim"
	"github.com/hyperledger/fabric/protos/peer"
	"github.com/tidwall/gjson"
)

var logger = shim.NewLogger("CLDChaincode")

//TransactionAsset - Chaincode for asset Transaction
type TransactionAsset struct {
}

//Transaction - Details of the asset type Transaction
type Transaction struct {
	ID               string `json:"ID"`
	ObjectType       string `json:"docType"`
	Status           string `json:"status"`
	CreationDate     string `json:"creationDate"`
	FrameworkPOID    string `json:"frameworkPOID"`
	DealType         string `json:"dealType"`
	SwapType         string `json:"swapType"`
	Volume           string `json:"volume"`
	DTSScore         string `json:"dtsScore"`
	SwappedByProduct string `json:"swappedByProduct"`
	SwappedToProduct string `json:"swappedToProduct"`
	SwapFromLocation string `json:"swapFromLocation"`
	SwapToLocation   string `json:"swapToLocation"`
	SwapDate         string `json:"swapDate"`
	SwapFutureDate   string `json:"swapFutureDate"`
	BuyerID          string `json:"buyerID"`
	SellerID         string `json:"sellerID"`
}

//TransactionIDIndex - Index on IDs for retrieval all Transactions
type TransactionIDIndex struct {
	IDs []string `json:"IDs"`
}

func main() {
	err := shim.Start(new(TransactionAsset))
	if err != nil {
		fmt.Printf("Error starting TransactionAsset chaincode function main(): %s", err)
	} else {
		fmt.Printf("Starting TransactionAsset chaincode function main() executed successfully")
	}
}

//Init - The chaincode Init function: No  arguments, only initializes a ID array as Index for retrieval of all Transactions
func (txn *TransactionAsset) Init(stub shim.ChaincodeStubInterface) peer.Response {
	var transactionIDIndex TransactionIDIndex
	record, _ := stub.GetState("transactionIDIndex")
	//chaincode upgrade: Index might already exist, create new only if it does not
	if record == nil {
		bytes, _ := json.Marshal(transactionIDIndex)
		stub.PutState("transactionIDIndex", bytes)
	}
	return shim.Success(nil)
}

//Invoke - The chaincode Invoke function:
func (txn *TransactionAsset) Invoke(stub shim.ChaincodeStubInterface) peer.Response {
	function, args := stub.GetFunctionAndParameters()
	logger.Debug("function: ", function)
	switch function {
	case "addNewTransaction":
		return txn.addNewTransaction(stub, args)
	case "updateTransaction":
		return txn.updateTransaction(stub, args)
	case "removeTransaction":
		return txn.removeTransaction(stub, args[0])
	case "removeAllTransactions":
		return txn.removeAllTransactions(stub)
	case "readTransaction":
		return txn.readTransaction(stub, args[0])
	case "readAllTransactions":
		return txn.readAllTransactions(stub)
	default:
		return shim.Error("Received unknown function invocation")
	}
}

//Invoke Route: addNewTransaction
func (txn *TransactionAsset) addNewTransaction(stub shim.ChaincodeStubInterface, args []string) peer.Response {
	transaction, err := getTransactionFromArgs(args)
	if err != nil {
		return shim.Error("Transaction Data is Corrupted")
	}
	transaction.ObjectType = "Asset.TransactionAsset"
	record, err := stub.GetState(transaction.ID)
	if record != nil {
		return shim.Error("This Transaction already exists: " + transaction.ID)
	}
	_, err = txn.saveTransaction(stub, transaction)
	if err != nil {
		return shim.Error(err.Error())
	}
	_, err = txn.updateTransactionIDIndex(stub, transaction)
	if err != nil {
		return shim.Error(err.Error())
	}
	return shim.Success(nil)
}

//Invoke Route: updateTransaction
func (txn *TransactionAsset) updateTransaction(stub shim.ChaincodeStubInterface, args []string) peer.Response {

	input, err := getTransactionFromArgs(args)
	if err != nil {
		return shim.Error("Transaction Data is Corrupted")
	}
	record, err := stub.GetState(input.ID)
	if record == nil {
		return shim.Error("This Transaction does not exist: " + input.ID)
	}
	_, err = txn.saveTransaction(stub, input)
	if err != nil {
		return shim.Error(err.Error())
	}
	return shim.Success(nil)
}

//Invoke Route: removeTransaction
func (txn *TransactionAsset) removeTransaction(stub shim.ChaincodeStubInterface, transactionID string) peer.Response {
	_, err := txn.deleteTransaction(stub, transactionID)
	if err != nil {
		return shim.Error(err.Error())
	}
	_, err = txn.deleteTransactionIDIndex(stub, transactionID)
	if err != nil {
		return shim.Error(err.Error())
	}
	return shim.Success(nil)
}

//Invoke Route: removeAllTransactions
func (txn *TransactionAsset) removeAllTransactions(stub shim.ChaincodeStubInterface) peer.Response {
	var transactionIDIndex TransactionIDIndex
	bytes, err := stub.GetState("transactionIDIndex")
	if err != nil {
		return shim.Error("removeAllTransactions: Error getting transactionIDIndex array")
	}
	err = json.Unmarshal(bytes, &transactionIDIndex)
	if err != nil {
		return shim.Error("removeAllTransactions: Error unmarshalling transactionIDIndex array JSON")
	}
	if len(transactionIDIndex.IDs) == 0 {
		return shim.Error("removeAllTransactions: No transactions to remove")
	}
	for _, transactionStructID := range transactionIDIndex.IDs {
		_, err = txn.deleteTransaction(stub, transactionStructID)
		if err != nil {
			return shim.Error("Failed to remove Transaction with ID: " + transactionStructID)
		}
		_, err = txn.deleteTransactionIDIndex(stub, transactionStructID)
		if err != nil {
			return shim.Error(err.Error())
		}
	}
	txn.initHolder(stub)
	return shim.Success(nil)
}

//Query Route: readTransaction
func (txn *TransactionAsset) readTransaction(stub shim.ChaincodeStubInterface, transactionID string) peer.Response {
	transactionAsByteArray, err := txn.retrieveTransaction(stub, transactionID)
	if err != nil {
		return shim.Error(err.Error())
	}
	return shim.Success(transactionAsByteArray)
}

//Query Route: readAllTransactions
func (txn *TransactionAsset) readAllTransactions(stub shim.ChaincodeStubInterface) peer.Response {
	var transactionIDs TransactionIDIndex
	bytes, err := stub.GetState("transactionIDIndex")
	if err != nil {
		return shim.Error("readAllTransactions: Error getting transactionIDIndex array")
	}
	err = json.Unmarshal(bytes, &transactionIDs)
	if err != nil {
		return shim.Error("readAllTransactions: Error unmarshalling transactionIDIndex array JSON")
	}
	result := "["

	var transactionAsByteArray []byte

	for _, transactionID := range transactionIDs.IDs {
		transactionAsByteArray, err = txn.retrieveTransaction(stub, transactionID)
		if err != nil {
			return shim.Error("Failed to retrieve transaction with ID: " + transactionID)
		}
		result += string(transactionAsByteArray) + ","
	}
	if len(result) == 1 {
		result = "[]"
	} else {
		result = result[:len(result)-1] + "]"
	}
	return shim.Success([]byte(result))
}

//Helper: Save TransactionAsset
func (txn *TransactionAsset) saveTransaction(stub shim.ChaincodeStubInterface, transaction Transaction) (bool, error) {
	bytes, err := json.Marshal(transaction)
	if err != nil {
		return false, errors.New("Error converting transaction record JSON")
	}
	err = stub.PutState(transaction.ID, bytes)
	if err != nil {
		return false, errors.New("Error storing Transaction record")
	}
	return true, nil
}

//Helper: delete TransactionAsset
func (txn *TransactionAsset) deleteTransaction(stub shim.ChaincodeStubInterface, transactionID string) (bool, error) {
	_, err := txn.retrieveTransaction(stub, transactionID)
	if err != nil {
		return false, errors.New("Transaction with ID: " + transactionID + " not found")
	}
	err = stub.DelState(transactionID)
	if err != nil {
		return false, errors.New("Error deleting Transaction record")
	}
	return true, nil
}

//Helper: Update transaction Holder - updates Index
func (txn *TransactionAsset) updateTransactionIDIndex(stub shim.ChaincodeStubInterface, transaction Transaction) (bool, error) {
	var transactionIDs TransactionIDIndex
	bytes, err := stub.GetState("transactionIDIndex")
	if err != nil {
		return false, errors.New("updateTransactionIDIndex: Error getting transactionIDIndex array Index from state")
	}
	err = json.Unmarshal(bytes, &transactionIDs)
	if err != nil {
		return false, errors.New("updateTransactionIDIndex: Error unmarshalling transactionIDIndex array JSON")
	}
	transactionIDs.IDs = append(transactionIDs.IDs, transaction.ID)
	bytes, err = json.Marshal(transactionIDs)
	if err != nil {
		return false, errors.New("updateTransactionIDIndex: Error marshalling new transaction ID")
	}
	err = stub.PutState("transactionIDIndex", bytes)
	if err != nil {
		return false, errors.New("updateTransactionIDIndex: Error storing new transaction ID in transactionIDIndex (Index)")
	}
	return true, nil
}

//Helper: delete ID from transactionStruct Holder
func (txn *TransactionAsset) deleteTransactionIDIndex(stub shim.ChaincodeStubInterface, transactionID string) (bool, error) {
	var transactionIDIndex TransactionIDIndex
	bytes, err := stub.GetState("transactionIDIndex")
	if err != nil {
		return false, errors.New("deleteTransactionIDIndex: Error getting transactionIDIndex array Index from state")
	}
	err = json.Unmarshal(bytes, &transactionIDIndex)
	if err != nil {
		return false, errors.New("deleteTransactionIDIndex: Error unmarshalling transactionIDIndex array JSON")
	}
	transactionIDIndex.IDs, err = deleteKeyFromStringArray(transactionIDIndex.IDs, transactionID)
	if err != nil {
		return false, errors.New(err.Error())
	}
	bytes, err = json.Marshal(transactionIDIndex)
	if err != nil {
		return false, errors.New("deleteTransactionIDIndex: Error marshalling new transactionStruct ID")
	}
	err = stub.PutState("transactionIDIndex", bytes)
	if err != nil {
		return false, errors.New("deleteTransactionIDIndex: Error storing new transactionStruct ID in transactionIDIndex (Index)")
	}
	return true, nil
}

//Helper: Initialize truck ID Holder //change template
func (txn *TransactionAsset) initHolder(stub shim.ChaincodeStubInterface) bool {
	var transactionIDIndex TransactionIDIndex
	bytes, _ := json.Marshal(transactionIDIndex)
	stub.DelState("transactionIDIndex")
	stub.PutState("transactionIDIndex", bytes)
	return true
}

//deleteKeyFromArray
func deleteKeyFromStringArray(array []string, key string) (newArray []string, err error) {
	for _, entry := range array {
		if entry != key {
			newArray = append(newArray, entry)
		}
	}
	if len(newArray) == len(array) {
		return newArray, errors.New("Specified Key: " + key + " not found in Array")
	}
	return newArray, nil
}

//Helper: Retrieve purchaser
func (txn *TransactionAsset) retrieveTransaction(stub shim.ChaincodeStubInterface, transactionID string) ([]byte, error) {
	var transaction Transaction
	var transactionAsByteArray []byte
	bytes, err := stub.GetState(transactionID)
	if err != nil {
		return transactionAsByteArray, errors.New("retrieveTransaction: Error retrieving transaction with ID: " + transactionID)
	}
	err = json.Unmarshal(bytes, &transaction)
	if err != nil {
		return transactionAsByteArray, errors.New("retrieveTransaction: Corrupt transaction record " + string(bytes))
	}
	transactionAsByteArray, err = json.Marshal(transaction)
	if err != nil {
		return transactionAsByteArray, errors.New("readTransaction: Invalid transaction Object - Not a  valid JSON")
	}
	return transactionAsByteArray, nil
}

//getTransactionFromArgs - construct a transaction structure from string array of arguments
func getTransactionFromArgs(args []string) (transaction Transaction, err error) {

	if !gjson.Valid(args[0]) {
		return transaction, errors.New("Invalid json")
	}

	err = gjson.Unmarshal([]byte(args[0]), &transaction)
	if err != nil {
		return transaction, err
	}
	return transaction, nil
}
