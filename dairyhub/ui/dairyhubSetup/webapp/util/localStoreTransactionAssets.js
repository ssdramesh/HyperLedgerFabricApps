sap.ui.define(function () {
	"use strict";

	jQuery.sap.require("jquery.sap.storage");
	var oStorage = jQuery.sap.storage(jQuery.sap.storage.Type.local);
	var transactionAssetsDataID = "transactionAssets";

	return {

		init: function () {

			oStorage.put(transactionAssetsDataID, []);
			oStorage.put(
				transactionAssetsDataID, [{
					"ID": "Transaction1001",
					"docType": "Asset.TransactionAsset",
					"status": "Confirmed",
					"creationDate": "09/14/2018",
					"frameworkPOID": "FrameworkPO1001",
					"dealType": "swap",
					"swapType": "Location",
					"volume": "10000",
					"dtsScore": "13%",
					"swappedByProduct": "Milk",
					"swappedToProduct": "Milk",
					"swapFromLocation": "Perth",
					"swapToLocation": "Chelsea Heights",
					"swapDate": "09/17/2018",
					"swapFutureDate": "12/10/2018",
					"buyerID": "Fonterra",
					"sellerID": "Lion Dairy"
				}, {
					"ID": "Transaction1002",
					"docType": "Asset.TransactionAsset",
					"status": "Delivered",
					"creationDate": "09/12/2018",
					"frameworkPOID": "FrameworkPO1002",
					"dealType": "swap",
					"swapType": "Product",
					"volume": "5000",
					"dtsScore": "13%",
					"swappedByProduct": "Milk",
					"swappedToProduct": "Cream",
					"swapFromLocation": "",
					"swapToLocation": "",
					"swapDate": "09/17/2018",
					"swapFutureDate": "12/10/2018",
					"buyerID": "Saputo",
					"sellerID": "Lion Dairy"
				}]

			);
		},

		getTransactionAssetDataID: function () {

			return transactionAssetsDataID;
		},

		getTransactionAssetData: function () {

			return oStorage.get("transactionAssets");
		},

		put: function (newTransactionAsset) {

			var transactionAssetData = this.getTransactionAssetData();
			transactionAssetData.push(newTransactionAsset);
			oStorage.put(transactionAssetsDataID, transactionAssetData);
		},

		patch: function (transactionAsset) {

			this.remove(transactionAsset.ID);
			this.put(transactionAsset);
		},

		remove: function (id) {

			var transactionAssetData = this.getTransactionAssetData();
			transactionAssetData = _.without(transactionAssetData, _.findWhere(transactionAssetData, {
				ID: id
			}));
			oStorage.put(transactionAssetsDataID, transactionAssetData);
		},

		removeAll: function () {

			oStorage.put(transactionAssetsDataID, []);
		},

		clearTransactionAssetData: function () {

			oStorage.put(transactionAssetsDataID, []);
		}
	};
});