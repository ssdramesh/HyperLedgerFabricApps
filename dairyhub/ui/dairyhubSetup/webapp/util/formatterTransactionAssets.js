sap.ui.define(function () {
	"use strict";

	return {

		mapTransactionAssetToModel: function (responseData) {
			return {
				ID: responseData.ID,
				ObjectType: responseData.docType,
				Status: responseData.status,
				CreationDate: responseData.creationDate,
				FrameworkPOID: responseData.frameworkPOID,
				DealType: responseData.dealType,
				SwapType: responseData.swapType,
				Volume: responseData.volume,
				DTSScore: responseData.dtsScore,
				SwappedByProduct: responseData.swappedByProduct,
				SwappedToProduct: responseData.swappedToProduct,
				SwapFromLocation: responseData.swapFromLocation,
				SwapToLocation: responseData.swapToLocation,
				SwapDate: responseData.swapDate,
				SwapFutureDate: responseData.swapFutureDate,
				BuyerID: responseData.buyerID,
				SellerID: responseData.sellerID
			};
		},

		mapTransactionAssetsToModel: function (responseData) {

			var items = [];
			if (responseData) {
				for (var i = 0; i < responseData.length; i++) {
					items.push(this.mapTransactionAssetToModel(responseData[i]));
				}
			}
			return items;
		},

		mapTransactionAssetToChaincode: function (oModel, newTransactionAsset) {

			if (newTransactionAsset === true) {
				return {
					"ID": this.getNewTransactionAssetID(oModel),
					"docType": "Asset.TransactionAsset",
					"status": oModel.getProperty("/newTransactionAsset/Status"),
					"creationDate": oModel.getProperty("/newTransactionAsset/CreationDate"),
					"frameworkPOID": oModel.getProperty("/newTransactionAsset/FrameworkPOID"),
					"dealType": oModel.getProperty("/newTransactionAsset/DealType"),
					"swapType": oModel.getProperty("/newTransactionAsset/SwapType"),
					"volume": oModel.getProperty("/newTransactionAsset/Volume"),
					"dtsScore": oModel.getProperty("/newTransactionAsset/DTSScore"),
					"swappedByProduct": oModel.getProperty("/newTransactionAsset/SwappedByProduct"),
					"swappedToProduct": oModel.getProperty("/newTransactionAsset/SwappedToProduct"),
					"swapFromLocation": oModel.getProperty("/newTransactionAsset/SwapFromLocation"),
					"swapToLocation": oModel.getProperty("/newTransactionAsset/SwapToLocation"),
					"swapDate": oModel.getProperty("/newTransactionAsset/SwapDate"),
					"swapFutureDate": oModel.getProperty("/newTransactionAsset/SwapFutureDate"),
					"buyerID": oModel.getProperty("/newTransactionAsset/BuyerID"),
					"sellerID": oModel.getProperty("/newTransactionAsset/SellerID")
				};
			} else {
				return {
					"ID": oModel.getProperty("/selectedTransactionAsset/ID"),
					"docType": "Asset.TransactionAsset",
					"status": oModel.getProperty("/selectedTransactionAsset/Status"),
					"creationDate": oModel.getProperty("/selectedTransactionAsset/CreationDate"),
					"frameworkPOID": oModel.getProperty("/selectedTransactionAsset/FrameworkPOID"),
					"dealType": oModel.getProperty("/selectedTransactionAsset/DealType"),
					"swapType": oModel.getProperty("/selectedTransactionAsset/SwapType"),
					"volume": oModel.getProperty("/selectedTransactionAsset/Volume"),
					"dtsScore": oModel.getProperty("/selectedTransactionAsset/DTSScore"),
					"swappedByProduct": oModel.getProperty("/selectedTransactionAsset/SwappedByProduct"),
					"swappedToProduct": oModel.getProperty("/selectedTransactionAsset/SwappedToProduct"),
					"swapFromLocation": oModel.getProperty("/selectedTransactionAsset/SwapFromLocation"),
					"swapToLocation": oModel.getProperty("/selectedTransactionAsset/SwapToLocation"),
					"swapDate": oModel.getProperty("/selectedTransactionAsset/SwapDate"),
					"swapFutureDate": oModel.getProperty("/selectedTransactionAsset/SwapFutureDate"),
					"buyerID": oModel.getProperty("/selectedTransactionAsset/BuyerID"),
					"sellerID": oModel.getProperty("/selectedTransactionAsset/SellerID")
				};
			}
		},

		mapTransactionAssetToLocalStorage: function (oModel, newTransactionAsset) {

			return this.mapTransactionAssetToChaincode(oModel, newTransactionAsset);
		},

		getNewTransactionAssetID: function (oModel) {

			if (typeof oModel.getProperty("/newTransactionAsset/ID") === "undefined" ||
				oModel.getProperty("/newTransactionAsset/ID") === ""
			) {
				var iD = "TransactionAsset";
				var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
				for (var i = 0; i < 8; i++) {
					iD += possible.charAt(Math.floor(Math.random() * possible.length));
				}
			} else {
				iD = oModel.getProperty("/newTransactionAsset/ID");
			}
			oModel.setProperty("/newTransactionAsset/ID", iD);
			return iD;
		}
	};
});