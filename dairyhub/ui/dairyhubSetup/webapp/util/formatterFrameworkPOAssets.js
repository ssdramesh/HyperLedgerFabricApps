sap.ui.define(function () {
	"use strict";

	return {

		mapFrameworkPOAssetToModel: function (responseData) {
			return {
				ID: responseData.ID,
				ObjectType: responseData.docType,
				Status: responseData.status,
				CreationDate: responseData.creationDate,
				StartDate: responseData.startDate,
				EndDate: responseData.endDate,
				Price: responseData.price,
				PriceUnit: responseData.priceUnit,
				DairyType: responseData.dairyType,
				DTSScore: responseData.dtsScore,
				VolumeLimit: responseData.volumeLimit,
				BuyerID: responseData.buyerID,
				SellerID: responseData.sellerID
			};
		},

		mapFrameworkPOAssetsToModel: function (responseData) {

			var items = [];
			if (responseData) {
				for (var i = 0; i < responseData.length; i++) {
					items.push(this.mapFrameworkPOAssetToModel(responseData[i]));
				}
			}
			return items;
		},

		mapFrameworkPOAssetToChaincode: function (oModel, newFrameworkPOAsset) {

			if (newFrameworkPOAsset === true) {
				return {
					"ID": this.getNewFrameworkPOAssetID(oModel),
					"docType": "Asset.FrameworkPOAsset",
					"status": oModel.getProperty("/newFrameworkPOAsset/Status"),
					"creationDate": oModel.getProperty("/newFrameworkPOAsset/CreationDate"),
					"startDate": oModel.getProperty("/newFrameworkPOAsset/StartDate"),
					"endDate": oModel.getProperty("/newFrameworkPOAsset/EndDate"),
					"price": oModel.getProperty("/newFrameworkPOAsset/Price"),
					"priceUnit": oModel.getProperty("/newFrameworkPOAsset/PriceUnit"),
					"dairyType": oModel.getProperty("/newFrameworkPOAsset/DairyType"),
					"dtsScore": oModel.getProperty("/newFrameworkPOAsset/DTSScore"),
					"volumeLimit": oModel.getProperty("/newFrameworkPOAsset/VolumeLimit"),
					"buyerID": oModel.getProperty("/newFrameworkPOAsset/BuyerID"),
					"sellerID": oModel.getProperty("/newFrameworkPOAsset/SellerID")
				};
			} else {
				return {
					"ID": oModel.getProperty("/selectedFrameworkPOAsset/ID"),
					"docType": "Asset.FrameworkPOAsset",
					"status": oModel.getProperty("/selectedFrameworkPOAsset/Status"),
					"creationDate": oModel.getProperty("/selectedFrameworkPOAsset/CreationDate"),
					"startDate": oModel.getProperty("/selectedFrameworkPOAsset/StartDate"),
					"endDate": oModel.getProperty("/selectedFrameworkPOAsset/EndDate"),
					"price": oModel.getProperty("/selectedFrameworkPOAsset/Price"),
					"priceUnit": oModel.getProperty("/selectedFrameworkPOAsset/PriceUnit"),
					"dairyType": oModel.getProperty("/selectedFrameworkPOAsset/DairyType"),
					"dtsScore": oModel.getProperty("/selectedFrameworkPOAsset/DTSScore"),
					"volumeLimit": oModel.getProperty("/selectedFrameworkPOAsset/VolumeLimit"),
					"buyerID": oModel.getProperty("/selectedFrameworkPOAsset/BuyerID"),
					"sellerID": oModel.getProperty("/selectedFrameworkPOAsset/SellerID")
				};
			}
		},

		mapFrameworkPOAssetToLocalStorage: function (oModel, newFrameworkPOAsset) {

			return this.mapFrameworkPOAssetToChaincode(oModel, newFrameworkPOAsset);
		},

		getNewFrameworkPOAssetID: function (oModel) {

			if (typeof oModel.getProperty("/newFrameworkPOAsset/ID") === "undefined" ||
				oModel.getProperty("/newFrameworkPOAsset/ID") === ""
			) {
				var iD = "FrameworkPOAsset";
				var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
				for (var i = 0; i < 8; i++) {
					iD += possible.charAt(Math.floor(Math.random() * possible.length));
				}
			} else {
				iD = oModel.getProperty("/newFrameworkPOAsset/ID");
			}
			oModel.setProperty("/newFrameworkPOAsset/ID", iD);
			return iD;
		}
	};
});