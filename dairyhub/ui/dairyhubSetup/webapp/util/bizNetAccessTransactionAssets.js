sap.ui.define([
	"dairyhubSetup/util/restBuilder",
	"dairyhubSetup/util/formatterTransactionAssets",
	"dairyhubSetup/util/localStoreTransactionAssets"
], function (
	restBuilder,
	formatterTransactionAssets,
	localStoreTransactionAssets
) {
	"use strict";

	return {

		loadAllTransactionAssets: function (oComponent, oModel) {

			if (!(oComponent.getModel("TestSwitch").getProperty("/testMode"))) {
				restBuilder.execute(
					oModel.getProperty("/chaincode"),
					"readAll", [],
					function (responseData) {
						oModel.setProperty(
							"/transactionAssetCollection/items",
							formatterTransactionAssets.mapTransactionAssetsToModel(responseData)
						);
					});
			} else {
				oModel.setProperty(
					"/transactionAssetCollection/items",
					formatterTransactionAssets.mapTransactionAssetsToModel(localStoreTransactionAssets.getTransactionAssetData())
				);
			}
		},

		loadTransactionAsset: function (oModel, selectedTransactionAssetID) {

			oModel.setProperty(
				"/selectedTransactionAsset",
				_.findWhere(oModel.getProperty("/transactionAssetCollection/items"), {
						ID: selectedTransactionAssetID
					},
					this));
		},

		addNewTransactionAsset: function (oComponent, oModel) {

			if (!(oComponent.getModel("TestSwitch").getProperty("/testMode"))) {
				restBuilder.execute(
					oModel.getProperty("/chaincode"),
					"addNew",
					formatterTransactionAssets.mapTransactionAssetToChaincode(oModel, true)
				);
			} else {
				localStoreTransactionAssets.put(formatterTransactionAssets.mapTransactionAssetToLocalStorage(oModel, true));
			}
			this.loadAllTransactionAssets(oComponent, oModel);
			return oModel.getProperty("/newTransactionAsset/ID");
		},

		updateTransactionAsset: function (oComponent, oModel) {

			if (!(oComponent.getModel("TestSwitch").getProperty("/testMode"))) {
				restBuilder.execute(
					oModel.getProperty("/chaincode"),
					"update",
					formatterTransactionAssets.mapTransactionAssetToChaincode(oModel, false)
				);
			} else {
				localStoreTransactionAssets.patch(formatterTransactionAssets.mapTransactionAssetToLocalStorage(oModel, false));
			}
			this.loadAllTransactionAssets(oComponent, oModel);
		},

		removeTransactionAsset: function (oComponent, oModel, transactionAssetID) {

			if (!(oComponent.getModel("TestSwitch").getProperty("/testMode"))) {
				restBuilder.execute(
					oModel.getProperty("/chaincode"),
					"remove", {
						ID: transactionAssetID
					}
				);
			} else {
				localStoreTransactionAssets.remove(transactionAssetID);
			}
			this.loadAllTransactionAssets(oComponent, oModel);
			return true;
		},

		removeAllTransactionAssets: function (oComponent, oModel) {

			if (!(oComponent.getModel("TestSwitch").getProperty("/testMode"))) {
				restBuilder.execute(
					oModel.getProperty("/chaincode"),
					"removeAll", []
				);
			} else {
				localStoreTransactionAssets.removeAll();
			}
			this.loadAllTransactionAssets(oComponent, oModel);
			oModel.setProperty("/selectedTransactionAsset", {});
			return true;
		}
	};
});