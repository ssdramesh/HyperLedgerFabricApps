sap.ui.define([
	"dairyhubSetup/util/restBuilder",
	"dairyhubSetup/util/formatterFrameworkPOAssets",
	"dairyhubSetup/util/localStoreFrameworkPOAssets"
], function(
		restBuilder,
		formatterFrameworkPOAssets,
		localStoreFrameworkPOAssets
	) {
	"use strict";

	return {

		loadAllFrameworkPOAssets:function(oComponent, oModel) {

			if ( !( oComponent.getModel("TestSwitch").getProperty("/testMode") ) ){
				restBuilder.execute(
					oModel.getProperty("/chaincode"),
					"readAll",
					[],
					function(responseData){
						oModel.setProperty(
							"/frameworkPOAssetCollection/items",
							formatterFrameworkPOAssets.mapFrameworkPOAssetsToModel(responseData)
						);
					});
			} else {
				oModel.setProperty(
					"/frameworkPOAssetCollection/items",
					formatterFrameworkPOAssets.mapFrameworkPOAssetsToModel(localStoreFrameworkPOAssets.getFrameworkPOAssetData())
				);
			}
		},

		loadFrameworkPOAsset:function(oModel, selectedFrameworkPOAssetID){

			oModel.setProperty(
				"/selectedFrameworkPOAsset",
				_.findWhere(oModel.getProperty("/frameworkPOAssetCollection/items"),
					{
						ID: selectedFrameworkPOAssetID
					},
				this));
		},

		addNewFrameworkPOAsset:function(oComponent, oModel){

			if ( !( oComponent.getModel("TestSwitch").getProperty("/testMode") ) ){
				restBuilder.execute(
					oModel.getProperty("/chaincode"),
					"addNew",
					formatterFrameworkPOAssets.mapFrameworkPOAssetToChaincode(oModel, true)
				);
			}  else {
				localStoreFrameworkPOAssets.put(formatterFrameworkPOAssets.mapFrameworkPOAssetToLocalStorage(oModel, true));
			}
			this.loadAllFrameworkPOAssets(oComponent, oModel);
			return oModel.getProperty("/newFrameworkPOAsset/ID");
		},

		removeFrameworkPOAsset : function(oComponent, oModel, frameworkPOAssetID){

			if ( !( oComponent.getModel("TestSwitch").getProperty("/testMode") ) ) {
				restBuilder.execute(
					oModel.getProperty("/chaincode"),
					"remove",
					{ID:frameworkPOAssetID}
				);
			} else {
				localStoreFrameworkPOAssets.remove(frameworkPOAssetID);
			}
			this.loadAllFrameworkPOAssets(oComponent, oModel);
			return true;
		},

		removeAllFrameworkPOAssets : function(oComponent, oModel){

			if (!(oComponent.getModel("TestSwitch").getProperty("/testMode"))) {
				restBuilder.execute(
					oModel.getProperty("/chaincode"),
					"removeAll",
					[]
				);
			} else {
				localStoreFrameworkPOAssets.removeAll();
			}
			this.loadAllFrameworkPOAssets(oComponent, oModel);
			oModel.setProperty("/selectedFrameworkPOAsset",{});
			return true;
		}
	};
});
