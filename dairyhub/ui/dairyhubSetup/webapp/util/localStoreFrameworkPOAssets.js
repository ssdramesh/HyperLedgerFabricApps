sap.ui.define(function() {
	"use strict";

	jQuery.sap.require("jquery.sap.storage");
	var oStorage = jQuery.sap.storage(jQuery.sap.storage.Type.local);
	var frameworkPOAssetsDataID = "frameworkPOAssets";

	return {

		init: function(){

			oStorage.put(frameworkPOAssetsDataID,[]);
			oStorage.put(
				frameworkPOAssetsDataID,
[
	{
		"ID":"FrameworkPOAsset1001",
		"docType":"Asset.FrameworkPOAsset",
		"status":"Created",
		"creationDate":"12/28/2107",
		"startDate":"startDate001",
		"endDate":"endDate001",
		"price":"price001",
		"priceUnit":"priceUnit001",
		"dairyType":"dairyType001",
		"dtsScore":"dtsScore001",
		"volumeLimit":"volumeLimit001",
		"buyerID":"buyerID001",
		"sellerID":"sellerID001"
	}
]				
			);
		},

		getFrameworkPOAssetDataID : function(){

			return frameworkPOAssetsDataID;
		},

		getFrameworkPOAssetData  : function(){

			return oStorage.get("frameworkPOAssets");
		},

		put: function(newFrameworkPOAsset){

			var frameworkPOAssetData = this.getFrameworkPOAssetData();
			frameworkPOAssetData.push(newFrameworkPOAsset);
			oStorage.put(frameworkPOAssetsDataID, frameworkPOAssetData);
		},

		remove : function (id){

			var frameworkPOAssetData = this.getFrameworkPOAssetData();
			frameworkPOAssetData = _.without(frameworkPOAssetData,_.findWhere(frameworkPOAssetData,{ID:id}));
			oStorage.put(frameworkPOAssetsDataID, frameworkPOAssetData);
		},

		removeAll : function(){

			oStorage.put(frameworkPOAssetsDataID,[]);
		},

		clearFrameworkPOAssetData: function(){

			oStorage.put(frameworkPOAssetsDataID,[]);
		}
	};
});
