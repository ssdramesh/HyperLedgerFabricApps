sap.ui.define([
	"dairyhubSetup/controller/BaseController",
	"dairyhubSetup/model/modelsBase",
	"dairyhubSetup/util/messageProvider",
	"dairyhubSetup/util/localStoreFrameworkPOAssets",
	"dairyhubSetup/util/bizNetAccessFrameworkPOAssets",
	"sap/ui/core/routing/History",
	"sap/ui/model/Filter",
	"sap/ui/model/FilterOperator",
	"sap/ui/Device"
], function(
		BaseController,
		modelsBase,
		messageProvider,
		localStoreFrameworkPOAssets,
		bizNetAccessFrameworkPOAssets,
		History,
		Filter,
		FilterOperator,
		Device
	) {
	"use strict";

	return BaseController.extend("dairyhubSetup.controller.FrameworkPOAssetsOverview", {

		onInit : function(){

			var oList = this.byId("frameworkPOAssetsList");
			this._oList = oList;
			this._oListFilterState = {
				aFilter : [],
				aSearch : []
			};
			localStoreFrameworkPOAssets.init();
			if ( typeof this.getOwnerComponent().getModel("Messages") === "undefined" ){
				this.getOwnerComponent().setModel(modelsBase.createMessagesModel(), "Messages");
			}
			messageProvider.init(this.getOwnerComponent().getModel("Messages"));
		},

		onSelectionChange : function (oEvent) {

			this._showDetail(oEvent.getParameter("listItem") || oEvent.getSource());
		},

		onAdd : function() {

			this.getRouter().navTo("frameworkPOAsset", {frameworkPOAssetId : "___new"});
		},

		onRemoveAll : function(){

			bizNetAccessFrameworkPOAssets.removeAllFrameworkPOAssets(this.getOwnerComponent(), this.getOwnerComponent().getModel("FrameworkPOAssets"));
		},

		onSearch : function(oEvent) {

			var sQuery = oEvent.getParameter("query");
			if (sQuery) {
				this._oListFilterState.aSearch = [new Filter("ID", FilterOperator.Contains, sQuery)];
			} else {
				this._oListFilterState.aSearch = [];
			}
			this._applyFilterSearch();
			var bReplace = !Device.system.phone;
			var oModel = this.getModel("FrameworkPOAssets");
			oModel.setProperty(
				"/selectedFrameworkPOAsset",
				_.findWhere(oModel.getProperty("/frameworkPOAssetCollection/items"),
					{
						ID: oModel.getProperty("/searchFrameworkPOAssetID")
					},
				this));
			this.getRouter().navTo("frameworkPOAsset", {
				frameworkPOAssetId : oModel.getProperty("/selectedFrameworkPOAsset").ID
			}, bReplace);
		},

		_onToggleBaaS : function(){

			this.getOwnerComponent().getModel("TestSwitch").setProperty("/testMode",!(this.getView().byId("__buttonToggleFrameworkPOAssets").getPressed()));
			var location = this.getRunMode().location;
			messageProvider.addMessage("Success", "Switching Network for Blockchain Data to: ", "No Description", location, 1, "", "http://www.sap.com");
			if ( this.getRunMode().testMode && localStoreFrameworkPOAssets.getFrameworkPOAssetData() === null ){
				localStoreFrameworkPOAssets.init();
			}
			bizNetAccessFrameworkPOAssets.loadAllFrameworkPOAssets(this.getOwnerComponent(), this.getModel("FrameworkPOAssets"));
		},

		_showDetail : function(oItem) {

			var bReplace = !Device.system.phone;
			var oModel = this.getModel("FrameworkPOAssets");
			this.getRouter().navTo("frameworkPOAsset", {
				frameworkPOAssetId : oModel.getObject(oItem.getBindingContextPath()).ID
			}, bReplace);
		},

		_applyFilterSearch : function () {
			var aFilters = this._oListFilterState.aSearch.concat(this._oListFilterState.aFilter),
				oViewModel = this.getModel("FrameworkPOAssets");
			this._oList.getBinding("items").filter(aFilters, "Application");
			if (aFilters.length !== 0) {
				oViewModel.setProperty("/noDataText", this.getResourceBundle().getText("_labelNoFrameworkPOAssetsText"));
			}
		},

		onMessagePress: function() {

			this.onShowMessageDialog("dairyhubSetup Log");
		},

		onNavBack : function() {
			this.getRouter().navTo("home");
		}
	});
});
