sap.ui.define([
	"dairyhubSetup/controller/BaseController",
	"dairyhubSetup/util/bizNetAccessFrameworkPOAssets"
], function(
		BaseController,
		bizNetAccessFrameworkPOAssets
	) {
	"use strict";

	return BaseController.extend("dairyhubSetup.controller.FrameworkPOAssetDetails", {

		onInit : function(){

			this.getOwnerComponent().getRouter().getRoute("frameworkPOAsset").attachPatternMatched(this._onObjectMatched, this);
		},

		_onObjectMatched : function (oEvent) {

			var pId = oEvent.getParameter("arguments").frameworkPOAssetId;
			if ( pId === "___new" ) {
				this.getView().byId("__barFrameworkPOAsset").setSelectedKey("New");
			} else {
				bizNetAccessFrameworkPOAssets.loadFrameworkPOAsset(this.getView().getModel("FrameworkPOAssets"), oEvent.getParameter("arguments").frameworkPOAssetId);
			}
		},

		addNew : function(){

			var oModel = this.getView().getModel("FrameworkPOAssets");
			if ( oModel.getProperty("/newFrameworkPOAsset/Alias") !== "" ||
				   oModel.getProperty("/newFrameworkPOAsset/Description") !== "" ) {
				var frameworkPOAssetId = bizNetAccessFrameworkPOAssets.addNewFrameworkPOAsset(this.getOwnerComponent(), oModel);
				this.getView().byId("__barFrameworkPOAsset").setSelectedKey("Details");
				bizNetAccessFrameworkPOAssets.loadFrameworkPOAsset(this.getView().getModel("FrameworkPOAssets"), frameworkPOAssetId);
			} else {
				sap.m.MessageToast.show("Please enter valid data in all fields", {});
			}
			this._clearNewAsset();
		},

		removeFrameworkPOAsset : function(){

			var oModel = this.getView().getModel("FrameworkPOAssets");
			bizNetAccessFrameworkPOAssets.removeFrameworkPOAsset(this.getOwnerComponent(), oModel, oModel.getProperty("/selectedFrameworkPOAsset/ID"));
		},

		_clearNewAsset : function(){

			this.getView().getModel("FrameworkPOAssets").setProperty("/newFrameworkPOAsset",{});
		}
	});

});
