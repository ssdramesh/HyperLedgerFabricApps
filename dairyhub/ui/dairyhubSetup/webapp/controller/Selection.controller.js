sap.ui.define([
	"dairyhubSetup/controller/BaseController",
	"dairyhubSetup/util/messageProvider",

        "dairyhubSetup/util/bizNetAccessFrameworkPOAssets",
        "dairyhubSetup/model/modelsFrameworkPOAssets",

        "dairyhubSetup/util/bizNetAccessTransactionAssets",
        "dairyhubSetup/model/modelsTransactionAssets",

	"dairyhubSetup/model/modelsBase"
], function(
		BaseController,
		messageProvider,

        bizNetAccessFrameworkPOAssets,
        modelsFrameworkPOAssets,

        bizNetAccessTransactionAssets,
        modelsTransactionAssets,

		modelsBase
	) {
	"use strict";

	return BaseController.extend("dairyhubSetup.controller.Selection", {

		onInit: function(){
			if ( typeof this.getOwnerComponent().getModel("Messages") === "undefined" ){
				this.getOwnerComponent().setModel(modelsBase.createMessagesModel(), "Messages");
			}
			messageProvider.init(this.getOwnerComponent().getModel("Messages"));
		},

		onSelect: function(oEvent) {
			switch (oEvent.getSource().getText()) {

        case "FrameworkPOAssets":
          this.getView().getModel("Selection").setProperty("/entityName", "frameworkPOAsset");
          this.getOwnerComponent().setModel(modelsFrameworkPOAssets.createFrameworkPOAssetsModel(), "FrameworkPOAssets");
          this.loadMetaData("frameworkPOAsset", this.getModel("FrameworkPOAssets"));
          bizNetAccessFrameworkPOAssets.loadAllFrameworkPOAssets(this.getOwnerComponent(), this.getView().getModel("FrameworkPOAssets"));
          this.getOwnerComponent().getRouter().navTo("frameworkPOAssets", {});
          break;

        case "TransactionAssets":
          this.getView().getModel("Selection").setProperty("/entityName", "transactionAsset");
          this.getOwnerComponent().setModel(modelsTransactionAssets.createTransactionAssetsModel(), "TransactionAssets");
          this.loadMetaData("transactionAsset", this.getModel("TransactionAssets"));
          bizNetAccessTransactionAssets.loadAllTransactionAssets(this.getOwnerComponent(), this.getView().getModel("TransactionAssets"));
          this.getOwnerComponent().getRouter().navTo("transactionAssets", {});
          break;

			}
		},

		removeAllEntities: function() {

			var msgStripID = this.getView().byId("__stripMessage");
			var location = this.getRunMode().location;


      if (typeof this.getOwnerComponent().getModel("FrameworkPOAssets") === "undefined") {
        this.getOwnerComponent().setModel(modelsFrameworkPOAssets.createFrameworkPOAssetsModel(), "FrameworkPOAssets");
      }
      bizNetAccessFrameworkPOAssets.removeAllFrameworkPOAssets(this.getOwnerComponent(), this.getView().getModel("FrameworkPOAssets"));
      messageProvider.addMessage("Success", "All FrameworkPOAssets deleted from sample-network", "No Description", location, 1, "", "http://www.sap.com");

      if (typeof this.getOwnerComponent().getModel("TransactionAssets") === "undefined") {
        this.getOwnerComponent().setModel(modelsTransactionAssets.createTransactionAssetsModel(), "TransactionAssets");
      }
      bizNetAccessTransactionAssets.removeAllTransactionAssets(this.getOwnerComponent(), this.getView().getModel("TransactionAssets"));
      messageProvider.addMessage("Success", "All TransactionAssets deleted from sample-network", "No Description", location, 1, "", "http://www.sap.com");


			this.showMessageStrip(msgStripID,"All Existing dairyhub data deleted from SAP BaaS", "S");
		},

		onToggleBaaS: function() {

			this.onToggleRunMode(this.getView().byId("__stripMessage"));
		},

		onMessagePress: function() {

			this.onShowMessageDialog("dairyhub Maintenance Log");
		}
	});
});
