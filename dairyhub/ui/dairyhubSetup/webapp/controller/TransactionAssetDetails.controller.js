sap.ui.define([
	"dairyhubSetup/controller/BaseController",
	"dairyhubSetup/util/bizNetAccessTransactionAssets"
], function (BaseController, bizNetAccessTransactionAssets) {

	"use strict";

	return BaseController.extend("dairyhubSetup.controller.TransactionAssetDetails", {

		onInit: function () {

			this.getOwnerComponent().getRouter().getRoute("transactionAsset").attachPatternMatched(this._onObjectMatched, this);
		},

		_onObjectMatched: function (oEvent) {

			var pId = oEvent.getParameter("arguments").transactionAssetId;
			if (pId === "___new") {
				this.getView().byId("__barTransactionAsset").setSelectedKey("New");
			} else {
				bizNetAccessTransactionAssets.loadTransactionAsset(this.getView().getModel("TransactionAssets"), oEvent.getParameter("arguments").transactionAssetId);
			}
		},

		addNew: function () {

			var oModel = this.getView().getModel("TransactionAssets");
			if (oModel.getProperty("/newTransactionAsset/Alias") !== "" || oModel.getProperty("/newTransactionAsset/Description") !== "") {
				var transactionAssetId = bizNetAccessTransactionAssets.addNewTransactionAsset(this.getOwnerComponent(), oModel);
				this.getView().byId("__barTransactionAsset").setSelectedKey("Details");
				bizNetAccessTransactionAssets.loadTransactionAsset(this.getView().getModel("TransactionAssets"), transactionAssetId);
			} else {
				sap.m.MessageToast.show("Please enter valid data in all fields", {});
			}
			this._clearNewAsset();
		},

		removeTransactionAsset: function () {

			var oModel = this.getView().getModel("TransactionAssets");
			bizNetAccessTransactionAssets.removeTransactionAsset(this.getOwnerComponent(), oModel, oModel.getProperty(
				"/selectedTransactionAsset/ID"));
		},

		_clearNewAsset: function () {

			this.getView().getModel("TransactionAssets").setProperty("/newTransactionAsset", {});
		},

		onTransactionEdit: function (oEvent) {

			if (this.getView().byId("__inputSelTransactionAssetStatus").getEditable() === false) {
				this.getView().byId("__inputSelTransactionAssetStatus").setEditable(true);
			} else {
				this.getView().byId("__inputSelTransactionAssetStatus").setEditable(false);
			}
			if (this.getView().byId("__inputSelTransactionAssetVolume").getEditable() === false) {
				this.getView().byId("__inputSelTransactionAssetVolume").setEditable(true);
			} else {
				this.getView().byId("__inputSelTransactionAssetVolume").setEditable(false);
			}
			if (this.getView().byId("__inputSelTransactionAssetSwappedToProduct").getEditable() === false) {
				this.getView().byId("__inputSelTransactionAssetSwappedToProduct").setEditable(true);
			} else {
				this.getView().byId("__inputSelTransactionAssetSwappedToProduct").setEditable(false);
			}
			if (this.getView().byId("__inputSelTransactionAssetSwapFromLocation").getEditable() === false) {
				this.getView().byId("__inputSelTransactionAssetSwapFromLocation").setEditable(true);
			} else {
				this.getView().byId("__inputSelTransactionAssetSwapFromLocation").setEditable(false);
			}
			if (this.getView().byId("__inputSelTransactionAssetSwapToLocation").getEditable() === false) {
				this.getView().byId("__inputSelTransactionAssetSwapToLocation").setEditable(true);
			} else {
				this.getView().byId("__inputSelTransactionAssetSwapToLocation").setEditable(false);
			}
			if (this.getView().byId("__inputSelTransactionAssetSwapDate").getEditable() === false) {
				this.getView().byId("__inputSelTransactionAssetSwapDate").setEditable(true);
			} else {
				this.getView().byId("__inputSelTransactionAssetSwapDate").setEditable(false);
			}
			if (this.getView().byId("__inputSelTransactionAssetSwapFutureDate").getEditable() === false) {
				this.getView().byId("__inputSelTransactionAssetSwapFutureDate").setEditable(true);
			} else {
				this.getView().byId("__inputSelTransactionAssetSwapFutureDate").setEditable(false);
			}
			if (this.getView().byId("__inputSelTransactionAssetSellerID").getEditable() === false) {
				this.getView().byId("__inputSelTransactionAssetSellerID").setEditable(true);
			} else {
				this.getView().byId("__inputSelTransactionAssetSellerID").setEditable(false);
			}
		},

		saveTransactionAsset: function (oEvent) {

			var oModel = this.getView().getModel("TransactionAssets");
			bizNetAccessTransactionAssets.updateTransactionAsset(this.getOwnerComponent(), oModel);
			this.onTransactionEdit(oEvent);
		}
	});
});