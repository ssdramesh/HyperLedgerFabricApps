sap.ui.define([
	"dairyhubSetup/controller/BaseController",
	"dairyhubSetup/model/modelsBase",
	"dairyhubSetup/util/messageProvider",
	"dairyhubSetup/util/localStoreTransactionAssets",
	"dairyhubSetup/util/bizNetAccessTransactionAssets",
	"sap/ui/core/routing/History",
	"sap/ui/model/Filter",
	"sap/ui/model/FilterOperator",
	"sap/ui/Device"
], function(
		BaseController,
		modelsBase,
		messageProvider,
		localStoreTransactionAssets,
		bizNetAccessTransactionAssets,
		History,
		Filter,
		FilterOperator,
		Device
	) {
	"use strict";

	return BaseController.extend("dairyhubSetup.controller.TransactionAssetsOverview", {

		onInit : function(){

			var oList = this.byId("transactionAssetsList");
			this._oList = oList;
			this._oListFilterState = {
				aFilter : [],
				aSearch : []
			};
			localStoreTransactionAssets.init();
			if ( typeof this.getOwnerComponent().getModel("Messages") === "undefined" ){
				this.getOwnerComponent().setModel(modelsBase.createMessagesModel(), "Messages");
			}
			messageProvider.init(this.getOwnerComponent().getModel("Messages"));
		},

		onSelectionChange : function (oEvent) {

			this._showDetail(oEvent.getParameter("listItem") || oEvent.getSource());
		},

		onAdd : function() {

			this.getRouter().navTo("transactionAsset", {transactionAssetId : "___new"});
		},

		onRemoveAll : function(){

			bizNetAccessTransactionAssets.removeAllTransactionAssets(this.getOwnerComponent(), this.getOwnerComponent().getModel("TransactionAssets"));
		},

		onSearch : function(oEvent) {

			var sQuery = oEvent.getParameter("query");
			if (sQuery) {
				this._oListFilterState.aSearch = [new Filter("ID", FilterOperator.Contains, sQuery)];
			} else {
				this._oListFilterState.aSearch = [];
			}
			this._applyFilterSearch();
			var bReplace = !Device.system.phone;
			var oModel = this.getModel("TransactionAssets");
			oModel.setProperty(
				"/selectedTransactionAsset",
				_.findWhere(oModel.getProperty("/transactionAssetCollection/items"),
					{
						ID: oModel.getProperty("/searchTransactionAssetID")
					},
				this));
			this.getRouter().navTo("transactionAsset", {
				transactionAssetId : oModel.getProperty("/selectedTransactionAsset").ID
			}, bReplace);
		},

		_onToggleBaaS : function(){

			this.getOwnerComponent().getModel("TestSwitch").setProperty("/testMode",!(this.getView().byId("__buttonToggleTransactionAssets").getPressed()));
			var location = this.getRunMode().location;
			messageProvider.addMessage("Success", "Switching Network for Blockchain Data to: ", "No Description", location, 1, "", "http://www.sap.com");
			if ( this.getRunMode().testMode && localStoreTransactionAssets.getTransactionAssetData() === null ){
				localStoreTransactionAssets.init();
			}
			bizNetAccessTransactionAssets.loadAllTransactionAssets(this.getOwnerComponent(), this.getModel("TransactionAssets"));
		},

		_showDetail : function(oItem) {

			var bReplace = !Device.system.phone;
			var oModel = this.getModel("TransactionAssets");
			this.getRouter().navTo("transactionAsset", {
				transactionAssetId : oModel.getObject(oItem.getBindingContextPath()).ID
			}, bReplace);
		},

		_applyFilterSearch : function () {
			var aFilters = this._oListFilterState.aSearch.concat(this._oListFilterState.aFilter),
				oViewModel = this.getModel("TransactionAssets");
			this._oList.getBinding("items").filter(aFilters, "Application");
			if (aFilters.length !== 0) {
				oViewModel.setProperty("/noDataText", this.getResourceBundle().getText("_labelNoTransactionAssetsText"));
			}
		},

		onMessagePress: function() {

			this.onShowMessageDialog("dairyhubSetup Log");
		},

		onNavBack : function() {
			this.getRouter().navTo("home");
		}
	});
});
