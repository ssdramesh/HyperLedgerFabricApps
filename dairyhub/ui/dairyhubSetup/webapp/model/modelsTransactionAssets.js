sap.ui.define([
	"sap/ui/model/json/JSONModel"
], function (JSONModel) {
	"use strict";

	return {

		createTransactionAssetsModel: function () {

			var oModel = new JSONModel();
			oModel.setDefaultBindingMode("TwoWay");
			oModel.setData({
				chaincode: {
					serviceUrl: "",
					serviceKey: {},
					transactionAsset: {}
				},
				transactionAssetCollection: {
					cols: [{
						name: "ID"
					}, {
						name: "ObjectType"
					}, {
						name: "Status"
					}, {
						name: "CreationDate"
					}, {
						name: "FrameworkPOID"
					}, {
						name: "DealType"
					}, {
						name: "SwapType"
					}, {
						name: "Volume"
					}, {
						name: "DTSScore"
					}, {
						name: "SwappedByProduct"
					}, {
						name: "SwappedToProduct"
					}, {
						name: "SwapFromLocation"
					}, {
						name: "SwapToLocation"
					}, {
						name: "SwapDate"
					}, {
						name: "SwapFutureDate"
					}, {
						name: "BuyerID"
					}, {
						name: "SellerID"
					}],
					items: []
				},
				selectedTransactionAsset: {},
				newTransactionAsset: {
					ID: "",
					ObjectType: "",
					Status: "",
					CreationDate: "",
					FrameworkPOID: "",
					DealType: "",
					SwapType: "",
					Volume: "",
					DTSScore: "",
					SwappedByProduct: "",
					SwappedToProduct: "",
					SwapFromLocation: "",
					SwapToLocation: "",
					SwapDate: "",
					SwapFutureDate: "",
					BuyerID: "",
					SellerID: ""
				},
				selectedTransactionAssetID: "",
				searchTransactionAssetID: ""
			});
			return oModel;
		}
	};
});