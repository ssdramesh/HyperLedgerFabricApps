sap.ui.define([
	"sap/ui/model/json/JSONModel"
], function (JSONModel) {
	"use strict";

	return {

		createFrameworkPOAssetsModel: function () {

			var oModel = new JSONModel();
			oModel.setDefaultBindingMode("TwoWay");
			oModel.setData({
				chaincode: {
					serviceUrl: "",
					serviceKey: {},
					frameworkPOAsset: {}
				},
				frameworkPOAssetCollection: {
					cols: [{
						name: "ID"
					}, {
						name: "ObjectType"
					}, {
						name: "Status"
					}, {
						name: "CreationDate"
					}, {
						name: "StartDate"
					}, {
						name: "EndDate"
					}, {
						name: "Price"
					}, {
						name: "PriceUnit"
					}, {
						name: "DairyType"
					}, {
						name: "DTSScore"
					}, {
						name: "VolumeLimit"
					}, {
						name: "BuyerID"
					}, {
						name: "SellerID"
					}],
					items: []
				},
				selectedFrameworkPOAsset: {},
				newFrameworkPOAsset: {
					ID: "",
					ObjectType: "",
					Status: "",
					CreationDate: "",
					StartDate: "",
					EndDate: "",
					Price: "",
					PriceUnit: "",
					DairyType: "",
					DTSScore: "",
					VolumeLimit: "",
					BuyerID: "",
					SellerID: ""
				},
				selectedFrameworkPOAssetID: "",
				searchFrameworkPOAssetID: ""
			});
			return oModel;
		}
	};
});