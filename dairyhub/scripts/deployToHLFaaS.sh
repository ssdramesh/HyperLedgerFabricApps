#!/bin/bash

BIZNET_FOLDER_NAME=dairyhub
BIZNET_SRC_DIR=/Users/i047582/Documents/Workspaces/git/github.wdf.sap.corp/I047582/HyperLedgerFabricApps/dairyhub/chaincode
BIZNET_DEPLOY_DIR=/Users/i047582/Documents/Workspaces/git/github.wdf.sap.corp/I047582/HyperLedgerFabricApps/dairyhub/deployment
BIZNET_TEST_DIR=/Users/i047582/Documents/Workspaces/git/github.wdf.sap.corp/I047582/HyperLedgerFabricApps/dairyhub/test
BIZNET_UI_DIR=/Users/i047582/Documents/Workspaces/git/github.wdf.sap.corp/I047582/HyperLedgerFabricApps/dairyhub/ui
BIZNET_SCRIPTS_DIR=/Users/i047582/Documents/Workspaces/git/github.wdf.sap.corp/I047582/HyperLedgerFabricApps/dairyhub/scripts

cd "${BIZNET_DEPLOY_DIR}/ChannelDetails"
echo -e "Please select a file that describes your deployment environment"
LIST=$(ls -f *.json)
select CHANNEL_ACCESS_FILE in ${LIST}; do
# Extract Channel Service Name (!!!Assumption:  This is the file name)
  HLF_CHANNEL_INSTANCE_NAME=${CHANNEL_ACCESS_FILE%.json}
  if [[ "${HLF_CHANNEL_INSTANCE_NAME}" == "" ]]; then
    echo "deployment environment is not fully specified!"
    exit 1
  fi
# Extract deployment URL (serviceUrl)
  DEPLOY_URL=`cat $CHANNEL_ACCESS_FILE | jq '.serviceUrl'`
  DEPLOY_URL=${DEPLOY_URL%\"}
  DEPLOY_URL=${DEPLOY_URL#\"}
  if [[ "${DEPLOY_URL}" == "" ]]; then
    echo "deployment environment is not fully specified!"
    exit 1
  fi
# Extract oAuth Token URL (oAuth->url)
  TOKEN_URL=`cat $CHANNEL_ACCESS_FILE | jq '.oAuth.url'`
  TOKEN_URL=${TOKEN_URL%\"}
  TOKEN_URL=${TOKEN_URL#\"}
  if [[ "${TOKEN_URL}" == "" ]]; then
    echo "deployment environment is not fully specified!"
    exit 1
  fi
# Extract the oAuth->clientId
  CLIENT_ID=`cat $CHANNEL_ACCESS_FILE | jq '.oAuth.clientId'`
  CLIENT_ID=${CLIENT_ID%\"}
  CLIENT_ID=${CLIENT_ID#\"}
  if [[ "${CLIENT_ID}" == "" ]]; then
    echo "deployment environment is not fully specified!"
    exit 1
  fi
# Extract Identity zone
  IDENTITY_ZONE=`cat $CHANNEL_ACCESS_FILE | jq '.oAuth.identityZone'`
  IDENTITY_ZONE=${IDENTITY_ZONE%\"}
  IDENTITY_ZONE=${IDENTITY_ZONE#\"}
  if [[ "${IDENTITY_ZONE}" == "" ]]; then
    echo "deployment environment is not fully specified!"
    exit 1
  fi
# Extract The oAuth->clientSecret
  CLIENT_SECRET=`cat $CHANNEL_ACCESS_FILE | jq '.oAuth.clientSecret'`
  CLIENT_SECRET=${CLIENT_SECRET%\"}
  CLIENT_SECRET=${CLIENT_SECRET#\"}
  if [[ "${CLIENT_SECRET}" == "" ]]; then
    echo "deployment environment is not fully specified!"
    exit 1
  fi
  break
done

#Get region from DEPLOY_URL
IFS='.'
read -r -a array <<< "$DEPLOY_URL"
REGION="${array[2]}"
IFS=''

#Extract API End point
API_ENDPOINT="https://api.cf.${REGION}.hana.ondemand.com"
cd "${BIZNET_SCRIPTS_DIR}"
sed -i.bak 's|<API_ENDPOINT>|'${API_ENDPOINT}'|g' "deployCFUI.sh"
find . -name "*.bak" -type f -delete

# Deployment descriptor
cd "${BIZNET_DEPLOY_DIR}"
echo "Enter the filename of your deployment descriptor: "
read DEPLOYMENT_DESCRIPTOR_FILENAME
if [[ "${DEPLOYMENT_DESCRIPTOR_FILENAME}" == "" ]]; then
  DEPLOYMENT_DESCRIPTOR_FILENAME="chaincodeDetails.json"
fi

# Deployment descriptor filename
echo "Updating of deployment descriptor..."
cd "${BIZNET_DEPLOY_DIR}"
sed -i.bak 's~<Enter Channel Service Instance Name Here>~'${HLF_CHANNEL_INSTANCE_NAME}'~g' "${DEPLOYMENT_DESCRIPTOR_FILENAME}"
sed -i.bak 's~<Enter your clientId here>~'${CLIENT_ID}'~g' "${DEPLOYMENT_DESCRIPTOR_FILENAME}"
sed -i.bak 's~<Enter your client secret here>~'${CLIENT_SECRET}'~g' "${DEPLOYMENT_DESCRIPTOR_FILENAME}"
sed -i.bak 's~<Enter your identity zone here>~'${IDENTITY_ZONE}'~g' "${DEPLOYMENT_DESCRIPTOR_FILENAME}"
sed -i.bak 's~<Enter your region here>~'${REGION}'~g' "${DEPLOYMENT_DESCRIPTOR_FILENAME}"
find . -name "*.bak" -type f -delete
echo "Update of deployment descriptor...Done!"
echo

# Update the postman / newman test files
echo "Updating Postman environment..."
cd "${BIZNET_TEST_DIR}/newman/CleanAllAndSetup"
sed -i.bak 's~<IDENTITY_ZONE>~'${IDENTITY_ZONE}'~g' "${BIZNET_TEST_DIR}/newman/CleanAllAndSetup/${BIZNET_FOLDER_NAME}.postman_environment.json"
sed -i.bak 's~<REGION>~'${REGION}'~g' "${BIZNET_TEST_DIR}/newman/CleanAllAndSetup/${BIZNET_FOLDER_NAME}.postman_environment.json"
sed -i.bak 's~<CLIENT_ID>~'${CLIENT_ID}'~g' "${BIZNET_TEST_DIR}/newman/CleanAllAndSetup/${BIZNET_FOLDER_NAME}.postman_environment.json"
sed -i.bak 's~<CLIENT_SECRET>~'${CLIENT_SECRET}'~g' "${BIZNET_TEST_DIR}/newman/CleanAllAndSetup/${BIZNET_FOLDER_NAME}.postman_environment.json"
find . -name "*.bak" -type f -delete
echo "Updating Postman environment...Done!"

#Prepare chaincode Deployment
TOKEN_URL="${TOKEN_URL}/oauth/token?grant_type=client_credentials"
DEPLOY_URL="${DEPLOY_URL}/chaincodes"
AUTH_HEADER="$(printf '%s' "${CLIENT_ID}:${CLIENT_SECRET}" | base64)"
echo "Getting OAUTH Bearer token for deployment..."
echo
CURL_CMD_GET_TOKEN="curl -X GET \"${TOKEN_URL}\" -H 'Authorization: Basic ${AUTH_HEADER}' -H 'Accept: application/json'"
ACCESS_TOKEN=`eval $CURL_CMD_GET_TOKEN | jq '.access_token'`
ACCESS_TOKEN="${ACCESS_TOKEN%\"}"
ACCESS_TOKEN="${ACCESS_TOKEN#\"}"
echo
echo "Getting OAUTH Bearer token successful...Proceeding to deploy..."

#Individual chaincode deployment
cd "${BIZNET_SRC_DIR}"
for CHAINCODE_SRC_FOLDER in *; do
    if [[ -d ${CHAINCODE_SRC_FOLDER} ]]; then
      cd "${CHAINCODE_SRC_FOLDER}"
      ENTITY_NAME="$(basename "${CHAINCODE_SRC_FOLDER}")"
      ENTITY_NAME_LOWER=$(echo ${ENTITY_NAME:0:1} | tr "[A-Z]" "[a-z]")${ENTITY_NAME:1}
      echo "Following chaincode .zip files are found:"
      find . -name "*.zip"
      echo "Enter chiancode .zip filename from above to deploy: "
      read CHAINCODE_FILENAME
      echo "Enter Chaincode description for deployment: "
      read CHAINCODE_DESCRIPTION
      if [[ "${CHAINCODE_DESCRIPTION}" == "" ]]; then
        CHAINCODE_DESCRIPTION="${CHAINCODE_FILENAME}"
      fi
      # Deploy chaincode
      echo
      CURL_CMD_DEPLOY_CHAINCODE="curl -X POST \"${DEPLOY_URL}\" \
      -H 'Authorization: Bearer ${ACCESS_TOKEN}' \
      -H 'Accept: application/json' \
      -H 'content-type: multipart/form-data' \
      -F 'file=@${PWD}/${CHAINCODE_FILENAME}' \
      -F 'arguments=[]' \
      -F 'description=${CHAINCODE_DESCRIPTION}'"
      ENTITY_CHAINCODEID=`eval $CURL_CMD_DEPLOY_CHAINCODE | jq '.id'`
      ENTITY_CHAINCODEID="${ENTITY_CHAINCODEID%\"}"
      ENTITY_CHAINCODEID="${ENTITY_CHAINCODEID#\"}"
#     Enable script re-run in case of failure
      if [[ ${ENTITY_CHAINCODEID} == "" ]]; then
        ENTITY_CHAINCODEID="<CHAINCODE_ID_'${ENTITY_NAME}'>"
      fi
      #Update deployment descriptor file with entity chaincode ID
      cd "${BIZNET_DEPLOY_DIR}"
      sed -i.bak 's~<Enter the deployed chaincodeId for '${ENTITY_NAME_LOWER}' here>~'${ENTITY_CHAINCODEID}'~g' "${BIZNET_DEPLOY_DIR}/${DEPLOYMENT_DESCRIPTOR_FILENAME}"
      #Delete the backup file
      find . -name "*.bak" -type f -delete
      cd "${BIZNET_TEST_DIR}"
      sed -i.bak 's~<CHAINCODE_ID_'${ENTITY_NAME}'>~'${ENTITY_CHAINCODEID}'~g' "${BIZNET_TEST_DIR}/newman/CleanAllAndSetup/${BIZNET_FOLDER_NAME}.postman_environment.json"
      sed -i.bak 's~<DEPLOY_URL>~'${DEPLOY_URL}'~g' "${BIZNET_TEST_DIR}/newman/CleanAllAndSetup/${BIZNET_FOLDER_NAME}.postman_environment.json"
      #Delete the backup file
      find . -name "*.bak" -type f -delete
      cd "${BIZNET_SRC_DIR}"
    fi
done

#Update the relevant UI folders / files
cp -fi "${BIZNET_DEPLOY_DIR}/${DEPLOYMENT_DESCRIPTOR_FILENAME}" "${BIZNET_UI_DIR}/${BIZNET_FOLDER_NAME}Setup/webapp/model/chaincodeDetails.json"
cd "${BIZNET_UI_DIR}/${BIZNET_FOLDER_NAME}Setup"
sed -i.bak 's/<hlf-channel-instance-name>/'${HLF_CHANNEL_INSTANCE_NAME}'/g' manifest.yaml
find . -name "*.bak" -type f -delete
