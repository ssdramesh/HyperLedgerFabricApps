#!/bin/bash

cd /Users/i047582/Documents/Workspaces/git/github.wdf.sap.corp/I047582/HyperLedgerFabricApps/dairyhub/test/newman/CleanAllAndSetup


echo "Clean All FrameworkPOAssets before Setup..."
newman run cleanAllFrameworkPOAssets.postman_collection.json -e dairyhub.postman_environment.json --bail newman
echo "Setting up FrameworkPOAssets..."
newman run createAllFrameworkPOAssets.postman_collection.json -e dairyhub.postman_environment.json -d ./data/FrameworkPOAssets.json --bail newman


echo "Clean All TransactionAssets before Setup..."
newman run cleanAllTransactionAssets.postman_collection.json -e dairyhub.postman_environment.json --bail newman
echo "Setting up TransactionAssets..."
newman run createAllTransactionAssets.postman_collection.json -e dairyhub.postman_environment.json -d ./data/TransactionAssets.json --bail newman

echo "Done. Ready to run!"
