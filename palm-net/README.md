This Demo Scenario for the palm oil sustainability track and trace sample network.  The sample data and details are for demo purposes only and no claim is made towards the validity and authenticity of the data used.  The sample data is only to showcase the concept of how blockchain technology can be applied for track and trace use case.

# Use case description
Blockchain technology has the potential to improve transparency and accountability across the supply chain. Applications are already being used to track and trace materials back to the source, prove authenticity and origin, get ahead of recalls, and accelerate the flow of goods.

This demo illustrates how blockchain can be applied to showcase the palm oil supply chain, to track and trace the journey of oil palm from plantation to finished product containing Palm Oil in the consumer’s hand.

# Data Model
![Data Model](./model/PalmOilNetWorkStructure.jpg)

# Demo Flow
Access:
[Base URL](https://palmnettrace-g85556318.dispatcher.jp1.hana.ondemand.com/webapp/index.html?hc_reset)

Enter: BA1001 and search or access below [URL](https://palmnettrace-g85556318.dispatcher.jp1.hana.ondemand.com/webapp/index.html?hc_reset#//batch/BA1001) --> leads to all certifications positive

# Maintain Sample Data
In case, you want to maintain your own sample data, for example country-specific locations, company names and certification agencies etc.  Please use this [URL](https://palm-netsetup.cfapps.us10.hana.ondemand.com/ui5/)

# Contact
Ramesh Suraparaju (I047582)
