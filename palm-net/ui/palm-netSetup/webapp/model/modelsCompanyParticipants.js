sap.ui.define([
	"sap/ui/model/json/JSONModel"
], function(JSONModel) {
	"use strict";

	return {

		createCompanyParticipantsModel: function(){

			var oModel = new JSONModel();
			oModel.setDefaultBindingMode("TwoWay");
			oModel.setData({
				chaincode:{
					serviceUrl:"",
					serviceKey:{},
					companyParticipant:{}
				},
				companyParticipantCollection	: {
					cols:[
						{name:"ID"},
						{name:"ObjectType"},
						{name:"Status"},
						{name:"CreationDate"},
            {name:"RegistrationID"},
            {name:"CertificationID"},
            {name:"Type"},
            {name:"Name"}
					],
					items:[]
				},
				selectedCompanyParticipant:{},
				newCompanyParticipant:{
					ID:"",
					ObjectType:"",
					Status:"",
					CreationDate:"",
          RegistrationID:"",
          CertificationID:"",
          Type:"",
          Name:""
				},
				selectedCompanyParticipantID	: "",
				searchCompanyParticipantID : ""
			});
			return oModel;
		}
	};
});
