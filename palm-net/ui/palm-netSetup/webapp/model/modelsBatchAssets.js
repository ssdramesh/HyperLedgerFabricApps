sap.ui.define([
	"sap/ui/model/json/JSONModel"
], function(JSONModel) {
	"use strict";

	return {

		createBatchAssetsModel: function(){

			var oModel = new JSONModel();
			oModel.setDefaultBindingMode("TwoWay");
			oModel.setData({
				chaincode:{
					serviceUrl:"",
					serviceKey:{},
					batchAsset:{}
				},
				batchAssetCollection	: {
					cols:[
						{name:"ID"},
						{name:"ObjectType"},
						{name:"Status"},
						{name:"CreationDate"},
            {name:"Volume"},
            {name:"FatC16"},
            {name:"FatC18"},
            {name:"ProductID"},
            {name:"ProductDescription"}
					],
					items:[]
				},
				selectedBatchAsset:{},
				newBatchAsset:{
					ID:"",
					ObjectType:"",
					Status:"",
					CreationDate:"",
          Volume:"",
          FatC16:"",
          FatC18:"",
          ProductID:"",
          ProductDescription:""
				},
				selectedBatchAssetID	: "",
				searchBatchAssetID : ""
			});
			return oModel;
		}
	};
});
