sap.ui.define([
	"sap/ui/model/json/JSONModel"
], function(JSONModel) {
	"use strict";

	return {

		createAdministratorParticipantsModel: function(){

			var oModel = new JSONModel();
			oModel.setDefaultBindingMode("TwoWay");
			oModel.setData({
				chaincode:{
					serviceUrl:"",
					serviceKey:{},
					administratorParticipant:{}
				},
				administratorParticipantCollection	: {
					cols:[
						{name:"ID"},
						{name:"ObjectType"},
						{name:"Status"},
						{name:"CreationDate"}
					],
					items:[]
				},
				selectedAdministratorParticipant:{},
				newAdministratorParticipant:{
					ID:"",
					ObjectType:"",
					Status:"",
					CreationDate:""
				},
				selectedAdministratorParticipantID	: "",
				searchAdministratorParticipantID : ""
			});
			return oModel;
		}
	};
});
