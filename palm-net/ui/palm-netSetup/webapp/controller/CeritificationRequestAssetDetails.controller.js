sap.ui.define([
	"palm-netSetup/controller/BaseController",
	"palm-netSetup/util/bizNetAccessCeritificationRequestAssets"
], function(
		BaseController,
		bizNetAccessCeritificationRequestAssets
	) {
	"use strict";

	return BaseController.extend("palm-netSetup.controller.CeritificationRequestAssetDetails", {

		onInit : function(){

			this.getOwnerComponent().getRouter().getRoute("certificationRequestAsset").attachPatternMatched(this._onObjectMatched, this);
		},

		_onObjectMatched : function (oEvent) {

			var pId = oEvent.getParameter("arguments").certificationRequestAssetId;
			if ( pId === "___new" ) {
				this.getView().byId("__barCeritificationRequestAsset").setSelectedKey("New");
			} else {
				bizNetAccessCeritificationRequestAssets.loadCeritificationRequestAsset(this.getView().getModel("CeritificationRequestAssets"), oEvent.getParameter("arguments").certificationRequestAssetId);
			}
		},

		addNew : function(){

			var oModel = this.getView().getModel("CeritificationRequestAssets");
			if ( oModel.getProperty("/newCeritificationRequestAsset/Alias") !== "" ||
				   oModel.getProperty("/newCeritificationRequestAsset/Description") !== "" ) {
				var certificationRequestAssetId = bizNetAccessCeritificationRequestAssets.addNewCeritificationRequestAsset(this.getOwnerComponent(), oModel);
				this.getView().byId("__barCeritificationRequestAsset").setSelectedKey("Details");
				bizNetAccessCeritificationRequestAssets.loadCeritificationRequestAsset(this.getView().getModel("CeritificationRequestAssets"), certificationRequestAssetId);
			} else {
				sap.m.MessageToast.show("Please enter valid data in all fields", {});
			}
			this._clearNewAsset();
		},

		removeCeritificationRequestAsset : function(){

			var oModel = this.getView().getModel("CeritificationRequestAssets");
			bizNetAccessCeritificationRequestAssets.removeCeritificationRequestAsset(this.getOwnerComponent(), oModel, oModel.getProperty("/selectedCeritificationRequestAsset/ID"));
		},

		_clearNewAsset : function(){

			this.getView().getModel("CeritificationRequestAssets").setProperty("/newCeritificationRequestAsset",{});
		}
	});

});
