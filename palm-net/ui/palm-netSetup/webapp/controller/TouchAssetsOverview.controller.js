sap.ui.define([
	"palm-netSetup/controller/BaseController",
	"palm-netSetup/model/modelsBase",
	"palm-netSetup/util/messageProvider",
	"palm-netSetup/util/localStoreTouchAssets",
	"palm-netSetup/util/bizNetAccessTouchAssets",
	"sap/ui/core/routing/History",
	"sap/ui/model/Filter",
	"sap/ui/model/FilterOperator",
	"sap/ui/Device"
], function(
		BaseController,
		modelsBase,
		messageProvider,
		localStoreTouchAssets,
		bizNetAccessTouchAssets,
		History,
		Filter,
		FilterOperator,
		Device
	) {
	"use strict";

	return BaseController.extend("palm-netSetup.controller.TouchAssetsOverview", {

		onInit : function(){

			var oList = this.byId("touchAssetsList");
			this._oList = oList;
			this._oListFilterState = {
				aFilter : [],
				aSearch : []
			};
			localStoreTouchAssets.init();
			if ( typeof this.getOwnerComponent().getModel("Messages") === "undefined" ){
				this.getOwnerComponent().setModel(modelsBase.createMessagesModel(), "Messages");
			}
			messageProvider.init(this.getOwnerComponent().getModel("Messages"));
		},

		onSelectionChange : function (oEvent) {

			this._showDetail(oEvent.getParameter("listItem") || oEvent.getSource());
		},

		onAdd : function() {

			this.getRouter().navTo("touchAsset", {touchAssetId : "___new"});
		},

		onRemoveAll : function(){

			bizNetAccessTouchAssets.removeAllTouchAssets(this.getOwnerComponent(), this.getOwnerComponent().getModel("TouchAssets"));
		},

		onSearch : function(oEvent) {

			var sQuery = oEvent.getParameter("query");
			if (sQuery) {
				this._oListFilterState.aSearch = [new Filter("ID", FilterOperator.Contains, sQuery)];
			} else {
				this._oListFilterState.aSearch = [];
			}
			this._applyFilterSearch();
			var bReplace = !Device.system.phone;
			var oModel = this.getModel("TouchAssets");
			oModel.setProperty(
				"/selectedTouchAsset",
				_.findWhere(oModel.getProperty("/touchAssetCollection/items"),
					{
						ID: oModel.getProperty("/searchTouchAssetID")
					},
				this));
			this.getRouter().navTo("touchAsset", {
				touchAssetId : oModel.getProperty("/selectedTouchAsset").ID
			}, bReplace);
		},

		_onToggleBaaS : function(){

			this.getOwnerComponent().getModel("TestSwitch").setProperty("/testMode",!(this.getView().byId("__buttonToggleTouchAssets").getPressed()));
			var location = this.getRunMode().location;
			messageProvider.addMessage("Success", "Switching Network for Blockchain Data to: ", "No Description", location, 1, "", "http://www.sap.com");
			if ( this.getRunMode().testMode && localStoreTouchAssets.getTouchAssetData() === null ){
				localStoreTouchAssets.init();
			}
			bizNetAccessTouchAssets.loadAllTouchAssets(this.getOwnerComponent(), this.getModel("TouchAssets"));
		},

		_showDetail : function(oItem) {

			var bReplace = !Device.system.phone;
			var oModel = this.getModel("TouchAssets");
			this.getRouter().navTo("touchAsset", {
				touchAssetId : oModel.getObject(oItem.getBindingContextPath()).ID
			}, bReplace);
		},

		_applyFilterSearch : function () {
			var aFilters = this._oListFilterState.aSearch.concat(this._oListFilterState.aFilter),
				oViewModel = this.getModel("TouchAssets");
			this._oList.getBinding("items").filter(aFilters, "Application");
			if (aFilters.length !== 0) {
				oViewModel.setProperty("/noDataText", this.getResourceBundle().getText("_labelNoTouchAssetsText"));
			}
		},

		onMessagePress: function() {

			this.onShowMessageDialog("palm-netSetup Log");
		},

		onNavBack : function() {
			this.getRouter().navTo("home");
		}
	});
});
