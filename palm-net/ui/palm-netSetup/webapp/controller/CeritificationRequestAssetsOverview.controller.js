sap.ui.define([
	"palm-netSetup/controller/BaseController",
	"palm-netSetup/model/modelsBase",
	"palm-netSetup/util/messageProvider",
	"palm-netSetup/util/localStoreCeritificationRequestAssets",
	"palm-netSetup/util/bizNetAccessCeritificationRequestAssets",
	"sap/ui/core/routing/History",
	"sap/ui/model/Filter",
	"sap/ui/model/FilterOperator",
	"sap/ui/Device"
], function(
		BaseController,
		modelsBase,
		messageProvider,
		localStoreCeritificationRequestAssets,
		bizNetAccessCeritificationRequestAssets,
		History,
		Filter,
		FilterOperator,
		Device
	) {
	"use strict";

	return BaseController.extend("palm-netSetup.controller.CeritificationRequestAssetsOverview", {

		onInit : function(){

			var oList = this.byId("certificationRequestAssetsList");
			this._oList = oList;
			this._oListFilterState = {
				aFilter : [],
				aSearch : []
			};
			localStoreCeritificationRequestAssets.init();
			if ( typeof this.getOwnerComponent().getModel("Messages") === "undefined" ){
				this.getOwnerComponent().setModel(modelsBase.createMessagesModel(), "Messages");
			}
			messageProvider.init(this.getOwnerComponent().getModel("Messages"));
		},

		onSelectionChange : function (oEvent) {

			this._showDetail(oEvent.getParameter("listItem") || oEvent.getSource());
		},

		onAdd : function() {

			this.getRouter().navTo("certificationRequestAsset", {certificationRequestAssetId : "___new"});
		},

		onRemoveAll : function(){

			bizNetAccessCeritificationRequestAssets.removeAllCeritificationRequestAssets(this.getOwnerComponent(), this.getOwnerComponent().getModel("CeritificationRequestAssets"));
		},

		onSearch : function(oEvent) {

			var sQuery = oEvent.getParameter("query");
			if (sQuery) {
				this._oListFilterState.aSearch = [new Filter("ID", FilterOperator.Contains, sQuery)];
			} else {
				this._oListFilterState.aSearch = [];
			}
			this._applyFilterSearch();
			var bReplace = !Device.system.phone;
			var oModel = this.getModel("CeritificationRequestAssets");
			oModel.setProperty(
				"/selectedCeritificationRequestAsset",
				_.findWhere(oModel.getProperty("/certificationRequestAssetCollection/items"),
					{
						ID: oModel.getProperty("/searchCeritificationRequestAssetID")
					},
				this));
			this.getRouter().navTo("certificationRequestAsset", {
				certificationRequestAssetId : oModel.getProperty("/selectedCeritificationRequestAsset").ID
			}, bReplace);
		},

		_onToggleBaaS : function(){

			this.getOwnerComponent().getModel("TestSwitch").setProperty("/testMode",!(this.getView().byId("__buttonToggleCeritificationRequestAssets").getPressed()));
			var location = this.getRunMode().location;
			messageProvider.addMessage("Success", "Switching Network for Blockchain Data to: ", "No Description", location, 1, "", "http://www.sap.com");
			if ( this.getRunMode().testMode && localStoreCeritificationRequestAssets.getCeritificationRequestAssetData() === null ){
				localStoreCeritificationRequestAssets.init();
			}
			bizNetAccessCeritificationRequestAssets.loadAllCeritificationRequestAssets(this.getOwnerComponent(), this.getModel("CeritificationRequestAssets"));
		},

		_showDetail : function(oItem) {

			var bReplace = !Device.system.phone;
			var oModel = this.getModel("CeritificationRequestAssets");
			this.getRouter().navTo("certificationRequestAsset", {
				certificationRequestAssetId : oModel.getObject(oItem.getBindingContextPath()).ID
			}, bReplace);
		},

		_applyFilterSearch : function () {
			var aFilters = this._oListFilterState.aSearch.concat(this._oListFilterState.aFilter),
				oViewModel = this.getModel("CeritificationRequestAssets");
			this._oList.getBinding("items").filter(aFilters, "Application");
			if (aFilters.length !== 0) {
				oViewModel.setProperty("/noDataText", this.getResourceBundle().getText("_labelNoCeritificationRequestAssetsText"));
			}
		},

		onMessagePress: function() {

			this.onShowMessageDialog("palm-netSetup Log");
		},

		onNavBack : function() {
			this.getRouter().navTo("home");
		}
	});
});
