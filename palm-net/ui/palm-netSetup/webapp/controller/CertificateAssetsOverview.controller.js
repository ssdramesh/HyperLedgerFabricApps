sap.ui.define([
	"palm-netSetup/controller/BaseController",
	"palm-netSetup/model/modelsBase",
	"palm-netSetup/util/messageProvider",
	"palm-netSetup/util/localStoreCertificateAssets",
	"palm-netSetup/util/bizNetAccessCertificateAssets",
	"sap/ui/core/routing/History",
	"sap/ui/model/Filter",
	"sap/ui/model/FilterOperator",
	"sap/ui/Device"
], function(
		BaseController,
		modelsBase,
		messageProvider,
		localStoreCertificateAssets,
		bizNetAccessCertificateAssets,
		History,
		Filter,
		FilterOperator,
		Device
	) {
	"use strict";

	return BaseController.extend("palm-netSetup.controller.CertificateAssetsOverview", {

		onInit : function(){

			var oList = this.byId("certificateAssetsList");
			this._oList = oList;
			this._oListFilterState = {
				aFilter : [],
				aSearch : []
			};
			localStoreCertificateAssets.init();
			if ( typeof this.getOwnerComponent().getModel("Messages") === "undefined" ){
				this.getOwnerComponent().setModel(modelsBase.createMessagesModel(), "Messages");
			}
			messageProvider.init(this.getOwnerComponent().getModel("Messages"));
		},

		onSelectionChange : function (oEvent) {

			this._showDetail(oEvent.getParameter("listItem") || oEvent.getSource());
		},

		onAdd : function() {

			this.getRouter().navTo("certificateAsset", {certificateAssetId : "___new"});
		},

		onRemoveAll : function(){

			bizNetAccessCertificateAssets.removeAllCertificateAssets(this.getOwnerComponent(), this.getOwnerComponent().getModel("CertificateAssets"));
		},

		onSearch : function(oEvent) {

			var sQuery = oEvent.getParameter("query");
			if (sQuery) {
				this._oListFilterState.aSearch = [new Filter("ID", FilterOperator.Contains, sQuery)];
			} else {
				this._oListFilterState.aSearch = [];
			}
			this._applyFilterSearch();
			var bReplace = !Device.system.phone;
			var oModel = this.getModel("CertificateAssets");
			oModel.setProperty(
				"/selectedCertificateAsset",
				_.findWhere(oModel.getProperty("/certificateAssetCollection/items"),
					{
						ID: oModel.getProperty("/searchCertificateAssetID")
					},
				this));
			this.getRouter().navTo("certificateAsset", {
				certificateAssetId : oModel.getProperty("/selectedCertificateAsset").ID
			}, bReplace);
		},

		_onToggleBaaS : function(){

			this.getOwnerComponent().getModel("TestSwitch").setProperty("/testMode",!(this.getView().byId("__buttonToggleCertificateAssets").getPressed()));
			var location = this.getRunMode().location;
			messageProvider.addMessage("Success", "Switching Network for Blockchain Data to: ", "No Description", location, 1, "", "http://www.sap.com");
			if ( this.getRunMode().testMode && localStoreCertificateAssets.getCertificateAssetData() === null ){
				localStoreCertificateAssets.init();
			}
			bizNetAccessCertificateAssets.loadAllCertificateAssets(this.getOwnerComponent(), this.getModel("CertificateAssets"));
		},

		_showDetail : function(oItem) {

			var bReplace = !Device.system.phone;
			var oModel = this.getModel("CertificateAssets");
			this.getRouter().navTo("certificateAsset", {
				certificateAssetId : oModel.getObject(oItem.getBindingContextPath()).ID
			}, bReplace);
		},

		_applyFilterSearch : function () {
			var aFilters = this._oListFilterState.aSearch.concat(this._oListFilterState.aFilter),
				oViewModel = this.getModel("CertificateAssets");
			this._oList.getBinding("items").filter(aFilters, "Application");
			if (aFilters.length !== 0) {
				oViewModel.setProperty("/noDataText", this.getResourceBundle().getText("_labelNoCertificateAssetsText"));
			}
		},

		onMessagePress: function() {

			this.onShowMessageDialog("palm-netSetup Log");
		},

		onNavBack : function() {
			this.getRouter().navTo("home");
		}
	});
});
