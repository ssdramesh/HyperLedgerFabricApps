sap.ui.define(function() {
	"use strict";

	return {

		mapCeritificationRequestAssetToModel:function(responseData){
			return {
				ID:responseData.ID,
				ObjectType:responseData.docType,
				
                Status:responseData.status,
                CreationDate:responseData.creationDate
,
                CompanyID:responseData.companyID,
                CertificationAuthorityID:responseData.certificationAuthorityID
			};
		},

		mapCeritificationRequestAssetsToModel:function(responseData){

			var items = [];
			if( responseData ){
				for ( var i = 0; i < responseData.length; i++ ){
					items.push(this.mapCeritificationRequestAssetToModel(responseData[i]));
				}
			}
			return items;
		},

		mapCeritificationRequestAssetToChaincode:function(oModel, newCeritificationRequestAsset){

			if ( newCeritificationRequestAsset === true ) {
				return {
						"ID":this.getNewCeritificationRequestAssetID(oModel),
						"docType":"Asset.CeritificationRequestAsset",
						
                        "status":oModel.getProperty("/newCeritificationRequestAsset/Status"),
                        "creationDate":oModel.getProperty("/newCeritificationRequestAsset/CreationDate")
,
                  "companyID":oModel.getProperty("/newCeritificationRequestAsset/CompanyID"),
                  "certificationAuthorityID":oModel.getProperty("/newCeritificationRequestAsset/CertificationAuthorityID")
				};
			} else {
				return {
						"ID":oModel.getProperty("/selectedCeritificationRequestAsset/ID"),
						"docType":"Asset.CeritificationRequestAsset",
						
                        "status":oModel.getProperty("/selectedCeritificationRequestAsset/Status"),
                        "creationDate":oModel.getProperty("/selectedCeritificationRequestAsset/CreationDate")
,
                  "companyID":oModel.getProperty("/selectedCeritificationRequestAsset/CompanyID"),
                  "certificationAuthorityID":oModel.getProperty("/selectedCeritificationRequestAsset/CertificationAuthorityID")
				};
			}
		},

		mapCeritificationRequestAssetToLocalStorage : function(oModel, newCeritificationRequestAsset){

			return this.mapCeritificationRequestAssetToChaincode(oModel, newCeritificationRequestAsset);
		},

		getNewCeritificationRequestAssetID:function(oModel){

		    if ( typeof oModel.getProperty("/newCeritificationRequestAsset/ID") === "undefined" ||
		    		oModel.getProperty("/newCeritificationRequestAsset/ID") === ""
		    	){
			    var iD = "CeritificationRequestAsset";
			    var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
			    for( var i = 0; i < 8; i++ ){
			        iD += possible.charAt(Math.floor(Math.random() * possible.length));
			    }
		    } else {
				iD = oModel.getProperty("/newCeritificationRequestAsset/ID");
			}
			oModel.setProperty("/newCeritificationRequestAsset/ID",iD);
		    return iD;
		}
	};
});
