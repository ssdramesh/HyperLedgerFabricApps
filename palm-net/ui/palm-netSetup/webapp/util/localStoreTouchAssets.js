sap.ui.define(function() {
	"use strict";

	jQuery.sap.require("jquery.sap.storage");
	var oStorage = jQuery.sap.storage(jQuery.sap.storage.Type.local);
	var touchAssetsDataID = "touchAssets";

	return {

		init: function(){

			oStorage.put(touchAssetsDataID,[]);
			oStorage.put(
				touchAssetsDataID,
[
	{
		"ID":"TouchAsset1001",
		"docType":"Asset.TouchAsset",
		"status":"Created",
		"creationDate":"12/28/2107",
		"companyID":"companyID001",
		"batchID":"batchID001",
		"latitude":"latitude001",
		"longitude":"longitude001",
		"description":"description001",
		"temperature":"temperature001"
	}
]				
			);
		},

		getTouchAssetDataID : function(){

			return touchAssetsDataID;
		},

		getTouchAssetData  : function(){

			return oStorage.get("touchAssets");
		},

		put: function(newTouchAsset){

			var touchAssetData = this.getTouchAssetData();
			touchAssetData.push(newTouchAsset);
			oStorage.put(touchAssetsDataID, touchAssetData);
		},

		remove : function (id){

			var touchAssetData = this.getTouchAssetData();
			touchAssetData = _.without(touchAssetData,_.findWhere(touchAssetData,{ID:id}));
			oStorage.put(touchAssetsDataID, touchAssetData);
		},

		removeAll : function(){

			oStorage.put(touchAssetsDataID,[]);
		},

		clearTouchAssetData: function(){

			oStorage.put(touchAssetsDataID,[]);
		}
	};
});
