sap.ui.define(function() {
	"use strict";

	jQuery.sap.require("jquery.sap.storage");
	var oStorage = jQuery.sap.storage(jQuery.sap.storage.Type.local);
	var certificateAssetsDataID = "certificateAssets";

	return {

		init: function(){

			oStorage.put(certificateAssetsDataID,[]);
			oStorage.put(
				certificateAssetsDataID,
[
	{
		"ID":"CertificateAsset1001",
		"docType":"Asset.CertificateAsset",
		"status":"Created",
		"creationDate":"12/28/2107",
		"certificationAuthorityID":"certificationAuthorityID001",
		"companyID":"companyID001",
		"issueDate":"issueDate001",
		"validityStartDate":"validityStartDate001",
		"validityEndDate":"validityEndDate001"
	}
]				
			);
		},

		getCertificateAssetDataID : function(){

			return certificateAssetsDataID;
		},

		getCertificateAssetData  : function(){

			return oStorage.get("certificateAssets");
		},

		put: function(newCertificateAsset){

			var certificateAssetData = this.getCertificateAssetData();
			certificateAssetData.push(newCertificateAsset);
			oStorage.put(certificateAssetsDataID, certificateAssetData);
		},

		remove : function (id){

			var certificateAssetData = this.getCertificateAssetData();
			certificateAssetData = _.without(certificateAssetData,_.findWhere(certificateAssetData,{ID:id}));
			oStorage.put(certificateAssetsDataID, certificateAssetData);
		},

		removeAll : function(){

			oStorage.put(certificateAssetsDataID,[]);
		},

		clearCertificateAssetData: function(){

			oStorage.put(certificateAssetsDataID,[]);
		}
	};
});
