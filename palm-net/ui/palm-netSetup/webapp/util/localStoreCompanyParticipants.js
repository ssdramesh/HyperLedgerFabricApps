sap.ui.define(function() {
	"use strict";

	jQuery.sap.require("jquery.sap.storage");
	var oStorage = jQuery.sap.storage(jQuery.sap.storage.Type.local);
	var companyParticipantsDataID = "companyParticipants";

	return {

		init: function(){

			oStorage.put(companyParticipantsDataID,[]);
			oStorage.put(
				companyParticipantsDataID,
[
	{
		"ID":"CompanyParticipant1001",
		"docType":"Participant.CompanyParticipant",
		"alias":"BRITECH",
		"description":"British Technology Pvt. Ltd.",
    "registrationID":"registrationID001",
    "certificationID":"certificationID001",
    "type":"type001",
    "name":"name001"
  }
]				
			);
		},

		getCompanyParticipantDataID : function(){

			return companyParticipantsDataID;
		},

		getCompanyParticipantData  : function(){

			return oStorage.get("companyParticipants");
		},

		put: function(newCompanyParticipant){

			var companyParticipantData = this.getCompanyParticipantData();
			companyParticipantData.push(newCompanyParticipant);
			oStorage.put(companyParticipantsDataID, companyParticipantData);
		},

		remove : function (id){

			var companyParticipantData = this.getCompanyParticipantData();
			companyParticipantData = _.without(companyParticipantData,_.findWhere(companyParticipantData,{ID:id}));
			oStorage.put(companyParticipantsDataID, companyParticipantData);
		},

		removeAll : function(){

			oStorage.put(companyParticipantsDataID,[]);
		},

		clearCompanyParticipantData: function(){

			oStorage.put(companyParticipantsDataID,[]);
		}
	};
});
