sap.ui.define(function() {
	"use strict";

	jQuery.sap.require("jquery.sap.storage");
	var oStorage = jQuery.sap.storage(jQuery.sap.storage.Type.local);
	var administratorParticipantsDataID = "administratorParticipants";

	return {

		init: function(){

			oStorage.put(administratorParticipantsDataID,[]);
			oStorage.put(
				administratorParticipantsDataID,
[
	{
		"ID":"AdministratorParticipant1001",
		"docType":"Participant.AdministratorParticipant",
		"alias":"BRITECH",
		"description":"British Technology Pvt. Ltd."
  }
]				
			);
		},

		getAdministratorParticipantDataID : function(){

			return administratorParticipantsDataID;
		},

		getAdministratorParticipantData  : function(){

			return oStorage.get("administratorParticipants");
		},

		put: function(newAdministratorParticipant){

			var administratorParticipantData = this.getAdministratorParticipantData();
			administratorParticipantData.push(newAdministratorParticipant);
			oStorage.put(administratorParticipantsDataID, administratorParticipantData);
		},

		remove : function (id){

			var administratorParticipantData = this.getAdministratorParticipantData();
			administratorParticipantData = _.without(administratorParticipantData,_.findWhere(administratorParticipantData,{ID:id}));
			oStorage.put(administratorParticipantsDataID, administratorParticipantData);
		},

		removeAll : function(){

			oStorage.put(administratorParticipantsDataID,[]);
		},

		clearAdministratorParticipantData: function(){

			oStorage.put(administratorParticipantsDataID,[]);
		}
	};
});
