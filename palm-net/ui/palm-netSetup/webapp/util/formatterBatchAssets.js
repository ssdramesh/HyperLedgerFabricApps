sap.ui.define(function() {
	"use strict";

	return {

		mapBatchAssetToModel:function(responseData){
			return {
				ID:responseData.ID,
				ObjectType:responseData.docType,
				
                Status:responseData.status,
                CreationDate:responseData.creationDate
,
                Volume:responseData.volume,
                FatC16:responseData.fatC16,
                FatC18:responseData.fatC18,
                ProductID:responseData.productID,
                ProductDescription:responseData.productDescription
			};
		},

		mapBatchAssetsToModel:function(responseData){

			var items = [];
			if( responseData ){
				for ( var i = 0; i < responseData.length; i++ ){
					items.push(this.mapBatchAssetToModel(responseData[i]));
				}
			}
			return items;
		},

		mapBatchAssetToChaincode:function(oModel, newBatchAsset){

			if ( newBatchAsset === true ) {
				return {
						"ID":this.getNewBatchAssetID(oModel),
						"docType":"Asset.BatchAsset",
						
                        "status":oModel.getProperty("/newBatchAsset/Status"),
                        "creationDate":oModel.getProperty("/newBatchAsset/CreationDate")
,
                  "volume":oModel.getProperty("/newBatchAsset/Volume"),
                  "fatC16":oModel.getProperty("/newBatchAsset/FatC16"),
                  "fatC18":oModel.getProperty("/newBatchAsset/FatC18"),
                  "productID":oModel.getProperty("/newBatchAsset/ProductID"),
                  "productDescription":oModel.getProperty("/newBatchAsset/ProductDescription")
				};
			} else {
				return {
						"ID":oModel.getProperty("/selectedBatchAsset/ID"),
						"docType":"Asset.BatchAsset",
						
                        "status":oModel.getProperty("/selectedBatchAsset/Status"),
                        "creationDate":oModel.getProperty("/selectedBatchAsset/CreationDate")
,
                  "volume":oModel.getProperty("/selectedBatchAsset/Volume"),
                  "fatC16":oModel.getProperty("/selectedBatchAsset/FatC16"),
                  "fatC18":oModel.getProperty("/selectedBatchAsset/FatC18"),
                  "productID":oModel.getProperty("/selectedBatchAsset/ProductID"),
                  "productDescription":oModel.getProperty("/selectedBatchAsset/ProductDescription")
				};
			}
		},

		mapBatchAssetToLocalStorage : function(oModel, newBatchAsset){

			return this.mapBatchAssetToChaincode(oModel, newBatchAsset);
		},

		getNewBatchAssetID:function(oModel){

		    if ( typeof oModel.getProperty("/newBatchAsset/ID") === "undefined" ||
		    		oModel.getProperty("/newBatchAsset/ID") === ""
		    	){
			    var iD = "BatchAsset";
			    var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
			    for( var i = 0; i < 8; i++ ){
			        iD += possible.charAt(Math.floor(Math.random() * possible.length));
			    }
		    } else {
				iD = oModel.getProperty("/newBatchAsset/ID");
			}
			oModel.setProperty("/newBatchAsset/ID",iD);
		    return iD;
		}
	};
});
