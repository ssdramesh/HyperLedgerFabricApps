sap.ui.define(function() {
	"use strict";

	return {

		mapCertificateAssetToModel:function(responseData){
			return {
				ID:responseData.ID,
				ObjectType:responseData.docType,
				
                Status:responseData.status,
                CreationDate:responseData.creationDate
,
                CertificationAuthorityID:responseData.certificationAuthorityID,
                CompanyID:responseData.companyID,
                IssueDate:responseData.issueDate,
                ValidityStartDate:responseData.validityStartDate,
                ValidityEndDate:responseData.validityEndDate
			};
		},

		mapCertificateAssetsToModel:function(responseData){

			var items = [];
			if( responseData ){
				for ( var i = 0; i < responseData.length; i++ ){
					items.push(this.mapCertificateAssetToModel(responseData[i]));
				}
			}
			return items;
		},

		mapCertificateAssetToChaincode:function(oModel, newCertificateAsset){

			if ( newCertificateAsset === true ) {
				return {
						"ID":this.getNewCertificateAssetID(oModel),
						"docType":"Asset.CertificateAsset",
						
                        "status":oModel.getProperty("/newCertificateAsset/Status"),
                        "creationDate":oModel.getProperty("/newCertificateAsset/CreationDate")
,
                  "certificationAuthorityID":oModel.getProperty("/newCertificateAsset/CertificationAuthorityID"),
                  "companyID":oModel.getProperty("/newCertificateAsset/CompanyID"),
                  "issueDate":oModel.getProperty("/newCertificateAsset/IssueDate"),
                  "validityStartDate":oModel.getProperty("/newCertificateAsset/ValidityStartDate"),
                  "validityEndDate":oModel.getProperty("/newCertificateAsset/ValidityEndDate")
				};
			} else {
				return {
						"ID":oModel.getProperty("/selectedCertificateAsset/ID"),
						"docType":"Asset.CertificateAsset",
						
                        "status":oModel.getProperty("/selectedCertificateAsset/Status"),
                        "creationDate":oModel.getProperty("/selectedCertificateAsset/CreationDate")
,
                  "certificationAuthorityID":oModel.getProperty("/selectedCertificateAsset/CertificationAuthorityID"),
                  "companyID":oModel.getProperty("/selectedCertificateAsset/CompanyID"),
                  "issueDate":oModel.getProperty("/selectedCertificateAsset/IssueDate"),
                  "validityStartDate":oModel.getProperty("/selectedCertificateAsset/ValidityStartDate"),
                  "validityEndDate":oModel.getProperty("/selectedCertificateAsset/ValidityEndDate")
				};
			}
		},

		mapCertificateAssetToLocalStorage : function(oModel, newCertificateAsset){

			return this.mapCertificateAssetToChaincode(oModel, newCertificateAsset);
		},

		getNewCertificateAssetID:function(oModel){

		    if ( typeof oModel.getProperty("/newCertificateAsset/ID") === "undefined" ||
		    		oModel.getProperty("/newCertificateAsset/ID") === ""
		    	){
			    var iD = "CertificateAsset";
			    var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
			    for( var i = 0; i < 8; i++ ){
			        iD += possible.charAt(Math.floor(Math.random() * possible.length));
			    }
		    } else {
				iD = oModel.getProperty("/newCertificateAsset/ID");
			}
			oModel.setProperty("/newCertificateAsset/ID",iD);
		    return iD;
		}
	};
});
