sap.ui.define([
	"palm-netSetup/util/restBuilder",
	"palm-netSetup/util/formatterCeritificationRequestAssets",
	"palm-netSetup/util/localStoreCeritificationRequestAssets"
], function(
		restBuilder,
		formatterCeritificationRequestAssets,
		localStoreCeritificationRequestAssets
	) {
	"use strict";

	return {

		loadAllCeritificationRequestAssets:function(oComponent, oModel) {

			if ( !( oComponent.getModel("TestSwitch").getProperty("/testMode") ) ){
				restBuilder.execute(
					oModel.getProperty("/chaincode"),
					"readAll",
					[],
					function(responseData){
						oModel.setProperty(
							"/certificationRequestAssetCollection/items",
							formatterCeritificationRequestAssets.mapCeritificationRequestAssetsToModel(responseData)
						);
					});
			} else {
				oModel.setProperty(
					"/certificationRequestAssetCollection/items",
					formatterCeritificationRequestAssets.mapCeritificationRequestAssetsToModel(localStoreCeritificationRequestAssets.getCeritificationRequestAssetData())
				);
			}
		},

		loadCeritificationRequestAsset:function(oModel, selectedCeritificationRequestAssetID){

			oModel.setProperty(
				"/selectedCeritificationRequestAsset",
				_.findWhere(oModel.getProperty("/certificationRequestAssetCollection/items"),
					{
						ID: selectedCeritificationRequestAssetID
					},
				this));
		},

		addNewCeritificationRequestAsset:function(oComponent, oModel){

			if ( !( oComponent.getModel("TestSwitch").getProperty("/testMode") ) ){
				restBuilder.execute(
					oModel.getProperty("/chaincode"),
					"addNew",
					formatterCeritificationRequestAssets.mapCeritificationRequestAssetToChaincode(oModel, true)
				);
			}  else {
				localStoreCeritificationRequestAssets.put(formatterCeritificationRequestAssets.mapCeritificationRequestAssetToLocalStorage(oModel, true));
			}
			this.loadAllCeritificationRequestAssets(oComponent, oModel);
			return oModel.getProperty("/newCeritificationRequestAsset/ID");
		},

		removeCeritificationRequestAsset : function(oComponent, oModel, certificationRequestAssetID){

			if ( !( oComponent.getModel("TestSwitch").getProperty("/testMode") ) ) {
				restBuilder.execute(
					oModel.getProperty("/chaincode"),
					"remove",
					{ID:certificationRequestAssetID}
				);
			} else {
				localStoreCeritificationRequestAssets.remove(certificationRequestAssetID);
			}
			this.loadAllCeritificationRequestAssets(oComponent, oModel);
			return true;
		},

		removeAllCeritificationRequestAssets : function(oComponent, oModel){

			if (!(oComponent.getModel("TestSwitch").getProperty("/testMode"))) {
				restBuilder.execute(
					oModel.getProperty("/chaincode"),
					"removeAll",
					[]
				);
			} else {
				localStoreCeritificationRequestAssets.removeAll();
			}
			this.loadAllCeritificationRequestAssets(oComponent, oModel);
			oModel.setProperty("/selectedCeritificationRequestAsset",{});
			return true;
		}
	};
});
