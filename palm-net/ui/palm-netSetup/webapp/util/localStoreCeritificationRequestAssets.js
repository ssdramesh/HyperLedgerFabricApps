sap.ui.define(function() {
	"use strict";

	jQuery.sap.require("jquery.sap.storage");
	var oStorage = jQuery.sap.storage(jQuery.sap.storage.Type.local);
	var certificationRequestAssetsDataID = "certificationRequestAssets";

	return {

		init: function(){

			oStorage.put(certificationRequestAssetsDataID,[]);
			oStorage.put(
				certificationRequestAssetsDataID,
[
	{
		"ID":"CeritificationRequestAsset1001",
		"docType":"Asset.CeritificationRequestAsset",
		"status":"Created",
		"creationDate":"12/28/2107",
		"companyID":"companyID001",
		"certificationAuthorityID":"certificationAuthorityID001"
	}
]				
			);
		},

		getCeritificationRequestAssetDataID : function(){

			return certificationRequestAssetsDataID;
		},

		getCeritificationRequestAssetData  : function(){

			return oStorage.get("certificationRequestAssets");
		},

		put: function(newCeritificationRequestAsset){

			var certificationRequestAssetData = this.getCeritificationRequestAssetData();
			certificationRequestAssetData.push(newCeritificationRequestAsset);
			oStorage.put(certificationRequestAssetsDataID, certificationRequestAssetData);
		},

		remove : function (id){

			var certificationRequestAssetData = this.getCeritificationRequestAssetData();
			certificationRequestAssetData = _.without(certificationRequestAssetData,_.findWhere(certificationRequestAssetData,{ID:id}));
			oStorage.put(certificationRequestAssetsDataID, certificationRequestAssetData);
		},

		removeAll : function(){

			oStorage.put(certificationRequestAssetsDataID,[]);
		},

		clearCeritificationRequestAssetData: function(){

			oStorage.put(certificationRequestAssetsDataID,[]);
		}
	};
});
