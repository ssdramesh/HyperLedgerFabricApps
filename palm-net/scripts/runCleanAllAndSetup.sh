#!/bin/bash

cd /Users/i047582/Documents/Workspaces/git/github.wdf.sap.corp/I047582/HyperLedgerFabricApps/palm-net/test/newman/CleanAllAndSetup


echo "Clean All AdministratorParticipants before Setup..."
newman run cleanAllAdministratorParticipants.postman_collection.json -e palm-net.postman_environment.json --bail newman
echo "Setting up AdministratorParticipants..."
newman run createAllAdministratorParticipants.postman_collection.json -e palm-net.postman_environment.json -d ./data/AdministratorParticipants.json --bail newman


echo "Clean All BatchAssets before Setup..."
newman run cleanAllBatchAssets.postman_collection.json -e palm-net.postman_environment.json --bail newman
echo "Setting up BatchAssets..."
newman run createAllBatchAssets.postman_collection.json -e palm-net.postman_environment.json -d ./data/BatchAssets.json --bail newman


echo "Clean All CertificateAssets before Setup..."
newman run cleanAllCertificateAssets.postman_collection.json -e palm-net.postman_environment.json --bail newman
echo "Setting up CertificateAssets..."
newman run createAllCertificateAssets.postman_collection.json -e palm-net.postman_environment.json -d ./data/CertificateAssets.json --bail newman


echo "Clean All CertificationAuthorityParticipants before Setup..."
newman run cleanAllCertificationAuthorityParticipants.postman_collection.json -e palm-net.postman_environment.json --bail newman
echo "Setting up CertificationAuthorityParticipants..."
newman run createAllCertificationAuthorityParticipants.postman_collection.json -e palm-net.postman_environment.json -d ./data/CertificationAuthorityParticipants.json --bail newman


echo "Clean All CeritificationRequestAssets before Setup..."
newman run cleanAllCeritificationRequestAssets.postman_collection.json -e palm-net.postman_environment.json --bail newman
echo "Setting up CeritificationRequestAssets..."
newman run createAllCeritificationRequestAssets.postman_collection.json -e palm-net.postman_environment.json -d ./data/CeritificationRequestAssets.json --bail newman


echo "Clean All CompanyParticipants before Setup..."
newman run cleanAllCompanyParticipants.postman_collection.json -e palm-net.postman_environment.json --bail newman
echo "Setting up CompanyParticipants..."
newman run createAllCompanyParticipants.postman_collection.json -e palm-net.postman_environment.json -d ./data/CompanyParticipants.json --bail newman


echo "Clean All TouchAssets before Setup..."
newman run cleanAllTouchAssets.postman_collection.json -e palm-net.postman_environment.json --bail newman
echo "Setting up TouchAssets..."
newman run createAllTouchAssets.postman_collection.json -e palm-net.postman_environment.json -d ./data/TouchAssets.json --bail newman

echo "Done. Ready to run!"
