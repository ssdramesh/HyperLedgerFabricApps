package main

//Template Version: 1.1-20180704
import (
	"encoding/json"
	"errors"
	"fmt"

	"github.com/hyperledger/fabric/core/chaincode/shim"
	"github.com/hyperledger/fabric/protos/peer"
)

var logger = shim.NewLogger("CLDChaincode")

//CeritificationRequestAsset - Chaincode for asset CeritificationRequest
type CeritificationRequestAsset struct {
}

//CeritificationRequest - Details of the asset type CeritificationRequest
type CeritificationRequest struct {
 ID          string `json:"ID"`
 ObjectType   string `json:"docType"`
  Status       string `json:"status"`
  CreationDate string `json:"creationDate"`
CompanyID string `json:"companyID"`
CertificationAuthorityID string `json:"certificationAuthorityID"`
}

//CeritificationRequestIDIndex - Index on IDs for retrieval all CeritificationRequests
type CeritificationRequestIDIndex struct {
	IDs []string `json:"IDs"`
}

func main() {
	err := shim.Start(new(CeritificationRequestAsset))
	if err != nil {
		fmt.Printf("Error starting CeritificationRequestAsset chaincode function main(): %s", err)
	} else {
		fmt.Printf("Starting CeritificationRequestAsset chaincode function main() executed successfully")
	}
}

//Init - The chaincode Init function: No  arguments, only initializes a ID array as Index for retrieval of all CeritificationRequests
func (creq *CeritificationRequestAsset) Init(stub shim.ChaincodeStubInterface) peer.Response {
	var certificationRequestIDIndex CeritificationRequestIDIndex
	record, _ := stub.GetState("certificationRequestIDIndex")
	//chaincode upgrade: Index might already exist, create new only if it does not
	if record == nil {
		bytes, _ := json.Marshal(certificationRequestIDIndex)
		stub.PutState("certificationRequestIDIndex", bytes)
	}
	return shim.Success(nil)
}

//Invoke - The chaincode Invoke function:
func (creq *CeritificationRequestAsset) Invoke(stub shim.ChaincodeStubInterface) peer.Response {
	function, args := stub.GetFunctionAndParameters()
	logger.Debug("function: ", function)
	switch function {
	case "addNewCeritificationRequest":
		return creq.addNewCeritificationRequest(stub, args)
	case "removeCeritificationRequest":
		return creq.removeCeritificationRequest(stub, args[0])
	case "removeAllCeritificationRequests":
		return creq.removeAllCeritificationRequests(stub)
	case "readCeritificationRequest":
		return creq.readCeritificationRequest(stub, args[0])
	case "readAllCeritificationRequests":
		return creq.readAllCeritificationRequests(stub)
	default:
		return shim.Error("Received unknown function invocation")
	}
}

//Invoke Route: addNewCeritificationRequest
func (creq *CeritificationRequestAsset) addNewCeritificationRequest(stub shim.ChaincodeStubInterface, args []string) peer.Response {
	certificationRequest, err := getCeritificationRequestFromArgs(args)
	if err != nil {
		return shim.Error("CeritificationRequest Data is Corrupted")
	}
	certificationRequest.ObjectType = "Asset.CeritificationRequestAsset"
	record, err := stub.GetState(certificationRequest.ID)
	if record != nil {
		return shim.Error("This CeritificationRequest already exists: " + certificationRequest.ID)
	}
	_, err = creq.saveCeritificationRequest(stub, certificationRequest)
	if err != nil {
		return shim.Error(err.Error())
	}
	_, err = creq.updateCeritificationRequestIDIndex(stub, certificationRequest)
	if err != nil {
		return shim.Error(err.Error())
	}
	return shim.Success(nil)
}

//Invoke Route: removeCeritificationRequest
func (creq *CeritificationRequestAsset) removeCeritificationRequest(stub shim.ChaincodeStubInterface, certificationRequestID string) peer.Response {
	_, err := creq.deleteCeritificationRequest(stub, certificationRequestID)
	if err != nil {
		return shim.Error(err.Error())
	}
	_, err = creq.deleteCeritificationRequestIDIndex(stub, certificationRequestID)
	if err != nil {
		return shim.Error(err.Error())
	}
	return shim.Success(nil)
}

//Invoke Route: removeAllCeritificationRequests
func (creq *CeritificationRequestAsset) removeAllCeritificationRequests(stub shim.ChaincodeStubInterface) peer.Response {
	var certificationRequestIDIndex CeritificationRequestIDIndex
	bytes, err := stub.GetState("certificationRequestIDIndex")
	if err != nil {
		return shim.Error("removeAllCeritificationRequests: Error getting certificationRequestIDIndex array")
	}
	err = json.Unmarshal(bytes, &certificationRequestIDIndex)
	if err != nil {
		return shim.Error("removeAllCeritificationRequests: Error unmarshalling certificationRequestIDIndex array JSON")
	}
	if len(certificationRequestIDIndex.IDs) == 0 {
		return shim.Error("removeAllCeritificationRequests: No certificationRequests to remove")
	}
	for _, certificationRequestStructID := range certificationRequestIDIndex.IDs {
		_, err = creq.deleteCeritificationRequest(stub, certificationRequestStructID)
		if err != nil {
			return shim.Error("Failed to remove CeritificationRequest with ID: " + certificationRequestStructID)
		}
		_, err = creq.deleteCeritificationRequestIDIndex(stub, certificationRequestStructID)
		if err != nil {
			return shim.Error(err.Error())
		}
	}
	creq.initHolder(stub)
	return shim.Success(nil)
}

//Query Route: readCeritificationRequest
func (creq *CeritificationRequestAsset) readCeritificationRequest(stub shim.ChaincodeStubInterface, certificationRequestID string) peer.Response {
	certificationRequestAsByteArray, err := creq.retrieveCeritificationRequest(stub, certificationRequestID)
	if err != nil {
		return shim.Error(err.Error())
	}
	return shim.Success(certificationRequestAsByteArray)
}

//Query Route: readAllCeritificationRequests
func (creq *CeritificationRequestAsset) readAllCeritificationRequests(stub shim.ChaincodeStubInterface) peer.Response {
	var certificationRequestIDs CeritificationRequestIDIndex
	bytes, err := stub.GetState("certificationRequestIDIndex")
	if err != nil {
		return shim.Error("readAllCeritificationRequests: Error getting certificationRequestIDIndex array")
	}
	err = json.Unmarshal(bytes, &certificationRequestIDs)
	if err != nil {
		return shim.Error("readAllCeritificationRequests: Error unmarshalling certificationRequestIDIndex array JSON")
	}
	result := "["

	var certificationRequestAsByteArray []byte

	for _, certificationRequestID := range certificationRequestIDs.IDs {
		certificationRequestAsByteArray, err = creq.retrieveCeritificationRequest(stub, certificationRequestID)
		if err != nil {
			return shim.Error("Failed to retrieve certificationRequest with ID: " + certificationRequestID)
		}
		result += string(certificationRequestAsByteArray) + ","
	}
	if len(result) == 1 {
		result = "[]"
	} else {
		result = result[:len(result)-1] + "]"
	}
	return shim.Success([]byte(result))
}

//Helper: Save CeritificationRequestAsset
func (creq *CeritificationRequestAsset) saveCeritificationRequest(stub shim.ChaincodeStubInterface, certificationRequest CeritificationRequest) (bool, error) {
	bytes, err := json.Marshal(certificationRequest)
	if err != nil {
		return false, errors.New("Error converting certificationRequest record JSON")
	}
	err = stub.PutState(certificationRequest.ID, bytes)
	if err != nil {
		return false, errors.New("Error storing CeritificationRequest record")
	}
	return true, nil
}

//Helper: delete CeritificationRequestAsset
func (creq *CeritificationRequestAsset) deleteCeritificationRequest(stub shim.ChaincodeStubInterface, certificationRequestID string) (bool, error) {
	_, err := creq.retrieveCeritificationRequest(stub, certificationRequestID)
	if err != nil {
		return false, errors.New("CeritificationRequest with ID: " + certificationRequestID + " not found")
	}
	err = stub.DelState(certificationRequestID)
	if err != nil {
		return false, errors.New("Error deleting CeritificationRequest record")
	}
	return true, nil
}

//Helper: Update certificationRequest Holder - updates Index
func (creq *CeritificationRequestAsset) updateCeritificationRequestIDIndex(stub shim.ChaincodeStubInterface, certificationRequest CeritificationRequest) (bool, error) {
	var certificationRequestIDs CeritificationRequestIDIndex
	bytes, err := stub.GetState("certificationRequestIDIndex")
	if err != nil {
		return false, errors.New("updateCeritificationRequestIDIndex: Error getting certificationRequestIDIndex array Index from state")
	}
	err = json.Unmarshal(bytes, &certificationRequestIDs)
	if err != nil {
		return false, errors.New("updateCeritificationRequestIDIndex: Error unmarshalling certificationRequestIDIndex array JSON")
	}
	certificationRequestIDs.IDs = append(certificationRequestIDs.IDs, certificationRequest.ID)
	bytes, err = json.Marshal(certificationRequestIDs)
	if err != nil {
		return false, errors.New("updateCeritificationRequestIDIndex: Error marshalling new certificationRequest ID")
	}
	err = stub.PutState("certificationRequestIDIndex", bytes)
	if err != nil {
		return false, errors.New("updateCeritificationRequestIDIndex: Error storing new certificationRequest ID in certificationRequestIDIndex (Index)")
	}
	return true, nil
}

//Helper: delete ID from certificationRequestStruct Holder
func (creq *CeritificationRequestAsset) deleteCeritificationRequestIDIndex(stub shim.ChaincodeStubInterface, certificationRequestID string) (bool, error) {
	var certificationRequestIDIndex CeritificationRequestIDIndex
	bytes, err := stub.GetState("certificationRequestIDIndex")
	if err != nil {
		return false, errors.New("deleteCeritificationRequestIDIndex: Error getting certificationRequestIDIndex array Index from state")
	}
	err = json.Unmarshal(bytes, &certificationRequestIDIndex)
	if err != nil {
		return false, errors.New("deleteCeritificationRequestIDIndex: Error unmarshalling certificationRequestIDIndex array JSON")
	}
	certificationRequestIDIndex.IDs, err = deleteKeyFromStringArray(certificationRequestIDIndex.IDs, certificationRequestID)
	if err != nil {
		return false, errors.New(err.Error())
	}
	bytes, err = json.Marshal(certificationRequestIDIndex)
	if err != nil {
		return false, errors.New("deleteCeritificationRequestIDIndex: Error marshalling new certificationRequestStruct ID")
	}
	err = stub.PutState("certificationRequestIDIndex", bytes)
	if err != nil {
		return false, errors.New("deleteCeritificationRequestIDIndex: Error storing new certificationRequestStruct ID in certificationRequestIDIndex (Index)")
	}
	return true, nil
}

//Helper: Initialize truck ID Holder //change template
func (creq *CeritificationRequestAsset) initHolder(stub shim.ChaincodeStubInterface) bool {
	var certificationRequestIDIndex CeritificationRequestIDIndex
	bytes, _ := json.Marshal(certificationRequestIDIndex)
	stub.DelState("certificationRequestIDIndex")
	stub.PutState("certificationRequestIDIndex", bytes)
	return true
}

//deleteKeyFromArray
func deleteKeyFromStringArray(array []string, key string) (newArray []string, err error) {
	for _, entry := range array {
		if entry != key {
			newArray = append(newArray, entry)
		}
	}
	if len(newArray) == len(array) {
		return newArray, errors.New("Specified Key: " + key + " not found in Array")
	}
	return newArray, nil
}

//Helper: Retrieve purchaser
func (creq *CeritificationRequestAsset) retrieveCeritificationRequest(stub shim.ChaincodeStubInterface, certificationRequestID string) ([]byte, error) {
	var certificationRequest CeritificationRequest
	var certificationRequestAsByteArray []byte
	bytes, err := stub.GetState(certificationRequestID)
	if err != nil {
		return certificationRequestAsByteArray, errors.New("retrieveCeritificationRequest: Error retrieving certificationRequest with ID: " + certificationRequestID)
	}
	err = json.Unmarshal(bytes, &certificationRequest)
	if err != nil {
		return certificationRequestAsByteArray, errors.New("retrieveCeritificationRequest: Corrupt certificationRequest record " + string(bytes))
	}
	certificationRequestAsByteArray, err = json.Marshal(certificationRequest)
	if err != nil {
		return certificationRequestAsByteArray, errors.New("readCeritificationRequest: Invalid certificationRequest Object - Not a  valid JSON")
	}
	return certificationRequestAsByteArray, nil
}

//getCeritificationRequestFromArgs - construct a certificationRequest structure from string array of arguments
func getCeritificationRequestFromArgs(args []string) (certificationRequest CeritificationRequest, err error) {

	if !Valid(args[0]) {
		return certificationRequest, errors.New("Invalid json")
	}

	err = Unmarshal([]byte(args[0]), &certificationRequest)
	if err != nil {
		return certificationRequest, err
	}
	return certificationRequest, nil
}
