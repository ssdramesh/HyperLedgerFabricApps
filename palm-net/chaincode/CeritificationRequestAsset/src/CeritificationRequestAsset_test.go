package main

//Template Version: 1.1-20180704
import (
	"bytes"
	"encoding/json"
	"fmt"
	"strings"
	"testing"

	"github.com/hyperledger/fabric/core/chaincode/shim"
)

//TestCeritificationRequestAsset_Init
func TestCeritificationRequestAsset_Init(t *testing.T) {
	certificationRequest := new(CeritificationRequestAsset)
	stub := shim.NewMockStub("certificationRequest", certificationRequest)
	checkInit(t, stub, [][]byte{[]byte("init")})
	checkState(t, stub, "certificationRequestIDIndex", getExpectedCeritificationRequestIDIndex("init"))
}

//TestCeritificationRequestAsset_InvokeUnknownFunction
func TestCeritificationRequestAsset_InvokeUnknownFunction(t *testing.T) {
	certificationRequest := new(CeritificationRequestAsset)
	stub := shim.NewMockStub("certificationRequest", certificationRequest)
	checkInit(t, stub, [][]byte{[]byte("init")})
	checkInvokeUnknownFunction(t, stub, [][]byte{[]byte("myFunction"), []byte("docType:Asset")})
}

//TestCeritificationRequestAsset_Invoke_addNewCeritificationRequest
func TestCeritificationRequestAsset_Invoke_addNewCeritificationRequestOK(t *testing.T) {
	certificationRequest := new(CeritificationRequestAsset)
	stub := shim.NewMockStub("certificationRequest", certificationRequest)
	checkInit(t, stub, [][]byte{[]byte("init")})
	checkInvoke(t, stub, getFirstCeritificationRequestAssetForTesting())
	newCeritificationRequestID := "100001"
	checkState(t, stub, newCeritificationRequestID, getNewCeritificationRequestExpected())
	checkState(t, stub, "certificationRequestIDIndex", getExpectedCeritificationRequestIDIndex("addNewCeritificationRequest"))
}

//TestCeritificationRequestAsset_Invoke_addNewCeritificationRequest
func TestCeritificationRequestAsset_Invoke_addNewCeritificationRequestDuplicate(t *testing.T) {
	certificationRequest := new(CeritificationRequestAsset)
	stub := shim.NewMockStub("certificationRequest", certificationRequest)
	checkInit(t, stub, [][]byte{[]byte("init")})
	checkInvoke(t, stub, getFirstCeritificationRequestAssetForTesting())
	newCeritificationRequestID := "100001"
	checkState(t, stub, newCeritificationRequestID, getNewCeritificationRequestExpected())
	checkState(t, stub, "certificationRequestIDIndex", getExpectedCeritificationRequestIDIndex("addNewCeritificationRequest"))
	res := stub.MockInvoke("1", getFirstCeritificationRequestAssetForTesting())
	if res.Status != shim.OK {
		checkError(t, "This CeritificationRequest already exists: 100001", res.Message)
	} else {
		fmt.Println("Error was expected, but not raised")
		t.FailNow()
	}
}

//TestCeritificationRequestAsset_Invoke_removeCeritificationRequestOK  //change template
func TestCeritificationRequestAsset_Invoke_removeCeritificationRequestOK(t *testing.T) {
	certificationRequest := new(CeritificationRequestAsset)
	stub := shim.NewMockStub("certificationRequest", certificationRequest)
	checkInit(t, stub, [][]byte{[]byte("init")})
	checkInvoke(t, stub, getFirstCeritificationRequestAssetForTesting())
	checkInvoke(t, stub, getSecondCeritificationRequestAssetForTesting())
	checkReadAllCeritificationRequestsOK(t, stub)
	checkState(t, stub, "certificationRequestIDIndex", getExpectedCeritificationRequestIDIndex("beforeRemoveCeritificationRequest"))
	checkInvoke(t, stub, getRemoveSecondCeritificationRequestAssetForTesting())
	remainingCeritificationRequestID := "100001"
	checkReadCeritificationRequestOK(t, stub, remainingCeritificationRequestID)
	checkState(t, stub, "certificationRequestIDIndex", getExpectedCeritificationRequestIDIndex("afterRemoveCeritificationRequest"))
}

//TestCeritificationRequestAsset_Invoke_removeCeritificationRequestNOK  //change template
func TestCeritificationRequestAsset_Invoke_removeCeritificationRequestNOK(t *testing.T) {
	certificationRequest := new(CeritificationRequestAsset)
	stub := shim.NewMockStub("certificationRequest", certificationRequest)
	checkInit(t, stub, [][]byte{[]byte("init")})
	checkInvoke(t, stub, getFirstCeritificationRequestAssetForTesting())
	firstCeritificationRequestID := "100001"
	checkReadCeritificationRequestOK(t, stub, firstCeritificationRequestID)
	checkState(t, stub, "certificationRequestIDIndex", getExpectedCeritificationRequestIDIndex("addNewCeritificationRequest"))
	res := stub.MockInvoke("1", getRemoveSecondCeritificationRequestAssetForTesting())
	if res.Status != shim.OK {
		checkError(t, "CeritificationRequest with ID: "+"100002"+" not found", res.Message)
	} else {
		fmt.Println("Error was expected, but not raised")
		t.FailNow()
	}
	checkState(t, stub, "certificationRequestIDIndex", getExpectedCeritificationRequestIDIndex("addNewCeritificationRequest"))
}

//TestCeritificationRequestAsset_Invoke_removeAllCeritificationRequestsOK  //change template
func TestCeritificationRequestAsset_Invoke_removeAllCeritificationRequestsOK(t *testing.T) {
	certificationRequest := new(CeritificationRequestAsset)
	stub := shim.NewMockStub("certificationRequest", certificationRequest)
	checkInit(t, stub, [][]byte{[]byte("init")})
	checkInvoke(t, stub, getFirstCeritificationRequestAssetForTesting())
	checkInvoke(t, stub, getSecondCeritificationRequestAssetForTesting())
	checkReadAllCeritificationRequestsOK(t, stub)
	checkState(t, stub, "certificationRequestIDIndex", getExpectedCeritificationRequestIDIndex("beforeRemoveCeritificationRequest"))
	checkInvoke(t, stub, getRemoveAllCeritificationRequestAssetsForTesting())
	checkState(t, stub, "certificationRequestIDIndex", getExpectedCeritificationRequestIDIndex(""))
}

//TestCeritificationRequestAsset_Invoke_removeCeritificationRequestNOK  //change template
func TestCeritificationRequestAsset_Invoke_removeAllCeritificationRequestsNOK(t *testing.T) {
	certificationRequest := new(CeritificationRequestAsset)
	stub := shim.NewMockStub("certificationRequest", certificationRequest)
	checkInit(t, stub, [][]byte{[]byte("init")})
	res := stub.MockInvoke("1", getRemoveAllCeritificationRequestAssetsForTesting())
	if res.Status != shim.OK {
		checkError(t, "removeAllCeritificationRequests: No certificationRequests to remove", res.Message)
	} else {
		fmt.Println("Error was expected, but not raised")
		t.FailNow()
	}
	checkState(t, stub, "certificationRequestIDIndex", getExpectedCeritificationRequestIDIndex(""))
}

//TestCeritificationRequestAsset_Query_readCeritificationRequest
func TestCeritificationRequestAsset_Query_readCeritificationRequest(t *testing.T) {
	certificationRequest := new(CeritificationRequestAsset)
	stub := shim.NewMockStub("certificationRequest", certificationRequest)
	checkInit(t, stub, [][]byte{[]byte("init")})
	certificationRequestID := "100001"
	checkInvoke(t, stub, getFirstCeritificationRequestAssetForTesting())
	checkReadCeritificationRequestOK(t, stub, certificationRequestID)
	checkReadCeritificationRequestNOK(t, stub, "")
}

//TestCeritificationRequestAsset_Query_readAllCeritificationRequests
func TestCeritificationRequestAsset_Query_readAllCeritificationRequests(t *testing.T) {
	certificationRequest := new(CeritificationRequestAsset)
	stub := shim.NewMockStub("certificationRequest", certificationRequest)
	checkInit(t, stub, [][]byte{[]byte("init")})
	checkInvoke(t, stub, getFirstCeritificationRequestAssetForTesting())
	checkInvoke(t, stub, getSecondCeritificationRequestAssetForTesting())
	checkReadAllCeritificationRequestsOK(t, stub)
}

/*
*
*	Helper Functions
*
 */
//Get first CeritificationRequestAsset for testing
func getFirstCeritificationRequestAssetForTesting() [][]byte { 
 return [][]byte{[]byte("addNewCeritificationRequest"), 
 []byte("{\"ID\":\"100001\",\"docType\":\"Asset.CeritificationRequestAsset\",\"status\":\"0\",\"creationDate\":\"12/01/2018\", \"companyID\":\"companyID001\", \"certificationAuthorityID\":\"certificationAuthorityID001\"}")}
}

//Get second CeritificationRequestAsset for testing
func getSecondCeritificationRequestAssetForTesting() [][]byte { 
 return [][]byte{[]byte("addNewCeritificationRequest"), 
 []byte("{\"ID\":\"100002\",\"docType\":\"Asset.CeritificationRequestAsset\",\"status\":\"0\",\"creationDate\":\"12/01/2018\", \"companyID\":\"companyID002\", \"certificationAuthorityID\":\"certificationAuthorityID002\"}")}
}

//Get remove second CeritificationRequestAsset for testing //change template
func getRemoveSecondCeritificationRequestAssetForTesting() [][]byte {
	return [][]byte{[]byte("removeCeritificationRequest"),
		[]byte("100002")}
}

//Get remove all CeritificationRequestAssets for testing //change template
func getRemoveAllCeritificationRequestAssetsForTesting() [][]byte {
	return [][]byte{[]byte("removeAllCeritificationRequests")}
}

//Get an expected value for testing
func getNewCeritificationRequestExpected() []byte {
	var certificationRequest CeritificationRequest
		certificationRequest.ID = "100001"
	certificationRequest.ObjectType = "Asset.CeritificationRequestAsset"
  certificationRequest.Status = "0"
	certificationRequest.CreationDate = "12/01/2018"
certificationRequest.CompanyID="companyID001"
certificationRequest.CertificationAuthorityID="certificationAuthorityID001"
	certificationRequestJSON, err := json.Marshal(certificationRequest)
	if err != nil {
		fmt.Println("Error converting a CeritificationRequest record to JSON")
		return nil
	}
	return []byte(certificationRequestJSON)
}

//Get expected values of CeritificationRequests for testing
func getExpectedCeritificationRequests() []byte {
	var certificationRequests []CeritificationRequest
	var certificationRequest CeritificationRequest
		certificationRequest.ID = "100001"
	certificationRequest.ObjectType = "Asset.CeritificationRequestAsset"
  certificationRequest.Status = "0"
	certificationRequest.CreationDate = "12/01/2018"
certificationRequest.CompanyID="companyID001"
certificationRequest.CertificationAuthorityID="certificationAuthorityID001"
	certificationRequests = append(certificationRequests, certificationRequest)
		certificationRequest.ID = "100002"
	certificationRequest.ObjectType = "Asset.CeritificationRequestAsset"
  certificationRequest.Status = "0"
	certificationRequest.CreationDate = "12/01/2018"
certificationRequest.CompanyID="companyID002"
certificationRequest.CertificationAuthorityID="certificationAuthorityID002"
	certificationRequests = append(certificationRequests, certificationRequest)
	certificationRequestJSON, err := json.Marshal(certificationRequests)
	if err != nil {
		fmt.Println("Error converting certificationRequest records to JSON")
		return nil
	}
	return []byte(certificationRequestJSON)
}

func getExpectedCeritificationRequestIDIndex(funcName string) []byte {
	var certificationRequestIDIndex CeritificationRequestIDIndex
	switch funcName {
	case "addNewCeritificationRequest":
		certificationRequestIDIndex.IDs = append(certificationRequestIDIndex.IDs, "100001")
		certificationRequestIDIndexBytes, err := json.Marshal(certificationRequestIDIndex)
		if err != nil {
			fmt.Println("Error converting CeritificationRequestIDIndex to JSON")
			return nil
		}
		return certificationRequestIDIndexBytes
	case "beforeRemoveCeritificationRequest":
		certificationRequestIDIndex.IDs = append(certificationRequestIDIndex.IDs, "100001")
		certificationRequestIDIndex.IDs = append(certificationRequestIDIndex.IDs, "100002")
		certificationRequestIDIndexBytes, err := json.Marshal(certificationRequestIDIndex)
		if err != nil {
			fmt.Println("Error converting CeritificationRequestIDIndex to JSON")
			return nil
		}
		return certificationRequestIDIndexBytes
	case "afterRemoveCeritificationRequest":
		certificationRequestIDIndex.IDs = append(certificationRequestIDIndex.IDs, "100001")
		certificationRequestIDIndexBytes, err := json.Marshal(certificationRequestIDIndex)
		if err != nil {
			fmt.Println("Error converting CeritificationRequestIDIndex to JSON")
			return nil
		}
		return certificationRequestIDIndexBytes
	default:
		certificationRequestIDIndexBytes, err := json.Marshal(certificationRequestIDIndex)
		if err != nil {
			fmt.Println("Error converting CeritificationRequestIDIndex to JSON")
			return nil
		}
		return certificationRequestIDIndexBytes
	}
}

//checkInit - helper to check the Initialization of chaincode: CeritificationRequestAsset
func checkInit(t *testing.T, stub *shim.MockStub, args [][]byte) {
	res := stub.MockInit("1", args)
	if res.Status != shim.OK {
		fmt.Println("Init failed", string(res.Message))
		t.FailNow()
	}
}

//checkState - helper for checking the chaincode state for a given stateKey afgainst an expected value
func checkState(t *testing.T, stub *shim.MockStub, stateKey string, expectedState []byte) {
	actualState := stub.State[stateKey]
	if actualState == nil {
		fmt.Println("State for ", stateKey, ": failed to get value")
		t.FailNow()
	}

	if bytes.Compare(actualState, expectedState) != 0 {
		fmt.Println("Incorrect State for " + stateKey + ": \nExpected: " + string(expectedState) + "\nActual  : " + string(actualState))
		t.FailNow()
	}
}

//checkInvoke - helper for checking Invoke of chaincode
func checkInvoke(t *testing.T, stub *shim.MockStub, args [][]byte) {
	res := stub.MockInvoke("1", args)
	if res.Status != shim.OK {
		fmt.Println("Invoke", args, "failed", string(res.Message))
		t.FailNow()
	}
}

func checkInvokeUnknownFunction(t *testing.T, stub *shim.MockStub, args [][]byte) {
	res := stub.MockInvoke("1", args)
	if res.Status != shim.OK {
		expectedErr := "Received unknown function invocation"
		actualErr := string(res.Message)
		if !(strings.Contains(actualErr, expectedErr)) {
			fmt.Println("Invoke with unknown function must throw error: ", expectedErr, "\n Actual Error :", actualErr)
			t.FailNow()
		}
	} else {
		t.FailNow()
	}
}

//checkReadCeritificationRequestOK - helper for positive test readCeritificationRequest
func checkReadCeritificationRequestOK(t *testing.T, stub *shim.MockStub, certificationRequestID string) {
	res := stub.MockInvoke("1", [][]byte{[]byte("readCeritificationRequest"), []byte(certificationRequestID)})
	if res.Status != shim.OK {
		fmt.Println("func readCeritificationRequest with ID: ", certificationRequestID, " failed"+string(res.Message))
		t.FailNow()
	}
	if res.Payload == nil {
		fmt.Println("func readCeritificationRequest with ID: ", certificationRequestID, "failed to get value")
		t.FailNow()
	}
	if bytes.Compare(getNewCeritificationRequestExpected(), []byte(res.Payload)) != 0 {
		fmt.Println("func readCeritificationRequest with ID: ", certificationRequestID, "Expected:", string(getNewCeritificationRequestExpected()), "Actual:", string(res.Payload))
		t.FailNow()
	}
}

//checkReadCeritificationRequestNOK - helper for negative testing of readCeritificationRequest
func checkReadCeritificationRequestNOK(t *testing.T, stub *shim.MockStub, certificationRequestID string) {
	//with no certificationRequestID
	res := stub.MockInvoke("1", [][]byte{[]byte("readCeritificationRequest"), []byte("")})
	if res.Status != shim.OK {
		expectedErr := "retrieveCeritificationRequest: Corrupt certificationRequest record "
		actualErr := string(res.Message)
		if !(strings.Contains(actualErr, expectedErr)) {
			fmt.Println("func readCeritificationRequest negative test: ", "Expected Error:", expectedErr, "Actual Error", actualErr)
			t.FailNow()
		}
	} else {
		t.FailNow()
	}
}

func checkReadAllCeritificationRequestsOK(t *testing.T, stub *shim.MockStub) {
	res := stub.MockInvoke("1", [][]byte{[]byte("readAllCeritificationRequests")})
	if res.Status != shim.OK {
		fmt.Println("func readAllCeritificationRequests failed", string(res.Message))
		t.FailNow()
	}
	if res.Payload == nil {
		fmt.Println("func readAllCeritificationRequests failed to get value")
		t.FailNow()
	}
	if bytes.Compare(getExpectedCeritificationRequests(), []byte(res.Payload)) != 0 {
		fmt.Println("func readAllCeritificationRequests Expected:\n", string(getExpectedCeritificationRequests()), "\nActual:\n", string(res.Payload))
		t.FailNow()
	}
}

func checkError(t *testing.T, exp string, act string) {
	if strings.Compare(exp, act) != 0 {
		fmt.Println("Unexpected Error! Expecting ", exp, "\n Actual :", act)
		t.FailNow()
	}
}
