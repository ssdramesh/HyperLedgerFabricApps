package main

//Template Version: 1.1-20180704
import (
	"bytes"
	"encoding/json"
	"fmt"
	"strings"
	"testing"

	"github.com/hyperledger/fabric/core/chaincode/shim"
)

//TestCompanyParticipant_Init
func TestCompanyParticipant_Init(t *testing.T) {
	company := new(CompanyParticipant)
	stub := shim.NewMockStub("company", company)
	checkInit(t, stub, [][]byte{[]byte("init")})
	checkState(t, stub, "companyIDIndex", getExpectedCompanyIDIndex("init"))
}

//TestCompanyParticipant_InvokeUnknownFunction
func TestCompanyParticipant_InvokeUnknownFunction(t *testing.T) {
	company := new(CompanyParticipant)
	stub := shim.NewMockStub("company", company)
	checkInit(t, stub, [][]byte{[]byte("init")})
	checkInvokeUnknownFunction(t, stub, [][]byte{[]byte("myFunction"), []byte("docType:Participant")})
}

//TestCompanyParticipant_Invoke_addNewCompany
func TestCompanyParticipant_Invoke_addNewCompanyOK(t *testing.T) {
	company := new(CompanyParticipant)
	stub := shim.NewMockStub("company", company)
	checkInit(t, stub, [][]byte{[]byte("init")})
	checkInvoke(t, stub, getFirstCompanyParticipantForTesting())
	newCompanyID := "100001"
	checkState(t, stub, newCompanyID, getNewCompanyExpected())
	checkState(t, stub, "companyIDIndex", getExpectedCompanyIDIndex("addNewCompany"))
}

//TestCompanyParticipant_Invoke_addNewCompany
func TestCompanyParticipant_Invoke_addNewCompanyDuplicate(t *testing.T) {
	company := new(CompanyParticipant)
	stub := shim.NewMockStub("company", company)
	checkInit(t, stub, [][]byte{[]byte("init")})
	checkInvoke(t, stub, getFirstCompanyParticipantForTesting())
	newCompanyID := "100001"
	checkState(t, stub, newCompanyID, getNewCompanyExpected())
	checkState(t, stub, "companyIDIndex", getExpectedCompanyIDIndex("addNewCompany"))
	res := stub.MockInvoke("1", getFirstCompanyParticipantForTesting())
	if res.Status != shim.OK {
		checkError(t, "This Company already exists: 100001", res.Message)
	} else {
		fmt.Println("Error was expected, but not raised")
		t.FailNow()
	}
}

//TestCompanyParticipant_Invoke_removeCompanyOK  //change template
func TestCompanyParticipant_Invoke_removeCompanyOK(t *testing.T) {
	company := new(CompanyParticipant)
	stub := shim.NewMockStub("company", company)
	checkInit(t, stub, [][]byte{[]byte("init")})
	checkInvoke(t, stub, getFirstCompanyParticipantForTesting())
	checkInvoke(t, stub, getSecondCompanyParticipantForTesting())
	checkReadAllCompanysOK(t, stub)
	checkState(t, stub, "companyIDIndex", getExpectedCompanyIDIndex("beforeRemoveCompany"))
	checkInvoke(t, stub, getRemoveSecondCompanyParticipantForTesting())
	remainingCompanyID := "100001"
	checkReadCompanyOK(t, stub, remainingCompanyID)
	checkState(t, stub, "companyIDIndex", getExpectedCompanyIDIndex("afterRemoveCompany"))
}

//TestCompanyParticipant_Invoke_removeCompanyNOK  //change template
func TestCompanyParticipant_Invoke_removeCompanyNOK(t *testing.T) {
	company := new(CompanyParticipant)
	stub := shim.NewMockStub("company", company)
	checkInit(t, stub, [][]byte{[]byte("init")})
	checkInvoke(t, stub, getFirstCompanyParticipantForTesting())
	firstCompanyID := "100001"
	checkReadCompanyOK(t, stub, firstCompanyID)
	checkState(t, stub, "companyIDIndex", getExpectedCompanyIDIndex("addNewCompany"))
	res := stub.MockInvoke("1", getRemoveSecondCompanyParticipantForTesting())
	if res.Status != shim.OK {
		checkError(t, "Company with ID: "+"100002"+" not found", res.Message)
	} else {
		fmt.Println("Error was expected, but not raised")
		t.FailNow()
	}
	checkState(t, stub, "companyIDIndex", getExpectedCompanyIDIndex("addNewCompany"))
}

//TestCompanyParticipant_Invoke_removeAllCompanysOK  //change template
func TestCompanyParticipant_Invoke_removeAllCompanysOK(t *testing.T) {
	company := new(CompanyParticipant)
	stub := shim.NewMockStub("company", company)
	checkInit(t, stub, [][]byte{[]byte("init")})
	checkInvoke(t, stub, getFirstCompanyParticipantForTesting())
	checkInvoke(t, stub, getSecondCompanyParticipantForTesting())
	checkReadAllCompanysOK(t, stub)
	checkState(t, stub, "companyIDIndex", getExpectedCompanyIDIndex("beforeRemoveCompany"))
	checkInvoke(t, stub, getRemoveAllCompanyParticipantsForTesting())
	checkState(t, stub, "companyIDIndex", getExpectedCompanyIDIndex(""))
}

//TestCompanyParticipant_Invoke_removeCompanyNOK  //change template
func TestCompanyParticipant_Invoke_removeAllCompanysNOK(t *testing.T) {
	company := new(CompanyParticipant)
	stub := shim.NewMockStub("company", company)
	checkInit(t, stub, [][]byte{[]byte("init")})
	res := stub.MockInvoke("1", getRemoveAllCompanyParticipantsForTesting())
	if res.Status != shim.OK {
		checkError(t, "removeAllCompanys: No companys to remove", res.Message)
	} else {
		fmt.Println("Error was expected, but not raised")
		t.FailNow()
	}
	checkState(t, stub, "companyIDIndex", getExpectedCompanyIDIndex(""))
}

//TestCompanyParticipant_Query_readCompany
func TestCompanyParticipant_Query_readCompany(t *testing.T) {
	company := new(CompanyParticipant)
	stub := shim.NewMockStub("company", company)
	checkInit(t, stub, [][]byte{[]byte("init")})
	companyID := "100001"
	checkInvoke(t, stub, getFirstCompanyParticipantForTesting())
	checkReadCompanyOK(t, stub, companyID)
	checkReadCompanyNOK(t, stub, "")
}

//TestCompanyParticipant_Query_readAllCompanys
func TestCompanyParticipant_Query_readAllCompanys(t *testing.T) {
	company := new(CompanyParticipant)
	stub := shim.NewMockStub("company", company)
	checkInit(t, stub, [][]byte{[]byte("init")})
	checkInvoke(t, stub, getFirstCompanyParticipantForTesting())
	checkInvoke(t, stub, getSecondCompanyParticipantForTesting())
	checkReadAllCompanysOK(t, stub)
}

/*
*
*	Helper Functions
*
 */
//Get first CompanyParticipant for testing
func getFirstCompanyParticipantForTesting() [][]byte { 
 return [][]byte{[]byte("addNewCompany"), 
 []byte("{\"ID\":\"100001\",\"docType\":\"Participant.CompanyParticipant\",\"alias\":\"BRITECH\",\"description\":\"British Technology Pvt. Ltd.\", \"registrationID\":\"registrationID001\", \"certificationID\":\"certificationID001\", \"type\":\"type001\", \"name\":\"name001\"}")}
}

//Get second CompanyParticipant for testing
func getSecondCompanyParticipantForTesting() [][]byte { 
 return [][]byte{[]byte("addNewCompany"), 
 []byte("{\"ID\":\"100002\",\"docType\":\"Participant.CompanyParticipant\",\"alias\":\"DEMAGDELAG\",\"description\":\"Demag Delewal AG\", \"registrationID\":\"registrationID002\", \"certificationID\":\"certificationID002\", \"type\":\"type002\", \"name\":\"name002\"}")}
}

//Get remove second CompanyParticipant for testing //change template
func getRemoveSecondCompanyParticipantForTesting() [][]byte {
	return [][]byte{[]byte("removeCompany"),
		[]byte("100002")}
}

//Get remove all CompanyParticipants for testing //change template
func getRemoveAllCompanyParticipantsForTesting() [][]byte {
	return [][]byte{[]byte("removeAllCompanys")}
}

//Get an expected value for testing
func getNewCompanyExpected() []byte {
	var company Company
		company.ID = "100001"
	company.ObjectType = "Participant.CompanyParticipant"
	company.Alias = "BRITECH"
	company.Description = "British Technology Pvt. Ltd."
company.RegistrationID="registrationID001"
company.CertificationID="certificationID001"
company.Type="type001"
company.Name="name001"
	companyJSON, err := json.Marshal(company)
	if err != nil {
		fmt.Println("Error converting a Company record to JSON")
		return nil
	}
	return []byte(companyJSON)
}

//Get expected values of Companys for testing
func getExpectedCompanys() []byte {
	var companys []Company
	var company Company
		company.ID = "100001"
	company.ObjectType = "Participant.CompanyParticipant"
	company.Alias = "BRITECH"
	company.Description = "British Technology Pvt. Ltd."
company.RegistrationID="registrationID001"
company.CertificationID="certificationID001"
company.Type="type001"
company.Name="name001"
	companys = append(companys, company)
		company.ID = "100002"
	company.ObjectType = "Participant.CompanyParticipant"
	company.Alias = "DEMAGDELAG"
	company.Description = "Demag Delewal AG"
company.RegistrationID="registrationID002"
company.CertificationID="certificationID002"
company.Type="type002"
company.Name="name002"
	company.Description = "Demag Delewal AG"
	companys = append(companys, company)
	companyJSON, err := json.Marshal(companys)
	if err != nil {
		fmt.Println("Error converting companyancer records to JSON")
		return nil
	}
	return []byte(companyJSON)
}

func getExpectedCompanyIDIndex(funcName string) []byte {
	var companyIDIndex CompanyIDIndex
	switch funcName {
	case "addNewCompany":
		companyIDIndex.IDs = append(companyIDIndex.IDs, "100001")
		companyIDIndexBytes, err := json.Marshal(companyIDIndex)
		if err != nil {
			fmt.Println("Error converting CompanyIDIndex to JSON")
			return nil
		}
		return companyIDIndexBytes
	case "beforeRemoveCompany":
		companyIDIndex.IDs = append(companyIDIndex.IDs, "100001")
		companyIDIndex.IDs = append(companyIDIndex.IDs, "100002")
		companyIDIndexBytes, err := json.Marshal(companyIDIndex)
		if err != nil {
			fmt.Println("Error converting CompanyIDIndex to JSON")
			return nil
		}
		return companyIDIndexBytes
	case "afterRemoveCompany":
		companyIDIndex.IDs = append(companyIDIndex.IDs, "100001")
		companyIDIndexBytes, err := json.Marshal(companyIDIndex)
		if err != nil {
			fmt.Println("Error converting CompanyIDIndex to JSON")
			return nil
		}
		return companyIDIndexBytes
	default:
		companyIDIndexBytes, err := json.Marshal(companyIDIndex)
		if err != nil {
			fmt.Println("Error converting CompanyIDIndex to JSON")
			return nil
		}
		return companyIDIndexBytes
	}
}

//checkInit - helper to check the Initialization of chaincode: CompanyParticipant
func checkInit(t *testing.T, stub *shim.MockStub, args [][]byte) {
	res := stub.MockInit("1", args)
	if res.Status != shim.OK {
		fmt.Println("Init failed", string(res.Message))
		t.FailNow()
	}
}

//checkState - helper for checking the chaincode state for a given stateKey afgainst an expected value
func checkState(t *testing.T, stub *shim.MockStub, stateKey string, expectedState []byte) {
	actualState := stub.State[stateKey]
	if actualState == nil {
		fmt.Println("State for ", stateKey, ": failed to get value")
		t.FailNow()
	}

	if bytes.Compare(actualState, expectedState) != 0 {
		fmt.Println("Incorrect State for " + stateKey + ": \nExpected: " + string(expectedState) + "\nActual  : " + string(actualState))
		t.FailNow()
	}
}

//checkInvoke - helper for checking Invoke of chaincode
func checkInvoke(t *testing.T, stub *shim.MockStub, args [][]byte) {
	res := stub.MockInvoke("1", args)
	if res.Status != shim.OK {
		fmt.Println("Invoke", args, "failed", string(res.Message))
		t.FailNow()
	}
}

func checkInvokeUnknownFunction(t *testing.T, stub *shim.MockStub, args [][]byte) {
	res := stub.MockInvoke("1", args)
	if res.Status != shim.OK {
		expectedErr := "Received unknown function invocation"
		actualErr := string(res.Message)
		if !(strings.Contains(actualErr, expectedErr)) {
			fmt.Println("Invoke with unknown function must throw error: ", expectedErr, "\n Actual Error :", actualErr)
			t.FailNow()
		}
	} else {
		t.FailNow()
	}
}

//checkReadCompanyOK - helper for positive test readCompany
func checkReadCompanyOK(t *testing.T, stub *shim.MockStub, companyancerID string) {
	res := stub.MockInvoke("1", [][]byte{[]byte("readCompany"), []byte(companyancerID)})
	if res.Status != shim.OK {
		fmt.Println("func readCompany with ID: ", companyancerID, " failed"+string(res.Message))
		t.FailNow()
	}
	if res.Payload == nil {
		fmt.Println("func readCompany with ID: ", companyancerID, "failed to get value")
		t.FailNow()
	}
	if bytes.Compare(getNewCompanyExpected(), []byte(res.Payload)) != 0 {
		fmt.Println("func readCompany with ID: ", companyancerID, "Expected:", string(getNewCompanyExpected()), "Actual:", string(res.Payload))
		t.FailNow()
	}
}

//checkReadCompanyNOK - helper for negative testing of readCompany
func checkReadCompanyNOK(t *testing.T, stub *shim.MockStub, companyancerID string) {
	//with no companyancerID
	res := stub.MockInvoke("1", [][]byte{[]byte("readCompany"), []byte("")})
	if res.Status != shim.OK {
		expectedErr := "retrieveCompany: Corrupt company record "
		actualErr := string(res.Message)
		if !(strings.Contains(actualErr, expectedErr)) {
			fmt.Println("func readCompany neagtive test: ", "Expected Error:", expectedErr, "Actual Error", actualErr)
			t.FailNow()
		}
	} else {
		t.FailNow()
	}
}
func checkReadAllCompanysOK(t *testing.T, stub *shim.MockStub) {
	res := stub.MockInvoke("1", [][]byte{[]byte("readAllCompanys")})
	if res.Status != shim.OK {
		fmt.Println("func readAllCompanys failed", string(res.Message))
		t.FailNow()
	}
	if res.Payload == nil {
		fmt.Println("func readAllCompanys failed to get value")
		t.FailNow()
	}
	if bytes.Compare(getExpectedCompanys(), []byte(res.Payload)) != 0 {
		fmt.Println("func readAllCompanys Expected:\n", string(getExpectedCompanys()), "\nActual:\n", string(res.Payload))
		t.FailNow()
	}
}

func checkError(t *testing.T, exp string, act string) {
	if strings.Compare(exp, act) != 0 {
		fmt.Println("Unexpected Error! Expecting ", exp, "\n Actual :", act)
		t.FailNow()
	}
}
