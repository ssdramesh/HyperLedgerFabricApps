package main

//Template Version: 1.1-20180704
import (
	"bytes"
	"encoding/json"
	"fmt"
	"strings"
	"testing"

	"github.com/hyperledger/fabric/core/chaincode/shim"
)

// History/Search
// ... the History and Search API calls are not supported in the mock interface

//TestSampleAsset_Init
func TestSampleAsset_Init(t *testing.T) {
	sample := new(SampleAsset)
	stub := shim.NewMockStub("sample", sample)
	checkInit(t, stub, [][]byte{[]byte("init")})
	checkState(t, stub, "sampleIDIndex", getExpectedSampleIDIndex("init"))
}

//TestSampleAsset_InvokeUnknownFunction
func TestSampleAsset_InvokeUnknownFunction(t *testing.T) {
	sample := new(SampleAsset)
	stub := shim.NewMockStub("sample", sample)
	checkInit(t, stub, [][]byte{[]byte("init")})
	checkInvokeUnknownFunction(t, stub, [][]byte{[]byte("myFunction"), []byte("docType:Asset")})
}

//TestSampleAsset_Invoke_addNewSample
func TestSampleAsset_Invoke_addNewSampleOK(t *testing.T) {
	sample := new(SampleAsset)
	stub := shim.NewMockStub("sample", sample)
	checkInit(t, stub, [][]byte{[]byte("init")})
	checkInvoke(t, stub, getFirstSampleAssetForTesting())
	newSampleID := "100001"
	checkState(t, stub, newSampleID, getNewSampleExpected())
	checkState(t, stub, "sampleIDIndex", getExpectedSampleIDIndex("addNewSample"))
}

//TestSampleAsset_Invoke_addNewSample
func TestSampleAsset_Invoke_addNewSampleDuplicate(t *testing.T) {
	sample := new(SampleAsset)
	stub := shim.NewMockStub("sample", sample)
	checkInit(t, stub, [][]byte{[]byte("init")})
	checkInvoke(t, stub, getFirstSampleAssetForTesting())
	newSampleID := "100001"
	checkState(t, stub, newSampleID, getNewSampleExpected())
	checkState(t, stub, "sampleIDIndex", getExpectedSampleIDIndex("addNewSample"))
	res := stub.MockInvoke("1", getFirstSampleAssetForTesting())
	if res.Status != shim.OK {
		checkError(t, "This Sample already exists: 100001", res.Message)
	} else {
		fmt.Println("Error was expected, but not raised")
		t.FailNow()
	}
}

func TestSampleAsset_Invoke_updateSampleOK(t *testing.T) {
	sample := new(SampleAsset)
	stub := shim.NewMockStub("sample", sample)
	checkInit(t, stub, [][]byte{[]byte("init")})
	checkInvoke(t, stub, getFirstSampleAssetForTesting())
	newSampleID := "100001"
	checkState(t, stub, newSampleID, getNewSampleExpected())
	checkInvoke(t, stub, getFirstSampleAssetForUpdateTestingOK())
	checkReadSampleAssetAfterUpdateOK(t, stub, newSampleID)
}

//TestSampleAsset_Invoke_removeSampleOK  //change template
func TestSampleAsset_Invoke_removeSampleOK(t *testing.T) {
	sample := new(SampleAsset)
	stub := shim.NewMockStub("sample", sample)
	checkInit(t, stub, [][]byte{[]byte("init")})
	checkInvoke(t, stub, getFirstSampleAssetForTesting())
	checkInvoke(t, stub, getSecondSampleAssetForTesting())
	checkReadAllSamplesOK(t, stub)
	checkState(t, stub, "sampleIDIndex", getExpectedSampleIDIndex("beforeRemoveSample"))
	checkInvoke(t, stub, getRemoveSecondSampleAssetForTesting())
	remainingSampleID := "100001"
	checkReadSampleOK(t, stub, remainingSampleID)
	checkState(t, stub, "sampleIDIndex", getExpectedSampleIDIndex("afterRemoveSample"))
}

//TestSampleAsset_Invoke_removeSampleNOK  //change template
func TestSampleAsset_Invoke_removeSampleNOK(t *testing.T) {
	sample := new(SampleAsset)
	stub := shim.NewMockStub("sample", sample)
	checkInit(t, stub, [][]byte{[]byte("init")})
	checkInvoke(t, stub, getFirstSampleAssetForTesting())
	firstSampleID := "100001"
	checkReadSampleOK(t, stub, firstSampleID)
	checkState(t, stub, "sampleIDIndex", getExpectedSampleIDIndex("addNewSample"))
	res := stub.MockInvoke("1", getRemoveSecondSampleAssetForTesting())
	if res.Status != shim.OK {
		checkError(t, "Sample with ID: "+"100002"+" not found", res.Message)
	} else {
		fmt.Println("Error was expected, but not raised")
		t.FailNow()
	}
	checkState(t, stub, "sampleIDIndex", getExpectedSampleIDIndex("addNewSample"))
}

//TestSampleAsset_Invoke_removeAllSamplesOK  //change template
func TestSampleAsset_Invoke_removeAllSamplesOK(t *testing.T) {
	sample := new(SampleAsset)
	stub := shim.NewMockStub("sample", sample)
	checkInit(t, stub, [][]byte{[]byte("init")})
	checkInvoke(t, stub, getFirstSampleAssetForTesting())
	checkInvoke(t, stub, getSecondSampleAssetForTesting())
	checkReadAllSamplesOK(t, stub)
	checkState(t, stub, "sampleIDIndex", getExpectedSampleIDIndex("beforeRemoveSample"))
	checkInvoke(t, stub, getRemoveAllSampleAssetsForTesting())
	checkState(t, stub, "sampleIDIndex", getExpectedSampleIDIndex(""))
}

//TestSampleAsset_Invoke_removeSampleNOK  //change template
func TestSampleAsset_Invoke_removeAllSamplesNOK(t *testing.T) {
	sample := new(SampleAsset)
	stub := shim.NewMockStub("sample", sample)
	checkInit(t, stub, [][]byte{[]byte("init")})
	res := stub.MockInvoke("1", getRemoveAllSampleAssetsForTesting())
	if res.Status != shim.OK {
		checkError(t, "removeAllSamples: No samples to remove", res.Message)
	} else {
		fmt.Println("Error was expected, but not raised")
		t.FailNow()
	}
	checkState(t, stub, "sampleIDIndex", getExpectedSampleIDIndex(""))
}

//TestSampleAsset_Query_readSample
func TestSampleAsset_Query_readSample(t *testing.T) {
	sample := new(SampleAsset)
	stub := shim.NewMockStub("sample", sample)
	checkInit(t, stub, [][]byte{[]byte("init")})
	sampleID := "100001"
	checkInvoke(t, stub, getFirstSampleAssetForTesting())
	checkReadSampleOK(t, stub, sampleID)
	checkReadSampleNOK(t, stub, "")
}

//TestSampleAsset_Query_readAllSamples
func TestSampleAsset_Query_readAllSamples(t *testing.T) {
	sample := new(SampleAsset)
	stub := shim.NewMockStub("sample", sample)
	checkInit(t, stub, [][]byte{[]byte("init")})
	checkInvoke(t, stub, getFirstSampleAssetForTesting())
	checkInvoke(t, stub, getSecondSampleAssetForTesting())
	checkReadAllSamplesOK(t, stub)
}

/*
*
*	Helper Functions
*
 */
//Get first SampleAsset for testing
func getFirstSampleAssetForTesting() [][]byte { 
 return [][]byte{[]byte("addNewSample"), 
 []byte("{\"ID\":\"100001\",\"docType\":\"Asset.SampleAsset\",\"status\":\"0\",\"creationDate\":\"12/01/2018\", \"temperature\":\"temperature001\"}")}
}

//Get first SampleAsset for update testing
func getFirstSampleAssetForUpdateTestingOK() [][]byte { 
 return [][]byte{[]byte("updateSample"), 
 []byte("{\"ID\":\"100001\",\"docType\":\"Asset.SampleAsset\",\"status\":\"1\",\"creationDate\":\"12/01/2018\", \"temperature\":\"temperature001\"}")}
}

//Get second SampleAsset for testing
func getSecondSampleAssetForTesting() [][]byte { 
 return [][]byte{[]byte("addNewSample"), 
 []byte("{\"ID\":\"100002\",\"docType\":\"Asset.SampleAsset\",\"status\":\"0\",\"creationDate\":\"12/01/2018\", \"temperature\":\"temperature002\"}")}
}

//Get remove second SampleAsset for testing //change template
func getRemoveSecondSampleAssetForTesting() [][]byte {
	return [][]byte{[]byte("removeSample"),
		[]byte("100002")}
}

//Get remove all SampleAssets for testing //change template
func getRemoveAllSampleAssetsForTesting() [][]byte {
	return [][]byte{[]byte("removeAllSamples")}
}

//Get an expected value for testing
func getNewSampleExpected() []byte {
	var sample Sample
		sample.ID = "100001"
	sample.ObjectType = "Asset.SampleAsset"
  sample.Status = "0"
	sample.CreationDate = "12/01/2018"
sample.Temperature="temperature001"
	sampleJSON, err := json.Marshal(sample)
	if err != nil {
		fmt.Println("Error converting a Sample record to JSON")
		return nil
	}
	return []byte(sampleJSON)
}

//Get an expected value for testing
func getUpdatedSampleExpected() []byte {
	var sample Sample
	sample.ID = "100001"
	sample.ObjectType = "Asset.SampleAsset"
  sample.Status = "1"
	sample.CreationDate = "12/01/2018"
sample.Temperature="temperature001"
sample.Temperature="temperature001"
	sampleJSON, err := json.Marshal(sample)
	if err != nil {
		fmt.Println("Error converting a Sample record to JSON")
		return nil
	}
	return []byte(sampleJSON)
}

//Get expected values of Samples for testing
func getExpectedSamples() []byte {
	var samples []Sample
	var sample Sample
		sample.ID = "100001"
	sample.ObjectType = "Asset.SampleAsset"
  sample.Status = "0"
	sample.CreationDate = "12/01/2018"
sample.Temperature="temperature001"
	samples = append(samples, sample)
		sample.ID = "100002"
	sample.ObjectType = "Asset.SampleAsset"
  sample.Status = "0"
	sample.CreationDate = "12/01/2018"
sample.Temperature="temperature002"
	samples = append(samples, sample)
	sampleJSON, err := json.Marshal(samples)
	if err != nil {
		fmt.Println("Error converting sample records to JSON")
		return nil
	}
	return []byte(sampleJSON)
}

func getExpectedSampleIDIndex(funcName string) []byte {
	var sampleIDIndex SampleIDIndex
	switch funcName {
	case "addNewSample":
		sampleIDIndex.IDs = append(sampleIDIndex.IDs, "100001")
		sampleIDIndexBytes, err := json.Marshal(sampleIDIndex)
		if err != nil {
			fmt.Println("Error converting SampleIDIndex to JSON")
			return nil
		}
		return sampleIDIndexBytes
	case "beforeRemoveSample":
		sampleIDIndex.IDs = append(sampleIDIndex.IDs, "100001")
		sampleIDIndex.IDs = append(sampleIDIndex.IDs, "100002")
		sampleIDIndexBytes, err := json.Marshal(sampleIDIndex)
		if err != nil {
			fmt.Println("Error converting SampleIDIndex to JSON")
			return nil
		}
		return sampleIDIndexBytes
	case "afterRemoveSample":
		sampleIDIndex.IDs = append(sampleIDIndex.IDs, "100001")
		sampleIDIndexBytes, err := json.Marshal(sampleIDIndex)
		if err != nil {
			fmt.Println("Error converting SampleIDIndex to JSON")
			return nil
		}
		return sampleIDIndexBytes
	default:
		sampleIDIndexBytes, err := json.Marshal(sampleIDIndex)
		if err != nil {
			fmt.Println("Error converting SampleIDIndex to JSON")
			return nil
		}
		return sampleIDIndexBytes
	}
}

//checkInit - helper to check the Initialization of chaincode: SampleAsset
func checkInit(t *testing.T, stub *shim.MockStub, args [][]byte) {
	res := stub.MockInit("1", args)
	if res.Status != shim.OK {
		fmt.Println("Init failed", string(res.Message))
		t.FailNow()
	}
}

//checkState - helper for checking the chaincode state for a given stateKey afgainst an expected value
func checkState(t *testing.T, stub *shim.MockStub, stateKey string, expectedState []byte) {
	actualState := stub.State[stateKey]
	if actualState == nil {
		fmt.Println("State for ", stateKey, ": failed to get value")
		t.FailNow()
	}

	if bytes.Compare(actualState, expectedState) != 0 {
		fmt.Println("Incorrect State for " + stateKey + ": \nExpected: " + string(expectedState) + "\nActual  : " + string(actualState))
		t.FailNow()
	}
}

//checkInvoke - helper for checking Invoke of chaincode
func checkInvoke(t *testing.T, stub *shim.MockStub, args [][]byte) {
	res := stub.MockInvoke("1", args)
	if res.Status != shim.OK {
		fmt.Println("Invoke", args, "failed", string(res.Message))
		t.FailNow()
	}
}

func checkInvokeUnknownFunction(t *testing.T, stub *shim.MockStub, args [][]byte) {
	res := stub.MockInvoke("1", args)
	if res.Status != shim.OK {
		expectedErr := "Received unknown function invocation"
		actualErr := string(res.Message)
		if !(strings.Contains(actualErr, expectedErr)) {
			fmt.Println("Invoke with unknown function must throw error: ", expectedErr, "\n Actual Error :", actualErr)
			t.FailNow()
		}
	} else {
		t.FailNow()
	}
}

//checkReadSampleOK - helper for positive test readSample
func checkReadSampleOK(t *testing.T, stub *shim.MockStub, sampleID string) {
	res := stub.MockInvoke("1", [][]byte{[]byte("readSample"), []byte(sampleID)})
	if res.Status != shim.OK {
		fmt.Println("func readSample with ID: ", sampleID, " failed"+string(res.Message))
		t.FailNow()
	}
	if res.Payload == nil {
		fmt.Println("func readSample with ID: ", sampleID, "failed to get value")
		t.FailNow()
	}
	if bytes.Compare(getNewSampleExpected(), []byte(res.Payload)) != 0 {
		fmt.Println("func readSample with ID: ", sampleID, "Expected:", string(getNewSampleExpected()), "Actual:", string(res.Payload))
		t.FailNow()
	}
}

//checkReadSampleNOK - helper for negative testing of readSample
func checkReadSampleNOK(t *testing.T, stub *shim.MockStub, sampleID string) {
	//with no sampleID
	res := stub.MockInvoke("1", [][]byte{[]byte("readSample"), []byte("")})
	if res.Status != shim.OK {
		expectedErr := "retrieveSample: Corrupt sample record "
		actualErr := string(res.Message)
		if !(strings.Contains(actualErr, expectedErr)) {
			fmt.Println("func readSample negative test: ", "Expected Error:", expectedErr, "Actual Error", actualErr)
			t.FailNow()
		}
	} else {
		t.FailNow()
	}
}

//checkReadSampleAssetAfterUpdateOK - helper for positive test readSample after update
func checkReadSampleAssetAfterUpdateOK(t *testing.T, stub *shim.MockStub, SampleID string) {
	res := stub.MockInvoke("1", [][]byte{[]byte("readSample"), []byte(SampleID)})
	if res.Status != shim.OK {
		fmt.Println("func readSample with ID: ", SampleID, " failed"+string(res.Message))
		t.FailNow()
	}
	if res.Payload == nil {
		fmt.Println("func readSample with ID: ", SampleID, "failed to get value")
		t.FailNow()
	}
	if bytes.Compare(getUpdatedSampleExpected(), []byte(res.Payload)) != 0 {
		fmt.Println("func readSample with ID: ", SampleID, "Expected:", string(getUpdatedSampleExpected()), "Actual:", string(res.Payload))
		t.FailNow()
	}
}

func checkReadAllSamplesOK(t *testing.T, stub *shim.MockStub) {
	res := stub.MockInvoke("1", [][]byte{[]byte("readAllSamples")})
	if res.Status != shim.OK {
		fmt.Println("func readAllSamples failed", string(res.Message))
		t.FailNow()
	}
	if res.Payload == nil {
		fmt.Println("func readAllSamples failed to get value")
		t.FailNow()
	}
	if bytes.Compare(getExpectedSamples(), []byte(res.Payload)) != 0 {
		fmt.Println("func readAllSamples Expected:\n", string(getExpectedSamples()), "\nActual:\n", string(res.Payload))
		t.FailNow()
	}
}

func checkError(t *testing.T, exp string, act string) {
	if strings.Compare(exp, act) != 0 {
		fmt.Println("Unexpected Error! Expecting ", exp, "\n Actual :", act)
		t.FailNow()
	}
}
