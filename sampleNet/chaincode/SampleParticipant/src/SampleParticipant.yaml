swagger: "2.0"

info:
  description: "The SampleParticipant chaincode can read/write SampleParticipants
    onto the blockchain and can expose these functions as REST API.
    THIS SAMPLE CODE MAY BE USED SOLELY AS PART OF THE TEST AND EVALUATION OF THE SAP CLOUD PLATFORM
    HYPERLEDGER SERVICE (THE “SERVICE”) AND IN ACCORDANCE WITH THE AGREEMENT FOR THE SERVICE.
    THIS SAMPLE CODE PROVIDED “AS IS”, WITHOUT ANY WARRANTY, ESCROW, TRAINING, MAINTENANCE, OR
    SERVICE OBLIGATIONS WHATSOEVER ON THE PART OF SAP."
  version: "1.0"
  title: "SampleParticipant"

consumes:
  - application/json

parameters:

  id:
    name: id
    in: path
    description: ID of the SampleParticipant
    required: true
    type: string
    maxLength: 64

definitions:
  sampleParticipant:
    type: object
    properties:
      ID:
        type: string
      docType:
        type: string
      alias:
        type: string
      description:
        type: string
      city:
        type: string
      country:
        type: string

  sampleParticipantHistory:
    type: object
    properties:
      values:
        type: array
        items:
          type: object
          properties:
            timestamp:
              type: string
            sample:
              $ref: '#/definitions/sampleParticipant'

  sampleParticipantByIDSearchResult:
    type: object
    properties:
      values:
        type: array
        items:
          type: object
          properties:
            ID:
              type: string
            sample:
              $ref: '#/definitions/sampleParticipant'
paths:

  /sampleParticipant:
    get:
      operationId: readAllSamples
      summary: Read all (existing) SampleParticipants
      produces:
      - application/json
      responses:
        200:
          description: OK
        500:
          description: Failed

    post:
      operationId: addNewSample
      summary: Adds a new SampleParticipant
      consumes:
      - application/json
      parameters:
      - in: body
        name: newSampleParticipant
        description: New SampleParticipant
        required: true
        schema:
          $ref: '#/definitions/sampleParticipant'
      responses:
        200:
          description: SampleParticipant Written
        500:
          description: Failed

  /sampleParticipant/update:
    post:
      operationId: updateSample
      summary: Update SampleParticipant
      consumes:
      - application/json
      parameters:
      - in: body
        name: Update SampleParticipant
        description: SampleParticipant for Update
        required: true
        schema:
          $ref: '#/definitions/sampleParticipant'
      responses:
        200:
          description: SampleParticipant Updated
        500:
          description: Failed

  /sampleParticipant/clear:
    delete:
      operationId: removeAllSamples
      summary: Remove all SampleParticipantss
      produces:
      - application/json
      responses:
        200:
          description: OK
        500:
          description: Failed

  /sampleParticipant/{id}:

    get:
      operationId: readSample
      summary: Read SampleParticipant by SampleParticipant ID
      parameters:
      - $ref: '#/parameters/id'
      produces:
      - application/json
      responses:
        200:
          description: OK
        500:
          description: Failed

    delete:
      operationId: removeSample
      summary: Remove SampleParticipant by SampleParticipant ID
      parameters:
      - $ref: '#/parameters/id'
      produces:
      - application/json
      responses:
        200:
          description: OK
        500:
          description: Failed
  /sampleParticipant/{id}/history:

    get:
      operationId: getSampleHistory
      summary: Return history of Sample by ID
      parameters:
      - $ref: '#/parameters/id'
      produces:
      - application/json
      responses:
        200:
          $ref: '#/definitions/sampleParticipantHistory'
        400:
          description: Parameter Mismatch
        404:
          description: Not Found

  /sampleParticipant/searchSamplesByID/{wildcard}:

    get:
      operationId: searchSamplesByID
      summary: Find samples by ID - supports wildcard search in the text strings
      description: Search for all matching IDs, given a (regex) value expression and return both the IDs and text. For example '^H.llo' will match any string starting with 'Hello' or 'Hallo'.
      parameters:
      - name: wildcard
        in: path
        description: Wildcard regular expression to match against texts
        required: true
        type: string
        maxLength: 64
      responses:
        200:
          $ref: '#/definitions/sampleParticipantByIDSearchResult'
        500:
          description: Internal Server Error
