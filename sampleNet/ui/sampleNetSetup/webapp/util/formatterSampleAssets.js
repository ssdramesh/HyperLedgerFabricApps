sap.ui.define(function() {
	"use strict";

	return {

		mapSampleAssetToModel:function(responseData){
			return {
				ID:responseData.ID,
				ObjectType:responseData.docType,
				
                Status:responseData.status,
                CreationDate:responseData.creationDate
,
                Temperature:responseData.temperature
			};
		},

		mapSampleAssetsToModel:function(responseData){

			var items = [];
			if( responseData ){
				for ( var i = 0; i < responseData.length; i++ ){
					items.push(this.mapSampleAssetToModel(responseData[i]));
				}
			}
			return items;
		},

		mapSampleAssetToChaincode:function(oModel, newSampleAsset){

			if ( newSampleAsset === true ) {
				return {
						"ID":this.getNewSampleAssetID(oModel),
						"docType":"Asset.SampleAsset",
						
                        "status":oModel.getProperty("/newSampleAsset/Status"),
                        "creationDate":oModel.getProperty("/newSampleAsset/CreationDate")
,
                  "temperature":oModel.getProperty("/newSampleAsset/Temperature")
				};
			} else {
				return {
						"ID":oModel.getProperty("/selectedSampleAsset/ID"),
						"docType":"Asset.SampleAsset",
						
                        "status":oModel.getProperty("/selectedSampleAsset/Status"),
                        "creationDate":oModel.getProperty("/selectedSampleAsset/CreationDate")
,
                  "temperature":oModel.getProperty("/selectedSampleAsset/Temperature")
				};
			}
		},

		mapSampleAssetToLocalStorage : function(oModel, newSampleAsset){

			return this.mapSampleAssetToChaincode(oModel, newSampleAsset);
		},

		getNewSampleAssetID:function(oModel){

		    if ( typeof oModel.getProperty("/newSampleAsset/ID") === "undefined" ||
		    		oModel.getProperty("/newSampleAsset/ID") === ""
		    	){
			    var iD = "SampleAsset";
			    var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
			    for( var i = 0; i < 8; i++ ){
			        iD += possible.charAt(Math.floor(Math.random() * possible.length));
			    }
		    } else {
				iD = oModel.getProperty("/newSampleAsset/ID");
			}
			oModel.setProperty("/newSampleAsset/ID",iD);
		    return iD;
		}
	};
});
