sap.ui.define(function() {
	"use strict";

	jQuery.sap.require("jquery.sap.storage");
	var oStorage = jQuery.sap.storage(jQuery.sap.storage.Type.local);
	var sampleParticipantsDataID = "sampleParticipants";

	return {

		init: function(){

			oStorage.put(sampleParticipantsDataID,[]);
			oStorage.put(
				sampleParticipantsDataID,
[
	{
		"ID":"SampleParticipant1001",
		"docType":"Participant.SampleParticipant",
		"alias":"BRITECH",
		"description":"British Technology Pvt. Ltd.",
		"city":"city001",
		"country":"country001"
	}
]
			);
		},

		getSampleParticipantDataID : function(){

			return sampleParticipantsDataID;
		},

		getSampleParticipantData  : function(){

			return oStorage.get("sampleParticipants");
		},

		put: function(newSampleParticipant){

			var sampleParticipantData = this.getSampleParticipantData();
			sampleParticipantData.push(newSampleParticipant);
			oStorage.put(sampleParticipantsDataID, sampleParticipantData);
		},

		patch: function(sampleParticipant){

			this.remove(sampleParticipant.ID);
			this.put(sampleParticipant);
		},

		remove : function (id){

			var sampleParticipantData = this.getSampleParticipantData();
			sampleParticipantData = _.without(sampleParticipantData,_.findWhere(sampleParticipantData,{ID:id}));
			oStorage.put(sampleParticipantsDataID, sampleParticipantData);
		},

		removeAll : function(){

			oStorage.put(sampleParticipantsDataID,[]);
		},

		clearSampleParticipantData: function(){

			oStorage.put(sampleParticipantsDataID,[]);
		}
	};
});
