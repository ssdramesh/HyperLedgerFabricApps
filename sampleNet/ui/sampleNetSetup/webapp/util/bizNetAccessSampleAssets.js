sap.ui.define([
	"sampleNetSetup/util/restBuilder",
	"sampleNetSetup/util/formatterSampleAssets",
	"sampleNetSetup/util/localStoreSampleAssets"
], function(
		restBuilder,
		formatterSampleAssets,
		localStoreSampleAssets
	) {
	"use strict";

	return {

		loadAllSampleAssets:function(oComponent, oModel) {

			if ( !( oComponent.getModel("TestSwitch").getProperty("/testMode") ) ){
				restBuilder.execute(
					oModel.getProperty("/chaincode"),
					"readAll",
					[],
					function(responseData){
						oModel.setProperty(
							"/sampleAssetCollection/items",
							formatterSampleAssets.mapSampleAssetsToModel(responseData)
						);
					});
			} else {
				oModel.setProperty(
					"/sampleAssetCollection/items",
					formatterSampleAssets.mapSampleAssetsToModel(localStoreSampleAssets.getSampleAssetData())
				);
			}
		},

		loadSampleAsset:function(oModel, selectedSampleAssetID){

			oModel.setProperty(
				"/selectedSampleAsset",
				_.findWhere(oModel.getProperty("/sampleAssetCollection/items"),
					{
						ID: selectedSampleAssetID
					},
				this));
		},

		addNewSampleAsset:function(oComponent, oModel){

			if ( !( oComponent.getModel("TestSwitch").getProperty("/testMode") ) ){
				restBuilder.execute(
					oModel.getProperty("/chaincode"),
					"addNew",
					formatterSampleAssets.mapSampleAssetToChaincode(oModel, true)
				);
			}  else {
				localStoreSampleAssets.put(formatterSampleAssets.mapSampleAssetToLocalStorage(oModel, true));
			}
			this.loadAllSampleAssets(oComponent, oModel);
			return oModel.getProperty("/newSampleAsset/ID");
		},

		updateSampleAsset: function (oComponent, oModel) {

			if (!(oComponent.getModel("TestSwitch").getProperty("/testMode"))) {
				restBuilder.execute(
					oModel.getProperty("/chaincode"),
					"update",
					formatterSampleAssets.mapSampleAssetToChaincode(oModel, false)
				);
			} else {
				localStoreSampleAssets.patch(formatterSampleAssets.mapSampleAssetToLocalStorage(oModel, false));
			}
			this.loadAllSampleAssets(oComponent, oModel);
		},

		removeSampleAsset : function(oComponent, oModel, sampleAssetID){

			if ( !( oComponent.getModel("TestSwitch").getProperty("/testMode") ) ) {
				restBuilder.execute(
					oModel.getProperty("/chaincode"),
					"remove",
					{ID:sampleAssetID}
				);
			} else {
				localStoreSampleAssets.remove(sampleAssetID);
			}
			this.loadAllSampleAssets(oComponent, oModel);
			return true;
		},

		removeAllSampleAssets : function(oComponent, oModel){

			if (!(oComponent.getModel("TestSwitch").getProperty("/testMode"))) {
				restBuilder.execute(
					oModel.getProperty("/chaincode"),
					"removeAll",
					[]
				);
			} else {
				localStoreSampleAssets.removeAll();
			}
			this.loadAllSampleAssets(oComponent, oModel);
			oModel.setProperty("/selectedSampleAsset",{});
			return true;
		}
	};
});
