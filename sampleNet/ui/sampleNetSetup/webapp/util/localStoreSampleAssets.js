sap.ui.define(function() {
	"use strict";

	jQuery.sap.require("jquery.sap.storage");
	var oStorage = jQuery.sap.storage(jQuery.sap.storage.Type.local);
	var sampleAssetsDataID = "sampleAssets";

	return {

		init: function(){

			oStorage.put(sampleAssetsDataID,[]);
			oStorage.put(
				sampleAssetsDataID,
[
	{
		"ID":"SampleAsset1001",
		"docType":"Asset.SampleAsset",
		"status":"Created",
		"creationDate":"12/28/2107",
		"temperature":"temperature001"
	}
]
			);
		},

		getSampleAssetDataID : function(){

			return sampleAssetsDataID;
		},

		getSampleAssetData  : function(){

			return oStorage.get("sampleAssets");
		},

		put: function(newSampleAsset){

			var sampleAssetData = this.getSampleAssetData();
			sampleAssetData.push(newSampleAsset);
			oStorage.put(sampleAssetsDataID, sampleAssetData);
		},

		patch: function(sampleAsset){

			this.remove(sampleAsset.ID);
			this.put(sampleAsset);
		},

		remove : function (id){

			var sampleAssetData = this.getSampleAssetData();
			sampleAssetData = _.without(sampleAssetData,_.findWhere(sampleAssetData,{ID:id}));
			oStorage.put(sampleAssetsDataID, sampleAssetData);
		},

		removeAll : function(){

			oStorage.put(sampleAssetsDataID,[]);
		},

		clearSampleAssetData: function(){

			oStorage.put(sampleAssetsDataID,[]);
		}
	};
});
