sap.ui.define(function() {
	"use strict";

	return {

		mapSampleParticipantToModel:function(responseData){
			return {
				ID:responseData.ID,
				ObjectType:responseData.docType,
				
                Alias:responseData.alias,
                Description:responseData.description
,
                City:responseData.city,
                Country:responseData.country
			};
		},

		mapSampleParticipantsToModel:function(responseData){

			var items = [];
			if( responseData ){
				for ( var i = 0; i < responseData.length; i++ ){
					items.push(this.mapSampleParticipantToModel(responseData[i]));
				}
			}
			return items;
		},

		mapSampleParticipantToChaincode:function(oModel, newSampleParticipant){

			if ( newSampleParticipant === true ) {
				return {
						"ID":this.getNewSampleParticipantID(oModel),
						"docType":"Participant.SampleParticipant",
						
                        "alias":oModel.getProperty("/newSampleParticipant/Alias"),
                        "description":oModel.getProperty("/newSampleParticipant/Description")
,
                  "city":oModel.getProperty("/newSampleParticipant/City"),
                  "country":oModel.getProperty("/newSampleParticipant/Country")
				};
			} else {
				return {
						"ID":oModel.getProperty("/selectedSampleParticipant/ID"),
						"docType":"Participant.SampleParticipant",
						
                        "alias":oModel.getProperty("/selectedSampleParticipant/Alias"),
                        "description":oModel.getProperty("/selectedSampleParticipant/Description")
,
                  "city":oModel.getProperty("/selectedSampleParticipant/City"),
                  "country":oModel.getProperty("/selectedSampleParticipant/Country")
				};
			}
		},

		mapSampleParticipantToLocalStorage : function(oModel, newSampleParticipant){

			return this.mapSampleParticipantToChaincode(oModel, newSampleParticipant);
		},

		getNewSampleParticipantID:function(oModel){

		    if ( typeof oModel.getProperty("/newSampleParticipant/ID") === "undefined" ||
		    		oModel.getProperty("/newSampleParticipant/ID") === ""
		    	){
			    var iD = "SampleParticipant";
			    var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
			    for( var i = 0; i < 8; i++ ){
			        iD += possible.charAt(Math.floor(Math.random() * possible.length));
			    }
		    } else {
				iD = oModel.getProperty("/newSampleParticipant/ID");
			}
			oModel.setProperty("/newSampleParticipant/ID",iD);
		    return iD;
		}
	};
});
