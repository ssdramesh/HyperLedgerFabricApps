sap.ui.define([
	"sampleNetSetup/util/restBuilder",
	"sampleNetSetup/util/formatterSampleParticipants",
	"sampleNetSetup/util/localStoreSampleParticipants"
], function(
		restBuilder,
		formatterSampleParticipants,
		localStoreSampleParticipants
	) {
	"use strict";

	return {

		loadAllSampleParticipants:function(oComponent, oModel) {

			if ( !( oComponent.getModel("TestSwitch").getProperty("/testMode") ) ){
				restBuilder.execute(
					oModel.getProperty("/chaincode"),
					"readAll",
					[],
					function(responseData){
						oModel.setProperty(
							"/sampleParticipantCollection/items",
							formatterSampleParticipants.mapSampleParticipantsToModel(responseData)
						);
					});
			} else {
				oModel.setProperty(
					"/sampleParticipantCollection/items",
					formatterSampleParticipants.mapSampleParticipantsToModel(localStoreSampleParticipants.getSampleParticipantData())
				);
			}
		},

		loadSampleParticipant:function(oModel, selectedSampleParticipantID){

			oModel.setProperty(
				"/selectedSampleParticipant",
				_.findWhere(oModel.getProperty("/sampleParticipantCollection/items"),
					{
						ID: selectedSampleParticipantID
					},
				this));
		},

		addNewSampleParticipant:function(oComponent, oModel){

			if ( !( oComponent.getModel("TestSwitch").getProperty("/testMode") ) ){
				restBuilder.execute(
					oModel.getProperty("/chaincode"),
					"addNew",
					formatterSampleParticipants.mapSampleParticipantToChaincode(oModel, true)
				);
			}  else {
				localStoreSampleParticipants.put(formatterSampleParticipants.mapSampleParticipantToLocalStorage(oModel, true));
			}
			this.loadAllSampleParticipants(oComponent, oModel);
			return oModel.getProperty("/newSampleParticipant/ID");
		},

		updateSampleParticipant: function (oComponent, oModel) {

			if (!(oComponent.getModel("TestSwitch").getProperty("/testMode"))) {
				restBuilder.execute(
					oModel.getProperty("/chaincode"),
					"update",
					formatterSampleParticipants.mapSampleParticipantToChaincode(oModel, false)
				);
			} else {
				localStoreSampleParticipants.patch(formatterSampleParticipants.mapSampleParticipantToLocalStorage(oModel, false));
			}
			this.loadAllSampleParticipants(oComponent, oModel);
		},

		removeSampleParticipant : function(oComponent, oModel, sampleParticipantID){

			if ( !( oComponent.getModel("TestSwitch").getProperty("/testMode") ) ) {
				restBuilder.execute(
					oModel.getProperty("/chaincode"),
					"remove",
					{ID:sampleParticipantID}
				);
			} else {
				localStoreSampleParticipants.remove(sampleParticipantID);
			}
			this.loadAllSampleParticipants(oComponent, oModel);
			return true;
		},

		removeAllSampleParticipants : function(oComponent, oModel){

			if (!(oComponent.getModel("TestSwitch").getProperty("/testMode"))) {
				restBuilder.execute(
					oModel.getProperty("/chaincode"),
					"removeAll",
					[]
				);
			} else {
				localStoreSampleParticipants.removeAll();
			}
			this.loadAllSampleParticipants(oComponent, oModel);
			oModel.setProperty("/selectedSampleParticipant",{});
			return true;
		}
	};
});
