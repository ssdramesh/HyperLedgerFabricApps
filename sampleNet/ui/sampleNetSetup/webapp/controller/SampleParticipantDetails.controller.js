sap.ui.define([
	"sampleNetSetup/controller/BaseController",
	"sampleNetSetup/util/bizNetAccessSampleParticipants"
], function(
		BaseController,
		bizNetAccessSampleParticipants
	) {
	"use strict";

	return BaseController.extend("sampleNetSetup.controller.SampleParticipantDetails", {

		onInit : function(){

			this.getOwnerComponent().getRouter().getRoute("sampleParticipant").attachPatternMatched(this._onObjectMatched, this);
		},

		_onObjectMatched : function (oEvent) {

			var pId = oEvent.getParameter("arguments").sampleParticipantId;
			if ( pId === "___new" ) {
				this.getView().byId("__barSampleParticipant").setSelectedKey("New");
			} else {
				bizNetAccessSampleParticipants.loadSampleParticipant(this.getView().getModel("SampleParticipants"), oEvent.getParameter("arguments").sampleParticipantId);
			}
		},

		addNew : function(){

			var oModel = this.getView().getModel("SampleParticipants");
			if ( oModel.getProperty("/newSampleParticipant/Alias") !== "" ||
				   oModel.getProperty("/newSampleParticipant/Description") !== "" ) {
				var sampleParticipantId = bizNetAccessSampleParticipants.addNewSampleParticipant(this.getOwnerComponent(), oModel);
				this.getView().byId("__barSampleParticipant").setSelectedKey("Details");
				bizNetAccessSampleParticipants.loadSampleParticipant(this.getView().getModel("SampleParticipants"), sampleParticipantId);
			} else {
				sap.m.MessageToast.show("Please enter valid data in all fields", {});
			}
			this._clearNewAsset();
		},

		saveSampleParticipant : function(){

			var oModel = this.getView().getModel("SampleParticipants");
			bizNetAccessSampleParticipants.updateSampleParticipant(this.getOwnerComponent(), oModel);
			this.editSampleParticipant();
		},

		removeSampleParticipant : function(){

			var oModel = this.getView().getModel("SampleParticipants");
			bizNetAccessSampleParticipants.removeSampleParticipant(this.getOwnerComponent(), oModel, oModel.getProperty("/selectedSampleParticipant/ID"));
		},

		editSampleParticipant : function(oEvent){
/*
		This is ONLY an example of switching editability of an input element
		Please copy and adjust as per the use case requirements to each of the
		fields that are editable after creation.
		After you are done, DELETE this comment!

*/
			var isEditable = this.byId("__inputSelSampleAssetStatus").getEditable();
			if (isEditable === true) {
				this.byId("__inputSelSampleAssetStatus").setEditable(false);
			} else {
				this.byId("__inputSelSampleAssetStatus").setEditable(true);
			}
		},

		_clearNewAsset : function(){

			this.getView().getModel("SampleParticipants").setProperty("/newSampleParticipant",{});
		}
	});
});
