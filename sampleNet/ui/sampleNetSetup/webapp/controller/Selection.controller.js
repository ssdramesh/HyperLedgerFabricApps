sap.ui.define([
	"sampleNetSetup/controller/BaseController",
	"sampleNetSetup/util/messageProvider",

        "sampleNetSetup/util/bizNetAccessSampleAssets",
        "sampleNetSetup/model/modelsSampleAssets",

        "sampleNetSetup/util/bizNetAccessSampleParticipants",
        "sampleNetSetup/model/modelsSampleParticipants",

	"sampleNetSetup/model/modelsBase"
], function(
		BaseController,
		messageProvider,

        bizNetAccessSampleAssets,
        modelsSampleAssets,

        bizNetAccessSampleParticipants,
        modelsSampleParticipants,

		modelsBase
	) {
	"use strict";

	return BaseController.extend("sampleNetSetup.controller.Selection", {

		onInit: function(){
			if ( typeof this.getOwnerComponent().getModel("Messages") === "undefined" ){
				this.getOwnerComponent().setModel(modelsBase.createMessagesModel(), "Messages");
			}
			messageProvider.init(this.getOwnerComponent().getModel("Messages"));
		},

		onSelect: function(oEvent) {
			switch (oEvent.getSource().getText()) {

        case "SampleAssets":
          this.getView().getModel("Selection").setProperty("/entityName", "sampleAsset");
          this.getOwnerComponent().setModel(modelsSampleAssets.createSampleAssetsModel(), "SampleAssets");
          this.loadMetaData("sampleAsset", this.getModel("SampleAssets"));
          bizNetAccessSampleAssets.loadAllSampleAssets(this.getOwnerComponent(), this.getView().getModel("SampleAssets"));
          this.getOwnerComponent().getRouter().navTo("sampleAssets", {});
          break;

        case "SampleParticipants":
          this.getView().getModel("Selection").setProperty("/entityName", "sampleParticipant");
          this.getOwnerComponent().setModel(modelsSampleParticipants.createSampleParticipantsModel(), "SampleParticipants");
          this.loadMetaData("sampleParticipant", this.getModel("SampleParticipants"));
          bizNetAccessSampleParticipants.loadAllSampleParticipants(this.getOwnerComponent(), this.getView().getModel("SampleParticipants"));
          this.getOwnerComponent().getRouter().navTo("sampleParticipants", {});
          break;

			}
		},

		removeAllEntities: function() {

			var msgStripID = this.getView().byId("__stripMessage");
			var location = this.getRunMode().location;


      if (typeof this.getOwnerComponent().getModel("SampleAssets") === "undefined") {
        this.getOwnerComponent().setModel(modelsSampleAssets.createSampleAssetsModel(), "SampleAssets");
      }
      bizNetAccessSampleAssets.removeAllSampleAssets(this.getOwnerComponent(), this.getView().getModel("SampleAssets"));
      messageProvider.addMessage("Success", "All SampleAssets deleted from sample-network", "No Description", location, 1, "", "http://www.sap.com");

      if (typeof this.getOwnerComponent().getModel("SampleParticipants") === "undefined") {
        this.getOwnerComponent().setModel(modelsSampleParticipants.createSampleParticipantsModel(), "SampleParticipants");
      }
      bizNetAccessSampleParticipants.removeAllSampleParticipants(this.getOwnerComponent(), this.getView().getModel("SampleParticipants"));
      messageProvider.addMessage("Success", "All SampleParticipants deleted from sample-network", "No Description", location, 1, "", "http://www.sap.com");


			this.showMessageStrip(msgStripID,"All Existing sampleNet data deleted from SAP BaaS", "S");
		},

		onToggleBaaS: function() {

			this.onToggleRunMode(this.getView().byId("__stripMessage"));
		},

		onMessagePress: function() {

			this.onShowMessageDialog("sampleNet Maintenance Log");
		}
	});
});
