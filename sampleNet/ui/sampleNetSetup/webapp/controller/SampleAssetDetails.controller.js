sap.ui.define([
	"sampleNetSetup/controller/BaseController",
	"sampleNetSetup/util/bizNetAccessSampleAssets"
], function(
		BaseController,
		bizNetAccessSampleAssets
	) {
	"use strict";

	return BaseController.extend("sampleNetSetup.controller.SampleAssetDetails", {

		onInit : function(){

			this.getOwnerComponent().getRouter().getRoute("sampleAsset").attachPatternMatched(this._onObjectMatched, this);
		},

		_onObjectMatched : function (oEvent) {

			var pId = oEvent.getParameter("arguments").sampleAssetId;
			if ( pId === "___new" ) {
				this.getView().byId("__barSampleAsset").setSelectedKey("New");
			} else {
				bizNetAccessSampleAssets.loadSampleAsset(this.getView().getModel("SampleAssets"), oEvent.getParameter("arguments").sampleAssetId);
			}
		},

		addNew : function(){

			var oModel = this.getView().getModel("SampleAssets");
			if ( oModel.getProperty("/newSampleAsset/Alias") !== "" ||
				   oModel.getProperty("/newSampleAsset/Description") !== "" ) {
				var sampleAssetId = bizNetAccessSampleAssets.addNewSampleAsset(this.getOwnerComponent(), oModel);
				this.getView().byId("__barSampleAsset").setSelectedKey("Details");
				bizNetAccessSampleAssets.loadSampleAsset(this.getView().getModel("SampleAssets"), sampleAssetId);
			} else {
				sap.m.MessageToast.show("Please enter valid data in all fields", {});
			}
			this._clearNewAsset();
		},

		saveSampleAsset : function(){

			var oModel = this.getView().getModel("SampleAssets");
			bizNetAccessSampleAssets.updateSampleAsset(this.getOwnerComponent(), oModel);
			this.editSampleAsset();
		},

		removeSampleAsset : function(){

			var oModel = this.getView().getModel("SampleAssets");
			bizNetAccessSampleAssets.removeSampleAsset(this.getOwnerComponent(), oModel, oModel.getProperty("/selectedSampleAsset/ID"));
		},

		editSampleAsset : function(oEvent){
/*
		This is ONLY an example of switching editability of an input element
		Please copy and adjust as per the use case requirements to each of the
		fields that are editable after creation.
		After you are done, DELETE this comment!

*/
			var isEditable = this.byId("__inputSelSampleAssetStatus").getEditable();
			if (isEditable === true) {
				this.byId("__inputSelSampleAssetStatus").setEditable(false);
			} else {
				this.byId("__inputSelSampleAssetStatus").setEditable(true);
			}
		},

		_clearNewAsset : function(){

			this.getView().getModel("SampleAssets").setProperty("/newSampleAsset",{});
		}
	});
});
