sap.ui.define([
	"sampleNetSetup/controller/BaseController",
	"sampleNetSetup/model/modelsBase",
	"sampleNetSetup/util/messageProvider",
	"sampleNetSetup/util/localStoreSampleAssets",
	"sampleNetSetup/util/bizNetAccessSampleAssets",
	"sap/ui/core/routing/History",
	"sap/ui/model/Filter",
	"sap/ui/model/FilterOperator",
	"sap/ui/Device"
], function(
		BaseController,
		modelsBase,
		messageProvider,
		localStoreSampleAssets,
		bizNetAccessSampleAssets,
		History,
		Filter,
		FilterOperator,
		Device
	) {
	"use strict";

	return BaseController.extend("sampleNetSetup.controller.SampleAssetsOverview", {

		onInit : function(){

			var oList = this.byId("sampleAssetsList");
			this._oList = oList;
			this._oListFilterState = {
				aFilter : [],
				aSearch : []
			};
			localStoreSampleAssets.init();
			if ( typeof this.getOwnerComponent().getModel("Messages") === "undefined" ){
				this.getOwnerComponent().setModel(modelsBase.createMessagesModel(), "Messages");
			}
			messageProvider.init(this.getOwnerComponent().getModel("Messages"));
		},

		onSelectionChange : function (oEvent) {

			this._showDetail(oEvent.getParameter("listItem") || oEvent.getSource());
		},

		onAdd : function() {

			this.getRouter().navTo("sampleAsset", {sampleAssetId : "___new"});
		},

		onRemoveAll : function(){

			bizNetAccessSampleAssets.removeAllSampleAssets(this.getOwnerComponent(), this.getOwnerComponent().getModel("SampleAssets"));
		},

		onSearch : function(oEvent) {

			var sQuery = oEvent.getParameter("query");
			if (sQuery) {
				this._oListFilterState.aSearch = [new Filter("ID", FilterOperator.Contains, sQuery)];
			} else {
				this._oListFilterState.aSearch = [];
			}
			this._applyFilterSearch();
			var bReplace = !Device.system.phone;
			var oModel = this.getModel("SampleAssets");
			oModel.setProperty(
				"/selectedSampleAsset",
				_.findWhere(oModel.getProperty("/sampleAssetCollection/items"),
					{
						ID: oModel.getProperty("/searchSampleAssetID")
					},
				this));
			this.getRouter().navTo("sampleAsset", {
				sampleAssetId : oModel.getProperty("/selectedSampleAsset").ID
			}, bReplace);
		},

		_onToggleBaaS : function(){

			this.getOwnerComponent().getModel("TestSwitch").setProperty("/testMode",!(this.getView().byId("__buttonToggleSampleAssets").getPressed()));
			var location = this.getRunMode().location;
			messageProvider.addMessage("Success", "Switching Network for Blockchain Data to: ", "No Description", location, 1, "", "http://www.sap.com");
			if ( this.getRunMode().testMode && localStoreSampleAssets.getSampleAssetData() === null ){
				localStoreSampleAssets.init();
			}
			bizNetAccessSampleAssets.loadAllSampleAssets(this.getOwnerComponent(), this.getModel("SampleAssets"));
		},

		_showDetail : function(oItem) {

			var bReplace = !Device.system.phone;
			var oModel = this.getModel("SampleAssets");
			this.getRouter().navTo("sampleAsset", {
				sampleAssetId : oModel.getObject(oItem.getBindingContextPath()).ID
			}, bReplace);
		},

		_applyFilterSearch : function () {
			var aFilters = this._oListFilterState.aSearch.concat(this._oListFilterState.aFilter),
				oViewModel = this.getModel("SampleAssets");
			this._oList.getBinding("items").filter(aFilters, "Application");
			if (aFilters.length !== 0) {
				oViewModel.setProperty("/noDataText", this.getResourceBundle().getText("_labelNoSampleAssetsText"));
			}
		},

		onMessagePress: function() {

			this.onShowMessageDialog("sampleNetSetup Log");
		},

		onNavBack : function() {
			this.getRouter().navTo("home");
		}
	});
});
