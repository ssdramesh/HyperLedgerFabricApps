sap.ui.define([
	"sampleNetSetup/controller/BaseController",
	"sampleNetSetup/model/modelsBase",
	"sampleNetSetup/util/messageProvider",
	"sampleNetSetup/util/localStoreSampleParticipants",
	"sampleNetSetup/util/bizNetAccessSampleParticipants",
	"sap/ui/core/routing/History",
	"sap/ui/model/Filter",
	"sap/ui/model/FilterOperator",
	"sap/ui/Device"
], function(
		BaseController,
		modelsBase,
		messageProvider,
		localStoreSampleParticipants,
		bizNetAccessSampleParticipants,
		History,
		Filter,
		FilterOperator,
		Device
	) {
	"use strict";

	return BaseController.extend("sampleNetSetup.controller.SampleParticipantsOverview", {

		onInit : function(){

			var oList = this.byId("sampleParticipantsList");
			this._oList = oList;
			this._oListFilterState = {
				aFilter : [],
				aSearch : []
			};
			localStoreSampleParticipants.init();
			if ( typeof this.getOwnerComponent().getModel("Messages") === "undefined" ){
				this.getOwnerComponent().setModel(modelsBase.createMessagesModel(), "Messages");
			}
			messageProvider.init(this.getOwnerComponent().getModel("Messages"));
		},

		onSelectionChange : function (oEvent) {

			this._showDetail(oEvent.getParameter("listItem") || oEvent.getSource());
		},

		onAdd : function() {

			this.getRouter().navTo("sampleParticipant", {sampleParticipantId : "___new"});
		},

		onRemoveAll : function(){

			bizNetAccessSampleParticipants.removeAllSampleParticipants(this.getOwnerComponent(), this.getOwnerComponent().getModel("SampleParticipants"));
		},

		onSearch : function(oEvent) {

			var sQuery = oEvent.getParameter("query");
			if (sQuery) {
				this._oListFilterState.aSearch = [new Filter("ID", FilterOperator.Contains, sQuery)];
			} else {
				this._oListFilterState.aSearch = [];
			}
			this._applyFilterSearch();
			var bReplace = !Device.system.phone;
			var oModel = this.getModel("SampleParticipants");
			oModel.setProperty(
				"/selectedSampleParticipant",
				_.findWhere(oModel.getProperty("/sampleParticipantCollection/items"),
					{
						ID: oModel.getProperty("/searchSampleParticipantID")
					},
				this));
			this.getRouter().navTo("sampleParticipant", {
				sampleParticipantId : oModel.getProperty("/selectedSampleParticipant").ID
			}, bReplace);
		},

		_onToggleBaaS : function(){

			this.getOwnerComponent().getModel("TestSwitch").setProperty("/testMode",!(this.getView().byId("__buttonToggleSampleParticipants").getPressed()));
			var location = this.getRunMode().location;
			messageProvider.addMessage("Success", "Switching Network for Blockchain Data to: ", "No Description", location, 1, "", "http://www.sap.com");
			if ( this.getRunMode().testMode && localStoreSampleParticipants.getSampleParticipantData() === null ){
				localStoreSampleParticipants.init();
			}
			bizNetAccessSampleParticipants.loadAllSampleParticipants(this.getOwnerComponent(), this.getModel("SampleParticipants"));
		},

		_showDetail : function(oItem) {

			var bReplace = !Device.system.phone;
			var oModel = this.getModel("SampleParticipants");
			this.getRouter().navTo("sampleParticipant", {
				sampleParticipantId : oModel.getObject(oItem.getBindingContextPath()).ID
			}, bReplace);
		},

		_applyFilterSearch : function () {
			var aFilters = this._oListFilterState.aSearch.concat(this._oListFilterState.aFilter),
				oViewModel = this.getModel("SampleParticipants");
			this._oList.getBinding("items").filter(aFilters, "Application");
			if (aFilters.length !== 0) {
				oViewModel.setProperty("/noDataText", this.getResourceBundle().getText("_labelNoSampleParticipantsText"));
			}
		},

		onMessagePress: function() {

			this.onShowMessageDialog("sampleNetSetup Log");
		},

		onNavBack : function() {
			this.getRouter().navTo("home");
		}
	});
});
