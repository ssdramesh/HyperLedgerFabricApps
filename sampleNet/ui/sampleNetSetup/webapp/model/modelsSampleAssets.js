sap.ui.define([
	"sap/ui/model/json/JSONModel"
], function(JSONModel) {
	"use strict";

	return {

		createSampleAssetsModel: function(){

			var oModel = new JSONModel();
			oModel.setDefaultBindingMode("TwoWay");
			oModel.setData({
				chaincode:{
					serviceUrl:"",
					serviceKey:{},
					sampleAsset:{}
				},
				sampleAssetCollection	: {
					cols:[
						{name:"ID"},
						{name:"ObjectType"},
						{name:"Status"},
						{name:"CreationDate"},
            {name:"Temperature"}
					],
					items:[]
				},
				selectedSampleAsset:{},
				newSampleAsset:{
					ID:"",
					ObjectType:"",
					Status:"",
					CreationDate:"",
          Temperature:""
				},
				selectedSampleAssetID	: "",
				searchSampleAssetID : ""
			});
			return oModel;
		}
	};
});
