sap.ui.define([
	"sap/ui/model/json/JSONModel"
], function(JSONModel) {
	"use strict";

	return {

		createSampleParticipantsModel: function(){

			var oModel = new JSONModel();
			oModel.setDefaultBindingMode("TwoWay");
			oModel.setData({
				chaincode:{
					serviceUrl:"",
					serviceKey:{},
					sampleParticipant:{}
				},
				sampleParticipantCollection	: {
					cols:[
						{name:"ID"},
						{name:"ObjectType"},
						{name:"Status"},
						{name:"CreationDate"},
            {name:"City"},
            {name:"Country"}
					],
					items:[]
				},
				selectedSampleParticipant:{},
				newSampleParticipant:{
					ID:"",
					ObjectType:"",
					Status:"",
					CreationDate:"",
          City:"",
          Country:""
				},
				selectedSampleParticipantID	: "",
				searchSampleParticipantID : ""
			});
			return oModel;
		}
	};
});
