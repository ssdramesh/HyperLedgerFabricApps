#!/bin/bash

cd /Users/i047582/Documents/Workspaces/github.com/ssdramesh/HyperLedgerFabricApps/sampleNet/test/newman/CleanAllAndSetup


echo "Clean All SampleAssets before Setup..."
newman run cleanAllSampleAssets.postman_collection.json -e sampleNet.postman_environment.json --bail newman
echo "Setting up SampleAssets..."
newman run createAllSampleAssets.postman_collection.json -e sampleNet.postman_environment.json -d ./data/SampleAssets.json --bail newman


echo "Clean All SampleParticipants before Setup..."
newman run cleanAllSampleParticipants.postman_collection.json -e sampleNet.postman_environment.json --bail newman
echo "Setting up SampleParticipants..."
newman run createAllSampleParticipants.postman_collection.json -e sampleNet.postman_environment.json -d ./data/SampleParticipants.json --bail newman

echo "Done. Ready to run!"
