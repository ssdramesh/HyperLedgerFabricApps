sap.ui.define(function () {
	"use strict";

	jQuery.sap.require("jquery.sap.storage");
	var oStorage = jQuery.sap.storage(jQuery.sap.storage.Type.local);
	var labResultAssetsDataID = "labResultAssets";

	return {

		init: function () {

			oStorage.put(labResultAssetsDataID, []);
			oStorage.put(
				labResultAssetsDataID, [{
					"ID": "ASTLR100001",
					"docType": "Asset.LabResultAsset",
					"status": "Completed",
					"creationDate": "09/20/2018",
					"labID": "OMIC-Bangkok",
					"labSampleID": "ASTLS100001",
					"certificateID": "DMSC18-9657681",
					"bioTestResult": {
						"disease": "none",
						"antibioticPercent": "13",
						"contaminationPercent": "0.03"
					},
					"quality": {
						"nIRSProteinPercent": "21.2",
						"fatPercent": "2.6"
					}
				}, {
					"ID": "ASTLR100002",
					"docType": "Asset.LabResultAsset",
					"status": "Certified",
					"creationDate": "09/11/2018",
					"labID": "OMIC-Bangkok",
					"labSampleID": "ASTLS100002",
					"certificateID": "DMSC18-9657681",
					"bioTestResult": {
						"disease": "none",
						"antibioticPercent": "21",
						"contaminationPercent": "0.05"
					},
					"quality": {
						"nIRSProteinPercent": "19.8",
						"fatPercent": "3.2"
					}
				}]
			);
		},

		getLabResultAssetDataID: function () {

			return labResultAssetsDataID;
		},

		getLabResultAssetData: function () {

			return oStorage.get("labResultAssets");
		},

		put: function (newLabResultAsset) {

			var labResultAssetData = this.getLabResultAssetData();
			labResultAssetData.push(newLabResultAsset);
			oStorage.put(labResultAssetsDataID, labResultAssetData);
		},

		patch: function (labResultAsset) {

			this.remove(labResultAsset.ID);
			this.put(labResultAsset);
		},

		remove: function (id) {

			var labResultAssetData = this.getLabResultAssetData();
			labResultAssetData = _.without(labResultAssetData, _.findWhere(labResultAssetData, {
				ID: id
			}));
			oStorage.put(labResultAssetsDataID, labResultAssetData);
		},

		removeAll: function () {

			oStorage.put(labResultAssetsDataID, []);
		},

		clearLabResultAssetData: function () {

			oStorage.put(labResultAssetsDataID, []);
		}
	};
});