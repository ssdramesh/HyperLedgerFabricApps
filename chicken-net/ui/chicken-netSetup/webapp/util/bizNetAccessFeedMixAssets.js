sap.ui.define([
	"chicken-netSetup/util/restBuilder",
	"chicken-netSetup/util/formatterFeedMixAssets",
	"chicken-netSetup/util/localStoreFeedMixAssets"
], function(
		restBuilder,
		formatterFeedMixAssets,
		localStoreFeedMixAssets
	) {
	"use strict";

	return {

		loadAllFeedMixAssets:function(oComponent, oModel) {

			if ( !( oComponent.getModel("TestSwitch").getProperty("/testMode") ) ){
				restBuilder.execute(
					oModel.getProperty("/chaincode"),
					"readAll",
					[],
					function(responseData){
						oModel.setProperty(
							"/feedMixAssetCollection/items",
							formatterFeedMixAssets.mapFeedMixAssetsToModel(responseData)
						);
					});
			} else {
				oModel.setProperty(
					"/feedMixAssetCollection/items",
					formatterFeedMixAssets.mapFeedMixAssetsToModel(localStoreFeedMixAssets.getFeedMixAssetData())
				);
			}
		},

		loadFeedMixAsset:function(oModel, selectedFeedMixAssetID){

			oModel.setProperty(
				"/selectedFeedMixAsset",
				_.findWhere(oModel.getProperty("/feedMixAssetCollection/items"),
					{
						ID: selectedFeedMixAssetID
					},
				this));
		},

		addNewFeedMixAsset:function(oComponent, oModel){

			if ( !( oComponent.getModel("TestSwitch").getProperty("/testMode") ) ){
				restBuilder.execute(
					oModel.getProperty("/chaincode"),
					"addNew",
					formatterFeedMixAssets.mapFeedMixAssetToChaincode(oModel, true)
				);
			}  else {
				localStoreFeedMixAssets.put(formatterFeedMixAssets.mapFeedMixAssetToLocalStorage(oModel, true));
			}
			this.loadAllFeedMixAssets(oComponent, oModel);
			return oModel.getProperty("/newFeedMixAsset/ID");
		},

		updateFeedMixAsset: function (oComponent, oModel) {

			if (!(oComponent.getModel("TestSwitch").getProperty("/testMode"))) {
				restBuilder.execute(
					oModel.getProperty("/chaincode"),
					"update",
					formatterFeedMixAssets.mapFeedMixAssetToChaincode(oModel, false)
				);
			} else {
				localStoreFeedMixAssets.patch(formatterFeedMixAssets.mapFeedMixAssetToLocalStorage(oModel, false));
			}
			this.loadAllFeedMixAssets(oComponent, oModel);
		},

		removeFeedMixAsset : function(oComponent, oModel, feedMixAssetID){

			if ( !( oComponent.getModel("TestSwitch").getProperty("/testMode") ) ) {
				restBuilder.execute(
					oModel.getProperty("/chaincode"),
					"remove",
					{ID:feedMixAssetID}
				);
			} else {
				localStoreFeedMixAssets.remove(feedMixAssetID);
			}
			this.loadAllFeedMixAssets(oComponent, oModel);
			return true;
		},

		removeAllFeedMixAssets : function(oComponent, oModel){

			if (!(oComponent.getModel("TestSwitch").getProperty("/testMode"))) {
				restBuilder.execute(
					oModel.getProperty("/chaincode"),
					"removeAll",
					[]
				);
			} else {
				localStoreFeedMixAssets.removeAll();
			}
			this.loadAllFeedMixAssets(oComponent, oModel);
			oModel.setProperty("/selectedFeedMixAsset",{});
			return true;
		}
	};
});
