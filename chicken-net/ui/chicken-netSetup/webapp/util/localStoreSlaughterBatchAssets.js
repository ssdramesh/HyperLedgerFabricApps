sap.ui.define(function() {
	"use strict";

	jQuery.sap.require("jquery.sap.storage");
	var oStorage = jQuery.sap.storage(jQuery.sap.storage.Type.local);
	var slaughterBatchAssetsDataID = "slaughterBatchAssets";

	return {

		init: function(){

			oStorage.put(slaughterBatchAssetsDataID,[]);
			oStorage.put(
				slaughterBatchAssetsDataID,
				[
				  {
				    "ID": "ASTSB100001",
				    "docType": "Asset.SlaughterBatchAsset",
				    "status": "Processing Completed",
				    "creationDate": "09/20/2018",
				    "hatcheryFarmID": "HFBKK-001",
				    "growerFarmID": "GFBKK-001",
				    "henHouseID": "GFBKKHH-011",
				    "chickenLotID": "GFBKKHHCL-011001",
				    "husbandryLineageID": "HL-001",
				    "feedMixID": "ASTFM100001",
				    "feedFormulaID": "SBGMOF-001",
				    "feedStopDate": "09/17/2018",
				    "slaughterDate": "09/20/2018"
				  },
				  {
				    "ID": "ASTSB100002",
				    "docType": "Asset.SlaughterBatchAsset",
				    "status": "Processing Completed",
				    "creationDate": "09/09/2018",
				    "hatcheryFarmID": "HFPKT-002",
				    "growerFarmID": "GFPKT-002",
				    "henHouseID": "GFPKTHH-012",
				    "chickenLotID": "GFPKTHHCL-012002",
				    "husbandryLineageID": "HL-002",
				    "feedMixID": "ASTFM100001",
				    "feedFormulaID": "FF-002",
				    "feedStopDate": "09/06/2018",
				    "slaughterDate": "09/09/2018"
				  }
				]
			);
		},

		getSlaughterBatchAssetDataID : function(){

			return slaughterBatchAssetsDataID;
		},

		getSlaughterBatchAssetData  : function(){

			return oStorage.get("slaughterBatchAssets");
		},

		put: function(newSlaughterBatchAsset){

			var slaughterBatchAssetData = this.getSlaughterBatchAssetData();
			slaughterBatchAssetData.push(newSlaughterBatchAsset);
			oStorage.put(slaughterBatchAssetsDataID, slaughterBatchAssetData);
		},

		patch: function(slaughterBatchAsset){

			this.remove(slaughterBatchAsset.ID);
			this.put(slaughterBatchAsset);
		},

		remove : function (id){

			var slaughterBatchAssetData = this.getSlaughterBatchAssetData();
			slaughterBatchAssetData = _.without(slaughterBatchAssetData,_.findWhere(slaughterBatchAssetData,{ID:id}));
			oStorage.put(slaughterBatchAssetsDataID, slaughterBatchAssetData);
		},

		removeAll : function(){

			oStorage.put(slaughterBatchAssetsDataID,[]);
		},

		clearSlaughterBatchAssetData: function(){

			oStorage.put(slaughterBatchAssetsDataID,[]);
		}
	};
});
