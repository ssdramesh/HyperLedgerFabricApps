sap.ui.define(function () {
	"use strict";

	return {

		mapFeedMixAssetToModel: function (responseData) {
			return {
				ID: responseData.ID,
				ObjectType: responseData.docType,
				Status: responseData.status,
				CreationDate: responseData.creationDate,
				SupplierID: responseData.supplier.ID,
				SupplierLandDeedID: responseData.supplier.landDeedID,
				SupplierInvoiceID: responseData.supplier.invoiceID,
				PurchaserID: responseData.purchaser.ID,
				PurchaseOrderID: responseData.purchaser.purchaseOrderID,
				GoodsReceiptID: responseData.purchaser.goodsReceiptID,
				MaterialID: responseData.purchaser.materialID,
				TestResultID: responseData.testResult.ID,
				TestCertificateID: responseData.testResult.CertificateID,
				TestResultDate: responseData.testDate,
				Humidity: responseData.quality.humidity,
				ContaminationPercent: responseData.quality.contaminationPercent,
				InputType: responseData.inputType
			};
		},

		mapFeedMixAssetsToModel: function (responseData) {

			var items = [];
			if (responseData) {
				for (var i = 0; i < responseData.length; i++) {
					items.push(this.mapFeedMixAssetToModel(responseData[i]));
				}
			}
			return items;
		},

		mapFeedMixAssetToChaincode: function (oModel, newFeedMixAsset) {

			if (newFeedMixAsset === true) {
				return {
					"ID": this.getNewFeedMixAssetID(oModel),
					"docType": "Asset.FeedMixAsset",
					"status": oModel.getProperty("/newFeedMixAsset/Status"),
					"creationDate": oModel.getProperty("/newFeedMixAsset/CreationDate"),
					"supplier": {
						"ID": oModel.getProperty("/newFeedMixAsset/SupplierID"),
						"landDeedID": oModel.getProperty("/newFeedMixAsset/SupplierLandDeedID"),
						"invoiceID": oModel.getProperty("/newFeedMixAsset/SupplierInvoiceID")
					},
					"purchaser": {
						"ID": oModel.getProperty("/newFeedMixAsset/PurchaserID"),
						"purchaseOrderID": oModel.getProperty("/newFeedMixAsset/PurchaseOrderID"),
						"goodsReceiptID": oModel.getProperty("/newFeedMixAsset/GoodsReceiptID"),
						"materialID": oModel.getProperty("/newFeedMixAsset/MaterialID")
					},
					"testResult": {
						"ID": oModel.getProperty("/newFeedMixAsset/TestResultID"),
						"certificateID": oModel.getProperty("/newFeedMixAsset/TestResultCertificateID"),
						"date": oModel.getProperty("/newFeedMixAsset/TestResultDate")
					},
					"quality": {
						"humidity": oModel.getProperty("/newFeedMixAsset/Humidity"),
						"contaminationPercent": oModel.getProperty("/newFeedMixAsset/ContaminationPercent")
					},
					"inputType": oModel.getProperty("/newFeedMixAsset/InputType")
				};
			} else {
				return {
					"ID": this.getNewFeedMixAssetID(oModel),
					"docType": "Asset.FeedMixAsset",
					"status": oModel.getProperty("/selectedFeedMixAsset/Status"),
					"creationDate": oModel.getProperty("/selectedFeedMixAsset/CreationDate"),
					"supplier": {
						"ID": oModel.getProperty("/selectedFeedMixAsset/SupplierID"),
						"landDeedID": oModel.getProperty("/selectedFeedMixAsset/SupplierLandDeedID"),
						"invoiceID": oModel.getProperty("/selectedFeedMixAsset/SupplierInvoiceID")
					},
					"purchaser": {
						"ID": oModel.getProperty("/selectedFeedMixAsset/PurchaserID"),
						"purchaseOrderID": oModel.getProperty("/selectedFeedMixAsset/PurchaseOrderID"),
						"goodsReceiptID": oModel.getProperty("/selectedFeedMixAsset/GoodsReceiptID"),
						"materialID": oModel.getProperty("/selectedFeedMixAsset/MaterialID")
					},
					"testResult": {
						"ID": oModel.getProperty("/selectedFeedMixAsset/TestResultID"),
						"certificateID": oModel.getProperty("/selectedFeedMixAsset/TestResultCertificateID"),
						"date": oModel.getProperty("/selectedFeedMixAsset/TestResultDate")
					},
					"quality": {
						"humidity": oModel.getProperty("/selectedFeedMixAsset/Humidity"),
						"contaminationPercent": oModel.getProperty("/selectedFeedMixAsset/ContaminationPercent")
					},
					"inputType": oModel.getProperty("/selectedFeedMixAsset/InputType")
				};
			}
		},

		mapFeedMixAssetToLocalStorage: function (oModel, newFeedMixAsset) {

			return this.mapFeedMixAssetToChaincode(oModel, newFeedMixAsset);
		},

		getNewFeedMixAssetID: function (oModel) {

			if (typeof oModel.getProperty("/newFeedMixAsset/ID") === "undefined" ||
				oModel.getProperty("/newFeedMixAsset/ID") === ""
			) {
				var iD = "ASTFM";
				var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
				for (var i = 0; i < 8; i++) {
					iD += possible.charAt(Math.floor(Math.random() * possible.length));
				}
			} else {
				iD = oModel.getProperty("/newFeedMixAsset/ID");
			}
			oModel.setProperty("/newFeedMixAsset/ID", iD);
			return iD;
		}
	};
});