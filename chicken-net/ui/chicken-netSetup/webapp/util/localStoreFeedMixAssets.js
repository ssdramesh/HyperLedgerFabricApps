sap.ui.define(function () {
	"use strict";

	jQuery.sap.require("jquery.sap.storage");
	var oStorage = jQuery.sap.storage(jQuery.sap.storage.Type.local);
	var feedMixAssetsDataID = "feedMixAssets";

	return {

		init: function () {

			oStorage.put(feedMixAssetsDataID, []);
			oStorage.put(
				feedMixAssetsDataID, [{
					"ID": "ASTFM100001",
					"docType": "Asset.FeedMixAsset",
					"status": "0",
					"creationDate": "12/01/2018",
					"supplier": {
						"ID": "AVAPLUS",
						"landDeedID": "066091/999-4521",
						"invoiceID": "INV-2018-09-18-746"
					},
					"purchaser": {
						"ID": "Charoen Pokphand Foods",
						"purchaseOrderID": "PO-2018-09-12-653",
						"goodsReceiptID": "GR-2018-09-17-8976",
						"materialID": "MM-2018-9877634"
					},
					"testResult": {
						"ID": "STL18-095348611",
						"certificateID": "STL18-9657681",
						"date": "09/18/2018"
					},
					"quality": {
						"humidity": "10.4",
						"contaminationPercent": "0.32"
					},
					"inputType": "Soybean GMO"
				}, {
					"ID": "ASTFM100002",
					"docType": "Asset.FeedMixAsset",
					"status": "0",
					"creationDate": "12/01/2018",
					"supplier": {
						"ID": "supplierID002",
						"landDeedID": "landDeedID002",
						"invoiceID": "invoiceID002"
					},
					"purchaser": {
						"ID": "purchaserID002",
						"purchaseOrderID": "purchaseOrderID002",
						"goodsReceiptID": "goodsReceiptID002",
						"materialID": "materialID002"
					},
					"testResult": {
						"ID": "testResultID002",
						"certificateID": "certificateID002",
						"date": "12/01/2018"
					},
					"quality": {
						"humidity": "humidity002",
						"contaminationPercent": "contaminationPercent002"
					},
					"inputType": "inputType002"
				}]
			);
		},

		getFeedMixAssetDataID: function () {

			return feedMixAssetsDataID;
		},

		getFeedMixAssetData: function () {

			return oStorage.get("feedMixAssets");
		},

		put: function (newFeedMixAsset) {

			var feedMixAssetData = this.getFeedMixAssetData();
			feedMixAssetData.push(newFeedMixAsset);
			oStorage.put(feedMixAssetsDataID, feedMixAssetData);
		},

		patch: function (feedMixAsset) {

			this.remove(feedMixAsset.ID);
			this.put(feedMixAsset);
		},

		remove: function (id) {

			var feedMixAssetData = this.getFeedMixAssetData();
			feedMixAssetData = _.without(feedMixAssetData, _.findWhere(feedMixAssetData, {
				ID: id
			}));
			oStorage.put(feedMixAssetsDataID, feedMixAssetData);
		},

		removeAll: function () {

			oStorage.put(feedMixAssetsDataID, []);
		},

		clearFeedMixAssetData: function () {

			oStorage.put(feedMixAssetsDataID, []);
		}
	};
});