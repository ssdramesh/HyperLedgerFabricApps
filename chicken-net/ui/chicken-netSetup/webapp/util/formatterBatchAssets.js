sap.ui.define(function () {
	"use strict";

	return {

		mapBatchAssetToModel: function (responseData) {
			return {
				ID: responseData.ID,
				ObjectType: responseData.docType,
				Status: responseData.status,
				CreationDate: responseData.creationDate,
				FeedMixID: responseData.feedMix.feedMixID,
				SupplierID: responseData.feedMix.supplierID,
				SupplierLandDeedID: responseData.feedMix.supplierLandDeedID,
				SlaughterBatchID: responseData.slaughterBatch.slaughterBatchID,
				FeedFormulaID: responseData.slaughterBatch.feedFormulaID,
				SalesOrderID: responseData.processData.salesOrderID,
				DeliveryOrderID: responseData.processData.deliveryOrderID,
				GoodsIssueID: responseData.processData.goodsIssueID,
				GoodsIssueBatchID: responseData.processData.goodsIssueBatchID,
				LabID: responseData.quality.labID,
				LabSampleID: responseData.quality.labSampleID,
				LabCertificateID: responseData.quality.certificateID,
				ProtienPercent: responseData.quality.protienPercent,
				FatPercent: responseData.quality.fatPercent,
				Disease: responseData.quality.disease,
				ContaminationPercent: responseData.quality.contaminationPercent,
				AntibioticPercent: responseData.quality.antibioticPercent
			};
		},

		mapBatchAssetsToModel: function (responseData) {

			var items = [];
			if (responseData) {
				for (var i = 0; i < responseData.length; i++) {
					items.push(this.mapBatchAssetToModel(responseData[i]));
				}
			}
			return items;
		},

		mapBatchAssetToChaincode: function (oModel, newBatchAsset) {

			if (newBatchAsset === true) {
				return {
					"ID": this.getNewBatchAssetID(oModel),
					"docType": "Asset.BatchAsset",
					"status": oModel.getProperty("/newBatchAsset/Status"),
					"creationDate": oModel.getProperty("/newBatchAsset/CreationDate"),
					"feedMix": {
						"feedMixID": oModel.getProperty("/newBatchAsset/FeedMixID"),
						"supplierID": oModel.getProperty("/newBatchAsset/SupplierID"),
						"supplierLandDeedID": oModel.getProperty("/newBatchAsset/SupplierLandDeedID")
					},
					"slaughterBatch": {
						"slaughterBatchID": oModel.getProperty("/newBatchAsset/SlaughterBatchID"),
						"feedFormulaID": oModel.getProperty("/newBatchAsset/FeedFormulaID")
					},
					"processData": {
						"salesOrderID": oModel.getProperty("/newBatchAsset/SalesOrderID"),
						"deliverOrderID": oModel.getProperty("/newBatchAsset/DeliveOrderID"),
						"goodsIssueID": oModel.getProperty("/newBatchAsset/GoodsIssueID"),
						"goodsIssueBatchID": oModel.getProperty("/newBatchAsset/GoodsIssueBatchID")
					},
					"quality": {
						"labID": oModel.getProperty("/newBatchAsset/LabID"),
						"labSampeID": oModel.getProperty("/newBatchAsset/LabSampleID"),
						"certificateID": oModel.getProperty("/newBatchAsset/LabCertificateID"),
						"protienPercent": oModel.getProperty("/newBatchAsset/ProtienPercent"),
						"fatPercent": oModel.getProperty("/newBatchAsset/FatPercent"),
						"disease": oModel.getProperty("/newBatchAsset/Disease"),
						"contaminationPercent": oModel.getProperty("/newBatchAsset/ContaminationPercent"),
						"antibioticPercent": oModel.getProperty("/newBatchAsset/AntibioticPercent")
					}
				};
			} else {
				return {
					"ID": this.getNewBatchAssetID(oModel),
					"docType": "Asset.BatchAsset",
					"status": oModel.getProperty("/selectedBatchAsset/Status"),
					"creationDate": oModel.getProperty("/selectedBatchAsset/CreationDate"),
					"feedMix": {
						"feedMixID": oModel.getProperty("/selectedBatchAsset/FeedMixID"),
						"supplierID": oModel.getProperty("/selectedBatchAsset/SupplierID"),
						"supplierLandDeedID": oModel.getProperty("/selectedBatchAsset/SupplierLandDeedID")
					},
					"slaughterBatch": {
						"slaughterBatchID": oModel.getProperty("/selectedBatchAsset/SlaughterBatchID"),
						"feedFormulaID": oModel.getProperty("/selectedBatchAsset/FeedFormulaID")
					},
					"processData": {
						"salesOrderID": oModel.getProperty("/selectedBatchAsset/SalesOrderID"),
						"deliverOrderID": oModel.getProperty("/selectedBatchAsset/DeliveOrderID"),
						"goodsIssueID": oModel.getProperty("/selectedBatchAsset/GoodsIssueID"),
						"goodsIssueBatchID": oModel.getProperty("/selectedBatchAsset/GoodsIssueBatchID")
					},
					"quality": {
						"labID": oModel.getProperty("/selectedBatchAsset/LabID"),
						"labSampeID": oModel.getProperty("/selectedBatchAsset/LabSampleID"),
						"certificateID": oModel.getProperty("/selectedBatchAsset/LabCertificateID"),
						"protienPercent": oModel.getProperty("/selectedBatchAsset/ProtienPercent"),
						"fatPercent": oModel.getProperty("/selectedBatchAsset/FatPercent"),
						"disease": oModel.getProperty("/selectedBatchAsset/Disease"),
						"contaminationPercent": oModel.getProperty("/selectedBatchAsset/ContaminationPercent"),
						"antibioticPercent": oModel.getProperty("/selectedBatchAsset/AntibioticPercent")
					}
				};
			}
		},

		mapBatchAssetToLocalStorage: function (oModel, newBatchAsset) {

			return this.mapBatchAssetToChaincode(oModel, newBatchAsset);
		},

		getNewBatchAssetID: function (oModel) {

			if (typeof oModel.getProperty("/newBatchAsset/ID") === "undefined" ||
				oModel.getProperty("/newBatchAsset/ID") === ""
			) {
				var iD = "ASTBA";
				var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
				for (var i = 0; i < 8; i++) {
					iD += possible.charAt(Math.floor(Math.random() * possible.length));
				}
			} else {
				iD = oModel.getProperty("/newBatchAsset/ID");
			}
			oModel.setProperty("/newBatchAsset/ID", iD);
			return iD;
		}
	};
});