sap.ui.define([
	"chicken-netSetup/util/restBuilder",
	"chicken-netSetup/util/formatterSlaughterBatchAssets",
	"chicken-netSetup/util/localStoreSlaughterBatchAssets"
], function(
		restBuilder,
		formatterSlaughterBatchAssets,
		localStoreSlaughterBatchAssets
	) {
	"use strict";

	return {

		loadAllSlaughterBatchAssets:function(oComponent, oModel) {

			if ( !( oComponent.getModel("TestSwitch").getProperty("/testMode") ) ){
				restBuilder.execute(
					oModel.getProperty("/chaincode"),
					"readAll",
					[],
					function(responseData){
						oModel.setProperty(
							"/slaughterBatchAssetCollection/items",
							formatterSlaughterBatchAssets.mapSlaughterBatchAssetsToModel(responseData)
						);
					});
			} else {
				oModel.setProperty(
					"/slaughterBatchAssetCollection/items",
					formatterSlaughterBatchAssets.mapSlaughterBatchAssetsToModel(localStoreSlaughterBatchAssets.getSlaughterBatchAssetData())
				);
			}
		},

		loadSlaughterBatchAsset:function(oModel, selectedSlaughterBatchAssetID){

			oModel.setProperty(
				"/selectedSlaughterBatchAsset",
				_.findWhere(oModel.getProperty("/slaughterBatchAssetCollection/items"),
					{
						ID: selectedSlaughterBatchAssetID
					},
				this));
		},

		addNewSlaughterBatchAsset:function(oComponent, oModel){

			if ( !( oComponent.getModel("TestSwitch").getProperty("/testMode") ) ){
				restBuilder.execute(
					oModel.getProperty("/chaincode"),
					"addNew",
					formatterSlaughterBatchAssets.mapSlaughterBatchAssetToChaincode(oModel, true)
				);
			}  else {
				localStoreSlaughterBatchAssets.put(formatterSlaughterBatchAssets.mapSlaughterBatchAssetToLocalStorage(oModel, true));
			}
			this.loadAllSlaughterBatchAssets(oComponent, oModel);
			return oModel.getProperty("/newSlaughterBatchAsset/ID");
		},

		updateSlaughterBatchAsset: function (oComponent, oModel) {

			if (!(oComponent.getModel("TestSwitch").getProperty("/testMode"))) {
				restBuilder.execute(
					oModel.getProperty("/chaincode"),
					"update",
					formatterSlaughterBatchAssets.mapSlaughterBatchAssetToChaincode(oModel, false)
				);
			} else {
				localStoreSlaughterBatchAssets.patch(formatterSlaughterBatchAssets.mapSlaughterBatchAssetToLocalStorage(oModel, false));
			}
			this.loadAllSlaughterBatchAssets(oComponent, oModel);
		},

		removeSlaughterBatchAsset : function(oComponent, oModel, slaughterBatchAssetID){

			if ( !( oComponent.getModel("TestSwitch").getProperty("/testMode") ) ) {
				restBuilder.execute(
					oModel.getProperty("/chaincode"),
					"remove",
					{ID:slaughterBatchAssetID}
				);
			} else {
				localStoreSlaughterBatchAssets.remove(slaughterBatchAssetID);
			}
			this.loadAllSlaughterBatchAssets(oComponent, oModel);
			return true;
		},

		removeAllSlaughterBatchAssets : function(oComponent, oModel){

			if (!(oComponent.getModel("TestSwitch").getProperty("/testMode"))) {
				restBuilder.execute(
					oModel.getProperty("/chaincode"),
					"removeAll",
					[]
				);
			} else {
				localStoreSlaughterBatchAssets.removeAll();
			}
			this.loadAllSlaughterBatchAssets(oComponent, oModel);
			oModel.setProperty("/selectedSlaughterBatchAsset",{});
			return true;
		}
	};
});
