sap.ui.define(function () {
	"use strict";

	return {

		mapLabSampleAssetToModel: function (responseData) {
			return {
				ID: responseData.ID,
				ObjectType: responseData.docType,
				Status: responseData.status,
				CreationDate: responseData.creationDate,
				LabID: responseData.labID,
				SlaughterBatchID: responseData.slaughterBatchID,
				Type: responseData.type,
				CollectionDate: responseData.collectionDate,
				SubmissionDate: responseData.submissionDate
			};
		},

		mapLabSampleAssetsToModel: function (responseData) {

			var items = [];
			if (responseData) {
				for (var i = 0; i < responseData.length; i++) {
					items.push(this.mapLabSampleAssetToModel(responseData[i]));
				}
			}
			return items;
		},

		mapLabSampleAssetToChaincode: function (oModel, newLabSampleAsset) {

			if (newLabSampleAsset === true) {
				return {
					"ID": this.getNewLabSampleAssetID(oModel),
					"docType": "Asset.LabSampleAsset",
					"status": oModel.getProperty("/newLabSampleAsset/Status"),
					"creationDate": oModel.getProperty("/newLabSampleAsset/CreationDate"),
					"labID": oModel.getProperty("/newLabSampleAsset/LabID"),
					"slaughterBatchID": oModel.getProperty("/newLabSampleAsset/SlaughterBatchID"),
					"type": oModel.getProperty("/newLabSampleAsset/Type"),
					"collectionDate": oModel.getProperty("/newLabSampleAsset/CollectionDate"),
					"submissionDate": oModel.getProperty("/newLabSampleAsset/SubmissionDate")
				};
			} else {
				return {
					"ID": oModel.getProperty("/selectedLabSampleAsset/ID"),
					"docType": "Asset.LabSampleAsset",
					"status": oModel.getProperty("/selectedLabSampleAsset/Status"),
					"creationDate": oModel.getProperty("/selectedLabSampleAsset/CreationDate"),
					"labID": oModel.getProperty("/selectedLabSampleAsset/LabID"),
					"slaughterBatchID": oModel.getProperty("/selectedLabSampleAsset/SlaughterBatchID"),
					"type": oModel.getProperty("/selectedLabSampleAsset/Type"),
					"collectionDate": oModel.getProperty("/selectedLabSampleAsset/CollectionDate"),
					"submissionDate": oModel.getProperty("/selectedLabSampleAsset/SubmissionDate")
				};
			}
		},

		mapLabSampleAssetToLocalStorage: function (oModel, newLabSampleAsset) {

			return this.mapLabSampleAssetToChaincode(oModel, newLabSampleAsset);
		},

		getNewLabSampleAssetID: function (oModel) {

			if (typeof oModel.getProperty("/newLabSampleAsset/ID") === "undefined" ||
				oModel.getProperty("/newLabSampleAsset/ID") === ""
			) {
				var iD = "ASTLS";
				var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
				for (var i = 0; i < 8; i++) {
					iD += possible.charAt(Math.floor(Math.random() * possible.length));
				}
			} else {
				iD = oModel.getProperty("/newLabSampleAsset/ID");
			}
			oModel.setProperty("/newLabSampleAsset/ID", iD);
			return iD;
		}
	};
});