sap.ui.define(function () {
	"use strict";

	return {

		mapSlaughterBatchAssetToModel: function (responseData) {
			return {
				ID: responseData.ID,
				ObjectType: responseData.docType,
				Status: responseData.status,
				CreationDate: responseData.creationDate,
				HatcheryFarmID: responseData.hatcheryFarmID,
				GrowerFarmID: responseData.growerFarmID,
				HenHouseID: responseData.henHouseID,
				ChickenLotID: responseData.chickenLotID,
				HusbandryLineageID: responseData.husbandryLineageID,
				FeedMixID: responseData.feedMixID,
				FeedFormulaID: responseData.feedFormulaID,
				FeedStopDate: responseData.feedStopDate,
				SlaughterDate: responseData.slaughterDate
			};
		},

		mapSlaughterBatchAssetsToModel: function (responseData) {

			var items = [];
			if (responseData) {
				for (var i = 0; i < responseData.length; i++) {
					items.push(this.mapSlaughterBatchAssetToModel(responseData[i]));
				}
			}
			return items;
		},

		mapSlaughterBatchAssetToChaincode: function (oModel, newSlaughterBatchAsset) {

			if (newSlaughterBatchAsset === true) {
				return {
					"ID": this.getNewSlaughterBatchAssetID(oModel),
					"docType": "Asset.SlaughterBatchAsset",
					"status": oModel.getProperty("/newSlaughterBatchAsset/Status"),
					"creationDate": oModel.getProperty("/newSlaughterBatchAsset/CreationDate"),
					"hatcheryFarmID": oModel.getProperty("/newSlaughterBatchAsset/HatcheryFarmID"),
					"growerFarmID": oModel.getProperty("/newSlaughterBatchAsset/GrowerFarmID"),
					"henHouseID": oModel.getProperty("/newSlaughterBatchAsset/HenHouseID"),
					"chickenLotID": oModel.getProperty("/newSlaughterBatchAsset/ChickenLotID"),
					"husbandryLineageID": oModel.getProperty("/newSlaughterBatchAsset/HusbandryLineageID"),
					"feedMixID": oModel.getProperty("/newSlaughterBatchAsset/FeedMixID"),
					"feedFormulaID": oModel.getProperty("/newSlaughterBatchAsset/FeedFormulaID"),
					"feedStopDate": oModel.getProperty("/newSlaughterBatchAsset/FeedStopDate"),
					"slaughterDate": oModel.getProperty("/newSlaughterBatchAsset/SlaughterDate")
				};
			} else {
				return {
					"ID": oModel.getProperty("/selectedSlaughterBatchAsset/ID"),
					"docType": "Asset.SlaughterBatchAsset",
					"status": oModel.getProperty("/selectedSlaughterBatchAsset/Status"),
					"creationDate": oModel.getProperty("/selectedSlaughterBatchAsset/CreationDate"),
					"hatcheryFarmID": oModel.getProperty("/selectedSlaughterBatchAsset/HatcheryFarmID"),
					"growerFarmID": oModel.getProperty("/selectedSlaughterBatchAsset/GrowerFarmID"),
					"henHouseID": oModel.getProperty("/selectedSlaughterBatchAsset/HenHouseID"),
					"chickenLotID": oModel.getProperty("/selectedSlaughterBatchAsset/ChickenLotID"),
					"husbandryLineageID": oModel.getProperty("/selectedSlaughterBatchAsset/HusbandryLineageID"),
					"feedMixID": oModel.getProperty("/selectedSlaughterBatchAsset/FeedMixID"),
					"feedFormulaID": oModel.getProperty("/selectedSlaughterBatchAsset/FeedFormulaID"),
					"feedStopDate": oModel.getProperty("/selectedSlaughterBatchAsset/FeedStopDate"),
					"slaughterDate": oModel.getProperty("/selectedSlaughterBatchAsset/SlaughterDate")
				};
			}
		},

		mapSlaughterBatchAssetToLocalStorage: function (oModel, newSlaughterBatchAsset) {

			return this.mapSlaughterBatchAssetToChaincode(oModel, newSlaughterBatchAsset);
		},

		getNewSlaughterBatchAssetID: function (oModel) {

			if (typeof oModel.getProperty("/newSlaughterBatchAsset/ID") === "undefined" ||
				oModel.getProperty("/newSlaughterBatchAsset/ID") === ""
			) {
				var iD = "ASTSB";
				var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
				for (var i = 0; i < 8; i++) {
					iD += possible.charAt(Math.floor(Math.random() * possible.length));
				}
			} else {
				iD = oModel.getProperty("/newSlaughterBatchAsset/ID");
			}
			oModel.setProperty("/newSlaughterBatchAsset/ID", iD);
			return iD;
		}
	};
});