sap.ui.define([
	"chicken-netSetup/util/restBuilder",
	"chicken-netSetup/util/formatterLabResultAssets",
	"chicken-netSetup/util/localStoreLabResultAssets"
], function(
		restBuilder,
		formatterLabResultAssets,
		localStoreLabResultAssets
	) {
	"use strict";

	return {

		loadAllLabResultAssets:function(oComponent, oModel) {

			if ( !( oComponent.getModel("TestSwitch").getProperty("/testMode") ) ){
				restBuilder.execute(
					oModel.getProperty("/chaincode"),
					"readAll",
					[],
					function(responseData){
						oModel.setProperty(
							"/labResultAssetCollection/items",
							formatterLabResultAssets.mapLabResultAssetsToModel(responseData)
						);
					});
			} else {
				oModel.setProperty(
					"/labResultAssetCollection/items",
					formatterLabResultAssets.mapLabResultAssetsToModel(localStoreLabResultAssets.getLabResultAssetData())
				);
			}
		},

		loadLabResultAsset:function(oModel, selectedLabResultAssetID){

			oModel.setProperty(
				"/selectedLabResultAsset",
				_.findWhere(oModel.getProperty("/labResultAssetCollection/items"),
					{
						ID: selectedLabResultAssetID
					},
				this));
		},

		addNewLabResultAsset:function(oComponent, oModel){

			if ( !( oComponent.getModel("TestSwitch").getProperty("/testMode") ) ){
				restBuilder.execute(
					oModel.getProperty("/chaincode"),
					"addNew",
					formatterLabResultAssets.mapLabResultAssetToChaincode(oModel, true)
				);
			}  else {
				localStoreLabResultAssets.put(formatterLabResultAssets.mapLabResultAssetToLocalStorage(oModel, true));
			}
			this.loadAllLabResultAssets(oComponent, oModel);
			return oModel.getProperty("/newLabResultAsset/ID");
		},

		updateLabResultAsset: function (oComponent, oModel) {

			if (!(oComponent.getModel("TestSwitch").getProperty("/testMode"))) {
				restBuilder.execute(
					oModel.getProperty("/chaincode"),
					"update",
					formatterLabResultAssets.mapLabResultAssetToChaincode(oModel, false)
				);
			} else {
				localStoreLabResultAssets.patch(formatterLabResultAssets.mapLabResultAssetToLocalStorage(oModel, false));
			}
			this.loadAllLabResultAssets(oComponent, oModel);
		},

		removeLabResultAsset : function(oComponent, oModel, labResultAssetID){

			if ( !( oComponent.getModel("TestSwitch").getProperty("/testMode") ) ) {
				restBuilder.execute(
					oModel.getProperty("/chaincode"),
					"remove",
					{ID:labResultAssetID}
				);
			} else {
				localStoreLabResultAssets.remove(labResultAssetID);
			}
			this.loadAllLabResultAssets(oComponent, oModel);
			return true;
		},

		removeAllLabResultAssets : function(oComponent, oModel){

			if (!(oComponent.getModel("TestSwitch").getProperty("/testMode"))) {
				restBuilder.execute(
					oModel.getProperty("/chaincode"),
					"removeAll",
					[]
				);
			} else {
				localStoreLabResultAssets.removeAll();
			}
			this.loadAllLabResultAssets(oComponent, oModel);
			oModel.setProperty("/selectedLabResultAsset",{});
			return true;
		}
	};
});
