sap.ui.define([

	"chicken-netSetup/util/restBuilder",
	"chicken-netSetup/util/formatterBatchAssets",
	"chicken-netSetup/util/localStoreBatchAssets"

], function (
	restBuilder,
	formatterBatchAssets,
	localStoreBatchAssets
) {
	"use strict";

	return {

		loadAllBatchAssets: function (oComponent, oModel) {

			if (!(oComponent.getModel("TestSwitch").getProperty("/testMode"))) {
				restBuilder.execute(
					oModel.getProperty("/chaincode"),
					"readAll", [],
					function (responseData) {
						oModel.setProperty(
							"/batchAssetCollection/items",
							formatterBatchAssets.mapBatchAssetsToModel(responseData)
						);
					});
			} else {
				oModel.setProperty(
					"/batchAssetCollection/items",
					formatterBatchAssets.mapBatchAssetsToModel(localStoreBatchAssets.getBatchAssetData())
				);
			}
		},

		loadBatchAsset: function (oModel, selectedBatchAssetID) {

			oModel.setProperty(
				"/selectedBatchAsset",
				_.findWhere(oModel.getProperty("/batchAssetCollection/items"), {
						ID: selectedBatchAssetID
					},
					this));
		},

		addNewBatchAsset: function (oComponent, oModel) {

			if (!(oComponent.getModel("TestSwitch").getProperty("/testMode"))) {
				restBuilder.execute(
					oModel.getProperty("/chaincode"),
					"addNew",
					formatterBatchAssets.mapBatchAssetToChaincode(oModel, true)
				);
			} else {
				localStoreBatchAssets.put(formatterBatchAssets.mapBatchAssetToLocalStorage(oModel, true));
			}
			this.loadAllBatchAssets(oComponent, oModel);
			return oModel.getProperty("/newBatchAsset/ID");
		},

		updateBatchAsset: function (oComponent, oModel) {

			if (!(oComponent.getModel("TestSwitch").getProperty("/testMode"))) {
				restBuilder.execute(
					oModel.getProperty("/chaincode"),
					"update",
					formatterBatchAssets.mapBatchAssetToChaincode(oModel, false)
				);
			} else {
				localStoreBatchAssets.patch(formatterBatchAssets.mapBatchAssetToLocalStorage(oModel, false));
			}
			this.loadAllBatchAssets(oComponent, oModel);
		},

		removeBatchAsset: function (oComponent, oModel, batchAssetID) {

			if (!(oComponent.getModel("TestSwitch").getProperty("/testMode"))) {
				restBuilder.execute(
					oModel.getProperty("/chaincode"),
					"remove", {
						ID: batchAssetID
					}
				);
			} else {
				localStoreBatchAssets.remove(batchAssetID);
			}
			this.loadAllBatchAssets(oComponent, oModel);
			return true;
		},

		removeAllBatchAssets: function (oComponent, oModel) {

			if (!(oComponent.getModel("TestSwitch").getProperty("/testMode"))) {
				restBuilder.execute(
					oModel.getProperty("/chaincode"),
					"removeAll", []
				);
			} else {
				localStoreBatchAssets.removeAll();
			}
			this.loadAllBatchAssets(oComponent, oModel);
			oModel.setProperty("/selectedBatchAsset", {});
			return true;
		}
	};
});