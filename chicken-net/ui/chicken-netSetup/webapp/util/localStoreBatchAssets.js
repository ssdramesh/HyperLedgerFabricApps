sap.ui.define(function () {
	"use strict";

	jQuery.sap.require("jquery.sap.storage");
	var oStorage = jQuery.sap.storage(jQuery.sap.storage.Type.local);
	var batchAssetsDataID = "batchAssets";

	return {

		init: function () {

			oStorage.put(batchAssetsDataID, []);
			oStorage.put(
				batchAssetsDataID, [{
					"ID": "ASTBA100001",
					"docType": "Asset.BatchAsset",
					"status": "In Distribution",
					"creationDate": "09/21/2018",
					"feedMix": {
						"feedMixID": "Soybean GMO",
						"supplierID": "AVAPLUS",
						"supplierLandDeedID": "066091/999-4521"
					},
					"slaughterBatch": {
						"slaughterBatchID": "SB-2018-09-20-654",
						"feedFormulaID": "SBGMOF-001"
					},
					"processData": {
						"salesOrderID": "SO-2018-09-786234",
						"deliveryOrderID": "DO-2018-09-876212",
						"goodsIssueID": "GI-2018-09-765329",
						"goodsIssueBatchID": "GIB-2018-09-76374856"
					},
					"quality": {
						"labID": "OMIC-Bangkok",
						"labSampleID": "ASTLS100001",
						"certificateID": "DMSC18-9657681",
						"protienPercent": "21.2",
						"fatPercent": "2.6",
						"disease": "none",
						"contaminationPercent": "0.003",
						"antibioticPercent": "13"
					}
				}, {
					"ID": "ASTBA100002",
					"docType": "Asset.BatchAsset",
					"status": "Ready for Distribution",
					"creationDate": "09/10/2018",
					"feedMix": {
						"feedMixID": "FishDry",
						"supplierID": "TANHUAT",
						"supplierLandDeedID": "099657/5647-6541"
					},
					"slaughterBatch": {
						"slaughterBatchID": "SB-2018-09-07-867",
						"feedFormulaID": "FF-002"
					},
					"processData": {
						"salesOrderID": "SO-2018-09-7986723",
						"deliveryOrderID": "DO-2018-09-87891",
						"goodsIssueID": "GI-2018-09-671542",
						"goodsIssueBatchID": "GIB-2018-09-13656"
					},
					"quality": {
						"labID": "OMIC-Bangkok",
						"labSampleID": "ASTLS100002",
						"certificateID": "DMSC18-9657681",
						"protienPercent": "19.8",
						"fatPercent": "3.2",
						"disease": "none",
						"contaminationPercent": "0.05",
						"antibioticPercent": "21"
					}
				}]
			);
		},

		getBatchAssetDataID: function () {

			return batchAssetsDataID;
		},

		getBatchAssetData: function () {

			return oStorage.get("batchAssets");
		},

		put: function (newBatchAsset) {

			var batchAssetData = this.getBatchAssetData();
			batchAssetData.push(newBatchAsset);
			oStorage.put(batchAssetsDataID, batchAssetData);
		},

		patch: function (batchAsset) {

			this.remove(batchAsset.ID);
			this.put(batchAsset);
		},

		remove: function (id) {

			var batchAssetData = this.getBatchAssetData();
			batchAssetData = _.without(batchAssetData, _.findWhere(batchAssetData, {
				ID: id
			}));
			oStorage.put(batchAssetsDataID, batchAssetData);
		},

		removeAll: function () {

			oStorage.put(batchAssetsDataID, []);
		},

		clearBatchAssetData: function () {

			oStorage.put(batchAssetsDataID, []);
		}
	};
});