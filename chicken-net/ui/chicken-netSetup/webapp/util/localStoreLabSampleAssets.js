sap.ui.define(function () {
	"use strict";

	jQuery.sap.require("jquery.sap.storage");
	var oStorage = jQuery.sap.storage(jQuery.sap.storage.Type.local);
	var labSampleAssetsDataID = "labSampleAssets";

	return {

		init: function () {

			oStorage.put(labSampleAssetsDataID, []);
			oStorage.put(
				labSampleAssetsDataID, [{
					"ID": "ASTLS100001",
					"docType": "Asset.LabSampleAsset",
					"status": "Result Submitted",
					"creationDate": "09/20/2018",
					"labID": "OMIC-Bangkok",
					"slaughterBatchID": "SB-2018-09-20-654",
					"type": "BLOOD",
					"collectionDate": "09/20/2018",
					"submissionDate": "09/19/2018"
				}, {
					"ID": "ASTLS100002",
					"docType": "Asset.LabSampleAsset",
					"status": "Result Approved",
					"creationDate": "09/08/2018",
					"labID": "OMIC-Bangkok",
					"slaughterBatchID": "SB-2018-09-07-867",
					"type": "SWAB",
					"collectionDate": "09/07/2018",
					"submissionDate": "09/07/2018"
				}]
			);
		},

		getLabSampleAssetDataID: function () {

			return labSampleAssetsDataID;
		},

		getLabSampleAssetData: function () {

			return oStorage.get("labSampleAssets");
		},

		put: function (newLabSampleAsset) {

			var labSampleAssetData = this.getLabSampleAssetData();
			labSampleAssetData.push(newLabSampleAsset);
			oStorage.put(labSampleAssetsDataID, labSampleAssetData);
		},

		patch: function (labSampleAsset) {

			this.remove(labSampleAsset.ID);
			this.put(labSampleAsset);
		},

		remove: function (id) {

			var labSampleAssetData = this.getLabSampleAssetData();
			labSampleAssetData = _.without(labSampleAssetData, _.findWhere(labSampleAssetData, {
				ID: id
			}));
			oStorage.put(labSampleAssetsDataID, labSampleAssetData);
		},

		removeAll: function () {

			oStorage.put(labSampleAssetsDataID, []);
		},

		clearLabSampleAssetData: function () {

			oStorage.put(labSampleAssetsDataID, []);
		}
	};
});