sap.ui.define(function () {
	"use strict";

	return {

		mapLabResultAssetToModel: function (responseData) {
			return {
				ID: responseData.ID,
				ObjectType: responseData.docType,
				Status: responseData.status,
				CreationDate: responseData.creationDate,
				LabID: responseData.labID,
				LabSampleID: responseData.labSampleID,
				CertificateID: responseData.certificateID,
				Disease: responseData.bioTestResult.disease,
				AntibioticPercent: responseData.bioTestResult.antibioticPercent,
				ContaminationPercent: responseData.bioTestResult.contaminationPercent,
				NIRSProteinPercent: responseData.quality.nIRSProteinPercent,
				FatPercent: responseData.quality.fatPercent
			};
		},

		mapLabResultAssetsToModel: function (responseData) {

			var items = [];
			if (responseData) {
				for (var i = 0; i < responseData.length; i++) {
					items.push(this.mapLabResultAssetToModel(responseData[i]));
				}
			}
			return items;
		},

		mapLabResultAssetToChaincode: function (oModel, newLabResultAsset) {

			if (newLabResultAsset === true) {
				return {
					"ID": this.getNewLabResultAssetID(oModel),
					"docType": "Asset.LabResultAsset",
					"status": oModel.getProperty("/newLabResultAsset/Status"),
					"creationDate": oModel.getProperty("/newLabResultAsset/CreationDate"),
					"labID": oModel.getProperty("/newLabResultAsset/LabID"),
					"labSampleID": oModel.getProperty("/newLabResultAsset/LabSampleID"),
					"certificateID": oModel.getProperty("/newLabResultAsset/CertificateID"),
					"bioTestResult": {
						"disease": oModel.getProperty("/newLabResultAsset/Disease"),
						"contaminationPercent": oModel.getProperty("/newLabResultAsset/ContaminationPercent"),
						"antibioticPercent": oModel.getProperty("/newLabResultAsset/AntibioticPercent")
					},
					"quality": {
						"nIRSProteinPercent": oModel.getProperty("/newLabResultAsset/NIRSProteinPercent"),
						"fatPercent": oModel.getProperty("/newLabResultAsset/FatPercent")
					}
				};
			} else {
				return {
					"ID": oModel.getProperty("/selectedLabResultAsset/ID"),
					"docType": "Asset.LabResultAsset",
					"status": oModel.getProperty("/selectedLabResultAsset/Status"),
					"creationDate": oModel.getProperty("/selectedLabResultAsset/CreationDate"),
					"labID": oModel.getProperty("/selectedLabResultAsset/LabID"),
					"labSampleID": oModel.getProperty("/selectedLabResultAsset/LabSampleID"),
					"certificateID": oModel.getProperty("/selectedLabResultAsset/CertificateID"),
					"bioTestResult": {
						"disease": oModel.getProperty("/selectedLabResultAsset/Disease"),
						"contaminationPercent": oModel.getProperty("/selectedLabResultAsset/ContaminationPercent"),
						"antibioticPercent": oModel.getProperty("/selectedLabResultAsset/AntibioticPercent")
					},
					"quality": {
						"nIRSProteinPercent": oModel.getProperty("/selectedLabResultAsset/NIRSProteinPercent"),
						"fatPercent": oModel.getProperty("/selectedLabResultAsset/FatPercent")
					}
				};
			}
		},

		mapLabResultAssetToLocalStorage: function (oModel, newLabResultAsset) {

			return this.mapLabResultAssetToChaincode(oModel, newLabResultAsset);
		},

		getNewLabResultAssetID: function (oModel) {

			if (typeof oModel.getProperty("/newLabResultAsset/ID") === "undefined" ||
				oModel.getProperty("/newLabResultAsset/ID") === ""
			) {
				var iD = "ASTLR";
				var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
				for (var i = 0; i < 8; i++) {
					iD += possible.charAt(Math.floor(Math.random() * possible.length));
				}
			} else {
				iD = oModel.getProperty("/newLabResultAsset/ID");
			}
			oModel.setProperty("/newLabResultAsset/ID", iD);
			return iD;
		}
	};
});