sap.ui.define([
	"chicken-netSetup/util/restBuilder",
	"chicken-netSetup/util/formatterLabSampleAssets",
	"chicken-netSetup/util/localStoreLabSampleAssets"
], function(
		restBuilder,
		formatterLabSampleAssets,
		localStoreLabSampleAssets
	) {
	"use strict";

	return {

		loadAllLabSampleAssets:function(oComponent, oModel) {

			if ( !( oComponent.getModel("TestSwitch").getProperty("/testMode") ) ){
				restBuilder.execute(
					oModel.getProperty("/chaincode"),
					"readAll",
					[],
					function(responseData){
						oModel.setProperty(
							"/labSampleAssetCollection/items",
							formatterLabSampleAssets.mapLabSampleAssetsToModel(responseData)
						);
					});
			} else {
				oModel.setProperty(
					"/labSampleAssetCollection/items",
					formatterLabSampleAssets.mapLabSampleAssetsToModel(localStoreLabSampleAssets.getLabSampleAssetData())
				);
			}
		},

		loadLabSampleAsset:function(oModel, selectedLabSampleAssetID){

			oModel.setProperty(
				"/selectedLabSampleAsset",
				_.findWhere(oModel.getProperty("/labSampleAssetCollection/items"),
					{
						ID: selectedLabSampleAssetID
					},
				this));
		},

		addNewLabSampleAsset:function(oComponent, oModel){

			if ( !( oComponent.getModel("TestSwitch").getProperty("/testMode") ) ){
				restBuilder.execute(
					oModel.getProperty("/chaincode"),
					"addNew",
					formatterLabSampleAssets.mapLabSampleAssetToChaincode(oModel, true)
				);
			}  else {
				localStoreLabSampleAssets.put(formatterLabSampleAssets.mapLabSampleAssetToLocalStorage(oModel, true));
			}
			this.loadAllLabSampleAssets(oComponent, oModel);
			return oModel.getProperty("/newLabSampleAsset/ID");
		},

		updateLabSampleAsset: function (oComponent, oModel) {

			if (!(oComponent.getModel("TestSwitch").getProperty("/testMode"))) {
				restBuilder.execute(
					oModel.getProperty("/chaincode"),
					"update",
					formatterLabSampleAssets.mapLabSampleAssetToChaincode(oModel, false)
				);
			} else {
				localStoreLabSampleAssets.patch(formatterLabSampleAssets.mapLabSampleAssetToLocalStorage(oModel, false));
			}
			this.loadAllLabSampleAssets(oComponent, oModel);
		},

		removeLabSampleAsset : function(oComponent, oModel, labSampleAssetID){

			if ( !( oComponent.getModel("TestSwitch").getProperty("/testMode") ) ) {
				restBuilder.execute(
					oModel.getProperty("/chaincode"),
					"remove",
					{ID:labSampleAssetID}
				);
			} else {
				localStoreLabSampleAssets.remove(labSampleAssetID);
			}
			this.loadAllLabSampleAssets(oComponent, oModel);
			return true;
		},

		removeAllLabSampleAssets : function(oComponent, oModel){

			if (!(oComponent.getModel("TestSwitch").getProperty("/testMode"))) {
				restBuilder.execute(
					oModel.getProperty("/chaincode"),
					"removeAll",
					[]
				);
			} else {
				localStoreLabSampleAssets.removeAll();
			}
			this.loadAllLabSampleAssets(oComponent, oModel);
			oModel.setProperty("/selectedLabSampleAsset",{});
			return true;
		}
	};
});
