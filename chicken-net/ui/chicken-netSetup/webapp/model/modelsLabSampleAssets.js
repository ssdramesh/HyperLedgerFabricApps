sap.ui.define([
	"sap/ui/model/json/JSONModel"
], function(JSONModel) {
	"use strict";

	return {

		createLabSampleAssetsModel: function(){

			var oModel = new JSONModel();
			oModel.setDefaultBindingMode("TwoWay");
			oModel.setData({
				chaincode:{
					serviceUrl:"",
					serviceKey:{},
					labSampleAsset:{}
				},
				labSampleAssetCollection	: {
					cols:[
						{name:"ID"},
						{name:"ObjectType"},
						{name:"Status"},
						{name:"CreationDate"},
            {name:"LabID"},
            {name:"SlaughterBatchID"},
            {name:"Type"},
            {name:"CollectionDate"},
            {name:"SubmissionDate"}
					],
					items:[]
				},
				selectedLabSampleAsset:{},
				newLabSampleAsset:{
					ID:"",
					ObjectType:"",
					Status:"",
					CreationDate:"",
          LabID:"",
          SlaughterBatchID:"",
          Type:"",
          CollectionDate:"",
          SubmissionDate:""
				},
				selectedLabSampleAssetID	: "",
				searchLabSampleAssetID : ""
			});
			return oModel;
		}
	};
});
