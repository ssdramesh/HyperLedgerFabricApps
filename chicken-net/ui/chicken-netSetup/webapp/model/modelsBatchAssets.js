sap.ui.define([
	"sap/ui/model/json/JSONModel"
], function(JSONModel) {
	"use strict";

	return {

		createBatchAssetsModel: function(){

			var oModel = new JSONModel();
			oModel.setDefaultBindingMode("TwoWay");
			oModel.setData({
				chaincode:{
					serviceUrl:"",
					serviceKey:{},
					batchAsset:{}
				},
				batchAssetCollection	: {
					cols:[
						{name:"ID"},
						{name:"ObjectType"},
						{name:"Status"},
						{name:"CreationDate"},
            {name:"FeedMixID"},
						{name:"SupplierID"},
						{name:"SupplierLandDeedID"},
            {name:"SlaughterBatchID"},
						{name:"FeedFormulaID"},
            {name:"SalesOrderID"},
						{name:"DeliveryOrderID"},
						{name:"GoodIssueID"},
						{name:"GoodsIssueBatchID"},
            {name:"LabID"},
						{name:"LabSampleID"},
						{name:"LabCertificateID"},
						{name:"ProtienPercent"},
						{name:"FatPercent"},
						{name:"Disease"},
						{name:"ContaminationPercent"},
						{name:"AntibioticPercent"}
					],
					items:[]
				},
				selectedBatchAsset:{},
				newBatchAsset:{
					ID:"",
					ObjectType:"",
					Status:"",
					CreationDate:"",
          FeedMixID:"",
					SupplierID:"",
					SupplierLandDeedID:"",
          SlaughterBatchID:"",
					FeedFormulaID:"",
          SalesOrderID:"",
					DeliveryOrderID:"",
					GoodsIssueID:"",
					GoodsIssueBatchID:"",
          LabID:"",
					LabSampleID:"",
					LabCertificateID:"",
					ProtienPercent:"",
					FatPercent:"",
					Disease:"",
					ContaminationPercent:"",
					AntibioticPercent:""
				},
				selectedBatchAssetID	: "",
				searchBatchAssetID : ""
			});
			return oModel;
		}
	};
});
