sap.ui.define([
	"sap/ui/model/json/JSONModel"
], function(JSONModel) {
	"use strict";

	return {

		createLabResultAssetsModel: function(){

			var oModel = new JSONModel();
			oModel.setDefaultBindingMode("TwoWay");
			oModel.setData({
				chaincode:{
					serviceUrl:"",
					serviceKey:{},
					labResultAsset:{}
				},
				labResultAssetCollection	: {
					cols:[
						{name:"ID"},
						{name:"ObjectType"},
						{name:"Status"},
						{name:"CreationDate"},
            {name:"LabID"},
            {name:"LabSampleID"},
            {name:"CertificateID"},
            {name:"Disease"},
						{name:"AntibioticPercent"},
						{name:"ContaminationPercent"},
            {name:"NIRSProteinPercent"},
						{name:"FatPercent"}
					],
					items:[]
				},
				selectedLabResultAsset:{},
				newLabResultAsset:{
					ID:"",
					ObjectType:"",
					Status:"",
					CreationDate:"",
          LabID:"",
          LabSampleID:"",
          CertificateID:"",
          Disease:"",
					AntibioticPercent:"",
					ContaminationPercent:"",
          NIRSProteinPercent:"",
					FatPercent:""
				},
				selectedLabResultAssetID	: "",
				searchLabResultAssetID : ""
			});
			return oModel;
		}
	};
});
