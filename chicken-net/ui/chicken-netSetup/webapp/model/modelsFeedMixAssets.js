sap.ui.define([
	"sap/ui/model/json/JSONModel"
], function(JSONModel) {
	"use strict";

	return {

		createFeedMixAssetsModel: function(){

			var oModel = new JSONModel();
			oModel.setDefaultBindingMode("TwoWay");
			oModel.setData({
				chaincode:{
					serviceUrl:"",
					serviceKey:{},
					feedMixAsset:{}
				},
				feedMixAssetCollection	: {
					cols:[
						{name:"ID"},
						{name:"ObjectType"},
						{name:"Status"},
						{name:"CreationDate"},
            {name:"SupplierID"},
            {name:"SupplierLandDeedID"},
            {name:"SupplierInvoiceID"},
						{name:"PurchaserID"},
            {name:"PurchaseOrderID"},
            {name:"GoodsReceiptID"},
            {name:"MaterialID"},
            {name:"TestResultID"},
						{name:"TestResultCertificateID"},
						{name:"TestResultDate"},
            {name:"Humidity"},
            {name:"ContaminationPercent"},
						{name:"InputType"}
					],
					items:[]
				},
				selectedFeedMixAsset:{},
				newFeedMixAsset:{
					ID:"",
					ObjectType:"",
					Status:"",
					CreationDate:"",
          SupplierID:"",
          SupplierLandDeedID:"",
          SupplierInvoiceID:"",
					PurchaserID:"",
          PurchaseOrderID:"",
          GoodsReceiptID:"",
          MaterialID:"",
          TestResultID:"",
          TestResultCertificateID:"",
          TestResultDate:"",
          Humidity:"",
          Contamination:"",
					InputType:""
				},
				selectedFeedMixAssetID	: "",
				searchFeedMixAssetID : ""
			});
			return oModel;
		}
	};
});
