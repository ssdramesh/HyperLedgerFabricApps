sap.ui.define([
	"sap/ui/model/json/JSONModel"
], function(JSONModel) {
	"use strict";

	return {

		createSlaughterBatchAssetsModel: function(){

			var oModel = new JSONModel();
			oModel.setDefaultBindingMode("TwoWay");
			oModel.setData({
				chaincode:{
					serviceUrl:"",
					serviceKey:{},
					slaughterBatchAsset:{}
				},
				slaughterBatchAssetCollection	: {
					cols:[
						{name:"ID"},
						{name:"ObjectType"},
						{name:"Status"},
						{name:"CreationDate"},
            {name:"HatcheryFarmID"},
            {name:"GrowerFarmID"},
            {name:"HenHouseID"},
            {name:"ChickenLotID"},
            {name:"HusbandryLineageID"},
            {name:"FeedMixID"},
            {name:"FeedFormulaID"},
            {name:"FeedStopDate"},
            {name:"SlaughterDate"}
					],
					items:[]
				},
				selectedSlaughterBatchAsset:{},
				newSlaughterBatchAsset:{
					ID:"",
					ObjectType:"",
					Status:"",
					CreationDate:"",
          HatcheryFarmID:"",
          GrowerFarmID:"",
          HenHouseID:"",
          ChickenLotID:"",
          HusbandryLineageID:"",
          FeedMixID:"",
          FeedFormulaID:"",
          FeedStopDate:"",
          SlaughterDate:""
				},
				selectedSlaughterBatchAssetID	: "",
				searchSlaughterBatchAssetID : ""
			});
			return oModel;
		}
	};
});
