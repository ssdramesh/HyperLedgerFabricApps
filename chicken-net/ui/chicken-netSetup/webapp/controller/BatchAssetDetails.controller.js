sap.ui.define([
	"chicken-netSetup/controller/BaseController",
	"chicken-netSetup/util/bizNetAccessBatchAssets"
], function (
	BaseController,
	bizNetAccessBatchAssets
) {
	"use strict";

	return BaseController.extend("chicken-netSetup.controller.BatchAssetDetails", {

		onInit: function () {

			this.getOwnerComponent().getRouter().getRoute("batchAsset").attachPatternMatched(this._onObjectMatched, this);
		},

		_onObjectMatched: function (oEvent) {

			var pId = oEvent.getParameter("arguments").batchAssetId;
			if (pId === "___new") {
				this.getView().byId("__barBatchAsset").setSelectedKey("New");
			} else {
				bizNetAccessBatchAssets.loadBatchAsset(this.getView().getModel("BatchAssets"), oEvent.getParameter("arguments").batchAssetId);
			}
		},

		addNew: function () {

			var oModel = this.getView().getModel("BatchAssets");
			if (oModel.getProperty("/newBatchAsset/Alias") !== "" ||
				oModel.getProperty("/newBatchAsset/Description") !== "") {
				var batchAssetId = bizNetAccessBatchAssets.addNewBatchAsset(this.getOwnerComponent(), oModel);
				this.getView().byId("__barBatchAsset").setSelectedKey("Details");
				bizNetAccessBatchAssets.loadBatchAsset(this.getView().getModel("BatchAssets"), batchAssetId);
			} else {
				sap.m.MessageToast.show("Please enter valid data in all fields", {});
			}
			this._clearNewAsset();
		},

		saveBatchAsset: function (oEvent) {

			var oModel = this.getView().getModel("BatchAssets");
			bizNetAccessBatchAssets.updateBatchAsset(this.getOwnerComponent(), oModel);
			this.onBatchAssetEdit(oEvent);
		},

		removeBatchAsset: function () {

			var oModel = this.getView().getModel("BatchAssets");
			bizNetAccessBatchAssets.removeBatchAsset(this.getOwnerComponent(), oModel, oModel.getProperty("/selectedBatchAsset/ID"));
		},

		editBatchAsset: function (oEvent) {
			var isEditable;
			if (isEditable === true) {
				isEditable = false;
			} else {
				isEditable = true;
			}
		},

		_clearNewAsset: function () {

			this.getView().getModel("BatchAssets").setProperty("/newBatchAsset", {});
		}
	});

});