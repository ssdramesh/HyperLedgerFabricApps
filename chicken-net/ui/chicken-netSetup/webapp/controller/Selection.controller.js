sap.ui.define([
	"chicken-netSetup/controller/BaseController",
	"chicken-netSetup/util/messageProvider",
	"chicken-netSetup/util/bizNetAccessFeedMixAssets",
	"chicken-netSetup/model/modelsFeedMixAssets",
	"chicken-netSetup/util/bizNetAccessLabSampleAssets",
	"chicken-netSetup/model/modelsLabSampleAssets",
	"chicken-netSetup/util/bizNetAccessLabResultAssets",
	"chicken-netSetup/model/modelsLabResultAssets",
	"chicken-netSetup/util/bizNetAccessSlaughterBatchAssets",
	"chicken-netSetup/model/modelsSlaughterBatchAssets",
	"chicken-netSetup/util/bizNetAccessBatchAssets",
	"chicken-netSetup/model/modelsBatchAssets",
	"chicken-netSetup/model/modelsBase"
], function (
	BaseController,
	messageProvider,
	bizNetAccessFeedMixAssets,
	modelsFeedMixAssets,
	bizNetAccessLabSampleAssets,
	modelsLabSampleAssets,
	bizNetAccessLabResultAssets,
	modelsLabResultAssets,
	bizNetAccessSlaughterBatchAssets,
	modelsSlaughterBatchAssets,
	bizNetAccessBatchAssets,
	modelsBatchAssets,
	modelsBase
) {
	"use strict";

	return BaseController.extend("chicken-netSetup.controller.Selection", {

		onInit: function () {
			if (typeof this.getOwnerComponent().getModel("Messages") === "undefined") {
				this.getOwnerComponent().setModel(modelsBase.createMessagesModel(), "Messages");
			}
			messageProvider.init(this.getOwnerComponent().getModel("Messages"));
		},

		onSelect: function (oEvent) {
			switch (oEvent.getSource().getText()) {

			case "Feed Mixes":
				this.getView().getModel("Selection").setProperty("/entityName", "feedMixAsset");
				this.getOwnerComponent().setModel(modelsFeedMixAssets.createFeedMixAssetsModel(), "FeedMixAssets");
				this.loadMetaData("feedMixAsset", this.getModel("FeedMixAssets"));
				bizNetAccessFeedMixAssets.loadAllFeedMixAssets(this.getOwnerComponent(), this.getView().getModel("FeedMixAssets"));
				this.getOwnerComponent().getRouter().navTo("feedMixAssets", {});
				break;

			case "Lab Samples":
				this.getView().getModel("Selection").setProperty("/entityName", "labSampleAsset");
				this.getOwnerComponent().setModel(modelsLabSampleAssets.createLabSampleAssetsModel(), "LabSampleAssets");
				this.loadMetaData("labSampleAsset", this.getModel("LabSampleAssets"));
				bizNetAccessLabSampleAssets.loadAllLabSampleAssets(this.getOwnerComponent(), this.getView().getModel("LabSampleAssets"));
				this.getOwnerComponent().getRouter().navTo("labSampleAssets", {});
				break;

			case "Lab Results":
				this.getView().getModel("Selection").setProperty("/entityName", "labResultAsset");
				this.getOwnerComponent().setModel(modelsLabResultAssets.createLabResultAssetsModel(), "LabResultAssets");
				this.loadMetaData("labResultAsset", this.getModel("LabResultAssets"));
				bizNetAccessLabResultAssets.loadAllLabResultAssets(this.getOwnerComponent(), this.getView().getModel("LabResultAssets"));
				this.getOwnerComponent().getRouter().navTo("labResultAssets", {});
				break;

			case "Slaughter Batches":
				this.getView().getModel("Selection").setProperty("/entityName", "slaughterBatchAsset");
				this.getOwnerComponent().setModel(modelsSlaughterBatchAssets.createSlaughterBatchAssetsModel(), "SlaughterBatchAssets");
				this.loadMetaData("slaughterBatchAsset", this.getModel("SlaughterBatchAssets"));
				bizNetAccessSlaughterBatchAssets.loadAllSlaughterBatchAssets(this.getOwnerComponent(), this.getView().getModel(
					"SlaughterBatchAssets"));
				this.getOwnerComponent().getRouter().navTo("slaughterBatchAssets", {});
				break;

			case "Batches":
				this.getView().getModel("Selection").setProperty("/entityName", "batchAsset");
				this.getOwnerComponent().setModel(modelsBatchAssets.createBatchAssetsModel(), "BatchAssets");
				this.loadMetaData("batchAsset", this.getModel("BatchAssets"));
				bizNetAccessBatchAssets.loadAllBatchAssets(this.getOwnerComponent(), this.getView().getModel("BatchAssets"));
				this.getOwnerComponent().getRouter().navTo("batchAssets", {});
				break;

			}
		},

		removeAllEntities: function () {

			var msgStripID = this.getView().byId("__stripMessage");
			var location = this.getRunMode().location;

			if (typeof this.getOwnerComponent().getModel("FeedMixAssets") === "undefined") {
				this.getOwnerComponent().setModel(modelsFeedMixAssets.createFeedMixAssetsModel(), "FeedMixAssets");
			}
			bizNetAccessFeedMixAssets.removeAllFeedMixAssets(this.getOwnerComponent(), this.getView().getModel("FeedMixAssets"));
			messageProvider.addMessage("Success", "All FeedMixAssets deleted from sample-network", "No Description", location, 1, "",
				"http://www.sap.com");

			if (typeof this.getOwnerComponent().getModel("LabSampleAssets") === "undefined") {
				this.getOwnerComponent().setModel(modelsLabSampleAssets.createLabSampleAssetsModel(), "LabSampleAssets");
			}
			bizNetAccessLabSampleAssets.removeAllLabSampleAssets(this.getOwnerComponent(), this.getView().getModel("LabSampleAssets"));
			messageProvider.addMessage("Success", "All LabSampleAssets deleted from sample-network", "No Description", location, 1, "",
				"http://www.sap.com");

			if (typeof this.getOwnerComponent().getModel("LabResultAssets") === "undefined") {
				this.getOwnerComponent().setModel(modelsLabResultAssets.createLabResultAssetsModel(), "LabResultAssets");
			}
			bizNetAccessLabResultAssets.removeAllLabResultAssets(this.getOwnerComponent(), this.getView().getModel("LabResultAssets"));
			messageProvider.addMessage("Success", "All LabResultAssets deleted from sample-network", "No Description", location, 1, "",
				"http://www.sap.com");

			if (typeof this.getOwnerComponent().getModel("SlaughterBatchAssets") === "undefined") {
				this.getOwnerComponent().setModel(modelsSlaughterBatchAssets.createSlaughterBatchAssetsModel(), "SlaughterBatchAssets");
			}
			bizNetAccessSlaughterBatchAssets.removeAllSlaughterBatchAssets(this.getOwnerComponent(), this.getView().getModel(
				"SlaughterBatchAssets"));
			messageProvider.addMessage("Success", "All SlaughterBatchAssets deleted from sample-network", "No Description", location, 1, "",
				"http://www.sap.com");

			if (typeof this.getOwnerComponent().getModel("BatchAssets") === "undefined") {
				this.getOwnerComponent().setModel(modelsBatchAssets.createBatchAssetsModel(), "BatchAssets");
			}
			bizNetAccessBatchAssets.removeAllBatchAssets(this.getOwnerComponent(), this.getView().getModel("BatchAssets"));
			messageProvider.addMessage("Success", "All BatchAssets deleted from sample-network", "No Description", location, 1, "",
				"http://www.sap.com");

			this.showMessageStrip(msgStripID, "All Existing chicken-net data deleted from SAP BaaS", "S");
		},

		onToggleBaaS: function () {

			this.onToggleRunMode(this.getView().byId("__stripMessage"));
		},

		onMessagePress: function () {

			this.onShowMessageDialog("chicken-net Maintenance Log");
		}
	});
});