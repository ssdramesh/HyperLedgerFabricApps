sap.ui.define([
	"chicken-netSetup/controller/BaseController",
	"chicken-netSetup/model/modelsBase",
	"chicken-netSetup/util/messageProvider",
	"chicken-netSetup/util/localStoreSlaughterBatchAssets",
	"chicken-netSetup/util/bizNetAccessSlaughterBatchAssets",
	"sap/ui/core/routing/History",
	"sap/ui/model/Filter",
	"sap/ui/model/FilterOperator",
	"sap/ui/Device"
], function(
	
		BaseController,
		modelsBase,
		messageProvider,
		localStoreSlaughterBatchAssets,
		bizNetAccessSlaughterBatchAssets,
		History,
		Filter,
		FilterOperator,
		Device
	) {
	"use strict";

	return BaseController.extend("chicken-netSetup.controller.SlaughterBatchAssetsOverview", {

		onInit : function(){

			var oList = this.byId("slaughterBatchAssetsList");
			this._oList = oList;
			this._oListFilterState = {
				aFilter : [],
				aSearch : []
			};
			localStoreSlaughterBatchAssets.init();
			if ( typeof this.getOwnerComponent().getModel("Messages") === "undefined" ){
				this.getOwnerComponent().setModel(modelsBase.createMessagesModel(), "Messages");
			}
			messageProvider.init(this.getOwnerComponent().getModel("Messages"));
		},

		onSelectionChange : function (oEvent) {

			this._showDetail(oEvent.getParameter("listItem") || oEvent.getSource());
		},

		onAdd : function() {

			this.getRouter().navTo("slaughterBatchAsset", {slaughterBatchAssetId : "___new"});
		},

		onRemoveAll : function(){

			bizNetAccessSlaughterBatchAssets.removeAllSlaughterBatchAssets(this.getOwnerComponent(), this.getOwnerComponent().getModel("SlaughterBatchAssets"));
		},

		onSearch : function(oEvent) {

			var sQuery = oEvent.getParameter("query");
			if (sQuery) {
				this._oListFilterState.aSearch = [new Filter("ID", FilterOperator.Contains, sQuery)];
			} else {
				this._oListFilterState.aSearch = [];
			}
			this._applyFilterSearch();
			var bReplace = !Device.system.phone;
			var oModel = this.getModel("SlaughterBatchAssets");
			oModel.setProperty(
				"/selectedSlaughterBatchAsset",
				_.findWhere(oModel.getProperty("/slaughterBatchAssetCollection/items"),
					{
						ID: oModel.getProperty("/searchSlaughterBatchAssetID")
					},
				this));
			this.getRouter().navTo("slaughterBatchAsset", {
				slaughterBatchAssetId : oModel.getProperty("/selectedSlaughterBatchAsset").ID
			}, bReplace);
		},

		_onToggleBaaS : function(){

			this.getOwnerComponent().getModel("TestSwitch").setProperty("/testMode",!(this.getView().byId("__buttonToggleSlaughterBatchAssets").getPressed()));
			var location = this.getRunMode().location;
			messageProvider.addMessage("Success", "Switching Network for Blockchain Data to: ", "No Description", location, 1, "", "http://www.sap.com");
			if ( this.getRunMode().testMode && localStoreSlaughterBatchAssets.getSlaughterBatchAssetData() === null ){
				localStoreSlaughterBatchAssets.init();
			}
			bizNetAccessSlaughterBatchAssets.loadAllSlaughterBatchAssets(this.getOwnerComponent(), this.getModel("SlaughterBatchAssets"));
		},

		_showDetail : function(oItem) {

			var bReplace = !Device.system.phone;
			var oModel = this.getModel("SlaughterBatchAssets");
			this.getRouter().navTo("slaughterBatchAsset", {
				slaughterBatchAssetId : oModel.getObject(oItem.getBindingContextPath()).ID
			}, bReplace);
		},

		_applyFilterSearch : function () {
			var aFilters = this._oListFilterState.aSearch.concat(this._oListFilterState.aFilter),
				oViewModel = this.getModel("SlaughterBatchAssets");
			this._oList.getBinding("items").filter(aFilters, "Application");
			if (aFilters.length !== 0) {
				oViewModel.setProperty("/noDataText", this.getResourceBundle().getText("_labelNoSlaughterBatchAssetsText"));
			}
		},

		onMessagePress: function() {

			this.onShowMessageDialog("chicken-netSetup Log");
		},

		onNavBack : function() {
			this.getRouter().navTo("home");
		}
	});
});
