sap.ui.define([
	"chicken-netSetup/controller/BaseController",
	"chicken-netSetup/util/bizNetAccessLabResultAssets"
], function (
	BaseController,
	bizNetAccessLabResultAssets
) {
	"use strict";

	return BaseController.extend("chicken-netSetup.controller.LabResultAssetDetails", {

		onInit: function () {

			this.getOwnerComponent().getRouter().getRoute("labResultAsset").attachPatternMatched(this._onObjectMatched, this);
		},

		_onObjectMatched: function (oEvent) {

			var pId = oEvent.getParameter("arguments").labResultAssetId;
			if (pId === "___new") {
				this.getView().byId("__barLabResultAsset").setSelectedKey("New");
			} else {
				bizNetAccessLabResultAssets.loadLabResultAsset(this.getView().getModel("LabResultAssets"), oEvent.getParameter("arguments").labResultAssetId);
			}
		},

		addNew: function () {

			var oModel = this.getView().getModel("LabResultAssets");
			if (oModel.getProperty("/newLabResultAsset/Alias") !== "" ||
				oModel.getProperty("/newLabResultAsset/Description") !== "") {
				var labResultAssetId = bizNetAccessLabResultAssets.addNewLabResultAsset(this.getOwnerComponent(), oModel);
				this.getView().byId("__barLabResultAsset").setSelectedKey("Details");
				bizNetAccessLabResultAssets.loadLabResultAsset(this.getView().getModel("LabResultAssets"), labResultAssetId);
			} else {
				sap.m.MessageToast.show("Please enter valid data in all fields", {});
			}
			this._clearNewAsset();
		},

		saveLabResultAsset: function (oEvent) {

			var oModel = this.getView().getModel("LabResultAssets");
			bizNetAccessLabResultAssets.updateLabResultAsset(this.getOwnerComponent(), oModel);
			this.onLabResultAssetEdit(oEvent);
		},

		removeLabResultAsset: function () {

			var oModel = this.getView().getModel("LabResultAssets");
			bizNetAccessLabResultAssets.removeLabResultAsset(this.getOwnerComponent(), oModel, oModel.getProperty("/selectedLabResultAsset/ID"));
		},

		editLabResultAsset: function (oEvent) {
			var isEditable;
			if (isEditable === true) {
				isEditable = false;
			} else {
				isEditable = true;
			}
		},

		_clearNewAsset: function () {

			this.getView().getModel("LabResultAssets").setProperty("/newLabResultAsset", {});
		}
	});

});