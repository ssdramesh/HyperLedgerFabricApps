sap.ui.define([
	"chicken-netSetup/controller/BaseController",
	"chicken-netSetup/util/bizNetAccessLabSampleAssets"
], function (
	BaseController,
	bizNetAccessLabSampleAssets
) {
	"use strict";

	return BaseController.extend("chicken-netSetup.controller.LabSampleAssetDetails", {

		onInit: function () {

			this.getOwnerComponent().getRouter().getRoute("labSampleAsset").attachPatternMatched(this._onObjectMatched, this);
		},

		_onObjectMatched: function (oEvent) {

			var pId = oEvent.getParameter("arguments").labSampleAssetId;
			if (pId === "___new") {
				this.getView().byId("__barLabSampleAsset").setSelectedKey("New");
			} else {
				bizNetAccessLabSampleAssets.loadLabSampleAsset(this.getView().getModel("LabSampleAssets"), oEvent.getParameter("arguments").labSampleAssetId);
			}
		},

		addNew: function () {

			var oModel = this.getView().getModel("LabSampleAssets");
			if (oModel.getProperty("/newLabSampleAsset/Alias") !== "" ||
				oModel.getProperty("/newLabSampleAsset/Description") !== "") {
				var labSampleAssetId = bizNetAccessLabSampleAssets.addNewLabSampleAsset(this.getOwnerComponent(), oModel);
				this.getView().byId("__barLabSampleAsset").setSelectedKey("Details");
				bizNetAccessLabSampleAssets.loadLabSampleAsset(this.getView().getModel("LabSampleAssets"), labSampleAssetId);
			} else {
				sap.m.MessageToast.show("Please enter valid data in all fields", {});
			}
			this._clearNewAsset();
		},

		saveLabSampleAsset: function (oEvent) {

			var oModel = this.getView().getModel("LabSampleAssets");
			bizNetAccessLabSampleAssets.updateLabSampleAsset(this.getOwnerComponent(), oModel);
			this.onLabSampleAssetEdit(oEvent);
		},

		removeLabSampleAsset: function () {

			var oModel = this.getView().getModel("LabSampleAssets");
			bizNetAccessLabSampleAssets.removeLabSampleAsset(this.getOwnerComponent(), oModel, oModel.getProperty("/selectedLabSampleAsset/ID"));
		},

		editLabSampleAsset: function (oEvent) {
			var isEditable;
			if (isEditable === true) {
				isEditable = false;
			} else {
				isEditable = true;
			}
		},

		_clearNewAsset: function () {

			this.getView().getModel("LabSampleAssets").setProperty("/newLabSampleAsset", {});
		}
	});

});