sap.ui.define([
	"chicken-netSetup/controller/BaseController",
	"chicken-netSetup/model/modelsBase",
	"chicken-netSetup/util/messageProvider",
	"chicken-netSetup/util/localStoreLabResultAssets",
	"chicken-netSetup/util/bizNetAccessLabResultAssets",
	"sap/ui/core/routing/History",
	"sap/ui/model/Filter",
	"sap/ui/model/FilterOperator",
	"sap/ui/Device"
], function(
		BaseController,
		modelsBase,
		messageProvider,
		localStoreLabResultAssets,
		bizNetAccessLabResultAssets,
		History,
		Filter,
		FilterOperator,
		Device
	) {
	"use strict";

	return BaseController.extend("chicken-netSetup.controller.LabResultAssetsOverview", {

		onInit : function(){

			var oList = this.byId("labResultAssetsList");
			this._oList = oList;
			this._oListFilterState = {
				aFilter : [],
				aSearch : []
			};
			localStoreLabResultAssets.init();
			if ( typeof this.getOwnerComponent().getModel("Messages") === "undefined" ){
				this.getOwnerComponent().setModel(modelsBase.createMessagesModel(), "Messages");
			}
			messageProvider.init(this.getOwnerComponent().getModel("Messages"));
		},

		onSelectionChange : function (oEvent) {

			this._showDetail(oEvent.getParameter("listItem") || oEvent.getSource());
		},

		onAdd : function() {

			this.getRouter().navTo("labResultAsset", {labResultAssetId : "___new"});
		},

		onRemoveAll : function(){

			bizNetAccessLabResultAssets.removeAllLabResultAssets(this.getOwnerComponent(), this.getOwnerComponent().getModel("LabResultAssets"));
		},

		onSearch : function(oEvent) {

			var sQuery = oEvent.getParameter("query");
			if (sQuery) {
				this._oListFilterState.aSearch = [new Filter("ID", FilterOperator.Contains, sQuery)];
			} else {
				this._oListFilterState.aSearch = [];
			}
			this._applyFilterSearch();
			var bReplace = !Device.system.phone;
			var oModel = this.getModel("LabResultAssets");
			oModel.setProperty(
				"/selectedLabResultAsset",
				_.findWhere(oModel.getProperty("/labResultAssetCollection/items"),
					{
						ID: oModel.getProperty("/searchLabResultAssetID")
					},
				this));
			this.getRouter().navTo("labResultAsset", {
				labResultAssetId : oModel.getProperty("/selectedLabResultAsset").ID
			}, bReplace);
		},

		_onToggleBaaS : function(){

			this.getOwnerComponent().getModel("TestSwitch").setProperty("/testMode",!(this.getView().byId("__buttonToggleLabResultAssets").getPressed()));
			var location = this.getRunMode().location;
			messageProvider.addMessage("Success", "Switching Network for Blockchain Data to: ", "No Description", location, 1, "", "http://www.sap.com");
			if ( this.getRunMode().testMode && localStoreLabResultAssets.getLabResultAssetData() === null ){
				localStoreLabResultAssets.init();
			}
			bizNetAccessLabResultAssets.loadAllLabResultAssets(this.getOwnerComponent(), this.getModel("LabResultAssets"));
		},

		_showDetail : function(oItem) {

			var bReplace = !Device.system.phone;
			var oModel = this.getModel("LabResultAssets");
			this.getRouter().navTo("labResultAsset", {
				labResultAssetId : oModel.getObject(oItem.getBindingContextPath()).ID
			}, bReplace);
		},

		_applyFilterSearch : function () {
			var aFilters = this._oListFilterState.aSearch.concat(this._oListFilterState.aFilter),
				oViewModel = this.getModel("LabResultAssets");
			this._oList.getBinding("items").filter(aFilters, "Application");
			if (aFilters.length !== 0) {
				oViewModel.setProperty("/noDataText", this.getResourceBundle().getText("_labelNoLabResultAssetsText"));
			}
		},

		onMessagePress: function() {

			this.onShowMessageDialog("chicken-netSetup Log");
		},

		onNavBack : function() {
			this.getRouter().navTo("home");
		}
	});
});
