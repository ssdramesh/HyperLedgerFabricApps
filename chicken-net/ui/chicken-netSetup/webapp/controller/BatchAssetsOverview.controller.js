sap.ui.define([
	"chicken-netSetup/controller/BaseController",
	"chicken-netSetup/model/modelsBase",
	"chicken-netSetup/util/messageProvider",
	"chicken-netSetup/util/localStoreBatchAssets",
	"chicken-netSetup/util/bizNetAccessBatchAssets",
	"sap/ui/core/routing/History",
	"sap/ui/model/Filter",
	"sap/ui/model/FilterOperator",
	"sap/ui/Device"
], function (

	BaseController,
	modelsBase,
	messageProvider,
	localStoreBatchAssets,
	bizNetAccessBatchAssets,
	History,
	Filter,
	FilterOperator,
	Device
) {
	"use strict";

	return BaseController.extend("chicken-netSetup.controller.BatchAssetsOverview", {

		onInit: function () {

			var oList = this.byId("batchAssetsList");
			this._oList = oList;
			this._oListFilterState = {
				aFilter: [],
				aSearch: []
			};
			localStoreBatchAssets.init();
			if (typeof this.getOwnerComponent().getModel("Messages") === "undefined") {
				this.getOwnerComponent().setModel(modelsBase.createMessagesModel(), "Messages");
			}
			messageProvider.init(this.getOwnerComponent().getModel("Messages"));
		},

		onSelectionChange: function (oEvent) {

			this._showDetail(oEvent.getParameter("listItem") || oEvent.getSource());
		},

		onAdd: function () {

			this.getRouter().navTo("batchAsset", {
				batchAssetId: "___new"
			});
		},

		onRemoveAll: function () {

			bizNetAccessBatchAssets.removeAllBatchAssets(this.getOwnerComponent(), this.getOwnerComponent().getModel("BatchAssets"));
		},

		onSearch: function (oEvent) {

			var sQuery = oEvent.getParameter("query");
			if (sQuery) {
				this._oListFilterState.aSearch = [new Filter("ID", FilterOperator.Contains, sQuery)];
			} else {
				this._oListFilterState.aSearch = [];
			}
			this._applyFilterSearch();
			var bReplace = !Device.system.phone;
			var oModel = this.getModel("BatchAssets");
			oModel.setProperty(
				"/selectedBatchAsset",
				_.findWhere(oModel.getProperty("/batchAssetCollection/items"), {
						ID: oModel.getProperty("/searchBatchAssetID")
					},
					this));
			this.getRouter().navTo("batchAsset", {
				batchAssetId: oModel.getProperty("/selectedBatchAsset").ID
			}, bReplace);
		},

		_onToggleBaaS: function () {

			this.getOwnerComponent().getModel("TestSwitch").setProperty("/testMode", !(this.getView().byId("__buttonToggleBatchAssets").getPressed()));
			var location = this.getRunMode().location;
			messageProvider.addMessage("Success", "Switching Network for Blockchain Data to: ", "No Description", location, 1, "",
				"http://www.sap.com");
			if (this.getRunMode().testMode && localStoreBatchAssets.getBatchAssetData() === null) {
				localStoreBatchAssets.init();
			}
			bizNetAccessBatchAssets.loadAllBatchAssets(this.getOwnerComponent(), this.getModel("BatchAssets"));
		},

		_showDetail: function (oItem) {

			var bReplace = !Device.system.phone;
			var oModel = this.getModel("BatchAssets");
			this.getRouter().navTo("batchAsset", {
				batchAssetId: oModel.getObject(oItem.getBindingContextPath()).ID
			}, bReplace);
		},

		_applyFilterSearch: function () {
			var aFilters = this._oListFilterState.aSearch.concat(this._oListFilterState.aFilter),
				oViewModel = this.getModel("BatchAssets");
			this._oList.getBinding("items").filter(aFilters, "Application");
			if (aFilters.length !== 0) {
				oViewModel.setProperty("/noDataText", this.getResourceBundle().getText("_labelNoBatchAssetsText"));
			}
		},

		onMessagePress: function () {

			this.onShowMessageDialog("chicken-netSetup Log");
		},

		onNavBack: function () {
			this.getRouter().navTo("home");
		}
	});
});