sap.ui.define([
	"chicken-netSetup/controller/BaseController",
	"chicken-netSetup/util/bizNetAccessSlaughterBatchAssets"
], function (
	BaseController,
	bizNetAccessSlaughterBatchAssets
) {
	"use strict";

	return BaseController.extend("chicken-netSetup.controller.SlaughterBatchAssetDetails", {

		onInit: function () {

			this.getOwnerComponent().getRouter().getRoute("slaughterBatchAsset").attachPatternMatched(this._onObjectMatched, this);
		},

		_onObjectMatched: function (oEvent) {

			var pId = oEvent.getParameter("arguments").slaughterBatchAssetId;
			if (pId === "___new") {
				this.getView().byId("__barSlaughterBatchAsset").setSelectedKey("New");
			} else {
				bizNetAccessSlaughterBatchAssets.loadSlaughterBatchAsset(this.getView().getModel("SlaughterBatchAssets"), oEvent.getParameter(
					"arguments").slaughterBatchAssetId);
			}
		},

		addNew: function () {

			var oModel = this.getView().getModel("SlaughterBatchAssets");
			if (oModel.getProperty("/newSlaughterBatchAsset/Alias") !== "" ||
				oModel.getProperty("/newSlaughterBatchAsset/Description") !== "") {
				var slaughterBatchAssetId = bizNetAccessSlaughterBatchAssets.addNewSlaughterBatchAsset(this.getOwnerComponent(), oModel);
				this.getView().byId("__barSlaughterBatchAsset").setSelectedKey("Details");
				bizNetAccessSlaughterBatchAssets.loadSlaughterBatchAsset(this.getView().getModel("SlaughterBatchAssets"), slaughterBatchAssetId);
			} else {
				sap.m.MessageToast.show("Please enter valid data in all fields", {});
			}
			this._clearNewAsset();
		},

		saveSlaughterBatchAsset: function (oEvent) {

			var oModel = this.getView().getModel("SlaughterBatchAssets");
			bizNetAccessSlaughterBatchAssets.updateSlaughterBatchAsset(this.getOwnerComponent(), oModel);
			this.onSlaughterBatchAssetEdit(oEvent);
		},

		removeSlaughterBatchAsset: function () {

			var oModel = this.getView().getModel("SlaughterBatchAssets");
			bizNetAccessSlaughterBatchAssets.removeSlaughterBatchAsset(this.getOwnerComponent(), oModel, oModel.getProperty(
				"/selectedSlaughterBatchAsset/ID"));
		},

		editSlaughterBatchAsset: function (oEvent) {
			var isEditable;
			if (isEditable === true) {
				isEditable = false;
			} else {
				isEditable = true;
			}
		},

		_clearNewAsset: function () {

			this.getView().getModel("SlaughterBatchAssets").setProperty("/newSlaughterBatchAsset", {});
		}
	});

});