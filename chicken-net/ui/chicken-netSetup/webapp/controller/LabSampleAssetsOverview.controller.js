sap.ui.define([
	"chicken-netSetup/controller/BaseController",
	"chicken-netSetup/model/modelsBase",
	"chicken-netSetup/util/messageProvider",
	"chicken-netSetup/util/localStoreLabSampleAssets",
	"chicken-netSetup/util/bizNetAccessLabSampleAssets",
	"sap/ui/core/routing/History",
	"sap/ui/model/Filter",
	"sap/ui/model/FilterOperator",
	"sap/ui/Device"
], function (
	BaseController,
	modelsBase,
	messageProvider,
	localStoreLabSampleAssets,
	bizNetAccessLabSampleAssets,
	History,
	Filter,
	FilterOperator,
	Device
) {
	"use strict";

	return BaseController.extend("chicken-netSetup.controller.LabSampleAssetsOverview", {

		onInit: function () {

			var oList = this.byId("labSampleAssetsList");
			this._oList = oList;
			this._oListFilterState = {
				aFilter: [],
				aSearch: []
			};
			localStoreLabSampleAssets.init();
			if (typeof this.getOwnerComponent().getModel("Messages") === "undefined") {
				this.getOwnerComponent().setModel(modelsBase.createMessagesModel(), "Messages");
			}
			messageProvider.init(this.getOwnerComponent().getModel("Messages"));
		},

		onSelectionChange: function (oEvent) {

			this._showDetail(oEvent.getParameter("listItem") || oEvent.getSource());
		},

		onAdd: function () {

			this.getRouter().navTo("labSampleAsset", {
				labSampleAssetId: "___new"
			});
		},

		onRemoveAll: function () {

			bizNetAccessLabSampleAssets.removeAllLabSampleAssets(this.getOwnerComponent(), this.getOwnerComponent().getModel("LabSampleAssets"));
		},

		onSearch: function (oEvent) {

			var sQuery = oEvent.getParameter("query");
			if (sQuery) {
				this._oListFilterState.aSearch = [new Filter("ID", FilterOperator.Contains, sQuery)];
			} else {
				this._oListFilterState.aSearch = [];
			}
			this._applyFilterSearch();
			var bReplace = !Device.system.phone;
			var oModel = this.getModel("LabSampleAssets");
			oModel.setProperty(
				"/selectedLabSampleAsset",
				_.findWhere(oModel.getProperty("/labSampleAssetCollection/items"), {
						ID: oModel.getProperty("/searchLabSampleAssetID")
					},
					this));
			this.getRouter().navTo("labSampleAsset", {
				labSampleAssetId: oModel.getProperty("/selectedLabSampleAsset").ID
			}, bReplace);
		},

		_onToggleBaaS: function () {

			this.getOwnerComponent().getModel("TestSwitch").setProperty("/testMode", !(this.getView().byId("__buttonToggleLabSampleAssets").getPressed()));
			var location = this.getRunMode().location;
			messageProvider.addMessage("Success", "Switching Network for Blockchain Data to: ", "No Description", location, 1, "",
				"http://www.sap.com");
			if (this.getRunMode().testMode && localStoreLabSampleAssets.getLabSampleAssetData() === null) {
				localStoreLabSampleAssets.init();
			}
			bizNetAccessLabSampleAssets.loadAllLabSampleAssets(this.getOwnerComponent(), this.getModel("LabSampleAssets"));
		},

		_showDetail: function (oItem) {

			var bReplace = !Device.system.phone;
			var oModel = this.getModel("LabSampleAssets");
			this.getRouter().navTo("labSampleAsset", {
				labSampleAssetId: oModel.getObject(oItem.getBindingContextPath()).ID
			}, bReplace);
		},

		_applyFilterSearch: function () {
			var aFilters = this._oListFilterState.aSearch.concat(this._oListFilterState.aFilter),
				oViewModel = this.getModel("LabSampleAssets");
			this._oList.getBinding("items").filter(aFilters, "Application");
			if (aFilters.length !== 0) {
				oViewModel.setProperty("/noDataText", this.getResourceBundle().getText("_labelNoLabSampleAssetsText"));
			}
		},

		onMessagePress: function () {

			this.onShowMessageDialog("chicken-netSetup Log");
		},

		onNavBack: function () {
			this.getRouter().navTo("home");
		}
	});
});