sap.ui.define([
	"chicken-netSetup/controller/BaseController",
	"chicken-netSetup/model/modelsBase",
	"chicken-netSetup/util/messageProvider",
	"chicken-netSetup/util/localStoreFeedMixAssets",
	"chicken-netSetup/util/bizNetAccessFeedMixAssets",
	"sap/ui/core/routing/History",
	"sap/ui/model/Filter",
	"sap/ui/model/FilterOperator",
	"sap/ui/Device"
], function (

	BaseController,
	modelsBase,
	messageProvider,
	localStoreFeedMixAssets,
	bizNetAccessFeedMixAssets,
	History,
	Filter,
	FilterOperator,
	Device
) {
	"use strict";

	return BaseController.extend("chicken-netSetup.controller.FeedMixAssetsOverview", {

		onInit: function () {

			var oList = this.byId("feedMixAssetsList");
			this._oList = oList;
			this._oListFilterState = {
				aFilter: [],
				aSearch: []
			};
			localStoreFeedMixAssets.init();
			if (typeof this.getOwnerComponent().getModel("Messages") === "undefined") {
				this.getOwnerComponent().setModel(modelsBase.createMessagesModel(), "Messages");
			}
			messageProvider.init(this.getOwnerComponent().getModel("Messages"));
		},

		onSelectionChange: function (oEvent) {

			this._showDetail(oEvent.getParameter("listItem") || oEvent.getSource());
		},

		onAdd: function () {

			this.getRouter().navTo("feedMixAsset", {
				feedMixAssetId: "___new"
			});
		},

		onRemoveAll: function () {

			bizNetAccessFeedMixAssets.removeAllFeedMixAssets(this.getOwnerComponent(), this.getOwnerComponent().getModel("FeedMixAssets"));
		},

		onSearch: function (oEvent) {

			var sQuery = oEvent.getParameter("query");
			if (sQuery) {
				this._oListFilterState.aSearch = [new Filter("ID", FilterOperator.Contains, sQuery)];
			} else {
				this._oListFilterState.aSearch = [];
			}
			this._applyFilterSearch();
			var bReplace = !Device.system.phone;
			var oModel = this.getModel("FeedMixAssets");
			oModel.setProperty(
				"/selectedFeedMixAsset",
				_.findWhere(oModel.getProperty("/feedMixAssetCollection/items"), {
						ID: oModel.getProperty("/searchFeedMixAssetID")
					},
					this));
			this.getRouter().navTo("feedMixAsset", {
				feedMixAssetId: oModel.getProperty("/selectedFeedMixAsset").ID
			}, bReplace);
		},

		_onToggleBaaS: function () {

			this.getOwnerComponent().getModel("TestSwitch").setProperty("/testMode", !(this.getView().byId("__buttonToggleFeedMixAssets").getPressed()));
			var location = this.getRunMode().location;
			messageProvider.addMessage("Success", "Switching Network for Blockchain Data to: ", "No Description", location, 1, "",
				"http://www.sap.com");
			if (this.getRunMode().testMode && localStoreFeedMixAssets.getFeedMixAssetData() === null) {
				localStoreFeedMixAssets.init();
			}
			bizNetAccessFeedMixAssets.loadAllFeedMixAssets(this.getOwnerComponent(), this.getModel("FeedMixAssets"));
		},

		_showDetail: function (oItem) {

			var bReplace = !Device.system.phone;
			var oModel = this.getModel("FeedMixAssets");
			this.getRouter().navTo("feedMixAsset", {
				feedMixAssetId: oModel.getObject(oItem.getBindingContextPath()).ID
			}, bReplace);
		},

		_applyFilterSearch: function () {
			var aFilters = this._oListFilterState.aSearch.concat(this._oListFilterState.aFilter),
				oViewModel = this.getModel("FeedMixAssets");
			this._oList.getBinding("items").filter(aFilters, "Application");
			if (aFilters.length !== 0) {
				oViewModel.setProperty("/noDataText", this.getResourceBundle().getText("_labelNoFeedMixAssetsText"));
			}
		},

		onMessagePress: function () {

			this.onShowMessageDialog("chicken-netSetup Log");
		},

		onNavBack: function () {
			this.getRouter().navTo("home");
		}
	});
});