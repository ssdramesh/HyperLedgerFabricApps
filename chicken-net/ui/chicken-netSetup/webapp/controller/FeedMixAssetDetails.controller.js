sap.ui.define([
	"chicken-netSetup/controller/BaseController",
	"chicken-netSetup/util/bizNetAccessFeedMixAssets"
], function (

	BaseController,
	bizNetAccessFeedMixAssets
) {
	"use strict";

	return BaseController.extend("chicken-netSetup.controller.FeedMixAssetDetails", {

		onInit: function () {

			this.getOwnerComponent().getRouter().getRoute("feedMixAsset").attachPatternMatched(this._onObjectMatched, this);
		},

		_onObjectMatched: function (oEvent) {

			var pId = oEvent.getParameter("arguments").feedMixAssetId;
			if (pId === "___new") {
				this.getView().byId("__barFeedMixAsset").setSelectedKey("New");
			} else {
				bizNetAccessFeedMixAssets.loadFeedMixAsset(this.getView().getModel("FeedMixAssets"), oEvent.getParameter("arguments").feedMixAssetId);
			}
		},

		addNew: function () {

			var oModel = this.getView().getModel("FeedMixAssets");
			if (oModel.getProperty("/newFeedMixAsset/Alias") !== "" ||
				oModel.getProperty("/newFeedMixAsset/Description") !== "") {
				var feedMixAssetId = bizNetAccessFeedMixAssets.addNewFeedMixAsset(this.getOwnerComponent(), oModel);
				this.getView().byId("__barFeedMixAsset").setSelectedKey("Details");
				bizNetAccessFeedMixAssets.loadFeedMixAsset(this.getView().getModel("FeedMixAssets"), feedMixAssetId);
			} else {
				sap.m.MessageToast.show("Please enter valid data in all fields", {});
			}
			this._clearNewAsset();
		},

		saveFeedMixAsset: function (oEvent) {

			var oModel = this.getView().getModel("FeedMixAssets");
			bizNetAccessFeedMixAssets.updateFeedMixAsset(this.getOwnerComponent(), oModel);
			this.onFeedMixAssetEdit(oEvent);
		},

		removeFeedMixAsset: function () {

			var oModel = this.getView().getModel("FeedMixAssets");
			bizNetAccessFeedMixAssets.removeFeedMixAsset(this.getOwnerComponent(), oModel, oModel.getProperty("/selectedFeedMixAsset/ID"));
		},

		editFeedMixAsset: function (oEvent) {
			var isEditable;
			if (isEditable === true) {
				isEditable = false;
			} else {
				isEditable = true;
			}
		},

		_clearNewAsset: function () {

			this.getView().getModel("FeedMixAssets").setProperty("/newFeedMixAsset", {});
		}
	});

});