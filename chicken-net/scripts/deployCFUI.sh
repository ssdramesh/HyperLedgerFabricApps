#!/bin/bash

cd /Users/i047582/Documents/Workspaces/github.wdf.sap.corp/HyperLedgerFabricApps/chicken-net/ui/chicken-netSetup
echo
echo -e "${LIGHT_PURPLE}Installing dependencies for node.js buildpack...Please enter your ${RED}super user (sudo)${LIGHT_PURPLE} password...${NC}"
sudo npm install --save
sudo npm audit fix
echo
cf login -a https://api.cf.us10.hana.ondemand.com
cf push
