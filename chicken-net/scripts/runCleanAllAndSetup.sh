#!/bin/bash

cd /Users/i047582/Documents/Workspaces/github.wdf.sap.corp/HyperLedgerFabricApps/chicken-net/test/newman/CleanAllAndSetup

echo "Clean All FeedMixAssets before Setup..."
newman run cleanAllFeedMixAssets.postman_collection.json -e chicken-net.postman_environment.json --bail newman
echo "Setting up FeedMixAssets..."
newman run createAllFeedMixAssets.postman_collection.json -e chicken-net.postman_environment.json -d ./data/FeedMixAssets.json --bail newman

echo "Clean All LabSampleAssets before Setup..."
newman run cleanAllLabSampleAssets.postman_collection.json -e chicken-net.postman_environment.json --bail newman
echo "Setting up LabSampleAssets..."
newman run createAllLabSampleAssets.postman_collection.json -e chicken-net.postman_environment.json -d ./data/LabSampleAssets.json --bail newman

echo "Clean All LabResultAssets before Setup..."
newman run cleanAllLabResultAssets.postman_collection.json -e chicken-net.postman_environment.json --bail newman
echo "Setting up LabResultAssets..."
newman run createAllLabResultAssets.postman_collection.json -e chicken-net.postman_environment.json -d ./data/LabResultAssets.json --bail newman

echo "Clean All SlaughterBatchAssets before Setup..."
newman run cleanAllSlaughterBatchAssets.postman_collection.json -e chicken-net.postman_environment.json --bail newman
echo "Setting up SlaughterBatchAssets..."
newman run createAllSlaughterBatchAssets.postman_collection.json -e chicken-net.postman_environment.json -d ./data/SlaughterBatchAssets.json --bail newman

echo "Clean All BatchAssets before Setup..."
newman run cleanAllBatchAssets.postman_collection.json -e chicken-net.postman_environment.json --bail newman
echo "Setting up BatchAssets..."
newman run createAllBatchAssets.postman_collection.json -e chicken-net.postman_environment.json -d ./data/BatchAssets.json --bail newman

echo "Done. Ready to run!"
