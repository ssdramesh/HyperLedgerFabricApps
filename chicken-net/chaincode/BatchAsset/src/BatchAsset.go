package main

/*
Template Version: 1.2-20180921
Template Owner: Ramesh Suraparaju (ramesh.suraparaju@sap.com)
This template is used to generate the chaincode for an asset that implements the hyperledger fabric shim
*/
import (
	"bytes"
	"encoding/json"
	"errors"
	"fmt"
	"net/http"
	"strings"
	"time"

	"github.com/hyperledger/fabric/core/chaincode/shim"
	"github.com/hyperledger/fabric/protos/peer"
	"github.com/tidwall/gjson"
)

var logger = shim.NewLogger("CLDChaincode")

//BatchAssetStatus - enumeration for batchAsset statuses (Same status enumeration for BatchAsset)
type BatchAssetStatus int

const (
	ReadyForDistribution BatchAssetStatus = 1 + iota //ReadyForDistribution: Batch ready for distribution
	InDistribution                                   //InDistribution: Batch in distribution
)

var batchAssetStatuses = [...]string{
	"Ready for Distribution",
	"In Distribution",
}

func (batchAssetStatus BatchAssetStatus) batchAssetStatusToString() string {
	return batchAssetStatuses[batchAssetStatus-1]
}

//BatchFeedMix (dataType)
type BatchFeedMix struct {
	FeedMixID          string `json:"feedMixID"`
	SupplierID         string `json:"supplierID"`
	SupplierLandDeedID string `json:"supplierLandDeedID"`
}

//BatchSlaughterBatch (dataType)
type BatchSlaughterBatch struct {
	SlaughterBatchID string `json:"slaughterBatchID"`
	FeedFormulaID    string `json:"feedFormulaID"`
}

//BatchProcessData (dataType)
type BatchProcessData struct {
	SalesOrderID      string `json:"salesOrderID"`
	DeliveryOrderID   string `json:"deliveryOrderID"`
	GoodsIssueID      string `json:"goodsIssueID"`
	GoodsIssueBatchID string `json:"goodsIssueBatchID"`
}

//BatchQuality (dataType)
type BatchQuality struct {
	LabID                string `json:"labID"`
	LabSampleID          string `json:"labSampleID"`
	CertificateID        string `json:"certificateID"`
	ProtienPercent       string `json:"protienPercent"`
	FatPercent           string `json:"fatPercent"`
	Disease              string `json:"disease"`
	ContaminationPercent string `json:"contaminationPercent"`
	AntibioticPercent    string `json:"antibioticPercent"`
}

//BatchAsset - Chaincode for asset Batch
type BatchAsset struct {
}

//Batch - Details of the asset type Batch
type Batch struct {
	ID             string              `json:"ID"`
	ObjectType     string              `json:"docType"`
	Status         string              `json:"status"`
	CreationDate   string              `json:"creationDate"`
	FeedMix        BatchFeedMix        `json:"feedMix"`
	SlaughterBatch BatchSlaughterBatch `json:"slaughterBatch"`
	ProcessData    BatchProcessData    `json:"processData"`
	BatchQuality   BatchQuality        `json:"quality"`
}

//BatchIDIndex - Index on IDs for retrieval all Batchs
type BatchIDIndex struct {
	IDs []string `json:"IDs"`
}

//Success - Success Message
func Success(rc int32, doc string, payload []byte) peer.Response {
	logger.Infof("Success %d = %s", rc, doc, payload)
	return peer.Response{
		Status:  rc,
		Message: doc,
		Payload: payload,
	}
}

//Error - Error Message
func Error(rc int32, doc string) peer.Response {
	logger.Errorf("Error %d = %s", rc, doc)
	return peer.Response{
		Status:  rc,
		Message: doc,
	}
}

func main() {
	err := shim.Start(new(BatchAsset))
	if err != nil {
		fmt.Printf("Error starting BatchAsset chaincode function main(): %s", err)
	} else {
		fmt.Printf("Starting BatchAsset chaincode function main() executed successfully")
	}
}

//Init - The chaincode Init function: No  arguments, only initializes a ID array as Index for retrieval of all Batchs
func (btch *BatchAsset) Init(stub shim.ChaincodeStubInterface) peer.Response {
	var batchIDIndex BatchIDIndex
	record, _ := stub.GetState("batchIDIndex")
	//chaincode upgrade: Index might already exist, create new only if it does not
	if record == nil {
		bytes, _ := json.Marshal(batchIDIndex)
		stub.PutState("batchIDIndex", bytes)
	}
	return Success(http.StatusOK, "batchIDIndex initiated successfully", nil)
}

//Invoke - The chaincode Invoke function:
func (btch *BatchAsset) Invoke(stub shim.ChaincodeStubInterface) peer.Response {
	function, args := stub.GetFunctionAndParameters()
	logger.Debug("function: ", function)
	switch function {
	case "addNewBatch":
		return btch.addNewBatch(stub, args)
	case "updateBatch":
		return btch.updateBatch(stub, args)
	case "removeBatch":
		return btch.removeBatch(stub, args[0])
	case "removeAllBatchs":
		return btch.removeAllBatchs(stub)
	case "readBatch":
		return btch.readBatch(stub, args[0])
	case "readAllBatchs":
		return btch.readAllBatchs(stub)
	case "searchBatchsByID":
		return btch.searchBatchsByID(stub, args)
	case "getBatchHistory":
		return btch.getBatchHistory(stub, args)
	default:
		return Error(http.StatusBadRequest, "Received unknown function invocation")
	}
}

//Invoke Route: addNewBatch
func (btch *BatchAsset) addNewBatch(stub shim.ChaincodeStubInterface, args []string) peer.Response {
	batch, err := getBatchFromArgs(args)
	if err != nil {
		return Error(http.StatusBadRequest, "Batch Data is Corrupted")
	}
	batch.ObjectType = "Asset.BatchAsset"
	record, err := stub.GetState(batch.ID)
	if record != nil {
		return Error(http.StatusBadRequest, "This Batch already exists: "+batch.ID)
	}
	_, err = btch.saveBatch(stub, batch)
	if err != nil {
		return Error(http.StatusInternalServerError, err.Error())
	}
	_, err = btch.updateBatchIDIndex(stub, batch)
	if err != nil {
		return Error(http.StatusInternalServerError, err.Error())
	}
	return Success(http.StatusOK, "addNewBatch: successfully added: "+batch.ID, nil)
}

//Invoke Route: updateBatch
func (btch *BatchAsset) updateBatch(stub shim.ChaincodeStubInterface, args []string) peer.Response {
	input, err := getBatchFromArgs(args)
	if err != nil {
		return Error(http.StatusBadRequest, "Batch Data is Corrupted")
	}
	record, err := stub.GetState(input.ID)
	if record == nil {
		return Error(http.StatusBadRequest, "This Batch does not exist: "+input.ID)
	}
	_, err = btch.saveBatch(stub, input)
	if err != nil {
		return Error(http.StatusInternalServerError, err.Error())
	}
	return Success(http.StatusOK, "updateBatch: successfully updated: "+input.ID, nil)
}

//Invoke Route: removeBatch
func (btch *BatchAsset) removeBatch(stub shim.ChaincodeStubInterface, batchID string) peer.Response {
	_, err := btch.deleteBatch(stub, batchID)
	if err != nil {
		return Error(http.StatusInternalServerError, err.Error())
	}
	_, err = btch.deleteBatchIDIndex(stub, batchID)
	if err != nil {
		return Error(http.StatusInternalServerError, err.Error())
	}
	return Success(http.StatusOK, "removeBatch: successfully removed: "+batchID, nil)
}

//Invoke Route: removeAllBatchs
func (btch *BatchAsset) removeAllBatchs(stub shim.ChaincodeStubInterface) peer.Response {
	var batchIDIndex BatchIDIndex
	bytes, err := stub.GetState("batchIDIndex")
	if err != nil {
		return Error(http.StatusInternalServerError, "removeAllBatchs: Error getting batchIDIndex array")
	}
	err = json.Unmarshal(bytes, &batchIDIndex)
	if err != nil {
		return Error(http.StatusInternalServerError, "removeAllBatchs: Error unmarshalling batchIDIndex array JSON")
	}
	if len(batchIDIndex.IDs) == 0 {
		return Error(http.StatusInternalServerError, "removeAllBatchs: No batchs to remove")
	}
	for _, batchStructID := range batchIDIndex.IDs {
		_, err = btch.deleteBatch(stub, batchStructID)
		if err != nil {
			return Error(http.StatusInternalServerError, "Failed to remove Batch with ID: "+batchStructID)
		}
		_, err = btch.deleteBatchIDIndex(stub, batchStructID)
		if err != nil {
			return Error(http.StatusInternalServerError, err.Error())
		}
	}
	btch.initHolder(stub)
	return Success(http.StatusOK, "removeAllBatchs: Successfully removed all Batchs", nil)
}

//Query Route: readBatch
func (btch *BatchAsset) readBatch(stub shim.ChaincodeStubInterface, batchID string) peer.Response {
	batchAsByteArray, err := btch.retrieveBatch(stub, batchID)
	if err != nil {
		return Error(http.StatusInternalServerError, err.Error())
	}
	return Success(http.StatusOK, "readBatch: Successfully read Batch with ID: "+batchID, batchAsByteArray)
}

//Query Route: readAllBatchs
func (btch *BatchAsset) readAllBatchs(stub shim.ChaincodeStubInterface) peer.Response {
	var batchIDs BatchIDIndex
	bytes, err := stub.GetState("batchIDIndex")
	if err != nil {
		return Error(http.StatusInternalServerError, "readAllBatchs: Error getting batchIDIndex array")
	}
	err = json.Unmarshal(bytes, &batchIDs)
	if err != nil {
		return Error(http.StatusInternalServerError, "readAllBatchs: Error unmarshalling batchIDIndex array JSON")
	}
	result := "["

	var batchAsByteArray []byte

	for _, batchID := range batchIDs.IDs {
		batchAsByteArray, err = btch.retrieveBatch(stub, batchID)
		if err != nil {
			return Error(http.StatusNotFound, "Failed to retrieve batch with ID: "+batchID)
		}
		result += string(batchAsByteArray) + ","
	}
	if len(result) == 1 {
		result = "[]"
	} else {
		result = result[:len(result)-1] + "]"
	}
	return Success(http.StatusOK, "OK", []byte(result))
}

//GetBatchHistory - reads history of a batch with specified ID
func (btch *BatchAsset) getBatchHistory(stub shim.ChaincodeStubInterface, args []string) peer.Response {

	resultsIterator, err := stub.GetHistoryForKey(args[0])
	if err != nil {
		return Error(http.StatusNotFound, "Not Found")
	}
	defer resultsIterator.Close()
	// Write return buffer
	var buffer bytes.Buffer
	buffer.WriteString("{ \"values\": [")
	for resultsIterator.HasNext() {
		it, _ := resultsIterator.Next()
		if buffer.Len() > 15 {
			buffer.WriteString(",")
		}
		buffer.WriteString("{\"timestamp\":\"")
		buffer.WriteString(time.Unix(it.Timestamp.Seconds, int64(it.Timestamp.Nanos)).Format(time.Stamp))
		buffer.WriteString("\",\"batch\": ")
		buffer.WriteString(string(it.Value))
		buffer.WriteString("}")
	}
	buffer.WriteString("]}")
	return Success(http.StatusOK, "OK", buffer.Bytes())
}

// Search for all matching IDs, given a (regex) value expression and return both the IDs and text.
// For example: '^H.llo' will match any string starting with 'Hello' or 'Hallo'.
func (btch *BatchAsset) searchBatchsByID(stub shim.ChaincodeStubInterface, args []string) peer.Response {

	searchString := strings.Replace(args[0], "\"", ".", -1) // protect against SQL injection

	// stub.GetQueryResult takes a verbatim CouchDB (assuming this is used DB). See CouchDB documentation:
	//     http://docs.couchdb.org/en/2.0.0/api/database/find.html
	// For example:
	//	{
	//		"selector": {
	//			"value": {"$regex": %s"}
	//		},
	//		"fields": ["ID","value"],
	//		"limit":  99
	//	}
	queryString := fmt.Sprintf("{\"selector\": {\"ID\": {\"$regex\": \"%s\"}}, \"fields\": [\"ID\", \"docType\", \"status\", \"creationDate\", \"batch\":\"batch001\", \"slaughterBatch\":\"slaughterBatch001\", \"processData\":\"processData001\", \"batchQuality\":\"batchQuality001\"], \"limit\":99, \"execution_stats\": true}", strings.Replace(searchString, "\"", ".", -1))
	resultsIterator, err := stub.GetQueryResult(queryString)
	if err != nil {
		return Error(http.StatusInternalServerError, err.Error())
	}
	defer resultsIterator.Close()

	// Write return buffer
	var buffer bytes.Buffer
	buffer.WriteString("{ \"values\": [")
	for resultsIterator.HasNext() {
		it, _ := resultsIterator.Next()
		if buffer.Len() > 15 {
			buffer.WriteString(",")
		}
		buffer.WriteString("{\"ID\":\"")
		buffer.WriteString(it.Key)
		buffer.WriteString("\",\"batch\": ")
		buffer.WriteString(string(it.Value))
		buffer.WriteString("}")
	}
	buffer.WriteString("]}")
	return Success(http.StatusOK, "OK", buffer.Bytes())
}

//Helper: Save BatchAsset
func (btch *BatchAsset) saveBatch(stub shim.ChaincodeStubInterface, batch Batch) (bool, error) {
	bytes, err := json.Marshal(batch)
	if err != nil {
		return false, errors.New("Error converting batch record JSON")
	}
	err = stub.PutState(batch.ID, bytes)
	if err != nil {
		return false, errors.New("Error storing Batch record")
	}
	return true, nil
}

//Helper: delete BatchAsset
func (btch *BatchAsset) deleteBatch(stub shim.ChaincodeStubInterface, batchID string) (bool, error) {
	_, err := btch.retrieveBatch(stub, batchID)
	if err != nil {
		return false, errors.New("Batch with ID: " + batchID + " not found")
	}
	err = stub.DelState(batchID)
	if err != nil {
		return false, errors.New("Error deleting Batch record")
	}
	return true, nil
}

//Helper: Update batch Holder - updates Index
func (btch *BatchAsset) updateBatchIDIndex(stub shim.ChaincodeStubInterface, batch Batch) (bool, error) {
	var batchIDs BatchIDIndex
	bytes, err := stub.GetState("batchIDIndex")
	if err != nil {
		return false, errors.New("updateBatchIDIndex: Error getting batchIDIndex array Index from state")
	}
	err = json.Unmarshal(bytes, &batchIDs)
	if err != nil {
		return false, errors.New("updateBatchIDIndex: Error unmarshalling batchIDIndex array JSON")
	}
	batchIDs.IDs = append(batchIDs.IDs, batch.ID)
	bytes, err = json.Marshal(batchIDs)
	if err != nil {
		return false, errors.New("updateBatchIDIndex: Error marshalling new batch ID")
	}
	err = stub.PutState("batchIDIndex", bytes)
	if err != nil {
		return false, errors.New("updateBatchIDIndex: Error storing new batch ID in batchIDIndex (Index)")
	}
	return true, nil
}

//Helper: delete ID from batchStruct Holder
func (btch *BatchAsset) deleteBatchIDIndex(stub shim.ChaincodeStubInterface, batchID string) (bool, error) {
	var batchIDIndex BatchIDIndex
	bytes, err := stub.GetState("batchIDIndex")
	if err != nil {
		return false, errors.New("deleteBatchIDIndex: Error getting batchIDIndex array Index from state")
	}
	err = json.Unmarshal(bytes, &batchIDIndex)
	if err != nil {
		return false, errors.New("deleteBatchIDIndex: Error unmarshalling batchIDIndex array JSON")
	}
	batchIDIndex.IDs, err = deleteKeyFromStringArray(batchIDIndex.IDs, batchID)
	if err != nil {
		return false, errors.New(err.Error())
	}
	bytes, err = json.Marshal(batchIDIndex)
	if err != nil {
		return false, errors.New("deleteBatchIDIndex: Error marshalling new batchStruct ID")
	}
	err = stub.PutState("batchIDIndex", bytes)
	if err != nil {
		return false, errors.New("deleteBatchIDIndex: Error storing new batchStruct ID in batchIDIndex (Index)")
	}
	return true, nil
}

//Helper: Initialize truck ID Holder //change template
func (btch *BatchAsset) initHolder(stub shim.ChaincodeStubInterface) bool {
	var batchIDIndex BatchIDIndex
	bytes, _ := json.Marshal(batchIDIndex)
	stub.DelState("batchIDIndex")
	stub.PutState("batchIDIndex", bytes)
	return true
}

//deleteKeyFromArray
func deleteKeyFromStringArray(array []string, key string) (newArray []string, err error) {
	for _, entry := range array {
		if entry != key {
			newArray = append(newArray, entry)
		}
	}
	if len(newArray) == len(array) {
		return newArray, errors.New("Specified Key: " + key + " not found in Array")
	}
	return newArray, nil
}

//Helper: Retrieve
func (btch *BatchAsset) retrieveBatch(stub shim.ChaincodeStubInterface, batchID string) ([]byte, error) {
	var batch Batch
	var batchAsByteArray []byte
	bytes, err := stub.GetState(batchID)
	if err != nil {
		return batchAsByteArray, errors.New("retrieveBatch: Error retrieving batch with ID: " + batchID)
	}
	err = json.Unmarshal(bytes, &batch)
	if err != nil {
		return batchAsByteArray, errors.New("retrieveBatch: Corrupt batch record " + string(bytes))
	}
	batchAsByteArray, err = json.Marshal(batch)
	if err != nil {
		return batchAsByteArray, errors.New("readBatch: Invalid batch Object - Not a  valid JSON")
	}
	return batchAsByteArray, nil
}

//getBatchFromArgs - construct a batch structure from string array of arguments
func getBatchFromArgs(args []string) (batch Batch, err error) {
	if !gjson.Valid(args[0]) {
		return batch, errors.New("Invalid json")
	}

	err = gjson.Unmarshal([]byte(args[0]), &batch)
	if err != nil {
		return batch, err
	}
	return batch, nil
}
