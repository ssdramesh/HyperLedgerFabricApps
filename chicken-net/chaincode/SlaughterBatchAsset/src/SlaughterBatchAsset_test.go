package main

//Template Version: 1.1-20180704
import (
	"bytes"
	"encoding/json"
	"fmt"
	"strings"
	"testing"

	"github.com/hyperledger/fabric/core/chaincode/shim"
)

// History/Search
// ... the History and Search API calls are not supported in the mock interface

//TestSlaughterBatchAsset_Init
func TestSlaughterBatchAsset_Init(t *testing.T) {
	slaughterBatch := new(SlaughterBatchAsset)
	stub := shim.NewMockStub("slaughterBatch", slaughterBatch)
	checkInit(t, stub, [][]byte{[]byte("init")})
	checkState(t, stub, "slaughterBatchIDIndex", getExpectedSlaughterBatchIDIndex("init"))
}

//TestSlaughterBatchAsset_InvokeUnknownFunction
func TestSlaughterBatchAsset_InvokeUnknownFunction(t *testing.T) {
	slaughterBatch := new(SlaughterBatchAsset)
	stub := shim.NewMockStub("slaughterBatch", slaughterBatch)
	checkInit(t, stub, [][]byte{[]byte("init")})
	checkInvokeUnknownFunction(t, stub, [][]byte{[]byte("myFunction"), []byte("docType:Asset")})
}

//TestSlaughterBatchAsset_Invoke_addNewSlaughterBatch
func TestSlaughterBatchAsset_Invoke_addNewSlaughterBatchOK(t *testing.T) {
	slaughterBatch := new(SlaughterBatchAsset)
	stub := shim.NewMockStub("slaughterBatch", slaughterBatch)
	checkInit(t, stub, [][]byte{[]byte("init")})
	checkInvoke(t, stub, getFirstSlaughterBatchAssetForTesting())
	newSlaughterBatchID := "100001"
	checkState(t, stub, newSlaughterBatchID, getNewSlaughterBatchExpected())
	checkState(t, stub, "slaughterBatchIDIndex", getExpectedSlaughterBatchIDIndex("addNewSlaughterBatch"))
}

//TestSlaughterBatchAsset_Invoke_addNewSlaughterBatch
func TestSlaughterBatchAsset_Invoke_addNewSlaughterBatchDuplicate(t *testing.T) {
	slaughterBatch := new(SlaughterBatchAsset)
	stub := shim.NewMockStub("slaughterBatch", slaughterBatch)
	checkInit(t, stub, [][]byte{[]byte("init")})
	checkInvoke(t, stub, getFirstSlaughterBatchAssetForTesting())
	newSlaughterBatchID := "100001"
	checkState(t, stub, newSlaughterBatchID, getNewSlaughterBatchExpected())
	checkState(t, stub, "slaughterBatchIDIndex", getExpectedSlaughterBatchIDIndex("addNewSlaughterBatch"))
	res := stub.MockInvoke("1", getFirstSlaughterBatchAssetForTesting())
	if res.Status != shim.OK {
		checkError(t, "This SlaughterBatch already exists: 100001", res.Message)
	} else {
		fmt.Println("Error was expected, but not raised")
		t.FailNow()
	}
}

func TestSlaughterBatchAsset_Invoke_updateSlaughterBatchOK(t *testing.T) {
	slaughterBatch := new(SlaughterBatchAsset)
	stub := shim.NewMockStub("slaughterBatch", slaughterBatch)
	checkInit(t, stub, [][]byte{[]byte("init")})
	checkInvoke(t, stub, getFirstSlaughterBatchAssetForTesting())
	newSlaughterBatchID := "100001"
	checkState(t, stub, newSlaughterBatchID, getNewSlaughterBatchExpected())
	checkInvoke(t, stub, getFirstSlaughterBatchAssetForUpdateTestingOK())
	checkReadSlaughterBatchAssetAfterUpdateOK(t, stub, newSlaughterBatchID)
}

//TestSlaughterBatchAsset_Invoke_removeSlaughterBatchOK  //change template
func TestSlaughterBatchAsset_Invoke_removeSlaughterBatchOK(t *testing.T) {
	slaughterBatch := new(SlaughterBatchAsset)
	stub := shim.NewMockStub("slaughterBatch", slaughterBatch)
	checkInit(t, stub, [][]byte{[]byte("init")})
	checkInvoke(t, stub, getFirstSlaughterBatchAssetForTesting())
	checkInvoke(t, stub, getSecondSlaughterBatchAssetForTesting())
	checkReadAllSlaughterBatchsOK(t, stub)
	checkState(t, stub, "slaughterBatchIDIndex", getExpectedSlaughterBatchIDIndex("beforeRemoveSlaughterBatch"))
	checkInvoke(t, stub, getRemoveSecondSlaughterBatchAssetForTesting())
	remainingSlaughterBatchID := "100001"
	checkReadSlaughterBatchOK(t, stub, remainingSlaughterBatchID)
	checkState(t, stub, "slaughterBatchIDIndex", getExpectedSlaughterBatchIDIndex("afterRemoveSlaughterBatch"))
}

//TestSlaughterBatchAsset_Invoke_removeSlaughterBatchNOK  //change template
func TestSlaughterBatchAsset_Invoke_removeSlaughterBatchNOK(t *testing.T) {
	slaughterBatch := new(SlaughterBatchAsset)
	stub := shim.NewMockStub("slaughterBatch", slaughterBatch)
	checkInit(t, stub, [][]byte{[]byte("init")})
	checkInvoke(t, stub, getFirstSlaughterBatchAssetForTesting())
	firstSlaughterBatchID := "100001"
	checkReadSlaughterBatchOK(t, stub, firstSlaughterBatchID)
	checkState(t, stub, "slaughterBatchIDIndex", getExpectedSlaughterBatchIDIndex("addNewSlaughterBatch"))
	res := stub.MockInvoke("1", getRemoveSecondSlaughterBatchAssetForTesting())
	if res.Status != shim.OK {
		checkError(t, "SlaughterBatch with ID: "+"100002"+" not found", res.Message)
	} else {
		fmt.Println("Error was expected, but not raised")
		t.FailNow()
	}
	checkState(t, stub, "slaughterBatchIDIndex", getExpectedSlaughterBatchIDIndex("addNewSlaughterBatch"))
}

//TestSlaughterBatchAsset_Invoke_removeAllSlaughterBatchsOK  //change template
func TestSlaughterBatchAsset_Invoke_removeAllSlaughterBatchsOK(t *testing.T) {
	slaughterBatch := new(SlaughterBatchAsset)
	stub := shim.NewMockStub("slaughterBatch", slaughterBatch)
	checkInit(t, stub, [][]byte{[]byte("init")})
	checkInvoke(t, stub, getFirstSlaughterBatchAssetForTesting())
	checkInvoke(t, stub, getSecondSlaughterBatchAssetForTesting())
	checkReadAllSlaughterBatchsOK(t, stub)
	checkState(t, stub, "slaughterBatchIDIndex", getExpectedSlaughterBatchIDIndex("beforeRemoveSlaughterBatch"))
	checkInvoke(t, stub, getRemoveAllSlaughterBatchAssetsForTesting())
	checkState(t, stub, "slaughterBatchIDIndex", getExpectedSlaughterBatchIDIndex(""))
}

//TestSlaughterBatchAsset_Invoke_removeSlaughterBatchNOK  //change template
func TestSlaughterBatchAsset_Invoke_removeAllSlaughterBatchsNOK(t *testing.T) {
	slaughterBatch := new(SlaughterBatchAsset)
	stub := shim.NewMockStub("slaughterBatch", slaughterBatch)
	checkInit(t, stub, [][]byte{[]byte("init")})
	res := stub.MockInvoke("1", getRemoveAllSlaughterBatchAssetsForTesting())
	if res.Status != shim.OK {
		checkError(t, "removeAllSlaughterBatchs: No slaughterBatchs to remove", res.Message)
	} else {
		fmt.Println("Error was expected, but not raised")
		t.FailNow()
	}
	checkState(t, stub, "slaughterBatchIDIndex", getExpectedSlaughterBatchIDIndex(""))
}

//TestSlaughterBatchAsset_Query_readSlaughterBatch
func TestSlaughterBatchAsset_Query_readSlaughterBatch(t *testing.T) {
	slaughterBatch := new(SlaughterBatchAsset)
	stub := shim.NewMockStub("slaughterBatch", slaughterBatch)
	checkInit(t, stub, [][]byte{[]byte("init")})
	slaughterBatchID := "100001"
	checkInvoke(t, stub, getFirstSlaughterBatchAssetForTesting())
	checkReadSlaughterBatchOK(t, stub, slaughterBatchID)
	checkReadSlaughterBatchNOK(t, stub, "")
}

//TestSlaughterBatchAsset_Query_readAllSlaughterBatchs
func TestSlaughterBatchAsset_Query_readAllSlaughterBatchs(t *testing.T) {
	slaughterBatch := new(SlaughterBatchAsset)
	stub := shim.NewMockStub("slaughterBatch", slaughterBatch)
	checkInit(t, stub, [][]byte{[]byte("init")})
	checkInvoke(t, stub, getFirstSlaughterBatchAssetForTesting())
	checkInvoke(t, stub, getSecondSlaughterBatchAssetForTesting())
	checkReadAllSlaughterBatchsOK(t, stub)
}

/*
*
*	Helper Functions
*
 */
//Get first SlaughterBatchAsset for testing
func getFirstSlaughterBatchAssetForTesting() [][]byte {
	return [][]byte{[]byte("addNewSlaughterBatch"),
		[]byte("{\"ID\":\"100001\",\"docType\":\"Asset.SlaughterBatchAsset\",\"status\":\"0\",\"creationDate\":\"12/01/2018\", \"hatcheryFarmID\":\"hatcheryFarmID001\", \"growerFarmID\":\"growerFarmID001\", \"henHouseID\":\"henHouseID001\", \"chickenLotID\":\"chickenLotID001\", \"husbandryLineageID\":\"husbandryLineageID001\", \"feedMixID\":\"feedMixID001\", \"feedFormulaID\":\"feedFormulaID001\", \"feedStopDate\":\"feedStopDate001\", \"slaughterDate\":\"slaughterDate001\"}")}
}

//Get first SlaughterBatchAsset for update testing
func getFirstSlaughterBatchAssetForUpdateTestingOK() [][]byte {
	return [][]byte{[]byte("updateSlaughterBatch"),
		[]byte("{\"ID\":\"100001\",\"docType\":\"Asset.SlaughterBatchAsset\",\"status\":\"1\",\"creationDate\":\"12/01/2018\", \"hatcheryFarmID\":\"hatcheryFarmID001\", \"growerFarmID\":\"growerFarmID001\", \"henHouseID\":\"henHouseID001\", \"chickenLotID\":\"chickenLotID001\", \"husbandryLineageID\":\"husbandryLineageID001\", \"feedMixID\":\"feedMixID001\", \"feedFormulaID\":\"feedFormulaID001\", \"feedStopDate\":\"feedStopDate001\", \"slaughterDate\":\"slaughterDate001\"}")}
}

//Get second SlaughterBatchAsset for testing
func getSecondSlaughterBatchAssetForTesting() [][]byte {
	return [][]byte{[]byte("addNewSlaughterBatch"),
		[]byte("{\"ID\":\"100002\",\"docType\":\"Asset.SlaughterBatchAsset\",\"status\":\"0\",\"creationDate\":\"12/01/2018\", \"hatcheryFarmID\":\"hatcheryFarmID002\", \"growerFarmID\":\"growerFarmID002\", \"henHouseID\":\"henHouseID002\", \"chickenLotID\":\"chickenLotID002\", \"husbandryLineageID\":\"husbandryLineageID002\", \"feedMixID\":\"feedMixID002\", \"feedFormulaID\":\"feedFormulaID002\", \"feedStopDate\":\"feedStopDate002\", \"slaughterDate\":\"slaughterDate002\"}")}
}

//Get remove second SlaughterBatchAsset for testing //change template
func getRemoveSecondSlaughterBatchAssetForTesting() [][]byte {
	return [][]byte{[]byte("removeSlaughterBatch"),
		[]byte("100002")}
}

//Get remove all SlaughterBatchAssets for testing //change template
func getRemoveAllSlaughterBatchAssetsForTesting() [][]byte {
	return [][]byte{[]byte("removeAllSlaughterBatchs")}
}

//Get an expected value for testing
func getNewSlaughterBatchExpected() []byte {
	var slaughterBatch SlaughterBatch
	slaughterBatch.ID = "100001"
	slaughterBatch.ObjectType = "Asset.SlaughterBatchAsset"
	slaughterBatch.Status = "0"
	slaughterBatch.CreationDate = "12/01/2018"
	slaughterBatch.HatcheryFarmID = "hatcheryFarmID001"
	slaughterBatch.GrowerFarmID = "growerFarmID001"
	slaughterBatch.HenHouseID = "henHouseID001"
	slaughterBatch.ChickenLotID = "chickenLotID001"
	slaughterBatch.HusbandryLineageID = "husbandryLineageID001"
	slaughterBatch.FeedMixID = "feedMixID001"
	slaughterBatch.FeedFormulaID = "feedFormulaID001"
	slaughterBatch.FeedStopDate = "feedStopDate001"
	slaughterBatch.SlaughterDate = "slaughterDate001"
	slaughterBatchJSON, err := json.Marshal(slaughterBatch)
	if err != nil {
		fmt.Println("Error converting a SlaughterBatch record to JSON")
		return nil
	}
	return []byte(slaughterBatchJSON)
}

//Get an expected value for testing
func getUpdatedSlaughterBatchExpected() []byte {
	var slaughterBatch SlaughterBatch
	slaughterBatch.ID = "100001"
	slaughterBatch.ObjectType = "Asset.SlaughterBatchAsset"
	slaughterBatch.Status = "1"
	slaughterBatch.CreationDate = "12/01/2018"
	slaughterBatch.HatcheryFarmID = "hatcheryFarmID001"
	slaughterBatch.GrowerFarmID = "growerFarmID001"
	slaughterBatch.HenHouseID = "henHouseID001"
	slaughterBatch.ChickenLotID = "chickenLotID001"
	slaughterBatch.HusbandryLineageID = "husbandryLineageID001"
	slaughterBatch.FeedMixID = "feedMixID001"
	slaughterBatch.FeedFormulaID = "feedFormulaID001"
	slaughterBatch.FeedStopDate = "feedStopDate001"
	slaughterBatch.SlaughterDate = "slaughterDate001"
	slaughterBatch.HatcheryFarmID = "hatcheryFarmID001"
	slaughterBatch.GrowerFarmID = "growerFarmID001"
	slaughterBatch.HenHouseID = "henHouseID001"
	slaughterBatch.ChickenLotID = "chickenLotID001"
	slaughterBatch.HusbandryLineageID = "husbandryLineageID001"
	slaughterBatch.FeedMixID = "feedMixID001"
	slaughterBatch.FeedFormulaID = "feedFormulaID001"
	slaughterBatch.FeedStopDate = "feedStopDate001"
	slaughterBatch.SlaughterDate = "slaughterDate001"
	slaughterBatchJSON, err := json.Marshal(slaughterBatch)
	if err != nil {
		fmt.Println("Error converting a SlaughterBatch record to JSON")
		return nil
	}
	return []byte(slaughterBatchJSON)
}

//Get expected values of SlaughterBatchs for testing
func getExpectedSlaughterBatchs() []byte {
	var slaughterBatchs []SlaughterBatch
	var slaughterBatch SlaughterBatch
	slaughterBatch.ID = "100001"
	slaughterBatch.ObjectType = "Asset.SlaughterBatchAsset"
	slaughterBatch.Status = "0"
	slaughterBatch.CreationDate = "12/01/2018"
	slaughterBatch.HatcheryFarmID = "hatcheryFarmID001"
	slaughterBatch.GrowerFarmID = "growerFarmID001"
	slaughterBatch.HenHouseID = "henHouseID001"
	slaughterBatch.ChickenLotID = "chickenLotID001"
	slaughterBatch.HusbandryLineageID = "husbandryLineageID001"
	slaughterBatch.FeedMixID = "feedMixID001"
	slaughterBatch.FeedFormulaID = "feedFormulaID001"
	slaughterBatch.FeedStopDate = "feedStopDate001"
	slaughterBatch.SlaughterDate = "slaughterDate001"
	slaughterBatchs = append(slaughterBatchs, slaughterBatch)
	slaughterBatch.ID = "100002"
	slaughterBatch.ObjectType = "Asset.SlaughterBatchAsset"
	slaughterBatch.Status = "0"
	slaughterBatch.CreationDate = "12/01/2018"
	slaughterBatch.HatcheryFarmID = "hatcheryFarmID002"
	slaughterBatch.GrowerFarmID = "growerFarmID002"
	slaughterBatch.HenHouseID = "henHouseID002"
	slaughterBatch.ChickenLotID = "chickenLotID002"
	slaughterBatch.HusbandryLineageID = "husbandryLineageID002"
	slaughterBatch.FeedMixID = "feedMixID002"
	slaughterBatch.FeedFormulaID = "feedFormulaID002"
	slaughterBatch.FeedStopDate = "feedStopDate002"
	slaughterBatch.SlaughterDate = "slaughterDate002"
	slaughterBatchs = append(slaughterBatchs, slaughterBatch)
	slaughterBatchJSON, err := json.Marshal(slaughterBatchs)
	if err != nil {
		fmt.Println("Error converting slaughterBatch records to JSON")
		return nil
	}
	return []byte(slaughterBatchJSON)
}

func getExpectedSlaughterBatchIDIndex(funcName string) []byte {
	var slaughterBatchIDIndex SlaughterBatchIDIndex
	switch funcName {
	case "addNewSlaughterBatch":
		slaughterBatchIDIndex.IDs = append(slaughterBatchIDIndex.IDs, "100001")
		slaughterBatchIDIndexBytes, err := json.Marshal(slaughterBatchIDIndex)
		if err != nil {
			fmt.Println("Error converting SlaughterBatchIDIndex to JSON")
			return nil
		}
		return slaughterBatchIDIndexBytes
	case "beforeRemoveSlaughterBatch":
		slaughterBatchIDIndex.IDs = append(slaughterBatchIDIndex.IDs, "100001")
		slaughterBatchIDIndex.IDs = append(slaughterBatchIDIndex.IDs, "100002")
		slaughterBatchIDIndexBytes, err := json.Marshal(slaughterBatchIDIndex)
		if err != nil {
			fmt.Println("Error converting SlaughterBatchIDIndex to JSON")
			return nil
		}
		return slaughterBatchIDIndexBytes
	case "afterRemoveSlaughterBatch":
		slaughterBatchIDIndex.IDs = append(slaughterBatchIDIndex.IDs, "100001")
		slaughterBatchIDIndexBytes, err := json.Marshal(slaughterBatchIDIndex)
		if err != nil {
			fmt.Println("Error converting SlaughterBatchIDIndex to JSON")
			return nil
		}
		return slaughterBatchIDIndexBytes
	default:
		slaughterBatchIDIndexBytes, err := json.Marshal(slaughterBatchIDIndex)
		if err != nil {
			fmt.Println("Error converting SlaughterBatchIDIndex to JSON")
			return nil
		}
		return slaughterBatchIDIndexBytes
	}
}

//checkInit - helper to check the Initialization of chaincode: SlaughterBatchAsset
func checkInit(t *testing.T, stub *shim.MockStub, args [][]byte) {
	res := stub.MockInit("1", args)
	if res.Status != shim.OK {
		fmt.Println("Init failed", string(res.Message))
		t.FailNow()
	}
}

//checkState - helper for checking the chaincode state for a given stateKey afgainst an expected value
func checkState(t *testing.T, stub *shim.MockStub, stateKey string, expectedState []byte) {
	actualState := stub.State[stateKey]
	if actualState == nil {
		fmt.Println("State for ", stateKey, ": failed to get value")
		t.FailNow()
	}

	if bytes.Compare(actualState, expectedState) != 0 {
		fmt.Println("Incorrect State for " + stateKey + ": \nExpected: " + string(expectedState) + "\nActual  : " + string(actualState))
		t.FailNow()
	}
}

//checkInvoke - helper for checking Invoke of chaincode
func checkInvoke(t *testing.T, stub *shim.MockStub, args [][]byte) {
	res := stub.MockInvoke("1", args)
	if res.Status != shim.OK {
		fmt.Println("Invoke", args, "failed", string(res.Message))
		t.FailNow()
	}
}

func checkInvokeUnknownFunction(t *testing.T, stub *shim.MockStub, args [][]byte) {
	res := stub.MockInvoke("1", args)
	if res.Status != shim.OK {
		expectedErr := "Received unknown function invocation"
		actualErr := string(res.Message)
		if !(strings.Contains(actualErr, expectedErr)) {
			fmt.Println("Invoke with unknown function must throw error: ", expectedErr, "\n Actual Error :", actualErr)
			t.FailNow()
		}
	} else {
		t.FailNow()
	}
}

//checkReadSlaughterBatchOK - helper for positive test readSlaughterBatch
func checkReadSlaughterBatchOK(t *testing.T, stub *shim.MockStub, slaughterBatchID string) {
	res := stub.MockInvoke("1", [][]byte{[]byte("readSlaughterBatch"), []byte(slaughterBatchID)})
	if res.Status != shim.OK {
		fmt.Println("func readSlaughterBatch with ID: ", slaughterBatchID, " failed"+string(res.Message))
		t.FailNow()
	}
	if res.Payload == nil {
		fmt.Println("func readSlaughterBatch with ID: ", slaughterBatchID, "failed to get value")
		t.FailNow()
	}
	if bytes.Compare(getNewSlaughterBatchExpected(), []byte(res.Payload)) != 0 {
		fmt.Println("func readSlaughterBatch with ID: ", slaughterBatchID, "Expected:", string(getNewSlaughterBatchExpected()), "Actual:", string(res.Payload))
		t.FailNow()
	}
}

//checkReadSlaughterBatchNOK - helper for negative testing of readSlaughterBatch
func checkReadSlaughterBatchNOK(t *testing.T, stub *shim.MockStub, slaughterBatchID string) {
	//with no slaughterBatchID
	res := stub.MockInvoke("1", [][]byte{[]byte("readSlaughterBatch"), []byte("")})
	if res.Status != shim.OK {
		expectedErr := "retrieveSlaughterBatch: Corrupt slaughterBatch record "
		actualErr := string(res.Message)
		if !(strings.Contains(actualErr, expectedErr)) {
			fmt.Println("func readSlaughterBatch negative test: ", "Expected Error:", expectedErr, "Actual Error", actualErr)
			t.FailNow()
		}
	} else {
		t.FailNow()
	}
}

//checkReadSlaughterBatchAssetAfterUpdateOK - helper for positive test readSlaughterBatch after update
func checkReadSlaughterBatchAssetAfterUpdateOK(t *testing.T, stub *shim.MockStub, SlaughterBatchID string) {
	res := stub.MockInvoke("1", [][]byte{[]byte("readSlaughterBatch"), []byte(SlaughterBatchID)})
	if res.Status != shim.OK {
		fmt.Println("func readSlaughterBatch with ID: ", SlaughterBatchID, " failed"+string(res.Message))
		t.FailNow()
	}
	if res.Payload == nil {
		fmt.Println("func readSlaughterBatch with ID: ", SlaughterBatchID, "failed to get value")
		t.FailNow()
	}
	if bytes.Compare(getUpdatedSlaughterBatchExpected(), []byte(res.Payload)) != 0 {
		fmt.Println("func readSlaughterBatch with ID: ", SlaughterBatchID, "Expected:", string(getUpdatedSlaughterBatchExpected()), "Actual:", string(res.Payload))
		t.FailNow()
	}
}

func checkReadAllSlaughterBatchsOK(t *testing.T, stub *shim.MockStub) {
	res := stub.MockInvoke("1", [][]byte{[]byte("readAllSlaughterBatchs")})
	if res.Status != shim.OK {
		fmt.Println("func readAllSlaughterBatchs failed", string(res.Message))
		t.FailNow()
	}
	if res.Payload == nil {
		fmt.Println("func readAllSlaughterBatchs failed to get value")
		t.FailNow()
	}
	if bytes.Compare(getExpectedSlaughterBatchs(), []byte(res.Payload)) != 0 {
		fmt.Println("func readAllSlaughterBatchs Expected:\n", string(getExpectedSlaughterBatchs()), "\nActual:\n", string(res.Payload))
		t.FailNow()
	}
}

func checkError(t *testing.T, exp string, act string) {
	if strings.Compare(exp, act) != 0 {
		fmt.Println("Unexpected Error! Expecting ", exp, "\n Actual :", act)
		t.FailNow()
	}
}
