package main

/*
Template Version: 1.2-20180921
Template Owner: Ramesh Suraparaju (ramesh.suraparaju@sap.com)
This template is used to generate the chaincode for an asset that implements the hyperledger fabric shim
*/
import (
	"bytes"
	"encoding/json"
	"errors"
	"fmt"
	"net/http"
	"strings"
	"time"

	"github.com/hyperledger/fabric/core/chaincode/shim"
	"github.com/hyperledger/fabric/protos/peer"
	"github.com/tidwall/gjson"
)

var logger = shim.NewLogger("CLDChaincode")

//SlaughterBatchAsset - Chaincode for asset SlaughterBatch
type SlaughterBatchAsset struct {
}

//SlaughterBatch - Details of the asset type SlaughterBatch
type SlaughterBatch struct {
	ID                 string `json:"ID"`
	ObjectType         string `json:"docType"`
	Status             string `json:"status"`
	CreationDate       string `json:"creationDate"`
	HatcheryFarmID     string `json:"hatcheryFarmID"`
	GrowerFarmID       string `json:"growerFarmID"`
	HenHouseID         string `json:"henHouseID"`
	ChickenLotID       string `json:"chickenLotID"`
	HusbandryLineageID string `json:"husbandryLineageID"`
	FeedMixID          string `json:"feedMixID"`
	FeedFormulaID      string `json:"feedFormulaID"`
	FeedStopDate       string `json:"feedStopDate"`
	SlaughterDate      string `json:"slaughterDate"`
}

//SlaughterBatchIDIndex - Index on IDs for retrieval all SlaughterBatchs
type SlaughterBatchIDIndex struct {
	IDs []string `json:"IDs"`
}

//Success - Success Message
func Success(rc int32, doc string, payload []byte) peer.Response {
	logger.Infof("Success %d = %s", rc, doc, payload)
	return peer.Response{
		Status:  rc,
		Message: doc,
		Payload: payload,
	}
}

//Error - Error Message
func Error(rc int32, doc string) peer.Response {
	logger.Errorf("Error %d = %s", rc, doc)
	return peer.Response{
		Status:  rc,
		Message: doc,
	}
}

func main() {
	err := shim.Start(new(SlaughterBatchAsset))
	if err != nil {
		fmt.Printf("Error starting SlaughterBatchAsset chaincode function main(): %s", err)
	} else {
		fmt.Printf("Starting SlaughterBatchAsset chaincode function main() executed successfully")
	}
}

//Init - The chaincode Init function: No  arguments, only initializes a ID array as Index for retrieval of all SlaughterBatchs
func (slghtrBtch *SlaughterBatchAsset) Init(stub shim.ChaincodeStubInterface) peer.Response {
	var slaughterBatchIDIndex SlaughterBatchIDIndex
	record, _ := stub.GetState("slaughterBatchIDIndex")
	//chaincode upgrade: Index might already exist, create new only if it does not
	if record == nil {
		bytes, _ := json.Marshal(slaughterBatchIDIndex)
		stub.PutState("slaughterBatchIDIndex", bytes)
	}
	return Success(http.StatusOK, "slaughterBatchIDIndex initiated successfully", nil)
}

//Invoke - The chaincode Invoke function:
func (slghtrBtch *SlaughterBatchAsset) Invoke(stub shim.ChaincodeStubInterface) peer.Response {
	function, args := stub.GetFunctionAndParameters()
	logger.Debug("function: ", function)
	switch function {
	case "addNewSlaughterBatch":
		return slghtrBtch.addNewSlaughterBatch(stub, args)
	case "updateSlaughterBatch":
		return slghtrBtch.updateSlaughterBatch(stub, args)
	case "removeSlaughterBatch":
		return slghtrBtch.removeSlaughterBatch(stub, args[0])
	case "removeAllSlaughterBatchs":
		return slghtrBtch.removeAllSlaughterBatchs(stub)
	case "readSlaughterBatch":
		return slghtrBtch.readSlaughterBatch(stub, args[0])
	case "readAllSlaughterBatchs":
		return slghtrBtch.readAllSlaughterBatchs(stub)
	case "searchSlaughterBatchsByID":
		return slghtrBtch.searchSlaughterBatchsByID(stub, args)
	case "getSlaughterBatchHistory":
		return slghtrBtch.getSlaughterBatchHistory(stub, args)
	default:
		return Error(http.StatusBadRequest, "Received unknown function invocation")
	}
}

//Invoke Route: addNewSlaughterBatch
func (slghtrBtch *SlaughterBatchAsset) addNewSlaughterBatch(stub shim.ChaincodeStubInterface, args []string) peer.Response {
	slaughterBatch, err := getSlaughterBatchFromArgs(args)
	if err != nil {
		return Error(http.StatusBadRequest, "SlaughterBatch Data is Corrupted")
	}
	slaughterBatch.ObjectType = "Asset.SlaughterBatchAsset"
	record, err := stub.GetState(slaughterBatch.ID)
	if record != nil {
		return Error(http.StatusBadRequest, "This SlaughterBatch already exists: "+slaughterBatch.ID)
	}
	_, err = slghtrBtch.saveSlaughterBatch(stub, slaughterBatch)
	if err != nil {
		return Error(http.StatusInternalServerError, err.Error())
	}
	_, err = slghtrBtch.updateSlaughterBatchIDIndex(stub, slaughterBatch)
	if err != nil {
		return Error(http.StatusInternalServerError, err.Error())
	}
	return Success(http.StatusOK, "addNewSlaughterBatch: successfully added: "+slaughterBatch.ID, nil)
}

//Invoke Route: updateSlaughterBatch
func (slghtrBtch *SlaughterBatchAsset) updateSlaughterBatch(stub shim.ChaincodeStubInterface, args []string) peer.Response {
	input, err := getSlaughterBatchFromArgs(args)
	if err != nil {
		return Error(http.StatusBadRequest, "SlaughterBatch Data is Corrupted")
	}
	record, err := stub.GetState(input.ID)
	if record == nil {
		return Error(http.StatusBadRequest, "This SlaughterBatch does not exist: "+input.ID)
	}
	_, err = slghtrBtch.saveSlaughterBatch(stub, input)
	if err != nil {
		return Error(http.StatusInternalServerError, err.Error())
	}
	return Success(http.StatusOK, "updateSlaughterBatch: successfully updated: "+input.ID, nil)
}

//Invoke Route: removeSlaughterBatch
func (slghtrBtch *SlaughterBatchAsset) removeSlaughterBatch(stub shim.ChaincodeStubInterface, slaughterBatchID string) peer.Response {
	_, err := slghtrBtch.deleteSlaughterBatch(stub, slaughterBatchID)
	if err != nil {
		return Error(http.StatusInternalServerError, err.Error())
	}
	_, err = slghtrBtch.deleteSlaughterBatchIDIndex(stub, slaughterBatchID)
	if err != nil {
		return Error(http.StatusInternalServerError, err.Error())
	}
	return Success(http.StatusOK, "removeSlaughterBatch: successfully removed: "+slaughterBatchID, nil)
}

//Invoke Route: removeAllSlaughterBatchs
func (slghtrBtch *SlaughterBatchAsset) removeAllSlaughterBatchs(stub shim.ChaincodeStubInterface) peer.Response {
	var slaughterBatchIDIndex SlaughterBatchIDIndex
	bytes, err := stub.GetState("slaughterBatchIDIndex")
	if err != nil {
		return Error(http.StatusInternalServerError, "removeAllSlaughterBatchs: Error getting slaughterBatchIDIndex array")
	}
	err = json.Unmarshal(bytes, &slaughterBatchIDIndex)
	if err != nil {
		return Error(http.StatusInternalServerError, "removeAllSlaughterBatchs: Error unmarshalling slaughterBatchIDIndex array JSON")
	}
	if len(slaughterBatchIDIndex.IDs) == 0 {
		return Error(http.StatusInternalServerError, "removeAllSlaughterBatchs: No slaughterBatchs to remove")
	}
	for _, slaughterBatchStructID := range slaughterBatchIDIndex.IDs {
		_, err = slghtrBtch.deleteSlaughterBatch(stub, slaughterBatchStructID)
		if err != nil {
			return Error(http.StatusInternalServerError, "Failed to remove SlaughterBatch with ID: "+slaughterBatchStructID)
		}
		_, err = slghtrBtch.deleteSlaughterBatchIDIndex(stub, slaughterBatchStructID)
		if err != nil {
			return Error(http.StatusInternalServerError, err.Error())
		}
	}
	slghtrBtch.initHolder(stub)
	return Success(http.StatusOK, "removeAllSlaughterBatchs: Successfully removed all SlaughterBatchs", nil)
}

//Query Route: readSlaughterBatch
func (slghtrBtch *SlaughterBatchAsset) readSlaughterBatch(stub shim.ChaincodeStubInterface, slaughterBatchID string) peer.Response {
	slaughterBatchAsByteArray, err := slghtrBtch.retrieveSlaughterBatch(stub, slaughterBatchID)
	if err != nil {
		return Error(http.StatusInternalServerError, err.Error())
	}
	return Success(http.StatusOK, "readSlaughterBatch: Successfully read SlaughterBatch with ID: "+slaughterBatchID, slaughterBatchAsByteArray)
}

//Query Route: readAllSlaughterBatchs
func (slghtrBtch *SlaughterBatchAsset) readAllSlaughterBatchs(stub shim.ChaincodeStubInterface) peer.Response {
	var slaughterBatchIDs SlaughterBatchIDIndex
	bytes, err := stub.GetState("slaughterBatchIDIndex")
	if err != nil {
		return Error(http.StatusInternalServerError, "readAllSlaughterBatchs: Error getting slaughterBatchIDIndex array")
	}
	err = json.Unmarshal(bytes, &slaughterBatchIDs)
	if err != nil {
		return Error(http.StatusInternalServerError, "readAllSlaughterBatchs: Error unmarshalling slaughterBatchIDIndex array JSON")
	}
	result := "["

	var slaughterBatchAsByteArray []byte

	for _, slaughterBatchID := range slaughterBatchIDs.IDs {
		slaughterBatchAsByteArray, err = slghtrBtch.retrieveSlaughterBatch(stub, slaughterBatchID)
		if err != nil {
			return Error(http.StatusNotFound, "Failed to retrieve slaughterBatch with ID: "+slaughterBatchID)
		}
		result += string(slaughterBatchAsByteArray) + ","
	}
	if len(result) == 1 {
		result = "[]"
	} else {
		result = result[:len(result)-1] + "]"
	}
	return Success(http.StatusOK, "OK", []byte(result))
}

//GetSlaughterBatchHistory - reads history of a slaughterBatch with specified ID
func (slghtrBtch *SlaughterBatchAsset) getSlaughterBatchHistory(stub shim.ChaincodeStubInterface, args []string) peer.Response {

	resultsIterator, err := stub.GetHistoryForKey(args[0])
	if err != nil {
		return Error(http.StatusNotFound, "Not Found")
	}
	defer resultsIterator.Close()
	// Write return buffer
	var buffer bytes.Buffer
	buffer.WriteString("{ \"values\": [")
	for resultsIterator.HasNext() {
		it, _ := resultsIterator.Next()
		if buffer.Len() > 15 {
			buffer.WriteString(",")
		}
		buffer.WriteString("{\"timestamp\":\"")
		buffer.WriteString(time.Unix(it.Timestamp.Seconds, int64(it.Timestamp.Nanos)).Format(time.Stamp))
		buffer.WriteString("\",\"slaughterBatch\": ")
		buffer.WriteString(string(it.Value))
		buffer.WriteString("}")
	}
	buffer.WriteString("]}")
	return Success(http.StatusOK, "OK", buffer.Bytes())
}

// Search for all matching IDs, given a (regex) value expression and return both the IDs and text.
// For example: '^H.llo' will match any string starting with 'Hello' or 'Hallo'.
func (slghtrBtch *SlaughterBatchAsset) searchSlaughterBatchsByID(stub shim.ChaincodeStubInterface, args []string) peer.Response {

	searchString := strings.Replace(args[0], "\"", ".", -1) // protect against SQL injection

	// stub.GetQueryResult takes a verbatim CouchDB (assuming this is used DB). See CouchDB documentation:
	//     http://docs.couchdb.org/en/2.0.0/api/database/find.html
	// For example:
	//	{
	//		"selector": {
	//			"value": {"$regex": %s"}
	//		},
	//		"fields": ["ID","value"],
	//		"limit":  99
	//	}
	queryString := fmt.Sprintf("{\"selector\": {\"ID\": {\"$regex\": \"%s\"}}, \"fields\": [\"ID\", \"docType\", \"status\", \"creationDate\", \"hatcheryFarmID\":\"hatcheryFarmID001\", \"growerFarmID\":\"growerFarmID001\", \"henHouseID\":\"henHouseID001\", \"chickenLotID\":\"chickenLotID001\", \"husbandryLineageID\":\"husbandryLineageID001\", \"feedMixID\":\"feedMixID001\", \"feedFormulaID\":\"feedFormulaID001\", \"feedStopDate\":\"feedStopDate001\", \"slaughterDate\":\"slaughterDate001\"], \"limit\":99, \"execution_stats\": true}", strings.Replace(searchString, "\"", ".", -1))
	resultsIterator, err := stub.GetQueryResult(queryString)
	if err != nil {
		return Error(http.StatusInternalServerError, err.Error())
	}
	defer resultsIterator.Close()

	// Write return buffer
	var buffer bytes.Buffer
	buffer.WriteString("{ \"values\": [")
	for resultsIterator.HasNext() {
		it, _ := resultsIterator.Next()
		if buffer.Len() > 15 {
			buffer.WriteString(",")
		}
		buffer.WriteString("{\"ID\":\"")
		buffer.WriteString(it.Key)
		buffer.WriteString("\",\"slaughterBatch\": ")
		buffer.WriteString(string(it.Value))
		buffer.WriteString("}")
	}
	buffer.WriteString("]}")
	return Success(http.StatusOK, "OK", buffer.Bytes())
}

//Helper: Save SlaughterBatchAsset
func (slghtrBtch *SlaughterBatchAsset) saveSlaughterBatch(stub shim.ChaincodeStubInterface, slaughterBatch SlaughterBatch) (bool, error) {
	bytes, err := json.Marshal(slaughterBatch)
	if err != nil {
		return false, errors.New("Error converting slaughterBatch record JSON")
	}
	err = stub.PutState(slaughterBatch.ID, bytes)
	if err != nil {
		return false, errors.New("Error storing SlaughterBatch record")
	}
	return true, nil
}

//Helper: delete SlaughterBatchAsset
func (slghtrBtch *SlaughterBatchAsset) deleteSlaughterBatch(stub shim.ChaincodeStubInterface, slaughterBatchID string) (bool, error) {
	_, err := slghtrBtch.retrieveSlaughterBatch(stub, slaughterBatchID)
	if err != nil {
		return false, errors.New("SlaughterBatch with ID: " + slaughterBatchID + " not found")
	}
	err = stub.DelState(slaughterBatchID)
	if err != nil {
		return false, errors.New("Error deleting SlaughterBatch record")
	}
	return true, nil
}

//Helper: Update slaughterBatch Holder - updates Index
func (slghtrBtch *SlaughterBatchAsset) updateSlaughterBatchIDIndex(stub shim.ChaincodeStubInterface, slaughterBatch SlaughterBatch) (bool, error) {
	var slaughterBatchIDs SlaughterBatchIDIndex
	bytes, err := stub.GetState("slaughterBatchIDIndex")
	if err != nil {
		return false, errors.New("updateSlaughterBatchIDIndex: Error getting slaughterBatchIDIndex array Index from state")
	}
	err = json.Unmarshal(bytes, &slaughterBatchIDs)
	if err != nil {
		return false, errors.New("updateSlaughterBatchIDIndex: Error unmarshalling slaughterBatchIDIndex array JSON")
	}
	slaughterBatchIDs.IDs = append(slaughterBatchIDs.IDs, slaughterBatch.ID)
	bytes, err = json.Marshal(slaughterBatchIDs)
	if err != nil {
		return false, errors.New("updateSlaughterBatchIDIndex: Error marshalling new slaughterBatch ID")
	}
	err = stub.PutState("slaughterBatchIDIndex", bytes)
	if err != nil {
		return false, errors.New("updateSlaughterBatchIDIndex: Error storing new slaughterBatch ID in slaughterBatchIDIndex (Index)")
	}
	return true, nil
}

//Helper: delete ID from slaughterBatchStruct Holder
func (slghtrBtch *SlaughterBatchAsset) deleteSlaughterBatchIDIndex(stub shim.ChaincodeStubInterface, slaughterBatchID string) (bool, error) {
	var slaughterBatchIDIndex SlaughterBatchIDIndex
	bytes, err := stub.GetState("slaughterBatchIDIndex")
	if err != nil {
		return false, errors.New("deleteSlaughterBatchIDIndex: Error getting slaughterBatchIDIndex array Index from state")
	}
	err = json.Unmarshal(bytes, &slaughterBatchIDIndex)
	if err != nil {
		return false, errors.New("deleteSlaughterBatchIDIndex: Error unmarshalling slaughterBatchIDIndex array JSON")
	}
	slaughterBatchIDIndex.IDs, err = deleteKeyFromStringArray(slaughterBatchIDIndex.IDs, slaughterBatchID)
	if err != nil {
		return false, errors.New(err.Error())
	}
	bytes, err = json.Marshal(slaughterBatchIDIndex)
	if err != nil {
		return false, errors.New("deleteSlaughterBatchIDIndex: Error marshalling new slaughterBatchStruct ID")
	}
	err = stub.PutState("slaughterBatchIDIndex", bytes)
	if err != nil {
		return false, errors.New("deleteSlaughterBatchIDIndex: Error storing new slaughterBatchStruct ID in slaughterBatchIDIndex (Index)")
	}
	return true, nil
}

//Helper: Initialize truck ID Holder //change template
func (slghtrBtch *SlaughterBatchAsset) initHolder(stub shim.ChaincodeStubInterface) bool {
	var slaughterBatchIDIndex SlaughterBatchIDIndex
	bytes, _ := json.Marshal(slaughterBatchIDIndex)
	stub.DelState("slaughterBatchIDIndex")
	stub.PutState("slaughterBatchIDIndex", bytes)
	return true
}

//deleteKeyFromArray
func deleteKeyFromStringArray(array []string, key string) (newArray []string, err error) {
	for _, entry := range array {
		if entry != key {
			newArray = append(newArray, entry)
		}
	}
	if len(newArray) == len(array) {
		return newArray, errors.New("Specified Key: " + key + " not found in Array")
	}
	return newArray, nil
}

//Helper: Retrieve
func (slghtrBtch *SlaughterBatchAsset) retrieveSlaughterBatch(stub shim.ChaincodeStubInterface, slaughterBatchID string) ([]byte, error) {
	var slaughterBatch SlaughterBatch
	var slaughterBatchAsByteArray []byte
	bytes, err := stub.GetState(slaughterBatchID)
	if err != nil {
		return slaughterBatchAsByteArray, errors.New("retrieveSlaughterBatch: Error retrieving slaughterBatch with ID: " + slaughterBatchID)
	}
	err = json.Unmarshal(bytes, &slaughterBatch)
	if err != nil {
		return slaughterBatchAsByteArray, errors.New("retrieveSlaughterBatch: Corrupt slaughterBatch record " + string(bytes))
	}
	slaughterBatchAsByteArray, err = json.Marshal(slaughterBatch)
	if err != nil {
		return slaughterBatchAsByteArray, errors.New("readSlaughterBatch: Invalid slaughterBatch Object - Not a  valid JSON")
	}
	return slaughterBatchAsByteArray, nil
}

//getSlaughterBatchFromArgs - construct a slaughterBatch structure from string array of arguments
func getSlaughterBatchFromArgs(args []string) (slaughterBatch SlaughterBatch, err error) {
	if !gjson.Valid(args[0]) {
		return slaughterBatch, errors.New("Invalid json")
	}

	err = gjson.Unmarshal([]byte(args[0]), &slaughterBatch)
	if err != nil {
		return slaughterBatch, err
	}
	return slaughterBatch, nil
}
