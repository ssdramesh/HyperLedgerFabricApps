package main

//Template Version: 1.1-20180704
import (
	"bytes"
	"encoding/json"
	"fmt"
	"strings"
	"testing"

	"github.com/hyperledger/fabric/core/chaincode/shim"
)

// History/Search
// ... the History and Search API calls are not supported in the mock interface

//TestLabSampleAsset_Init
func TestLabSampleAsset_Init(t *testing.T) {
	labSample := new(LabSampleAsset)
	stub := shim.NewMockStub("labSample", labSample)
	checkInit(t, stub, [][]byte{[]byte("init")})
	checkState(t, stub, "labSampleIDIndex", getExpectedLabSampleIDIndex("init"))
}

//TestLabSampleAsset_InvokeUnknownFunction
func TestLabSampleAsset_InvokeUnknownFunction(t *testing.T) {
	labSample := new(LabSampleAsset)
	stub := shim.NewMockStub("labSample", labSample)
	checkInit(t, stub, [][]byte{[]byte("init")})
	checkInvokeUnknownFunction(t, stub, [][]byte{[]byte("myFunction"), []byte("docType:Asset")})
}

//TestLabSampleAsset_Invoke_addNewLabSample
func TestLabSampleAsset_Invoke_addNewLabSampleOK(t *testing.T) {
	labSample := new(LabSampleAsset)
	stub := shim.NewMockStub("labSample", labSample)
	checkInit(t, stub, [][]byte{[]byte("init")})
	checkInvoke(t, stub, getFirstLabSampleAssetForTesting())
	newLabSampleID := "100001"
	checkState(t, stub, newLabSampleID, getNewLabSampleExpected())
	checkState(t, stub, "labSampleIDIndex", getExpectedLabSampleIDIndex("addNewLabSample"))
}

//TestLabSampleAsset_Invoke_addNewLabSample
func TestLabSampleAsset_Invoke_addNewLabSampleDuplicate(t *testing.T) {
	labSample := new(LabSampleAsset)
	stub := shim.NewMockStub("labSample", labSample)
	checkInit(t, stub, [][]byte{[]byte("init")})
	checkInvoke(t, stub, getFirstLabSampleAssetForTesting())
	newLabSampleID := "100001"
	checkState(t, stub, newLabSampleID, getNewLabSampleExpected())
	checkState(t, stub, "labSampleIDIndex", getExpectedLabSampleIDIndex("addNewLabSample"))
	res := stub.MockInvoke("1", getFirstLabSampleAssetForTesting())
	if res.Status != shim.OK {
		checkError(t, "This LabSample already exists: 100001", res.Message)
	} else {
		fmt.Println("Error was expected, but not raised")
		t.FailNow()
	}
}

func TestLabSampleAsset_Invoke_updateLabSampleOK(t *testing.T) {
	labSample := new(LabSampleAsset)
	stub := shim.NewMockStub("labSample", labSample)
	checkInit(t, stub, [][]byte{[]byte("init")})
	checkInvoke(t, stub, getFirstLabSampleAssetForTesting())
	newLabSampleID := "100001"
	checkState(t, stub, newLabSampleID, getNewLabSampleExpected())
	checkInvoke(t, stub, getFirstLabSampleAssetForUpdateTestingOK())
	checkReadLabSampleAssetAfterUpdateOK(t, stub, newLabSampleID)
}

//TestLabSampleAsset_Invoke_removeLabSampleOK  //change template
func TestLabSampleAsset_Invoke_removeLabSampleOK(t *testing.T) {
	labSample := new(LabSampleAsset)
	stub := shim.NewMockStub("labSample", labSample)
	checkInit(t, stub, [][]byte{[]byte("init")})
	checkInvoke(t, stub, getFirstLabSampleAssetForTesting())
	checkInvoke(t, stub, getSecondLabSampleAssetForTesting())
	checkReadAllLabSamplesOK(t, stub)
	checkState(t, stub, "labSampleIDIndex", getExpectedLabSampleIDIndex("beforeRemoveLabSample"))
	checkInvoke(t, stub, getRemoveSecondLabSampleAssetForTesting())
	remainingLabSampleID := "100001"
	checkReadLabSampleOK(t, stub, remainingLabSampleID)
	checkState(t, stub, "labSampleIDIndex", getExpectedLabSampleIDIndex("afterRemoveLabSample"))
}

//TestLabSampleAsset_Invoke_removeLabSampleNOK  //change template
func TestLabSampleAsset_Invoke_removeLabSampleNOK(t *testing.T) {
	labSample := new(LabSampleAsset)
	stub := shim.NewMockStub("labSample", labSample)
	checkInit(t, stub, [][]byte{[]byte("init")})
	checkInvoke(t, stub, getFirstLabSampleAssetForTesting())
	firstLabSampleID := "100001"
	checkReadLabSampleOK(t, stub, firstLabSampleID)
	checkState(t, stub, "labSampleIDIndex", getExpectedLabSampleIDIndex("addNewLabSample"))
	res := stub.MockInvoke("1", getRemoveSecondLabSampleAssetForTesting())
	if res.Status != shim.OK {
		checkError(t, "LabSample with ID: "+"100002"+" not found", res.Message)
	} else {
		fmt.Println("Error was expected, but not raised")
		t.FailNow()
	}
	checkState(t, stub, "labSampleIDIndex", getExpectedLabSampleIDIndex("addNewLabSample"))
}

//TestLabSampleAsset_Invoke_removeAllLabSamplesOK  //change template
func TestLabSampleAsset_Invoke_removeAllLabSamplesOK(t *testing.T) {
	labSample := new(LabSampleAsset)
	stub := shim.NewMockStub("labSample", labSample)
	checkInit(t, stub, [][]byte{[]byte("init")})
	checkInvoke(t, stub, getFirstLabSampleAssetForTesting())
	checkInvoke(t, stub, getSecondLabSampleAssetForTesting())
	checkReadAllLabSamplesOK(t, stub)
	checkState(t, stub, "labSampleIDIndex", getExpectedLabSampleIDIndex("beforeRemoveLabSample"))
	checkInvoke(t, stub, getRemoveAllLabSampleAssetsForTesting())
	checkState(t, stub, "labSampleIDIndex", getExpectedLabSampleIDIndex(""))
}

//TestLabSampleAsset_Invoke_removeLabSampleNOK  //change template
func TestLabSampleAsset_Invoke_removeAllLabSamplesNOK(t *testing.T) {
	labSample := new(LabSampleAsset)
	stub := shim.NewMockStub("labSample", labSample)
	checkInit(t, stub, [][]byte{[]byte("init")})
	res := stub.MockInvoke("1", getRemoveAllLabSampleAssetsForTesting())
	if res.Status != shim.OK {
		checkError(t, "removeAllLabSamples: No labSamples to remove", res.Message)
	} else {
		fmt.Println("Error was expected, but not raised")
		t.FailNow()
	}
	checkState(t, stub, "labSampleIDIndex", getExpectedLabSampleIDIndex(""))
}

//TestLabSampleAsset_Query_readLabSample
func TestLabSampleAsset_Query_readLabSample(t *testing.T) {
	labSample := new(LabSampleAsset)
	stub := shim.NewMockStub("labSample", labSample)
	checkInit(t, stub, [][]byte{[]byte("init")})
	labSampleID := "100001"
	checkInvoke(t, stub, getFirstLabSampleAssetForTesting())
	checkReadLabSampleOK(t, stub, labSampleID)
	checkReadLabSampleNOK(t, stub, "")
}

//TestLabSampleAsset_Query_readAllLabSamples
func TestLabSampleAsset_Query_readAllLabSamples(t *testing.T) {
	labSample := new(LabSampleAsset)
	stub := shim.NewMockStub("labSample", labSample)
	checkInit(t, stub, [][]byte{[]byte("init")})
	checkInvoke(t, stub, getFirstLabSampleAssetForTesting())
	checkInvoke(t, stub, getSecondLabSampleAssetForTesting())
	checkReadAllLabSamplesOK(t, stub)
}

/*
*
*	Helper Functions
*
 */
//Get first LabSampleAsset for testing
func getFirstLabSampleAssetForTesting() [][]byte {
	return [][]byte{[]byte("addNewLabSample"),
		[]byte("{\"ID\":\"100001\",\"docType\":\"Asset.LabSampleAsset\",\"status\":\"0\",\"creationDate\":\"12/01/2018\", \"labID\":\"labID001\", \"slaughterBatchID\":\"slaughterBatchID001\", \"type\":\"type001\", \"collectionDate\":\"collectionDate001\", \"submissionDate\":\"submissionDate001\"}")}
}

//Get first LabSampleAsset for update testing
func getFirstLabSampleAssetForUpdateTestingOK() [][]byte {
	return [][]byte{[]byte("updateLabSample"),
		[]byte("{\"ID\":\"100001\",\"docType\":\"Asset.LabSampleAsset\",\"status\":\"1\",\"creationDate\":\"12/01/2018\", \"labID\":\"labID001\", \"slaughterBatchID\":\"slaughterBatchID001\", \"type\":\"type001\", \"collectionDate\":\"collectionDate001\", \"submissionDate\":\"submissionDate001\"}")}
}

//Get second LabSampleAsset for testing
func getSecondLabSampleAssetForTesting() [][]byte {
	return [][]byte{[]byte("addNewLabSample"),
		[]byte("{\"ID\":\"100002\",\"docType\":\"Asset.LabSampleAsset\",\"status\":\"0\",\"creationDate\":\"12/01/2018\", \"labID\":\"labID002\", \"slaughterBatchID\":\"slaughterBatchID002\", \"type\":\"type002\", \"collectionDate\":\"collectionDate002\", \"submissionDate\":\"submissionDate002\"}")}
}

//Get remove second LabSampleAsset for testing //change template
func getRemoveSecondLabSampleAssetForTesting() [][]byte {
	return [][]byte{[]byte("removeLabSample"),
		[]byte("100002")}
}

//Get remove all LabSampleAssets for testing //change template
func getRemoveAllLabSampleAssetsForTesting() [][]byte {
	return [][]byte{[]byte("removeAllLabSamples")}
}

//Get an expected value for testing
func getNewLabSampleExpected() []byte {
	var labSample LabSample
	labSample.ID = "100001"
	labSample.ObjectType = "Asset.LabSampleAsset"
	labSample.Status = "0"
	labSample.CreationDate = "12/01/2018"
	labSample.LabID = "labID001"
	labSample.SlaughterBatchID = "slaughterBatchID001"
	labSample.Type = "type001"
	labSample.CollectionDate = "collectionDate001"
	labSample.SubmissionDate = "submissionDate001"
	labSampleJSON, err := json.Marshal(labSample)
	if err != nil {
		fmt.Println("Error converting a LabSample record to JSON")
		return nil
	}
	return []byte(labSampleJSON)
}

//Get an expected value for testing
func getUpdatedLabSampleExpected() []byte {
	var labSample LabSample
	labSample.ID = "100001"
	labSample.ObjectType = "Asset.LabSampleAsset"
	labSample.Status = "1"
	labSample.CreationDate = "12/01/2018"
	labSample.LabID = "labID001"
	labSample.SlaughterBatchID = "slaughterBatchID001"
	labSample.Type = "type001"
	labSample.CollectionDate = "collectionDate001"
	labSample.SubmissionDate = "submissionDate001"
	labSample.LabID = "labID001"
	labSample.SlaughterBatchID = "slaughterBatchID001"
	labSample.Type = "type001"
	labSample.CollectionDate = "collectionDate001"
	labSample.SubmissionDate = "submissionDate001"
	labSampleJSON, err := json.Marshal(labSample)
	if err != nil {
		fmt.Println("Error converting a LabSample record to JSON")
		return nil
	}
	return []byte(labSampleJSON)
}

//Get expected values of LabSamples for testing
func getExpectedLabSamples() []byte {
	var labSamples []LabSample
	var labSample LabSample
	labSample.ID = "100001"
	labSample.ObjectType = "Asset.LabSampleAsset"
	labSample.Status = "0"
	labSample.CreationDate = "12/01/2018"
	labSample.LabID = "labID001"
	labSample.SlaughterBatchID = "slaughterBatchID001"
	labSample.Type = "type001"
	labSample.CollectionDate = "collectionDate001"
	labSample.SubmissionDate = "submissionDate001"
	labSamples = append(labSamples, labSample)
	labSample.ID = "100002"
	labSample.ObjectType = "Asset.LabSampleAsset"
	labSample.Status = "0"
	labSample.CreationDate = "12/01/2018"
	labSample.LabID = "labID002"
	labSample.SlaughterBatchID = "slaughterBatchID002"
	labSample.Type = "type002"
	labSample.CollectionDate = "collectionDate002"
	labSample.SubmissionDate = "submissionDate002"
	labSamples = append(labSamples, labSample)
	labSampleJSON, err := json.Marshal(labSamples)
	if err != nil {
		fmt.Println("Error converting labSample records to JSON")
		return nil
	}
	return []byte(labSampleJSON)
}

func getExpectedLabSampleIDIndex(funcName string) []byte {
	var labSampleIDIndex LabSampleIDIndex
	switch funcName {
	case "addNewLabSample":
		labSampleIDIndex.IDs = append(labSampleIDIndex.IDs, "100001")
		labSampleIDIndexBytes, err := json.Marshal(labSampleIDIndex)
		if err != nil {
			fmt.Println("Error converting LabSampleIDIndex to JSON")
			return nil
		}
		return labSampleIDIndexBytes
	case "beforeRemoveLabSample":
		labSampleIDIndex.IDs = append(labSampleIDIndex.IDs, "100001")
		labSampleIDIndex.IDs = append(labSampleIDIndex.IDs, "100002")
		labSampleIDIndexBytes, err := json.Marshal(labSampleIDIndex)
		if err != nil {
			fmt.Println("Error converting LabSampleIDIndex to JSON")
			return nil
		}
		return labSampleIDIndexBytes
	case "afterRemoveLabSample":
		labSampleIDIndex.IDs = append(labSampleIDIndex.IDs, "100001")
		labSampleIDIndexBytes, err := json.Marshal(labSampleIDIndex)
		if err != nil {
			fmt.Println("Error converting LabSampleIDIndex to JSON")
			return nil
		}
		return labSampleIDIndexBytes
	default:
		labSampleIDIndexBytes, err := json.Marshal(labSampleIDIndex)
		if err != nil {
			fmt.Println("Error converting LabSampleIDIndex to JSON")
			return nil
		}
		return labSampleIDIndexBytes
	}
}

//checkInit - helper to check the Initialization of chaincode: LabSampleAsset
func checkInit(t *testing.T, stub *shim.MockStub, args [][]byte) {
	res := stub.MockInit("1", args)
	if res.Status != shim.OK {
		fmt.Println("Init failed", string(res.Message))
		t.FailNow()
	}
}

//checkState - helper for checking the chaincode state for a given stateKey afgainst an expected value
func checkState(t *testing.T, stub *shim.MockStub, stateKey string, expectedState []byte) {
	actualState := stub.State[stateKey]
	if actualState == nil {
		fmt.Println("State for ", stateKey, ": failed to get value")
		t.FailNow()
	}

	if bytes.Compare(actualState, expectedState) != 0 {
		fmt.Println("Incorrect State for " + stateKey + ": \nExpected: " + string(expectedState) + "\nActual  : " + string(actualState))
		t.FailNow()
	}
}

//checkInvoke - helper for checking Invoke of chaincode
func checkInvoke(t *testing.T, stub *shim.MockStub, args [][]byte) {
	res := stub.MockInvoke("1", args)
	if res.Status != shim.OK {
		fmt.Println("Invoke", args, "failed", string(res.Message))
		t.FailNow()
	}
}

func checkInvokeUnknownFunction(t *testing.T, stub *shim.MockStub, args [][]byte) {
	res := stub.MockInvoke("1", args)
	if res.Status != shim.OK {
		expectedErr := "Received unknown function invocation"
		actualErr := string(res.Message)
		if !(strings.Contains(actualErr, expectedErr)) {
			fmt.Println("Invoke with unknown function must throw error: ", expectedErr, "\n Actual Error :", actualErr)
			t.FailNow()
		}
	} else {
		t.FailNow()
	}
}

//checkReadLabSampleOK - helper for positive test readLabSample
func checkReadLabSampleOK(t *testing.T, stub *shim.MockStub, labSampleID string) {
	res := stub.MockInvoke("1", [][]byte{[]byte("readLabSample"), []byte(labSampleID)})
	if res.Status != shim.OK {
		fmt.Println("func readLabSample with ID: ", labSampleID, " failed"+string(res.Message))
		t.FailNow()
	}
	if res.Payload == nil {
		fmt.Println("func readLabSample with ID: ", labSampleID, "failed to get value")
		t.FailNow()
	}
	if bytes.Compare(getNewLabSampleExpected(), []byte(res.Payload)) != 0 {
		fmt.Println("func readLabSample with ID: ", labSampleID, "Expected:", string(getNewLabSampleExpected()), "Actual:", string(res.Payload))
		t.FailNow()
	}
}

//checkReadLabSampleNOK - helper for negative testing of readLabSample
func checkReadLabSampleNOK(t *testing.T, stub *shim.MockStub, labSampleID string) {
	//with no labSampleID
	res := stub.MockInvoke("1", [][]byte{[]byte("readLabSample"), []byte("")})
	if res.Status != shim.OK {
		expectedErr := "retrieveLabSample: Corrupt labSample record "
		actualErr := string(res.Message)
		if !(strings.Contains(actualErr, expectedErr)) {
			fmt.Println("func readLabSample negative test: ", "Expected Error:", expectedErr, "Actual Error", actualErr)
			t.FailNow()
		}
	} else {
		t.FailNow()
	}
}

//checkReadLabSampleAssetAfterUpdateOK - helper for positive test readLabSample after update
func checkReadLabSampleAssetAfterUpdateOK(t *testing.T, stub *shim.MockStub, LabSampleID string) {
	res := stub.MockInvoke("1", [][]byte{[]byte("readLabSample"), []byte(LabSampleID)})
	if res.Status != shim.OK {
		fmt.Println("func readLabSample with ID: ", LabSampleID, " failed"+string(res.Message))
		t.FailNow()
	}
	if res.Payload == nil {
		fmt.Println("func readLabSample with ID: ", LabSampleID, "failed to get value")
		t.FailNow()
	}
	if bytes.Compare(getUpdatedLabSampleExpected(), []byte(res.Payload)) != 0 {
		fmt.Println("func readLabSample with ID: ", LabSampleID, "Expected:", string(getUpdatedLabSampleExpected()), "Actual:", string(res.Payload))
		t.FailNow()
	}
}

func checkReadAllLabSamplesOK(t *testing.T, stub *shim.MockStub) {
	res := stub.MockInvoke("1", [][]byte{[]byte("readAllLabSamples")})
	if res.Status != shim.OK {
		fmt.Println("func readAllLabSamples failed", string(res.Message))
		t.FailNow()
	}
	if res.Payload == nil {
		fmt.Println("func readAllLabSamples failed to get value")
		t.FailNow()
	}
	if bytes.Compare(getExpectedLabSamples(), []byte(res.Payload)) != 0 {
		fmt.Println("func readAllLabSamples Expected:\n", string(getExpectedLabSamples()), "\nActual:\n", string(res.Payload))
		t.FailNow()
	}
}

func checkError(t *testing.T, exp string, act string) {
	if strings.Compare(exp, act) != 0 {
		fmt.Println("Unexpected Error! Expecting ", exp, "\n Actual :", act)
		t.FailNow()
	}
}
