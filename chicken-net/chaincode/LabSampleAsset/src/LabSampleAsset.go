package main

/*
Template Version: 1.2-20180921
Template Owner: Ramesh Suraparaju (ramesh.suraparaju@sap.com)
This template is used to generate the chaincode for an asset that implements the hyperledger fabric shim
*/
import (
	"bytes"
	"encoding/json"
	"errors"
	"fmt"
	"net/http"
	"strings"
	"time"

	"github.com/hyperledger/fabric/core/chaincode/shim"
	"github.com/hyperledger/fabric/protos/peer"
	"github.com/tidwall/gjson"
)

var logger = shim.NewLogger("CLDChaincode")

//LabSampleAssetStatus - enumeration for labSampleAsset statuses (Same status enumeration for LabSampleAsset)
type LabSampleAssetStatus int

const (
	Submitted       LabSampleAssetStatus = 1 + iota //Submitted: LabSample submitted to Lab
	InProcess                                       //Accepted: LabSample accepted by Lab
	ResultSubmitted                                 //ResultSubmitted: Result submitted for approval
	ResultApproved                                  //ResultApproved: Result approved
)

var labSampleAssetStatuses = [...]string{
	"Submitted",
	"Accepted",
	"Result Submitted",
	"Result Approved",
}

func (labSampleAssetStatus LabSampleAssetStatus) labSampleAssetStatusToString() string {
	return labSampleAssetStatuses[labSampleAssetStatus-1]
}

//LabSampleType - enumeration for labSampleType
type LabSampleType int

const (
	Blood  LabSampleType = 1 + iota //Blood: Blood Sample
	Swab                            //Swab: Swab Sample
	Manure                          //Manure: Manure Sample
	Whole                           //Whole: Whole Sample
)

var labSampleTypes = [...]string{
	"Blood",
	"Swab",
	"Manure",
	"Whole",
}

func (labSampleType LabSampleType) labSampleTypeToString() string {
	return labSampleTypes[labSampleType-1]
}

//LabSampleAsset - Chaincode for asset LabSample
type LabSampleAsset struct {
}

//LabSample - Details of the asset type LabSample
type LabSample struct {
	ID               string `json:"ID"`
	ObjectType       string `json:"docType"`
	Status           string `json:"status"`
	CreationDate     string `json:"creationDate"`
	LabID            string `json:"labID"`
	SlaughterBatchID string `json:"slaughterBatchID"`
	Type             string `json:"type"`
	CollectionDate   string `json:"collectionDate"`
	SubmissionDate   string `json:"submissionDate"`
}

//LabSampleIDIndex - Index on IDs for retrieval all LabSamples
type LabSampleIDIndex struct {
	IDs []string `json:"IDs"`
}

//Success - Success Message
func Success(rc int32, doc string, payload []byte) peer.Response {
	logger.Infof("Success %d = %s", rc, doc, payload)
	return peer.Response{
		Status:  rc,
		Message: doc,
		Payload: payload,
	}
}

//Error - Error Message
func Error(rc int32, doc string) peer.Response {
	logger.Errorf("Error %d = %s", rc, doc)
	return peer.Response{
		Status:  rc,
		Message: doc,
	}
}

func main() {
	err := shim.Start(new(LabSampleAsset))
	if err != nil {
		fmt.Printf("Error starting LabSampleAsset chaincode function main(): %s", err)
	} else {
		fmt.Printf("Starting LabSampleAsset chaincode function main() executed successfully")
	}
}

//Init - The chaincode Init function: No  arguments, only initializes a ID array as Index for retrieval of all LabSamples
func (lbSmpl *LabSampleAsset) Init(stub shim.ChaincodeStubInterface) peer.Response {
	var labSampleIDIndex LabSampleIDIndex
	record, _ := stub.GetState("labSampleIDIndex")
	//chaincode upgrade: Index might already exist, create new only if it does not
	if record == nil {
		bytes, _ := json.Marshal(labSampleIDIndex)
		stub.PutState("labSampleIDIndex", bytes)
	}
	return Success(http.StatusOK, "labSampleIDIndex initiated successfully", nil)
}

//Invoke - The chaincode Invoke function:
func (lbSmpl *LabSampleAsset) Invoke(stub shim.ChaincodeStubInterface) peer.Response {
	function, args := stub.GetFunctionAndParameters()
	logger.Debug("function: ", function)
	switch function {
	case "addNewLabSample":
		return lbSmpl.addNewLabSample(stub, args)
	case "updateLabSample":
		return lbSmpl.updateLabSample(stub, args)
	case "removeLabSample":
		return lbSmpl.removeLabSample(stub, args[0])
	case "removeAllLabSamples":
		return lbSmpl.removeAllLabSamples(stub)
	case "readLabSample":
		return lbSmpl.readLabSample(stub, args[0])
	case "readAllLabSamples":
		return lbSmpl.readAllLabSamples(stub)
	case "searchLabSamplesByID":
		return lbSmpl.searchLabSamplesByID(stub, args)
	case "getLabSampleHistory":
		return lbSmpl.getLabSampleHistory(stub, args)
	default:
		return Error(http.StatusBadRequest, "Received unknown function invocation")
	}
}

//Invoke Route: addNewLabSample
func (lbSmpl *LabSampleAsset) addNewLabSample(stub shim.ChaincodeStubInterface, args []string) peer.Response {
	labSample, err := getLabSampleFromArgs(args)
	if err != nil {
		return Error(http.StatusBadRequest, "LabSample Data is Corrupted")
	}
	labSample.ObjectType = "Asset.LabSampleAsset"
	record, err := stub.GetState(labSample.ID)
	if record != nil {
		return Error(http.StatusBadRequest, "This LabSample already exists: "+labSample.ID)
	}
	_, err = lbSmpl.saveLabSample(stub, labSample)
	if err != nil {
		return Error(http.StatusInternalServerError, err.Error())
	}
	_, err = lbSmpl.updateLabSampleIDIndex(stub, labSample)
	if err != nil {
		return Error(http.StatusInternalServerError, err.Error())
	}
	return Success(http.StatusOK, "addNewLabSample: successfully added: "+labSample.ID, nil)
}

//Invoke Route: updateLabSample
func (lbSmpl *LabSampleAsset) updateLabSample(stub shim.ChaincodeStubInterface, args []string) peer.Response {
	input, err := getLabSampleFromArgs(args)
	if err != nil {
		return Error(http.StatusBadRequest, "LabSample Data is Corrupted")
	}
	record, err := stub.GetState(input.ID)
	if record == nil {
		return Error(http.StatusBadRequest, "This LabSample does not exist: "+input.ID)
	}
	_, err = lbSmpl.saveLabSample(stub, input)
	if err != nil {
		return Error(http.StatusInternalServerError, err.Error())
	}
	return Success(http.StatusOK, "updateLabSample: successfully updated: "+input.ID, nil)
}

//Invoke Route: removeLabSample
func (lbSmpl *LabSampleAsset) removeLabSample(stub shim.ChaincodeStubInterface, labSampleID string) peer.Response {
	_, err := lbSmpl.deleteLabSample(stub, labSampleID)
	if err != nil {
		return Error(http.StatusInternalServerError, err.Error())
	}
	_, err = lbSmpl.deleteLabSampleIDIndex(stub, labSampleID)
	if err != nil {
		return Error(http.StatusInternalServerError, err.Error())
	}
	return Success(http.StatusOK, "removeLabSample: successfully removed: "+labSampleID, nil)
}

//Invoke Route: removeAllLabSamples
func (lbSmpl *LabSampleAsset) removeAllLabSamples(stub shim.ChaincodeStubInterface) peer.Response {
	var labSampleIDIndex LabSampleIDIndex
	bytes, err := stub.GetState("labSampleIDIndex")
	if err != nil {
		return Error(http.StatusInternalServerError, "removeAllLabSamples: Error getting labSampleIDIndex array")
	}
	err = json.Unmarshal(bytes, &labSampleIDIndex)
	if err != nil {
		return Error(http.StatusInternalServerError, "removeAllLabSamples: Error unmarshalling labSampleIDIndex array JSON")
	}
	if len(labSampleIDIndex.IDs) == 0 {
		return Error(http.StatusInternalServerError, "removeAllLabSamples: No labSamples to remove")
	}
	for _, labSampleStructID := range labSampleIDIndex.IDs {
		_, err = lbSmpl.deleteLabSample(stub, labSampleStructID)
		if err != nil {
			return Error(http.StatusInternalServerError, "Failed to remove LabSample with ID: "+labSampleStructID)
		}
		_, err = lbSmpl.deleteLabSampleIDIndex(stub, labSampleStructID)
		if err != nil {
			return Error(http.StatusInternalServerError, err.Error())
		}
	}
	lbSmpl.initHolder(stub)
	return Success(http.StatusOK, "removeAllLabSamples: Successfully removed all LabSamples", nil)
}

//Query Route: readLabSample
func (lbSmpl *LabSampleAsset) readLabSample(stub shim.ChaincodeStubInterface, labSampleID string) peer.Response {
	labSampleAsByteArray, err := lbSmpl.retrieveLabSample(stub, labSampleID)
	if err != nil {
		return Error(http.StatusInternalServerError, err.Error())
	}
	return Success(http.StatusOK, "readLabSample: Successfully read LabSample with ID: "+labSampleID, labSampleAsByteArray)
}

//Query Route: readAllLabSamples
func (lbSmpl *LabSampleAsset) readAllLabSamples(stub shim.ChaincodeStubInterface) peer.Response {
	var labSampleIDs LabSampleIDIndex
	bytes, err := stub.GetState("labSampleIDIndex")
	if err != nil {
		return Error(http.StatusInternalServerError, "readAllLabSamples: Error getting labSampleIDIndex array")
	}
	err = json.Unmarshal(bytes, &labSampleIDs)
	if err != nil {
		return Error(http.StatusInternalServerError, "readAllLabSamples: Error unmarshalling labSampleIDIndex array JSON")
	}
	result := "["

	var labSampleAsByteArray []byte

	for _, labSampleID := range labSampleIDs.IDs {
		labSampleAsByteArray, err = lbSmpl.retrieveLabSample(stub, labSampleID)
		if err != nil {
			return Error(http.StatusNotFound, "Failed to retrieve labSample with ID: "+labSampleID)
		}
		result += string(labSampleAsByteArray) + ","
	}
	if len(result) == 1 {
		result = "[]"
	} else {
		result = result[:len(result)-1] + "]"
	}
	return Success(http.StatusOK, "OK", []byte(result))
}

//GetLabSampleHistory - reads history of a labSample with specified ID
func (lbSmpl *LabSampleAsset) getLabSampleHistory(stub shim.ChaincodeStubInterface, args []string) peer.Response {

	resultsIterator, err := stub.GetHistoryForKey(args[0])
	if err != nil {
		return Error(http.StatusNotFound, "Not Found")
	}
	defer resultsIterator.Close()
	// Write return buffer
	var buffer bytes.Buffer
	buffer.WriteString("{ \"values\": [")
	for resultsIterator.HasNext() {
		it, _ := resultsIterator.Next()
		if buffer.Len() > 15 {
			buffer.WriteString(",")
		}
		buffer.WriteString("{\"timestamp\":\"")
		buffer.WriteString(time.Unix(it.Timestamp.Seconds, int64(it.Timestamp.Nanos)).Format(time.Stamp))
		buffer.WriteString("\",\"labSample\": ")
		buffer.WriteString(string(it.Value))
		buffer.WriteString("}")
	}
	buffer.WriteString("]}")
	return Success(http.StatusOK, "OK", buffer.Bytes())
}

// Search for all matching IDs, given a (regex) value expression and return both the IDs and text.
// For example: '^H.llo' will match any string starting with 'Hello' or 'Hallo'.
func (lbSmpl *LabSampleAsset) searchLabSamplesByID(stub shim.ChaincodeStubInterface, args []string) peer.Response {

	searchString := strings.Replace(args[0], "\"", ".", -1) // protect against SQL injection

	// stub.GetQueryResult takes a verbatim CouchDB (assuming this is used DB). See CouchDB documentation:
	//     http://docs.couchdb.org/en/2.0.0/api/database/find.html
	// For example:
	//	{
	//		"selector": {
	//			"value": {"$regex": %s"}
	//		},
	//		"fields": ["ID","value"],
	//		"limit":  99
	//	}
	queryString := fmt.Sprintf("{\"selector\": {\"ID\": {\"$regex\": \"%s\"}}, \"fields\": [\"ID\", \"docType\", \"status\", \"creationDate\", \"labID\":\"labID001\", \"slaughterBatchID\":\"slaughterBatchID001\", \"type\":\"type001\", \"collectionDate\":\"collectionDate001\", \"submissionDate\":\"submissionDate001\"], \"limit\":99, \"execution_stats\": true}", strings.Replace(searchString, "\"", ".", -1))
	resultsIterator, err := stub.GetQueryResult(queryString)
	if err != nil {
		return Error(http.StatusInternalServerError, err.Error())
	}
	defer resultsIterator.Close()

	// Write return buffer
	var buffer bytes.Buffer
	buffer.WriteString("{ \"values\": [")
	for resultsIterator.HasNext() {
		it, _ := resultsIterator.Next()
		if buffer.Len() > 15 {
			buffer.WriteString(",")
		}
		buffer.WriteString("{\"ID\":\"")
		buffer.WriteString(it.Key)
		buffer.WriteString("\",\"labSample\": ")
		buffer.WriteString(string(it.Value))
		buffer.WriteString("}")
	}
	buffer.WriteString("]}")
	return Success(http.StatusOK, "OK", buffer.Bytes())
}

//Helper: Save LabSampleAsset
func (lbSmpl *LabSampleAsset) saveLabSample(stub shim.ChaincodeStubInterface, labSample LabSample) (bool, error) {
	bytes, err := json.Marshal(labSample)
	if err != nil {
		return false, errors.New("Error converting labSample record JSON")
	}
	err = stub.PutState(labSample.ID, bytes)
	if err != nil {
		return false, errors.New("Error storing LabSample record")
	}
	return true, nil
}

//Helper: delete LabSampleAsset
func (lbSmpl *LabSampleAsset) deleteLabSample(stub shim.ChaincodeStubInterface, labSampleID string) (bool, error) {
	_, err := lbSmpl.retrieveLabSample(stub, labSampleID)
	if err != nil {
		return false, errors.New("LabSample with ID: " + labSampleID + " not found")
	}
	err = stub.DelState(labSampleID)
	if err != nil {
		return false, errors.New("Error deleting LabSample record")
	}
	return true, nil
}

//Helper: Update labSample Holder - updates Index
func (lbSmpl *LabSampleAsset) updateLabSampleIDIndex(stub shim.ChaincodeStubInterface, labSample LabSample) (bool, error) {
	var labSampleIDs LabSampleIDIndex
	bytes, err := stub.GetState("labSampleIDIndex")
	if err != nil {
		return false, errors.New("updateLabSampleIDIndex: Error getting labSampleIDIndex array Index from state")
	}
	err = json.Unmarshal(bytes, &labSampleIDs)
	if err != nil {
		return false, errors.New("updateLabSampleIDIndex: Error unmarshalling labSampleIDIndex array JSON")
	}
	labSampleIDs.IDs = append(labSampleIDs.IDs, labSample.ID)
	bytes, err = json.Marshal(labSampleIDs)
	if err != nil {
		return false, errors.New("updateLabSampleIDIndex: Error marshalling new labSample ID")
	}
	err = stub.PutState("labSampleIDIndex", bytes)
	if err != nil {
		return false, errors.New("updateLabSampleIDIndex: Error storing new labSample ID in labSampleIDIndex (Index)")
	}
	return true, nil
}

//Helper: delete ID from labSampleStruct Holder
func (lbSmpl *LabSampleAsset) deleteLabSampleIDIndex(stub shim.ChaincodeStubInterface, labSampleID string) (bool, error) {
	var labSampleIDIndex LabSampleIDIndex
	bytes, err := stub.GetState("labSampleIDIndex")
	if err != nil {
		return false, errors.New("deleteLabSampleIDIndex: Error getting labSampleIDIndex array Index from state")
	}
	err = json.Unmarshal(bytes, &labSampleIDIndex)
	if err != nil {
		return false, errors.New("deleteLabSampleIDIndex: Error unmarshalling labSampleIDIndex array JSON")
	}
	labSampleIDIndex.IDs, err = deleteKeyFromStringArray(labSampleIDIndex.IDs, labSampleID)
	if err != nil {
		return false, errors.New(err.Error())
	}
	bytes, err = json.Marshal(labSampleIDIndex)
	if err != nil {
		return false, errors.New("deleteLabSampleIDIndex: Error marshalling new labSampleStruct ID")
	}
	err = stub.PutState("labSampleIDIndex", bytes)
	if err != nil {
		return false, errors.New("deleteLabSampleIDIndex: Error storing new labSampleStruct ID in labSampleIDIndex (Index)")
	}
	return true, nil
}

//Helper: Initialize truck ID Holder //change template
func (lbSmpl *LabSampleAsset) initHolder(stub shim.ChaincodeStubInterface) bool {
	var labSampleIDIndex LabSampleIDIndex
	bytes, _ := json.Marshal(labSampleIDIndex)
	stub.DelState("labSampleIDIndex")
	stub.PutState("labSampleIDIndex", bytes)
	return true
}

//deleteKeyFromArray
func deleteKeyFromStringArray(array []string, key string) (newArray []string, err error) {
	for _, entry := range array {
		if entry != key {
			newArray = append(newArray, entry)
		}
	}
	if len(newArray) == len(array) {
		return newArray, errors.New("Specified Key: " + key + " not found in Array")
	}
	return newArray, nil
}

//Helper: Retrieve
func (lbSmpl *LabSampleAsset) retrieveLabSample(stub shim.ChaincodeStubInterface, labSampleID string) ([]byte, error) {
	var labSample LabSample
	var labSampleAsByteArray []byte
	bytes, err := stub.GetState(labSampleID)
	if err != nil {
		return labSampleAsByteArray, errors.New("retrieveLabSample: Error retrieving labSample with ID: " + labSampleID)
	}
	err = json.Unmarshal(bytes, &labSample)
	if err != nil {
		return labSampleAsByteArray, errors.New("retrieveLabSample: Corrupt labSample record " + string(bytes))
	}
	labSampleAsByteArray, err = json.Marshal(labSample)
	if err != nil {
		return labSampleAsByteArray, errors.New("readLabSample: Invalid labSample Object - Not a  valid JSON")
	}
	return labSampleAsByteArray, nil
}

//getLabSampleFromArgs - construct a labSample structure from string array of arguments
func getLabSampleFromArgs(args []string) (labSample LabSample, err error) {
	if !gjson.Valid(args[0]) {
		return labSample, errors.New("Invalid json")
	}

	err = gjson.Unmarshal([]byte(args[0]), &labSample)
	if err != nil {
		return labSample, err
	}
	return labSample, nil
}
