package main

//Template Version: 1.1-20180704
import (
	"bytes"
	"encoding/json"
	"fmt"
	"strings"
	"testing"

	"github.com/hyperledger/fabric/core/chaincode/shim"
)

// History/Search
// ... the History and Search API calls are not supported in the mock interface

//TestLabResultAsset_Init
func TestLabResultAsset_Init(t *testing.T) {
	labResult := new(LabResultAsset)
	stub := shim.NewMockStub("labResult", labResult)
	checkInit(t, stub, [][]byte{[]byte("init")})
	checkState(t, stub, "labResultIDIndex", getExpectedLabResultIDIndex("init"))
}

//TestLabResultAsset_InvokeUnknownFunction
func TestLabResultAsset_InvokeUnknownFunction(t *testing.T) {
	labResult := new(LabResultAsset)
	stub := shim.NewMockStub("labResult", labResult)
	checkInit(t, stub, [][]byte{[]byte("init")})
	checkInvokeUnknownFunction(t, stub, [][]byte{[]byte("myFunction"), []byte("docType:Asset")})
}

//TestLabResultAsset_Invoke_addNewLabResult
func TestLabResultAsset_Invoke_addNewLabResultOK(t *testing.T) {
	labResult := new(LabResultAsset)
	stub := shim.NewMockStub("labResult", labResult)
	checkInit(t, stub, [][]byte{[]byte("init")})
	checkInvoke(t, stub, getFirstLabResultAssetForTesting())
	newLabResultID := "100001"
	checkState(t, stub, newLabResultID, getNewLabResultExpected())
	checkState(t, stub, "labResultIDIndex", getExpectedLabResultIDIndex("addNewLabResult"))
}

//TestLabResultAsset_Invoke_addNewLabResult
func TestLabResultAsset_Invoke_addNewLabResultDuplicate(t *testing.T) {
	labResult := new(LabResultAsset)
	stub := shim.NewMockStub("labResult", labResult)
	checkInit(t, stub, [][]byte{[]byte("init")})
	checkInvoke(t, stub, getFirstLabResultAssetForTesting())
	newLabResultID := "100001"
	checkState(t, stub, newLabResultID, getNewLabResultExpected())
	checkState(t, stub, "labResultIDIndex", getExpectedLabResultIDIndex("addNewLabResult"))
	res := stub.MockInvoke("1", getFirstLabResultAssetForTesting())
	if res.Status != shim.OK {
		checkError(t, "This LabResult already exists: 100001", res.Message)
	} else {
		fmt.Println("Error was expected, but not raised")
		t.FailNow()
	}
}

func TestLabResultAsset_Invoke_updateLabResultOK(t *testing.T) {
	labResult := new(LabResultAsset)
	stub := shim.NewMockStub("labResult", labResult)
	checkInit(t, stub, [][]byte{[]byte("init")})
	checkInvoke(t, stub, getFirstLabResultAssetForTesting())
	newLabResultID := "100001"
	checkState(t, stub, newLabResultID, getNewLabResultExpected())
	checkInvoke(t, stub, getFirstLabResultAssetForUpdateTestingOK())
	checkReadLabResultAssetAfterUpdateOK(t, stub, newLabResultID)
}

//TestLabResultAsset_Invoke_removeLabResultOK  //change template
func TestLabResultAsset_Invoke_removeLabResultOK(t *testing.T) {
	labResult := new(LabResultAsset)
	stub := shim.NewMockStub("labResult", labResult)
	checkInit(t, stub, [][]byte{[]byte("init")})
	checkInvoke(t, stub, getFirstLabResultAssetForTesting())
	checkInvoke(t, stub, getSecondLabResultAssetForTesting())
	checkReadAllLabResultsOK(t, stub)
	checkState(t, stub, "labResultIDIndex", getExpectedLabResultIDIndex("beforeRemoveLabResult"))
	checkInvoke(t, stub, getRemoveSecondLabResultAssetForTesting())
	remainingLabResultID := "100001"
	checkReadLabResultOK(t, stub, remainingLabResultID)
	checkState(t, stub, "labResultIDIndex", getExpectedLabResultIDIndex("afterRemoveLabResult"))
}

//TestLabResultAsset_Invoke_removeLabResultNOK  //change template
func TestLabResultAsset_Invoke_removeLabResultNOK(t *testing.T) {
	labResult := new(LabResultAsset)
	stub := shim.NewMockStub("labResult", labResult)
	checkInit(t, stub, [][]byte{[]byte("init")})
	checkInvoke(t, stub, getFirstLabResultAssetForTesting())
	firstLabResultID := "100001"
	checkReadLabResultOK(t, stub, firstLabResultID)
	checkState(t, stub, "labResultIDIndex", getExpectedLabResultIDIndex("addNewLabResult"))
	res := stub.MockInvoke("1", getRemoveSecondLabResultAssetForTesting())
	if res.Status != shim.OK {
		checkError(t, "LabResult with ID: "+"100002"+" not found", res.Message)
	} else {
		fmt.Println("Error was expected, but not raised")
		t.FailNow()
	}
	checkState(t, stub, "labResultIDIndex", getExpectedLabResultIDIndex("addNewLabResult"))
}

//TestLabResultAsset_Invoke_removeAllLabResultsOK  //change template
func TestLabResultAsset_Invoke_removeAllLabResultsOK(t *testing.T) {
	labResult := new(LabResultAsset)
	stub := shim.NewMockStub("labResult", labResult)
	checkInit(t, stub, [][]byte{[]byte("init")})
	checkInvoke(t, stub, getFirstLabResultAssetForTesting())
	checkInvoke(t, stub, getSecondLabResultAssetForTesting())
	checkReadAllLabResultsOK(t, stub)
	checkState(t, stub, "labResultIDIndex", getExpectedLabResultIDIndex("beforeRemoveLabResult"))
	checkInvoke(t, stub, getRemoveAllLabResultAssetsForTesting())
	checkState(t, stub, "labResultIDIndex", getExpectedLabResultIDIndex(""))
}

//TestLabResultAsset_Invoke_removeLabResultNOK  //change template
func TestLabResultAsset_Invoke_removeAllLabResultsNOK(t *testing.T) {
	labResult := new(LabResultAsset)
	stub := shim.NewMockStub("labResult", labResult)
	checkInit(t, stub, [][]byte{[]byte("init")})
	res := stub.MockInvoke("1", getRemoveAllLabResultAssetsForTesting())
	if res.Status != shim.OK {
		checkError(t, "removeAllLabResults: No labResults to remove", res.Message)
	} else {
		fmt.Println("Error was expected, but not raised")
		t.FailNow()
	}
	checkState(t, stub, "labResultIDIndex", getExpectedLabResultIDIndex(""))
}

//TestLabResultAsset_Query_readLabResult
func TestLabResultAsset_Query_readLabResult(t *testing.T) {
	labResult := new(LabResultAsset)
	stub := shim.NewMockStub("labResult", labResult)
	checkInit(t, stub, [][]byte{[]byte("init")})
	labResultID := "100001"
	checkInvoke(t, stub, getFirstLabResultAssetForTesting())
	checkReadLabResultOK(t, stub, labResultID)
	checkReadLabResultNOK(t, stub, "")
}

//TestLabResultAsset_Query_readAllLabResults
func TestLabResultAsset_Query_readAllLabResults(t *testing.T) {
	labResult := new(LabResultAsset)
	stub := shim.NewMockStub("labResult", labResult)
	checkInit(t, stub, [][]byte{[]byte("init")})
	checkInvoke(t, stub, getFirstLabResultAssetForTesting())
	checkInvoke(t, stub, getSecondLabResultAssetForTesting())
	checkReadAllLabResultsOK(t, stub)
}

/*
*
*	Helper Functions
*
 */
//Get first LabResultAsset for testing
func getFirstLabResultAssetForTesting() [][]byte {
	return [][]byte{[]byte("addNewLabResult"),
		[]byte("{\"ID\":\"100001\",\"docType\":\"Asset.LabResultAsset\",\"status\":\"0\",\"creationDate\":\"12/01/2018\", \"labID\":\"labID001\", \"labSampleID\":\"labSampleID001\", \"certificateID\":\"certificateID001\", \"bioTestResult\":{\"disease\":\"disease001\",\"antibioticPercent\":\"antibioticPercent001\",\"contaminationPercent\":\"contaminationPercent001\"}, \"quality\":{\"nIRSProteinPercent\":\"nIRSProteinPercent001\", \"fatPercent\":\"fatPercent001\"}}")}
}

//Get first LabResultAsset for update testing
func getFirstLabResultAssetForUpdateTestingOK() [][]byte {
	return [][]byte{[]byte("updateLabResult"),
		[]byte("{\"ID\":\"100001\",\"docType\":\"Asset.LabResultAsset\",\"status\":\"1\",\"creationDate\":\"12/01/2018\", \"labID\":\"labID001\", \"labSampleID\":\"labSampleID001\", \"certificateID\":\"certificateID001\", \"bioTestResult\":{\"disease\":\"disease001\",\"antibioticPercent\":\"antibioticPercent001\",\"contaminationPercent\":\"contaminationPercent001\"}, \"quality\":{\"nIRSProteinPercent\":\"nIRSProteinPercent001\", \"fatPercent\":\"fatPercent001\"}}")}
}

//Get second LabResultAsset for testing
func getSecondLabResultAssetForTesting() [][]byte {
	return [][]byte{[]byte("addNewLabResult"),
		[]byte("{\"ID\":\"100002\",\"docType\":\"Asset.LabResultAsset\",\"status\":\"0\",\"creationDate\":\"12/01/2018\", \"labID\":\"labID002\", \"labSampleID\":\"labSampleID002\", \"certificateID\":\"certificateID002\", \"bioTestResult\":{\"disease\":\"disease002\",\"antibioticPercent\":\"antibioticPercent002\",\"contaminationPercent\":\"contaminationPercent002\"}, \"quality\":{\"nIRSProteinPercent\":\"nIRSProteinPercent002\", \"fatPercent\":\"fatPercent002\"}}")}
}

//Get remove second LabResultAsset for testing //change template
func getRemoveSecondLabResultAssetForTesting() [][]byte {
	return [][]byte{[]byte("removeLabResult"),
		[]byte("100002")}
}

//Get remove all LabResultAssets for testing //change template
func getRemoveAllLabResultAssetsForTesting() [][]byte {
	return [][]byte{[]byte("removeAllLabResults")}
}

//Get an expected value for testing
func getNewLabResultExpected() []byte {

	var labResult LabResult
	var bioTestResult BioTestResult
	var quality LabResultQuality

	bioTestResult.Disease = "disease001"
	bioTestResult.AntibioticPercent = "antibioticPercent001"
	bioTestResult.ContaminationPercent = "contaminationPercent001"

	quality.NIRSProteinPercent = "nIRSProteinPercent001"
	quality.FatPercent = "fatPercent001"

	labResult.ID = "100001"
	labResult.ObjectType = "Asset.LabResultAsset"
	labResult.Status = "0"
	labResult.CreationDate = "12/01/2018"
	labResult.LabID = "labID001"
	labResult.LabSampleID = "labSampleID001"
	labResult.CertificateID = "certificateID001"
	labResult.BioTestResult = bioTestResult
	labResult.Quality = quality

	labResultJSON, err := json.Marshal(labResult)
	if err != nil {
		fmt.Println("Error converting a LabResult record to JSON")
		return nil
	}
	return []byte(labResultJSON)
}

//Get an expected value for testing
func getUpdatedLabResultExpected() []byte {
	var labResult LabResult
	var bioTestResult BioTestResult
	var quality LabResultQuality

	bioTestResult.Disease = "disease001"
	bioTestResult.AntibioticPercent = "antibioticPercent001"
	bioTestResult.ContaminationPercent = "contaminationPercent001"

	quality.NIRSProteinPercent = "nIRSProteinPercent001"
	quality.FatPercent = "fatPercent001"

	labResult.ID = "100001"
	labResult.ObjectType = "Asset.LabResultAsset"
	labResult.Status = "1"
	labResult.CreationDate = "12/01/2018"
	labResult.LabID = "labID001"
	labResult.LabSampleID = "labSampleID001"
	labResult.CertificateID = "certificateID001"
	labResult.BioTestResult = bioTestResult
	labResult.Quality = quality

	labResultJSON, err := json.Marshal(labResult)
	if err != nil {
		fmt.Println("Error converting a LabResult record to JSON")
		return nil
	}
	return []byte(labResultJSON)
}

//Get expected values of LabResults for testing
func getExpectedLabResults() []byte {
	var labResults []LabResult
	var labResult LabResult
	var bioTestResult BioTestResult
	var quality LabResultQuality

	bioTestResult.Disease = "disease001"
	bioTestResult.AntibioticPercent = "antibioticPercent001"
	bioTestResult.ContaminationPercent = "contaminationPercent001"

	quality.NIRSProteinPercent = "nIRSProteinPercent001"
	quality.FatPercent = "fatPercent001"

	labResult.ID = "100001"
	labResult.ObjectType = "Asset.LabResultAsset"
	labResult.Status = "0"
	labResult.CreationDate = "12/01/2018"
	labResult.LabID = "labID001"
	labResult.LabSampleID = "labSampleID001"
	labResult.CertificateID = "certificateID001"
	labResult.BioTestResult = bioTestResult
	labResult.Quality = quality

	labResults = append(labResults, labResult)

	bioTestResult.Disease = "disease002"
	bioTestResult.AntibioticPercent = "antibioticPercent002"
	bioTestResult.ContaminationPercent = "contaminationPercent002"

	quality.NIRSProteinPercent = "nIRSProteinPercent002"
	quality.FatPercent = "fatPercent002"

	labResult.ID = "100002"
	labResult.ObjectType = "Asset.LabResultAsset"
	labResult.Status = "0"
	labResult.CreationDate = "12/01/2018"
	labResult.LabID = "labID002"
	labResult.LabSampleID = "labSampleID002"
	labResult.CertificateID = "certificateID002"
	labResult.BioTestResult = bioTestResult
	labResult.Quality = quality

	labResults = append(labResults, labResult)
	labResultJSON, err := json.Marshal(labResults)
	if err != nil {
		fmt.Println("Error converting labResult records to JSON")
		return nil
	}
	return []byte(labResultJSON)
}

func getExpectedLabResultIDIndex(funcName string) []byte {
	var labResultIDIndex LabResultIDIndex
	switch funcName {
	case "addNewLabResult":
		labResultIDIndex.IDs = append(labResultIDIndex.IDs, "100001")
		labResultIDIndexBytes, err := json.Marshal(labResultIDIndex)
		if err != nil {
			fmt.Println("Error converting LabResultIDIndex to JSON")
			return nil
		}
		return labResultIDIndexBytes
	case "beforeRemoveLabResult":
		labResultIDIndex.IDs = append(labResultIDIndex.IDs, "100001")
		labResultIDIndex.IDs = append(labResultIDIndex.IDs, "100002")
		labResultIDIndexBytes, err := json.Marshal(labResultIDIndex)
		if err != nil {
			fmt.Println("Error converting LabResultIDIndex to JSON")
			return nil
		}
		return labResultIDIndexBytes
	case "afterRemoveLabResult":
		labResultIDIndex.IDs = append(labResultIDIndex.IDs, "100001")
		labResultIDIndexBytes, err := json.Marshal(labResultIDIndex)
		if err != nil {
			fmt.Println("Error converting LabResultIDIndex to JSON")
			return nil
		}
		return labResultIDIndexBytes
	default:
		labResultIDIndexBytes, err := json.Marshal(labResultIDIndex)
		if err != nil {
			fmt.Println("Error converting LabResultIDIndex to JSON")
			return nil
		}
		return labResultIDIndexBytes
	}
}

//checkInit - helper to check the Initialization of chaincode: LabResultAsset
func checkInit(t *testing.T, stub *shim.MockStub, args [][]byte) {
	res := stub.MockInit("1", args)
	if res.Status != shim.OK {
		fmt.Println("Init failed", string(res.Message))
		t.FailNow()
	}
}

//checkState - helper for checking the chaincode state for a given stateKey afgainst an expected value
func checkState(t *testing.T, stub *shim.MockStub, stateKey string, expectedState []byte) {
	actualState := stub.State[stateKey]
	if actualState == nil {
		fmt.Println("State for ", stateKey, ": failed to get value")
		t.FailNow()
	}

	if bytes.Compare(actualState, expectedState) != 0 {
		fmt.Println("Incorrect State for " + stateKey + ": \nExpected: " + string(expectedState) + "\nActual  : " + string(actualState))
		t.FailNow()
	}
}

//checkInvoke - helper for checking Invoke of chaincode
func checkInvoke(t *testing.T, stub *shim.MockStub, args [][]byte) {
	res := stub.MockInvoke("1", args)
	if res.Status != shim.OK {
		fmt.Println("Invoke", args, "failed", string(res.Message))
		t.FailNow()
	}
}

func checkInvokeUnknownFunction(t *testing.T, stub *shim.MockStub, args [][]byte) {
	res := stub.MockInvoke("1", args)
	if res.Status != shim.OK {
		expectedErr := "Received unknown function invocation"
		actualErr := string(res.Message)
		if !(strings.Contains(actualErr, expectedErr)) {
			fmt.Println("Invoke with unknown function must throw error: ", expectedErr, "\n Actual Error :", actualErr)
			t.FailNow()
		}
	} else {
		t.FailNow()
	}
}

//checkReadLabResultOK - helper for positive test readLabResult
func checkReadLabResultOK(t *testing.T, stub *shim.MockStub, labResultID string) {
	res := stub.MockInvoke("1", [][]byte{[]byte("readLabResult"), []byte(labResultID)})
	if res.Status != shim.OK {
		fmt.Println("func readLabResult with ID: ", labResultID, " failed"+string(res.Message))
		t.FailNow()
	}
	if res.Payload == nil {
		fmt.Println("func readLabResult with ID: ", labResultID, "failed to get value")
		t.FailNow()
	}
	if bytes.Compare(getNewLabResultExpected(), []byte(res.Payload)) != 0 {
		fmt.Println("func readLabResult with ID: ", labResultID, "Expected:", string(getNewLabResultExpected()), "Actual:", string(res.Payload))
		t.FailNow()
	}
}

//checkReadLabResultNOK - helper for negative testing of readLabResult
func checkReadLabResultNOK(t *testing.T, stub *shim.MockStub, labResultID string) {
	//with no labResultID
	res := stub.MockInvoke("1", [][]byte{[]byte("readLabResult"), []byte("")})
	if res.Status != shim.OK {
		expectedErr := "retrieveLabResult: Corrupt labResult record "
		actualErr := string(res.Message)
		if !(strings.Contains(actualErr, expectedErr)) {
			fmt.Println("func readLabResult negative test: ", "Expected Error:", expectedErr, "Actual Error", actualErr)
			t.FailNow()
		}
	} else {
		t.FailNow()
	}
}

//checkReadLabResultAssetAfterUpdateOK - helper for positive test readLabResult after update
func checkReadLabResultAssetAfterUpdateOK(t *testing.T, stub *shim.MockStub, LabResultID string) {
	res := stub.MockInvoke("1", [][]byte{[]byte("readLabResult"), []byte(LabResultID)})
	if res.Status != shim.OK {
		fmt.Println("func readLabResult with ID: ", LabResultID, " failed"+string(res.Message))
		t.FailNow()
	}
	if res.Payload == nil {
		fmt.Println("func readLabResult with ID: ", LabResultID, "failed to get value")
		t.FailNow()
	}
	if bytes.Compare(getUpdatedLabResultExpected(), []byte(res.Payload)) != 0 {
		fmt.Println("func readLabResult with ID: ", LabResultID, "Expected:", string(getUpdatedLabResultExpected()), "Actual:", string(res.Payload))
		t.FailNow()
	}
}

func checkReadAllLabResultsOK(t *testing.T, stub *shim.MockStub) {
	res := stub.MockInvoke("1", [][]byte{[]byte("readAllLabResults")})
	if res.Status != shim.OK {
		fmt.Println("func readAllLabResults failed", string(res.Message))
		t.FailNow()
	}
	if res.Payload == nil {
		fmt.Println("func readAllLabResults failed to get value")
		t.FailNow()
	}
	if bytes.Compare(getExpectedLabResults(), []byte(res.Payload)) != 0 {
		fmt.Println("func readAllLabResults Expected:\n", string(getExpectedLabResults()), "\nActual:\n", string(res.Payload))
		t.FailNow()
	}
}

func checkError(t *testing.T, exp string, act string) {
	if strings.Compare(exp, act) != 0 {
		fmt.Println("Unexpected Error! Expecting ", exp, "\n Actual :", act)
		t.FailNow()
	}
}
