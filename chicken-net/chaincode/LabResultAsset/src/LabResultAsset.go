package main

/*
Template Version: 1.2-20180921
Template Owner: Ramesh Suraparaju (ramesh.suraparaju@sap.com)
This template is used to generate the chaincode for an asset that implements the hyperledger fabric shim
*/
import (
	"bytes"
	"encoding/json"
	"errors"
	"fmt"
	"net/http"
	"strings"
	"time"

	"github.com/hyperledger/fabric/core/chaincode/shim"
	"github.com/hyperledger/fabric/protos/peer"
	"github.com/tidwall/gjson"
)

var logger = shim.NewLogger("CLDChaincode")

//LabResultAssetStatus - enumeration for labResultAsset statuses (Same status enumeration for LabResultAsset)
type LabResultAssetStatus int

const (
	InProcess LabResultAssetStatus = 1 + iota //InProcess: LabResult in process
	Completed                                 //Completed: LabResult completed
	Certified                                 //Certified: LabResult certified
)

var labResultAssetStatuses = [...]string{
	"In Process",
	"Completed",
	"Certified",
}

func (labResultAssetStatus LabResultAssetStatus) labResultAssetStatusToString() string {
	return labResultAssetStatuses[labResultAssetStatus-1]
}

//BioTestResult (dataType)
type BioTestResult struct {
	Disease              string `json:"disease"`
	ContaminationPercent string `json:"contaminationPercent"`
	AntibioticPercent    string `json:"antibioticPercent"`
}

//LabResultQuality (dataType)
type LabResultQuality struct {
	NIRSProteinPercent string `json:"nIRSProteinPercent"`
	FatPercent         string `json:"fatPercent"`
}

//LabResultAsset - Chaincode for asset LabResult
type LabResultAsset struct {
}

//LabResult - Details of the asset type LabResult
type LabResult struct {
	ID            string           `json:"ID"`
	ObjectType    string           `json:"docType"`
	Status        string           `json:"status"`
	CreationDate  string           `json:"creationDate"`
	LabID         string           `json:"labID"`
	LabSampleID   string           `json:"labSampleID"`
	CertificateID string           `json:"certificateID"`
	BioTestResult BioTestResult    `json:"bioTestResult"`
	Quality       LabResultQuality `json:"quality"`
}

//LabResultIDIndex - Index on IDs for retrieval all LabResults
type LabResultIDIndex struct {
	IDs []string `json:"IDs"`
}

//Success - Success Message
func Success(rc int32, doc string, payload []byte) peer.Response {
	logger.Infof("Success %d = %s", rc, doc, payload)
	return peer.Response{
		Status:  rc,
		Message: doc,
		Payload: payload,
	}
}

//Error - Error Message
func Error(rc int32, doc string) peer.Response {
	logger.Errorf("Error %d = %s", rc, doc)
	return peer.Response{
		Status:  rc,
		Message: doc,
	}
}

func main() {
	err := shim.Start(new(LabResultAsset))
	if err != nil {
		fmt.Printf("Error starting LabResultAsset chaincode function main(): %s", err)
	} else {
		fmt.Printf("Starting LabResultAsset chaincode function main() executed successfully")
	}
}

//Init - The chaincode Init function: No  arguments, only initializes a ID array as Index for retrieval of all LabResults
func (lbRslt *LabResultAsset) Init(stub shim.ChaincodeStubInterface) peer.Response {
	var labResultIDIndex LabResultIDIndex
	record, _ := stub.GetState("labResultIDIndex")
	//chaincode upgrade: Index might already exist, create new only if it does not
	if record == nil {
		bytes, _ := json.Marshal(labResultIDIndex)
		stub.PutState("labResultIDIndex", bytes)
	}
	return Success(http.StatusOK, "labResultIDIndex initiated successfully", nil)
}

//Invoke - The chaincode Invoke function:
func (lbRslt *LabResultAsset) Invoke(stub shim.ChaincodeStubInterface) peer.Response {
	function, args := stub.GetFunctionAndParameters()
	logger.Debug("function: ", function)
	switch function {
	case "addNewLabResult":
		return lbRslt.addNewLabResult(stub, args)
	case "updateLabResult":
		return lbRslt.updateLabResult(stub, args)
	case "removeLabResult":
		return lbRslt.removeLabResult(stub, args[0])
	case "removeAllLabResults":
		return lbRslt.removeAllLabResults(stub)
	case "readLabResult":
		return lbRslt.readLabResult(stub, args[0])
	case "readAllLabResults":
		return lbRslt.readAllLabResults(stub)
	case "searchLabResultsByID":
		return lbRslt.searchLabResultsByID(stub, args)
	case "getLabResultHistory":
		return lbRslt.getLabResultHistory(stub, args)
	default:
		return Error(http.StatusBadRequest, "Received unknown function invocation")
	}
}

//Invoke Route: addNewLabResult
func (lbRslt *LabResultAsset) addNewLabResult(stub shim.ChaincodeStubInterface, args []string) peer.Response {
	labResult, err := getLabResultFromArgs(args)
	if err != nil {
		return Error(http.StatusBadRequest, "LabResult Data is Corrupted")
	}
	labResult.ObjectType = "Asset.LabResultAsset"
	record, err := stub.GetState(labResult.ID)
	if record != nil {
		return Error(http.StatusBadRequest, "This LabResult already exists: "+labResult.ID)
	}
	_, err = lbRslt.saveLabResult(stub, labResult)
	if err != nil {
		return Error(http.StatusInternalServerError, err.Error())
	}
	_, err = lbRslt.updateLabResultIDIndex(stub, labResult)
	if err != nil {
		return Error(http.StatusInternalServerError, err.Error())
	}
	return Success(http.StatusOK, "addNewLabResult: successfully added: "+labResult.ID, nil)
}

//Invoke Route: updateLabResult
func (lbRslt *LabResultAsset) updateLabResult(stub shim.ChaincodeStubInterface, args []string) peer.Response {
	input, err := getLabResultFromArgs(args)
	if err != nil {
		return Error(http.StatusBadRequest, "LabResult Data is Corrupted")
	}
	record, err := stub.GetState(input.ID)
	if record == nil {
		return Error(http.StatusBadRequest, "This LabResult does not exist: "+input.ID)
	}
	_, err = lbRslt.saveLabResult(stub, input)
	if err != nil {
		return Error(http.StatusInternalServerError, err.Error())
	}
	return Success(http.StatusOK, "updateLabResult: successfully updated: "+input.ID, nil)
}

//Invoke Route: removeLabResult
func (lbRslt *LabResultAsset) removeLabResult(stub shim.ChaincodeStubInterface, labResultID string) peer.Response {
	_, err := lbRslt.deleteLabResult(stub, labResultID)
	if err != nil {
		return Error(http.StatusInternalServerError, err.Error())
	}
	_, err = lbRslt.deleteLabResultIDIndex(stub, labResultID)
	if err != nil {
		return Error(http.StatusInternalServerError, err.Error())
	}
	return Success(http.StatusOK, "removeLabResult: successfully removed: "+labResultID, nil)
}

//Invoke Route: removeAllLabResults
func (lbRslt *LabResultAsset) removeAllLabResults(stub shim.ChaincodeStubInterface) peer.Response {
	var labResultIDIndex LabResultIDIndex
	bytes, err := stub.GetState("labResultIDIndex")
	if err != nil {
		return Error(http.StatusInternalServerError, "removeAllLabResults: Error getting labResultIDIndex array")
	}
	err = json.Unmarshal(bytes, &labResultIDIndex)
	if err != nil {
		return Error(http.StatusInternalServerError, "removeAllLabResults: Error unmarshalling labResultIDIndex array JSON")
	}
	if len(labResultIDIndex.IDs) == 0 {
		return Error(http.StatusInternalServerError, "removeAllLabResults: No labResults to remove")
	}
	for _, labResultStructID := range labResultIDIndex.IDs {
		_, err = lbRslt.deleteLabResult(stub, labResultStructID)
		if err != nil {
			return Error(http.StatusInternalServerError, "Failed to remove LabResult with ID: "+labResultStructID)
		}
		_, err = lbRslt.deleteLabResultIDIndex(stub, labResultStructID)
		if err != nil {
			return Error(http.StatusInternalServerError, err.Error())
		}
	}
	lbRslt.initHolder(stub)
	return Success(http.StatusOK, "removeAllLabResults: Successfully removed all LabResults", nil)
}

//Query Route: readLabResult
func (lbRslt *LabResultAsset) readLabResult(stub shim.ChaincodeStubInterface, labResultID string) peer.Response {
	labResultAsByteArray, err := lbRslt.retrieveLabResult(stub, labResultID)
	if err != nil {
		return Error(http.StatusInternalServerError, err.Error())
	}
	return Success(http.StatusOK, "readLabResult: Successfully read LabResult with ID: "+labResultID, labResultAsByteArray)
}

//Query Route: readAllLabResults
func (lbRslt *LabResultAsset) readAllLabResults(stub shim.ChaincodeStubInterface) peer.Response {
	var labResultIDs LabResultIDIndex
	bytes, err := stub.GetState("labResultIDIndex")
	if err != nil {
		return Error(http.StatusInternalServerError, "readAllLabResults: Error getting labResultIDIndex array")
	}
	err = json.Unmarshal(bytes, &labResultIDs)
	if err != nil {
		return Error(http.StatusInternalServerError, "readAllLabResults: Error unmarshalling labResultIDIndex array JSON")
	}
	result := "["

	var labResultAsByteArray []byte

	for _, labResultID := range labResultIDs.IDs {
		labResultAsByteArray, err = lbRslt.retrieveLabResult(stub, labResultID)
		if err != nil {
			return Error(http.StatusNotFound, "Failed to retrieve labResult with ID: "+labResultID)
		}
		result += string(labResultAsByteArray) + ","
	}
	if len(result) == 1 {
		result = "[]"
	} else {
		result = result[:len(result)-1] + "]"
	}
	return Success(http.StatusOK, "OK", []byte(result))
}

//GetLabResultHistory - reads history of a labResult with specified ID
func (lbRslt *LabResultAsset) getLabResultHistory(stub shim.ChaincodeStubInterface, args []string) peer.Response {

	resultsIterator, err := stub.GetHistoryForKey(args[0])
	if err != nil {
		return Error(http.StatusNotFound, "Not Found")
	}
	defer resultsIterator.Close()
	// Write return buffer
	var buffer bytes.Buffer
	buffer.WriteString("{ \"values\": [")
	for resultsIterator.HasNext() {
		it, _ := resultsIterator.Next()
		if buffer.Len() > 15 {
			buffer.WriteString(",")
		}
		buffer.WriteString("{\"timestamp\":\"")
		buffer.WriteString(time.Unix(it.Timestamp.Seconds, int64(it.Timestamp.Nanos)).Format(time.Stamp))
		buffer.WriteString("\",\"labResult\": ")
		buffer.WriteString(string(it.Value))
		buffer.WriteString("}")
	}
	buffer.WriteString("]}")
	return Success(http.StatusOK, "OK", buffer.Bytes())
}

// Search for all matching IDs, given a (regex) value expression and return both the IDs and text.
// For example: '^H.llo' will match any string starting with 'Hello' or 'Hallo'.
func (lbRslt *LabResultAsset) searchLabResultsByID(stub shim.ChaincodeStubInterface, args []string) peer.Response {

	searchString := strings.Replace(args[0], "\"", ".", -1) // protect against SQL injection

	// stub.GetQueryResult takes a verbatim CouchDB (assuming this is used DB). See CouchDB documentation:
	//     http://docs.couchdb.org/en/2.0.0/api/database/find.html
	// For example:
	//	{
	//		"selector": {
	//			"value": {"$regex": %s"}
	//		},
	//		"fields": ["ID","value"],
	//		"limit":  99
	//	}
	queryString := fmt.Sprintf("{\"selector\": {\"ID\": {\"$regex\": \"%s\"}}, \"fields\": [\"ID\", \"docType\", \"status\", \"creationDate\", \"labID\":\"labID001\", \"labLabResultID\":\"labLabResultID001\", \"certificateID\":\"certificateID001\", \"bioTestResult\":\"bioTestResult001\", \"quality\":\"quality001\"], \"limit\":99, \"execution_stats\": true}", strings.Replace(searchString, "\"", ".", -1))
	resultsIterator, err := stub.GetQueryResult(queryString)
	if err != nil {
		return Error(http.StatusInternalServerError, err.Error())
	}
	defer resultsIterator.Close()

	// Write return buffer
	var buffer bytes.Buffer
	buffer.WriteString("{ \"values\": [")
	for resultsIterator.HasNext() {
		it, _ := resultsIterator.Next()
		if buffer.Len() > 15 {
			buffer.WriteString(",")
		}
		buffer.WriteString("{\"ID\":\"")
		buffer.WriteString(it.Key)
		buffer.WriteString("\",\"labResult\": ")
		buffer.WriteString(string(it.Value))
		buffer.WriteString("}")
	}
	buffer.WriteString("]}")
	return Success(http.StatusOK, "OK", buffer.Bytes())
}

//Helper: Save LabResultAsset
func (lbRslt *LabResultAsset) saveLabResult(stub shim.ChaincodeStubInterface, labResult LabResult) (bool, error) {
	bytes, err := json.Marshal(labResult)
	if err != nil {
		return false, errors.New("Error converting labResult record JSON")
	}
	err = stub.PutState(labResult.ID, bytes)
	if err != nil {
		return false, errors.New("Error storing LabResult record")
	}
	return true, nil
}

//Helper: delete LabResultAsset
func (lbRslt *LabResultAsset) deleteLabResult(stub shim.ChaincodeStubInterface, labResultID string) (bool, error) {
	_, err := lbRslt.retrieveLabResult(stub, labResultID)
	if err != nil {
		return false, errors.New("LabResult with ID: " + labResultID + " not found")
	}
	err = stub.DelState(labResultID)
	if err != nil {
		return false, errors.New("Error deleting LabResult record")
	}
	return true, nil
}

//Helper: Update labResult Holder - updates Index
func (lbRslt *LabResultAsset) updateLabResultIDIndex(stub shim.ChaincodeStubInterface, labResult LabResult) (bool, error) {
	var labResultIDs LabResultIDIndex
	bytes, err := stub.GetState("labResultIDIndex")
	if err != nil {
		return false, errors.New("updateLabResultIDIndex: Error getting labResultIDIndex array Index from state")
	}
	err = json.Unmarshal(bytes, &labResultIDs)
	if err != nil {
		return false, errors.New("updateLabResultIDIndex: Error unmarshalling labResultIDIndex array JSON")
	}
	labResultIDs.IDs = append(labResultIDs.IDs, labResult.ID)
	bytes, err = json.Marshal(labResultIDs)
	if err != nil {
		return false, errors.New("updateLabResultIDIndex: Error marshalling new labResult ID")
	}
	err = stub.PutState("labResultIDIndex", bytes)
	if err != nil {
		return false, errors.New("updateLabResultIDIndex: Error storing new labResult ID in labResultIDIndex (Index)")
	}
	return true, nil
}

//Helper: delete ID from labResultStruct Holder
func (lbRslt *LabResultAsset) deleteLabResultIDIndex(stub shim.ChaincodeStubInterface, labResultID string) (bool, error) {
	var labResultIDIndex LabResultIDIndex
	bytes, err := stub.GetState("labResultIDIndex")
	if err != nil {
		return false, errors.New("deleteLabResultIDIndex: Error getting labResultIDIndex array Index from state")
	}
	err = json.Unmarshal(bytes, &labResultIDIndex)
	if err != nil {
		return false, errors.New("deleteLabResultIDIndex: Error unmarshalling labResultIDIndex array JSON")
	}
	labResultIDIndex.IDs, err = deleteKeyFromStringArray(labResultIDIndex.IDs, labResultID)
	if err != nil {
		return false, errors.New(err.Error())
	}
	bytes, err = json.Marshal(labResultIDIndex)
	if err != nil {
		return false, errors.New("deleteLabResultIDIndex: Error marshalling new labResultStruct ID")
	}
	err = stub.PutState("labResultIDIndex", bytes)
	if err != nil {
		return false, errors.New("deleteLabResultIDIndex: Error storing new labResultStruct ID in labResultIDIndex (Index)")
	}
	return true, nil
}

//Helper: Initialize truck ID Holder //change template
func (lbRslt *LabResultAsset) initHolder(stub shim.ChaincodeStubInterface) bool {
	var labResultIDIndex LabResultIDIndex
	bytes, _ := json.Marshal(labResultIDIndex)
	stub.DelState("labResultIDIndex")
	stub.PutState("labResultIDIndex", bytes)
	return true
}

//deleteKeyFromArray
func deleteKeyFromStringArray(array []string, key string) (newArray []string, err error) {
	for _, entry := range array {
		if entry != key {
			newArray = append(newArray, entry)
		}
	}
	if len(newArray) == len(array) {
		return newArray, errors.New("Specified Key: " + key + " not found in Array")
	}
	return newArray, nil
}

//Helper: Retrieve
func (lbRslt *LabResultAsset) retrieveLabResult(stub shim.ChaincodeStubInterface, labResultID string) ([]byte, error) {
	var labResult LabResult
	var labResultAsByteArray []byte
	bytes, err := stub.GetState(labResultID)
	if err != nil {
		return labResultAsByteArray, errors.New("retrieveLabResult: Error retrieving labResult with ID: " + labResultID)
	}
	err = json.Unmarshal(bytes, &labResult)
	if err != nil {
		return labResultAsByteArray, errors.New("retrieveLabResult: Corrupt labResult record " + string(bytes))
	}
	labResultAsByteArray, err = json.Marshal(labResult)
	if err != nil {
		return labResultAsByteArray, errors.New("readLabResult: Invalid labResult Object - Not a  valid JSON")
	}
	return labResultAsByteArray, nil
}

//getLabResultFromArgs - construct a labResult structure from string array of arguments
func getLabResultFromArgs(args []string) (labResult LabResult, err error) {
	if !gjson.Valid(args[0]) {
		return labResult, errors.New("Invalid json")
	}

	err = gjson.Unmarshal([]byte(args[0]), &labResult)
	if err != nil {
		return labResult, err
	}
	return labResult, nil
}
