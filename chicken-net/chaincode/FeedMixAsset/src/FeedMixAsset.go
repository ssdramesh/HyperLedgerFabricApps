package main

/*
Template Version: 1.2-20180921
Template Owner: Ramesh Suraparaju (ramesh.suraparaju@sap.com)
This template is used to generate the chaincode for an asset that implements the hyperledger fabric shim
*/
import (
	"bytes"
	"encoding/json"
	"errors"
	"fmt"
	"net/http"
	"strings"
	"time"

	"github.com/hyperledger/fabric/core/chaincode/shim"
	"github.com/hyperledger/fabric/protos/peer"
	"github.com/tidwall/gjson"
)

var logger = shim.NewLogger("CLDChaincode")

//FeedMixAssetStatus - enumeration for feedMixAsset statuses (Same status enumeration for FeedMixAsset)
type FeedMixAssetStatus int

const (
	Submitted FeedMixAssetStatus = 1 + iota //Submitted: FeedMix submitted to chicken farm
	Accepted                                //Accepted: FeedMix accepted by chicken farm
)

var feedMixAssetStatuses = [...]string{
	"Submitted",
	"Accepted",
}

func (feedMixAssetStatus FeedMixAssetStatus) feedMixAssetStatusToString() string {
	return feedMixAssetStatuses[feedMixAssetStatus-1]
}

//Supplier (dataType)
type FeedMixSupplier struct {
	ID         string `json:"ID"`
	LandDeedID string `json:"landDeedID"`
	InvoiceID  string `json:"invoiceID"`
}

//Purchaser (dataType)
type FeedMixPurchaser struct {
	ID              string `json:"ID"`
	PurchaseOrderID string `json:"purchaseOrderID"`
	GoodsReceiptID  string `json:"goodsReceiptID"`
	MaterialID      string `json:"materialID"`
}

//FeedMixTestResult (dataType)
type FeedMixTestResult struct {
	ID            string `json:"ID"`
	Date          string `json:"date"`
	CertificateID string `json:"certificateID"`
}

//FeedMixQuality (dataType)
type FeedMixQuality struct {
	Hummidity            string `json:"humidity"`
	ContaminationPercent string `json:"contaminationPercent"`
}

//FeedMixAsset - Chaincode for asset FeedMix
type FeedMixAsset struct {
}

//FeedMix - Details of the asset type FeedMix
type FeedMix struct {
	ID           string            `json:"ID"`
	ObjectType   string            `json:"docType"`
	Status       string            `json:"status"`
	CreationDate string            `json:"creationDate"`
	Supplier     FeedMixSupplier   `json:"supplier"`
	Purchaser    FeedMixPurchaser  `json:"purchaser"`
	TestResult   FeedMixTestResult `json:"testResult"`
	Quality      FeedMixQuality    `json:"quality"`
	InputType    string            `json:"inputType"`
}

//FeedMixIDIndex - Index on IDs for retrieval all FeedMixs
type FeedMixIDIndex struct {
	IDs []string `json:"IDs"`
}

//Success - Success Message
func Success(rc int32, doc string, payload []byte) peer.Response {
	logger.Infof("Success %d = %s", rc, doc, payload)
	return peer.Response{
		Status:  rc,
		Message: doc,
		Payload: payload,
	}
}

//Error - Error Message
func Error(rc int32, doc string) peer.Response {
	logger.Errorf("Error %d = %s", rc, doc)
	return peer.Response{
		Status:  rc,
		Message: doc,
	}
}

func main() {
	err := shim.Start(new(FeedMixAsset))
	if err != nil {
		fmt.Printf("Error starting FeedMixAsset chaincode function main(): %s", err)
	} else {
		fmt.Printf("Starting FeedMixAsset chaincode function main() executed successfully")
	}
}

//Init - The chaincode Init function: No  arguments, only initializes a ID array as Index for retrieval of all FeedMixs
func (fdMx *FeedMixAsset) Init(stub shim.ChaincodeStubInterface) peer.Response {
	var feedMixIDIndex FeedMixIDIndex
	record, _ := stub.GetState("feedMixIDIndex")
	//chaincode upgrade: Index might already exist, create new only if it does not
	if record == nil {
		bytes, _ := json.Marshal(feedMixIDIndex)
		stub.PutState("feedMixIDIndex", bytes)
	}
	return Success(http.StatusOK, "feedMixIDIndex initiated successfully", nil)
}

//Invoke - The chaincode Invoke function:
func (fdMx *FeedMixAsset) Invoke(stub shim.ChaincodeStubInterface) peer.Response {
	function, args := stub.GetFunctionAndParameters()
	logger.Debug("function: ", function)
	switch function {
	case "addNewFeedMix":
		return fdMx.addNewFeedMix(stub, args)
	case "updateFeedMix":
		return fdMx.updateFeedMix(stub, args)
	case "removeFeedMix":
		return fdMx.removeFeedMix(stub, args[0])
	case "removeAllFeedMixs":
		return fdMx.removeAllFeedMixs(stub)
	case "readFeedMix":
		return fdMx.readFeedMix(stub, args[0])
	case "readAllFeedMixs":
		return fdMx.readAllFeedMixs(stub)
	case "searchFeedMixsByID":
		return fdMx.searchFeedMixsByID(stub, args)
	case "getFeedMixHistory":
		return fdMx.getFeedMixHistory(stub, args)
	default:
		return Error(http.StatusBadRequest, "Received unknown function invocation")
	}
}

//Invoke Route: addNewFeedMix
func (fdMx *FeedMixAsset) addNewFeedMix(stub shim.ChaincodeStubInterface, args []string) peer.Response {
	feedMix, err := getFeedMixFromArgs(args)
	if err != nil {
		return Error(http.StatusBadRequest, "FeedMix Data is Corrupted")
	}
	feedMix.ObjectType = "Asset.FeedMixAsset"
	record, err := stub.GetState(feedMix.ID)
	if record != nil {
		return Error(http.StatusBadRequest, "This FeedMix already exists: "+feedMix.ID)
	}
	_, err = fdMx.saveFeedMix(stub, feedMix)
	if err != nil {
		return Error(http.StatusInternalServerError, err.Error())
	}
	_, err = fdMx.updateFeedMixIDIndex(stub, feedMix)
	if err != nil {
		return Error(http.StatusInternalServerError, err.Error())
	}
	return Success(http.StatusOK, "addNewFeedMix: successfully added: "+feedMix.ID, nil)
}

//Invoke Route: updateFeedMix
func (fdMx *FeedMixAsset) updateFeedMix(stub shim.ChaincodeStubInterface, args []string) peer.Response {
	input, err := getFeedMixFromArgs(args)
	if err != nil {
		return Error(http.StatusBadRequest, "FeedMix Data is Corrupted")
	}
	record, err := stub.GetState(input.ID)
	if record == nil {
		return Error(http.StatusBadRequest, "This FeedMix does not exist: "+input.ID)
	}
	_, err = fdMx.saveFeedMix(stub, input)
	if err != nil {
		return Error(http.StatusInternalServerError, err.Error())
	}
	return Success(http.StatusOK, "updateFeedMix: successfully updated: "+input.ID, nil)
}

//Invoke Route: removeFeedMix
func (fdMx *FeedMixAsset) removeFeedMix(stub shim.ChaincodeStubInterface, feedMixID string) peer.Response {
	_, err := fdMx.deleteFeedMix(stub, feedMixID)
	if err != nil {
		return Error(http.StatusInternalServerError, err.Error())
	}
	_, err = fdMx.deleteFeedMixIDIndex(stub, feedMixID)
	if err != nil {
		return Error(http.StatusInternalServerError, err.Error())
	}
	return Success(http.StatusOK, "removeFeedMix: successfully removed: "+feedMixID, nil)
}

//Invoke Route: removeAllFeedMixs
func (fdMx *FeedMixAsset) removeAllFeedMixs(stub shim.ChaincodeStubInterface) peer.Response {
	var feedMixIDIndex FeedMixIDIndex
	bytes, err := stub.GetState("feedMixIDIndex")
	if err != nil {
		return Error(http.StatusInternalServerError, "removeAllFeedMixs: Error getting feedMixIDIndex array")
	}
	err = json.Unmarshal(bytes, &feedMixIDIndex)
	if err != nil {
		return Error(http.StatusInternalServerError, "removeAllFeedMixs: Error unmarshalling feedMixIDIndex array JSON")
	}
	if len(feedMixIDIndex.IDs) == 0 {
		return Error(http.StatusInternalServerError, "removeAllFeedMixs: No feedMixs to remove")
	}
	for _, feedMixStructID := range feedMixIDIndex.IDs {
		_, err = fdMx.deleteFeedMix(stub, feedMixStructID)
		if err != nil {
			return Error(http.StatusInternalServerError, "Failed to remove FeedMix with ID: "+feedMixStructID)
		}
		_, err = fdMx.deleteFeedMixIDIndex(stub, feedMixStructID)
		if err != nil {
			return Error(http.StatusInternalServerError, err.Error())
		}
	}
	fdMx.initHolder(stub)
	return Success(http.StatusOK, "removeAllFeedMixs: Successfully removed all FeedMixs", nil)
}

//Query Route: readFeedMix
func (fdMx *FeedMixAsset) readFeedMix(stub shim.ChaincodeStubInterface, feedMixID string) peer.Response {
	feedMixAsByteArray, err := fdMx.retrieveFeedMix(stub, feedMixID)
	if err != nil {
		return Error(http.StatusInternalServerError, err.Error())
	}
	return Success(http.StatusOK, "readFeedMix: Successfully read FeedMix with ID: "+feedMixID, feedMixAsByteArray)
}

//Query Route: readAllFeedMixs
func (fdMx *FeedMixAsset) readAllFeedMixs(stub shim.ChaincodeStubInterface) peer.Response {
	var feedMixIDs FeedMixIDIndex
	bytes, err := stub.GetState("feedMixIDIndex")
	if err != nil {
		return Error(http.StatusInternalServerError, "readAllFeedMixs: Error getting feedMixIDIndex array")
	}
	err = json.Unmarshal(bytes, &feedMixIDs)
	if err != nil {
		return Error(http.StatusInternalServerError, "readAllFeedMixs: Error unmarshalling feedMixIDIndex array JSON")
	}
	result := "["

	var feedMixAsByteArray []byte

	for _, feedMixID := range feedMixIDs.IDs {
		feedMixAsByteArray, err = fdMx.retrieveFeedMix(stub, feedMixID)
		if err != nil {
			return Error(http.StatusNotFound, "Failed to retrieve feedMix with ID: "+feedMixID)
		}
		result += string(feedMixAsByteArray) + ","
	}
	if len(result) == 1 {
		result = "[]"
	} else {
		result = result[:len(result)-1] + "]"
	}
	return Success(http.StatusOK, "OK", []byte(result))
}

//GetFeedMixHistory - reads history of a feedMix with specified ID
func (fdMx *FeedMixAsset) getFeedMixHistory(stub shim.ChaincodeStubInterface, args []string) peer.Response {

	resultsIterator, err := stub.GetHistoryForKey(args[0])
	if err != nil {
		return Error(http.StatusNotFound, "Not Found")
	}
	defer resultsIterator.Close()
	// Write return buffer
	var buffer bytes.Buffer
	buffer.WriteString("{ \"values\": [")
	for resultsIterator.HasNext() {
		it, _ := resultsIterator.Next()
		if buffer.Len() > 15 {
			buffer.WriteString(",")
		}
		buffer.WriteString("{\"timestamp\":\"")
		buffer.WriteString(time.Unix(it.Timestamp.Seconds, int64(it.Timestamp.Nanos)).Format(time.Stamp))
		buffer.WriteString("\",\"feedMix\": ")
		buffer.WriteString(string(it.Value))
		buffer.WriteString("}")
	}
	buffer.WriteString("]}")
	return Success(http.StatusOK, "OK", buffer.Bytes())
}

// Search for all matching IDs, given a (regex) value expression and return both the IDs and text.
// For example: '^H.llo' will match any string starting with 'Hello' or 'Hallo'.
func (fdMx *FeedMixAsset) searchFeedMixsByID(stub shim.ChaincodeStubInterface, args []string) peer.Response {

	searchString := strings.Replace(args[0], "\"", ".", -1) // protect against SQL injection

	// stub.GetQueryResult takes a verbatim CouchDB (assuming this is used DB). See CouchDB documentation:
	//     http://docs.couchdb.org/en/2.0.0/api/database/find.html
	// For example:
	//	{
	//		"selector": {
	//			"value": {"$regex": %s"}
	//		},
	//		"fields": ["ID","value"],
	//		"limit":  99
	//	}
	queryString := fmt.Sprintf("{\"selector\": {\"ID\": {\"$regex\": \"%s\"}}, \"fields\": [\"ID\", \"docType\", \"status\", \"creationDate\", \"supplierID\":\"supplierID001\", \"supplierLandDeedID\":\"supplierLandDeedID001\", \"supplierInvoiceID\":\"supplierInvoiceID001\", \"purchaseOrderID\":\"purchaseOrderID001\", \"goodsReceiptID\":\"goodsReceiptID001\", \"materialID\":\"materialID001\", \"inputType\":\"inputType001\", \"testResultID\":\"testResultID001\", \"testDate\":\"testDate001\", \"testCertificateID\":\"testCertificateID001\", \"humidity\":\"humidity001\", \"isContaminated\":\"isContaminated001\"], \"limit\":99, \"execution_stats\": true}", strings.Replace(searchString, "\"", ".", -1))
	resultsIterator, err := stub.GetQueryResult(queryString)
	if err != nil {
		return Error(http.StatusInternalServerError, err.Error())
	}
	defer resultsIterator.Close()

	// Write return buffer
	var buffer bytes.Buffer
	buffer.WriteString("{ \"values\": [")
	for resultsIterator.HasNext() {
		it, _ := resultsIterator.Next()
		if buffer.Len() > 15 {
			buffer.WriteString(",")
		}
		buffer.WriteString("{\"ID\":\"")
		buffer.WriteString(it.Key)
		buffer.WriteString("\",\"feedMix\": ")
		buffer.WriteString(string(it.Value))
		buffer.WriteString("}")
	}
	buffer.WriteString("]}")
	return Success(http.StatusOK, "OK", buffer.Bytes())
}

//Helper: Save FeedMixAsset
func (fdMx *FeedMixAsset) saveFeedMix(stub shim.ChaincodeStubInterface, feedMix FeedMix) (bool, error) {
	bytes, err := json.Marshal(feedMix)
	if err != nil {
		return false, errors.New("Error converting feedMix record JSON")
	}
	err = stub.PutState(feedMix.ID, bytes)
	if err != nil {
		return false, errors.New("Error storing FeedMix record")
	}
	return true, nil
}

//Helper: delete FeedMixAsset
func (fdMx *FeedMixAsset) deleteFeedMix(stub shim.ChaincodeStubInterface, feedMixID string) (bool, error) {
	_, err := fdMx.retrieveFeedMix(stub, feedMixID)
	if err != nil {
		return false, errors.New("FeedMix with ID: " + feedMixID + " not found")
	}
	err = stub.DelState(feedMixID)
	if err != nil {
		return false, errors.New("Error deleting FeedMix record")
	}
	return true, nil
}

//Helper: Update feedMix Holder - updates Index
func (fdMx *FeedMixAsset) updateFeedMixIDIndex(stub shim.ChaincodeStubInterface, feedMix FeedMix) (bool, error) {
	var feedMixIDs FeedMixIDIndex
	bytes, err := stub.GetState("feedMixIDIndex")
	if err != nil {
		return false, errors.New("updateFeedMixIDIndex: Error getting feedMixIDIndex array Index from state")
	}
	err = json.Unmarshal(bytes, &feedMixIDs)
	if err != nil {
		return false, errors.New("updateFeedMixIDIndex: Error unmarshalling feedMixIDIndex array JSON")
	}
	feedMixIDs.IDs = append(feedMixIDs.IDs, feedMix.ID)
	bytes, err = json.Marshal(feedMixIDs)
	if err != nil {
		return false, errors.New("updateFeedMixIDIndex: Error marshalling new feedMix ID")
	}
	err = stub.PutState("feedMixIDIndex", bytes)
	if err != nil {
		return false, errors.New("updateFeedMixIDIndex: Error storing new feedMix ID in feedMixIDIndex (Index)")
	}
	return true, nil
}

//Helper: delete ID from feedMixStruct Holder
func (fdMx *FeedMixAsset) deleteFeedMixIDIndex(stub shim.ChaincodeStubInterface, feedMixID string) (bool, error) {
	var feedMixIDIndex FeedMixIDIndex
	bytes, err := stub.GetState("feedMixIDIndex")
	if err != nil {
		return false, errors.New("deleteFeedMixIDIndex: Error getting feedMixIDIndex array Index from state")
	}
	err = json.Unmarshal(bytes, &feedMixIDIndex)
	if err != nil {
		return false, errors.New("deleteFeedMixIDIndex: Error unmarshalling feedMixIDIndex array JSON")
	}
	feedMixIDIndex.IDs, err = deleteKeyFromStringArray(feedMixIDIndex.IDs, feedMixID)
	if err != nil {
		return false, errors.New(err.Error())
	}
	bytes, err = json.Marshal(feedMixIDIndex)
	if err != nil {
		return false, errors.New("deleteFeedMixIDIndex: Error marshalling new feedMixStruct ID")
	}
	err = stub.PutState("feedMixIDIndex", bytes)
	if err != nil {
		return false, errors.New("deleteFeedMixIDIndex: Error storing new feedMixStruct ID in feedMixIDIndex (Index)")
	}
	return true, nil
}

//Helper: Initialize truck ID Holder //change template
func (fdMx *FeedMixAsset) initHolder(stub shim.ChaincodeStubInterface) bool {
	var feedMixIDIndex FeedMixIDIndex
	bytes, _ := json.Marshal(feedMixIDIndex)
	stub.DelState("feedMixIDIndex")
	stub.PutState("feedMixIDIndex", bytes)
	return true
}

//deleteKeyFromArray
func deleteKeyFromStringArray(array []string, key string) (newArray []string, err error) {
	for _, entry := range array {
		if entry != key {
			newArray = append(newArray, entry)
		}
	}
	if len(newArray) == len(array) {
		return newArray, errors.New("Specified Key: " + key + " not found in Array")
	}
	return newArray, nil
}

//Helper: Retrieve
func (fdMx *FeedMixAsset) retrieveFeedMix(stub shim.ChaincodeStubInterface, feedMixID string) ([]byte, error) {
	var feedMix FeedMix
	var feedMixAsByteArray []byte
	bytes, err := stub.GetState(feedMixID)
	if err != nil {
		return feedMixAsByteArray, errors.New("retrieveFeedMix: Error retrieving feedMix with ID: " + feedMixID)
	}
	err = json.Unmarshal(bytes, &feedMix)
	if err != nil {
		return feedMixAsByteArray, errors.New("retrieveFeedMix: Corrupt feedMix record " + string(bytes))
	}
	feedMixAsByteArray, err = json.Marshal(feedMix)
	if err != nil {
		return feedMixAsByteArray, errors.New("readFeedMix: Invalid feedMix Object - Not a  valid JSON")
	}
	return feedMixAsByteArray, nil
}

//getFeedMixFromArgs - construct a feedMix structure from string array of arguments
func getFeedMixFromArgs(args []string) (feedMix FeedMix, err error) {
	if !gjson.Valid(args[0]) {
		return feedMix, errors.New("Invalid json")
	}

	err = gjson.Unmarshal([]byte(args[0]), &feedMix)
	if err != nil {
		return feedMix, err
	}
	return feedMix, nil
}
