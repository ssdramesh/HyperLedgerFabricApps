package main

//Template Version: 1.1-20180704
import (
	"bytes"
	"encoding/json"
	"fmt"
	"strings"
	"testing"

	"github.com/hyperledger/fabric/core/chaincode/shim"
)

// History/Search
// ... the History and Search API calls are not supported in the mock interface

//TestFeedMixAsset_Init
func TestFeedMixAsset_Init(t *testing.T) {
	feedMix := new(FeedMixAsset)
	stub := shim.NewMockStub("feedMix", feedMix)
	checkInit(t, stub, [][]byte{[]byte("init")})
	checkState(t, stub, "feedMixIDIndex", getExpectedFeedMixIDIndex("init"))
}

//TestFeedMixAsset_InvokeUnknownFunction
func TestFeedMixAsset_InvokeUnknownFunction(t *testing.T) {
	feedMix := new(FeedMixAsset)
	stub := shim.NewMockStub("feedMix", feedMix)
	checkInit(t, stub, [][]byte{[]byte("init")})
	checkInvokeUnknownFunction(t, stub, [][]byte{[]byte("myFunction"), []byte("docType:Asset")})
}

//TestFeedMixAsset_Invoke_addNewFeedMix
func TestFeedMixAsset_Invoke_addNewFeedMixOK(t *testing.T) {
	feedMix := new(FeedMixAsset)
	stub := shim.NewMockStub("feedMix", feedMix)
	checkInit(t, stub, [][]byte{[]byte("init")})
	checkInvoke(t, stub, getFirstFeedMixAssetForTesting())
	newFeedMixID := "100001"
	checkState(t, stub, newFeedMixID, getNewFeedMixExpected())
	checkState(t, stub, "feedMixIDIndex", getExpectedFeedMixIDIndex("addNewFeedMix"))
}

//TestFeedMixAsset_Invoke_addNewFeedMix
func TestFeedMixAsset_Invoke_addNewFeedMixDuplicate(t *testing.T) {
	feedMix := new(FeedMixAsset)
	stub := shim.NewMockStub("feedMix", feedMix)
	checkInit(t, stub, [][]byte{[]byte("init")})
	checkInvoke(t, stub, getFirstFeedMixAssetForTesting())
	newFeedMixID := "100001"
	checkState(t, stub, newFeedMixID, getNewFeedMixExpected())
	checkState(t, stub, "feedMixIDIndex", getExpectedFeedMixIDIndex("addNewFeedMix"))
	res := stub.MockInvoke("1", getFirstFeedMixAssetForTesting())
	if res.Status != shim.OK {
		checkError(t, "This FeedMix already exists: 100001", res.Message)
	} else {
		fmt.Println("Error was expected, but not raised")
		t.FailNow()
	}
}

func TestFeedMixAsset_Invoke_updateFeedMixOK(t *testing.T) {
	feedMix := new(FeedMixAsset)
	stub := shim.NewMockStub("feedMix", feedMix)
	checkInit(t, stub, [][]byte{[]byte("init")})
	checkInvoke(t, stub, getFirstFeedMixAssetForTesting())
	newFeedMixID := "100001"
	checkState(t, stub, newFeedMixID, getNewFeedMixExpected())
	checkInvoke(t, stub, getFirstFeedMixAssetForUpdateTestingOK())
	checkReadFeedMixAssetAfterUpdateOK(t, stub, newFeedMixID)
}

//TestFeedMixAsset_Invoke_removeFeedMixOK  //change template
func TestFeedMixAsset_Invoke_removeFeedMixOK(t *testing.T) {
	feedMix := new(FeedMixAsset)
	stub := shim.NewMockStub("feedMix", feedMix)
	checkInit(t, stub, [][]byte{[]byte("init")})
	checkInvoke(t, stub, getFirstFeedMixAssetForTesting())
	checkInvoke(t, stub, getSecondFeedMixAssetForTesting())
	checkReadAllFeedMixsOK(t, stub)
	checkState(t, stub, "feedMixIDIndex", getExpectedFeedMixIDIndex("beforeRemoveFeedMix"))
	checkInvoke(t, stub, getRemoveSecondFeedMixAssetForTesting())
	remainingFeedMixID := "100001"
	checkReadFeedMixOK(t, stub, remainingFeedMixID)
	checkState(t, stub, "feedMixIDIndex", getExpectedFeedMixIDIndex("afterRemoveFeedMix"))
}

//TestFeedMixAsset_Invoke_removeFeedMixNOK  //change template
func TestFeedMixAsset_Invoke_removeFeedMixNOK(t *testing.T) {
	feedMix := new(FeedMixAsset)
	stub := shim.NewMockStub("feedMix", feedMix)
	checkInit(t, stub, [][]byte{[]byte("init")})
	checkInvoke(t, stub, getFirstFeedMixAssetForTesting())
	firstFeedMixID := "100001"
	checkReadFeedMixOK(t, stub, firstFeedMixID)
	checkState(t, stub, "feedMixIDIndex", getExpectedFeedMixIDIndex("addNewFeedMix"))
	res := stub.MockInvoke("1", getRemoveSecondFeedMixAssetForTesting())
	if res.Status != shim.OK {
		checkError(t, "FeedMix with ID: "+"100002"+" not found", res.Message)
	} else {
		fmt.Println("Error was expected, but not raised")
		t.FailNow()
	}
	checkState(t, stub, "feedMixIDIndex", getExpectedFeedMixIDIndex("addNewFeedMix"))
}

//TestFeedMixAsset_Invoke_removeAllFeedMixsOK  //change template
func TestFeedMixAsset_Invoke_removeAllFeedMixsOK(t *testing.T) {
	feedMix := new(FeedMixAsset)
	stub := shim.NewMockStub("feedMix", feedMix)
	checkInit(t, stub, [][]byte{[]byte("init")})
	checkInvoke(t, stub, getFirstFeedMixAssetForTesting())
	checkInvoke(t, stub, getSecondFeedMixAssetForTesting())
	checkReadAllFeedMixsOK(t, stub)
	checkState(t, stub, "feedMixIDIndex", getExpectedFeedMixIDIndex("beforeRemoveFeedMix"))
	checkInvoke(t, stub, getRemoveAllFeedMixAssetsForTesting())
	checkState(t, stub, "feedMixIDIndex", getExpectedFeedMixIDIndex(""))
}

//TestFeedMixAsset_Invoke_removeFeedMixNOK  //change template
func TestFeedMixAsset_Invoke_removeAllFeedMixsNOK(t *testing.T) {
	feedMix := new(FeedMixAsset)
	stub := shim.NewMockStub("feedMix", feedMix)
	checkInit(t, stub, [][]byte{[]byte("init")})
	res := stub.MockInvoke("1", getRemoveAllFeedMixAssetsForTesting())
	if res.Status != shim.OK {
		checkError(t, "removeAllFeedMixs: No feedMixs to remove", res.Message)
	} else {
		fmt.Println("Error was expected, but not raised")
		t.FailNow()
	}
	checkState(t, stub, "feedMixIDIndex", getExpectedFeedMixIDIndex(""))
}

//TestFeedMixAsset_Query_readFeedMix
func TestFeedMixAsset_Query_readFeedMix(t *testing.T) {
	feedMix := new(FeedMixAsset)
	stub := shim.NewMockStub("feedMix", feedMix)
	checkInit(t, stub, [][]byte{[]byte("init")})
	feedMixID := "100001"
	checkInvoke(t, stub, getFirstFeedMixAssetForTesting())
	checkReadFeedMixOK(t, stub, feedMixID)
	checkReadFeedMixNOK(t, stub, "")
}

//TestFeedMixAsset_Query_readAllFeedMixs
func TestFeedMixAsset_Query_readAllFeedMixs(t *testing.T) {
	feedMix := new(FeedMixAsset)
	stub := shim.NewMockStub("feedMix", feedMix)
	checkInit(t, stub, [][]byte{[]byte("init")})
	checkInvoke(t, stub, getFirstFeedMixAssetForTesting())
	checkInvoke(t, stub, getSecondFeedMixAssetForTesting())
	checkReadAllFeedMixsOK(t, stub)
}

/*
*
*	Helper Functions
*
 */
//Get first FeedMixAsset for testing
func getFirstFeedMixAssetForTesting() [][]byte {
	return [][]byte{[]byte("addNewFeedMix"),
		[]byte("{\"ID\":\"100001\",\"docType\":\"Asset.FeedMixAsset\",\"status\":\"0\",\"creationDate\":\"12/01/2018\",\"supplier\":{\"ID\":\"supplierID001\",\"landDeedID\":\"landDeedID001\",\"invoiceID\":\"invoiceID001\"},\"purchaser\":{\"ID\":\"purchaserID001\",\"purchaseOrderID\":\"purchaseOrderID001\",\"goodsReceiptID\":\"goodsReceiptID001\",\"materialID\":\"materialID001\"},\"testResult\":{\"ID\":\"testResultID001\",\"certificateID\":\"certificateID001\",\"date\":\"12/01/2018\"},\"quality\":{\"humidity\":\"humidity001\",\"contaminationPercent\":\"contaminationPercent001\"},\"inputType\":\"inputType001\"}")}
}

//Get first FeedMixAsset for update testing
func getFirstFeedMixAssetForUpdateTestingOK() [][]byte {
	return [][]byte{[]byte("updateFeedMix"),
		[]byte("{\"ID\":\"100001\",\"docType\":\"Asset.FeedMixAsset\",\"status\":\"1\",\"creationDate\":\"12/01/2018\",\"supplier\":{\"ID\":\"supplierID001\",\"landDeedID\":\"landDeedID001\",\"invoiceID\":\"invoiceID001\"},\"purchaser\":{\"ID\":\"purchaserID001\",\"purchaseOrderID\":\"purchaseOrderID001\",\"goodsReceiptID\":\"goodsReceiptID001\",\"materialID\":\"materialID001\"},\"testResult\":{\"ID\":\"testResultID001\",\"certificateID\":\"certificateID001\",\"date\":\"12/01/2018\"},\"quality\":{\"humidity\":\"humidity001\",\"contaminationPercent\":\"contaminationPercent001\"},\"inputType\":\"inputType001\"}")}
}

//Get second FeedMixAsset for testing
func getSecondFeedMixAssetForTesting() [][]byte {
	return [][]byte{[]byte("addNewFeedMix"),
		[]byte("{\"ID\":\"100002\",\"docType\":\"Asset.FeedMixAsset\",\"status\":\"0\",\"creationDate\":\"12/01/2018\",\"supplier\":{\"ID\":\"supplierID002\",\"landDeedID\":\"landDeedID002\",\"invoiceID\":\"invoiceID002\"},\"purchaser\":{\"ID\":\"purchaserID002\",\"purchaseOrderID\":\"purchaseOrderID002\",\"goodsReceiptID\":\"goodsReceiptID002\",\"materialID\":\"materialID002\"},\"testResult\":{\"ID\":\"testResultID002\",\"certificateID\":\"certificateID002\",\"date\":\"12/01/2018\"},\"quality\":{\"humidity\":\"humidity002\",\"contaminationPercent\":\"contaminationPercent002\"},\"inputType\":\"inputType002\"}")}
}

//Get remove second FeedMixAsset for testing //change template
func getRemoveSecondFeedMixAssetForTesting() [][]byte {
	return [][]byte{[]byte("removeFeedMix"),
		[]byte("100002")}
}

//Get remove all FeedMixAssets for testing //change template
func getRemoveAllFeedMixAssetsForTesting() [][]byte {
	return [][]byte{[]byte("removeAllFeedMixs")}
}

//Get an expected value for testing
func getNewFeedMixExpected() []byte {
	var feedMix FeedMix
	var supplier FeedMixSupplier
	var purchaser FeedMixPurchaser
	var testResult FeedMixTestResult
	var quality FeedMixQuality

	supplier.ID = "supplierID001"
	supplier.LandDeedID = "landDeedID001"
	supplier.InvoiceID = "invoiceID001"

	purchaser.ID = "purchaserID001"
	purchaser.PurchaseOrderID = "purchaseOrderID001"
	purchaser.GoodsReceiptID = "goodsReceiptID001"
	purchaser.MaterialID = "materialID001"

	testResult.ID = "testResultID001"
	testResult.CertificateID = "certificateID001"
	testResult.Date = "12/01/2018"

	quality.Hummidity = "humidity001"
	quality.ContaminationPercent = "contaminationPercent001"

	feedMix.ID = "100001"
	feedMix.ObjectType = "Asset.FeedMixAsset"
	feedMix.Status = "0"
	feedMix.CreationDate = "12/01/2018"
	feedMix.Supplier = supplier
	feedMix.Purchaser = purchaser
	feedMix.TestResult = testResult
	feedMix.Quality = quality
	feedMix.InputType = "inputType001"

	feedMixJSON, err := json.Marshal(feedMix)
	if err != nil {
		fmt.Println("Error converting a FeedMix record to JSON")
		return nil
	}
	return []byte(feedMixJSON)
}

//Get an expected value for testing
func getUpdatedFeedMixExpected() []byte {
	var feedMix FeedMix
	var supplier FeedMixSupplier
	var purchaser FeedMixPurchaser
	var testResult FeedMixTestResult
	var quality FeedMixQuality

	supplier.ID = "supplierID001"
	supplier.LandDeedID = "landDeedID001"
	supplier.InvoiceID = "invoiceID001"

	purchaser.ID = "purchaserID001"
	purchaser.PurchaseOrderID = "purchaseOrderID001"
	purchaser.GoodsReceiptID = "goodsReceiptID001"
	purchaser.MaterialID = "materialID001"

	testResult.ID = "testResultID001"
	testResult.CertificateID = "certificateID001"
	testResult.Date = "12/01/2018"

	quality.Hummidity = "humidity001"
	quality.ContaminationPercent = "isContaminated001"

	feedMix.ID = "100001"
	feedMix.ObjectType = "Asset.FeedMixAsset"
	feedMix.Status = "1"
	feedMix.CreationDate = "12/01/2018"
	feedMix.Supplier = supplier
	feedMix.Purchaser = purchaser
	feedMix.TestResult = testResult
	feedMix.Quality = quality
	feedMix.InputType = "inputType001"

	feedMixJSON, err := json.Marshal(feedMix)
	if err != nil {
		fmt.Println("Error converting a FeedMix record to JSON")
		return nil
	}
	return []byte(feedMixJSON)
}

//Get expected values of FeedMixs for testing
func getExpectedFeedMixs() []byte {
	var feedMixs []FeedMix
	var feedMix FeedMix
	var supplier FeedMixSupplier
	var purchaser FeedMixPurchaser
	var testResult FeedMixTestResult
	var quality FeedMixQuality

	supplier.ID = "supplierID001"
	supplier.LandDeedID = "landDeedID001"
	supplier.InvoiceID = "invoiceID001"

	purchaser.ID = "purchaserID001"
	purchaser.PurchaseOrderID = "purchaseOrderID001"
	purchaser.GoodsReceiptID = "goodsReceiptID001"
	purchaser.MaterialID = "materialID001"

	testResult.ID = "testResultID001"
	testResult.CertificateID = "certificateID001"
	testResult.Date = "12/01/2018"

	quality.Hummidity = "humidity001"
	quality.ContaminationPercent = "contaminationPercent001"

	feedMix.ID = "100001"
	feedMix.ObjectType = "Asset.FeedMixAsset"
	feedMix.Status = "0"
	feedMix.CreationDate = "12/01/2018"
	feedMix.Supplier = supplier
	feedMix.Purchaser = purchaser
	feedMix.TestResult = testResult
	feedMix.Quality = quality
	feedMix.InputType = "inputType001"

	feedMixs = append(feedMixs, feedMix)

	supplier.ID = "supplierID002"
	supplier.LandDeedID = "landDeedID002"
	supplier.InvoiceID = "invoiceID002"

	purchaser.ID = "purchaserID002"
	purchaser.PurchaseOrderID = "purchaseOrderID002"
	purchaser.GoodsReceiptID = "goodsReceiptID002"
	purchaser.MaterialID = "materialID002"

	testResult.ID = "testResultID002"
	testResult.CertificateID = "certificateID002"
	testResult.Date = "12/01/2018"

	quality.Hummidity = "humidity002"
	quality.ContaminationPercent = "contaminationPercent002"

	feedMix.ID = "100002"
	feedMix.ObjectType = "Asset.FeedMixAsset"
	feedMix.Status = "0"
	feedMix.CreationDate = "12/01/2018"
	feedMix.Supplier = supplier
	feedMix.Purchaser = purchaser
	feedMix.TestResult = testResult
	feedMix.Quality = quality
	feedMix.InputType = "inputType002"

	feedMixs = append(feedMixs, feedMix)
	feedMixJSON, err := json.Marshal(feedMixs)
	if err != nil {
		fmt.Println("Error converting feedMix records to JSON")
		return nil
	}
	return []byte(feedMixJSON)
}

func getExpectedFeedMixIDIndex(funcName string) []byte {
	var feedMixIDIndex FeedMixIDIndex
	switch funcName {
	case "addNewFeedMix":
		feedMixIDIndex.IDs = append(feedMixIDIndex.IDs, "100001")
		feedMixIDIndexBytes, err := json.Marshal(feedMixIDIndex)
		if err != nil {
			fmt.Println("Error converting FeedMixIDIndex to JSON")
			return nil
		}
		return feedMixIDIndexBytes
	case "beforeRemoveFeedMix":
		feedMixIDIndex.IDs = append(feedMixIDIndex.IDs, "100001")
		feedMixIDIndex.IDs = append(feedMixIDIndex.IDs, "100002")
		feedMixIDIndexBytes, err := json.Marshal(feedMixIDIndex)
		if err != nil {
			fmt.Println("Error converting FeedMixIDIndex to JSON")
			return nil
		}
		return feedMixIDIndexBytes
	case "afterRemoveFeedMix":
		feedMixIDIndex.IDs = append(feedMixIDIndex.IDs, "100001")
		feedMixIDIndexBytes, err := json.Marshal(feedMixIDIndex)
		if err != nil {
			fmt.Println("Error converting FeedMixIDIndex to JSON")
			return nil
		}
		return feedMixIDIndexBytes
	default:
		feedMixIDIndexBytes, err := json.Marshal(feedMixIDIndex)
		if err != nil {
			fmt.Println("Error converting FeedMixIDIndex to JSON")
			return nil
		}
		return feedMixIDIndexBytes
	}
}

//checkInit - helper to check the Initialization of chaincode: FeedMixAsset
func checkInit(t *testing.T, stub *shim.MockStub, args [][]byte) {
	res := stub.MockInit("1", args)
	if res.Status != shim.OK {
		fmt.Println("Init failed", string(res.Message))
		t.FailNow()
	}
}

//checkState - helper for checking the chaincode state for a given stateKey afgainst an expected value
func checkState(t *testing.T, stub *shim.MockStub, stateKey string, expectedState []byte) {
	actualState := stub.State[stateKey]
	if actualState == nil {
		fmt.Println("State for ", stateKey, ": failed to get value")
		t.FailNow()
	}

	if bytes.Compare(actualState, expectedState) != 0 {
		fmt.Println("Incorrect State for " + stateKey + ": \nExpected: " + string(expectedState) + "\nActual  : " + string(actualState))
		t.FailNow()
	}
}

//checkInvoke - helper for checking Invoke of chaincode
func checkInvoke(t *testing.T, stub *shim.MockStub, args [][]byte) {
	res := stub.MockInvoke("1", args)
	if res.Status != shim.OK {
		fmt.Println("Invoke", args, "failed", string(res.Message))
		t.FailNow()
	}
}

func checkInvokeUnknownFunction(t *testing.T, stub *shim.MockStub, args [][]byte) {
	res := stub.MockInvoke("1", args)
	if res.Status != shim.OK {
		expectedErr := "Received unknown function invocation"
		actualErr := string(res.Message)
		if !(strings.Contains(actualErr, expectedErr)) {
			fmt.Println("Invoke with unknown function must throw error: ", expectedErr, "\n Actual Error :", actualErr)
			t.FailNow()
		}
	} else {
		t.FailNow()
	}
}

//checkReadFeedMixOK - helper for positive test readFeedMix
func checkReadFeedMixOK(t *testing.T, stub *shim.MockStub, feedMixID string) {
	res := stub.MockInvoke("1", [][]byte{[]byte("readFeedMix"), []byte(feedMixID)})
	if res.Status != shim.OK {
		fmt.Println("func readFeedMix with ID: ", feedMixID, " failed"+string(res.Message))
		t.FailNow()
	}
	if res.Payload == nil {
		fmt.Println("func readFeedMix with ID: ", feedMixID, "failed to get value")
		t.FailNow()
	}
	if bytes.Compare(getNewFeedMixExpected(), []byte(res.Payload)) != 0 {
		fmt.Println("func readFeedMix with ID: ", feedMixID, "Expected:", string(getNewFeedMixExpected()), "Actual:", string(res.Payload))
		t.FailNow()
	}
}

//checkReadFeedMixNOK - helper for negative testing of readFeedMix
func checkReadFeedMixNOK(t *testing.T, stub *shim.MockStub, feedMixID string) {
	//with no feedMixID
	res := stub.MockInvoke("1", [][]byte{[]byte("readFeedMix"), []byte("")})
	if res.Status != shim.OK {
		expectedErr := "retrieveFeedMix: Corrupt feedMix record "
		actualErr := string(res.Message)
		if !(strings.Contains(actualErr, expectedErr)) {
			fmt.Println("func readFeedMix negative test: ", "Expected Error:", expectedErr, "Actual Error", actualErr)
			t.FailNow()
		}
	} else {
		t.FailNow()
	}
}

//checkReadFeedMixAssetAfterUpdateOK - helper for positive test readFeedMix after update
func checkReadFeedMixAssetAfterUpdateOK(t *testing.T, stub *shim.MockStub, FeedMixID string) {
	res := stub.MockInvoke("1", [][]byte{[]byte("readFeedMix"), []byte(FeedMixID)})
	if res.Status != shim.OK {
		fmt.Println("func readFeedMix with ID: ", FeedMixID, " failed"+string(res.Message))
		t.FailNow()
	}
	if res.Payload == nil {
		fmt.Println("func readFeedMix with ID: ", FeedMixID, "failed to get value")
		t.FailNow()
	}
	if bytes.Compare(getUpdatedFeedMixExpected(), []byte(res.Payload)) != 0 {
		fmt.Println("func readFeedMix with ID: ", FeedMixID, "Expected:", string(getUpdatedFeedMixExpected()), "Actual:", string(res.Payload))
		t.FailNow()
	}
}

func checkReadAllFeedMixsOK(t *testing.T, stub *shim.MockStub) {
	res := stub.MockInvoke("1", [][]byte{[]byte("readAllFeedMixs")})
	if res.Status != shim.OK {
		fmt.Println("func readAllFeedMixs failed", string(res.Message))
		t.FailNow()
	}
	if res.Payload == nil {
		fmt.Println("func readAllFeedMixs failed to get value")
		t.FailNow()
	}
	if bytes.Compare(getExpectedFeedMixs(), []byte(res.Payload)) != 0 {
		fmt.Println("func readAllFeedMixs Expected:\n", string(getExpectedFeedMixs()), "\nActual:\n", string(res.Payload))
		t.FailNow()
	}
}

func checkError(t *testing.T, exp string, act string) {
	if strings.Compare(exp, act) != 0 {
		fmt.Println("Unexpected Error! Expecting ", exp, "\n Actual :", act)
		t.FailNow()
	}
}
