swagger: "2.0"

info:
  description: "The FeedMixAsset chaincode can read/write FeedMixAssets
    onto the blockchain and can expose these functions as REST API.
    THIS SAMPLE CODE MAY BE USED SOLELY AS PART OF THE TEST AND EVALUATION OF THE SAP CLOUD PLATFORM
    HYPERLEDGER SERVICE (THE “SERVICE”) AND IN ACCORDANCE WITH THE AGREEMENT FOR THE SERVICE.
    THIS SAMPLE CODE PROVIDED “AS IS”, WITHOUT ANY WARRANTY, ESCROW, TRAINING, MAINTENANCE, OR
    SERVICE OBLIGATIONS WHATSOEVER ON THE PART OF SAP."
  version: "1.0"
  title: "FeedMixAsset"

consumes:
  - application/json

parameters:
  id:
    name: id
    in: path
    description: ID of the FeedMixAsset
    required: true
    type: string
    maxLength: 64

definitions:

  feedMixSupplier:
    type: object
    properties:
      ID:
        type: string
      landDeedID:
        type: string
      invoiceID:
        type: string

  feedMixPurchaser:
    type: object
    properties:
      ID:
        type: string
      purchaseOrderID:
        type: string
      goodsReceiptID:
        type: string
      materialID:
        type: string

  feedMixTestResult:
    type: object
    properties:
      ID:
        type: string
      certificateID:
        type: string
      date:
        type: string

  feedMixQuality:
    type: object
    properties:
      humidity:
        type: string
      contaminationPercent:
        type: string

  feedMixAsset:
    type: object
    properties:
      ID:
        type: string
      docType:
        type: string
      status:
        type: string
      creationDate:
        type: string
      supplier:
        $ref: '#/definitions/feedMixSupplier'
      purchaser:
        $ref: '#/definitions/feedMixPurchaser'
      testResult:
        $ref: '#/definitions/feedMixTestResult'
      quality:
        $ref: '#/definitions/feedMixQuality'
      inputType:
        type: string

  feedMixAssetHistory:
    type: object
    properties:
      values:
        type: array
        items:
          type: object
          properties:
            timestamp:
              type: string
            feedMix:
              $ref: '#/definitions/feedMixAsset'

  feedMixAssetByIDSearchResult:
    type: object
    properties:
      values:
        type: array
        items:
          type: object
          properties:
            ID:
              type: string
            feedMix:
              $ref: '#/definitions/feedMixAsset'

paths:

  /:
    get:
      operationId: readAllFeedMixs
      summary: Read all (existing) FeedMixAssets
      produces:
      - application/json
      responses:
        200:
          description: OK
        500:
          description: Failed

    post:
      operationId: addNewFeedMix
      summary: Adds a new FeedMixAsset
      consumes:
      - application/json
      parameters:
      - in: body
        name: newFeedMixAsset
        description: New FeedMixAsset
        required: true
        schema:
          $ref: '#/definitions/feedMixAsset'
      responses:
        200:
          description: FeedMixAsset Written
        500:
          description: Failed

  /update:
    post:
      operationId: updateFeedMix
      summary: Update FeedMixAsset
      consumes:
      - application/json
      parameters:
      - in: body
        name: Update FeedMixAsset
        description: FeedMixAsset for Update
        required: true
        schema:
          $ref: '#/definitions/feedMixAsset'
      responses:
        200:
          description: FeedMixAsset Updated
        500:
          description: Failed

  /clear:

    delete:
      operationId: removeAllFeedMixs
      summary: Remove all FeedMixAssets
      produces:
      - application/json
      responses:
        200:
          description: OK
        500:
          description: Failed

  /{id}:

    get:
      operationId: readFeedMix
      summary: Read FeedMixAsset by FeedMixAsset ID
      parameters:
      - $ref: '#/parameters/id'
      produces:
      - application/json
      responses:
        200:
          description: OK
        500:
          description: Failed
    delete:
      operationId: removeFeedMix
      summary: Remove FeedMixAsset by FeedMixAsset ID
      parameters:
      - $ref: '#/parameters/id'
      produces:
      - application/json
      responses:
        200:
          description: OK
        500:
          description: Failed

  /{id}/history:

    get:
      operationId: getFeedMixHistory
      summary: Return history of FeedMix by ID
      parameters:
      - $ref: '#/parameters/id'
      produces:
      - application/json
      responses:
        200:
          $ref: '#/definitions/feedMixAssetHistory'
        400:
          description: Parameter Mismatch
        404:
          description: Not Found

  /searchFeedMixsByID/{wildcard}:

    get:
      operationId: searchFeedMixsByID
      summary: Find feedMixs by ID - supports wildcard search in the text strings
      description: Search for all matching IDs, given a (regex) value expression and return both the IDs and text. For example '^H.llo' will match any string starting with 'Hello' or 'Hallo'.
      parameters:
      - name: wildcard
        in: path
        description: Wildcard regular expression to match against texts
        required: true
        type: string
        maxLength: 64
      responses:
        200:
          $ref: '#/definitions/feedMixAssetByIDSearchResult'
        500:
          description: Internal Server Error
