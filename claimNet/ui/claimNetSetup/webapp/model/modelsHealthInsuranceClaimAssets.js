sap.ui.define([
	"sap/ui/model/json/JSONModel"
], function(JSONModel) {
	"use strict";

	return {

		createHealthInsuranceClaimAssetsModel: function(){

			var oModel = new JSONModel();
			oModel.setDefaultBindingMode("TwoWay");
			oModel.setData({
				chaincode:{
					serviceUrl:"",
					serviceKey:{},
					healthInsuranceClaimAsset:{}
				},
				healthInsuranceClaimAssetCollection	: {
					cols:[
						{name:"ID"},
						{name:"ObjectType"},
						{name:"Status"},
						{name:"CreationDate"},
            {name:"InsuranceType"},
            {name:"OtherInsuredPolicyID"},
            {name:"InsuredID"},
            {name:"PatientID"},
            {name:"PatientRelationship"},
            {name:"PatientMaritalStatus"},
            {name:"InsuranceProgram"},
            {name:"ClaimCode"},
            {name:"ResubmissionCode"},
            {name:"IllnessDate"},
            {name:"HospitalizationStartDate"},
            {name:"HospitalizationEndDate"},
            {name:"TotalClaimAmount"},
            {name:"TotalPaidAmount"},
            {name:"FederalTaxID"}
					],
					items:[]
				},
				selectedHealthInsuranceClaimAsset:{},
				newHealthInsuranceClaimAsset:{
					ID:"",
					ObjectType:"",
					Status:"",
					CreationDate:"",
          InsuranceType:"",
          OtherInsuredPolicyID:"",
          InsuredID:"",
          PatientID:"",
          PatientRelationship:"",
          PatientMaritalStatus:"",
          InsuranceProgram:"",
          ClaimCode:"",
          ResubmissionCode:"",
          IllnessDate:"",
          HospitalizationStartDate:"",
          HospitalizationEndDate:"",
          TotalClaimAmount:"",
          TotalPaidAmount:"",
          FederalTaxID:""
				},
				selectedHealthInsuranceClaimAssetID	: "",
				searchHealthInsuranceClaimAssetID : ""
			});
			return oModel;
		}
	};
});
