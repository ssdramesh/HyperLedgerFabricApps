sap.ui.define([
	"claimNetSetup/controller/BaseController",
	"claimNetSetup/util/bizNetAccessHealthInsuranceClaimAssets"
], function(
		BaseController,
		bizNetAccessHealthInsuranceClaimAssets
	) {
	"use strict";

	return BaseController.extend("claimNetSetup.controller.HealthInsuranceClaimAssetDetails", {

		onInit : function(){

			this.getOwnerComponent().getRouter().getRoute("healthInsuranceClaimAsset").attachPatternMatched(this._onObjectMatched, this);
		},

		_onObjectMatched : function (oEvent) {

			var pId = oEvent.getParameter("arguments").healthInsuranceClaimAssetId;
			if ( pId === "___new" ) {
				this.getView().byId("__barHealthInsuranceClaimAsset").setSelectedKey("New");
			} else {
				bizNetAccessHealthInsuranceClaimAssets.loadHealthInsuranceClaimAsset(this.getView().getModel("HealthInsuranceClaimAssets"), oEvent.getParameter("arguments").healthInsuranceClaimAssetId);
			}
		},

		addNew : function(){

			var oModel = this.getView().getModel("HealthInsuranceClaimAssets");
			if ( oModel.getProperty("/newHealthInsuranceClaimAsset/Alias") !== "" ||
				   oModel.getProperty("/newHealthInsuranceClaimAsset/Description") !== "" ) {
				var healthInsuranceClaimAssetId = bizNetAccessHealthInsuranceClaimAssets.addNewHealthInsuranceClaimAsset(this.getOwnerComponent(), oModel);
				this.getView().byId("__barHealthInsuranceClaimAsset").setSelectedKey("Details");
				bizNetAccessHealthInsuranceClaimAssets.loadHealthInsuranceClaimAsset(this.getView().getModel("HealthInsuranceClaimAssets"), healthInsuranceClaimAssetId);
			} else {
				sap.m.MessageToast.show("Please enter valid data in all fields", {});
			}
			this._clearNewAsset();
		},

		removeHealthInsuranceClaimAsset : function(){

			var oModel = this.getView().getModel("HealthInsuranceClaimAssets");
			bizNetAccessHealthInsuranceClaimAssets.removeHealthInsuranceClaimAsset(this.getOwnerComponent(), oModel, oModel.getProperty("/selectedHealthInsuranceClaimAsset/ID"));
		},

		_clearNewAsset : function(){

			this.getView().getModel("HealthInsuranceClaimAssets").setProperty("/newHealthInsuranceClaimAsset",{});
		}
	});

});
