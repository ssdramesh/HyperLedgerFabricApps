sap.ui.define([
	"claimNetSetup/controller/BaseController",
	"claimNetSetup/util/messageProvider",

        "claimNetSetup/util/bizNetAccessHealthInsuranceClaimAssets",
        "claimNetSetup/model/modelsHealthInsuranceClaimAssets",

	"claimNetSetup/model/modelsBase"
], function(
		BaseController,
		messageProvider,

        bizNetAccessHealthInsuranceClaimAssets,
        modelsHealthInsuranceClaimAssets,

		modelsBase
	) {
	"use strict";

	return BaseController.extend("claimNetSetup.controller.Selection", {

		onInit: function(){
			if ( typeof this.getOwnerComponent().getModel("Messages") === "undefined" ){
				this.getOwnerComponent().setModel(modelsBase.createMessagesModel(), "Messages");
			}
			messageProvider.init(this.getOwnerComponent().getModel("Messages"));
		},

		onSelect: function(oEvent) {
			switch (oEvent.getSource().getText()) {

        case "HealthInsuranceClaimAssets":
          this.getView().getModel("Selection").setProperty("/entityName", "healthInsuranceClaimAsset");
          this.getOwnerComponent().setModel(modelsHealthInsuranceClaimAssets.createHealthInsuranceClaimAssetsModel(), "HealthInsuranceClaimAssets");
          this.loadMetaData("healthInsuranceClaimAsset", this.getModel("HealthInsuranceClaimAssets"));
          bizNetAccessHealthInsuranceClaimAssets.loadAllHealthInsuranceClaimAssets(this.getOwnerComponent(), this.getView().getModel("HealthInsuranceClaimAssets"));
          this.getOwnerComponent().getRouter().navTo("healthInsuranceClaimAssets", {});
          break;

			}
		},

		removeAllEntities: function() {

			var msgStripID = this.getView().byId("__stripMessage");
			var location = this.getRunMode().location;


      if (typeof this.getOwnerComponent().getModel("HealthInsuranceClaimAssets") === "undefined") {
        this.getOwnerComponent().setModel(modelsHealthInsuranceClaimAssets.createHealthInsuranceClaimAssetsModel(), "HealthInsuranceClaimAssets");
      }
      bizNetAccessHealthInsuranceClaimAssets.removeAllHealthInsuranceClaimAssets(this.getOwnerComponent(), this.getView().getModel("HealthInsuranceClaimAssets"));
      messageProvider.addMessage("Success", "All HealthInsuranceClaimAssets deleted from sample-network", "No Description", location, 1, "", "http://www.sap.com");


			this.showMessageStrip(msgStripID,"All Existing claimNet data deleted from SAP BaaS", "S");
		},

		onToggleBaaS: function() {

			this.onToggleRunMode(this.getView().byId("__stripMessage"));
		},

		onMessagePress: function() {

			this.onShowMessageDialog("claimNet Maintenance Log");
		}
	});
});
