sap.ui.define([
	"claimNetSetup/controller/BaseController",
	"claimNetSetup/model/modelsBase",
	"claimNetSetup/util/messageProvider",
	"claimNetSetup/util/localStoreHealthInsuranceClaimAssets",
	"claimNetSetup/util/bizNetAccessHealthInsuranceClaimAssets",
	"sap/ui/core/routing/History",
	"sap/ui/model/Filter",
	"sap/ui/model/FilterOperator",
	"sap/ui/Device"
], function(
		BaseController,
		modelsBase,
		messageProvider,
		localStoreHealthInsuranceClaimAssets,
		bizNetAccessHealthInsuranceClaimAssets,
		History,
		Filter,
		FilterOperator,
		Device
	) {
	"use strict";

	return BaseController.extend("claimNetSetup.controller.HealthInsuranceClaimAssetsOverview", {

		onInit : function(){

			var oList = this.byId("healthInsuranceClaimAssetsList");
			this._oList = oList;
			this._oListFilterState = {
				aFilter : [],
				aSearch : []
			};
			localStoreHealthInsuranceClaimAssets.init();
			if ( typeof this.getOwnerComponent().getModel("Messages") === "undefined" ){
				this.getOwnerComponent().setModel(modelsBase.createMessagesModel(), "Messages");
			}
			messageProvider.init(this.getOwnerComponent().getModel("Messages"));
		},

		onSelectionChange : function (oEvent) {

			this._showDetail(oEvent.getParameter("listItem") || oEvent.getSource());
		},

		onAdd : function() {

			this.getRouter().navTo("healthInsuranceClaimAsset", {healthInsuranceClaimAssetId : "___new"});
		},

		onRemoveAll : function(){

			bizNetAccessHealthInsuranceClaimAssets.removeAllHealthInsuranceClaimAssets(this.getOwnerComponent(), this.getOwnerComponent().getModel("HealthInsuranceClaimAssets"));
		},

		onSearch : function(oEvent) {

			var sQuery = oEvent.getParameter("query");
			if (sQuery) {
				this._oListFilterState.aSearch = [new Filter("ID", FilterOperator.Contains, sQuery)];
			} else {
				this._oListFilterState.aSearch = [];
			}
			this._applyFilterSearch();
			var bReplace = !Device.system.phone;
			var oModel = this.getModel("HealthInsuranceClaimAssets");
			oModel.setProperty(
				"/selectedHealthInsuranceClaimAsset",
				_.findWhere(oModel.getProperty("/healthInsuranceClaimAssetCollection/items"),
					{
						ID: oModel.getProperty("/searchHealthInsuranceClaimAssetID")
					},
				this));
			this.getRouter().navTo("healthInsuranceClaimAsset", {
				healthInsuranceClaimAssetId : oModel.getProperty("/selectedHealthInsuranceClaimAsset").ID
			}, bReplace);
		},

		_onToggleBaaS : function(){

			this.getOwnerComponent().getModel("TestSwitch").setProperty("/testMode",!(this.getView().byId("__buttonToggleHealthInsuranceClaimAssets").getPressed()));
			var location = this.getRunMode().location;
			messageProvider.addMessage("Success", "Switching Network for Blockchain Data to: ", "No Description", location, 1, "", "http://www.sap.com");
			if ( this.getRunMode().testMode && localStoreHealthInsuranceClaimAssets.getHealthInsuranceClaimAssetData() === null ){
				localStoreHealthInsuranceClaimAssets.init();
			}
			bizNetAccessHealthInsuranceClaimAssets.loadAllHealthInsuranceClaimAssets(this.getOwnerComponent(), this.getModel("HealthInsuranceClaimAssets"));
		},

		_showDetail : function(oItem) {

			var bReplace = !Device.system.phone;
			var oModel = this.getModel("HealthInsuranceClaimAssets");
			this.getRouter().navTo("healthInsuranceClaimAsset", {
				healthInsuranceClaimAssetId : oModel.getObject(oItem.getBindingContextPath()).ID
			}, bReplace);
		},

		_applyFilterSearch : function () {
			var aFilters = this._oListFilterState.aSearch.concat(this._oListFilterState.aFilter),
				oViewModel = this.getModel("HealthInsuranceClaimAssets");
			this._oList.getBinding("items").filter(aFilters, "Application");
			if (aFilters.length !== 0) {
				oViewModel.setProperty("/noDataText", this.getResourceBundle().getText("_labelNoHealthInsuranceClaimAssetsText"));
			}
		},

		onMessagePress: function() {

			this.onShowMessageDialog("claimNetSetup Log");
		},

		onNavBack : function() {
			this.getRouter().navTo("home");
		}
	});
});
