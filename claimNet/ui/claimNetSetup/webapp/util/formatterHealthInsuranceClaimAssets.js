sap.ui.define(function() {
	"use strict";

	return {

		mapHealthInsuranceClaimAssetToModel:function(responseData){
			return {
				ID:responseData.ID,
				ObjectType:responseData.docType,
				
                Status:responseData.status,
                CreationDate:responseData.creationDate
,
                InsuranceType:responseData.insuranceType,
                OtherInsuredPolicyID:responseData.otherInsuredPolicyID,
                InsuredID:responseData.insuredID,
                PatientID:responseData.patientID,
                PatientRelationship:responseData.patientRelationship,
                PatientMaritalStatus:responseData.patientMaritalStatus,
                InsuranceProgram:responseData.insuranceProgram,
                ClaimCode:responseData.claimCode,
                ResubmissionCode:responseData.resubmissionCode,
                IllnessDate:responseData.illnessDate,
                HospitalizationStartDate:responseData.hospitalizationStartDate,
                HospitalizationEndDate:responseData.hospitalizationEndDate,
                TotalClaimAmount:responseData.totalClaimAmount,
                TotalPaidAmount:responseData.totalPaidAmount,
                FederalTaxID:responseData.federalTaxID
			};
		},

		mapHealthInsuranceClaimAssetsToModel:function(responseData){

			var items = [];
			if( responseData ){
				for ( var i = 0; i < responseData.length; i++ ){
					items.push(this.mapHealthInsuranceClaimAssetToModel(responseData[i]));
				}
			}
			return items;
		},

		mapHealthInsuranceClaimAssetToChaincode:function(oModel, newHealthInsuranceClaimAsset){

			if ( newHealthInsuranceClaimAsset === true ) {
				return {
						"ID":this.getNewHealthInsuranceClaimAssetID(oModel),
						"docType":"Asset.HealthInsuranceClaimAsset",
						
                        "status":oModel.getProperty("/newHealthInsuranceClaimAsset/Status"),
                        "creationDate":oModel.getProperty("/newHealthInsuranceClaimAsset/CreationDate")
,
                  "insuranceType":oModel.getProperty("/newHealthInsuranceClaimAsset/InsuranceType"),
                  "otherInsuredPolicyID":oModel.getProperty("/newHealthInsuranceClaimAsset/OtherInsuredPolicyID"),
                  "insuredID":oModel.getProperty("/newHealthInsuranceClaimAsset/InsuredID"),
                  "patientID":oModel.getProperty("/newHealthInsuranceClaimAsset/PatientID"),
                  "patientRelationship":oModel.getProperty("/newHealthInsuranceClaimAsset/PatientRelationship"),
                  "patientMaritalStatus":oModel.getProperty("/newHealthInsuranceClaimAsset/PatientMaritalStatus"),
                  "insuranceProgram":oModel.getProperty("/newHealthInsuranceClaimAsset/InsuranceProgram"),
                  "claimCode":oModel.getProperty("/newHealthInsuranceClaimAsset/ClaimCode"),
                  "resubmissionCode":oModel.getProperty("/newHealthInsuranceClaimAsset/ResubmissionCode"),
                  "illnessDate":oModel.getProperty("/newHealthInsuranceClaimAsset/IllnessDate"),
                  "hospitalizationStartDate":oModel.getProperty("/newHealthInsuranceClaimAsset/HospitalizationStartDate"),
                  "hospitalizationEndDate":oModel.getProperty("/newHealthInsuranceClaimAsset/HospitalizationEndDate"),
                  "totalClaimAmount":oModel.getProperty("/newHealthInsuranceClaimAsset/TotalClaimAmount"),
                  "totalPaidAmount":oModel.getProperty("/newHealthInsuranceClaimAsset/TotalPaidAmount"),
                  "federalTaxID":oModel.getProperty("/newHealthInsuranceClaimAsset/FederalTaxID")
				};
			} else {
				return {
						"ID":oModel.getProperty("/selectedHealthInsuranceClaimAsset/ID"),
						"docType":"Asset.HealthInsuranceClaimAsset",
						
                        "status":oModel.getProperty("/selectedHealthInsuranceClaimAsset/Status"),
                        "creationDate":oModel.getProperty("/selectedHealthInsuranceClaimAsset/CreationDate")
,
                  "insuranceType":oModel.getProperty("/selectedHealthInsuranceClaimAsset/InsuranceType"),
                  "otherInsuredPolicyID":oModel.getProperty("/selectedHealthInsuranceClaimAsset/OtherInsuredPolicyID"),
                  "insuredID":oModel.getProperty("/selectedHealthInsuranceClaimAsset/InsuredID"),
                  "patientID":oModel.getProperty("/selectedHealthInsuranceClaimAsset/PatientID"),
                  "patientRelationship":oModel.getProperty("/selectedHealthInsuranceClaimAsset/PatientRelationship"),
                  "patientMaritalStatus":oModel.getProperty("/selectedHealthInsuranceClaimAsset/PatientMaritalStatus"),
                  "insuranceProgram":oModel.getProperty("/selectedHealthInsuranceClaimAsset/InsuranceProgram"),
                  "claimCode":oModel.getProperty("/selectedHealthInsuranceClaimAsset/ClaimCode"),
                  "resubmissionCode":oModel.getProperty("/selectedHealthInsuranceClaimAsset/ResubmissionCode"),
                  "illnessDate":oModel.getProperty("/selectedHealthInsuranceClaimAsset/IllnessDate"),
                  "hospitalizationStartDate":oModel.getProperty("/selectedHealthInsuranceClaimAsset/HospitalizationStartDate"),
                  "hospitalizationEndDate":oModel.getProperty("/selectedHealthInsuranceClaimAsset/HospitalizationEndDate"),
                  "totalClaimAmount":oModel.getProperty("/selectedHealthInsuranceClaimAsset/TotalClaimAmount"),
                  "totalPaidAmount":oModel.getProperty("/selectedHealthInsuranceClaimAsset/TotalPaidAmount"),
                  "federalTaxID":oModel.getProperty("/selectedHealthInsuranceClaimAsset/FederalTaxID")
				};
			}
		},

		mapHealthInsuranceClaimAssetToLocalStorage : function(oModel, newHealthInsuranceClaimAsset){

			return this.mapHealthInsuranceClaimAssetToChaincode(oModel, newHealthInsuranceClaimAsset);
		},

		getNewHealthInsuranceClaimAssetID:function(oModel){

		    if ( typeof oModel.getProperty("/newHealthInsuranceClaimAsset/ID") === "undefined" ||
		    		oModel.getProperty("/newHealthInsuranceClaimAsset/ID") === ""
		    	){
			    var iD = "HealthInsuranceClaimAsset";
			    var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
			    for( var i = 0; i < 8; i++ ){
			        iD += possible.charAt(Math.floor(Math.random() * possible.length));
			    }
		    } else {
				iD = oModel.getProperty("/newHealthInsuranceClaimAsset/ID");
			}
			oModel.setProperty("/newHealthInsuranceClaimAsset/ID",iD);
		    return iD;
		}
	};
});
