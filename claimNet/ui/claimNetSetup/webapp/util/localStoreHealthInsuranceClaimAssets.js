sap.ui.define(function() {
	"use strict";

	jQuery.sap.require("jquery.sap.storage");
	var oStorage = jQuery.sap.storage(jQuery.sap.storage.Type.local);
	var healthInsuranceClaimAssetsDataID = "healthInsuranceClaimAssets";

	return {

		init: function(){

			oStorage.put(healthInsuranceClaimAssetsDataID,[]);
			oStorage.put(
				healthInsuranceClaimAssetsDataID,
[
	{
		"ID":"HealthInsuranceClaimAsset1001",
		"docType":"Asset.HealthInsuranceClaimAsset",
		"status":"Created",
		"creationDate":"12/28/2107",
		"insuranceType":"insuranceType001",
		"otherInsuredPolicyID":"otherInsuredPolicyID001",
		"insuredID":"insuredID001",
		"patientID":"patientID001",
		"patientRelationship":"patientRelationship001",
		"patientMaritalStatus":"patientMaritalStatus001",
		"insuranceProgram":"insuranceProgram001",
		"claimCode":"claimCode001",
		"resubmissionCode":"resubmissionCode001",
		"illnessDate":"illnessDate001",
		"hospitalizationStartDate":"hospitalizationStartDate001",
		"hospitalizationEndDate":"hospitalizationEndDate001",
		"totalClaimAmount":"totalClaimAmount001",
		"totalPaidAmount":"totalPaidAmount001",
		"federalTaxID":"federalTaxID001"
	}
]				
			);
		},

		getHealthInsuranceClaimAssetDataID : function(){

			return healthInsuranceClaimAssetsDataID;
		},

		getHealthInsuranceClaimAssetData  : function(){

			return oStorage.get("healthInsuranceClaimAssets");
		},

		put: function(newHealthInsuranceClaimAsset){

			var healthInsuranceClaimAssetData = this.getHealthInsuranceClaimAssetData();
			healthInsuranceClaimAssetData.push(newHealthInsuranceClaimAsset);
			oStorage.put(healthInsuranceClaimAssetsDataID, healthInsuranceClaimAssetData);
		},

		remove : function (id){

			var healthInsuranceClaimAssetData = this.getHealthInsuranceClaimAssetData();
			healthInsuranceClaimAssetData = _.without(healthInsuranceClaimAssetData,_.findWhere(healthInsuranceClaimAssetData,{ID:id}));
			oStorage.put(healthInsuranceClaimAssetsDataID, healthInsuranceClaimAssetData);
		},

		removeAll : function(){

			oStorage.put(healthInsuranceClaimAssetsDataID,[]);
		},

		clearHealthInsuranceClaimAssetData: function(){

			oStorage.put(healthInsuranceClaimAssetsDataID,[]);
		}
	};
});
