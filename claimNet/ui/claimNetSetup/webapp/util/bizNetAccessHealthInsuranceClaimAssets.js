sap.ui.define([
	"claimNetSetup/util/restBuilder",
	"claimNetSetup/util/formatterHealthInsuranceClaimAssets",
	"claimNetSetup/util/localStoreHealthInsuranceClaimAssets"
], function(
		restBuilder,
		formatterHealthInsuranceClaimAssets,
		localStoreHealthInsuranceClaimAssets
	) {
	"use strict";

	return {

		loadAllHealthInsuranceClaimAssets:function(oComponent, oModel) {

			if ( !( oComponent.getModel("TestSwitch").getProperty("/testMode") ) ){
				restBuilder.execute(
					oModel.getProperty("/chaincode"),
					"readAll",
					[],
					function(responseData){
						oModel.setProperty(
							"/healthInsuranceClaimAssetCollection/items",
							formatterHealthInsuranceClaimAssets.mapHealthInsuranceClaimAssetsToModel(responseData)
						);
					});
			} else {
				oModel.setProperty(
					"/healthInsuranceClaimAssetCollection/items",
					formatterHealthInsuranceClaimAssets.mapHealthInsuranceClaimAssetsToModel(localStoreHealthInsuranceClaimAssets.getHealthInsuranceClaimAssetData())
				);
			}
		},

		loadHealthInsuranceClaimAsset:function(oModel, selectedHealthInsuranceClaimAssetID){

			oModel.setProperty(
				"/selectedHealthInsuranceClaimAsset",
				_.findWhere(oModel.getProperty("/healthInsuranceClaimAssetCollection/items"),
					{
						ID: selectedHealthInsuranceClaimAssetID
					},
				this));
		},

		addNewHealthInsuranceClaimAsset:function(oComponent, oModel){

			if ( !( oComponent.getModel("TestSwitch").getProperty("/testMode") ) ){
				restBuilder.execute(
					oModel.getProperty("/chaincode"),
					"addNew",
					formatterHealthInsuranceClaimAssets.mapHealthInsuranceClaimAssetToChaincode(oModel, true)
				);
			}  else {
				localStoreHealthInsuranceClaimAssets.put(formatterHealthInsuranceClaimAssets.mapHealthInsuranceClaimAssetToLocalStorage(oModel, true));
			}
			this.loadAllHealthInsuranceClaimAssets(oComponent, oModel);
			return oModel.getProperty("/newHealthInsuranceClaimAsset/ID");
		},

		removeHealthInsuranceClaimAsset : function(oComponent, oModel, healthInsuranceClaimAssetID){

			if ( !( oComponent.getModel("TestSwitch").getProperty("/testMode") ) ) {
				restBuilder.execute(
					oModel.getProperty("/chaincode"),
					"remove",
					{ID:healthInsuranceClaimAssetID}
				);
			} else {
				localStoreHealthInsuranceClaimAssets.remove(healthInsuranceClaimAssetID);
			}
			this.loadAllHealthInsuranceClaimAssets(oComponent, oModel);
			return true;
		},

		removeAllHealthInsuranceClaimAssets : function(oComponent, oModel){

			if (!(oComponent.getModel("TestSwitch").getProperty("/testMode"))) {
				restBuilder.execute(
					oModel.getProperty("/chaincode"),
					"removeAll",
					[]
				);
			} else {
				localStoreHealthInsuranceClaimAssets.removeAll();
			}
			this.loadAllHealthInsuranceClaimAssets(oComponent, oModel);
			oModel.setProperty("/selectedHealthInsuranceClaimAsset",{});
			return true;
		}
	};
});
