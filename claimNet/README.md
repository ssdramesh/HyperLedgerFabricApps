# What's in here?
This is a demo for illustrating the collection of health insurance claim submissions and uploading to a blockchain network for fraud detection (prevention of duplicate submissions of the same claim to multiple insurance companies).

Use-case Cluster: Blockchain for Insurance Fraud Detection

# Context
Health Insurance Claim sample form

# Business Network Specification
The persona for the demo is the claim processer at the health insurance company who is responsible to ensure that the claim submission is unique across all participating insurance companies.

# Model
![Health Insurance Claim Data Model](./Model/claimNetModel.png)

# Deployed Systems

[Hyperledger Node Dashboard](https://hyperledger-fabric-dashboard.cfapps.us10.hana.ondemand.com/81ba251c-30bd-40b0-bb12-1b78158439a8/10cb28fb-7ef1-43c5-9a0a-e77b5008af7a/b11946ed-5fa2-482d-9a1e-249816b5b754/node)
[Hyperledger Channel Dashboard](https://hyperledger-fabric-dashboard.cfapps.us10.hana.ondemand.com/81ba251c-30bd-40b0-bb12-1b78158439a8/10cb28fb-7ef1-43c5-9a0a-e77b5008af7a/297b4ab9-587c-408a-943c-2f91023f6b7d/channel)

# Demo URLs
[Demo Application](https://claimnetsetup.cfapps.us10.hana.ondemand.com)

# Demo Script(s)

A short script for demo.  The demo URL is [here](https://claimnetsetup.cfapps.us10.hana.ondemand.com).
1. Search for a Claim ID on the master list.
2. VErify if the Claim is already uploaded to the blockchain or not.

# Published Links
No links are published yet in Innobook.

# Contact(s):
Ramesh Suraparaju
