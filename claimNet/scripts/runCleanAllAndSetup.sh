#!/bin/bash

cd /Users/i047582/Documents/Workspaces/git/github.wdf.sap.corp/I047582/HyperLedgerFabricApps/claimNet/test/newman/CleanAllAndSetup


echo "Clean All HealthInsuranceClaimAssets before Setup..."
newman run cleanAllHealthInsuranceClaimAssets.postman_collection.json -e claimNet.postman_environment.json --bail newman
echo "Setting up HealthInsuranceClaimAssets..."
newman run createAllHealthInsuranceClaimAssets.postman_collection.json -e claimNet.postman_environment.json -d ./data/HealthInsuranceClaimAssets.json --bail newman

echo "Done. Ready to run!"
