package main

//Template Version: 1.1-20180704
import (
	"bytes"
	"encoding/json"
	"fmt"
	"strings"
	"testing"

	"github.com/hyperledger/fabric/core/chaincode/shim"
)

//TestHealthInsuranceClaimAsset_Init
func TestHealthInsuranceClaimAsset_Init(t *testing.T) {
	healthInsuranceClaim := new(HealthInsuranceClaimAsset)
	stub := shim.NewMockStub("healthInsuranceClaim", healthInsuranceClaim)
	checkInit(t, stub, [][]byte{[]byte("init")})
	checkState(t, stub, "healthInsuranceClaimIDIndex", getExpectedHealthInsuranceClaimIDIndex("init"))
}

//TestHealthInsuranceClaimAsset_InvokeUnknownFunction
func TestHealthInsuranceClaimAsset_InvokeUnknownFunction(t *testing.T) {
	healthInsuranceClaim := new(HealthInsuranceClaimAsset)
	stub := shim.NewMockStub("healthInsuranceClaim", healthInsuranceClaim)
	checkInit(t, stub, [][]byte{[]byte("init")})
	checkInvokeUnknownFunction(t, stub, [][]byte{[]byte("myFunction"), []byte("docType:Asset")})
}

//TestHealthInsuranceClaimAsset_Invoke_addNewHealthInsuranceClaim
func TestHealthInsuranceClaimAsset_Invoke_addNewHealthInsuranceClaimOK(t *testing.T) {
	healthInsuranceClaim := new(HealthInsuranceClaimAsset)
	stub := shim.NewMockStub("healthInsuranceClaim", healthInsuranceClaim)
	checkInit(t, stub, [][]byte{[]byte("init")})
	checkInvoke(t, stub, getFirstHealthInsuranceClaimAssetForTesting())
	newHealthInsuranceClaimID := "100001"
	checkState(t, stub, newHealthInsuranceClaimID, getNewHealthInsuranceClaimExpected())
	checkState(t, stub, "healthInsuranceClaimIDIndex", getExpectedHealthInsuranceClaimIDIndex("addNewHealthInsuranceClaim"))
}

//TestHealthInsuranceClaimAsset_Invoke_addNewHealthInsuranceClaim
func TestHealthInsuranceClaimAsset_Invoke_addNewHealthInsuranceClaimDuplicate(t *testing.T) {
	healthInsuranceClaim := new(HealthInsuranceClaimAsset)
	stub := shim.NewMockStub("healthInsuranceClaim", healthInsuranceClaim)
	checkInit(t, stub, [][]byte{[]byte("init")})
	checkInvoke(t, stub, getFirstHealthInsuranceClaimAssetForTesting())
	newHealthInsuranceClaimID := "100001"
	checkState(t, stub, newHealthInsuranceClaimID, getNewHealthInsuranceClaimExpected())
	checkState(t, stub, "healthInsuranceClaimIDIndex", getExpectedHealthInsuranceClaimIDIndex("addNewHealthInsuranceClaim"))
	res := stub.MockInvoke("1", getFirstHealthInsuranceClaimAssetForTesting())
	if res.Status != shim.OK {
		checkError(t, "This HealthInsuranceClaim already exists: 100001", res.Message)
	} else {
		fmt.Println("Error was expected, but not raised")
		t.FailNow()
	}
}

//TestHealthInsuranceClaimAsset_Invoke_removeHealthInsuranceClaimOK  //change template
func TestHealthInsuranceClaimAsset_Invoke_removeHealthInsuranceClaimOK(t *testing.T) {
	healthInsuranceClaim := new(HealthInsuranceClaimAsset)
	stub := shim.NewMockStub("healthInsuranceClaim", healthInsuranceClaim)
	checkInit(t, stub, [][]byte{[]byte("init")})
	checkInvoke(t, stub, getFirstHealthInsuranceClaimAssetForTesting())
	checkInvoke(t, stub, getSecondHealthInsuranceClaimAssetForTesting())
	checkReadAllHealthInsuranceClaimsOK(t, stub)
	checkState(t, stub, "healthInsuranceClaimIDIndex", getExpectedHealthInsuranceClaimIDIndex("beforeRemoveHealthInsuranceClaim"))
	checkInvoke(t, stub, getRemoveSecondHealthInsuranceClaimAssetForTesting())
	remainingHealthInsuranceClaimID := "100001"
	checkReadHealthInsuranceClaimOK(t, stub, remainingHealthInsuranceClaimID)
	checkState(t, stub, "healthInsuranceClaimIDIndex", getExpectedHealthInsuranceClaimIDIndex("afterRemoveHealthInsuranceClaim"))
}

//TestHealthInsuranceClaimAsset_Invoke_removeHealthInsuranceClaimNOK  //change template
func TestHealthInsuranceClaimAsset_Invoke_removeHealthInsuranceClaimNOK(t *testing.T) {
	healthInsuranceClaim := new(HealthInsuranceClaimAsset)
	stub := shim.NewMockStub("healthInsuranceClaim", healthInsuranceClaim)
	checkInit(t, stub, [][]byte{[]byte("init")})
	checkInvoke(t, stub, getFirstHealthInsuranceClaimAssetForTesting())
	firstHealthInsuranceClaimID := "100001"
	checkReadHealthInsuranceClaimOK(t, stub, firstHealthInsuranceClaimID)
	checkState(t, stub, "healthInsuranceClaimIDIndex", getExpectedHealthInsuranceClaimIDIndex("addNewHealthInsuranceClaim"))
	res := stub.MockInvoke("1", getRemoveSecondHealthInsuranceClaimAssetForTesting())
	if res.Status != shim.OK {
		checkError(t, "HealthInsuranceClaim with ID: "+"100002"+" not found", res.Message)
	} else {
		fmt.Println("Error was expected, but not raised")
		t.FailNow()
	}
	checkState(t, stub, "healthInsuranceClaimIDIndex", getExpectedHealthInsuranceClaimIDIndex("addNewHealthInsuranceClaim"))
}

//TestHealthInsuranceClaimAsset_Invoke_removeAllHealthInsuranceClaimsOK  //change template
func TestHealthInsuranceClaimAsset_Invoke_removeAllHealthInsuranceClaimsOK(t *testing.T) {
	healthInsuranceClaim := new(HealthInsuranceClaimAsset)
	stub := shim.NewMockStub("healthInsuranceClaim", healthInsuranceClaim)
	checkInit(t, stub, [][]byte{[]byte("init")})
	checkInvoke(t, stub, getFirstHealthInsuranceClaimAssetForTesting())
	checkInvoke(t, stub, getSecondHealthInsuranceClaimAssetForTesting())
	checkReadAllHealthInsuranceClaimsOK(t, stub)
	checkState(t, stub, "healthInsuranceClaimIDIndex", getExpectedHealthInsuranceClaimIDIndex("beforeRemoveHealthInsuranceClaim"))
	checkInvoke(t, stub, getRemoveAllHealthInsuranceClaimAssetsForTesting())
	checkState(t, stub, "healthInsuranceClaimIDIndex", getExpectedHealthInsuranceClaimIDIndex(""))
}

//TestHealthInsuranceClaimAsset_Invoke_removeHealthInsuranceClaimNOK  //change template
func TestHealthInsuranceClaimAsset_Invoke_removeAllHealthInsuranceClaimsNOK(t *testing.T) {
	healthInsuranceClaim := new(HealthInsuranceClaimAsset)
	stub := shim.NewMockStub("healthInsuranceClaim", healthInsuranceClaim)
	checkInit(t, stub, [][]byte{[]byte("init")})
	res := stub.MockInvoke("1", getRemoveAllHealthInsuranceClaimAssetsForTesting())
	if res.Status != shim.OK {
		checkError(t, "removeAllHealthInsuranceClaims: No healthInsuranceClaims to remove", res.Message)
	} else {
		fmt.Println("Error was expected, but not raised")
		t.FailNow()
	}
	checkState(t, stub, "healthInsuranceClaimIDIndex", getExpectedHealthInsuranceClaimIDIndex(""))
}

//TestHealthInsuranceClaimAsset_Query_readHealthInsuranceClaim
func TestHealthInsuranceClaimAsset_Query_readHealthInsuranceClaim(t *testing.T) {
	healthInsuranceClaim := new(HealthInsuranceClaimAsset)
	stub := shim.NewMockStub("healthInsuranceClaim", healthInsuranceClaim)
	checkInit(t, stub, [][]byte{[]byte("init")})
	healthInsuranceClaimID := "100001"
	checkInvoke(t, stub, getFirstHealthInsuranceClaimAssetForTesting())
	checkReadHealthInsuranceClaimOK(t, stub, healthInsuranceClaimID)
	checkReadHealthInsuranceClaimNOK(t, stub, "")
}

//TestHealthInsuranceClaimAsset_Query_readAllHealthInsuranceClaims
func TestHealthInsuranceClaimAsset_Query_readAllHealthInsuranceClaims(t *testing.T) {
	healthInsuranceClaim := new(HealthInsuranceClaimAsset)
	stub := shim.NewMockStub("healthInsuranceClaim", healthInsuranceClaim)
	checkInit(t, stub, [][]byte{[]byte("init")})
	checkInvoke(t, stub, getFirstHealthInsuranceClaimAssetForTesting())
	checkInvoke(t, stub, getSecondHealthInsuranceClaimAssetForTesting())
	checkReadAllHealthInsuranceClaimsOK(t, stub)
}

/*
*
*	Helper Functions
*
 */
//Get first HealthInsuranceClaimAsset for testing
func getFirstHealthInsuranceClaimAssetForTesting() [][]byte { 
 return [][]byte{[]byte("addNewHealthInsuranceClaim"), 
 []byte("{\"ID\":\"100001\",\"docType\":\"Asset.HealthInsuranceClaimAsset\",\"status\":\"0\",\"creationDate\":\"12/01/2018\", \"insuranceType\":\"insuranceType001\", \"otherInsuredPolicyID\":\"otherInsuredPolicyID001\", \"insuredID\":\"insuredID001\", \"patientID\":\"patientID001\", \"patientRelationship\":\"patientRelationship001\", \"patientMaritalStatus\":\"patientMaritalStatus001\", \"insuranceProgram\":\"insuranceProgram001\", \"claimCode\":\"claimCode001\", \"resubmissionCode\":\"resubmissionCode001\", \"illnessDate\":\"illnessDate001\", \"hospitalizationStartDate\":\"hospitalizationStartDate001\", \"hospitalizationEndDate\":\"hospitalizationEndDate001\", \"totalClaimAmount\":\"totalClaimAmount001\", \"totalPaidAmount\":\"totalPaidAmount001\", \"federalTaxID\":\"federalTaxID001\"}")}
}

//Get second HealthInsuranceClaimAsset for testing
func getSecondHealthInsuranceClaimAssetForTesting() [][]byte { 
 return [][]byte{[]byte("addNewHealthInsuranceClaim"), 
 []byte("{\"ID\":\"100002\",\"docType\":\"Asset.HealthInsuranceClaimAsset\",\"status\":\"0\",\"creationDate\":\"12/01/2018\", \"insuranceType\":\"insuranceType002\", \"otherInsuredPolicyID\":\"otherInsuredPolicyID002\", \"insuredID\":\"insuredID002\", \"patientID\":\"patientID002\", \"patientRelationship\":\"patientRelationship002\", \"patientMaritalStatus\":\"patientMaritalStatus002\", \"insuranceProgram\":\"insuranceProgram002\", \"claimCode\":\"claimCode002\", \"resubmissionCode\":\"resubmissionCode002\", \"illnessDate\":\"illnessDate002\", \"hospitalizationStartDate\":\"hospitalizationStartDate002\", \"hospitalizationEndDate\":\"hospitalizationEndDate002\", \"totalClaimAmount\":\"totalClaimAmount002\", \"totalPaidAmount\":\"totalPaidAmount002\", \"federalTaxID\":\"federalTaxID002\"}")}
}

//Get remove second HealthInsuranceClaimAsset for testing //change template
func getRemoveSecondHealthInsuranceClaimAssetForTesting() [][]byte {
	return [][]byte{[]byte("removeHealthInsuranceClaim"),
		[]byte("100002")}
}

//Get remove all HealthInsuranceClaimAssets for testing //change template
func getRemoveAllHealthInsuranceClaimAssetsForTesting() [][]byte {
	return [][]byte{[]byte("removeAllHealthInsuranceClaims")}
}

//Get an expected value for testing
func getNewHealthInsuranceClaimExpected() []byte {
	var healthInsuranceClaim HealthInsuranceClaim
		healthInsuranceClaim.ID = "100001"
	healthInsuranceClaim.ObjectType = "Asset.HealthInsuranceClaimAsset"
  healthInsuranceClaim.Status = "0"
	healthInsuranceClaim.CreationDate = "12/01/2018"
healthInsuranceClaim.InsuranceType="insuranceType001"
healthInsuranceClaim.OtherInsuredPolicyID="otherInsuredPolicyID001"
healthInsuranceClaim.InsuredID="insuredID001"
healthInsuranceClaim.PatientID="patientID001"
healthInsuranceClaim.PatientRelationship="patientRelationship001"
healthInsuranceClaim.PatientMaritalStatus="patientMaritalStatus001"
healthInsuranceClaim.InsuranceProgram="insuranceProgram001"
healthInsuranceClaim.ClaimCode="claimCode001"
healthInsuranceClaim.ResubmissionCode="resubmissionCode001"
healthInsuranceClaim.IllnessDate="illnessDate001"
healthInsuranceClaim.HospitalizationStartDate="hospitalizationStartDate001"
healthInsuranceClaim.HospitalizationEndDate="hospitalizationEndDate001"
healthInsuranceClaim.TotalClaimAmount="totalClaimAmount001"
healthInsuranceClaim.TotalPaidAmount="totalPaidAmount001"
healthInsuranceClaim.FederalTaxID="federalTaxID001"
	healthInsuranceClaimJSON, err := json.Marshal(healthInsuranceClaim)
	if err != nil {
		fmt.Println("Error converting a HealthInsuranceClaim record to JSON")
		return nil
	}
	return []byte(healthInsuranceClaimJSON)
}

//Get expected values of HealthInsuranceClaims for testing
func getExpectedHealthInsuranceClaims() []byte {
	var healthInsuranceClaims []HealthInsuranceClaim
	var healthInsuranceClaim HealthInsuranceClaim
		healthInsuranceClaim.ID = "100001"
	healthInsuranceClaim.ObjectType = "Asset.HealthInsuranceClaimAsset"
  healthInsuranceClaim.Status = "0"
	healthInsuranceClaim.CreationDate = "12/01/2018"
healthInsuranceClaim.InsuranceType="insuranceType001"
healthInsuranceClaim.OtherInsuredPolicyID="otherInsuredPolicyID001"
healthInsuranceClaim.InsuredID="insuredID001"
healthInsuranceClaim.PatientID="patientID001"
healthInsuranceClaim.PatientRelationship="patientRelationship001"
healthInsuranceClaim.PatientMaritalStatus="patientMaritalStatus001"
healthInsuranceClaim.InsuranceProgram="insuranceProgram001"
healthInsuranceClaim.ClaimCode="claimCode001"
healthInsuranceClaim.ResubmissionCode="resubmissionCode001"
healthInsuranceClaim.IllnessDate="illnessDate001"
healthInsuranceClaim.HospitalizationStartDate="hospitalizationStartDate001"
healthInsuranceClaim.HospitalizationEndDate="hospitalizationEndDate001"
healthInsuranceClaim.TotalClaimAmount="totalClaimAmount001"
healthInsuranceClaim.TotalPaidAmount="totalPaidAmount001"
healthInsuranceClaim.FederalTaxID="federalTaxID001"
	healthInsuranceClaims = append(healthInsuranceClaims, healthInsuranceClaim)
		healthInsuranceClaim.ID = "100002"
	healthInsuranceClaim.ObjectType = "Asset.HealthInsuranceClaimAsset"
  healthInsuranceClaim.Status = "0"
	healthInsuranceClaim.CreationDate = "12/01/2018"
healthInsuranceClaim.InsuranceType="insuranceType002"
healthInsuranceClaim.OtherInsuredPolicyID="otherInsuredPolicyID002"
healthInsuranceClaim.InsuredID="insuredID002"
healthInsuranceClaim.PatientID="patientID002"
healthInsuranceClaim.PatientRelationship="patientRelationship002"
healthInsuranceClaim.PatientMaritalStatus="patientMaritalStatus002"
healthInsuranceClaim.InsuranceProgram="insuranceProgram002"
healthInsuranceClaim.ClaimCode="claimCode002"
healthInsuranceClaim.ResubmissionCode="resubmissionCode002"
healthInsuranceClaim.IllnessDate="illnessDate002"
healthInsuranceClaim.HospitalizationStartDate="hospitalizationStartDate002"
healthInsuranceClaim.HospitalizationEndDate="hospitalizationEndDate002"
healthInsuranceClaim.TotalClaimAmount="totalClaimAmount002"
healthInsuranceClaim.TotalPaidAmount="totalPaidAmount002"
healthInsuranceClaim.FederalTaxID="federalTaxID002"
	healthInsuranceClaims = append(healthInsuranceClaims, healthInsuranceClaim)
	healthInsuranceClaimJSON, err := json.Marshal(healthInsuranceClaims)
	if err != nil {
		fmt.Println("Error converting healthInsuranceClaim records to JSON")
		return nil
	}
	return []byte(healthInsuranceClaimJSON)
}

func getExpectedHealthInsuranceClaimIDIndex(funcName string) []byte {
	var healthInsuranceClaimIDIndex HealthInsuranceClaimIDIndex
	switch funcName {
	case "addNewHealthInsuranceClaim":
		healthInsuranceClaimIDIndex.IDs = append(healthInsuranceClaimIDIndex.IDs, "100001")
		healthInsuranceClaimIDIndexBytes, err := json.Marshal(healthInsuranceClaimIDIndex)
		if err != nil {
			fmt.Println("Error converting HealthInsuranceClaimIDIndex to JSON")
			return nil
		}
		return healthInsuranceClaimIDIndexBytes
	case "beforeRemoveHealthInsuranceClaim":
		healthInsuranceClaimIDIndex.IDs = append(healthInsuranceClaimIDIndex.IDs, "100001")
		healthInsuranceClaimIDIndex.IDs = append(healthInsuranceClaimIDIndex.IDs, "100002")
		healthInsuranceClaimIDIndexBytes, err := json.Marshal(healthInsuranceClaimIDIndex)
		if err != nil {
			fmt.Println("Error converting HealthInsuranceClaimIDIndex to JSON")
			return nil
		}
		return healthInsuranceClaimIDIndexBytes
	case "afterRemoveHealthInsuranceClaim":
		healthInsuranceClaimIDIndex.IDs = append(healthInsuranceClaimIDIndex.IDs, "100001")
		healthInsuranceClaimIDIndexBytes, err := json.Marshal(healthInsuranceClaimIDIndex)
		if err != nil {
			fmt.Println("Error converting HealthInsuranceClaimIDIndex to JSON")
			return nil
		}
		return healthInsuranceClaimIDIndexBytes
	default:
		healthInsuranceClaimIDIndexBytes, err := json.Marshal(healthInsuranceClaimIDIndex)
		if err != nil {
			fmt.Println("Error converting HealthInsuranceClaimIDIndex to JSON")
			return nil
		}
		return healthInsuranceClaimIDIndexBytes
	}
}

//checkInit - helper to check the Initialization of chaincode: HealthInsuranceClaimAsset
func checkInit(t *testing.T, stub *shim.MockStub, args [][]byte) {
	res := stub.MockInit("1", args)
	if res.Status != shim.OK {
		fmt.Println("Init failed", string(res.Message))
		t.FailNow()
	}
}

//checkState - helper for checking the chaincode state for a given stateKey afgainst an expected value
func checkState(t *testing.T, stub *shim.MockStub, stateKey string, expectedState []byte) {
	actualState := stub.State[stateKey]
	if actualState == nil {
		fmt.Println("State for ", stateKey, ": failed to get value")
		t.FailNow()
	}

	if bytes.Compare(actualState, expectedState) != 0 {
		fmt.Println("Incorrect State for " + stateKey + ": \nExpected: " + string(expectedState) + "\nActual  : " + string(actualState))
		t.FailNow()
	}
}

//checkInvoke - helper for checking Invoke of chaincode
func checkInvoke(t *testing.T, stub *shim.MockStub, args [][]byte) {
	res := stub.MockInvoke("1", args)
	if res.Status != shim.OK {
		fmt.Println("Invoke", args, "failed", string(res.Message))
		t.FailNow()
	}
}

func checkInvokeUnknownFunction(t *testing.T, stub *shim.MockStub, args [][]byte) {
	res := stub.MockInvoke("1", args)
	if res.Status != shim.OK {
		expectedErr := "Received unknown function invocation"
		actualErr := string(res.Message)
		if !(strings.Contains(actualErr, expectedErr)) {
			fmt.Println("Invoke with unknown function must throw error: ", expectedErr, "\n Actual Error :", actualErr)
			t.FailNow()
		}
	} else {
		t.FailNow()
	}
}

//checkReadHealthInsuranceClaimOK - helper for positive test readHealthInsuranceClaim
func checkReadHealthInsuranceClaimOK(t *testing.T, stub *shim.MockStub, healthInsuranceClaimID string) {
	res := stub.MockInvoke("1", [][]byte{[]byte("readHealthInsuranceClaim"), []byte(healthInsuranceClaimID)})
	if res.Status != shim.OK {
		fmt.Println("func readHealthInsuranceClaim with ID: ", healthInsuranceClaimID, " failed"+string(res.Message))
		t.FailNow()
	}
	if res.Payload == nil {
		fmt.Println("func readHealthInsuranceClaim with ID: ", healthInsuranceClaimID, "failed to get value")
		t.FailNow()
	}
	if bytes.Compare(getNewHealthInsuranceClaimExpected(), []byte(res.Payload)) != 0 {
		fmt.Println("func readHealthInsuranceClaim with ID: ", healthInsuranceClaimID, "Expected:", string(getNewHealthInsuranceClaimExpected()), "Actual:", string(res.Payload))
		t.FailNow()
	}
}

//checkReadHealthInsuranceClaimNOK - helper for negative testing of readHealthInsuranceClaim
func checkReadHealthInsuranceClaimNOK(t *testing.T, stub *shim.MockStub, healthInsuranceClaimID string) {
	//with no healthInsuranceClaimID
	res := stub.MockInvoke("1", [][]byte{[]byte("readHealthInsuranceClaim"), []byte("")})
	if res.Status != shim.OK {
		expectedErr := "retrieveHealthInsuranceClaim: Corrupt healthInsuranceClaim record "
		actualErr := string(res.Message)
		if !(strings.Contains(actualErr, expectedErr)) {
			fmt.Println("func readHealthInsuranceClaim negative test: ", "Expected Error:", expectedErr, "Actual Error", actualErr)
			t.FailNow()
		}
	} else {
		t.FailNow()
	}
}

func checkReadAllHealthInsuranceClaimsOK(t *testing.T, stub *shim.MockStub) {
	res := stub.MockInvoke("1", [][]byte{[]byte("readAllHealthInsuranceClaims")})
	if res.Status != shim.OK {
		fmt.Println("func readAllHealthInsuranceClaims failed", string(res.Message))
		t.FailNow()
	}
	if res.Payload == nil {
		fmt.Println("func readAllHealthInsuranceClaims failed to get value")
		t.FailNow()
	}
	if bytes.Compare(getExpectedHealthInsuranceClaims(), []byte(res.Payload)) != 0 {
		fmt.Println("func readAllHealthInsuranceClaims Expected:\n", string(getExpectedHealthInsuranceClaims()), "\nActual:\n", string(res.Payload))
		t.FailNow()
	}
}

func checkError(t *testing.T, exp string, act string) {
	if strings.Compare(exp, act) != 0 {
		fmt.Println("Unexpected Error! Expecting ", exp, "\n Actual :", act)
		t.FailNow()
	}
}
