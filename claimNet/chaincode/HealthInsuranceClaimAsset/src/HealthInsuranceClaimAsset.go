package main

//Template Version: 1.1-20180704
import (
	"encoding/json"
	"errors"
	"fmt"

	"github.com/hyperledger/fabric/core/chaincode/shim"
	"github.com/hyperledger/fabric/protos/peer"
)

var logger = shim.NewLogger("CLDChaincode")

//HealthInsuranceClaimAsset - Chaincode for asset HealthInsuranceClaim
type HealthInsuranceClaimAsset struct {
}

//HealthInsuranceClaim - Details of the asset type HealthInsuranceClaim
type HealthInsuranceClaim struct {
	ID                       string `json:"ID"`
	ObjectType               string `json:"docType"`
	Status                   string `json:"status"`
	CreationDate             string `json:"creationDate"`
	InsuranceType            string `json:"insuranceType"`
	OtherInsuredPolicyID     string `json:"otherInsuredPolicyID"`
	InsuredID                string `json:"insuredID"`
	PatientID                string `json:"patientID"`
	PatientRelationship      string `json:"patientRelationship"`
	PatientMaritalStatus     string `json:"patientMaritalStatus"`
	InsuranceProgram         string `json:"insuranceProgram"`
	ClaimCode                string `json:"claimCode"`
	ResubmissionCode         string `json:"resubmissionCode"`
	IllnessDate              string `json:"illnessDate"`
	HospitalizationStartDate string `json:"hospitalizationStartDate"`
	HospitalizationEndDate   string `json:"hospitalizationEndDate"`
	TotalClaimAmount         string `json:"totalClaimAmount"`
	TotalPaidAmount          string `json:"totalPaidAmount"`
	FederalTaxID             string `json:"federalTaxID"`
}

//HealthInsuranceClaimIDIndex - Index on IDs for retrieval all HealthInsuranceClaims
type HealthInsuranceClaimIDIndex struct {
	IDs []string `json:"IDs"`
}

func main() {
	err := shim.Start(new(HealthInsuranceClaimAsset))
	if err != nil {
		fmt.Printf("Error starting HealthInsuranceClaimAsset chaincode function main(): %s", err)
	} else {
		fmt.Printf("Starting HealthInsuranceClaimAsset chaincode function main() executed successfully")
	}
}

//Init - The chaincode Init function: No  arguments, only initializes a ID array as Index for retrieval of all HealthInsuranceClaims
func (hlClaim *HealthInsuranceClaimAsset) Init(stub shim.ChaincodeStubInterface) peer.Response {
	var healthInsuranceClaimIDIndex HealthInsuranceClaimIDIndex
	record, _ := stub.GetState("healthInsuranceClaimIDIndex")
	//chaincode upgrade: Index might already exist, create new only if it does not
	if record == nil {
		bytes, _ := json.Marshal(healthInsuranceClaimIDIndex)
		stub.PutState("healthInsuranceClaimIDIndex", bytes)
	}
	return shim.Success(nil)
}

//Invoke - The chaincode Invoke function:
func (hlClaim *HealthInsuranceClaimAsset) Invoke(stub shim.ChaincodeStubInterface) peer.Response {
	function, args := stub.GetFunctionAndParameters()
	logger.Debug("function: ", function)
	switch function {
	case "addNewHealthInsuranceClaim":
		return hlClaim.addNewHealthInsuranceClaim(stub, args)
	case "removeHealthInsuranceClaim":
		return hlClaim.removeHealthInsuranceClaim(stub, args[0])
	case "removeAllHealthInsuranceClaims":
		return hlClaim.removeAllHealthInsuranceClaims(stub)
	case "readHealthInsuranceClaim":
		return hlClaim.readHealthInsuranceClaim(stub, args[0])
	case "readAllHealthInsuranceClaims":
		return hlClaim.readAllHealthInsuranceClaims(stub)
	default:
		return shim.Error("Received unknown function invocation")
	}
}

//Invoke Route: addNewHealthInsuranceClaim
func (hlClaim *HealthInsuranceClaimAsset) addNewHealthInsuranceClaim(stub shim.ChaincodeStubInterface, args []string) peer.Response {
	healthInsuranceClaim, err := getHealthInsuranceClaimFromArgs(args)
	if err != nil {
		return shim.Error("HealthInsuranceClaim Data is Corrupted")
	}
	healthInsuranceClaim.ObjectType = "Asset.HealthInsuranceClaimAsset"
	record, err := stub.GetState(healthInsuranceClaim.ID)
	if record != nil {
		return shim.Error("This HealthInsuranceClaim already exists: " + healthInsuranceClaim.ID)
	}
	_, err = hlClaim.saveHealthInsuranceClaim(stub, healthInsuranceClaim)
	if err != nil {
		return shim.Error(err.Error())
	}
	_, err = hlClaim.updateHealthInsuranceClaimIDIndex(stub, healthInsuranceClaim)
	if err != nil {
		return shim.Error(err.Error())
	}
	return shim.Success(nil)
}

//Invoke Route: removeHealthInsuranceClaim
func (hlClaim *HealthInsuranceClaimAsset) removeHealthInsuranceClaim(stub shim.ChaincodeStubInterface, healthInsuranceClaimID string) peer.Response {
	_, err := hlClaim.deleteHealthInsuranceClaim(stub, healthInsuranceClaimID)
	if err != nil {
		return shim.Error(err.Error())
	}
	_, err = hlClaim.deleteHealthInsuranceClaimIDIndex(stub, healthInsuranceClaimID)
	if err != nil {
		return shim.Error(err.Error())
	}
	return shim.Success(nil)
}

//Invoke Route: removeAllHealthInsuranceClaims
func (hlClaim *HealthInsuranceClaimAsset) removeAllHealthInsuranceClaims(stub shim.ChaincodeStubInterface) peer.Response {
	var healthInsuranceClaimIDIndex HealthInsuranceClaimIDIndex
	bytes, err := stub.GetState("healthInsuranceClaimIDIndex")
	if err != nil {
		return shim.Error("removeAllHealthInsuranceClaims: Error getting healthInsuranceClaimIDIndex array")
	}
	err = json.Unmarshal(bytes, &healthInsuranceClaimIDIndex)
	if err != nil {
		return shim.Error("removeAllHealthInsuranceClaims: Error unmarshalling healthInsuranceClaimIDIndex array JSON")
	}
	if len(healthInsuranceClaimIDIndex.IDs) == 0 {
		return shim.Error("removeAllHealthInsuranceClaims: No healthInsuranceClaims to remove")
	}
	for _, healthInsuranceClaimStructID := range healthInsuranceClaimIDIndex.IDs {
		_, err = hlClaim.deleteHealthInsuranceClaim(stub, healthInsuranceClaimStructID)
		if err != nil {
			return shim.Error("Failed to remove HealthInsuranceClaim with ID: " + healthInsuranceClaimStructID)
		}
		_, err = hlClaim.deleteHealthInsuranceClaimIDIndex(stub, healthInsuranceClaimStructID)
		if err != nil {
			return shim.Error(err.Error())
		}
	}
	hlClaim.initHolder(stub)
	return shim.Success(nil)
}

//Query Route: readHealthInsuranceClaim
func (hlClaim *HealthInsuranceClaimAsset) readHealthInsuranceClaim(stub shim.ChaincodeStubInterface, healthInsuranceClaimID string) peer.Response {
	healthInsuranceClaimAsByteArray, err := hlClaim.retrieveHealthInsuranceClaim(stub, healthInsuranceClaimID)
	if err != nil {
		return shim.Error(err.Error())
	}
	return shim.Success(healthInsuranceClaimAsByteArray)
}

//Query Route: readAllHealthInsuranceClaims
func (hlClaim *HealthInsuranceClaimAsset) readAllHealthInsuranceClaims(stub shim.ChaincodeStubInterface) peer.Response {
	var healthInsuranceClaimIDs HealthInsuranceClaimIDIndex
	bytes, err := stub.GetState("healthInsuranceClaimIDIndex")
	if err != nil {
		return shim.Error("readAllHealthInsuranceClaims: Error getting healthInsuranceClaimIDIndex array")
	}
	err = json.Unmarshal(bytes, &healthInsuranceClaimIDs)
	if err != nil {
		return shim.Error("readAllHealthInsuranceClaims: Error unmarshalling healthInsuranceClaimIDIndex array JSON")
	}
	result := "["

	var healthInsuranceClaimAsByteArray []byte

	for _, healthInsuranceClaimID := range healthInsuranceClaimIDs.IDs {
		healthInsuranceClaimAsByteArray, err = hlClaim.retrieveHealthInsuranceClaim(stub, healthInsuranceClaimID)
		if err != nil {
			return shim.Error("Failed to retrieve healthInsuranceClaim with ID: " + healthInsuranceClaimID)
		}
		result += string(healthInsuranceClaimAsByteArray) + ","
	}
	if len(result) == 1 {
		result = "[]"
	} else {
		result = result[:len(result)-1] + "]"
	}
	return shim.Success([]byte(result))
}

//Helper: Save HealthInsuranceClaimAsset
func (hlClaim *HealthInsuranceClaimAsset) saveHealthInsuranceClaim(stub shim.ChaincodeStubInterface, healthInsuranceClaim HealthInsuranceClaim) (bool, error) {
	bytes, err := json.Marshal(healthInsuranceClaim)
	if err != nil {
		return false, errors.New("Error converting healthInsuranceClaim record JSON")
	}
	err = stub.PutState(healthInsuranceClaim.ID, bytes)
	if err != nil {
		return false, errors.New("Error storing HealthInsuranceClaim record")
	}
	return true, nil
}

//Helper: delete HealthInsuranceClaimAsset
func (hlClaim *HealthInsuranceClaimAsset) deleteHealthInsuranceClaim(stub shim.ChaincodeStubInterface, healthInsuranceClaimID string) (bool, error) {
	_, err := hlClaim.retrieveHealthInsuranceClaim(stub, healthInsuranceClaimID)
	if err != nil {
		return false, errors.New("HealthInsuranceClaim with ID: " + healthInsuranceClaimID + " not found")
	}
	err = stub.DelState(healthInsuranceClaimID)
	if err != nil {
		return false, errors.New("Error deleting HealthInsuranceClaim record")
	}
	return true, nil
}

//Helper: Update healthInsuranceClaim Holder - updates Index
func (hlClaim *HealthInsuranceClaimAsset) updateHealthInsuranceClaimIDIndex(stub shim.ChaincodeStubInterface, healthInsuranceClaim HealthInsuranceClaim) (bool, error) {
	var healthInsuranceClaimIDs HealthInsuranceClaimIDIndex
	bytes, err := stub.GetState("healthInsuranceClaimIDIndex")
	if err != nil {
		return false, errors.New("updateHealthInsuranceClaimIDIndex: Error getting healthInsuranceClaimIDIndex array Index from state")
	}
	err = json.Unmarshal(bytes, &healthInsuranceClaimIDs)
	if err != nil {
		return false, errors.New("updateHealthInsuranceClaimIDIndex: Error unmarshalling healthInsuranceClaimIDIndex array JSON")
	}
	healthInsuranceClaimIDs.IDs = append(healthInsuranceClaimIDs.IDs, healthInsuranceClaim.ID)
	bytes, err = json.Marshal(healthInsuranceClaimIDs)
	if err != nil {
		return false, errors.New("updateHealthInsuranceClaimIDIndex: Error marshalling new healthInsuranceClaim ID")
	}
	err = stub.PutState("healthInsuranceClaimIDIndex", bytes)
	if err != nil {
		return false, errors.New("updateHealthInsuranceClaimIDIndex: Error storing new healthInsuranceClaim ID in healthInsuranceClaimIDIndex (Index)")
	}
	return true, nil
}

//Helper: delete ID from healthInsuranceClaimStruct Holder
func (hlClaim *HealthInsuranceClaimAsset) deleteHealthInsuranceClaimIDIndex(stub shim.ChaincodeStubInterface, healthInsuranceClaimID string) (bool, error) {
	var healthInsuranceClaimIDIndex HealthInsuranceClaimIDIndex
	bytes, err := stub.GetState("healthInsuranceClaimIDIndex")
	if err != nil {
		return false, errors.New("deleteHealthInsuranceClaimIDIndex: Error getting healthInsuranceClaimIDIndex array Index from state")
	}
	err = json.Unmarshal(bytes, &healthInsuranceClaimIDIndex)
	if err != nil {
		return false, errors.New("deleteHealthInsuranceClaimIDIndex: Error unmarshalling healthInsuranceClaimIDIndex array JSON")
	}
	healthInsuranceClaimIDIndex.IDs, err = deleteKeyFromStringArray(healthInsuranceClaimIDIndex.IDs, healthInsuranceClaimID)
	if err != nil {
		return false, errors.New(err.Error())
	}
	bytes, err = json.Marshal(healthInsuranceClaimIDIndex)
	if err != nil {
		return false, errors.New("deleteHealthInsuranceClaimIDIndex: Error marshalling new healthInsuranceClaimStruct ID")
	}
	err = stub.PutState("healthInsuranceClaimIDIndex", bytes)
	if err != nil {
		return false, errors.New("deleteHealthInsuranceClaimIDIndex: Error storing new healthInsuranceClaimStruct ID in healthInsuranceClaimIDIndex (Index)")
	}
	return true, nil
}

//Helper: Initialize truck ID Holder //change template
func (hlClaim *HealthInsuranceClaimAsset) initHolder(stub shim.ChaincodeStubInterface) bool {
	var healthInsuranceClaimIDIndex HealthInsuranceClaimIDIndex
	bytes, _ := json.Marshal(healthInsuranceClaimIDIndex)
	stub.DelState("healthInsuranceClaimIDIndex")
	stub.PutState("healthInsuranceClaimIDIndex", bytes)
	return true
}

//deleteKeyFromArray
func deleteKeyFromStringArray(array []string, key string) (newArray []string, err error) {
	for _, entry := range array {
		if entry != key {
			newArray = append(newArray, entry)
		}
	}
	if len(newArray) == len(array) {
		return newArray, errors.New("Specified Key: " + key + " not found in Array")
	}
	return newArray, nil
}

//Helper: Retrieve purchaser
func (hlClaim *HealthInsuranceClaimAsset) retrieveHealthInsuranceClaim(stub shim.ChaincodeStubInterface, healthInsuranceClaimID string) ([]byte, error) {
	var healthInsuranceClaim HealthInsuranceClaim
	var healthInsuranceClaimAsByteArray []byte
	bytes, err := stub.GetState(healthInsuranceClaimID)
	if err != nil {
		return healthInsuranceClaimAsByteArray, errors.New("retrieveHealthInsuranceClaim: Error retrieving healthInsuranceClaim with ID: " + healthInsuranceClaimID)
	}
	err = json.Unmarshal(bytes, &healthInsuranceClaim)
	if err != nil {
		return healthInsuranceClaimAsByteArray, errors.New("retrieveHealthInsuranceClaim: Corrupt healthInsuranceClaim record " + string(bytes))
	}
	healthInsuranceClaimAsByteArray, err = json.Marshal(healthInsuranceClaim)
	if err != nil {
		return healthInsuranceClaimAsByteArray, errors.New("readHealthInsuranceClaim: Invalid healthInsuranceClaim Object - Not a  valid JSON")
	}
	return healthInsuranceClaimAsByteArray, nil
}

//getHealthInsuranceClaimFromArgs - construct a healthInsuranceClaim structure from string array of arguments
func getHealthInsuranceClaimFromArgs(args []string) (healthInsuranceClaim HealthInsuranceClaim, err error) {

	if !Valid(args[0]) {
		return healthInsuranceClaim, errors.New("Invalid json")
	}

	err = Unmarshal([]byte(args[0]), &healthInsuranceClaim)
	if err != nil {
		return healthInsuranceClaim, err
	}
	return healthInsuranceClaim, nil
}
