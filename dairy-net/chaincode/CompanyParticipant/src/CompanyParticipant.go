package main

//Template Version: 1.1-20180704
import (
	"encoding/json"
	"errors"
	"fmt"

	"github.com/hyperledger/fabric/core/chaincode/shim"
	"github.com/hyperledger/fabric/protos/peer"
)

var logger = shim.NewLogger("CLDChaincode")

//CompanyParticipant - Chaincode for Company Participant
type CompanyParticipant struct {
}

//Company - Details of the participant type Company
type Company struct {
 ID          string `json:"ID"`
 ObjectType   string `json:"docType"`
  Alias       string `json:"alias"`
  Description string `json:"description"`
RegistrationID string `json:"registrationID"`
CertificationID string `json:"certificationID"`
Type string `json:"type"`
Name string `json:"name"`
}

//CompanyIDIndex - Index on IDs for retrieval all Companys
type CompanyIDIndex struct {
	IDs []string `json:"IDs"`
}

func main() {
	err := shim.Start(new(CompanyParticipant))
	if err != nil {
		fmt.Printf("Error starting CompanyParticipant chaincode function main(): %s", err)
	} else {
		fmt.Printf("Starting CompanyParticipant chaincode function main() executed successfully")
	}
}

//Init - The chaincode Init function: No  arguments, only initializes a ID array as Index for retrieval of all Companys
func (com *CompanyParticipant) Init(stub shim.ChaincodeStubInterface) peer.Response {
	var companyIDIndex CompanyIDIndex
	record, _ := stub.GetState("companyIDIndex")
	//chaincode upgrade: Index might already exist, create new only if it does not
	if record == nil {
		bytes, _ := json.Marshal(companyIDIndex)
		stub.PutState("companyIDIndex", bytes)
	}
	return shim.Success(nil)
}

//Invoke - The chaincode Invoke function:
func (com *CompanyParticipant) Invoke(stub shim.ChaincodeStubInterface) peer.Response {
	function, args := stub.GetFunctionAndParameters()
	logger.Debug("function: ", function)
	switch function {
	case "addNewCompany":
		return com.addNewCompany(stub, args)
	case "removeCompany":
		return com.removeCompany(stub, args[0])
	case "removeAllCompanys":
		return com.removeAllCompanys(stub)
	case "readCompany":
		return com.readCompany(stub, args[0])
	case "readAllCompanys":
		return com.readAllCompanys(stub)
	default:
		return shim.Error("Received unknown function invocation")
	}
}

//Invoke Route: addNewCompany
func (com *CompanyParticipant) addNewCompany(stub shim.ChaincodeStubInterface, args []string) peer.Response {
	company, err := getCompanyFromArgs(args)
	if err != nil {
		return shim.Error("Company Data is Corrupted")
	}
	company.ObjectType = "Participant.CompanyParticipant"
	record, err := stub.GetState(company.ID)
	if record != nil {
		return shim.Error("This Company already exists: " + company.ID)
	}
	_, err = com.saveCompany(stub, company)
	if err != nil {
		return shim.Error(err.Error())
	}
	_, err = com.updateCompanyIDIndex(stub, company)
	if err != nil {
		return shim.Error(err.Error())
	}
	return shim.Success(nil)
}

//Invoke Route: removeCompany
func (com *CompanyParticipant) removeCompany(stub shim.ChaincodeStubInterface, companyStructParticipantID string) peer.Response {
	_, err := com.deleteCompany(stub, companyStructParticipantID)
	if err != nil {
		return shim.Error(err.Error())
	}
	_, err = com.deleteCompanyIDIndex(stub, companyStructParticipantID)
	if err != nil {
		return shim.Error(err.Error())
	}
	return shim.Success(nil)
}

//Invoke Route: removeAllCompanys
func (com *CompanyParticipant) removeAllCompanys(stub shim.ChaincodeStubInterface) peer.Response {
	var companyStructIDs CompanyIDIndex
	bytes, err := stub.GetState("companyIDIndex")
	if err != nil {
		return shim.Error("removeAllCompanys: Error getting companyIDIndex array")
	}
	err = json.Unmarshal(bytes, &companyStructIDs)
	if err != nil {
		return shim.Error("removeAllCompanys: Error unmarshalling companyIDIndex array JSON")
	}
	if len(companyStructIDs.IDs) == 0 {
		return shim.Error("removeAllCompanys: No companys to remove")
	}
	for _, companyStructParticipantID := range companyStructIDs.IDs {
		_, err = com.deleteCompany(stub, companyStructParticipantID)
		if err != nil {
			return shim.Error("Failed to remove Company with ID: " + companyStructParticipantID)
		}
		_, err = com.deleteCompanyIDIndex(stub, companyStructParticipantID)
		if err != nil {
			return shim.Error(err.Error())
		}
	}
	com.initHolder(stub)
	return shim.Success(nil)
}

//Query Route: readCompany
func (com *CompanyParticipant) readCompany(stub shim.ChaincodeStubInterface, companyParticipantID string) peer.Response {
	companyAsByteArray, err := com.retrieveCompany(stub, companyParticipantID)
	if err != nil {
		return shim.Error(err.Error())
	}
	return shim.Success(companyAsByteArray)
}

//Query Route: readAllCompanys
func (com *CompanyParticipant) readAllCompanys(stub shim.ChaincodeStubInterface) peer.Response {
	var companyIDs CompanyIDIndex
	bytes, err := stub.GetState("companyIDIndex")
	if err != nil {
		return shim.Error("readAllCompanys: Error getting companyIDIndex array")
	}
	err = json.Unmarshal(bytes, &companyIDs)
	if err != nil {
		return shim.Error("readAllCompanys: Error unmarshalling companyIDIndex array JSON")
	}
	result := "["

	var companyAsByteArray []byte

	for _, companyID := range companyIDs.IDs {
		companyAsByteArray, err = com.retrieveCompany(stub, companyID)
		if err != nil {
			return shim.Error("Failed to retrieve company with ID: " + companyID)
		}
		result += string(companyAsByteArray) + ","
	}
	if len(result) == 1 {
		result = "[]"
	} else {
		result = result[:len(result)-1] + "]"
	}
	return shim.Success([]byte(result))
}

//Helper: Save companyParticipant
func (com *CompanyParticipant) saveCompany(stub shim.ChaincodeStubInterface, company Company) (bool, error) {
	bytes, err := json.Marshal(company)
	if err != nil {
		return false, errors.New("Error converting company record JSON")
	}
	err = stub.PutState(company.ID, bytes)
	if err != nil {
		return false, errors.New("Error storing Company record")
	}
	return true, nil
}

//Helper: Delete companyStructParticipant
func (com *CompanyParticipant) deleteCompany(stub shim.ChaincodeStubInterface, companyStructParticipantID string) (bool, error) {
	_, err := com.retrieveCompany(stub, companyStructParticipantID)
	if err != nil {
		return false, errors.New("Company with ID: " + companyStructParticipantID + " not found")
	}
	err = stub.DelState(companyStructParticipantID)
	if err != nil {
		return false, errors.New("Error deleting Company record")
	}
	return true, nil
}

//Helper: Update company Holder - updates Index
func (com *CompanyParticipant) updateCompanyIDIndex(stub shim.ChaincodeStubInterface, company Company) (bool, error) {
	var companyIDs CompanyIDIndex
	bytes, err := stub.GetState("companyIDIndex")
	if err != nil {
		return false, errors.New("upadeteCompanyIDIndex: Error getting companyIDIndex array Index from state")
	}
	err = json.Unmarshal(bytes, &companyIDs)
	if err != nil {
		return false, errors.New("upadeteCompanyIDIndex: Error unmarshalling companyIDIndex array JSON")
	}
	companyIDs.IDs = append(companyIDs.IDs, company.ID)
	bytes, err = json.Marshal(companyIDs)
	if err != nil {
		return false, errors.New("updateCompanyIDIndex: Error marshalling new company ID")
	}
	err = stub.PutState("companyIDIndex", bytes)
	if err != nil {
		return false, errors.New("updateCompanyIDIndex: Error storing new company ID in companyIDIndex (Index)")
	}
	return true, nil
}

//Helper: delete ID from companyStruct Holder
func (com *CompanyParticipant) deleteCompanyIDIndex(stub shim.ChaincodeStubInterface, companyStructParticipantID string) (bool, error) {
	var companyStructIDs CompanyIDIndex
	bytes, err := stub.GetState("companyIDIndex")
	if err != nil {
		return false, errors.New("deleteCompanyIDIndex: Error getting companyIDIndex array Index from state")
	}
	err = json.Unmarshal(bytes, &companyStructIDs)
	if err != nil {
		return false, errors.New("deleteCompanyIDIndex: Error unmarshalling companyIDIndex array JSON")
	}
	companyStructIDs.IDs, err = deleteKeyFromStringArray(companyStructIDs.IDs, companyStructParticipantID)
	if err != nil {
		return false, errors.New(err.Error())
	}
	bytes, err = json.Marshal(companyStructIDs)
	if err != nil {
		return false, errors.New("deleteCompanyIDIndex: Error marshalling new companyStruct ID")
	}
	err = stub.PutState("companyIDIndex", bytes)
	if err != nil {
		return false, errors.New("deleteCompanyIDIndex: Error storing new companyStruct ID in companyIDIndex (Index)")
	}
	return true, nil
}

//Helper: Initialize company ID Holder
func (com *CompanyParticipant) initHolder(stub shim.ChaincodeStubInterface) bool {
	var companyIDIndex CompanyIDIndex
	bytes, _ := json.Marshal(companyIDIndex)
	stub.DelState("companyIDIndex")
	stub.PutState("companyIDIndex", bytes)
	return true
}

//Helper: Retrieve companyParticipant
func (com *CompanyParticipant) retrieveCompany(stub shim.ChaincodeStubInterface, companyParticipantID string) ([]byte, error) {
	var company Company
	var companyAsByteArray []byte
	bytes, err := stub.GetState(companyParticipantID)
	if err != nil {
		return companyAsByteArray, errors.New("retrieveCompany: Error retrieving company with ID: " + companyParticipantID)
	}
	err = json.Unmarshal(bytes, &company)
	if err != nil {
		return companyAsByteArray, errors.New("retrieveCompany: Corrupt company record " + string(bytes))
	}
	companyAsByteArray, err = json.Marshal(company)
	if err != nil {
		return companyAsByteArray, errors.New("readCompany: Invalid company Object - Not a  valid JSON")
	}
	return companyAsByteArray, nil
}

//deleteKeyFromArray
func deleteKeyFromStringArray(array []string, key string) (newArray []string, err error) {
	for _, entry := range array {
		if entry != key {
			newArray = append(newArray, entry)
		}
	}
	if len(newArray) == len(array) {
		return newArray, errors.New("Specified Key: " + key + " not found in Array")
	}
	return newArray, nil
}

//getCompanyFromArgs - construct a company structure from string array of arguments
func getCompanyFromArgs(args []string) (company Company, err error) {

	if !Valid(args[0]) {
		return company, errors.New("Invalid json")
	}

	err = Unmarshal([]byte(args[0]), &company)
	if err != nil {
		return company, err
	}
	return company, nil
}
