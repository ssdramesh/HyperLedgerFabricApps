package main

//Template Version: 1.1-20180704
import (
	"encoding/json"
	"errors"
	"fmt"

	"github.com/hyperledger/fabric/core/chaincode/shim"
	"github.com/hyperledger/fabric/protos/peer"
)

var logger = shim.NewLogger("CLDChaincode")

//CertificateAsset - Chaincode for asset Certificate
type CertificateAsset struct {
}

//Certificate - Details of the asset type Certificate
type Certificate struct {
 ID          string `json:"ID"`
 ObjectType   string `json:"docType"`
  Status       string `json:"status"`
  CreationDate string `json:"creationDate"`
CertificationAuthorityID string `json:"certificationAuthorityID"`
CompanyID string `json:"companyID"`
IssueDate string `json:"issueDate"`
ValidityStartDate string `json:"validityStartDate"`
ValidityEndDate string `json:"validityEndDate"`
}

//CertificateIDIndex - Index on IDs for retrieval all Certificates
type CertificateIDIndex struct {
	IDs []string `json:"IDs"`
}

func main() {
	err := shim.Start(new(CertificateAsset))
	if err != nil {
		fmt.Printf("Error starting CertificateAsset chaincode function main(): %s", err)
	} else {
		fmt.Printf("Starting CertificateAsset chaincode function main() executed successfully")
	}
}

//Init - The chaincode Init function: No  arguments, only initializes a ID array as Index for retrieval of all Certificates
func (cert *CertificateAsset) Init(stub shim.ChaincodeStubInterface) peer.Response {
	var certificateIDIndex CertificateIDIndex
	record, _ := stub.GetState("certificateIDIndex")
	//chaincode upgrade: Index might already exist, create new only if it does not
	if record == nil {
		bytes, _ := json.Marshal(certificateIDIndex)
		stub.PutState("certificateIDIndex", bytes)
	}
	return shim.Success(nil)
}

//Invoke - The chaincode Invoke function:
func (cert *CertificateAsset) Invoke(stub shim.ChaincodeStubInterface) peer.Response {
	function, args := stub.GetFunctionAndParameters()
	logger.Debug("function: ", function)
	switch function {
	case "addNewCertificate":
		return cert.addNewCertificate(stub, args)
	case "removeCertificate":
		return cert.removeCertificate(stub, args[0])
	case "removeAllCertificates":
		return cert.removeAllCertificates(stub)
	case "readCertificate":
		return cert.readCertificate(stub, args[0])
	case "readAllCertificates":
		return cert.readAllCertificates(stub)
	default:
		return shim.Error("Received unknown function invocation")
	}
}

//Invoke Route: addNewCertificate
func (cert *CertificateAsset) addNewCertificate(stub shim.ChaincodeStubInterface, args []string) peer.Response {
	certificate, err := getCertificateFromArgs(args)
	if err != nil {
		return shim.Error("Certificate Data is Corrupted")
	}
	certificate.ObjectType = "Asset.CertificateAsset"
	record, err := stub.GetState(certificate.ID)
	if record != nil {
		return shim.Error("This Certificate already exists: " + certificate.ID)
	}
	_, err = cert.saveCertificate(stub, certificate)
	if err != nil {
		return shim.Error(err.Error())
	}
	_, err = cert.updateCertificateIDIndex(stub, certificate)
	if err != nil {
		return shim.Error(err.Error())
	}
	return shim.Success(nil)
}

//Invoke Route: removeCertificate
func (cert *CertificateAsset) removeCertificate(stub shim.ChaincodeStubInterface, certificateID string) peer.Response {
	_, err := cert.deleteCertificate(stub, certificateID)
	if err != nil {
		return shim.Error(err.Error())
	}
	_, err = cert.deleteCertificateIDIndex(stub, certificateID)
	if err != nil {
		return shim.Error(err.Error())
	}
	return shim.Success(nil)
}

//Invoke Route: removeAllCertificates
func (cert *CertificateAsset) removeAllCertificates(stub shim.ChaincodeStubInterface) peer.Response {
	var certificateIDIndex CertificateIDIndex
	bytes, err := stub.GetState("certificateIDIndex")
	if err != nil {
		return shim.Error("removeAllCertificates: Error getting certificateIDIndex array")
	}
	err = json.Unmarshal(bytes, &certificateIDIndex)
	if err != nil {
		return shim.Error("removeAllCertificates: Error unmarshalling certificateIDIndex array JSON")
	}
	if len(certificateIDIndex.IDs) == 0 {
		return shim.Error("removeAllCertificates: No certificates to remove")
	}
	for _, certificateStructID := range certificateIDIndex.IDs {
		_, err = cert.deleteCertificate(stub, certificateStructID)
		if err != nil {
			return shim.Error("Failed to remove Certificate with ID: " + certificateStructID)
		}
		_, err = cert.deleteCertificateIDIndex(stub, certificateStructID)
		if err != nil {
			return shim.Error(err.Error())
		}
	}
	cert.initHolder(stub)
	return shim.Success(nil)
}

//Query Route: readCertificate
func (cert *CertificateAsset) readCertificate(stub shim.ChaincodeStubInterface, certificateID string) peer.Response {
	certificateAsByteArray, err := cert.retrieveCertificate(stub, certificateID)
	if err != nil {
		return shim.Error(err.Error())
	}
	return shim.Success(certificateAsByteArray)
}

//Query Route: readAllCertificates
func (cert *CertificateAsset) readAllCertificates(stub shim.ChaincodeStubInterface) peer.Response {
	var certificateIDs CertificateIDIndex
	bytes, err := stub.GetState("certificateIDIndex")
	if err != nil {
		return shim.Error("readAllCertificates: Error getting certificateIDIndex array")
	}
	err = json.Unmarshal(bytes, &certificateIDs)
	if err != nil {
		return shim.Error("readAllCertificates: Error unmarshalling certificateIDIndex array JSON")
	}
	result := "["

	var certificateAsByteArray []byte

	for _, certificateID := range certificateIDs.IDs {
		certificateAsByteArray, err = cert.retrieveCertificate(stub, certificateID)
		if err != nil {
			return shim.Error("Failed to retrieve certificate with ID: " + certificateID)
		}
		result += string(certificateAsByteArray) + ","
	}
	if len(result) == 1 {
		result = "[]"
	} else {
		result = result[:len(result)-1] + "]"
	}
	return shim.Success([]byte(result))
}

//Helper: Save CertificateAsset
func (cert *CertificateAsset) saveCertificate(stub shim.ChaincodeStubInterface, certificate Certificate) (bool, error) {
	bytes, err := json.Marshal(certificate)
	if err != nil {
		return false, errors.New("Error converting certificate record JSON")
	}
	err = stub.PutState(certificate.ID, bytes)
	if err != nil {
		return false, errors.New("Error storing Certificate record")
	}
	return true, nil
}

//Helper: delete CertificateAsset
func (cert *CertificateAsset) deleteCertificate(stub shim.ChaincodeStubInterface, certificateID string) (bool, error) {
	_, err := cert.retrieveCertificate(stub, certificateID)
	if err != nil {
		return false, errors.New("Certificate with ID: " + certificateID + " not found")
	}
	err = stub.DelState(certificateID)
	if err != nil {
		return false, errors.New("Error deleting Certificate record")
	}
	return true, nil
}

//Helper: Update certificate Holder - updates Index
func (cert *CertificateAsset) updateCertificateIDIndex(stub shim.ChaincodeStubInterface, certificate Certificate) (bool, error) {
	var certificateIDs CertificateIDIndex
	bytes, err := stub.GetState("certificateIDIndex")
	if err != nil {
		return false, errors.New("updateCertificateIDIndex: Error getting certificateIDIndex array Index from state")
	}
	err = json.Unmarshal(bytes, &certificateIDs)
	if err != nil {
		return false, errors.New("updateCertificateIDIndex: Error unmarshalling certificateIDIndex array JSON")
	}
	certificateIDs.IDs = append(certificateIDs.IDs, certificate.ID)
	bytes, err = json.Marshal(certificateIDs)
	if err != nil {
		return false, errors.New("updateCertificateIDIndex: Error marshalling new certificate ID")
	}
	err = stub.PutState("certificateIDIndex", bytes)
	if err != nil {
		return false, errors.New("updateCertificateIDIndex: Error storing new certificate ID in certificateIDIndex (Index)")
	}
	return true, nil
}

//Helper: delete ID from certificateStruct Holder
func (cert *CertificateAsset) deleteCertificateIDIndex(stub shim.ChaincodeStubInterface, certificateID string) (bool, error) {
	var certificateIDIndex CertificateIDIndex
	bytes, err := stub.GetState("certificateIDIndex")
	if err != nil {
		return false, errors.New("deleteCertificateIDIndex: Error getting certificateIDIndex array Index from state")
	}
	err = json.Unmarshal(bytes, &certificateIDIndex)
	if err != nil {
		return false, errors.New("deleteCertificateIDIndex: Error unmarshalling certificateIDIndex array JSON")
	}
	certificateIDIndex.IDs, err = deleteKeyFromStringArray(certificateIDIndex.IDs, certificateID)
	if err != nil {
		return false, errors.New(err.Error())
	}
	bytes, err = json.Marshal(certificateIDIndex)
	if err != nil {
		return false, errors.New("deleteCertificateIDIndex: Error marshalling new certificateStruct ID")
	}
	err = stub.PutState("certificateIDIndex", bytes)
	if err != nil {
		return false, errors.New("deleteCertificateIDIndex: Error storing new certificateStruct ID in certificateIDIndex (Index)")
	}
	return true, nil
}

//Helper: Initialize truck ID Holder //change template
func (cert *CertificateAsset) initHolder(stub shim.ChaincodeStubInterface) bool {
	var certificateIDIndex CertificateIDIndex
	bytes, _ := json.Marshal(certificateIDIndex)
	stub.DelState("certificateIDIndex")
	stub.PutState("certificateIDIndex", bytes)
	return true
}

//deleteKeyFromArray
func deleteKeyFromStringArray(array []string, key string) (newArray []string, err error) {
	for _, entry := range array {
		if entry != key {
			newArray = append(newArray, entry)
		}
	}
	if len(newArray) == len(array) {
		return newArray, errors.New("Specified Key: " + key + " not found in Array")
	}
	return newArray, nil
}

//Helper: Retrieve purchaser
func (cert *CertificateAsset) retrieveCertificate(stub shim.ChaincodeStubInterface, certificateID string) ([]byte, error) {
	var certificate Certificate
	var certificateAsByteArray []byte
	bytes, err := stub.GetState(certificateID)
	if err != nil {
		return certificateAsByteArray, errors.New("retrieveCertificate: Error retrieving certificate with ID: " + certificateID)
	}
	err = json.Unmarshal(bytes, &certificate)
	if err != nil {
		return certificateAsByteArray, errors.New("retrieveCertificate: Corrupt certificate record " + string(bytes))
	}
	certificateAsByteArray, err = json.Marshal(certificate)
	if err != nil {
		return certificateAsByteArray, errors.New("readCertificate: Invalid certificate Object - Not a  valid JSON")
	}
	return certificateAsByteArray, nil
}

//getCertificateFromArgs - construct a certificate structure from string array of arguments
func getCertificateFromArgs(args []string) (certificate Certificate, err error) {

	if !Valid(args[0]) {
		return certificate, errors.New("Invalid json")
	}

	err = Unmarshal([]byte(args[0]), &certificate)
	if err != nil {
		return certificate, err
	}
	return certificate, nil
}
