package main

//Template Version: 1.1-20180704
import (
	"encoding/json"
	"errors"
	"fmt"

	"github.com/hyperledger/fabric/core/chaincode/shim"
	"github.com/hyperledger/fabric/protos/peer"
)

var logger = shim.NewLogger("CLDChaincode")

//BatchAsset - Chaincode for asset Batch
type BatchAsset struct {
}

//Batch - Details of the asset type Batch
type Batch struct {
 ID          string `json:"ID"`
 ObjectType   string `json:"docType"`
  Status       string `json:"status"`
  CreationDate string `json:"creationDate"`
Volume string `json:"volume"`
FatPercent string `json:"fatPercent"`
SNF string `json:"SNF"`
ProductID string `json:"productID"`
ProductDescription string `json:"productDescription"`
}

//BatchIDIndex - Index on IDs for retrieval all Batchs
type BatchIDIndex struct {
	IDs []string `json:"IDs"`
}

func main() {
	err := shim.Start(new(BatchAsset))
	if err != nil {
		fmt.Printf("Error starting BatchAsset chaincode function main(): %s", err)
	} else {
		fmt.Printf("Starting BatchAsset chaincode function main() executed successfully")
	}
}

//Init - The chaincode Init function: No  arguments, only initializes a ID array as Index for retrieval of all Batchs
func (bat *BatchAsset) Init(stub shim.ChaincodeStubInterface) peer.Response {
	var batchIDIndex BatchIDIndex
	record, _ := stub.GetState("batchIDIndex")
	//chaincode upgrade: Index might already exist, create new only if it does not
	if record == nil {
		bytes, _ := json.Marshal(batchIDIndex)
		stub.PutState("batchIDIndex", bytes)
	}
	return shim.Success(nil)
}

//Invoke - The chaincode Invoke function:
func (bat *BatchAsset) Invoke(stub shim.ChaincodeStubInterface) peer.Response {
	function, args := stub.GetFunctionAndParameters()
	logger.Debug("function: ", function)
	switch function {
	case "addNewBatch":
		return bat.addNewBatch(stub, args)
	case "removeBatch":
		return bat.removeBatch(stub, args[0])
	case "removeAllBatchs":
		return bat.removeAllBatchs(stub)
	case "readBatch":
		return bat.readBatch(stub, args[0])
	case "readAllBatchs":
		return bat.readAllBatchs(stub)
	default:
		return shim.Error("Received unknown function invocation")
	}
}

//Invoke Route: addNewBatch
func (bat *BatchAsset) addNewBatch(stub shim.ChaincodeStubInterface, args []string) peer.Response {
	batch, err := getBatchFromArgs(args)
	if err != nil {
		return shim.Error("Batch Data is Corrupted")
	}
	batch.ObjectType = "Asset.BatchAsset"
	record, err := stub.GetState(batch.ID)
	if record != nil {
		return shim.Error("This Batch already exists: " + batch.ID)
	}
	_, err = bat.saveBatch(stub, batch)
	if err != nil {
		return shim.Error(err.Error())
	}
	_, err = bat.updateBatchIDIndex(stub, batch)
	if err != nil {
		return shim.Error(err.Error())
	}
	return shim.Success(nil)
}

//Invoke Route: removeBatch
func (bat *BatchAsset) removeBatch(stub shim.ChaincodeStubInterface, batchID string) peer.Response {
	_, err := bat.deleteBatch(stub, batchID)
	if err != nil {
		return shim.Error(err.Error())
	}
	_, err = bat.deleteBatchIDIndex(stub, batchID)
	if err != nil {
		return shim.Error(err.Error())
	}
	return shim.Success(nil)
}

//Invoke Route: removeAllBatchs
func (bat *BatchAsset) removeAllBatchs(stub shim.ChaincodeStubInterface) peer.Response {
	var batchIDIndex BatchIDIndex
	bytes, err := stub.GetState("batchIDIndex")
	if err != nil {
		return shim.Error("removeAllBatchs: Error getting batchIDIndex array")
	}
	err = json.Unmarshal(bytes, &batchIDIndex)
	if err != nil {
		return shim.Error("removeAllBatchs: Error unmarshalling batchIDIndex array JSON")
	}
	if len(batchIDIndex.IDs) == 0 {
		return shim.Error("removeAllBatchs: No batchs to remove")
	}
	for _, batchStructID := range batchIDIndex.IDs {
		_, err = bat.deleteBatch(stub, batchStructID)
		if err != nil {
			return shim.Error("Failed to remove Batch with ID: " + batchStructID)
		}
		_, err = bat.deleteBatchIDIndex(stub, batchStructID)
		if err != nil {
			return shim.Error(err.Error())
		}
	}
	bat.initHolder(stub)
	return shim.Success(nil)
}

//Query Route: readBatch
func (bat *BatchAsset) readBatch(stub shim.ChaincodeStubInterface, batchID string) peer.Response {
	batchAsByteArray, err := bat.retrieveBatch(stub, batchID)
	if err != nil {
		return shim.Error(err.Error())
	}
	return shim.Success(batchAsByteArray)
}

//Query Route: readAllBatchs
func (bat *BatchAsset) readAllBatchs(stub shim.ChaincodeStubInterface) peer.Response {
	var batchIDs BatchIDIndex
	bytes, err := stub.GetState("batchIDIndex")
	if err != nil {
		return shim.Error("readAllBatchs: Error getting batchIDIndex array")
	}
	err = json.Unmarshal(bytes, &batchIDs)
	if err != nil {
		return shim.Error("readAllBatchs: Error unmarshalling batchIDIndex array JSON")
	}
	result := "["

	var batchAsByteArray []byte

	for _, batchID := range batchIDs.IDs {
		batchAsByteArray, err = bat.retrieveBatch(stub, batchID)
		if err != nil {
			return shim.Error("Failed to retrieve batch with ID: " + batchID)
		}
		result += string(batchAsByteArray) + ","
	}
	if len(result) == 1 {
		result = "[]"
	} else {
		result = result[:len(result)-1] + "]"
	}
	return shim.Success([]byte(result))
}

//Helper: Save BatchAsset
func (bat *BatchAsset) saveBatch(stub shim.ChaincodeStubInterface, batch Batch) (bool, error) {
	bytes, err := json.Marshal(batch)
	if err != nil {
		return false, errors.New("Error converting batch record JSON")
	}
	err = stub.PutState(batch.ID, bytes)
	if err != nil {
		return false, errors.New("Error storing Batch record")
	}
	return true, nil
}

//Helper: delete BatchAsset
func (bat *BatchAsset) deleteBatch(stub shim.ChaincodeStubInterface, batchID string) (bool, error) {
	_, err := bat.retrieveBatch(stub, batchID)
	if err != nil {
		return false, errors.New("Batch with ID: " + batchID + " not found")
	}
	err = stub.DelState(batchID)
	if err != nil {
		return false, errors.New("Error deleting Batch record")
	}
	return true, nil
}

//Helper: Update batch Holder - updates Index
func (bat *BatchAsset) updateBatchIDIndex(stub shim.ChaincodeStubInterface, batch Batch) (bool, error) {
	var batchIDs BatchIDIndex
	bytes, err := stub.GetState("batchIDIndex")
	if err != nil {
		return false, errors.New("updateBatchIDIndex: Error getting batchIDIndex array Index from state")
	}
	err = json.Unmarshal(bytes, &batchIDs)
	if err != nil {
		return false, errors.New("updateBatchIDIndex: Error unmarshalling batchIDIndex array JSON")
	}
	batchIDs.IDs = append(batchIDs.IDs, batch.ID)
	bytes, err = json.Marshal(batchIDs)
	if err != nil {
		return false, errors.New("updateBatchIDIndex: Error marshalling new batch ID")
	}
	err = stub.PutState("batchIDIndex", bytes)
	if err != nil {
		return false, errors.New("updateBatchIDIndex: Error storing new batch ID in batchIDIndex (Index)")
	}
	return true, nil
}

//Helper: delete ID from batchStruct Holder
func (bat *BatchAsset) deleteBatchIDIndex(stub shim.ChaincodeStubInterface, batchID string) (bool, error) {
	var batchIDIndex BatchIDIndex
	bytes, err := stub.GetState("batchIDIndex")
	if err != nil {
		return false, errors.New("deleteBatchIDIndex: Error getting batchIDIndex array Index from state")
	}
	err = json.Unmarshal(bytes, &batchIDIndex)
	if err != nil {
		return false, errors.New("deleteBatchIDIndex: Error unmarshalling batchIDIndex array JSON")
	}
	batchIDIndex.IDs, err = deleteKeyFromStringArray(batchIDIndex.IDs, batchID)
	if err != nil {
		return false, errors.New(err.Error())
	}
	bytes, err = json.Marshal(batchIDIndex)
	if err != nil {
		return false, errors.New("deleteBatchIDIndex: Error marshalling new batchStruct ID")
	}
	err = stub.PutState("batchIDIndex", bytes)
	if err != nil {
		return false, errors.New("deleteBatchIDIndex: Error storing new batchStruct ID in batchIDIndex (Index)")
	}
	return true, nil
}

//Helper: Initialize truck ID Holder //change template
func (bat *BatchAsset) initHolder(stub shim.ChaincodeStubInterface) bool {
	var batchIDIndex BatchIDIndex
	bytes, _ := json.Marshal(batchIDIndex)
	stub.DelState("batchIDIndex")
	stub.PutState("batchIDIndex", bytes)
	return true
}

//deleteKeyFromArray
func deleteKeyFromStringArray(array []string, key string) (newArray []string, err error) {
	for _, entry := range array {
		if entry != key {
			newArray = append(newArray, entry)
		}
	}
	if len(newArray) == len(array) {
		return newArray, errors.New("Specified Key: " + key + " not found in Array")
	}
	return newArray, nil
}

//Helper: Retrieve purchaser
func (bat *BatchAsset) retrieveBatch(stub shim.ChaincodeStubInterface, batchID string) ([]byte, error) {
	var batch Batch
	var batchAsByteArray []byte
	bytes, err := stub.GetState(batchID)
	if err != nil {
		return batchAsByteArray, errors.New("retrieveBatch: Error retrieving batch with ID: " + batchID)
	}
	err = json.Unmarshal(bytes, &batch)
	if err != nil {
		return batchAsByteArray, errors.New("retrieveBatch: Corrupt batch record " + string(bytes))
	}
	batchAsByteArray, err = json.Marshal(batch)
	if err != nil {
		return batchAsByteArray, errors.New("readBatch: Invalid batch Object - Not a  valid JSON")
	}
	return batchAsByteArray, nil
}

//getBatchFromArgs - construct a batch structure from string array of arguments
func getBatchFromArgs(args []string) (batch Batch, err error) {

	if !Valid(args[0]) {
		return batch, errors.New("Invalid json")
	}

	err = Unmarshal([]byte(args[0]), &batch)
	if err != nil {
		return batch, err
	}
	return batch, nil
}
