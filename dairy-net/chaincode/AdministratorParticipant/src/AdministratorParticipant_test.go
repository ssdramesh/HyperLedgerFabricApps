package main

//Template Version: 1.1-20180704
import (
	"bytes"
	"encoding/json"
	"fmt"
	"strings"
	"testing"

	"github.com/hyperledger/fabric/core/chaincode/shim"
)

//TestAdministratorParticipant_Init
func TestAdministratorParticipant_Init(t *testing.T) {
	administrator := new(AdministratorParticipant)
	stub := shim.NewMockStub("administrator", administrator)
	checkInit(t, stub, [][]byte{[]byte("init")})
	checkState(t, stub, "administratorIDIndex", getExpectedAdministratorIDIndex("init"))
}

//TestAdministratorParticipant_InvokeUnknownFunction
func TestAdministratorParticipant_InvokeUnknownFunction(t *testing.T) {
	administrator := new(AdministratorParticipant)
	stub := shim.NewMockStub("administrator", administrator)
	checkInit(t, stub, [][]byte{[]byte("init")})
	checkInvokeUnknownFunction(t, stub, [][]byte{[]byte("myFunction"), []byte("docType:Participant")})
}

//TestAdministratorParticipant_Invoke_addNewAdministrator
func TestAdministratorParticipant_Invoke_addNewAdministratorOK(t *testing.T) {
	administrator := new(AdministratorParticipant)
	stub := shim.NewMockStub("administrator", administrator)
	checkInit(t, stub, [][]byte{[]byte("init")})
	checkInvoke(t, stub, getFirstAdministratorParticipantForTesting())
	newAdministratorID := "100001"
	checkState(t, stub, newAdministratorID, getNewAdministratorExpected())
	checkState(t, stub, "administratorIDIndex", getExpectedAdministratorIDIndex("addNewAdministrator"))
}

//TestAdministratorParticipant_Invoke_addNewAdministrator
func TestAdministratorParticipant_Invoke_addNewAdministratorDuplicate(t *testing.T) {
	administrator := new(AdministratorParticipant)
	stub := shim.NewMockStub("administrator", administrator)
	checkInit(t, stub, [][]byte{[]byte("init")})
	checkInvoke(t, stub, getFirstAdministratorParticipantForTesting())
	newAdministratorID := "100001"
	checkState(t, stub, newAdministratorID, getNewAdministratorExpected())
	checkState(t, stub, "administratorIDIndex", getExpectedAdministratorIDIndex("addNewAdministrator"))
	res := stub.MockInvoke("1", getFirstAdministratorParticipantForTesting())
	if res.Status != shim.OK {
		checkError(t, "This Administrator already exists: 100001", res.Message)
	} else {
		fmt.Println("Error was expected, but not raised")
		t.FailNow()
	}
}

//TestAdministratorParticipant_Invoke_removeAdministratorOK  //change template
func TestAdministratorParticipant_Invoke_removeAdministratorOK(t *testing.T) {
	administrator := new(AdministratorParticipant)
	stub := shim.NewMockStub("administrator", administrator)
	checkInit(t, stub, [][]byte{[]byte("init")})
	checkInvoke(t, stub, getFirstAdministratorParticipantForTesting())
	checkInvoke(t, stub, getSecondAdministratorParticipantForTesting())
	checkReadAllAdministratorsOK(t, stub)
	checkState(t, stub, "administratorIDIndex", getExpectedAdministratorIDIndex("beforeRemoveAdministrator"))
	checkInvoke(t, stub, getRemoveSecondAdministratorParticipantForTesting())
	remainingAdministratorID := "100001"
	checkReadAdministratorOK(t, stub, remainingAdministratorID)
	checkState(t, stub, "administratorIDIndex", getExpectedAdministratorIDIndex("afterRemoveAdministrator"))
}

//TestAdministratorParticipant_Invoke_removeAdministratorNOK  //change template
func TestAdministratorParticipant_Invoke_removeAdministratorNOK(t *testing.T) {
	administrator := new(AdministratorParticipant)
	stub := shim.NewMockStub("administrator", administrator)
	checkInit(t, stub, [][]byte{[]byte("init")})
	checkInvoke(t, stub, getFirstAdministratorParticipantForTesting())
	firstAdministratorID := "100001"
	checkReadAdministratorOK(t, stub, firstAdministratorID)
	checkState(t, stub, "administratorIDIndex", getExpectedAdministratorIDIndex("addNewAdministrator"))
	res := stub.MockInvoke("1", getRemoveSecondAdministratorParticipantForTesting())
	if res.Status != shim.OK {
		checkError(t, "Administrator with ID: "+"100002"+" not found", res.Message)
	} else {
		fmt.Println("Error was expected, but not raised")
		t.FailNow()
	}
	checkState(t, stub, "administratorIDIndex", getExpectedAdministratorIDIndex("addNewAdministrator"))
}

//TestAdministratorParticipant_Invoke_removeAllAdministratorsOK  //change template
func TestAdministratorParticipant_Invoke_removeAllAdministratorsOK(t *testing.T) {
	administrator := new(AdministratorParticipant)
	stub := shim.NewMockStub("administrator", administrator)
	checkInit(t, stub, [][]byte{[]byte("init")})
	checkInvoke(t, stub, getFirstAdministratorParticipantForTesting())
	checkInvoke(t, stub, getSecondAdministratorParticipantForTesting())
	checkReadAllAdministratorsOK(t, stub)
	checkState(t, stub, "administratorIDIndex", getExpectedAdministratorIDIndex("beforeRemoveAdministrator"))
	checkInvoke(t, stub, getRemoveAllAdministratorParticipantsForTesting())
	checkState(t, stub, "administratorIDIndex", getExpectedAdministratorIDIndex(""))
}

//TestAdministratorParticipant_Invoke_removeAdministratorNOK  //change template
func TestAdministratorParticipant_Invoke_removeAllAdministratorsNOK(t *testing.T) {
	administrator := new(AdministratorParticipant)
	stub := shim.NewMockStub("administrator", administrator)
	checkInit(t, stub, [][]byte{[]byte("init")})
	res := stub.MockInvoke("1", getRemoveAllAdministratorParticipantsForTesting())
	if res.Status != shim.OK {
		checkError(t, "removeAllAdministrators: No administrators to remove", res.Message)
	} else {
		fmt.Println("Error was expected, but not raised")
		t.FailNow()
	}
	checkState(t, stub, "administratorIDIndex", getExpectedAdministratorIDIndex(""))
}

//TestAdministratorParticipant_Query_readAdministrator
func TestAdministratorParticipant_Query_readAdministrator(t *testing.T) {
	administrator := new(AdministratorParticipant)
	stub := shim.NewMockStub("administrator", administrator)
	checkInit(t, stub, [][]byte{[]byte("init")})
	administratorID := "100001"
	checkInvoke(t, stub, getFirstAdministratorParticipantForTesting())
	checkReadAdministratorOK(t, stub, administratorID)
	checkReadAdministratorNOK(t, stub, "")
}

//TestAdministratorParticipant_Query_readAllAdministrators
func TestAdministratorParticipant_Query_readAllAdministrators(t *testing.T) {
	administrator := new(AdministratorParticipant)
	stub := shim.NewMockStub("administrator", administrator)
	checkInit(t, stub, [][]byte{[]byte("init")})
	checkInvoke(t, stub, getFirstAdministratorParticipantForTesting())
	checkInvoke(t, stub, getSecondAdministratorParticipantForTesting())
	checkReadAllAdministratorsOK(t, stub)
}

/*
*
*	Helper Functions
*
 */
//Get first AdministratorParticipant for testing
func getFirstAdministratorParticipantForTesting() [][]byte { 
 return [][]byte{[]byte("addNewAdministrator"), 
 []byte("{\"ID\":\"100001\",\"docType\":\"Participant.AdministratorParticipant\",\"alias\":\"BRITECH\",\"description\":\"British Technology Pvt. Ltd.\"}")}
}

//Get second AdministratorParticipant for testing
func getSecondAdministratorParticipantForTesting() [][]byte { 
 return [][]byte{[]byte("addNewAdministrator"), 
 []byte("{\"ID\":\"100002\",\"docType\":\"Participant.AdministratorParticipant\",\"alias\":\"DEMAGDELAG\",\"description\":\"Demag Delewal AG\"}")}
}

//Get remove second AdministratorParticipant for testing //change template
func getRemoveSecondAdministratorParticipantForTesting() [][]byte {
	return [][]byte{[]byte("removeAdministrator"),
		[]byte("100002")}
}

//Get remove all AdministratorParticipants for testing //change template
func getRemoveAllAdministratorParticipantsForTesting() [][]byte {
	return [][]byte{[]byte("removeAllAdministrators")}
}

//Get an expected value for testing
func getNewAdministratorExpected() []byte {
	var administrator Administrator
		administrator.ID = "100001"
	administrator.ObjectType = "Participant.AdministratorParticipant"
	administrator.Alias = "BRITECH"
	administrator.Description = "British Technology Pvt. Ltd."
	administratorJSON, err := json.Marshal(administrator)
	if err != nil {
		fmt.Println("Error converting a Administrator record to JSON")
		return nil
	}
	return []byte(administratorJSON)
}

//Get expected values of Administrators for testing
func getExpectedAdministrators() []byte {
	var administrators []Administrator
	var administrator Administrator
		administrator.ID = "100001"
	administrator.ObjectType = "Participant.AdministratorParticipant"
	administrator.Alias = "BRITECH"
	administrator.Description = "British Technology Pvt. Ltd."
	administrators = append(administrators, administrator)
		administrator.ID = "100002"
	administrator.ObjectType = "Participant.AdministratorParticipant"
	administrator.Alias = "DEMAGDELAG"
	administrator.Description = "Demag Delewal AG"
	administrator.Description = "Demag Delewal AG"
	administrators = append(administrators, administrator)
	administratorJSON, err := json.Marshal(administrators)
	if err != nil {
		fmt.Println("Error converting administratorancer records to JSON")
		return nil
	}
	return []byte(administratorJSON)
}

func getExpectedAdministratorIDIndex(funcName string) []byte {
	var administratorIDIndex AdministratorIDIndex
	switch funcName {
	case "addNewAdministrator":
		administratorIDIndex.IDs = append(administratorIDIndex.IDs, "100001")
		administratorIDIndexBytes, err := json.Marshal(administratorIDIndex)
		if err != nil {
			fmt.Println("Error converting AdministratorIDIndex to JSON")
			return nil
		}
		return administratorIDIndexBytes
	case "beforeRemoveAdministrator":
		administratorIDIndex.IDs = append(administratorIDIndex.IDs, "100001")
		administratorIDIndex.IDs = append(administratorIDIndex.IDs, "100002")
		administratorIDIndexBytes, err := json.Marshal(administratorIDIndex)
		if err != nil {
			fmt.Println("Error converting AdministratorIDIndex to JSON")
			return nil
		}
		return administratorIDIndexBytes
	case "afterRemoveAdministrator":
		administratorIDIndex.IDs = append(administratorIDIndex.IDs, "100001")
		administratorIDIndexBytes, err := json.Marshal(administratorIDIndex)
		if err != nil {
			fmt.Println("Error converting AdministratorIDIndex to JSON")
			return nil
		}
		return administratorIDIndexBytes
	default:
		administratorIDIndexBytes, err := json.Marshal(administratorIDIndex)
		if err != nil {
			fmt.Println("Error converting AdministratorIDIndex to JSON")
			return nil
		}
		return administratorIDIndexBytes
	}
}

//checkInit - helper to check the Initialization of chaincode: AdministratorParticipant
func checkInit(t *testing.T, stub *shim.MockStub, args [][]byte) {
	res := stub.MockInit("1", args)
	if res.Status != shim.OK {
		fmt.Println("Init failed", string(res.Message))
		t.FailNow()
	}
}

//checkState - helper for checking the chaincode state for a given stateKey afgainst an expected value
func checkState(t *testing.T, stub *shim.MockStub, stateKey string, expectedState []byte) {
	actualState := stub.State[stateKey]
	if actualState == nil {
		fmt.Println("State for ", stateKey, ": failed to get value")
		t.FailNow()
	}

	if bytes.Compare(actualState, expectedState) != 0 {
		fmt.Println("Incorrect State for " + stateKey + ": \nExpected: " + string(expectedState) + "\nActual  : " + string(actualState))
		t.FailNow()
	}
}

//checkInvoke - helper for checking Invoke of chaincode
func checkInvoke(t *testing.T, stub *shim.MockStub, args [][]byte) {
	res := stub.MockInvoke("1", args)
	if res.Status != shim.OK {
		fmt.Println("Invoke", args, "failed", string(res.Message))
		t.FailNow()
	}
}

func checkInvokeUnknownFunction(t *testing.T, stub *shim.MockStub, args [][]byte) {
	res := stub.MockInvoke("1", args)
	if res.Status != shim.OK {
		expectedErr := "Received unknown function invocation"
		actualErr := string(res.Message)
		if !(strings.Contains(actualErr, expectedErr)) {
			fmt.Println("Invoke with unknown function must throw error: ", expectedErr, "\n Actual Error :", actualErr)
			t.FailNow()
		}
	} else {
		t.FailNow()
	}
}

//checkReadAdministratorOK - helper for positive test readAdministrator
func checkReadAdministratorOK(t *testing.T, stub *shim.MockStub, administratorancerID string) {
	res := stub.MockInvoke("1", [][]byte{[]byte("readAdministrator"), []byte(administratorancerID)})
	if res.Status != shim.OK {
		fmt.Println("func readAdministrator with ID: ", administratorancerID, " failed"+string(res.Message))
		t.FailNow()
	}
	if res.Payload == nil {
		fmt.Println("func readAdministrator with ID: ", administratorancerID, "failed to get value")
		t.FailNow()
	}
	if bytes.Compare(getNewAdministratorExpected(), []byte(res.Payload)) != 0 {
		fmt.Println("func readAdministrator with ID: ", administratorancerID, "Expected:", string(getNewAdministratorExpected()), "Actual:", string(res.Payload))
		t.FailNow()
	}
}

//checkReadAdministratorNOK - helper for negative testing of readAdministrator
func checkReadAdministratorNOK(t *testing.T, stub *shim.MockStub, administratorancerID string) {
	//with no administratorancerID
	res := stub.MockInvoke("1", [][]byte{[]byte("readAdministrator"), []byte("")})
	if res.Status != shim.OK {
		expectedErr := "retrieveAdministrator: Corrupt administrator record "
		actualErr := string(res.Message)
		if !(strings.Contains(actualErr, expectedErr)) {
			fmt.Println("func readAdministrator neagtive test: ", "Expected Error:", expectedErr, "Actual Error", actualErr)
			t.FailNow()
		}
	} else {
		t.FailNow()
	}
}
func checkReadAllAdministratorsOK(t *testing.T, stub *shim.MockStub) {
	res := stub.MockInvoke("1", [][]byte{[]byte("readAllAdministrators")})
	if res.Status != shim.OK {
		fmt.Println("func readAllAdministrators failed", string(res.Message))
		t.FailNow()
	}
	if res.Payload == nil {
		fmt.Println("func readAllAdministrators failed to get value")
		t.FailNow()
	}
	if bytes.Compare(getExpectedAdministrators(), []byte(res.Payload)) != 0 {
		fmt.Println("func readAllAdministrators Expected:\n", string(getExpectedAdministrators()), "\nActual:\n", string(res.Payload))
		t.FailNow()
	}
}

func checkError(t *testing.T, exp string, act string) {
	if strings.Compare(exp, act) != 0 {
		fmt.Println("Unexpected Error! Expecting ", exp, "\n Actual :", act)
		t.FailNow()
	}
}
