package main

//Template Version: 1.1-20180704
import (
	"encoding/json"
	"errors"
	"fmt"

	"github.com/hyperledger/fabric/core/chaincode/shim"
	"github.com/hyperledger/fabric/protos/peer"
)

var logger = shim.NewLogger("CLDChaincode")

//CertificationRequestAsset - Chaincode for asset CertificationRequest
type CertificationRequestAsset struct {
}

//CertificationRequest - Details of the asset type CertificationRequest
type CertificationRequest struct {
 ID          string `json:"ID"`
 ObjectType   string `json:"docType"`
  Status       string `json:"status"`
  CreationDate string `json:"creationDate"`
CompanyID string `json:"companyID"`
CertificationAuthorityID string `json:"certificationAuthorityID"`
}

//CertificationRequestIDIndex - Index on IDs for retrieval all CertificationRequests
type CertificationRequestIDIndex struct {
	IDs []string `json:"IDs"`
}

func main() {
	err := shim.Start(new(CertificationRequestAsset))
	if err != nil {
		fmt.Printf("Error starting CertificationRequestAsset chaincode function main(): %s", err)
	} else {
		fmt.Printf("Starting CertificationRequestAsset chaincode function main() executed successfully")
	}
}

//Init - The chaincode Init function: No  arguments, only initializes a ID array as Index for retrieval of all CertificationRequests
func (cReq *CertificationRequestAsset) Init(stub shim.ChaincodeStubInterface) peer.Response {
	var certificationRequestIDIndex CertificationRequestIDIndex
	record, _ := stub.GetState("certificationRequestIDIndex")
	//chaincode upgrade: Index might already exist, create new only if it does not
	if record == nil {
		bytes, _ := json.Marshal(certificationRequestIDIndex)
		stub.PutState("certificationRequestIDIndex", bytes)
	}
	return shim.Success(nil)
}

//Invoke - The chaincode Invoke function:
func (cReq *CertificationRequestAsset) Invoke(stub shim.ChaincodeStubInterface) peer.Response {
	function, args := stub.GetFunctionAndParameters()
	logger.Debug("function: ", function)
	switch function {
	case "addNewCertificationRequest":
		return cReq.addNewCertificationRequest(stub, args)
	case "removeCertificationRequest":
		return cReq.removeCertificationRequest(stub, args[0])
	case "removeAllCertificationRequests":
		return cReq.removeAllCertificationRequests(stub)
	case "readCertificationRequest":
		return cReq.readCertificationRequest(stub, args[0])
	case "readAllCertificationRequests":
		return cReq.readAllCertificationRequests(stub)
	default:
		return shim.Error("Received unknown function invocation")
	}
}

//Invoke Route: addNewCertificationRequest
func (cReq *CertificationRequestAsset) addNewCertificationRequest(stub shim.ChaincodeStubInterface, args []string) peer.Response {
	certificationRequest, err := getCertificationRequestFromArgs(args)
	if err != nil {
		return shim.Error("CertificationRequest Data is Corrupted")
	}
	certificationRequest.ObjectType = "Asset.CertificationRequestAsset"
	record, err := stub.GetState(certificationRequest.ID)
	if record != nil {
		return shim.Error("This CertificationRequest already exists: " + certificationRequest.ID)
	}
	_, err = cReq.saveCertificationRequest(stub, certificationRequest)
	if err != nil {
		return shim.Error(err.Error())
	}
	_, err = cReq.updateCertificationRequestIDIndex(stub, certificationRequest)
	if err != nil {
		return shim.Error(err.Error())
	}
	return shim.Success(nil)
}

//Invoke Route: removeCertificationRequest
func (cReq *CertificationRequestAsset) removeCertificationRequest(stub shim.ChaincodeStubInterface, certificationRequestID string) peer.Response {
	_, err := cReq.deleteCertificationRequest(stub, certificationRequestID)
	if err != nil {
		return shim.Error(err.Error())
	}
	_, err = cReq.deleteCertificationRequestIDIndex(stub, certificationRequestID)
	if err != nil {
		return shim.Error(err.Error())
	}
	return shim.Success(nil)
}

//Invoke Route: removeAllCertificationRequests
func (cReq *CertificationRequestAsset) removeAllCertificationRequests(stub shim.ChaincodeStubInterface) peer.Response {
	var certificationRequestIDIndex CertificationRequestIDIndex
	bytes, err := stub.GetState("certificationRequestIDIndex")
	if err != nil {
		return shim.Error("removeAllCertificationRequests: Error getting certificationRequestIDIndex array")
	}
	err = json.Unmarshal(bytes, &certificationRequestIDIndex)
	if err != nil {
		return shim.Error("removeAllCertificationRequests: Error unmarshalling certificationRequestIDIndex array JSON")
	}
	if len(certificationRequestIDIndex.IDs) == 0 {
		return shim.Error("removeAllCertificationRequests: No certificationRequests to remove")
	}
	for _, certificationRequestStructID := range certificationRequestIDIndex.IDs {
		_, err = cReq.deleteCertificationRequest(stub, certificationRequestStructID)
		if err != nil {
			return shim.Error("Failed to remove CertificationRequest with ID: " + certificationRequestStructID)
		}
		_, err = cReq.deleteCertificationRequestIDIndex(stub, certificationRequestStructID)
		if err != nil {
			return shim.Error(err.Error())
		}
	}
	cReq.initHolder(stub)
	return shim.Success(nil)
}

//Query Route: readCertificationRequest
func (cReq *CertificationRequestAsset) readCertificationRequest(stub shim.ChaincodeStubInterface, certificationRequestID string) peer.Response {
	certificationRequestAsByteArray, err := cReq.retrieveCertificationRequest(stub, certificationRequestID)
	if err != nil {
		return shim.Error(err.Error())
	}
	return shim.Success(certificationRequestAsByteArray)
}

//Query Route: readAllCertificationRequests
func (cReq *CertificationRequestAsset) readAllCertificationRequests(stub shim.ChaincodeStubInterface) peer.Response {
	var certificationRequestIDs CertificationRequestIDIndex
	bytes, err := stub.GetState("certificationRequestIDIndex")
	if err != nil {
		return shim.Error("readAllCertificationRequests: Error getting certificationRequestIDIndex array")
	}
	err = json.Unmarshal(bytes, &certificationRequestIDs)
	if err != nil {
		return shim.Error("readAllCertificationRequests: Error unmarshalling certificationRequestIDIndex array JSON")
	}
	result := "["

	var certificationRequestAsByteArray []byte

	for _, certificationRequestID := range certificationRequestIDs.IDs {
		certificationRequestAsByteArray, err = cReq.retrieveCertificationRequest(stub, certificationRequestID)
		if err != nil {
			return shim.Error("Failed to retrieve certificationRequest with ID: " + certificationRequestID)
		}
		result += string(certificationRequestAsByteArray) + ","
	}
	if len(result) == 1 {
		result = "[]"
	} else {
		result = result[:len(result)-1] + "]"
	}
	return shim.Success([]byte(result))
}

//Helper: Save CertificationRequestAsset
func (cReq *CertificationRequestAsset) saveCertificationRequest(stub shim.ChaincodeStubInterface, certificationRequest CertificationRequest) (bool, error) {
	bytes, err := json.Marshal(certificationRequest)
	if err != nil {
		return false, errors.New("Error converting certificationRequest record JSON")
	}
	err = stub.PutState(certificationRequest.ID, bytes)
	if err != nil {
		return false, errors.New("Error storing CertificationRequest record")
	}
	return true, nil
}

//Helper: delete CertificationRequestAsset
func (cReq *CertificationRequestAsset) deleteCertificationRequest(stub shim.ChaincodeStubInterface, certificationRequestID string) (bool, error) {
	_, err := cReq.retrieveCertificationRequest(stub, certificationRequestID)
	if err != nil {
		return false, errors.New("CertificationRequest with ID: " + certificationRequestID + " not found")
	}
	err = stub.DelState(certificationRequestID)
	if err != nil {
		return false, errors.New("Error deleting CertificationRequest record")
	}
	return true, nil
}

//Helper: Update certificationRequest Holder - updates Index
func (cReq *CertificationRequestAsset) updateCertificationRequestIDIndex(stub shim.ChaincodeStubInterface, certificationRequest CertificationRequest) (bool, error) {
	var certificationRequestIDs CertificationRequestIDIndex
	bytes, err := stub.GetState("certificationRequestIDIndex")
	if err != nil {
		return false, errors.New("updateCertificationRequestIDIndex: Error getting certificationRequestIDIndex array Index from state")
	}
	err = json.Unmarshal(bytes, &certificationRequestIDs)
	if err != nil {
		return false, errors.New("updateCertificationRequestIDIndex: Error unmarshalling certificationRequestIDIndex array JSON")
	}
	certificationRequestIDs.IDs = append(certificationRequestIDs.IDs, certificationRequest.ID)
	bytes, err = json.Marshal(certificationRequestIDs)
	if err != nil {
		return false, errors.New("updateCertificationRequestIDIndex: Error marshalling new certificationRequest ID")
	}
	err = stub.PutState("certificationRequestIDIndex", bytes)
	if err != nil {
		return false, errors.New("updateCertificationRequestIDIndex: Error storing new certificationRequest ID in certificationRequestIDIndex (Index)")
	}
	return true, nil
}

//Helper: delete ID from certificationRequestStruct Holder
func (cReq *CertificationRequestAsset) deleteCertificationRequestIDIndex(stub shim.ChaincodeStubInterface, certificationRequestID string) (bool, error) {
	var certificationRequestIDIndex CertificationRequestIDIndex
	bytes, err := stub.GetState("certificationRequestIDIndex")
	if err != nil {
		return false, errors.New("deleteCertificationRequestIDIndex: Error getting certificationRequestIDIndex array Index from state")
	}
	err = json.Unmarshal(bytes, &certificationRequestIDIndex)
	if err != nil {
		return false, errors.New("deleteCertificationRequestIDIndex: Error unmarshalling certificationRequestIDIndex array JSON")
	}
	certificationRequestIDIndex.IDs, err = deleteKeyFromStringArray(certificationRequestIDIndex.IDs, certificationRequestID)
	if err != nil {
		return false, errors.New(err.Error())
	}
	bytes, err = json.Marshal(certificationRequestIDIndex)
	if err != nil {
		return false, errors.New("deleteCertificationRequestIDIndex: Error marshalling new certificationRequestStruct ID")
	}
	err = stub.PutState("certificationRequestIDIndex", bytes)
	if err != nil {
		return false, errors.New("deleteCertificationRequestIDIndex: Error storing new certificationRequestStruct ID in certificationRequestIDIndex (Index)")
	}
	return true, nil
}

//Helper: Initialize truck ID Holder //change template
func (cReq *CertificationRequestAsset) initHolder(stub shim.ChaincodeStubInterface) bool {
	var certificationRequestIDIndex CertificationRequestIDIndex
	bytes, _ := json.Marshal(certificationRequestIDIndex)
	stub.DelState("certificationRequestIDIndex")
	stub.PutState("certificationRequestIDIndex", bytes)
	return true
}

//deleteKeyFromArray
func deleteKeyFromStringArray(array []string, key string) (newArray []string, err error) {
	for _, entry := range array {
		if entry != key {
			newArray = append(newArray, entry)
		}
	}
	if len(newArray) == len(array) {
		return newArray, errors.New("Specified Key: " + key + " not found in Array")
	}
	return newArray, nil
}

//Helper: Retrieve purchaser
func (cReq *CertificationRequestAsset) retrieveCertificationRequest(stub shim.ChaincodeStubInterface, certificationRequestID string) ([]byte, error) {
	var certificationRequest CertificationRequest
	var certificationRequestAsByteArray []byte
	bytes, err := stub.GetState(certificationRequestID)
	if err != nil {
		return certificationRequestAsByteArray, errors.New("retrieveCertificationRequest: Error retrieving certificationRequest with ID: " + certificationRequestID)
	}
	err = json.Unmarshal(bytes, &certificationRequest)
	if err != nil {
		return certificationRequestAsByteArray, errors.New("retrieveCertificationRequest: Corrupt certificationRequest record " + string(bytes))
	}
	certificationRequestAsByteArray, err = json.Marshal(certificationRequest)
	if err != nil {
		return certificationRequestAsByteArray, errors.New("readCertificationRequest: Invalid certificationRequest Object - Not a  valid JSON")
	}
	return certificationRequestAsByteArray, nil
}

//getCertificationRequestFromArgs - construct a certificationRequest structure from string array of arguments
func getCertificationRequestFromArgs(args []string) (certificationRequest CertificationRequest, err error) {

	if !Valid(args[0]) {
		return certificationRequest, errors.New("Invalid json")
	}

	err = Unmarshal([]byte(args[0]), &certificationRequest)
	if err != nil {
		return certificationRequest, err
	}
	return certificationRequest, nil
}
