package main

//Template Version: 1.1-20180704
import (
	"bytes"
	"encoding/json"
	"fmt"
	"strings"
	"testing"

	"github.com/hyperledger/fabric/core/chaincode/shim"
)

//TestCertificationRequestAsset_Init
func TestCertificationRequestAsset_Init(t *testing.T) {
	certificationRequest := new(CertificationRequestAsset)
	stub := shim.NewMockStub("certificationRequest", certificationRequest)
	checkInit(t, stub, [][]byte{[]byte("init")})
	checkState(t, stub, "certificationRequestIDIndex", getExpectedCertificationRequestIDIndex("init"))
}

//TestCertificationRequestAsset_InvokeUnknownFunction
func TestCertificationRequestAsset_InvokeUnknownFunction(t *testing.T) {
	certificationRequest := new(CertificationRequestAsset)
	stub := shim.NewMockStub("certificationRequest", certificationRequest)
	checkInit(t, stub, [][]byte{[]byte("init")})
	checkInvokeUnknownFunction(t, stub, [][]byte{[]byte("myFunction"), []byte("docType:Asset")})
}

//TestCertificationRequestAsset_Invoke_addNewCertificationRequest
func TestCertificationRequestAsset_Invoke_addNewCertificationRequestOK(t *testing.T) {
	certificationRequest := new(CertificationRequestAsset)
	stub := shim.NewMockStub("certificationRequest", certificationRequest)
	checkInit(t, stub, [][]byte{[]byte("init")})
	checkInvoke(t, stub, getFirstCertificationRequestAssetForTesting())
	newCertificationRequestID := "100001"
	checkState(t, stub, newCertificationRequestID, getNewCertificationRequestExpected())
	checkState(t, stub, "certificationRequestIDIndex", getExpectedCertificationRequestIDIndex("addNewCertificationRequest"))
}

//TestCertificationRequestAsset_Invoke_addNewCertificationRequest
func TestCertificationRequestAsset_Invoke_addNewCertificationRequestDuplicate(t *testing.T) {
	certificationRequest := new(CertificationRequestAsset)
	stub := shim.NewMockStub("certificationRequest", certificationRequest)
	checkInit(t, stub, [][]byte{[]byte("init")})
	checkInvoke(t, stub, getFirstCertificationRequestAssetForTesting())
	newCertificationRequestID := "100001"
	checkState(t, stub, newCertificationRequestID, getNewCertificationRequestExpected())
	checkState(t, stub, "certificationRequestIDIndex", getExpectedCertificationRequestIDIndex("addNewCertificationRequest"))
	res := stub.MockInvoke("1", getFirstCertificationRequestAssetForTesting())
	if res.Status != shim.OK {
		checkError(t, "This CertificationRequest already exists: 100001", res.Message)
	} else {
		fmt.Println("Error was expected, but not raised")
		t.FailNow()
	}
}

//TestCertificationRequestAsset_Invoke_removeCertificationRequestOK  //change template
func TestCertificationRequestAsset_Invoke_removeCertificationRequestOK(t *testing.T) {
	certificationRequest := new(CertificationRequestAsset)
	stub := shim.NewMockStub("certificationRequest", certificationRequest)
	checkInit(t, stub, [][]byte{[]byte("init")})
	checkInvoke(t, stub, getFirstCertificationRequestAssetForTesting())
	checkInvoke(t, stub, getSecondCertificationRequestAssetForTesting())
	checkReadAllCertificationRequestsOK(t, stub)
	checkState(t, stub, "certificationRequestIDIndex", getExpectedCertificationRequestIDIndex("beforeRemoveCertificationRequest"))
	checkInvoke(t, stub, getRemoveSecondCertificationRequestAssetForTesting())
	remainingCertificationRequestID := "100001"
	checkReadCertificationRequestOK(t, stub, remainingCertificationRequestID)
	checkState(t, stub, "certificationRequestIDIndex", getExpectedCertificationRequestIDIndex("afterRemoveCertificationRequest"))
}

//TestCertificationRequestAsset_Invoke_removeCertificationRequestNOK  //change template
func TestCertificationRequestAsset_Invoke_removeCertificationRequestNOK(t *testing.T) {
	certificationRequest := new(CertificationRequestAsset)
	stub := shim.NewMockStub("certificationRequest", certificationRequest)
	checkInit(t, stub, [][]byte{[]byte("init")})
	checkInvoke(t, stub, getFirstCertificationRequestAssetForTesting())
	firstCertificationRequestID := "100001"
	checkReadCertificationRequestOK(t, stub, firstCertificationRequestID)
	checkState(t, stub, "certificationRequestIDIndex", getExpectedCertificationRequestIDIndex("addNewCertificationRequest"))
	res := stub.MockInvoke("1", getRemoveSecondCertificationRequestAssetForTesting())
	if res.Status != shim.OK {
		checkError(t, "CertificationRequest with ID: "+"100002"+" not found", res.Message)
	} else {
		fmt.Println("Error was expected, but not raised")
		t.FailNow()
	}
	checkState(t, stub, "certificationRequestIDIndex", getExpectedCertificationRequestIDIndex("addNewCertificationRequest"))
}

//TestCertificationRequestAsset_Invoke_removeAllCertificationRequestsOK  //change template
func TestCertificationRequestAsset_Invoke_removeAllCertificationRequestsOK(t *testing.T) {
	certificationRequest := new(CertificationRequestAsset)
	stub := shim.NewMockStub("certificationRequest", certificationRequest)
	checkInit(t, stub, [][]byte{[]byte("init")})
	checkInvoke(t, stub, getFirstCertificationRequestAssetForTesting())
	checkInvoke(t, stub, getSecondCertificationRequestAssetForTesting())
	checkReadAllCertificationRequestsOK(t, stub)
	checkState(t, stub, "certificationRequestIDIndex", getExpectedCertificationRequestIDIndex("beforeRemoveCertificationRequest"))
	checkInvoke(t, stub, getRemoveAllCertificationRequestAssetsForTesting())
	checkState(t, stub, "certificationRequestIDIndex", getExpectedCertificationRequestIDIndex(""))
}

//TestCertificationRequestAsset_Invoke_removeCertificationRequestNOK  //change template
func TestCertificationRequestAsset_Invoke_removeAllCertificationRequestsNOK(t *testing.T) {
	certificationRequest := new(CertificationRequestAsset)
	stub := shim.NewMockStub("certificationRequest", certificationRequest)
	checkInit(t, stub, [][]byte{[]byte("init")})
	res := stub.MockInvoke("1", getRemoveAllCertificationRequestAssetsForTesting())
	if res.Status != shim.OK {
		checkError(t, "removeAllCertificationRequests: No certificationRequests to remove", res.Message)
	} else {
		fmt.Println("Error was expected, but not raised")
		t.FailNow()
	}
	checkState(t, stub, "certificationRequestIDIndex", getExpectedCertificationRequestIDIndex(""))
}

//TestCertificationRequestAsset_Query_readCertificationRequest
func TestCertificationRequestAsset_Query_readCertificationRequest(t *testing.T) {
	certificationRequest := new(CertificationRequestAsset)
	stub := shim.NewMockStub("certificationRequest", certificationRequest)
	checkInit(t, stub, [][]byte{[]byte("init")})
	certificationRequestID := "100001"
	checkInvoke(t, stub, getFirstCertificationRequestAssetForTesting())
	checkReadCertificationRequestOK(t, stub, certificationRequestID)
	checkReadCertificationRequestNOK(t, stub, "")
}

//TestCertificationRequestAsset_Query_readAllCertificationRequests
func TestCertificationRequestAsset_Query_readAllCertificationRequests(t *testing.T) {
	certificationRequest := new(CertificationRequestAsset)
	stub := shim.NewMockStub("certificationRequest", certificationRequest)
	checkInit(t, stub, [][]byte{[]byte("init")})
	checkInvoke(t, stub, getFirstCertificationRequestAssetForTesting())
	checkInvoke(t, stub, getSecondCertificationRequestAssetForTesting())
	checkReadAllCertificationRequestsOK(t, stub)
}

/*
*
*	Helper Functions
*
 */
//Get first CertificationRequestAsset for testing
func getFirstCertificationRequestAssetForTesting() [][]byte { 
 return [][]byte{[]byte("addNewCertificationRequest"), 
 []byte("{\"ID\":\"100001\",\"docType\":\"Asset.CertificationRequestAsset\",\"status\":\"0\",\"creationDate\":\"12/01/2018\", \"companyID\":\"companyID001\", \"certificationAuthorityID\":\"certificationAuthorityID001\"}")}
}

//Get second CertificationRequestAsset for testing
func getSecondCertificationRequestAssetForTesting() [][]byte { 
 return [][]byte{[]byte("addNewCertificationRequest"), 
 []byte("{\"ID\":\"100002\",\"docType\":\"Asset.CertificationRequestAsset\",\"status\":\"0\",\"creationDate\":\"12/01/2018\", \"companyID\":\"companyID002\", \"certificationAuthorityID\":\"certificationAuthorityID002\"}")}
}

//Get remove second CertificationRequestAsset for testing //change template
func getRemoveSecondCertificationRequestAssetForTesting() [][]byte {
	return [][]byte{[]byte("removeCertificationRequest"),
		[]byte("100002")}
}

//Get remove all CertificationRequestAssets for testing //change template
func getRemoveAllCertificationRequestAssetsForTesting() [][]byte {
	return [][]byte{[]byte("removeAllCertificationRequests")}
}

//Get an expected value for testing
func getNewCertificationRequestExpected() []byte {
	var certificationRequest CertificationRequest
		certificationRequest.ID = "100001"
	certificationRequest.ObjectType = "Asset.CertificationRequestAsset"
  certificationRequest.Status = "0"
	certificationRequest.CreationDate = "12/01/2018"
certificationRequest.CompanyID="companyID001"
certificationRequest.CertificationAuthorityID="certificationAuthorityID001"
	certificationRequestJSON, err := json.Marshal(certificationRequest)
	if err != nil {
		fmt.Println("Error converting a CertificationRequest record to JSON")
		return nil
	}
	return []byte(certificationRequestJSON)
}

//Get expected values of CertificationRequests for testing
func getExpectedCertificationRequests() []byte {
	var certificationRequests []CertificationRequest
	var certificationRequest CertificationRequest
		certificationRequest.ID = "100001"
	certificationRequest.ObjectType = "Asset.CertificationRequestAsset"
  certificationRequest.Status = "0"
	certificationRequest.CreationDate = "12/01/2018"
certificationRequest.CompanyID="companyID001"
certificationRequest.CertificationAuthorityID="certificationAuthorityID001"
	certificationRequests = append(certificationRequests, certificationRequest)
		certificationRequest.ID = "100002"
	certificationRequest.ObjectType = "Asset.CertificationRequestAsset"
  certificationRequest.Status = "0"
	certificationRequest.CreationDate = "12/01/2018"
certificationRequest.CompanyID="companyID002"
certificationRequest.CertificationAuthorityID="certificationAuthorityID002"
	certificationRequests = append(certificationRequests, certificationRequest)
	certificationRequestJSON, err := json.Marshal(certificationRequests)
	if err != nil {
		fmt.Println("Error converting certificationRequest records to JSON")
		return nil
	}
	return []byte(certificationRequestJSON)
}

func getExpectedCertificationRequestIDIndex(funcName string) []byte {
	var certificationRequestIDIndex CertificationRequestIDIndex
	switch funcName {
	case "addNewCertificationRequest":
		certificationRequestIDIndex.IDs = append(certificationRequestIDIndex.IDs, "100001")
		certificationRequestIDIndexBytes, err := json.Marshal(certificationRequestIDIndex)
		if err != nil {
			fmt.Println("Error converting CertificationRequestIDIndex to JSON")
			return nil
		}
		return certificationRequestIDIndexBytes
	case "beforeRemoveCertificationRequest":
		certificationRequestIDIndex.IDs = append(certificationRequestIDIndex.IDs, "100001")
		certificationRequestIDIndex.IDs = append(certificationRequestIDIndex.IDs, "100002")
		certificationRequestIDIndexBytes, err := json.Marshal(certificationRequestIDIndex)
		if err != nil {
			fmt.Println("Error converting CertificationRequestIDIndex to JSON")
			return nil
		}
		return certificationRequestIDIndexBytes
	case "afterRemoveCertificationRequest":
		certificationRequestIDIndex.IDs = append(certificationRequestIDIndex.IDs, "100001")
		certificationRequestIDIndexBytes, err := json.Marshal(certificationRequestIDIndex)
		if err != nil {
			fmt.Println("Error converting CertificationRequestIDIndex to JSON")
			return nil
		}
		return certificationRequestIDIndexBytes
	default:
		certificationRequestIDIndexBytes, err := json.Marshal(certificationRequestIDIndex)
		if err != nil {
			fmt.Println("Error converting CertificationRequestIDIndex to JSON")
			return nil
		}
		return certificationRequestIDIndexBytes
	}
}

//checkInit - helper to check the Initialization of chaincode: CertificationRequestAsset
func checkInit(t *testing.T, stub *shim.MockStub, args [][]byte) {
	res := stub.MockInit("1", args)
	if res.Status != shim.OK {
		fmt.Println("Init failed", string(res.Message))
		t.FailNow()
	}
}

//checkState - helper for checking the chaincode state for a given stateKey afgainst an expected value
func checkState(t *testing.T, stub *shim.MockStub, stateKey string, expectedState []byte) {
	actualState := stub.State[stateKey]
	if actualState == nil {
		fmt.Println("State for ", stateKey, ": failed to get value")
		t.FailNow()
	}

	if bytes.Compare(actualState, expectedState) != 0 {
		fmt.Println("Incorrect State for " + stateKey + ": \nExpected: " + string(expectedState) + "\nActual  : " + string(actualState))
		t.FailNow()
	}
}

//checkInvoke - helper for checking Invoke of chaincode
func checkInvoke(t *testing.T, stub *shim.MockStub, args [][]byte) {
	res := stub.MockInvoke("1", args)
	if res.Status != shim.OK {
		fmt.Println("Invoke", args, "failed", string(res.Message))
		t.FailNow()
	}
}

func checkInvokeUnknownFunction(t *testing.T, stub *shim.MockStub, args [][]byte) {
	res := stub.MockInvoke("1", args)
	if res.Status != shim.OK {
		expectedErr := "Received unknown function invocation"
		actualErr := string(res.Message)
		if !(strings.Contains(actualErr, expectedErr)) {
			fmt.Println("Invoke with unknown function must throw error: ", expectedErr, "\n Actual Error :", actualErr)
			t.FailNow()
		}
	} else {
		t.FailNow()
	}
}

//checkReadCertificationRequestOK - helper for positive test readCertificationRequest
func checkReadCertificationRequestOK(t *testing.T, stub *shim.MockStub, certificationRequestID string) {
	res := stub.MockInvoke("1", [][]byte{[]byte("readCertificationRequest"), []byte(certificationRequestID)})
	if res.Status != shim.OK {
		fmt.Println("func readCertificationRequest with ID: ", certificationRequestID, " failed"+string(res.Message))
		t.FailNow()
	}
	if res.Payload == nil {
		fmt.Println("func readCertificationRequest with ID: ", certificationRequestID, "failed to get value")
		t.FailNow()
	}
	if bytes.Compare(getNewCertificationRequestExpected(), []byte(res.Payload)) != 0 {
		fmt.Println("func readCertificationRequest with ID: ", certificationRequestID, "Expected:", string(getNewCertificationRequestExpected()), "Actual:", string(res.Payload))
		t.FailNow()
	}
}

//checkReadCertificationRequestNOK - helper for negative testing of readCertificationRequest
func checkReadCertificationRequestNOK(t *testing.T, stub *shim.MockStub, certificationRequestID string) {
	//with no certificationRequestID
	res := stub.MockInvoke("1", [][]byte{[]byte("readCertificationRequest"), []byte("")})
	if res.Status != shim.OK {
		expectedErr := "retrieveCertificationRequest: Corrupt certificationRequest record "
		actualErr := string(res.Message)
		if !(strings.Contains(actualErr, expectedErr)) {
			fmt.Println("func readCertificationRequest negative test: ", "Expected Error:", expectedErr, "Actual Error", actualErr)
			t.FailNow()
		}
	} else {
		t.FailNow()
	}
}

func checkReadAllCertificationRequestsOK(t *testing.T, stub *shim.MockStub) {
	res := stub.MockInvoke("1", [][]byte{[]byte("readAllCertificationRequests")})
	if res.Status != shim.OK {
		fmt.Println("func readAllCertificationRequests failed", string(res.Message))
		t.FailNow()
	}
	if res.Payload == nil {
		fmt.Println("func readAllCertificationRequests failed to get value")
		t.FailNow()
	}
	if bytes.Compare(getExpectedCertificationRequests(), []byte(res.Payload)) != 0 {
		fmt.Println("func readAllCertificationRequests Expected:\n", string(getExpectedCertificationRequests()), "\nActual:\n", string(res.Payload))
		t.FailNow()
	}
}

func checkError(t *testing.T, exp string, act string) {
	if strings.Compare(exp, act) != 0 {
		fmt.Println("Unexpected Error! Expecting ", exp, "\n Actual :", act)
		t.FailNow()
	}
}
