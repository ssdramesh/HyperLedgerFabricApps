package main

//Template Version: 1.1-20180704
import (
	"encoding/json"
	"errors"
	"fmt"

	"github.com/hyperledger/fabric/core/chaincode/shim"
	"github.com/hyperledger/fabric/protos/peer"
)

var logger = shim.NewLogger("CLDChaincode")

//TouchAsset - Chaincode for asset Touch
type TouchAsset struct {
}

//Touch - Details of the asset type Touch
type Touch struct {
 ID          string `json:"ID"`
 ObjectType   string `json:"docType"`
  Status       string `json:"status"`
  CreationDate string `json:"creationDate"`
CompanyID string `json:"companyID"`
BatchID string `json:"batchID"`
Latitude string `json:"latitude"`
Longitude string `json:"longitude"`
Description string `json:"description"`
Temperature string `json:"temperature"`
}

//TouchIDIndex - Index on IDs for retrieval all Touchs
type TouchIDIndex struct {
	IDs []string `json:"IDs"`
}

func main() {
	err := shim.Start(new(TouchAsset))
	if err != nil {
		fmt.Printf("Error starting TouchAsset chaincode function main(): %s", err)
	} else {
		fmt.Printf("Starting TouchAsset chaincode function main() executed successfully")
	}
}

//Init - The chaincode Init function: No  arguments, only initializes a ID array as Index for retrieval of all Touchs
func (tch *TouchAsset) Init(stub shim.ChaincodeStubInterface) peer.Response {
	var touchIDIndex TouchIDIndex
	record, _ := stub.GetState("touchIDIndex")
	//chaincode upgrade: Index might already exist, create new only if it does not
	if record == nil {
		bytes, _ := json.Marshal(touchIDIndex)
		stub.PutState("touchIDIndex", bytes)
	}
	return shim.Success(nil)
}

//Invoke - The chaincode Invoke function:
func (tch *TouchAsset) Invoke(stub shim.ChaincodeStubInterface) peer.Response {
	function, args := stub.GetFunctionAndParameters()
	logger.Debug("function: ", function)
	switch function {
	case "addNewTouch":
		return tch.addNewTouch(stub, args)
	case "removeTouch":
		return tch.removeTouch(stub, args[0])
	case "removeAllTouchs":
		return tch.removeAllTouchs(stub)
	case "readTouch":
		return tch.readTouch(stub, args[0])
	case "readAllTouchs":
		return tch.readAllTouchs(stub)
	default:
		return shim.Error("Received unknown function invocation")
	}
}

//Invoke Route: addNewTouch
func (tch *TouchAsset) addNewTouch(stub shim.ChaincodeStubInterface, args []string) peer.Response {
	touch, err := getTouchFromArgs(args)
	if err != nil {
		return shim.Error("Touch Data is Corrupted")
	}
	touch.ObjectType = "Asset.TouchAsset"
	record, err := stub.GetState(touch.ID)
	if record != nil {
		return shim.Error("This Touch already exists: " + touch.ID)
	}
	_, err = tch.saveTouch(stub, touch)
	if err != nil {
		return shim.Error(err.Error())
	}
	_, err = tch.updateTouchIDIndex(stub, touch)
	if err != nil {
		return shim.Error(err.Error())
	}
	return shim.Success(nil)
}

//Invoke Route: removeTouch
func (tch *TouchAsset) removeTouch(stub shim.ChaincodeStubInterface, touchID string) peer.Response {
	_, err := tch.deleteTouch(stub, touchID)
	if err != nil {
		return shim.Error(err.Error())
	}
	_, err = tch.deleteTouchIDIndex(stub, touchID)
	if err != nil {
		return shim.Error(err.Error())
	}
	return shim.Success(nil)
}

//Invoke Route: removeAllTouchs
func (tch *TouchAsset) removeAllTouchs(stub shim.ChaincodeStubInterface) peer.Response {
	var touchIDIndex TouchIDIndex
	bytes, err := stub.GetState("touchIDIndex")
	if err != nil {
		return shim.Error("removeAllTouchs: Error getting touchIDIndex array")
	}
	err = json.Unmarshal(bytes, &touchIDIndex)
	if err != nil {
		return shim.Error("removeAllTouchs: Error unmarshalling touchIDIndex array JSON")
	}
	if len(touchIDIndex.IDs) == 0 {
		return shim.Error("removeAllTouchs: No touchs to remove")
	}
	for _, touchStructID := range touchIDIndex.IDs {
		_, err = tch.deleteTouch(stub, touchStructID)
		if err != nil {
			return shim.Error("Failed to remove Touch with ID: " + touchStructID)
		}
		_, err = tch.deleteTouchIDIndex(stub, touchStructID)
		if err != nil {
			return shim.Error(err.Error())
		}
	}
	tch.initHolder(stub)
	return shim.Success(nil)
}

//Query Route: readTouch
func (tch *TouchAsset) readTouch(stub shim.ChaincodeStubInterface, touchID string) peer.Response {
	touchAsByteArray, err := tch.retrieveTouch(stub, touchID)
	if err != nil {
		return shim.Error(err.Error())
	}
	return shim.Success(touchAsByteArray)
}

//Query Route: readAllTouchs
func (tch *TouchAsset) readAllTouchs(stub shim.ChaincodeStubInterface) peer.Response {
	var touchIDs TouchIDIndex
	bytes, err := stub.GetState("touchIDIndex")
	if err != nil {
		return shim.Error("readAllTouchs: Error getting touchIDIndex array")
	}
	err = json.Unmarshal(bytes, &touchIDs)
	if err != nil {
		return shim.Error("readAllTouchs: Error unmarshalling touchIDIndex array JSON")
	}
	result := "["

	var touchAsByteArray []byte

	for _, touchID := range touchIDs.IDs {
		touchAsByteArray, err = tch.retrieveTouch(stub, touchID)
		if err != nil {
			return shim.Error("Failed to retrieve touch with ID: " + touchID)
		}
		result += string(touchAsByteArray) + ","
	}
	if len(result) == 1 {
		result = "[]"
	} else {
		result = result[:len(result)-1] + "]"
	}
	return shim.Success([]byte(result))
}

//Helper: Save TouchAsset
func (tch *TouchAsset) saveTouch(stub shim.ChaincodeStubInterface, touch Touch) (bool, error) {
	bytes, err := json.Marshal(touch)
	if err != nil {
		return false, errors.New("Error converting touch record JSON")
	}
	err = stub.PutState(touch.ID, bytes)
	if err != nil {
		return false, errors.New("Error storing Touch record")
	}
	return true, nil
}

//Helper: delete TouchAsset
func (tch *TouchAsset) deleteTouch(stub shim.ChaincodeStubInterface, touchID string) (bool, error) {
	_, err := tch.retrieveTouch(stub, touchID)
	if err != nil {
		return false, errors.New("Touch with ID: " + touchID + " not found")
	}
	err = stub.DelState(touchID)
	if err != nil {
		return false, errors.New("Error deleting Touch record")
	}
	return true, nil
}

//Helper: Update touch Holder - updates Index
func (tch *TouchAsset) updateTouchIDIndex(stub shim.ChaincodeStubInterface, touch Touch) (bool, error) {
	var touchIDs TouchIDIndex
	bytes, err := stub.GetState("touchIDIndex")
	if err != nil {
		return false, errors.New("updateTouchIDIndex: Error getting touchIDIndex array Index from state")
	}
	err = json.Unmarshal(bytes, &touchIDs)
	if err != nil {
		return false, errors.New("updateTouchIDIndex: Error unmarshalling touchIDIndex array JSON")
	}
	touchIDs.IDs = append(touchIDs.IDs, touch.ID)
	bytes, err = json.Marshal(touchIDs)
	if err != nil {
		return false, errors.New("updateTouchIDIndex: Error marshalling new touch ID")
	}
	err = stub.PutState("touchIDIndex", bytes)
	if err != nil {
		return false, errors.New("updateTouchIDIndex: Error storing new touch ID in touchIDIndex (Index)")
	}
	return true, nil
}

//Helper: delete ID from touchStruct Holder
func (tch *TouchAsset) deleteTouchIDIndex(stub shim.ChaincodeStubInterface, touchID string) (bool, error) {
	var touchIDIndex TouchIDIndex
	bytes, err := stub.GetState("touchIDIndex")
	if err != nil {
		return false, errors.New("deleteTouchIDIndex: Error getting touchIDIndex array Index from state")
	}
	err = json.Unmarshal(bytes, &touchIDIndex)
	if err != nil {
		return false, errors.New("deleteTouchIDIndex: Error unmarshalling touchIDIndex array JSON")
	}
	touchIDIndex.IDs, err = deleteKeyFromStringArray(touchIDIndex.IDs, touchID)
	if err != nil {
		return false, errors.New(err.Error())
	}
	bytes, err = json.Marshal(touchIDIndex)
	if err != nil {
		return false, errors.New("deleteTouchIDIndex: Error marshalling new touchStruct ID")
	}
	err = stub.PutState("touchIDIndex", bytes)
	if err != nil {
		return false, errors.New("deleteTouchIDIndex: Error storing new touchStruct ID in touchIDIndex (Index)")
	}
	return true, nil
}

//Helper: Initialize truck ID Holder //change template
func (tch *TouchAsset) initHolder(stub shim.ChaincodeStubInterface) bool {
	var touchIDIndex TouchIDIndex
	bytes, _ := json.Marshal(touchIDIndex)
	stub.DelState("touchIDIndex")
	stub.PutState("touchIDIndex", bytes)
	return true
}

//deleteKeyFromArray
func deleteKeyFromStringArray(array []string, key string) (newArray []string, err error) {
	for _, entry := range array {
		if entry != key {
			newArray = append(newArray, entry)
		}
	}
	if len(newArray) == len(array) {
		return newArray, errors.New("Specified Key: " + key + " not found in Array")
	}
	return newArray, nil
}

//Helper: Retrieve purchaser
func (tch *TouchAsset) retrieveTouch(stub shim.ChaincodeStubInterface, touchID string) ([]byte, error) {
	var touch Touch
	var touchAsByteArray []byte
	bytes, err := stub.GetState(touchID)
	if err != nil {
		return touchAsByteArray, errors.New("retrieveTouch: Error retrieving touch with ID: " + touchID)
	}
	err = json.Unmarshal(bytes, &touch)
	if err != nil {
		return touchAsByteArray, errors.New("retrieveTouch: Corrupt touch record " + string(bytes))
	}
	touchAsByteArray, err = json.Marshal(touch)
	if err != nil {
		return touchAsByteArray, errors.New("readTouch: Invalid touch Object - Not a  valid JSON")
	}
	return touchAsByteArray, nil
}

//getTouchFromArgs - construct a touch structure from string array of arguments
func getTouchFromArgs(args []string) (touch Touch, err error) {

	if !Valid(args[0]) {
		return touch, errors.New("Invalid json")
	}

	err = Unmarshal([]byte(args[0]), &touch)
	if err != nil {
		return touch, err
	}
	return touch, nil
}
