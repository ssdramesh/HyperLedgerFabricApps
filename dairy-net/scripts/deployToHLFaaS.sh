#!/bin/bash

BIZNET_FOLDER_NAME=dairy-net
BIZNET_SRC_DIR=/Users/i047582/Documents/Workspaces/git/github.wdf.sap.corp/I047582/HyperLedgerFabricApps/dairy-net/chaincode
BIZNET_DEPLOY_DIR=/Users/i047582/Documents/Workspaces/git/github.wdf.sap.corp/I047582/HyperLedgerFabricApps/dairy-net/deployment
BIZNET_TEST_DIR=/Users/i047582/Documents/Workspaces/git/github.wdf.sap.corp/I047582/HyperLedgerFabricApps/dairy-net/test
BIZNET_UI_DIR=/Users/i047582/Documents/Workspaces/git/github.wdf.sap.corp/I047582/HyperLedgerFabricApps/dairy-net/ui

#Capture blockchain deployment environment variables
# Base URL
echo "Capturing your SAP Hyperledger as a Service blockchain environment variables..."
echo "Enter base URL: "
read BASE_URL
if [[ "${BASE_URL}" == "" ]]; then
# LeoCenter Canary: hyperledger-fabric.cfapps.sap.hana.ondemand.com
# IBS_POC_APJ       : hyperledger-fabric.cfapps.us10.hana.ondemand.com
# PDL US-East       : hyperledger-fabric.cfapps.us10.hana.ondemand.com
# PDL US-East Demo  : hyperledger-fabric.cfapps.us10.hana.ondemand.com
# PDL US-East Tuna  : hyperledger-fabric.cfapps.us10.hana.ondemand.com
# PDL US-East Dairy : hyperledger-fabric.cfapps.us10.hana.ondemand.com
  BASE_URL="hyperledger-fabric.cfapps.us10.hana.ondemand.com"
fi

#Get region from Base BASE_URL
IFS='.'
read -r -a array <<< "$BASE_URL"
REGION="${array[2]}"
IFS=''

# Identity Zone
echo "Enter Identity Zone: "
read IDENTITY_ZONE
if [[ "${IDENTITY_ZONE}" == "" ]]; then
# LeoCenter Canary  : hlf-demos
# IBS_POC_APJ       : hyundaicard
# PDL US-East       : pdl
# PDL US-East Demo  : pdl
# PDL US-East Tuna  : pdl
# PDL US-East Dairy : pdl
  IDENTITY_ZONE="pdl"
fi

# Client-Id
echo "Enter Client-ID: "
read CLIENT_ID
if [[ "${CLIENT_ID}" == "" ]]; then
# LeoCenter Canary  : sb-dbad26a4-e8ee-4150-9cac-3da2f1737c77!b1834|na-54bc25f3-f937-40b7-8b33-ffe240899cf0!b342
# IBS_POC_APJ       : sb-38b1cd20-6823-4d5b-bdaa-c26dedf7e0a8!b1001|na-3a01f1e2-bc33-4e12-86a2-ffffaea79918!b33
# PDL US-East       : sb-0ad13fac-6119-40a4-8030-53c51939c133!b1039|na-3a01f1e2-bc33-4e12-86a2-ffffaea79918!b33
# PDL US-East Demo  : sb-8edc5372-67ee-4496-b3d7-56aa68c2212f!b1039|na-3a01f1e2-bc33-4e12-86a2-ffffaea79918!b33
# PDL US-East Tuna  : sb-6f987f98-bf1d-45e6-8954-55fbc58438b7!b1039|na-3a01f1e2-bc33-4e12-86a2-ffffaea79918!b33
# PDL US-East Dairy : sb-09be9632-24f6-430d-8c42-e4c3171b8ca1!b1039|na-3a01f1e2-bc33-4e12-86a2-ffffaea79918!b33
  CLIENT_ID="sb-09be9632-24f6-430d-8c42-e4c3171b8ca1!b1039|na-3a01f1e2-bc33-4e12-86a2-ffffaea79918!b33"
fi

# Client-Secret
echo "Enter Client Secret: "
read CLIENT_SECRET
if [[ "${CLIENT_SECRET}" == "" ]]; then
# LeoCenter Canary  : w/1slM+k8a4UCy+Wol+ktpOA4Ek=
# IBS_POC_APJ       : RbpzHZqrBgF0xSY4+gVwZkLoTBY=
# PDL US-East       : o+bewyo8DLmvV95w9vW27fGGtC0=
# PDL US-East Demo  : SrEc+T8iQJIlj9QfQP/dpkAF5yo=
# PDL US-East Tuna  : nzbWoNEHmj0Ri2NMv/dW38lehk0=
# PDL US-East Dairy : mM1hNjiajUDcgN00/Oru+zQd6hY=
  CLIENT_SECRET="mM1hNjiajUDcgN00/Oru+zQd6hY="
fi

# HLF Channel service instance name
echo "Enter HLF channel service instance name:"
read HLF_CHANNEL_INSTANCE_NAME
if [[ "${HLF_CHANNEL_INSTANCE_NAME}" == "" ]]; then
# LeoCenter Canary  : sampleNet.smpl.dev-channel
# IBS_POC_APJ       : financingPlatform-devChannel
# PDL US-East       : dev.dev-channel
# PDL US-East Demo  : dev.demo-channel
# PDL US-East Tuna  : prod.tuna-channel
# PDL US-East Dairy : prod.dairy-channel
  HLF_CHANNEL_INSTANCE_NAME="prod.dairy-channel"
fi

# Deployment descriptor
echo "Enter the filename of your deployment descriptor: "
read DEPLOYMENT_DESCRIPTOR_FILENAME
if [[ "${DEPLOYMENT_DESCRIPTOR_FILENAME}" == "" ]]; then
  DEPLOYMENT_DESCRIPTOR_FILENAME="chaincodeDetails.json"
fi

# Deployment descriptor filename
echo "Updating of deployment descriptor..."
cd "${BIZNET_DEPLOY_DIR}"
sed -i.bak 's~<Enter Channel Service Instance Name Here>~'${HLF_CHANNEL_INSTANCE_NAME}'~g' "${DEPLOYMENT_DESCRIPTOR_FILENAME}"
sed -i.bak 's~<Enter your clientId here>~'${CLIENT_ID}'~g' "${DEPLOYMENT_DESCRIPTOR_FILENAME}"
sed -i.bak 's~<Enter your client secret here>~'${CLIENT_SECRET}'~g' "${DEPLOYMENT_DESCRIPTOR_FILENAME}"
sed -i.bak 's~<Enter your identity zone here>~'${IDENTITY_ZONE}'~g' "${DEPLOYMENT_DESCRIPTOR_FILENAME}"
sed -i.bak 's~<Enter your region here>~'${REGION}'~g' "${DEPLOYMENT_DESCRIPTOR_FILENAME}"
find . -name "*.bak" -type f -delete
echo "Update of deployment descriptor...Done!"
echo

# Update the postman / newman test files
echo "Updating Postman environment..."
cd "${BIZNET_TEST_DIR}/newman/CleanAllAndSetup"
sed -i.bak 's~<IDENTITY_ZONE>~'${IDENTITY_ZONE}'~g' "${BIZNET_TEST_DIR}/newman/CleanAllAndSetup/${BIZNET_FOLDER_NAME}.postman_environment.json"
sed -i.bak 's~<REGION>~'${REGION}'~g' "${BIZNET_TEST_DIR}/newman/CleanAllAndSetup/${BIZNET_FOLDER_NAME}.postman_environment.json"
sed -i.bak 's~<CLIENT_ID>~'${CLIENT_ID}'~g' "${BIZNET_TEST_DIR}/newman/CleanAllAndSetup/${BIZNET_FOLDER_NAME}.postman_environment.json"
sed -i.bak 's~<CLIENT_SECRET>~'${CLIENT_SECRET}'~g' "${BIZNET_TEST_DIR}/newman/CleanAllAndSetup/${BIZNET_FOLDER_NAME}.postman_environment.json"
find . -name "*.bak" -type f -delete
echo "Updating Postman environment...Done!"

#Prepare chaincode Deployment
TOKEN_URL="https://${IDENTITY_ZONE}.authentication.${REGION}.hana.ondemand.com/oauth/token?grant_type=client_credentials"
DEPLOY_URL="https://${BASE_URL}/api/v1/chaincodes"
AUTH_HEADER="$(printf '%s' "${CLIENT_ID}:${CLIENT_SECRET}" | base64)"
echo "Getting OAUTH Bearer token for deployment..."
echo
CURL_CMD_GET_TOKEN="curl -X GET \"${TOKEN_URL}\" -H 'Authorization: Basic ${AUTH_HEADER}' -H 'Accept: application/json'"
ACCESS_TOKEN=`eval $CURL_CMD_GET_TOKEN | jq '.access_token'`
ACCESS_TOKEN="${ACCESS_TOKEN%\"}"
ACCESS_TOKEN="${ACCESS_TOKEN#\"}"
echo
echo "Getting OAUTH Bearer token successful...Proceeding to deploy..."

#Individual chaincode deployment
cd "${BIZNET_SRC_DIR}"
for CHAINCODE_SRC_FOLDER in *; do
    if [[ -d ${CHAINCODE_SRC_FOLDER} ]]; then
      cd "${CHAINCODE_SRC_FOLDER}"
      ENTITY_NAME="$(basename "${CHAINCODE_SRC_FOLDER}")"
      ENTITY_NAME_LOWER=$(echo ${ENTITY_NAME:0:1} | tr "[A-Z]" "[a-z]")${ENTITY_NAME:1}
      echo "Following chaincode .zip files are found:"
      find . -name "*.zip"
      echo "Enter chiancode .zip filename from above to deploy: "
      read CHAINCODE_FILENAME
      echo "Enter Chaincode description for deployment: "
      read CHAINCODE_DESCRIPTION
      if [[ "${CHAINCODE_DESCRIPTION}" == "" ]]; then
        CHAINCODE_DESCRIPTION="${CHAINCODE_FILENAME}"
      fi
      # Deploy chaincode
      echo
      CURL_CMD_DEPLOY_CHAINCODE="curl -X POST \"${DEPLOY_URL}\" \
      -H 'Authorization: Bearer ${ACCESS_TOKEN}' \
      -H 'Accept: application/json' \
      -H 'content-type: multipart/form-data' \
      -F 'file=@${PWD}/${CHAINCODE_FILENAME}' \
      -F 'arguments=[]' \
      -F 'description=${CHAINCODE_DESCRIPTION}'"
      ENTITY_CHAINCODEID=`eval $CURL_CMD_DEPLOY_CHAINCODE | jq '.id'`
      ENTITY_CHAINCODEID="${ENTITY_CHAINCODEID%\"}"
      ENTITY_CHAINCODEID="${ENTITY_CHAINCODEID#\"}"
      #Update deployment descriptor file with entity chaincode ID
      cd "${BIZNET_DEPLOY_DIR}"
      sed -i.bak 's~<Enter the deployed chaincodeId for '${ENTITY_NAME_LOWER}' here>~'${ENTITY_CHAINCODEID}'~g' "${BIZNET_DEPLOY_DIR}/${DEPLOYMENT_DESCRIPTOR_FILENAME}"
      #Delete the backup file
      find . -name "*.bak" -type f -delete
      cd "${BIZNET_TEST_DIR}"
      sed -i.bak 's~<CHAINCODE_ID_'${ENTITY_NAME}'>~'${ENTITY_CHAINCODEID}'~g' "${BIZNET_TEST_DIR}/newman/CleanAllAndSetup/${BIZNET_FOLDER_NAME}.postman_environment.json"
      sed -i.bak 's~<DEPLOY_URL>~'${DEPLOY_URL}'~g' "${BIZNET_TEST_DIR}/newman/CleanAllAndSetup/${BIZNET_FOLDER_NAME}.postman_environment.json"
      #Delete the backup file
      find . -name "*.bak" -type f -delete
      cd "${BIZNET_SRC_DIR}"
    fi
done

#Update the relevant UI folders / files
cp -fi "${BIZNET_DEPLOY_DIR}/${DEPLOYMENT_DESCRIPTOR_FILENAME}" "${BIZNET_UI_DIR}/${BIZNET_FOLDER_NAME}Setup/webapp/model/chaincodeDetails.json"
cd "${BIZNET_UI_DIR}/${BIZNET_FOLDER_NAME}Setup"
sed -i.bak 's/<hlf-channel-instance-name>/'${HLF_CHANNEL_INSTANCE_NAME}'/g' manifest.yaml
find . -name "*.bak" -type f -delete
