This Demo Scenario for the customer Fonterra.  The sample data and details are for demo purposes only and no claim is made towards the validity and authenticity of the data used.  The sample data is only to showcase the concept of how blockchain technology can be applied for provenance use case.

# Use case description
Blockchain technology has the potential to improve transparency and accountability across the supply chain. Applications are already being used to track and trace materials back to the source, prove authenticity and origin, get ahead of recalls, and accelerate the flow of goods.

This demo illustrates how blockchain can be applied to showcase the dairy supply chain, to track and trace the journey of milk from farm to consumer’s hand.

# Data Model
![Data Model](./model/DairyNetWorkStructure.jpg)

# Demo Flow
Access:
[Base URL](https://dairynettrace-g85556318.dispatcher.jp1.hana.ondemand.com/webapp/index.html?hc_reset)

Enter: BVKF51ZZZ2 and search or access below [URL](https://dairynettrace-g85556318.dispatcher.jp1.hana.ondemand.com/webapp/index.html?hc_reset#//batch/BVKF51ZZZ2) --> leads to all certifications positive


Enter: 8MFB2ESV1Q and search or access below [URL](https://dairynettrace-g85556318.dispatcher.jp1.hana.ondemand.com/webapp/index.html?hc_reset#//batch/8MFB2ESV1Q) leads to some certifications negative & pending

# Maintain Sample Data
In case, you want to maintain your own sample data, for example country-specific locations, company names and certification agencies etc.  Please use this [URL](https://dairy-netsetup.cfapps.us10.hana.ondemand.com/ui5/)

# Contact
Ramesh Suraparaju (I047582)
