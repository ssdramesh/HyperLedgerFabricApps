sap.ui.define([
	"sap/ui/model/json/JSONModel"
], function(JSONModel) {
	"use strict";

	return {

		createBatchAssetsModel: function(){

			var oModel = new JSONModel();
			oModel.setDefaultBindingMode("TwoWay");
			oModel.setData({
				chaincode:{
					serviceUrl:"",
					serviceKey:{},
					batchAsset:{}
				},
				batchAssetCollection	: {
					cols:[
						{name:"ID"},
						{name:"ObjectType"},
						{name:"Status"},
						{name:"CreationDate"},
            {name:"Volume"},
            {name:"FatPercent"},
            {name:"SNF"},
            {name:"ProductID"},
            {name:"ProductDescription"}
					],
					items:[]
				},
				selectedBatchAsset:{},
				newBatchAsset:{
					ID:"",
					ObjectType:"",
					Status:"",
					CreationDate:"",
          Volume:"",
          FatPercent:"",
          SNF:"",
          ProductID:"",
          ProductDescription:""
				},
				selectedBatchAssetID	: "",
				searchBatchAssetID : ""
			});
			return oModel;
		}
	};
});
