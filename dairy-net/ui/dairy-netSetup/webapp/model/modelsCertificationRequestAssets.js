sap.ui.define([
	"sap/ui/model/json/JSONModel"
], function(JSONModel) {
	"use strict";

	return {

		createCertificationRequestAssetsModel: function(){

			var oModel = new JSONModel();
			oModel.setDefaultBindingMode("TwoWay");
			oModel.setData({
				chaincode:{
					serviceUrl:"",
					serviceKey:{},
					certificationRequestAsset:{}
				},
				certificationRequestAssetCollection	: {
					cols:[
						{name:"ID"},
						{name:"ObjectType"},
						{name:"Status"},
						{name:"CreationDate"},
            {name:"CompanyID"},
            {name:"CertificationAuthorityID"}
					],
					items:[]
				},
				selectedCertificationRequestAsset:{},
				newCertificationRequestAsset:{
					ID:"",
					ObjectType:"",
					Status:"",
					CreationDate:"",
          CompanyID:"",
          CertificationAuthorityID:""
				},
				selectedCertificationRequestAssetID	: "",
				searchCertificationRequestAssetID : ""
			});
			return oModel;
		}
	};
});
