sap.ui.define(function() {
	"use strict";

	jQuery.sap.require("jquery.sap.storage");
	var oStorage = jQuery.sap.storage(jQuery.sap.storage.Type.local);
	var certificationRequestAssetsDataID = "certificationRequestAssets";

	return {

		init: function(){

			oStorage.put(certificationRequestAssetsDataID,[]);
			oStorage.put(
				certificationRequestAssetsDataID,
[
	{
		"ID":"CertificationRequestAsset1001",
		"docType":"Asset.CertificationRequestAsset",
		"status":"Created",
		"creationDate":"12/28/2107",
		"companyID":"companyID001",
		"certificationAuthorityID":"certificationAuthorityID001"
	}
]				
			);
		},

		getCertificationRequestAssetDataID : function(){

			return certificationRequestAssetsDataID;
		},

		getCertificationRequestAssetData  : function(){

			return oStorage.get("certificationRequestAssets");
		},

		put: function(newCertificationRequestAsset){

			var certificationRequestAssetData = this.getCertificationRequestAssetData();
			certificationRequestAssetData.push(newCertificationRequestAsset);
			oStorage.put(certificationRequestAssetsDataID, certificationRequestAssetData);
		},

		remove : function (id){

			var certificationRequestAssetData = this.getCertificationRequestAssetData();
			certificationRequestAssetData = _.without(certificationRequestAssetData,_.findWhere(certificationRequestAssetData,{ID:id}));
			oStorage.put(certificationRequestAssetsDataID, certificationRequestAssetData);
		},

		removeAll : function(){

			oStorage.put(certificationRequestAssetsDataID,[]);
		},

		clearCertificationRequestAssetData: function(){

			oStorage.put(certificationRequestAssetsDataID,[]);
		}
	};
});
