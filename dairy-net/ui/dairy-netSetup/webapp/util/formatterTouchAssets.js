sap.ui.define(function() {
	"use strict";

	return {

		mapTouchAssetToModel:function(responseData){
			return {
				ID:responseData.ID,
				ObjectType:responseData.docType,
				
                Status:responseData.status,
                CreationDate:responseData.creationDate
,
                CompanyID:responseData.companyID,
                BatchID:responseData.batchID,
                Latitude:responseData.latitude,
                Longitude:responseData.longitude,
                Description:responseData.description,
                Temperature:responseData.temperature
			};
		},

		mapTouchAssetsToModel:function(responseData){

			var items = [];
			if( responseData ){
				for ( var i = 0; i < responseData.length; i++ ){
					items.push(this.mapTouchAssetToModel(responseData[i]));
				}
			}
			return items;
		},

		mapTouchAssetToChaincode:function(oModel, newTouchAsset){

			if ( newTouchAsset === true ) {
				return {
						"ID":this.getNewTouchAssetID(oModel),
						"docType":"Asset.TouchAsset",
						
                        "status":oModel.getProperty("/newTouchAsset/Status"),
                        "creationDate":oModel.getProperty("/newTouchAsset/CreationDate")
,
                  "companyID":oModel.getProperty("/newTouchAsset/CompanyID"),
                  "batchID":oModel.getProperty("/newTouchAsset/BatchID"),
                  "latitude":oModel.getProperty("/newTouchAsset/Latitude"),
                  "longitude":oModel.getProperty("/newTouchAsset/Longitude"),
                  "description":oModel.getProperty("/newTouchAsset/Description"),
                  "temperature":oModel.getProperty("/newTouchAsset/Temperature")
				};
			} else {
				return {
						"ID":oModel.getProperty("/selectedTouchAsset/ID"),
						"docType":"Asset.TouchAsset",
						
                        "status":oModel.getProperty("/selectedTouchAsset/Status"),
                        "creationDate":oModel.getProperty("/selectedTouchAsset/CreationDate")
,
                  "companyID":oModel.getProperty("/selectedTouchAsset/CompanyID"),
                  "batchID":oModel.getProperty("/selectedTouchAsset/BatchID"),
                  "latitude":oModel.getProperty("/selectedTouchAsset/Latitude"),
                  "longitude":oModel.getProperty("/selectedTouchAsset/Longitude"),
                  "description":oModel.getProperty("/selectedTouchAsset/Description"),
                  "temperature":oModel.getProperty("/selectedTouchAsset/Temperature")
				};
			}
		},

		mapTouchAssetToLocalStorage : function(oModel, newTouchAsset){

			return this.mapTouchAssetToChaincode(oModel, newTouchAsset);
		},

		getNewTouchAssetID:function(oModel){

		    if ( typeof oModel.getProperty("/newTouchAsset/ID") === "undefined" ||
		    		oModel.getProperty("/newTouchAsset/ID") === ""
		    	){
			    var iD = "TouchAsset";
			    var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
			    for( var i = 0; i < 8; i++ ){
			        iD += possible.charAt(Math.floor(Math.random() * possible.length));
			    }
		    } else {
				iD = oModel.getProperty("/newTouchAsset/ID");
			}
			oModel.setProperty("/newTouchAsset/ID",iD);
		    return iD;
		}
	};
});
