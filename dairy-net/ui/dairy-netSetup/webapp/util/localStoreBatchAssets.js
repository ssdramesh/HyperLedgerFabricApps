sap.ui.define(function() {
	"use strict";

	jQuery.sap.require("jquery.sap.storage");
	var oStorage = jQuery.sap.storage(jQuery.sap.storage.Type.local);
	var batchAssetsDataID = "batchAssets";

	return {

		init: function(){

			oStorage.put(batchAssetsDataID,[]);
			oStorage.put(
				batchAssetsDataID,
[
	{
		"ID":"BatchAsset1001",
		"docType":"Asset.BatchAsset",
		"status":"Created",
		"creationDate":"12/28/2107",
		"volume":"volume001",
		"fatPercent":"fatPercent001",
		"SNF":"SNF001",
		"productID":"productID001",
		"productDescription":"productDescription001"
	}
]				
			);
		},

		getBatchAssetDataID : function(){

			return batchAssetsDataID;
		},

		getBatchAssetData  : function(){

			return oStorage.get("batchAssets");
		},

		put: function(newBatchAsset){

			var batchAssetData = this.getBatchAssetData();
			batchAssetData.push(newBatchAsset);
			oStorage.put(batchAssetsDataID, batchAssetData);
		},

		remove : function (id){

			var batchAssetData = this.getBatchAssetData();
			batchAssetData = _.without(batchAssetData,_.findWhere(batchAssetData,{ID:id}));
			oStorage.put(batchAssetsDataID, batchAssetData);
		},

		removeAll : function(){

			oStorage.put(batchAssetsDataID,[]);
		},

		clearBatchAssetData: function(){

			oStorage.put(batchAssetsDataID,[]);
		}
	};
});
