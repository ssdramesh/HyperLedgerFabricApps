sap.ui.define([
	"dairy-netSetup/util/restBuilder",
	"dairy-netSetup/util/formatterCertificateAssets",
	"dairy-netSetup/util/localStoreCertificateAssets"
], function(
		restBuilder,
		formatterCertificateAssets,
		localStoreCertificateAssets
	) {
	"use strict";

	return {

		loadAllCertificateAssets:function(oComponent, oModel) {

			if ( !( oComponent.getModel("TestSwitch").getProperty("/testMode") ) ){
				restBuilder.execute(
					oModel.getProperty("/chaincode"),
					"readAll",
					[],
					function(responseData){
						oModel.setProperty(
							"/certificateAssetCollection/items",
							formatterCertificateAssets.mapCertificateAssetsToModel(responseData)
						);
					});
			} else {
				oModel.setProperty(
					"/certificateAssetCollection/items",
					formatterCertificateAssets.mapCertificateAssetsToModel(localStoreCertificateAssets.getCertificateAssetData())
				);
			}
		},

		loadCertificateAsset:function(oModel, selectedCertificateAssetID){

			oModel.setProperty(
				"/selectedCertificateAsset",
				_.findWhere(oModel.getProperty("/certificateAssetCollection/items"),
					{
						ID: selectedCertificateAssetID
					},
				this));
		},

		addNewCertificateAsset:function(oComponent, oModel){

			if ( !( oComponent.getModel("TestSwitch").getProperty("/testMode") ) ){
				restBuilder.execute(
					oModel.getProperty("/chaincode"),
					"addNew",
					formatterCertificateAssets.mapCertificateAssetToChaincode(oModel, true)
				);
			}  else {
				localStoreCertificateAssets.put(formatterCertificateAssets.mapCertificateAssetToLocalStorage(oModel, true));
			}
			this.loadAllCertificateAssets(oComponent, oModel);
			return oModel.getProperty("/newCertificateAsset/ID");
		},

		removeCertificateAsset : function(oComponent, oModel, certificateAssetID){

			if ( !( oComponent.getModel("TestSwitch").getProperty("/testMode") ) ) {
				restBuilder.execute(
					oModel.getProperty("/chaincode"),
					"remove",
					{ID:certificateAssetID}
				);
			} else {
				localStoreCertificateAssets.remove(certificateAssetID);
			}
			this.loadAllCertificateAssets(oComponent, oModel);
			return true;
		},

		removeAllCertificateAssets : function(oComponent, oModel){

			if (!(oComponent.getModel("TestSwitch").getProperty("/testMode"))) {
				restBuilder.execute(
					oModel.getProperty("/chaincode"),
					"removeAll",
					[]
				);
			} else {
				localStoreCertificateAssets.removeAll();
			}
			this.loadAllCertificateAssets(oComponent, oModel);
			oModel.setProperty("/selectedCertificateAsset",{});
			return true;
		}
	};
});
