sap.ui.define([
	"dairy-netSetup/util/restBuilder",
	"dairy-netSetup/util/formatterCertificationRequestAssets",
	"dairy-netSetup/util/localStoreCertificationRequestAssets"
], function(
		restBuilder,
		formatterCertificationRequestAssets,
		localStoreCertificationRequestAssets
	) {
	"use strict";

	return {

		loadAllCertificationRequestAssets:function(oComponent, oModel) {

			if ( !( oComponent.getModel("TestSwitch").getProperty("/testMode") ) ){
				restBuilder.execute(
					oModel.getProperty("/chaincode"),
					"readAll",
					[],
					function(responseData){
						oModel.setProperty(
							"/certificationRequestAssetCollection/items",
							formatterCertificationRequestAssets.mapCertificationRequestAssetsToModel(responseData)
						);
					});
			} else {
				oModel.setProperty(
					"/certificationRequestAssetCollection/items",
					formatterCertificationRequestAssets.mapCertificationRequestAssetsToModel(localStoreCertificationRequestAssets.getCertificationRequestAssetData())
				);
			}
		},

		loadCertificationRequestAsset:function(oModel, selectedCertificationRequestAssetID){

			oModel.setProperty(
				"/selectedCertificationRequestAsset",
				_.findWhere(oModel.getProperty("/certificationRequestAssetCollection/items"),
					{
						ID: selectedCertificationRequestAssetID
					},
				this));
		},

		addNewCertificationRequestAsset:function(oComponent, oModel){

			if ( !( oComponent.getModel("TestSwitch").getProperty("/testMode") ) ){
				restBuilder.execute(
					oModel.getProperty("/chaincode"),
					"addNew",
					formatterCertificationRequestAssets.mapCertificationRequestAssetToChaincode(oModel, true)
				);
			}  else {
				localStoreCertificationRequestAssets.put(formatterCertificationRequestAssets.mapCertificationRequestAssetToLocalStorage(oModel, true));
			}
			this.loadAllCertificationRequestAssets(oComponent, oModel);
			return oModel.getProperty("/newCertificationRequestAsset/ID");
		},

		removeCertificationRequestAsset : function(oComponent, oModel, certificationRequestAssetID){

			if ( !( oComponent.getModel("TestSwitch").getProperty("/testMode") ) ) {
				restBuilder.execute(
					oModel.getProperty("/chaincode"),
					"remove",
					{ID:certificationRequestAssetID}
				);
			} else {
				localStoreCertificationRequestAssets.remove(certificationRequestAssetID);
			}
			this.loadAllCertificationRequestAssets(oComponent, oModel);
			return true;
		},

		removeAllCertificationRequestAssets : function(oComponent, oModel){

			if (!(oComponent.getModel("TestSwitch").getProperty("/testMode"))) {
				restBuilder.execute(
					oModel.getProperty("/chaincode"),
					"removeAll",
					[]
				);
			} else {
				localStoreCertificationRequestAssets.removeAll();
			}
			this.loadAllCertificationRequestAssets(oComponent, oModel);
			oModel.setProperty("/selectedCertificationRequestAsset",{});
			return true;
		}
	};
});
