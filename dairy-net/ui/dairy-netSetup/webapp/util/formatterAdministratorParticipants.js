sap.ui.define(function() {
	"use strict";

	return {

		mapAdministratorParticipantToModel:function(responseData){
			return {
				ID:responseData.ID,
				ObjectType:responseData.docType,
				
                Alias:responseData.alias,
                Description:responseData.description

			};
		},

		mapAdministratorParticipantsToModel:function(responseData){

			var items = [];
			if( responseData ){
				for ( var i = 0; i < responseData.length; i++ ){
					items.push(this.mapAdministratorParticipantToModel(responseData[i]));
				}
			}
			return items;
		},

		mapAdministratorParticipantToChaincode:function(oModel, newAdministratorParticipant){

			if ( newAdministratorParticipant === true ) {
				return {
						"ID":this.getNewAdministratorParticipantID(oModel),
						"docType":"Participant.AdministratorParticipant",
						
                        "alias":oModel.getProperty("/newAdministratorParticipant/Alias"),
                        "description":oModel.getProperty("/newAdministratorParticipant/Description")

				};
			} else {
				return {
						"ID":oModel.getProperty("/selectedAdministratorParticipant/ID"),
						"docType":"Participant.AdministratorParticipant",
						
                        "alias":oModel.getProperty("/selectedAdministratorParticipant/Alias"),
                        "description":oModel.getProperty("/selectedAdministratorParticipant/Description")

				};
			}
		},

		mapAdministratorParticipantToLocalStorage : function(oModel, newAdministratorParticipant){

			return this.mapAdministratorParticipantToChaincode(oModel, newAdministratorParticipant);
		},

		getNewAdministratorParticipantID:function(oModel){

		    if ( typeof oModel.getProperty("/newAdministratorParticipant/ID") === "undefined" ||
		    		oModel.getProperty("/newAdministratorParticipant/ID") === ""
		    	){
			    var iD = "AdministratorParticipant";
			    var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
			    for( var i = 0; i < 8; i++ ){
			        iD += possible.charAt(Math.floor(Math.random() * possible.length));
			    }
		    } else {
				iD = oModel.getProperty("/newAdministratorParticipant/ID");
			}
			oModel.setProperty("/newAdministratorParticipant/ID",iD);
		    return iD;
		}
	};
});
