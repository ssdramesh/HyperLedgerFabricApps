sap.ui.define(function() {
	"use strict";

	return {

		mapCompanyParticipantToModel:function(responseData){
			return {
				ID:responseData.ID,
				ObjectType:responseData.docType,
				
                Alias:responseData.alias,
                Description:responseData.description
,
                RegistrationID:responseData.registrationID,
                CertificationID:responseData.certificationID,
                Type:responseData.type,
                Name:responseData.name
			};
		},

		mapCompanyParticipantsToModel:function(responseData){

			var items = [];
			if( responseData ){
				for ( var i = 0; i < responseData.length; i++ ){
					items.push(this.mapCompanyParticipantToModel(responseData[i]));
				}
			}
			return items;
		},

		mapCompanyParticipantToChaincode:function(oModel, newCompanyParticipant){

			if ( newCompanyParticipant === true ) {
				return {
						"ID":this.getNewCompanyParticipantID(oModel),
						"docType":"Participant.CompanyParticipant",
						
                        "alias":oModel.getProperty("/newCompanyParticipant/Alias"),
                        "description":oModel.getProperty("/newCompanyParticipant/Description")
,
                  "registrationID":oModel.getProperty("/newCompanyParticipant/RegistrationID"),
                  "certificationID":oModel.getProperty("/newCompanyParticipant/CertificationID"),
                  "type":oModel.getProperty("/newCompanyParticipant/Type"),
                  "name":oModel.getProperty("/newCompanyParticipant/Name")
				};
			} else {
				return {
						"ID":oModel.getProperty("/selectedCompanyParticipant/ID"),
						"docType":"Participant.CompanyParticipant",
						
                        "alias":oModel.getProperty("/selectedCompanyParticipant/Alias"),
                        "description":oModel.getProperty("/selectedCompanyParticipant/Description")
,
                  "registrationID":oModel.getProperty("/selectedCompanyParticipant/RegistrationID"),
                  "certificationID":oModel.getProperty("/selectedCompanyParticipant/CertificationID"),
                  "type":oModel.getProperty("/selectedCompanyParticipant/Type"),
                  "name":oModel.getProperty("/selectedCompanyParticipant/Name")
				};
			}
		},

		mapCompanyParticipantToLocalStorage : function(oModel, newCompanyParticipant){

			return this.mapCompanyParticipantToChaincode(oModel, newCompanyParticipant);
		},

		getNewCompanyParticipantID:function(oModel){

		    if ( typeof oModel.getProperty("/newCompanyParticipant/ID") === "undefined" ||
		    		oModel.getProperty("/newCompanyParticipant/ID") === ""
		    	){
			    var iD = "CompanyParticipant";
			    var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
			    for( var i = 0; i < 8; i++ ){
			        iD += possible.charAt(Math.floor(Math.random() * possible.length));
			    }
		    } else {
				iD = oModel.getProperty("/newCompanyParticipant/ID");
			}
			oModel.setProperty("/newCompanyParticipant/ID",iD);
		    return iD;
		}
	};
});
