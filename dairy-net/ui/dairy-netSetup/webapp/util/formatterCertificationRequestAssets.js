sap.ui.define(function() {
	"use strict";

	return {

		mapCertificationRequestAssetToModel:function(responseData){
			return {
				ID:responseData.ID,
				ObjectType:responseData.docType,
				
                Status:responseData.status,
                CreationDate:responseData.creationDate
,
                CompanyID:responseData.companyID,
                CertificationAuthorityID:responseData.certificationAuthorityID
			};
		},

		mapCertificationRequestAssetsToModel:function(responseData){

			var items = [];
			if( responseData ){
				for ( var i = 0; i < responseData.length; i++ ){
					items.push(this.mapCertificationRequestAssetToModel(responseData[i]));
				}
			}
			return items;
		},

		mapCertificationRequestAssetToChaincode:function(oModel, newCertificationRequestAsset){

			if ( newCertificationRequestAsset === true ) {
				return {
						"ID":this.getNewCertificationRequestAssetID(oModel),
						"docType":"Asset.CertificationRequestAsset",
						
                        "status":oModel.getProperty("/newCertificationRequestAsset/Status"),
                        "creationDate":oModel.getProperty("/newCertificationRequestAsset/CreationDate")
,
                  "companyID":oModel.getProperty("/newCertificationRequestAsset/CompanyID"),
                  "certificationAuthorityID":oModel.getProperty("/newCertificationRequestAsset/CertificationAuthorityID")
				};
			} else {
				return {
						"ID":oModel.getProperty("/selectedCertificationRequestAsset/ID"),
						"docType":"Asset.CertificationRequestAsset",
						
                        "status":oModel.getProperty("/selectedCertificationRequestAsset/Status"),
                        "creationDate":oModel.getProperty("/selectedCertificationRequestAsset/CreationDate")
,
                  "companyID":oModel.getProperty("/selectedCertificationRequestAsset/CompanyID"),
                  "certificationAuthorityID":oModel.getProperty("/selectedCertificationRequestAsset/CertificationAuthorityID")
				};
			}
		},

		mapCertificationRequestAssetToLocalStorage : function(oModel, newCertificationRequestAsset){

			return this.mapCertificationRequestAssetToChaincode(oModel, newCertificationRequestAsset);
		},

		getNewCertificationRequestAssetID:function(oModel){

		    if ( typeof oModel.getProperty("/newCertificationRequestAsset/ID") === "undefined" ||
		    		oModel.getProperty("/newCertificationRequestAsset/ID") === ""
		    	){
			    var iD = "CertificationRequestAsset";
			    var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
			    for( var i = 0; i < 8; i++ ){
			        iD += possible.charAt(Math.floor(Math.random() * possible.length));
			    }
		    } else {
				iD = oModel.getProperty("/newCertificationRequestAsset/ID");
			}
			oModel.setProperty("/newCertificationRequestAsset/ID",iD);
		    return iD;
		}
	};
});
