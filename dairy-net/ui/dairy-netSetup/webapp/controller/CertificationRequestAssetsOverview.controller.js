sap.ui.define([
	"dairy-netSetup/controller/BaseController",
	"dairy-netSetup/model/modelsBase",
	"dairy-netSetup/util/messageProvider",
	"dairy-netSetup/util/localStoreCertificationRequestAssets",
	"dairy-netSetup/util/bizNetAccessCertificationRequestAssets",
	"sap/ui/core/routing/History",
	"sap/ui/model/Filter",
	"sap/ui/model/FilterOperator",
	"sap/ui/Device"
], function(
		BaseController,
		modelsBase,
		messageProvider,
		localStoreCertificationRequestAssets,
		bizNetAccessCertificationRequestAssets,
		History,
		Filter,
		FilterOperator,
		Device
	) {
	"use strict";

	return BaseController.extend("dairy-netSetup.controller.CertificationRequestAssetsOverview", {

		onInit : function(){

			var oList = this.byId("certificationRequestAssetsList");
			this._oList = oList;
			this._oListFilterState = {
				aFilter : [],
				aSearch : []
			};
			localStoreCertificationRequestAssets.init();
			if ( typeof this.getOwnerComponent().getModel("Messages") === "undefined" ){
				this.getOwnerComponent().setModel(modelsBase.createMessagesModel(), "Messages");
			}
			messageProvider.init(this.getOwnerComponent().getModel("Messages"));
		},

		onSelectionChange : function (oEvent) {

			this._showDetail(oEvent.getParameter("listItem") || oEvent.getSource());
		},

		onAdd : function() {

			this.getRouter().navTo("certificationRequestAsset", {certificationRequestAssetId : "___new"});
		},

		onRemoveAll : function(){

			bizNetAccessCertificationRequestAssets.removeAllCertificationRequestAssets(this.getOwnerComponent(), this.getOwnerComponent().getModel("CertificationRequestAssets"));
		},

		onSearch : function(oEvent) {

			var sQuery = oEvent.getParameter("query");
			if (sQuery) {
				this._oListFilterState.aSearch = [new Filter("ID", FilterOperator.Contains, sQuery)];
			} else {
				this._oListFilterState.aSearch = [];
			}
			this._applyFilterSearch();
			var bReplace = !Device.system.phone;
			var oModel = this.getModel("CertificationRequestAssets");
			oModel.setProperty(
				"/selectedCertificationRequestAsset",
				_.findWhere(oModel.getProperty("/certificationRequestAssetCollection/items"),
					{
						ID: oModel.getProperty("/searchCertificationRequestAssetID")
					},
				this));
			this.getRouter().navTo("certificationRequestAsset", {
				certificationRequestAssetId : oModel.getProperty("/selectedCertificationRequestAsset").ID
			}, bReplace);
		},

		_onToggleBaaS : function(){

			this.getOwnerComponent().getModel("TestSwitch").setProperty("/testMode",!(this.getView().byId("__buttonToggleCertificationRequestAssets").getPressed()));
			var location = this.getRunMode().location;
			messageProvider.addMessage("Success", "Switching Network for Blockchain Data to: ", "No Description", location, 1, "", "http://www.sap.com");
			if ( this.getRunMode().testMode && localStoreCertificationRequestAssets.getCertificationRequestAssetData() === null ){
				localStoreCertificationRequestAssets.init();
			}
			bizNetAccessCertificationRequestAssets.loadAllCertificationRequestAssets(this.getOwnerComponent(), this.getModel("CertificationRequestAssets"));
		},

		_showDetail : function(oItem) {

			var bReplace = !Device.system.phone;
			var oModel = this.getModel("CertificationRequestAssets");
			this.getRouter().navTo("certificationRequestAsset", {
				certificationRequestAssetId : oModel.getObject(oItem.getBindingContextPath()).ID
			}, bReplace);
		},

		_applyFilterSearch : function () {
			var aFilters = this._oListFilterState.aSearch.concat(this._oListFilterState.aFilter),
				oViewModel = this.getModel("CertificationRequestAssets");
			this._oList.getBinding("items").filter(aFilters, "Application");
			if (aFilters.length !== 0) {
				oViewModel.setProperty("/noDataText", this.getResourceBundle().getText("_labelNoCertificationRequestAssetsText"));
			}
		},

		onMessagePress: function() {

			this.onShowMessageDialog("dairy-netSetup Log");
		},

		onNavBack : function() {
			this.getRouter().navTo("home");
		}
	});
});
