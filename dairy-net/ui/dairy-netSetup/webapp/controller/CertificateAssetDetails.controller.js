sap.ui.define([
	"dairy-netSetup/controller/BaseController",
	"dairy-netSetup/util/bizNetAccessCertificateAssets"
], function(
		BaseController,
		bizNetAccessCertificateAssets
	) {
	"use strict";

	return BaseController.extend("dairy-netSetup.controller.CertificateAssetDetails", {

		onInit : function(){

			this.getOwnerComponent().getRouter().getRoute("certificateAsset").attachPatternMatched(this._onObjectMatched, this);
		},

		_onObjectMatched : function (oEvent) {

			var pId = oEvent.getParameter("arguments").certificateAssetId;
			if ( pId === "___new" ) {
				this.getView().byId("__barCertificateAsset").setSelectedKey("New");
			} else {
				bizNetAccessCertificateAssets.loadCertificateAsset(this.getView().getModel("CertificateAssets"), oEvent.getParameter("arguments").certificateAssetId);
			}
		},

		addNew : function(){

			var oModel = this.getView().getModel("CertificateAssets");
			if ( oModel.getProperty("/newCertificateAsset/Alias") !== "" ||
				   oModel.getProperty("/newCertificateAsset/Description") !== "" ) {
				var certificateAssetId = bizNetAccessCertificateAssets.addNewCertificateAsset(this.getOwnerComponent(), oModel);
				this.getView().byId("__barCertificateAsset").setSelectedKey("Details");
				bizNetAccessCertificateAssets.loadCertificateAsset(this.getView().getModel("CertificateAssets"), certificateAssetId);
			} else {
				sap.m.MessageToast.show("Please enter valid data in all fields", {});
			}
			this._clearNewAsset();
		},

		removeCertificateAsset : function(){

			var oModel = this.getView().getModel("CertificateAssets");
			bizNetAccessCertificateAssets.removeCertificateAsset(this.getOwnerComponent(), oModel, oModel.getProperty("/selectedCertificateAsset/ID"));
		},

		_clearNewAsset : function(){

			this.getView().getModel("CertificateAssets").setProperty("/newCertificateAsset",{});
		}
	});

});
