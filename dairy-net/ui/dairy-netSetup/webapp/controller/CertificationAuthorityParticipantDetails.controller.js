sap.ui.define([
	"dairy-netSetup/controller/BaseController",
	"dairy-netSetup/util/bizNetAccessCertificationAuthorityParticipants"
], function(
		BaseController,
		bizNetAccessCertificationAuthorityParticipants
	) {
	"use strict";

	return BaseController.extend("dairy-netSetup.controller.CertificationAuthorityParticipantDetails", {

		onInit : function(){

			this.getOwnerComponent().getRouter().getRoute("certificationAuthorityParticipant").attachPatternMatched(this._onObjectMatched, this);
		},

		_onObjectMatched : function (oEvent) {

			var pId = oEvent.getParameter("arguments").certificationAuthorityParticipantId;
			if ( pId === "___new" ) {
				this.getView().byId("__barCertificationAuthorityParticipant").setSelectedKey("New");
			} else {
				bizNetAccessCertificationAuthorityParticipants.loadCertificationAuthorityParticipant(this.getView().getModel("CertificationAuthorityParticipants"), oEvent.getParameter("arguments").certificationAuthorityParticipantId);
			}
		},

		addNew : function(){

			var oModel = this.getView().getModel("CertificationAuthorityParticipants");
			if ( oModel.getProperty("/newCertificationAuthorityParticipant/Alias") !== "" ||
				   oModel.getProperty("/newCertificationAuthorityParticipant/Description") !== "" ) {
				var certificationAuthorityParticipantId = bizNetAccessCertificationAuthorityParticipants.addNewCertificationAuthorityParticipant(this.getOwnerComponent(), oModel);
				this.getView().byId("__barCertificationAuthorityParticipant").setSelectedKey("Details");
				bizNetAccessCertificationAuthorityParticipants.loadCertificationAuthorityParticipant(this.getView().getModel("CertificationAuthorityParticipants"), certificationAuthorityParticipantId);
			} else {
				sap.m.MessageToast.show("Please enter valid data in all fields", {});
			}
			this._clearNewAsset();
		},

		removeCertificationAuthorityParticipant : function(){

			var oModel = this.getView().getModel("CertificationAuthorityParticipants");
			bizNetAccessCertificationAuthorityParticipants.removeCertificationAuthorityParticipant(this.getOwnerComponent(), oModel, oModel.getProperty("/selectedCertificationAuthorityParticipant/ID"));
		},

		_clearNewAsset : function(){

			this.getView().getModel("CertificationAuthorityParticipants").setProperty("/newCertificationAuthorityParticipant",{});
		}
	});

});
