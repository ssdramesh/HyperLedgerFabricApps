sap.ui.define([
	"dairy-netSetup/controller/BaseController",
	"dairy-netSetup/util/bizNetAccessCompanyParticipants"
], function(
		BaseController,
		bizNetAccessCompanyParticipants
	) {
	"use strict";

	return BaseController.extend("dairy-netSetup.controller.CompanyParticipantDetails", {

		onInit : function(){

			this.getOwnerComponent().getRouter().getRoute("companyParticipant").attachPatternMatched(this._onObjectMatched, this);
		},

		_onObjectMatched : function (oEvent) {

			var pId = oEvent.getParameter("arguments").companyParticipantId;
			if ( pId === "___new" ) {
				this.getView().byId("__barCompanyParticipant").setSelectedKey("New");
			} else {
				bizNetAccessCompanyParticipants.loadCompanyParticipant(this.getView().getModel("CompanyParticipants"), oEvent.getParameter("arguments").companyParticipantId);
			}
		},

		addNew : function(){

			var oModel = this.getView().getModel("CompanyParticipants");
			if ( oModel.getProperty("/newCompanyParticipant/Alias") !== "" ||
				   oModel.getProperty("/newCompanyParticipant/Description") !== "" ) {
				var companyParticipantId = bizNetAccessCompanyParticipants.addNewCompanyParticipant(this.getOwnerComponent(), oModel);
				this.getView().byId("__barCompanyParticipant").setSelectedKey("Details");
				bizNetAccessCompanyParticipants.loadCompanyParticipant(this.getView().getModel("CompanyParticipants"), companyParticipantId);
			} else {
				sap.m.MessageToast.show("Please enter valid data in all fields", {});
			}
			this._clearNewAsset();
		},

		removeCompanyParticipant : function(){

			var oModel = this.getView().getModel("CompanyParticipants");
			bizNetAccessCompanyParticipants.removeCompanyParticipant(this.getOwnerComponent(), oModel, oModel.getProperty("/selectedCompanyParticipant/ID"));
		},

		_clearNewAsset : function(){

			this.getView().getModel("CompanyParticipants").setProperty("/newCompanyParticipant",{});
		}
	});

});
