sap.ui.define([
	"dairy-netSetup/controller/BaseController",
	"dairy-netSetup/util/messageProvider",

        "dairy-netSetup/util/bizNetAccessAdministratorParticipants",
        "dairy-netSetup/model/modelsAdministratorParticipants",

        "dairy-netSetup/util/bizNetAccessBatchAssets",
        "dairy-netSetup/model/modelsBatchAssets",

        "dairy-netSetup/util/bizNetAccessCertificateAssets",
        "dairy-netSetup/model/modelsCertificateAssets",

        "dairy-netSetup/util/bizNetAccessCertificationAuthorityParticipants",
        "dairy-netSetup/model/modelsCertificationAuthorityParticipants",

        "dairy-netSetup/util/bizNetAccessCertificationRequestAssets",
        "dairy-netSetup/model/modelsCertificationRequestAssets",

        "dairy-netSetup/util/bizNetAccessCompanyParticipants",
        "dairy-netSetup/model/modelsCompanyParticipants",

        "dairy-netSetup/util/bizNetAccessTouchAssets",
        "dairy-netSetup/model/modelsTouchAssets",

	"dairy-netSetup/model/modelsBase"
], function(
		BaseController,
		messageProvider,

        bizNetAccessAdministratorParticipants,
        modelsAdministratorParticipants,

        bizNetAccessBatchAssets,
        modelsBatchAssets,

        bizNetAccessCertificateAssets,
        modelsCertificateAssets,

        bizNetAccessCertificationAuthorityParticipants,
        modelsCertificationAuthorityParticipants,

        bizNetAccessCertificationRequestAssets,
        modelsCertificationRequestAssets,

        bizNetAccessCompanyParticipants,
        modelsCompanyParticipants,

        bizNetAccessTouchAssets,
        modelsTouchAssets,

		modelsBase
	) {
	"use strict";

	return BaseController.extend("dairy-netSetup.controller.Selection", {

		onInit: function(){
			if ( typeof this.getOwnerComponent().getModel("Messages") === "undefined" ){
				this.getOwnerComponent().setModel(modelsBase.createMessagesModel(), "Messages");
			}
			messageProvider.init(this.getOwnerComponent().getModel("Messages"));
		},

		onSelect: function(oEvent) {
			switch (oEvent.getSource().getText()) {

        case "AdministratorParticipants":
          this.getView().getModel("Selection").setProperty("/entityName", "administratorParticipant");
          this.getOwnerComponent().setModel(modelsAdministratorParticipants.createAdministratorParticipantsModel(), "AdministratorParticipants");
          this.loadMetaData("administratorParticipant", this.getModel("AdministratorParticipants"));
          bizNetAccessAdministratorParticipants.loadAllAdministratorParticipants(this.getOwnerComponent(), this.getView().getModel("AdministratorParticipants"));
          this.getOwnerComponent().getRouter().navTo("administratorParticipants", {});
          break;

        case "BatchAssets":
          this.getView().getModel("Selection").setProperty("/entityName", "batchAsset");
          this.getOwnerComponent().setModel(modelsBatchAssets.createBatchAssetsModel(), "BatchAssets");
          this.loadMetaData("batchAsset", this.getModel("BatchAssets"));
          bizNetAccessBatchAssets.loadAllBatchAssets(this.getOwnerComponent(), this.getView().getModel("BatchAssets"));
          this.getOwnerComponent().getRouter().navTo("batchAssets", {});
          break;

        case "CertificateAssets":
          this.getView().getModel("Selection").setProperty("/entityName", "certificateAsset");
          this.getOwnerComponent().setModel(modelsCertificateAssets.createCertificateAssetsModel(), "CertificateAssets");
          this.loadMetaData("certificateAsset", this.getModel("CertificateAssets"));
          bizNetAccessCertificateAssets.loadAllCertificateAssets(this.getOwnerComponent(), this.getView().getModel("CertificateAssets"));
          this.getOwnerComponent().getRouter().navTo("certificateAssets", {});
          break;

        case "CertificationAuthorityParticipants":
          this.getView().getModel("Selection").setProperty("/entityName", "certificationAuthorityParticipant");
          this.getOwnerComponent().setModel(modelsCertificationAuthorityParticipants.createCertificationAuthorityParticipantsModel(), "CertificationAuthorityParticipants");
          this.loadMetaData("certificationAuthorityParticipant", this.getModel("CertificationAuthorityParticipants"));
          bizNetAccessCertificationAuthorityParticipants.loadAllCertificationAuthorityParticipants(this.getOwnerComponent(), this.getView().getModel("CertificationAuthorityParticipants"));
          this.getOwnerComponent().getRouter().navTo("certificationAuthorityParticipants", {});
          break;

        case "CertificationRequestAssets":
          this.getView().getModel("Selection").setProperty("/entityName", "certificationRequestAsset");
          this.getOwnerComponent().setModel(modelsCertificationRequestAssets.createCertificationRequestAssetsModel(), "CertificationRequestAssets");
          this.loadMetaData("certificationRequestAsset", this.getModel("CertificationRequestAssets"));
          bizNetAccessCertificationRequestAssets.loadAllCertificationRequestAssets(this.getOwnerComponent(), this.getView().getModel("CertificationRequestAssets"));
          this.getOwnerComponent().getRouter().navTo("certificationRequestAssets", {});
          break;

        case "CompanyParticipants":
          this.getView().getModel("Selection").setProperty("/entityName", "companyParticipant");
          this.getOwnerComponent().setModel(modelsCompanyParticipants.createCompanyParticipantsModel(), "CompanyParticipants");
          this.loadMetaData("companyParticipant", this.getModel("CompanyParticipants"));
          bizNetAccessCompanyParticipants.loadAllCompanyParticipants(this.getOwnerComponent(), this.getView().getModel("CompanyParticipants"));
          this.getOwnerComponent().getRouter().navTo("companyParticipants", {});
          break;

        case "TouchAssets":
          this.getView().getModel("Selection").setProperty("/entityName", "touchAsset");
          this.getOwnerComponent().setModel(modelsTouchAssets.createTouchAssetsModel(), "TouchAssets");
          this.loadMetaData("touchAsset", this.getModel("TouchAssets"));
          bizNetAccessTouchAssets.loadAllTouchAssets(this.getOwnerComponent(), this.getView().getModel("TouchAssets"));
          this.getOwnerComponent().getRouter().navTo("touchAssets", {});
          break;

			}
		},

		removeAllEntities: function() {

			var msgStripID = this.getView().byId("__stripMessage");
			var location = this.getRunMode().location;


      if (typeof this.getOwnerComponent().getModel("AdministratorParticipants") === "undefined") {
        this.getOwnerComponent().setModel(modelsAdministratorParticipants.createAdministratorParticipantsModel(), "AdministratorParticipants");
      }
      bizNetAccessAdministratorParticipants.removeAllAdministratorParticipants(this.getOwnerComponent(), this.getView().getModel("AdministratorParticipants"));
      messageProvider.addMessage("Success", "All AdministratorParticipants deleted from sample-network", "No Description", location, 1, "", "http://www.sap.com");

      if (typeof this.getOwnerComponent().getModel("BatchAssets") === "undefined") {
        this.getOwnerComponent().setModel(modelsBatchAssets.createBatchAssetsModel(), "BatchAssets");
      }
      bizNetAccessBatchAssets.removeAllBatchAssets(this.getOwnerComponent(), this.getView().getModel("BatchAssets"));
      messageProvider.addMessage("Success", "All BatchAssets deleted from sample-network", "No Description", location, 1, "", "http://www.sap.com");

      if (typeof this.getOwnerComponent().getModel("CertificateAssets") === "undefined") {
        this.getOwnerComponent().setModel(modelsCertificateAssets.createCertificateAssetsModel(), "CertificateAssets");
      }
      bizNetAccessCertificateAssets.removeAllCertificateAssets(this.getOwnerComponent(), this.getView().getModel("CertificateAssets"));
      messageProvider.addMessage("Success", "All CertificateAssets deleted from sample-network", "No Description", location, 1, "", "http://www.sap.com");

      if (typeof this.getOwnerComponent().getModel("CertificationAuthorityParticipants") === "undefined") {
        this.getOwnerComponent().setModel(modelsCertificationAuthorityParticipants.createCertificationAuthorityParticipantsModel(), "CertificationAuthorityParticipants");
      }
      bizNetAccessCertificationAuthorityParticipants.removeAllCertificationAuthorityParticipants(this.getOwnerComponent(), this.getView().getModel("CertificationAuthorityParticipants"));
      messageProvider.addMessage("Success", "All CertificationAuthorityParticipants deleted from sample-network", "No Description", location, 1, "", "http://www.sap.com");

      if (typeof this.getOwnerComponent().getModel("CertificationRequestAssets") === "undefined") {
        this.getOwnerComponent().setModel(modelsCertificationRequestAssets.createCertificationRequestAssetsModel(), "CertificationRequestAssets");
      }
      bizNetAccessCertificationRequestAssets.removeAllCertificationRequestAssets(this.getOwnerComponent(), this.getView().getModel("CertificationRequestAssets"));
      messageProvider.addMessage("Success", "All CertificationRequestAssets deleted from sample-network", "No Description", location, 1, "", "http://www.sap.com");

      if (typeof this.getOwnerComponent().getModel("CompanyParticipants") === "undefined") {
        this.getOwnerComponent().setModel(modelsCompanyParticipants.createCompanyParticipantsModel(), "CompanyParticipants");
      }
      bizNetAccessCompanyParticipants.removeAllCompanyParticipants(this.getOwnerComponent(), this.getView().getModel("CompanyParticipants"));
      messageProvider.addMessage("Success", "All CompanyParticipants deleted from sample-network", "No Description", location, 1, "", "http://www.sap.com");

      if (typeof this.getOwnerComponent().getModel("TouchAssets") === "undefined") {
        this.getOwnerComponent().setModel(modelsTouchAssets.createTouchAssetsModel(), "TouchAssets");
      }
      bizNetAccessTouchAssets.removeAllTouchAssets(this.getOwnerComponent(), this.getView().getModel("TouchAssets"));
      messageProvider.addMessage("Success", "All TouchAssets deleted from sample-network", "No Description", location, 1, "", "http://www.sap.com");


			this.showMessageStrip(msgStripID,"All Existing dairy-net data deleted from SAP BaaS", "S");
		},

		onToggleBaaS: function() {

			this.onToggleRunMode(this.getView().byId("__stripMessage"));
		},

		onMessagePress: function() {

			this.onShowMessageDialog("dairy-net Maintenance Log");
		}
	});
});
