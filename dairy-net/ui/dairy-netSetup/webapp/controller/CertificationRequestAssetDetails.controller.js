sap.ui.define([
	"dairy-netSetup/controller/BaseController",
	"dairy-netSetup/util/bizNetAccessCertificationRequestAssets"
], function(
		BaseController,
		bizNetAccessCertificationRequestAssets
	) {
	"use strict";

	return BaseController.extend("dairy-netSetup.controller.CertificationRequestAssetDetails", {

		onInit : function(){

			this.getOwnerComponent().getRouter().getRoute("certificationRequestAsset").attachPatternMatched(this._onObjectMatched, this);
		},

		_onObjectMatched : function (oEvent) {

			var pId = oEvent.getParameter("arguments").certificationRequestAssetId;
			if ( pId === "___new" ) {
				this.getView().byId("__barCertificationRequestAsset").setSelectedKey("New");
			} else {
				bizNetAccessCertificationRequestAssets.loadCertificationRequestAsset(this.getView().getModel("CertificationRequestAssets"), oEvent.getParameter("arguments").certificationRequestAssetId);
			}
		},

		addNew : function(){

			var oModel = this.getView().getModel("CertificationRequestAssets");
			if ( oModel.getProperty("/newCertificationRequestAsset/Alias") !== "" ||
				   oModel.getProperty("/newCertificationRequestAsset/Description") !== "" ) {
				var certificationRequestAssetId = bizNetAccessCertificationRequestAssets.addNewCertificationRequestAsset(this.getOwnerComponent(), oModel);
				this.getView().byId("__barCertificationRequestAsset").setSelectedKey("Details");
				bizNetAccessCertificationRequestAssets.loadCertificationRequestAsset(this.getView().getModel("CertificationRequestAssets"), certificationRequestAssetId);
			} else {
				sap.m.MessageToast.show("Please enter valid data in all fields", {});
			}
			this._clearNewAsset();
		},

		removeCertificationRequestAsset : function(){

			var oModel = this.getView().getModel("CertificationRequestAssets");
			bizNetAccessCertificationRequestAssets.removeCertificationRequestAsset(this.getOwnerComponent(), oModel, oModel.getProperty("/selectedCertificationRequestAsset/ID"));
		},

		_clearNewAsset : function(){

			this.getView().getModel("CertificationRequestAssets").setProperty("/newCertificationRequestAsset",{});
		}
	});

});
